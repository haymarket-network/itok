﻿using ITOK.Core.Common.Interfaces;
using ITOK.Core.Data.Model;

namespace CMS.Helpers.MembershipProviders
{
    internal class MembershipProvider : IMembershipProvider
    {
        public User ActiveUser
        {
            get { return MembershipHelper.GetActiveUser(); }
        }

        public string ActiveUserId
        {
            get
            {
                return MembershipHelper.GetActiveUserId;
            }
        }
    }
}