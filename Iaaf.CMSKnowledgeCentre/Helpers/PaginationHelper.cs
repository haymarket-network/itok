﻿using ITOK.Core.ViewModels;
using System.Web;

namespace CMS.Helpers
{
    public class PaginationHelper
    {
        private const string InstanceKeyPrefix = "Pagination_";

        private string _instanceKey;

        public PaginationHelper(string instanceKey, string baseUrl = "", int? pageSize = null)
        {
            _instanceKey = InstanceKeyPrefix + instanceKey;
            SetBaseUrl(baseUrl);
            SetPageSize(pageSize);
        }

        public void SetPage(int page)
        {
            var pageModel = PaginationModel;
            pageModel.CurrentPage = page;
            PaginationModel = pageModel;
        }

        public void SetTotalItemCount(int count)
        {
            var pageModel = PaginationModel;
            pageModel.TotalItemCount = count;
            PaginationModel = pageModel;
        }

        public void SetPageSize(int? pageSize)
        {
            if (pageSize != null)
            {
                var pageModel = PaginationModel;
                pageModel.PageSize = (int)pageSize;
                PaginationModel = pageModel;
            }
        }

        public void SetBaseUrl(string baseUrl)
        {
            var pageModel = PaginationModel;
            pageModel.BaseUrl = baseUrl;
            PaginationModel = pageModel;
        }

        public void Reset()
        {
            _paginationModel = null;
            HttpContext.Current.Session[_instanceKey] = null;
        }

        private PaginationModel _paginationModel;

        public PaginationModel PaginationModel
        {
            get
            {
                _paginationModel = HttpContext.Current.Session[_instanceKey] as PaginationModel;
                if (_paginationModel == null)
                {
                    _paginationModel = new PaginationModel();
                    HttpContext.Current.Session[_instanceKey] = _paginationModel;
                }
                return _paginationModel;
            }
            set
            {
                HttpContext.Current.Session[_instanceKey] = value;
            }
        }
    }
}