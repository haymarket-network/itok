﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITOK.Core.Common.Constants;
using ITOK.Core.Services.Security;
using ITOK.Core.Services.Core;
using ITOK.Core.Common;

namespace CMS.Helpers
{
    public class GlobalVariables
    {
        public static Brand CurrentBrand
        {
            get
            {

                // return LoginManager.CurrentUser.CurrentBrand;
                return Brand.itok;

                ///TODO: return according to URL
            }

        }

        public static string ITOKLeaderEmail
        {
            get
            {
                ISiteSettingsService _settingsService = AppKernel.GetInstance<ISiteSettingsService>();
                return _settingsService.ByKey("ITOK.Leader.Email").Value;
            }
        }
    }
}