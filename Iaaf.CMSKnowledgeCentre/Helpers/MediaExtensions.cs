﻿using ITOK.Core.Common.Constants;
using ITOK.Core.Data.Model;
using System.Web.Mvc;

namespace CMS.Helpers.HtmlExtensions
{
    public static class MediaExtensions
    {
        public static MvcHtmlString GetMediaUri(this HtmlHelper helper, MediaProfile profile, Media media)
        {
            return new MvcHtmlString(MediaHelper.GetMediaUri(profile, media));
        }

        public static MvcHtmlString GetMediaUri(this HtmlHelper helper, MediaRatio ratio, Media media)
        {
            return new MvcHtmlString(MediaHelper.GetMediaUri(ratio, media));
        }

        public static MvcHtmlString GetSubmittedMediaUri(this HtmlHelper helper, MediaProfile profile, SubmittedMedia media)
        {
            return new MvcHtmlString(MediaHelper.GetSubmittedMediaUri(profile, media));
        }
        public static MvcHtmlString GetSubmittedMediaUri(this HtmlHelper helper, MediaRatio ratio, SubmittedMedia media)
        {
            return new MvcHtmlString(MediaHelper.GetSubmittedMediaUri(ratio, media));
        }

        public static MvcHtmlString GetVideoThumbnailUri(this HtmlHelper helper, Media media)
        {
            return new MvcHtmlString(MediaHelper.GetVideoThumbnailUri(media));
        }
    }
}