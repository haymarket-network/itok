﻿using System;
using System.Web;

namespace CMS.Helpers
{
    public class EntityEditHelper<T> where T : class, new()
    {
        private const string EntityCacheKeyPrefix = "EntityEditCache_";

        public T Cache
        {
            get
            {
                var entity = HttpContext.Current.Session[EntityCacheKeyPrefix + typeof(T).FullName] as T;
                if (entity == null)
                {
                    entity = Activator.CreateInstance(typeof(T)) as T;
                    HttpContext.Current.Session[EntityCacheKeyPrefix + typeof(T).FullName] = entity;
                }
                return entity;
            }
            set
            {
                HttpContext.Current.Session[EntityCacheKeyPrefix + typeof(T).FullName] = value;
            }
        }

        public void ClearCache()
        {
            HttpContext.Current.Session.Remove(EntityCacheKeyPrefix + typeof(T).FullName);
        }
    }
}