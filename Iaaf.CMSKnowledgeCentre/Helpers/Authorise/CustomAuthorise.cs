﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITOK.Core.Services.Security;
using ITOK.Core.Common;
using System.Web.Routing;
using ITOK.Core.Services.CMS;
using ITOK.Core.Data;

namespace CMS.Helpers.Authorise
{
    public class CustomAuthorise : AuthorizeAttribute
    {
        private UserService _userService;// AppKernel.GetInstance<UserService>();
        private IMongoRepository1 mongoRepository;
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (!HasValidUser())
            {
                filterContext.Result =
                         new RedirectToRouteResult(
                             new RouteValueDictionary(new { controller = "User", action = "AccessDenied" }));

            }
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return HasValidUser();
        }

        private bool HasValidUser()
        {
            if (LoginManager.CurrentUser.Id != null && LoginManager.CurrentUser.Forename != null)
            {
                return true;
            }
            else
                return false;
            // default user
            // LoginManager.CurrentUser = user;
        }

    }
}