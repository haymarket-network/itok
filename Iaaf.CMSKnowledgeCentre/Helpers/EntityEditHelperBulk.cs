﻿using System;
using System.Web;
using System.Collections.Generic;

namespace CMS.Helpers
{
    public class EntityEditHelperBulk<T> : IEntityEditHelperBulk<T> where T : class, new()
    {
        private const string EntityCacheKeyPrefix = "EntityEditCache_";

        public List<T> Cache
        {
            get
            {
                var entity = HttpContext.Current.Session[EntityCacheKeyPrefix + typeof(T).FullName + "ListMedia"] as List<T>;
                if (entity == null)
                {
                    entity = Activator.CreateInstance(typeof(T)) as List<T>;
                    HttpContext.Current.Session[EntityCacheKeyPrefix + typeof(T).FullName + "ListMedia"] = entity;
                }
                return entity;
            }
            set
            {
                HttpContext.Current.Session[EntityCacheKeyPrefix + typeof(T).FullName + "ListMedia"] = value;
            }
        }

        public void SetCache(List<T> value)
        {
            Cache = value;
        }

        public void ClearCache()
        {
            HttpContext.Current.Session.Remove(EntityCacheKeyPrefix + typeof(T).FullName + "ListMedia");
        }

        public List<T> GetCache()
        {
            return Cache;
        }


    }

    public interface IEntityEditHelperBulk<T> where T : class
    {
        List<T> GetCache();
        void SetCache(List<T> value);
        void ClearCache();
    }
}