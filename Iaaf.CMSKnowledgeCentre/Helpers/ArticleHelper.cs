﻿using ITOK.Core.Common.Utils;
using ITOK.Core.Data.Model;
using System;
using System.Web.Configuration;
using System.Web.Mvc;
using ITOK.Core.Common.Utilities;
namespace CMS.Helpers
{
    public static class ArticleHelper
    {
        public static MvcHtmlString GetArticleUri(this HtmlHelper helper, Article article)
        {
            return new MvcHtmlString(ArticleHelper.GetArticleUri(article));
        }

        public static MvcHtmlString GetSubmissionPreviewUri(this HtmlHelper helper, SubmittedArticle article)
        {
            var Siteurl = WebConfigurationManager.AppSettings["SiteUrl"];

            var url = string.Format("previewsubmission/{0}", article.Id);


            return new MvcHtmlString(url);
        }


        public static string GetArticleUri(Article article)
        {
            var Siteurl = WebConfigurationManager.AppSettings["SiteUrl"];

            return string.Format("{2}/{3}/{0}/{1}", ArticleUtilities.UrlStringFromArticleType(article.ArticleType), article.UrlSlug, Siteurl, GlobalVariables.CurrentBrand);
        }        

        public static MvcHtmlString GetPreviewUri(this HtmlHelper helper, Article article)
        {

            return new MvcHtmlString(ArticleHelper.GetPreviewUri(article));
        }

        public static string GetPreviewUri(Article article)
        {
            var Siteurl = WebConfigurationManager.AppSettings["SiteUrl"];

            return string.Format("{2}/preview/{1}", ArticleUtilities.UrlStringFromArticleType(article.ArticleType), article.Id, Siteurl, GlobalVariables.CurrentBrand);
        }



    }
}