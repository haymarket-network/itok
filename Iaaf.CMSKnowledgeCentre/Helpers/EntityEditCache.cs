﻿using ITOK.Core.Data;
using System;
using System.Collections.Generic;
using ITOK.Core.Services.Security;

namespace CMS.Helpers
{
    public class EntityEditCache<T> : IDisposable where T : class
    {
        private const string EntityCacheKeyPrefix = "EntityEditCache_";
        private const string NullIdReplacement = "null_entity_id";

        private readonly Func<T, object> _idExpr;
        private readonly TempMongoRepository<T> _tmpRepo;
        private IDictionary<object, T> _transientEntities = new Dictionary<object, T>();

        public EntityEditCache(Func<T, object> idExpr)
        {
            if (idExpr == null) throw new ArgumentException("An id expression is required but the value passed in is null");
            _idExpr = idExpr;
            _tmpRepo = new TempMongoRepository<T>(idExpr, EntityCacheKeyPrefix, LoginManager.CurrentUser.Id);
        }

        public bool EntityIsAvailable(T entity)
        {
            return EntityIsAvailable(getId(entity));
        }

        public bool EntityIsAvailable(object id)
        {
            if (_transientEntities.ContainsKey(id)) return true;
            return _tmpRepo.Exists(id);
        }

        public T GetEntity(object id)
        {
            id = getId(id);
            if (_transientEntities.ContainsKey(id)) return _transientEntities[id];
            var entity = _tmpRepo.Get(id);
            if (entity != null) _transientEntities.Add(new KeyValuePair<object, T>(id, entity)); // Add to transient dictionary
            else throw new Exception("Couldn't retrieve article from cache");
            return entity;
        }

        public void SetEntity(T entity)
        {
            _transientEntities[getId(entity)] = entity;
        }

        public void ClearCache(object id)
        {
            _tmpRepo.Remove(id);
            if (_transientEntities.ContainsKey(id)) _transientEntities.Remove(id);
            // Immediately remove from mongo store if the entity exists in it
            if (_tmpRepo.Exists(id)) _tmpRepo.Remove(id);
        }

        //IDisposable
        public virtual void Dispose()
        {
            // Save transient entities to mongo
            foreach (var transientEntity in _transientEntities)
            {
                _tmpRepo.Set(transientEntity.Value);
            }
        }

        private object getId(T entity)
        {
            var id = _idExpr(entity);
            return getId(id);
        }

        private object getId(object id)
        {
            if (id != null && id.ToString() != "")
                return id;
            else
                return NullIdReplacement;

            //return id != null ? id : NullIdReplacement;
        }
    }
}