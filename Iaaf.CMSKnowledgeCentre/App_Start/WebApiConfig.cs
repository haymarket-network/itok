﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace CMS
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

             config.Routes.MapHttpRoute(
                name: "ConvertImageToBase64",
                routeTemplate: "api/image/convert-base64",
                defaults: new { controller = "Editor", action = "ConvertImageToBase64" }
            );

            

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
