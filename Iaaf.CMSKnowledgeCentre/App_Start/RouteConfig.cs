﻿using System.Web.Mvc;
using System.Web.Routing;

namespace CMS
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                 "CreateNewSubmission",
                 "Submissions/selecttag/{tagid}/{topicid}/{eventid}/{yearid}",
                 new { controller = "submissions", action = "selecttag"}
             );

            routes.MapRoute(
                "Submissionoverview",
                "Submissions/overview/{eventid}/{yearid}",
                new { controller = "submissions", action = "overview" }
            );
            routes.MapRoute(
                  "Paged",
                  "{controller}/page/{pageNumber}",
                  new { controller = "Article", action = "Page", pageNumber = UrlParameter.Optional }
              );


            routes.MapRoute(
               "CreateQuestion",
               "Question/GetQuestionDetails/{ParentTagSlug}/{ParentTagId}/{questionId}",
               new { controller = "submissions", action = "overview", questionId = UrlParameter.Optional }
           );

            routes.MapMvcAttributeRoutes();
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
