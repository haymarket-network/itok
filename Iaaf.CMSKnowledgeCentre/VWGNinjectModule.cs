﻿using ITOK.Core.Common;
using ITOK.Core.Data;
using ITOK.Core.Services.Core.Wrapped;
using Ninject.Modules;
using ITOK.Core.Services.Core;
using ITOK.Core.Services.CMS;
using ITagsService = ITOK.Core.Services.Core.ITagsService;
using TagsService = ITOK.Core.Services.Core.TagsService;
using UserService = ITOK.Core.Services.CMS.UserService;

namespace CMS
{
    public class ITOKNinjectModule : NinjectModule
    {
        public override void Load()
        {
            AppKernel.kernel.Settings.AllowNullInjection = true;
            AppKernel.kernel.Bind<IMongoRepository1>().To<MongoRepository>();
            AppKernel.kernel.Bind<IMongoOutputCacheRepository>().To<MongoOutputCacheRepository>();

            AppKernel.kernel.Bind<IUserService>().To<UserService>();
            AppKernel.kernel.Bind<IUserEventService>().To<UserEventService>();
            AppKernel.kernel.Bind<ITagsService>().To<TagsService>();
            AppKernel.kernel.Bind<ITagsServiceWrapped>().To<TagsServiceWrapped>();

            AppKernel.kernel.Bind<IQuestionServiceWrapped>().To<QuestionServiceWrapped>();
            AppKernel.kernel.Bind<IQuestionService>().To<QuestionService>();
            AppKernel.kernel.Bind<IAnswerService>().To<AnswerService>();
            AppKernel.kernel.Bind<IAnswerFlagService>().To<AnswerFlagService>();
            AppKernel.kernel.Bind<IAnswerServiceWrapped>().To<AnswerServiceWrapped>();
            AppKernel.kernel.Bind<ISiteSettingsService>().To<SiteSettingsService>();
            AppKernel.kernel.Bind<IQuestionTemplateService>().To<QuestionTemplateService>();
            AppKernel.kernel.Bind<IQuestionCommentService>().To<QuestionCommentService>();
        }
    }
}
