﻿/*

Notes:
    
- Based on the jQuery template from http://css-tricks.com/snippets/jquery/jquery-plugin-template/

Dependencies:

- jqote2
- Twitter Bootstrap framework (javascript and css)

*/

(function ($) {

    var instanceReference = 0;

    $.tagSelector = function (listElement, options) {
        // To avoid scope issues, use 'base' instead of 'this'
        // to reference this class from internal events and functions.
        var base = this;

        // *** Constants ***

        var ADD_TAGS_HTML = "<li><a href='#' class='add-tags btn btn-primary'>Edit</a></li>";
        var NO_RESULTS_HTML = "<li class='no-results'>No Results</li>";

        // *** Instance Variables ***

        // Access to jQuery and DOM versions of element
        base.$listElement = $(listElement);
        base.listElement = listElement;

        // Store the instance reference number
        base.instanceNumber = instanceReference;
        // Declare the timeout variable used for searching
        base.searchTimer;
        // Declare the array that holds selected result ids
        base.selectedResults = [];
        // Increment the instance reference
        instanceReference++;

        // Add a reverse reference to the DOM object
        base.$listElement.data("tagSelector", base);

        // Add a tag-selector class to the list element
        base.$listElement.addClass("tag-selector");

        base.cachedResults = {};

        base.init = function () {

            base.options = $.extend({}, $.tagSelector.defaultOptions, options);

            // *** Initialisation Code ***

            // Set the jqote tag
            $.jqotetag("*");

            // Render the tag selector markup
            base.renderTagSelector();

            // Bind all events
            bindWidgetLayoutEvents();
            bindResultEvents();
            bindListEvents();

            // Get selected results and render
            getSelectedResults();

            // Set up the tag selector as a modal
            $("#tagSelector" + base.instanceNumber).modal({
                show: false
            });

        };

        base.addTagHandler = function (e) {
            return false;
        };
        base.renderTagSelector = function () {
            // Package the data required to render the container markup
            var viewModel = {
                instanceNumber: base.instanceNumber,
                heading: base.options.heading
            };
            base.$widget = $($("#tagSelectorTemplate").jqote(viewModel));
            // Insert the widget into the document
            $("body").append(base.$widget);

            if(console && console.log) console.log("Rendered the core TagSelector markup.");
        };
        base.reload = function () {
            getSelectedResults();
        };
        base.open = function () {
            base.$widget.modal("show");
            return false;
        };
        base.close = function () {
            base.$widget.modal("hide");
            return false;
        };

        // Bind events for the tag selector markup
        function bindWidgetLayoutEvents() {
           
            base.$widget.on("keyup", ".tag-search", searchTextChanged);
            base.$widget.on("click", ".done-btn", base.close);
        }
        function bindResultEvents() {
            base.$widget.on("click", ".add-result", addResult);
             base.$widget.on("click", ".add-newtag", addNewTag);
            base.$widget.on("click", ".remove", removeResult);
        }
        function bindListEvents() {
            base.$listElement.on("click", ".add-tags", base.open);
            base.$listElement.on("click", ".remove", removeResult);
        }


        function searchTextChanged(e) {
            var searchBox = $(e.currentTarget);
            if (searchBox.val() == "" || searchBox.val() == " ") {
                base.$widget.find(".tag-selector-results ul").html(NO_RESULTS_HTML);
            } else {
                clearTimeout(base.searchTimer);
                base.searchTimer = setTimeout(function () {
                    getResults();
                }, base.options.searchTimeout);
            }
            return false;
        }
        function getResults(success) {
            // Build the query data for a search or list request to the server
            var queryData = {
                searchText: base.$widget.find(".tag-search").val()
            };            

            $.ajax({
                url: base.options.searchUrl,
                data: queryData,
                success: function (resultsView) {
                    if (resultsView == "") {
                        base.$widget.find(".tag-selector-results ul").html(NO_RESULTS_HTML);
                    } else {
                        // Show results
                        base.$widget.find(".tag-selector-results ul").html(resultsView);
                    }

                    if (success) return success();
                },
                error: function (a, b, c) {
                    if (console && console.error && JSON && JSON.stringify) console.error("a: " + JSON.stringify(a) + "| b: " + b + "|c: " + c)
                }
            });
        }

        function getSelectedResults() {
            $.ajax({
                url: base.options.listSelectedUrl,
                data: base.options.additionalRequestData,
                success: function (resultsView) {
                    if (resultsView == "") {
                        base.$widget.find(".tag-selector-selected-results ul").html(NO_RESULTS_HTML);
                    } else {
                        // Show results
                        base.$widget.find(".tag-selector-selected-results ul").html(resultsView);
                        base.$listElement.html(resultsView);
                        base.$listElement.append(ADD_TAGS_HTML);
                    }
                },
                error: function (a, b, c) {
                    if (console && console.error && JSON && JSON.stringify) console.error("a: " + JSON.stringify(a) + "| b: " + b + "|c: " + c)
                }
            });
        }

            function addNewTag()
        {            
            var selectedText = base.$widget.find(".tag-search").val();
            

            var data = {};
            data[base.options.idArgumentName] = selectedText;
            data = $.extend(data, base.options.additionalRequestData);
            createUrl = "/Article/CreateNewTag";
            
            if (hncms.createTagUrl) createUrl = hncms.createTagUrl;
            $.ajax({
                url: createUrl,
                data: data,
                success: function (resultsView) {
                    if (resultsView != "") {
                        // Remove no-results list item if it exists and add the new selection
                        base.$widget.find(".tag-selector-selected-results ul li.no-results").remove();
                        base.$listElement.find("li.no-results").remove();

                        if (base.options.singleMode && base.$widget.find(".tag-selector-selected-results ul li").length > 0) {
                            base.$widget.find(".tag-selector-selected-results ul").html(resultsView);
                            base.$listElement.find("li").last().prev().replaceWith(resultsView);
                        } else {
                            base.$widget.find(".tag-selector-selected-results ul").append(resultsView);
                            base.$listElement.find("li").last().before(resultsView);
                        }
                        if (base.options.resultAdded) {
                            base.options.resultAdded(selectedEntityId);
                        }
                    }
                },
                error: function (a, b, c) {
                    if (console && console.error && JSON && JSON.stringify) console.error("a: " + JSON.stringify(a) + "| b: " + b + "|c: " + c)
                }
            });

        }


        function addResult(e) {
            var selectedEntityId = $(e.currentTarget).attr("rel");

            // Check that the selected entity hasnt already been added
            if (base.$widget.find(".tag-selector-selected-results ul li a[rel='" + selectedEntityId + "']").length != 0) return false;


            var data = {};
            data[base.options.idArgumentName] = selectedEntityId;
            data = $.extend(data, base.options.additionalRequestData);

            // Tell the server to add the selected tag
            $.ajax({
                url: base.options.addSelectedUrl,
                data: data,
                success: function (resultsView) {
                    if (resultsView != "") {
                        // Remove no-results list item if it exists and add the new selection
                        base.$widget.find(".tag-selector-selected-results ul li.no-results").remove();
                        base.$listElement.find("li.no-results").remove();

                        if (base.options.singleMode && base.$widget.find(".tag-selector-selected-results ul li").length > 0) {
                            base.$widget.find(".tag-selector-selected-results ul").html(resultsView);
                            base.$listElement.find("li").last().prev().replaceWith(resultsView);
                        } else {
                            base.$widget.find(".tag-selector-selected-results ul").append(resultsView);
                            base.$listElement.find("li").last().before(resultsView);
                        }
                        if (base.options.resultAdded) {
                            base.options.resultAdded(selectedEntityId);
                        }
                    }
                },
                error: function (a, b, c) {
                    if (console && console.error && JSON && JSON.stringify) console.error("a: " + JSON.stringify(a) + "| b: " + b + "|c: " + c)
                }
            });

            return false;
        }

        function removeResult(e) {
            var $selectedResult = $(e.currentTarget);
            var selectedEntityId = $selectedResult.attr("rel");

            var data = {};
            data[base.options.idArgumentName] = selectedEntityId;
            data = $.extend(data, base.options.additionalRequestData);

            // Tell the server to remove the selected tag
            $.ajax({
                url: base.options.removeSelectedUrl,
                data: data,
                success: function () {
                    // Remove the item with selectedEntityId in it "rel" attribute
                    base.$widget.find(".tag-selector-selected-results ul li a[rel='" + selectedEntityId + "']").parent().remove();
                    base.$listElement.find("li a[rel='" + selectedEntityId + "']").parent().remove();
                    // Add a "No Results" list item if no items are now selected
                    if (base.$listElement.find("li").length <= 1) {
                        base.$widget.find(".tag-selector-selected-results ul").html(NO_RESULTS_HTML);
                        base.$listElement.find("li").last().before(NO_RESULTS_HTML);
                    }
                    if (base.options.resultRemoved) {
                        base.options.resultRemoved(selectedEntityId);
                    }

                },
                error: function (a, b, c) {
                    if (console && console.error && JSON && JSON.stringify) console.error("a: " + JSON.stringify(a) + "| b: " + b + "|c: " + c)
                }
            });

            return false;
        }

        function switchToBrowseMode() {
            base.$widget.find(".browse-tab").show();
            base.$widget.find(".search-tab").hide();
        }

        function switchToSearchMode() {
            base.$widget.find(".browse-tab").hide();
            base.$widget.find(".search-tab").show();
        }

        // Run initializer
        base.init();
    };

    $.tagSelector.defaultOptions = {
        autoShow: false,
        baseMediaUrl: "/mm/List/",
        loaded: function () { },
        resultAdded: function (entityId) { },
        resultRemoved: function (entityId) { },
        searchTimeout: 1000,
        singleMode: false,
        heading: "Tag Selector",
        searchUrl: "/TagSelector/SearchArticles",
        listSelectedUrl: "/Article/ListSelectedArticles",
        addSelectedUrl: "/Article/AddSelectedArticle",
        removeSelectedUrl: "/Article/RemoveSelectedArticle",
        idArgumentName: "Id",
        additionalRequestData: {}
    };

    $.fn.tagSelector = function (options, action) {
        return this.each(function () {
            (new $.tagSelector(this, options, action))

            // HAVE YOUR PLUGIN DO STUFF HERE

            if (action) {
                selector[action]();
            }

            // END DOING STUFF

        });
    };

    $.fn.getTagSelector = function () {
        return this.data("tagSelector");
    };

})(jQuery);