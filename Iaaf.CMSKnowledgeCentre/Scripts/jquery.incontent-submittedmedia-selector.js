﻿/*

Notes:
    
    - Based on the jQuery template from http://css-tricks.com/snippets/jquery/jquery-plugin-template/

Dependencies:

    - jqote2
    - Twitter Bootstrap framework (javascript and css)

*/

(function ($) {
    var data = {};
    var instanceReference = 0;

    $.inlineMediaSelector = function (trigger, options) {
        // To avoid scope issues, use 'base' instead of 'this'
        // to reference this class from internal events and functions.
        var base = this;

        // Access to jQuery and DOM versions of element
        base.$trigger = $(trigger);
        base.trigger = trigger;

        // Store the instance reference number
        base.instanceNumber = instanceReference;
        // Declare a variable to hold the current tab
       
        // Declare the timeout variable used for searching
        base.searchTimer;
        // Increment the instance reference
        instanceReference++;

        // Add a reverse reference to the DOM object
        base.$trigger.data("inlinemediaSelector", base);

        // Instance variables
        base.cachedResults = {};

        base.init = function () {

            base.options = $.extend({}, $.inlineMediaSelector.defaultOptions, options);
            // If media base url is missing then try and get it from the script registrars client variable namespace
            if (base.options.baseMediaUrl == "" && hncms.baseMediaUrl) base.options.baseMediaUrl = hncms.baseMediaUrl;
            // Set the jqote tag
            $.jqotetag("*");

            // Render the media selector markup
            base.renderInLineMediaSelector();
                       

            // Set up the media selector as a modal
            $("#inlinemediaSelector" + base.instanceNumber).modal({
                show: false
            });

            getResults();
        };

        // Sample Function, Uncomment to use
        // base.functionName = function(paramaters){
        // 
        // };
       
       
        base.renderInLineMediaSelector = function () {
            // Package the data required to render the container markup
            var viewModel = {
                instanceNumber: base.instanceNumber
            };
            base.$widget = $($("#inlinemediaSelectorTemplate").jqote(viewModel));
            // Get the current tab config
           
            // Bind layout events
            bindLayoutEvents();
            // Insert the widget into the document
            $(".inline-media-selector").append(base.$widget);

            console.log("Rendered the core MediaSelector markup.");
        };
        base.renderResults = function (viewModel) {
           
            // If the tab view already exists then remove it
            var tabViewContainer = base.$widget.find(".inlinemedia-selector-tab-views");
            
            // Render the tab view with the supplied model
            var $tabViewMarkup = $($.jqote("#inlinemediaSelectorMediaItemTemplate", viewModel));
            // Bind events to the new tab view           

            $(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });

            base.$widget.find(".inlinemedia-selector-tab-views").html($tabViewMarkup);

        };
       

        // Private functions

        // Bind events for the media selector markup including tabs
        function bindLayoutEvents() {
            
            //                $("#mediaSelector" + base.instanceNumber + " .media-selector-tab-container li a").live("click", base.tabClickHandler);
            //                $("a.add-media-item").live("click", base.mediaClickHandler);

           
            base.$widget.find(".inlinemedia-search").keyup(searchTextChanged);
        }
       
        function searchTextChanged(e) {
            var searchBox = $(e.currentTarget);
            if (searchBox.val() == "" || searchBox.val() == " ") {
                getResults();
            } else {
                clearTimeout(base.searchTimer);
                base.searchTimer = setTimeout(function () {
                    getResults();
                }, base.options.searchTimeout);
            }
            return false;
        }
        function getResults(success) {
            // Get the tab config

            var searchBox = base.$widget.find(".inlinemedia-search");
                       
            var queryUrl = "/SubmittedMediaSelector/SearchByTitle";

            // Build the query data for a search or list request to the server
            var queryData = {
                skip: 0
            };

            queryData.searchText = searchBox.val();
            
            if (searchBox.val()=="") {
                queryUrl = "/SubmittedMediaSelector/RecentMedia";
            }

            $.ajax({
                // url: base.options.tabSettings[base.options.defaultBrowseTab].queryUrl,
                url: queryUrl,//tabConfig.queryUrl,
                dataType: "JSON",
                data: queryData,
                success: function (data) {
                    
                    // Build the view model
                    var viewModel = $.extend({                       
                        baseMediaUrl: base.options.baseMediaUrl
                    }, data);

                    // Render the data
                    base.renderResults(viewModel);

                    if (success) return success();
                },
                error: function (a, b, c) {
                    if (console && console.error) console.error("a: " + a + "| b: " + b + "| c: " + c);
                    
                }
            });
        }
        

        // Run initializer
        base.init();
    };

    $.inlineMediaSelector.defaultOptions = {
        autoShow: true,
        baseMediaUrl: "",
        defaultGalleryThumbnail: "default_gallery_icon.jpg",
        loaded: function () { }
        
    };

    

    $.fn.inlineMediaSelector = function (options, action) {
        return this.each(function () {
            var selector = new $.inlineMediaSelector(this, options, action);

            // HAVE YOUR PLUGIN DO STUFF HERE

            if (action) {
                selector[action]();
            }

            // END DOING STUFF

        });
    };
    return false;
})(jQuery);