﻿





function functionPopup(header, htmlcode) {


    var popupwidth = $(".overlay-panel").width();
    var popupheight = $(".overlay-panel").innerHeight();

    var windowwidth = $(window).width();
    var windowheight = $(window).height();

    $(".overlay-panel").css({ 'top': (windowheight - popupheight) / 2, 'left': (windowwidth - popupwidth) / 2 });
    $(".close-overlay").css({ 'top': (windowheight - popupheight) / 2 - 70, 'right': (windowwidth - popupwidth) / 2 }).fadeIn(250);


    $(".overlay-panel-content .modal-header h3").html(header);
    $(".overlay-panel-content .modal-body").html(htmlcode);


    $(window).resize(function () {

        windowwidth = $(window).width();
        windowheight = $(window).height();

        $(".overlay-panel").css({ 'top': (windowheight - popupheight) / 2, 'left': (windowwidth - popupwidth) / 2 });
        $(".close-overlay").css({ 'top': (windowheight - popupheight) / 2 - 70, 'right': (windowwidth - popupwidth) / 2 }).fadeIn(250);

    });


    $(".overlay-bg").fadeIn(250);


    setTimeout(function () {

        $(".overlay-panel").fadeIn(250);
        $(".close-overlay").fadeIn(250);

    }, 250);


    $(".overlay-bg, .close-overlay, .closedelete").click(function () {

        var cookiename = "true";
        var closedId = "closed";

        $(".overlay-bg").hide()
        $(".overlay-panel").hide();

        $(".overlay-panel").removeClass("nobgcolour");

        $(".overlay-panel-content .modal-header h3").html("");
        $(".overlay-panel-content .modal-body").html("");

        //adding cookie
        cookie(cookiename, '|' + closedId + '|');

        return false;

    });

}




$(".problems-viewing a").click(function () {

    $(".overlay-panel, .overlay-panel-content").width(600);

    setTimeout(function () {
        functionPopup("Browsing in Internet Explorer", "<p>The IAAF CMS website uses new features that do not display correctly in older versions of Internet Explorer. For best use of this website, we recommend downloading and/or browsing in Google Chrome or Mozilla Firefox.</p>");
    }, 550);

});






$(".deletesure").click(function () {
    
    titleelement = $(this).attr("rel");    
    url = $(this).data('url');
    
    $(".overlay-panel, .overlay-panel-content").width(600);

    setTimeout(function () {
        functionPopup("Delete <em>" + titleelement + "</em>?", "<p>Are you sure you would like to Delete this item?<br />&nbsp;<br /><strong>Please note</strong>, once deleted, you will be unable to restore this item.<br />&nbsp;</p><a class='btn' href='" + url + "'>Delete</a><a class='btn closedelete' href='#'>Cancel</a>");
    }, 550);

    $(".close-overlay").remove();

});





$(document).ready(function () {


    if ($.browser.msie) {

        $("body").addClass("iebrowser");

    }

});


