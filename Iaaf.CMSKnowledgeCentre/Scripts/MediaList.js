﻿$(function () {
    // Constants
    var searchUrls = {
        media: "/Media/Search",
        byDiscipline: "/Media/SearchByDiscipline",
        byCompetitor: "/Media/SearchByCompetitor",
        byEvent: "/Media/SearchByEvent"
    };

    searchCountUrl = "/Media/GetTotalSearchResults";
    searchCancelUrl = "/Media/CancelSearch";

    var currentSearchType = "media";

    // Global variables
    var searchTimer;
    var resultCount = 0;


    // Event handlers
    bindEvents();
    initialiseViewState();

    function bindEvents() {
        $("#mediaSearchBox").keyup(searchBoxChanged);
        $(".search-cancel").click(handleSearchCancel);
        $("#searchMoreMedia").click(moreResults);
        $("#searchMedia").click(switchSearchView);
        $("#searchDiscipline").click(switchSearchView);
        $("#searchCompetitor").click(switchSearchView);
        $("#searchEvent").click(switchSearchView);
        $("ul.media-list").on("mouseover", "li.media-item img", showMediaItemDetails);
        $("ul.media-list").on("mouseout", "li.media-item img", hideMediaItemDetails);
    }

    function initialiseViewState() {
        if (hncms.showSearchResults) {
            $("#searchTermsLabel").html($("#mediaSearchBox").val());
            resultCount = $("#searchedMediaList > li").length;
            showSearch(true)
        } else {
            showSearch(false);
        }
    }

    // Event handlers
    function searchBoxChanged(e) {
        var searchBox = $(e.currentTarget);
        // Reset the result counter back to zero
        resultCount = 0;

        if (searchBox.val() == "" || searchBox.val() == " ") {
            mediaSearchCancel();
            $("#searchedMediaList").html("");
            $(".search-cancel").addClass("disabled");
            showSearch(false);
        } else {
            $("#searchTermsLabel").html(searchBox.val());
            $(".search-cancel").removeClass("disabled");
            clearTimeout(searchTimer);
            var searchTerms = searchBox.val();
            searchTimer = setTimeout(function () { makeSearch(searchTerms, true) }, 500);
            showSearch(true);
        }
        return false;
    }

    function handleSearchCancel(e) {
        mediaSearchCancel();
        $("#mediaSearchBox").val("");
        showSearch(false);
    }

    function moreResults(e) {
        makeSearch($("#mediaSearchBox").val(), false);
        return false;
    }

    function switchSearchView(e) {
        switch ($(e.currentTarget).attr("id")) {
            case "searchMedia":
                currentSearchType = "media";
                break;
            case "searchDisciplines":
                currentSearchType = "byDiscipline";
                break;
            case "searchCompetitor":
                currentSearchType = "byCompetitor";
                break;
            case "searchEvent":
                currentSearchType = "byEvent";
                break;
        }
        makeSearch($("#mediaSearchBox").val());
    }

    function showMediaItemDetails(e) {
        var $mediaImage = $(e.currentTarget);
        var $mediaDetails = $mediaImage.parent().find(".media-details");
        var $mediaDetails
        //        $mediaDetails.css("top", $mediaImage.offset().top + ($mediaImage.height() / 2));
        //        $mediaDetails.css("left", $mediaImage.offset().left + ($mediaImage.width() / 2));
        $mediaDetails.css("top", $mediaImage.offset().top + 20);
        $mediaDetails.css("left", $mediaImage.offset().left + 20);
        $mediaDetails.show();
    }

    function hideMediaItemDetails(e) {
        $(e.currentTarget).parent().find(".media-details").fadeOut();
    }

    // Private methods
    function makeSearch(searchText, clearPreviousResults) {

        $("#searchMoreMedia").addClass("disabled");

        var searchUrl = searchUrls[currentSearchType];
        $.ajax({
            url: searchUrl,
            data: {
                searchText: searchText,
                skip: resultCount,
                searchType: "ByTitle" // See enum MediaSearchType for other possible values
            },
            success: function (partialView) {

                // Check if there are any more results available
                getTotalResultCount(searchText, {
                    success: function (totalResultCount) {
                        if (resultCount >= totalResultCount) {
                            $("#searchMoreMedia").addClass("disabled");
                        } else {
                            $("#searchMoreMedia").removeClass("disabled");
                        }
                    }
                });

                var results = $(partialView);
                resultCount += results.siblings("li").length;
                if (clearPreviousResults) {
                    $("#searchedMediaList").html(results);
                } else {
                    $("#searchedMediaList").append(results);
                }
            },
            error: function (a, b, c) {
                if (console && console.error) console.error("a: " + a + "| b: " + b + "| c: " + c);
            }
        });
    }

    function showSearch(show) {
        if (show) {
            $(".search-cancel").removeClass("disabled");
            $("#mediaListContainer").hide();
            $("#mediaSearchResultsContainer").show();
        } else {
            $(".search-cancel").addClass("disabled");
            $("#mediaListContainer").show();
            $("#mediaSearchResultsContainer").hide();
        }
    }

    function mediaSearchCancel() {
        $.ajax({
            url: searchCancelUrl,
            type: "POST",
            success: function () {

            },
            error: function (a, b, c) {
                if (console && console.error) console.error("a: " + a + "| b: " + b + "| c: " + c);
            }
        });
    }

    function getTotalResultCount(searchText, options) {
        $.ajax({
            url: searchCountUrl,
            type: "POST",
            data: {
                searchText: searchText,
                searchType: "ByTitle"
            },
            success: function (totalResultCount) {
                if (options && options.success) options.success(totalResultCount);
            },
            error: function (a, b, c) {
                if (console && console.error) console.error("a: " + a + "| b: " + b + "| c: " + c);
            }
        });
    }

});