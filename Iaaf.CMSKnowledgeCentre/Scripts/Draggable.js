﻿
var sitemapHistory = {
    stack: new Array(),
    temp: null,
    //takes an element and saves it's position in the sitemap.
    //note: doesn't commit the save until commit() is called!
    //this is because we might decide to cancel the move
    saveState: function (item) {
        sitemapHistory.temp = { item: $(item), itemParent: $(item).parent(), itemAfter: $(item).prev() };
    },
    commit: function (parentid, childid) {
        if (sitemapHistory.temp != null) sitemapHistory.stack.push(sitemapHistory.temp);
        alert(sitemapHistory.temp.itemAfter.attr("tagname"));
        alert(sitemapHistory.temp.itemParent.attr("tagname"));
        alert(sitemapHistory.temp.item.attr("tagname"));
        var data = {};
        data["beforeid"] = parentid;
        data["childid"] = childid

        $.ajax({
            url: "/tags/set",
            data: data,
            success: function () {
                alert('done');
            },
            error: function (a, b, c) {
                if (console && console.error && JSON && JSON.stringify) console.error("a: " + JSON.stringify(a) + "| b: " + b + "|c: " + c)
            }
        });
    },
    //restores the state of the last moved item.
    restoreState: function () {
        var h = sitemapHistory.stack.pop();
        if (h == null) return;
        if (h.itemAfter.length > 0) {
            h.itemAfter.after(h.item);
        }
        else {
            h.itemParent.prepend(h.item);
        }
        //checks the classes on the lists
        $('#sitemap li.sm2_liOpen').not(':has(li)').removeClass('sm2_liOpen');
        $('#sitemap li:has(ul li):not(.sm2_liClosed)').addClass('sm2_liOpen');
    }
}

//init functions
$(function () {
    $('#sitemap li').prepend('<div class="dropzone"></div>');

    $('#sitemap dl, #sitemap .dropzone').droppable({
        accept: '#sitemap li',
        tolerance: 'pointer',
        drop: function (e, ui) {
            var li = $(this).parent();
            var child = !$(this).hasClass('dropzone');
            if (child && li.children('ul').length == 0) {
                li.append('<ul/>');
            }
            if (child) {
                li.addClass('sm2_liOpen').removeClass('sm2_liClosed').children('ul').append(ui.draggable);
            }
            else {
                li.before(ui.draggable);
            }
            $('#sitemap li.sm2_liOpen').not(':has(li:not(.ui-draggable-dragging))').removeClass('sm2_liOpen');
            li.find('dl,.dropzone').css({ backgroundColor: '', borderColor: '' });

            sitemapHistory.commit(li.attr("tagid"), $(ui.draggable).attr("tagid"));
         //   alert($(this).attr("tagid") + " " + $(this).attr("tagname"));
        },
        over: function () {
            $(this).filter('dl').css({ backgroundColor: '#ccc' });
            $(this).filter('.dropzone').css({ borderColor: '#aaa' });
        },
        out: function () {
            $(this).filter('dl').css({ backgroundColor: '' });
            $(this).filter('.dropzone').css({ borderColor: '' });
        }
    });
    $('#sitemap li').draggable({
        handle: ' > dl',
        opacity: .8,
        addClasses: false,
        helper: 'clone',
        zIndex: 100,
        start: function (e, ui) {
            sitemapHistory.saveState(this);
        }
    });
    $('.sitemap_undo').click(sitemapHistory.restoreState);
    $(document).bind('keypress', function (e) {
        if (e.ctrlKey && (e.which == 122 || e.which == 26))
            sitemapHistory.restoreState();
    });
    $('.sm2_expander').on('click', null, function () {
        $(this).parent().parent().toggleClass('sm2_liOpen').toggleClass('sm2_liClosed');
        return false;
    });
});
