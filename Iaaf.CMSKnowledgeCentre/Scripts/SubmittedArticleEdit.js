﻿$(function () {
    var additionalRequestData = {};
    if (hncms.articleId) additionalRequestData["articleId"] = hncms.articleId;

  

    $("#primaryMedia").on("click", ".media_remove", function (e) {
        var data = {};
        if (hncms.articleId) data["articleId"] = hncms.articleId;

        $.ajax({
            url: "/submissions/RemovePrimaryMedia",
            data: data,
            success: function () {
                $("#primaryMedia > li").first().remove();
            },
            error: function (a, b, c) {
                if (console && console.error && JSON && JSON.stringify) console.error("a: " + JSON.stringify(a) + "| b: " + b + "|c: " + c)
            }
        });
        return false;
    });

    $("#addPrimaryMedia").mediaSelector({
        mediaSelected: function (mediaId) {
            // Send to the server
            addPrimaryMedia(mediaId);
        }
    });

    $("#quickUploadPrimaryMedia").mediaQuickUpload({
        complete: function (mediaId) {
            addPrimaryMedia(mediaId);
        }
    });

    function addPrimaryMedia(mediaId) {
        var data = { mediaId: mediaId };
        if (hncms.articleId) data["articleId"] = hncms.articleId;

        $.ajax({
            url: "/submissions/AddPrimaryMedia",
            data: data,
            success: function (viewUpdate) {
                // Make sure the .no-results element is removed
                $("#primaryMedia").find(".no-results").remove();
                // Add the ExistingArticleMediaItem partial view to the update element
                if ($("#primaryMedia > li").length == 2) {
                    $("#primaryMedia").prepend(viewUpdate);
                } else {
                    $("#primaryMedia > li").first().replaceWith(viewUpdate);
                }
            },
            error: function (a, b, c) {
                if (console && console.error && JSON && JSON.stringify) console.error("a: " + JSON.stringify(a) + "| b: " + b + "|c: " + c)
            }
        });
    }



    $("#secondaryMedia").on("click", ".media_remove", function (e) {
        var mediaId = $(e.currentTarget).attr("rel");
        var data = { mediaId: mediaId };
        if (hncms.articleId) data["articleId"] = hncms.articleId;

        $.ajax({
            url: "/submissions/RemoveRelatedMedia",
            data: data,
            success: function () {
                $(e.currentTarget).parent().remove();
            },
            error: function (a, b, c) {
                if (console && console.error) console.error("a: " + a + "| b: " + b + "|c: " + c)
            }
        });
        return false;
    });

    $("#addSecondaryMedia").mediaSelector({
        mediaSelected: function (mediaId) {
            // Send to the server
            addSecondaryMedia(mediaId);
        }
    });

    $("#quickUploadSecondaryMedia").mediaQuickUpload({
        complete: function (mediaId) {
            addSecondaryMedia(mediaId);
        }
    });

    function addSecondaryMedia(mediaId) {
        var data = { mediaId: mediaId };
        if (hncms.articleId) data["articleId"] = hncms.articleId;

        $.ajax({
            url: "/submissions/AddRelatedMedia",
            data: data,
            success: function (viewUpdate) {
                // Make sure the .no-results element is removed
                $("#secondaryMedia").find(".no-results").remove();
                // Add the ExistingArticleMediaItem partial view to the update element
                $("#secondaryMedia").prepend(viewUpdate);
            },
            error: function (a, b, c) {
                if (console && console.error && JSON && JSON.stringify) console.error("a: " + JSON.stringify(a) + "| b: " + b + "|c: " + c)
            }
        });
    }


   

    /********************In line content *************************/


    $("#inline-media-selector").inlineMediaSelector({});




     $("#relatedTags").tagSelector({
        heading: "Search for related tags",
        searchUrl: "/TagSelector/SearchTags",
        listSelectedUrl: "/submissions/ListRelatedTags",
        addSelectedUrl: "/submissions/SaveRelatedTags",
        removeSelectedUrl: "/submissions/RemoveRelatedTags",
        idArgumentName: "tagId",
        additionalRequestData: additionalRequestData,
        resultAdded: function () {
            $("#relatedTags").getTagSelector().reload();
        }
    });    

     $("#relatedEvents").tagSelector({
         heading: "Search for related events",
         searchUrl: "/TagSelector/SearchEvents",
         listSelectedUrl: "/submissions/ListRelatedEvents",
         addSelectedUrl: "/submissions/SaveRelatedEvent",
         removeSelectedUrl: "/submissions/RemoveRelatedEvent",
         idArgumentName: "eventId",
         additionalRequestData: additionalRequestData,
         resultAdded: function () {
             $("#relatedEvents").getTagSelector().reload();
         }
     });
    

  
     $("#relatedArticles").tagSelector({
         heading: "Search for related articles",
         searchUrl: "/TagSelector/SearchArticles",
         listSelectedUrl: "/submissions/ListRelatedArticles",
         addSelectedUrl: "/submissions/AddRelatedArticle",
         removeSelectedUrl: "/submissions/RemoveRelatedArticle",
         idArgumentName: "relatedArticleId",
         additionalRequestData: additionalRequestData
     });
   
   
});

