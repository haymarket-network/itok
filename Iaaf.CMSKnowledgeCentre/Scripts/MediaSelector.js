﻿$(function () {
    // Constants
    var tabSettings = {
        "RecentMedia": {
            tabLabelSelector: "mediaSelectorTabRecentMedia",
            tabViewSelector: "mediaSelectorTabViewRecentMedia",
            search: false,
            switchTab: "AnyMedia"
        },
        "GalleryList": {
            tabLabelSelector: "mediaSelectorTabGalleryList",
            tabViewSelector: "mediaSelectorTabViewGalleryList",
            search: false,
            switchTab: "Gallery"
        },
        "AnyMedia": {
            tabLabelSelector: "mediaSelectorTabAnyMedia",
            tabViewSelector: "mediaSelectorTabViewAnyMedia",
            searchUrl: "/MediaSelector/SearchByTitle",
            search: true
        },
      
    }
    // Variables
    var currentTabSettings = tabSettings["RecentMedia"];
    var searchTimer;

    // Initialisation
    bindEvents();
    initialiseTabLabels();
    initialiseTabViews();


    function bindEvents() {
        $("#mediaSelectorTabViewsContainer .media_item img").mouseover(showDetails);
        $("#mediaSelectorTabViewsContainer .media_item img").mouseout(hideDetails);
        $(".add_media_item").live("click", addMedia);
        $("#mediaSelectorTabContainer > li > a").click(switchTab);
        $("#mediaSearch").keyup(searchBoxChanged);
    }

    function initialiseTabLabels() {
        $("#mediaSelectorTabContainer .search_tab").parent().hide();
    }

    function initialiseTabViews() {
        $("#mediaSelectorTabRecentMedia").click();
    }

    // Handlers

    function showDetails(e) {
        var targetMediaItem = $(e.currentTarget);
        var details = targetMediaItem.siblings(".media_item_detail");
        if (details.html() != "" && details.html() != " ") {
            details.fadeIn();
        }
    }

    function hideDetails(e) {
        $(e.currentTarget)
            .siblings(".media_item_detail")
            .hide();
    }

    function addMedia(e) {
        // Get the current target
        var targetAnchor = $(e.currentTarget);
        // Extract media id and profile information from the url
        var urlSegments = targetAnchor.attr("href").split("/");
        var mediaId = urlSegments[5];
        // Send to the server
        $.ajax({
            url: addMediaUrl,
            data: { mediaId: mediaId },
            success: function (viewUpdate) {
                // Make sure the .empty_list_text element is removed
                $(updateElementSelector).find(".empty_list_text").remove();
                // Add the ExistingArticleMediaItem partial view to the update element
                $(updateElementSelector).append(viewUpdate);
            },
            error: function (xhr, status, msg) {
                alert(status + " - " + msg);
            }
        });
        return false;
    }

    function switchTab(e) {
        // Hide everything
        $("#mediaSelectorTabViewsContainer > li").hide();
        // Get search config
        currentTabSettings = tabSettings[$(e.currentTarget).attr("rel")];
        // Show the selected tab view
        $("#" + currentTabSettings.tabViewSelector).show();
        // Search
        if (currentTabSettings.search) { makeSearch($("#mediaSearch").val()); }

        return false;
    }

    function searchBoxChanged(e) {
        var searchText = $(e.currentTarget).val();
        // If the textbox is empty then 
        if (searchText == "" || searchText == " ") {
            //- Switch back to the recent media tab view
            $("#mediaSelectorTabRecentMedia").click();
            //- Switch to browse tabs
            showBrowseTabs();
        } else { // If the textbox is not empty then 
            //- If the current tab doesnt support searching then 
            if (!currentTabSettings.search) {
                //-- Switch to one that does
                currentTabSettings = tabSettings[currentTabSettings.switchTab];

            }
            //-- Switch to search tabs
            if ($("#mediaSelectorTabContainer .search_tab").is(":hidden")) { showSearchTabs(); }
            //- Set search timer
            clearTimeout(searchTimer);
            searchTimer = setTimeout(function () { makeSearch(searchText) }, 1000);
        }
        return false;
    }

    // Helper methods

    function makeSearch(searchText) {
        $.ajax({
            url: currentTabSettings.searchUrl,
            data: {
                searchText: searchText
            },
            success: function (resultsView) {
                $("#" + currentTabSettings.tabViewSelector).children("ul").html(resultsView);

            },
            error: function (xhr, status, msg) {
                alert(msg);
            }
        });
    }

    function showSearchTabs() {
        $("#mediaSelectorTabContainer .search_tab").parent().show();
        $("#mediaSelectorTabContainer .browse_tab").parent().hide();
    }

    function showBrowseTabs() {
        $("#mediaSelectorTabContainer .search_tab").parent().hide();
        $("#mediaSelectorTabContainer .browse_tab").parent().show();
    }

})