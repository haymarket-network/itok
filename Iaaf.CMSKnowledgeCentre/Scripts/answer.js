﻿
var jsGlobalEventId = "";
var jsGlobalYearId = "";
var jsGlobalAreaId = "";

$(document).ready(function () {
    flagClick();
    quickUploadMedia();
});

function initTinyMCE() {
    tinymce.remove();
    tinymce.init({
        selector: "textarea#answer",
        content_css: "/Content/Styles/EditorStylesOverride.css",
        plugins: [
            "advlist autolink lists link image base64_image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste ",
            "template"
        ],
        theme_advanced_blockformats: "p,h1,h2,h3,h4,h5,h6,blockquote,dt,dd,code,samp",
        menubar: false,
        toolbar: "insertfile undo redo | styleselect | template | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link anchor base64_image media | table | code",

        height: 300,
        paste_data_images: true,
        force_p_newlines: true,
        //allow_script_urls: true,
        extended_valid_elements: 'script[language|type|src|async|defer]'
    });
}

function addEditComment(qid, questionText) {

    $('#modalWrapper').html("");
    $.ajax({
        url: "/Home/GetQuestionComments", //
        data: { questionId: qid, questionText: questionText, eventId: jsGlobalEventId, yearId: jsGlobalYearId, areaId: jsGlobalAreaId },
        success: function (data) {
            $('#modalWrapper').html(data);
        }
    });
}

function addEditCommentForItokLeader(ansId, questionText) {

    $('#modalWrapper').html("");
    $.ajax({
        url: "/Home/GetQuestionCommentsByAnswerId", //
        data: { answerId: ansId, questionText: questionText },
        success: function (data) {
            $('#modalWrapper').html(data);
        }
    });
}

function editQuestion(qid, isAttachment, questionText) {
    window.alreadyAnswered = false;
    if ($('#' + qid + 'actionIcon').attr('title').indexOf('Answer complete') == 0) {
        window.alreadyAnswered = true;
    }
    $('#modalWrapper').html("");
    if (!isAttachment) {
        $("#lastClickedQuestionId").val("");
        $.ajax({
            url: "/Home/GetQuestionDetail", //
            data: { questionId: qid, eventId: jsGlobalEventId, yearId: jsGlobalYearId, areaId: jsGlobalAreaId },
            success: function (data) {
                $('#modalWrapper').html(data);

                initTinyMCE();
            }
        });
    } else {
        $("#lastClickedQuestionId").val(qid);
        $(".informationText").show();
    }
}


function editAttachment(qid, isAttachment) {
    $('#modalWrapper').html("");

    $("#lastClickedQuestionId").val("");
    $.ajax({
        url: "/Home/GetAttachments", //
        data: { questionId: qid, eventId: jsGlobalEventId, yearId: jsGlobalYearId, areaId: jsGlobalAreaId },
        success: function (data) {
            $('#modalWrapper').html(data);
        }
    });
}

let isRemoved = false;
function removeAttachment(qid, attachmentId) {
    $.ajax({
        url: "/Home/RemoveAttachment", //
        data: { questionId: qid, eventId: jsGlobalEventId, yearId: jsGlobalYearId, areaId: jsGlobalAreaId, attachmentId: attachmentId },
        success: function (data) {
            $("#" + attachmentId + "").hide('slow', function () {
                $("#" + attachmentId + "").remove();
            });
            isRemoved = true;
        },
        error: function (a, b, c) {
            if (console && console.error && JSON && JSON.stringify)
                console.error("a: " + JSON.stringify(a) + "| b: " + b + "|c: " + c);
        }
    });
}

$('#modalWrapper').on('hidden.bs.modal',
    function () {
        if (isRemoved) {
            console.log('model closed for itok leader');
            window.location = window.location.href;
        }
    });

function quickUploadMedia() {

    $(".fileUploadButton").mediaQuickUpload({
        complete: function (mediaId) {

            addDocument(mediaId);

        },
        editMediaRatioUrl: "/MediaBulk/EditMediaRatio",
        disableMediaRatioUrl: "/MediaBulk/DisableMediaRatio",
        saveMediaRatioUrl: "/MediaBulk/SaveMediaRatio",
        resizeMediaRatioUrl: "/MediaBulk/SaveMediaResize",
        createMediaUrl: "/MediaBulk/CreateJson",
        saveMediaUrl: "/MediaBulk/SaveJson",
        cancelMediaUrl: "/MediaBulk/CancelJson",
        uploadSteps: [1, 3, 4],
        stageLabels: ["Stage 1: Upload", "Stage 2: Basic Details", "Stage 3: Save"],
        mediaSelectorMessage: ""


    });
}
function addDocument(mediaId) {
    var questionId = $("#lastClickedQuestionId").val();

    $.ajax({
        type: "GET",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: "/Home/SaveAnswerAttachment/?questionId=" + questionId + "&attachmentId=" + mediaId + "&eventId=" + jsGlobalEventId + "&yearId=" + jsGlobalYearId + "&areaId=" + jsGlobalAreaId, //
        success: function (result) {
            if (window.location.href.toLowerCase().indexOf('leaderindex') > 0) {
                $('#' + questionId + jsGlobalEventId + jsGlobalYearId + jsGlobalAreaId).siblings('span').text(result.attachmentCount);

                if ($('#' + questionId + jsGlobalEventId + jsGlobalYearId + jsGlobalAreaId).parent().siblings().hasClass("fa-times")) {
                    $('#' + questionId + jsGlobalEventId + jsGlobalYearId + jsGlobalAreaId).parent().siblings(":last").remove();
                    $('#' + questionId + jsGlobalEventId + jsGlobalYearId + jsGlobalAreaId).parent().parent().append("<i class='fa fa-2x fa-check actionIcon' style='color:forestgreen!important'></i>");
                }
            }
            else {

                location.reload();
            }

        }
    });

}

function setFlagColor(selectedId) {

    const answerFlagId = $("#" + selectedId.id).parent().siblings(".answer-flag").val();
    const answerId = $("#" + selectedId.id).parent().siblings(".answer-id").val();
    const flagColor = selectedId.id;
    var jsonData = { Id: answerFlagId, AnswerId: answerId, FlagColor: flagColor };

    var jsonString = JSON.stringify(jsonData);
    var selectedFlagId = $('#hdnFlagId').val();
    $.ajax({
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: "/Home/SetFlagColor",
        data: jsonString,
        success: function (result) {
            if (result) {
                $('.pickcolorDD').html('');
                switch (flagColor) {
                    case "Red":
                        $('#' + selectedFlagId).removeClass("successStatus").removeClass("mediumStatus").addClass("dangerStatus");
                        break;
                    case "Green":
                        $('#' + selectedFlagId).addClass("successStatus").removeClass("mediumStatus").removeClass("dangerStatus");
                        break;
                    case "Amber":
                        $('#' + selectedFlagId).removeClass("successStatus").removeClass("dangerStatus").addClass("mediumStatus");

                        break;
                    default:
                }
            }
            else {
                alert('Something went wrong, please try again');
            }
        }
    });
    return true;
}

$(window).click(function () {
    $('.pickcolorDD').html('');
});

function flagClick() {
    $(".fa-flag").click(function (event) {
        $('.pickcolorDD').html('');

        var selectedFlagId = $(this).attr('id');
        $('#hdnFlagId').val(selectedFlagId);
        var options = '<li class="li-color-picker" id="Red" onclick="setFlagColor(Red)" value="Red" style="background-color: Red;"></option>';
        options += '<li class="li-color-picker" id="Amber" onclick="setFlagColor(Amber)" value="Amber" style="background-color: #FF9900;"></option>';
        options += '<li class="li-color-picker" onclick="setFlagColor(Green)" id="Green" value="Green" style="background-color: Green;"></option>';

        $('.pickcolor-' + selectedFlagId).append(options).show();
        event.stopPropagation();
    });
}

