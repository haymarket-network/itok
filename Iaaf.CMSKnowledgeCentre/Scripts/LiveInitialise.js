﻿var eventTimeOffset = null;
$(function () {
    var constants = {
        event: {
            SEARCH_URL: "/livefeed/SearchEvents",
            INITIAL_IMPORT_URL: "/livefeed/InitialiseEvent"
        },
        session: {
            UPDATE_URL: "/livefeed/UpdateSession"
        }
    };
    // Variables
    var searchedEventsCache = {},
        selectedEvent,
        initialImportCache;

    var handlers = {
        searchEvents: function (e) {
            if (e.which != 13) return; // Only search on enter
            var searchTerms = $(e.currentTarget).val();
            console.log("Searching events containing: '" + searchTerms + "'");
            $.ajax({
                url: constants.event.SEARCH_URL,
                type: "POST",
                data: { searchTerms: searchTerms },
                success: handlers.searchEventsSuccess,
                error: handlers.genericAjaxError
            });
        },
        searchEventsSuccess: function (results) {
            console.log("Found " + results.length + " results");
            // Cache results
            searchedEventsCache = {};
            for (var e in results) {
                var event = results[e];
                searchedEventsCache[event.Id] = event;
            }
            // Update view
            view.displayEventSearchResults(results);
        },
        genericAjaxError: function(a, b, c) {
            console.log("ERROR - a: " + a + " | b: " + b + " | c: " + c);
        },
        eventSelected: function (e) {
            $("#eventSearchResults li").removeClass("selected");
            $(e.currentTarget).parent().addClass("selected");
            var eventId = $(e.currentTarget).attr("rel");
            selectedEvent = searchedEventsCache[eventId];
            console.log("Selected " + selectedEvent.Name);

            //$.ajax({
            //    url: constants.event.INITIAL_IMPORT_URL,
            //    type: "POST",
            //    data: { eventId: eventId },
            //    success: handlers.eventInitialImportSuccess,
            //    error: handlers.genericAjaxError
            //});

            //handlers.switchStep(2);
            return false;
        },
        eventInitialImportSuccess: function(response){
            if(!response.Success) {
                console.error(response.Message);
                return;
            }
            console.log("Imported sessions and phases from event");
            for (var f in response.Feeds) {
                response.Feeds[f] = utilities.convertFeedDates(response.Feeds[f]);
            }
            initialImportCache = response.Feeds;
            view.displayFeeds(response.Feeds);
        },
        switchStep: function (e) {
            var switchTo = $(e.currentTarget).attr("rel");
            console.log("Switching to step " + switchTo);
            switch (switchTo) {
                case "1":

                    break;
                case "2":
                    if (!selectedEvent) {
                        console.log("An event must be selected before continuing");
                        return false;
                    }
                    $.ajax({
                        url: constants.event.INITIAL_IMPORT_URL,
                        type: "POST",
                        data: {
                            eventId: selectedEvent.Id,
                            eventTimeOffsetString: $("#eventTimeOffset").val(),
                            eventTimeOffsetSubtract: $('input[name=eventTimeOffsetSign]:checked').val() != "Subtract",
                            startFromDateString: $("input[name=startFromDateString]").val(),
                            eventLocation: $("input[name=eventLocation]").val(),
                        },
                        success: handlers.eventInitialImportSuccess,
                        error: handlers.genericAjaxError
                    });
                    break;
                case "3":

                    break;
            }
            view.switchStep(switchTo);
            return false;
        },
        editSession: function (e) {
            var sessionId = $(e.currentTarget).attr("rel");
            console.log("Editing session " + sessionId);
            view.editSession(sessionId);
            return false;
        },
        saveSession: function (e) {
            var $sessionListItem = $(e.currentTarget).parent();
            var sessionId = $(e.currentTarget).attr("rel");
            console.log("Saving session " + sessionId);
            $.ajax({
                url: constants.session.UPDATE_URL,
                type: "POST",
                data: {
                    startTimeString: $sessionListItem.find(".session-start-time").val(),
                    endTimeString: $sessionListItem.find(".session-end-time").val(),
                    name: $sessionListItem.find(".session-name").val(),
                    sessionId: sessionId
                },
                success: function (response) { handlers.sessionSaveSuccess(response, sessionId) },
                error: handlers.genericAjaxError
            });
            return false;
        },
        sessionSaveSuccess: function (response, sessionId) {
            
            if (!response.Success) {
                console.error(response.Message);
                $sessionListItem.find(".session-edit-save-message").html(response.Message);
                return;
            }

            response.UpdatedFeed = utilities.convertFeedDates(response.UpdatedFeed);

            // Update the view
            view.updateSessionListItem(response.UpdatedFeed);

            // Close the edit view
            view.exitSessionEdit(sessionId);

            console.log("Session saved");
        },
        cancelSessionEdit: function (e) {
            var sessionId = $(e.currentTarget).attr("rel");
            console.log("Canceled edit of session " + sessionId);
            view.exitSessionEdit(sessionId);
            return false;
        }
    };

    var utilities = {
        convertFeedDates: function (feed) {
            // Convert crappy mvc dates to actual js dates
            if (typeof feed.Session.Start == "string")
                feed.Session.Start = feed.Session.Start.length > 6 ? moment.utc(parseInt(feed.Session.Start.substr(6))) : moment();
            if (typeof feed.Session.End == "string")
                feed.Session.End = feed.Session.End.length > 6 ? moment.utc(parseInt(feed.Session.End.substr(6))) : moment();

            for (var i in feed.Phases) 
                if (typeof feed.Phases[i].Start == "string")
                    feed.Phases[i].Start = feed.Phases[i].Start.length > 6 ? moment.utc(parseInt(feed.Phases[i].Start.substr(6))) : moment();

            return feed;
        },
        convertSessionDates: function (session) {
            // Convert crappy mvc dates to actual js dates
            if (typeof session.Start == "string")
                session.Start = session.Start.length > 6 ? moment(parseInt(session.Start.substr(6))) : moment();
            if (typeof session.End == "string")
                session.End = session.End.length > 6 ? moment(parseInt(session.End.substr(6))) : moment();
        }
    };

    var view = {
        displayEventSearchResults: function(events) {
            $resultsList = $("#eventSearchResults");
            // Clear previous results
            $resultsList.html("");
            if (events.length > 0)
            {
                for (var e in events) {
                    var event = events[e];
                    $resultsList.append("<li><a href='#' rel='" + event.Id + "'>" + event.Name + "</a></li>")
                }
            } else {
                $resultsList.append("<li>No results</li>")
            }

        },
        switchStep: function(switchTo) {
            $(".step").hide();
            $(".step-" + switchTo).show();
        },
        displayFeeds: function(feeds) {
            $("#sessionList").html($("#sessionListTemplate").jqote(feeds));

            //$sessionListItem = $("<li class='session-" + feed.Session.Id + "'></li>");
            //$sessionListItem.append("<span class='session-name'>" + feed.Session.Name + "</span><ul class='phase-list'></ul>");
            //$sessionListItem.append("");
            //$("#sessionList").append($sessionListItem);
            //for(var p in feed.Phases)
            //{
            //    view.displayPhase(feed.Phases[p], $sessionListItem);
            //}
        },
        displayPhase: function(phase, $sessionListItem) {
            //$phaseList = $sessionListItem.find("ul.phase-list");
            //$phaseList.append("<li>" + phase.Name + "</li>");
        },
        editSession: function (sessionId) {
            $sessionListItem = $(".session-" + sessionId);
            $sessionListItem.find(".session-view").hide();
            $sessionListItem.find(".session-edit").show();
        },
        exitSessionEdit: function (sessionId) {
            $sessionListItem.find(".session-view").show();
            $sessionListItem.find(".session-edit").hide();
        },
        updateSessionListItem: function (feed) {
            var $sessionListItem = $(".session-" + feed.Session.Id);
            $sessionListItem.replaceWith($("#sessionListTemplate").jqote(feed));
        }
    };

    function initialise() {
        // Event handlers
        $("#eventSearch").keydown(handlers.searchEvents);
        $("#eventSearchResults").on("click", "li a", handlers.eventSelected);
        $("#sessionList")
            .on("click", "a.session-edit-start", handlers.editSession)
            .on("click", "a.session-edit-save", handlers.saveSession)
            .on("click", "a.session-edit-cancel", handlers.cancelSessionEdit);
        $(".change-step").click(handlers.switchStep);
        // Initialise steps
        view.switchStep(1);
    }

    initialise();
});