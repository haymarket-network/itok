﻿$(function () {

    $("#relatedArticles").tagSelector({
        heading: "Search for related articles",
        searchUrl: "/TagSelector/SearchArticles",
        listSelectedUrl: "/Media/ListRelatedArticles",
        addSelectedUrl: "/Media/AddRelatedArticle",
        removeSelectedUrl: "/Media/RemoveRelatedArticle"
    });

    //$("#relatedCompetitors").tagSelector({
    //    heading: "Search for related competitors",
    //    searchUrl: "/TagSelector/SearchCompetitors",
    //    listSelectedUrl: "/Media/ListRelatedCompetitors",
    //    addSelectedUrl: "/Media/AddRelatedCompetitor",
    //    removeSelectedUrl: "/Media/RemoveRelatedCompetitor"
    //});

    //$("#relatedCompetitions").tagSelector({
    //    heading: "Search for related competitions",
    //    searchUrl: "/TagSelector/SearchCompetitions",
    //    listSelectedUrl: "/Media/ListRelatedCompetitions",
    //    addSelectedUrl: "/Media/AddRelatedCompetition",
    //    removeSelectedUrl: "/Media/RemoveRelatedCompetition"
    //});

    //$("#relatedDisciplines").tagSelector({
    //    heading: "Search for related disciplines",
    //    searchUrl: "/TagSelector/SearchDisciplines",
    //    listSelectedUrl: "/Media/ListRelatedDisciplines",
    //    addSelectedUrl: "/Media/AddRelatedDiscipline",
    //    removeSelectedUrl: "/Media/RemoveRelatedDiscipline"
    //});

    //$("#relatedEvents").tagSelector({
    //    heading: "Search for related events",
    //    searchUrl: "/TagSelector/SearchEvents",
    //    listSelectedUrl: "/Media/ListRelatedEvents",
    //    addSelectedUrl: "/Media/AddRelatedEvent",
    //    removeSelectedUrl: "/Media/RemoveRelatedEvent",
    //    resultAdded: function () {
    //        $("#relatedCompetitions").getTagSelector().reload();
    //    }
    //});

    //$("#relatedLinks").linkEditor({
    //    listLinksUrl: "/Media/ListRelatedLinks",
    //    saveLinkUrl: "/Media/SaveRelatedLink",
    //    removeLinkUrl: "/Media/RemoveRelatedLink"
    //});

});

$(function () {

    // Constants
    var ServiceUrls = {
        EditMediaRatio: "/Media/EditMediaRatio",
        DisableMediaRatio: "/Media/DisableMediaRatio",
        SaveMediaRatio: "/Media/SaveMediaRatio"
    }

    // Variable that holds the data which will be sent to the server when the user saves a media ratio
    var cropData = {
        ratio: "",
        cropX1: 0,
        cropY1: 0,
        cropX2: 0,
        cropY2: 0
    };

    var remoteItemCheckTimer;

    // Initialisations
    bindEvents();
    initialiseModal();
    $("#mediaHosting").change();

    function bindEvents() {
        // User clicks enable/disable for a media ratio
        $("#mediaRatios").on("click", "li a.enable-ratio", toggleMediaRatio);
        $("#mediaRatios").on("click", "li a.disable-ratio", toggleMediaRatio);
        $("#mediaRatios").on("click", "li a.edit-cropping", editMediaRatio);

        // User saves the edited media ratio
        $("#editMediaRatio").on("click", "#saveMediaRatio", saveMediaRatioEdit);
        // User cancels media ratio edit
        $("#editMediaRatio").on("click", "#cancelMediaRatioEdit", closeMediaRatioEdit);
        // Hosting type changed
        $("#mediaHosting").change(hostingChanged);
        $("#Media_RemoteItemCode").keyup(remoteItemCodeChanged).trigger("keyup");
    }

    function initialiseModal() {
        $("#editMediaRatio").modal({
            show: false
        });
    }

    function toggleMediaRatio(e) {
        // Get the ratio name
        switch ($(e.currentTarget).hasClass("enable-ratio")) {
            case true:
                // Edit the ratio
                editMediaRatio(e);
                break;
            case false:
                // Disable the ratio
                $.ajax({
                    url: ServiceUrls.DisableMediaRatio,
                    data: {
                        ratio: $(e.currentTarget).attr("rel")
                    },
                    success: function (data) {
                        replaceMediaRatioEditItem(data);
                    },
                    error: function (a, b, c) {
                        if (console && console.error && JSON && JSON.stringify) console.error("a: " + JSON.stringify(a) + "| b: " + b + "|c: " + c)
                    }
                });
                break;
        }
        return false;
    }

    function editMediaRatio(e) {
        cropData.ratio = $(e.currentTarget).attr("rel");
        $.ajax({
            url: ServiceUrls.EditMediaRatio,
            data: {
                ratio: cropData.ratio
            },
            success: function (data) {
                $("#cropArea").html(data);
                $("#editMediaRatio").modal("show");
                initialiseCropTool();
            },
            error: function (a, b, c) {
                if (console && console.error && JSON && JSON.stringify) console.error("a: " + JSON.stringify(a) + "| b: " + b + "|c: " + c)
            }
        });
        return false;
    }

    function saveMediaRatioEdit() {
        $.ajax({
            url: ServiceUrls.SaveMediaRatio,
            data: cropData,
            success: function (data) {
                replaceMediaRatioEditItem(data);
            },
            error: function (a, b, c) {
                if (console && console.error && JSON && JSON.stringify) console.error("a: " + JSON.stringify(a) + "| b: " + b + "|c: " + c)
            }
        });
        closeMediaRatioEdit();
        return false;
    }

    function replaceMediaRatioEditItem(newEditItem) {
        var $newEditItem = $(newEditItem);
        // Find the equivalent element already in the media ratio list
        var existingEditItem = $("#mediaRatios > li[id=" + $newEditItem.attr("id") + "]");
        existingEditItem.after($newEditItem);
        existingEditItem.remove();
    }



    function closeMediaRatioEdit() {
        $("#editMediaRatio").modal("hide");
        // Clean up cropping tool overlay elements. Dont know why we have to do this and dont have time to look into it...
        $(".imgareaselect-outer").remove();
        $(".imgareaselect-selection").parent().remove();
        return false;
    }

    // Initialise the crop tool
    function initialiseCropTool() {
        // Get the aspect ratio from the size of mediaRatioPreview
        var previewBox = $("#mediaRatioPreview");
        var aspectRatio = previewBox.height() / previewBox.width();
        var stringRatio = "1:" + aspectRatio;
        $('#mediaFullCropbox').imgAreaSelect({
            zIndex: 20000,
            aspectRatio: stringRatio,
            handles: true,
            onSelectChange: previewRatiodImage,
            onSelectEnd: function (img, selection) {
                cropData.cropX1 = selection.x1;
                cropData.cropY1 = selection.y1;
                cropData.cropX2 = selection.x2;
                cropData.cropY2 = selection.y2;
            }
        });
    }

    function previewRatiodImage(img, selection) {

        var previewBoxWidth = $("#mediaRatioPreview").width();
        var previewBoxHeight = $("#mediaRatioPreview").height();

        var scaleX = previewBoxWidth / (selection.width || 1);
        var scaleY = previewBoxHeight / (selection.height || 1);

        var fullCropBoxWidth = $("#mediaFullCropbox").width();
        var fullCropBoxHeight = $("#mediaFullCropbox").height();

        $('#mediaRatioPreview img').css({

            width: Math.round(scaleX * fullCropBoxWidth) + 'px',
            height: Math.round(scaleY * fullCropBoxHeight) + 'px',
            marginLeft: '-' + Math.round(scaleX * selection.x1) + 'px',
            marginTop: '-' + Math.round(scaleY * selection.y1) + 'px'
        });
    }

    function hostingChanged(e) {
        switch ($(e.currentTarget).val()) {
            case "Local":
                $(".local_hosting_properties").show();
                $(".remote_hosting_properties").hide();
                break;
            case "Document":
                $(".local_hosting_properties").show();
                $(".remote_hosting_properties").hide();
                break;
            default:
                $(".local_hosting_properties").hide();
                $(".remote_hosting_properties").show();
                break;
        }
    }

    function remoteItemCodeChanged(e) {
        $(".remote-media-preview").hide();
        if ($(e.currentTarget).val() == "") {
            $("#remoteMediaNotSpecified").show();
            clearTimeout(remoteItemCheckTimer);
        }
        else {
            remoteItemCheckTimer = setTimeout(function () { checkRemoteItemExists() }, 500);
        }
    }

    function checkRemoteItemExists() {
        var remoteItemCode = $("#Media_RemoteItemCode").val();
        switch ($("#mediaHosting").val()) {
            case "Local":
                $("#mediaHosting").trigger("changed"); // This should not happen but if it does then re-trigger the media hosting seclect inputs change event.
                break;
            case "RemoteYoutube":
                // Check that the media item exists
                var errorTimer = setTimeout(function () {
                    // Assume error
                    $("#remoteMediaNotFound").show();
                }, 3000);
                $.ajax({
                    url: "https://gdata.youtube.com/feeds/api/videos/" + remoteItemCode + "?v=2&alt=json",
                    dataType: "jsonp",
                    success: function (data) {
                        clearTimeout(errorTimer);
                        $("#remoteMediaNotFound").hide();
                        $("#remoteMediaPreview")
                            .html('<iframe width="560" height="315" src="http://www.youtube.com/embed/' + remoteItemCode + '" frameborder="0" allowfullscreen></iframe>')
                            .show();
                    }
                });

            case "RemoteVimeo":
                // Check that the media item exists
                var errorTimer = setTimeout(function () {
                    // Assume error
                    $("#remoteMediaNotFound").show();
                }, 3000);
                $.ajax({
                    url: "http://vimeo.com/api/v2/video/" + remoteItemCode + ".json",
                    dataType: "jsonp",
                    success: function (data) {
                        clearTimeout(errorTimer);
                        $("#remoteMediaNotFound").hide();
                        $("#remoteMediaPreview")
                            .html('<iframe width="560" height="315" src="//player.vimeo.com/video/' + remoteItemCode + '?title=0&amp;byline=0&amp;portrait=0&amp;color=63aa9c" frameborder="0" allowfullscreen></iframe>')
                            .show();
                    }
                });

                break;
            case "RemoteBrightCove":
                // todo...
                break;
        }
    }

});