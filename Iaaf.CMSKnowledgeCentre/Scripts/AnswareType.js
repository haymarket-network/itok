﻿$(function () {
    $('.field-validation-valid').hide();
    $(".select_answer-type").change(function () {
        var selectedValue = $(this).val();
      
        $(".answer-type-input").hide();

        switch (parseInt(selectedValue)) {
            case 3:
                $('.options-type').show();
                $("#IsAttachment").prop("checked", false);
                $('#TemplateName option[value="0"]').attr("selected", true);
                break;
            case 4:
                $('.isattachment-type').show();
                $("#IsAttachment").prop("checked", true);
                $("#OptionType").prop("checked", '');
                $("#text-option").val('');
                $('#TemplateName option[value="0"]').attr("selected", true);
                break;
            case 5:
                $('.template-type').show();
                $("#OptionType").prop("checked", '');
                $("#IsAttachment").prop("checked", false);
                $("#text-option").val('');
                break;
        }
    });

});

function validateQuestionForm() {
    $('.field-validation-valid').hide();
    var selectedAnswerTypeValue = $("#AnswerType").val();
    if ($("#QuestionText").val() == "") {
        $('#required-questionText').show();
        return false;
    }
    else if (parseInt(selectedAnswerTypeValue) == 3 || parseInt(selectedAnswerTypeValue) == 4 || parseInt(selectedAnswerTypeValue) == 5) {

        switch (parseInt(selectedAnswerTypeValue)) {
            case 3:
                if ($("input[type=radio]:checked").length<1) {
                    $('#required-optionType').show();
                    return false;
                }
                else if ($("#text-option").val() == "") {
                    $('#required-optionTypeText').show();
                    return false;
                }
                break;
            case 4:
                if (!$("#IsAttachment").prop("checked")) {
                    $("#IsAttachment").prop("checked", true);
                }
                break;
            case 5:
                if ($('#TemplateName').val() == "0") {
                    $('#required-templateName').show();
                    return false;
                }
                break;
        }
    }
    else if ($("#Order").val() == "") {
        $('#required-order').show();
        return false;
    }
    return true;
}

function submitQuestion() {
    if (validateQuestionForm()) {
        $("[data-dismiss=modal]").trigger({ type: "click" });
        $.ajax({
            type: "POST",
            url: "/Question/GetQuestionDetail",
            data: $('form#AddCategoryForm').serialize(),
            success: function (data) {
                $('#divQuestions').html(data);
            },
            error: function () {
                alert('Error');
            }
        });
        return false;
    }
}

