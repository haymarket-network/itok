﻿$(function () {
    $("#permissions a").click(function (e) {
        var $hiddenPermissions = $("#hiddenPermissions");
        var $anchor = $(e.currentTarget);
        var $listItem = $anchor.parent();
        var permissionName = $anchor.attr("id");
        var existingHiddenPermissions = $hiddenPermissions.children("input[value=" + permissionName + "]");
        if ($listItem.hasClass("disabled")) {
            // Enable permission
            if (existingHiddenPermissions.length == 0) {
                $hiddenPermissions.append("<input type='hidden' name='User.Permissions' value='" + permissionName + "' />");
            }
            $listItem.removeClass("disabled").addClass("enabled");
        } else {
            // Disable permission
            if (existingHiddenPermissions.length != 0) {
                existingHiddenPermissions.remove();
            }
            $listItem.removeClass("enabled").addClass("disabled");
        }
        return false;
    });
});