﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using ITOK.Core.Data.Interceptors;
using ITOK.Core.Services.Security;
using CMS.Helpers.MembershipProviders;
using ITOK.Core.Services.Helpers;

namespace CMS
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            ITOKNinjectModule module = new ITOKNinjectModule();
            module.Load();

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

           

            EntityMapper.InitialiseMappings();



            // Register Membership Provider
            var membershipProvider = new MembershipProvider();
            LiveUser.SetMembershipProvider(membershipProvider);
            CMSAuthorizeAttribute.SetMembershipProvider(membershipProvider);

            // Configure versioning
            ITOK.Core.Data.MongoRepository.ConfigureVersioning<ITOK.Core.Data.DTOs.Article>(membershipProvider, x => x.Id);
            ITOK.Core.Data.MongoRepository.ConfigureVersioning<ITOK.Core.Data.DTOs.SubmittedArticle>(membershipProvider, x => x.Id);
            ITOK.Core.Data.MongoRepository.ConfigureVersioning<ITOK.Core.Data.DTOs.Media>(membershipProvider, x => x.Id);
            ITOK.Core.Data.MongoRepository.ConfigureVersioning<ITOK.Core.Data.DTOs.SubmittedMedia>(membershipProvider, x => x.Id);

            //Added 6/5/14 CR
            //ITOK.Core.Data.MongoRepository.ConfigureVersioning<FIPP.Data.DTOs.Event>(membershipProvider, x => x.ID);

            // Register interceptors
            ITOK.Core.Data.MongoRepository.RegisterInsertInterceptor(new AuditableInsertInterceptor(membershipProvider));
            ITOK.Core.Data.MongoRepository.RegisterUpdateInterceptor(new AuditableUpdateInterceptor(membershipProvider));

            var mediaInterceptor = new MediaInterceptor();
            ITOK.Core.Data.MongoRepository.RegisterInsertInterceptor(mediaInterceptor);
            ITOK.Core.Data.MongoRepository.RegisterUpdateInterceptor(mediaInterceptor);
            ITOK.Core.Data.MongoRepository.RegisterRemoveInterceptor(new MediaRemoveInterceptor());

            //ITOK.Core.Data.MongoRepository.RegisterRemoveInterceptor(new DocumentRemoveInterceptor());

            //ITOK.Core.Data.MongoRepository.RegisterRemoveInterceptor(new MediaGalleryRemoveInterceptor());

            // MongoRepository.RegisterUpdateInterceptor(new CompetitionUpdateInterceptor());
            var articleUpdateInsertInterceptor = new ArticleUpdateInsertInterceptor();
            ITOK.Core.Data.MongoRepository.RegisterUpdateInterceptor(articleUpdateInsertInterceptor);
            ITOK.Core.Data.MongoRepository.RegisterInsertInterceptor(articleUpdateInsertInterceptor);
            ITOK.Core.Data.MongoRepository.RegisterRemoveInterceptor(new ArticleRemoveInterceptor());
        }
    }
}
