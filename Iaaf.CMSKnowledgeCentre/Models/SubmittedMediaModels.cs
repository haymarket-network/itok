﻿using CMS.Models.Constants;
using ITOK.Core.Common.Config;
using ITOK.Core.Data.Model;
using ITOK.Core.ViewModels;
using System;
using System.Collections.Generic;


namespace CMS.Models
{   

    [Serializable]
    public class JsonSubmittedMediaSaveModel
    {
        public IList<UserResponseItem> Responses = new List<UserResponseItem>();

        public string MediaId { get; set; }

        public bool Success { get; set; }
    }

    [Serializable]
    public class JsonSubmittedMediaUploadModel
    {
        public IList<UserResponseItem> Responses = new List<UserResponseItem>();

        public SubmittedMedia Media { get; set; }

        public bool Success { get; set; }
    }

    [Serializable]
    public class SubmittedMediaEditModel
    {
        public IList<SubmittedMediaRatioEditItemModel> MediaRatioEditModels = new List<SubmittedMediaRatioEditItemModel>();

        public IList<UserResponseItem> Responses = new List<UserResponseItem>();

        public IList<VersionWrapper> VersionHistory;

        public SubmittedMedia Media { get; set; }
    }

    [Serializable]
    public class SubmittedMediaListModel
    {
        public MediaFilterType FilterType;
        public IList<SubmittedMedia> Media = new List<SubmittedMedia>(); // For use when displaying a list of media items        
        public PaginationModel Pagination;
        public IList<UserResponseItem> Responses = new List<UserResponseItem>();
        public IList<SubmittedMedia> SearchResults;
        public string SearchText;
        public MediaSearchType SearchType;
        public bool ShowDeletedMedia = false;
        public int TotalSearchResults;

    }
    [Serializable]
    public class SubmittedMediaRatioEditItemModel
    {
        public SubmittedMedia Media { get; set; }

        public MediaRatioElement RatioConfig { get; set; }
    }

    [Serializable]
    public class SubmittedMediaRatioEditPanelModel
    {
        public int CropBoxEditWidth { get; set; }

        public int CropBoxPreviewWidth { get; set; }

        public SubmittedMedia Media { get; set; }
        public MediaRatioElement RatioConfig { get; set; }
    }
}