﻿using ITOK.Core.Common.Config;
using ITOK.Core.Data.Model;
using ITOK.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace CMS.Models
{
    [Serializable]
    public class TagEditModel
    {
        public Tags Tag { get; set; }
        public List<Question> questions { get; set; }

        public List<SelectListItem> parentTags;
        public IList<UserResponseItem> Responses = new List<UserResponseItem>();
    }

}