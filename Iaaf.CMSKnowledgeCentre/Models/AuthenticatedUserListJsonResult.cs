﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Models
{
    [DataContract]
    public class UserListJsonResult
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string UserIdentification { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string UserEmail { get; set; }

        [DataMember]
        public string GivenName { get; set; }
        [DataMember]
        public string RetailerID { get; set; }
        [DataMember]
        public string Telephone { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string JobRoleId { get; set; }




        public UserListJsonResult(string id, string UserIdentification, string UserName, string UserEmail, string GivenName, string RetailerID, string Telephone, string Title, string JobRoleId)
        {
            this.Id = id;
            this.UserIdentification = UserIdentification;
            this.UserName = UserName;
            this.UserEmail = UserEmail;
            this.GivenName = GivenName;
            this.RetailerID = RetailerID;

            this.Telephone = Telephone;
            this.Title = Title;
            this.JobRoleId = JobRoleId;

        }


    }
}
