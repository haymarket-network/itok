﻿using ITOK.Core.Common.Constants;
using ITOK.Core.Data.Model;
using ITOK.Core.ViewModels;
using System;
using System.Collections.Generic;


namespace CMS.Models
{


    [Serializable]
    public class AlertEditModel
    {
        public Alert alert;
        public IList<AlertTag> jobRoles = new List<AlertTag>();
        public User CreatedBy;
        public User UpdatedBy;

        public IList<ArticleCommentItem> Comments = new List<ArticleCommentItem>();

        public string AuthorUserName;
        public string AuthorUserId;
        public AlertEditModel(Alert _alert)
        {
            alert = _alert;
        }

        public AlertEditModel()
        {

        }
    }

    public class AlertTag
    {
        public string Id { get; set; }
        public string DisplayText { get; set; }
        public bool IsSelected { get; set; }
    }


}