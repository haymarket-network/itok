﻿using ITOK.Core.Common.Constants;
using ITOK.Core.Data.Model;
using ITOK.Core.ViewModels;
using System;
using System.Collections.Generic;


namespace CMS.Models
{
    [Serializable]
    public class ArticleListModel
    {
        public IList<Article> Articles = new List<Article>();
        public IList<Article> FeaturedArticles = new List<Article>();
        public PaginationModel Pagination;
        public IList<UserResponseItem> Responses = new List<UserResponseItem>();
        public IList<ArticleType> articleTypes = new List<ArticleType>();
        public bool ShowDeletedArticles = false;

        public ArticleType FilterType;
        public string SearchText;
        public int TotalSearchResults;
        public IList<Article> SearchResults;

        public IList<Article> AreaFeatured { get; set; }

      
    }



    [Serializable]
    public class ArticleEditModel
    {
        public Article Article;
        public IList<VersionWrapper> VersionHistory;
        public IList<UserResponseItem> Responses = new List<UserResponseItem>();
        public IList<ArticleType> articleTypes = new List<ArticleType>();
        public IList<Tags> availableTags = new List<Tags>();
        public User CreatedBy;
        public User UpdatedBy;
        public int ViewCount;
        public bool Versioned;

        public IList<ArticleCommentItem> Comments = new List<ArticleCommentItem>();

        public string AuthorUserName;
        public string AuthorUserId;
        public ArticleEditModel(Article article)
        {
            Article = article;
        }

        public ArticleEditModel()
        {

        }
    }
}