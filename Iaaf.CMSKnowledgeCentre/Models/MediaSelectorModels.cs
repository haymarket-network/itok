﻿using ITOK.Core.Data.Model;
using System;
using System.Collections.Generic;

namespace CMS.Models
{
    [Serializable]
    public class MediaResults
    {
        public IList<MediaResultItem> Results = new List<MediaResultItem>();
        public ResultSetType Type;
    }

    [Serializable]
    public class MediaResultItem
    {
        public Media Media { get; set; }


        public string ThumbnailUrl { get; set; }
    }
    [Serializable]
    public enum ResultSetType
    {
        MediaRecent,
        MediaSearchAny,

        LogoMediaSearchByEvent,

        GalleryRecent,
        GallerySearchAny
    }



    [Serializable]
    public class SubmittedMediaResults
    {
        public IList<SubmittedMediaResultItem> Results = new List<SubmittedMediaResultItem>();
        public ResultSetType Type;
    }

    [Serializable]
    public class SubmittedMediaResultItem
    {
        public SubmittedMedia Media { get; set; }


        public string ThumbnailUrl { get; set; }
    }
  


}
