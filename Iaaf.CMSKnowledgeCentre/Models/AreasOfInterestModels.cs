﻿using System;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data.Model;
using ITOK.Core.ViewModels;
using System.Collections.Generic;

namespace CMS.Models
{
    [Serializable]
    public class AreasOfInterestEditModel
    {
        public AreasOfInterest areaOfInterest {get; set;}
        public List<Tags> availableParents { get; set; }
        public List<Tags> availableChoices { get; set; }
    }
}
