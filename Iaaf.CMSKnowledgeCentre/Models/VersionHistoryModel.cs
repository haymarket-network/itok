﻿using ITOK.Core.Data.Model;
using System.Collections.Generic;

namespace CMS.Models
{
    public class VersionHistoryModel
    {
        public VersionHistoryModel(IList<VersionWrapper> versionHistory, string entityName, string revertUrl)
        {
            VersionHistory = versionHistory;
            EntityName = entityName;
            RevertUrl = revertUrl;
        }
        public IList<VersionWrapper> VersionHistory;
        public string EntityName;
        public string RevertUrl;
    }
}