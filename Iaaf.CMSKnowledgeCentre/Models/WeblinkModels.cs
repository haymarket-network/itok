﻿using ITOK.Core.Common.Constants;
using ITOK.Core.Data.Model;
using ITOK.Core.ViewModels;
using System;
using System.Collections.Generic;


namespace CMS.Models
{


    [Serializable]
    public class WeblinkEditModel
    {
        public Weblink weblink;
        public IList<WeblinkTag> jobRoles = new List<WeblinkTag>();
        public User CreatedBy;
        public User UpdatedBy;

  
        public string AuthorUserName;
        public string AuthorUserId;
        public WeblinkEditModel(Weblink _Weblink)
        {
            weblink = _Weblink;
        }

        public WeblinkEditModel()
        {

        }
    }

    public class WeblinkTag
    {
        public string Id { get; set; }
        public string DisplayText { get; set; }
        public bool IsSelected { get; set; }
    }


}