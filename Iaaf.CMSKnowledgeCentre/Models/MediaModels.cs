﻿using CMS.Models.Constants;
using ITOK.Core.Common.Config;
using ITOK.Core.Data.Model;
using ITOK.Core.ViewModels;
using System;
using System.Collections.Generic;


namespace CMS.Models
{   

    [Serializable]
    public class JsonMediaSaveModel
    {
        public IList<UserResponseItem> Responses = new List<UserResponseItem>();

        public string MediaId { get; set; }

        public bool Success { get; set; }
    }

    [Serializable]
    public class JsonMediaUploadModel
    {
        public IList<UserResponseItem> Responses = new List<UserResponseItem>();

        public Media Media { get; set; }

        public bool Success { get; set; }
    }

    [Serializable]
    public class MediaEditModel
    {
        public IList<MediaRatioEditItemModel> MediaRatioEditModels = new List<MediaRatioEditItemModel>();

        public IList<UserResponseItem> Responses = new List<UserResponseItem>();

        public IList<VersionWrapper> VersionHistory;

        public Media Media { get; set; }
    }

    [Serializable]
    public class MediaListModel
    {
        public MediaFilterType FilterType;
        public IList<Media> Media = new List<Media>(); // For use when displaying a list of media items        
        public PaginationModel Pagination;
        public IList<UserResponseItem> Responses = new List<UserResponseItem>();
        public IList<Media> SearchResults;
        public string SearchText;
        public MediaSearchType SearchType;
        public bool ShowDeletedMedia = false;
        public int TotalSearchResults;

    }
    [Serializable]
    public class MediaRatioEditItemModel
    {
        public Media Media { get; set; }

        public MediaRatioElement RatioConfig { get; set; }
    }

    [Serializable]
    public class MediaRatioEditPanelModel
    {
        public int CropBoxEditWidth { get; set; }

        public int CropBoxPreviewWidth { get; set; }

        public Media Media { get; set; }
        public MediaRatioElement RatioConfig { get; set; }
    }
}