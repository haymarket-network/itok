﻿using ITOK.Core.Common.Constants;
using ITOK.Core.Data.Model;
using ITOK.Core.ViewModels;
using System;
using System.Collections.Generic;


namespace CMS.Models
{
    [Serializable]
    public class SubmissionOverview
    {
        public string tagName;
        public string tagId;
        public string topicHeader;
        public string topicId;
        public SubmittedArticle article;

    }
    [Serializable]
    public class SubmissionsOverviewModel
    {
        public List<SubmissionOverview> overviewList = new List<SubmissionOverview>();
        public Tags EventTag { get; set; }
        public Tags YearTag { get; set; }

    }

    [Serializable]
    public class SubmissionsSelectTagModel
    {
        public List<Tags> EventTags = new List<Tags>();
        public List<Tags> YearTags = new List<Tags>();

    }



    [Serializable]
    public class SubmissionsListModel
    {
        public IList<SubmittedArticle> Articles = new List<SubmittedArticle>();
        public PaginationModel Pagination;
        public IList<UserResponseItem> Responses = new List<UserResponseItem>();
        public IList<ArticleType> articleTypes = new List<ArticleType>();
        public bool ShowDeletedArticles = false;

        public SubmissionStatus SubmissionStatus;
        public string SearchText;
        public int TotalSearchResults;
        public IList<SubmittedArticle> SearchResults;
        
      
    }



    [Serializable]
    public class SubmissionEditModel
    {
        public SubmittedArticle Article;
        public IList<VersionWrapper> VersionHistory;
        public IList<UserResponseItem> Responses = new List<UserResponseItem>();
        public IList<ArticleType> articleTypes = new List<ArticleType>();
        public IList<Tags> availableTags = new List<Tags>();
        public User CreatedBy;
        public User UpdatedBy;
        public Tags eventTag;
        public Tags yearTag;
        public int ViewCount;
        public bool Versioned;

        public IList<ArticleCommentItem> Comments = new List<ArticleCommentItem>();

        public string AuthorUserName;
        public string AuthorUserId;
        public SubmissionEditModel(SubmittedArticle article)
        {
            Article = article;
        }

        public SubmissionEditModel()
        {

        }
    }
}