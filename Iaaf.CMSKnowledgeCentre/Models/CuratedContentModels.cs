﻿using System;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data.Model;
using ITOK.Core.ViewModels;
using System.Collections.Generic;
using System.Web.Mvc;

namespace CMS.Models
{
    [Serializable]
    public class CuratedContentEditModel
    {
        public CuratedContent curatedContent {get; set;}
        public Article selectedArticle { get; set; }
        public List<Tags> availableRoles { get; set; }
        public IList<UserResponseItem> Responses = new List<UserResponseItem>();
        public List<SelectListItem> jobRoles { get; set; }
    }

    [Serializable]
    public class CuratedContentListModel
    {
        public IList<CuratedContent> Articles = new List<CuratedContent>();
        public IList<UserResponseItem> Responses = new List<UserResponseItem>();
    }
}
