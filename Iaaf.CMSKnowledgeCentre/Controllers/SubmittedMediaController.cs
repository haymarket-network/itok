﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using CMS.Helpers;
using CMS.Helpers.Authorise;
using CMS.Models;
using CMS.Models.Constants;
using ITOK.Core.Common;
using ITOK.Core.Common.Config;
using ITOK.Core.Common.Constants;
using ITOK.Core.Common.Utils;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using ITOK.Core.Data.Model.HS;
using ITOK.Core.Services.CMS;
using ITOK.Core.ViewModels;
using ITOK.Core.Services.Security;

namespace CMS.Controllers
    {
    public class SubmittedMediaController : BaseController
        {
        //private const string SubmittedMediaModelsCacheKey = "SubmittedMediaModelsCache";

        private const decimal CropBoxEditHeightCap = 400;
        private const decimal CropBoxEditWidthCap = 400;
        private const decimal CropBoxPreviewHeightCap = 400;
        private const decimal CropBoxPreviewWidthCap = 400;
        private const int DefaultPageRange = 10;
        private const int DefaultPageSize = 20;
        private const int DefaultStartPage = 1;
        private const int MaxFileSize = 104857600;
        private const string MediaFilterTypeCacheKey = "MediaFilterTypeCache";
        private const string SubmittedMediaSearchCountCacheKey = "SubmittedMediaSearchCountCache";
        private const string SubmittedMediaSearchTextCacheKey = "SubmittedMediaSearchTextCache";
        private const string MediaSearchTypeCacheKey = "MediaSearchTypeCache";
        private const string PaginationBaseUrl = "/SubmittedMedia/";
        private const string PaginationInstanceKey = "SubmittedMediaIndex";
        private const string ShowDeletedSubmittedMediaCacheKey = "ShowDeletedSubmittedMediaCache";
        private const string UserResponseInstanceKey = "SubmittedMedia";
        /* 100 MB */

        // Helpers
        private readonly EntityEditHelper<SubmittedMedia> _SubmittedMediaEditHelper = new EntityEditHelper<SubmittedMedia>();

        private readonly PaginationHelper _pageHelper;
        private readonly UserResponseHelper _responseHelper;

        private IMongoRepository1 mongoRepository;

        private ArticleService _articleService;

        // Services
        private SubmittedMediaService _SubmittedMediaService;

        public SubmittedMediaController()
            {
            mongoRepository = AppKernel.GetInstance<IMongoRepository1>();
            _articleService = new ArticleService(mongoRepository);
            _SubmittedMediaService = new SubmittedMediaService(mongoRepository);
            _pageHelper = new PaginationHelper(PaginationInstanceKey, PaginationBaseUrl, DefaultPageSize);
            _responseHelper = new UserResponseHelper(UserResponseInstanceKey);
            }

        public SubmittedMediaController(IMongoRepository1 _mongoRepository)
            {
            this.mongoRepository = _mongoRepository;
            _articleService = new ArticleService(mongoRepository);
            _SubmittedMediaService = new SubmittedMediaService(mongoRepository);
            _pageHelper = new PaginationHelper(PaginationInstanceKey, PaginationBaseUrl, DefaultPageSize);
            _responseHelper = new UserResponseHelper(UserResponseInstanceKey);
            }

        #region Index & Page

        //public ActionResult AudioFile()
        //    {
        //    var fileStorageProvider = new AmazonS3FileStorageProvider();

        //    var fileUploadViewModel = new FileUploadViewModel(fileStorageProvider.PublicKey,
        //                                                      fileStorageProvider.PrivateKey,
        //                                                      fileStorageProvider.BucketName,
        //                                                      string.Format("{0}/SubmittedMedia/Create?type=audio", ITOKConfig.Instance.Audio.Redirct));

        //    fileUploadViewModel.SetPolicy(fileStorageProvider.GetPolicyString(
        //                    fileUploadViewModel.FileId, fileUploadViewModel.RedirectUrl));

        //    return View(fileUploadViewModel);
        //    }

     
        [CMSAuthorize("UseCMS")]
        public void CancelSearch()
            {
            SearchText = string.Empty;
            SearchCount = 0;
            SearchType = MediaSearchType.None;
            }

        [CMSAuthorize("UseCMS")]
        public ActionResult Filter(MediaFilterType type)
            {
            SubmittedMediaFilter = type;
            return RedirectToAction("Index");
            }

        [CMSAuthorize("UseCMS")]
        [HttpPost]
        public JsonResult GetTotalSearchResults(string searchText, MediaSearchType searchType)
            {
            SubmissionStatus[] showWithStatus;
            if (ShowDeletedSubmittedMedia) showWithStatus = new[] { SubmissionStatus.Deleted };
            else showWithStatus = new[] { SubmissionStatus.InProgress, SubmissionStatus.Submitted };

            var resultCount = 0;

            switch (searchType)
                {
                case MediaSearchType.ByTitle:
                    resultCount = _SubmittedMediaService.SearchCount(searchText, true, false, showWithStatus, false);
                    break;

                default:
                    throw new ArgumentException("Unrecognised MediaSearchType: " + searchType.ToString());
                }
            return Json(resultCount);
            }

        [CMSAuthorize("UseCMS")]
        public ActionResult Index()
            {
            return Page();
            }

        [CMSAuthorize("UseCMS")]
        public ActionResult Page(int pageNumber = -1, bool? showDeletedSubmittedMedia = null)
            {
            if (showDeletedSubmittedMedia != null) ShowDeletedSubmittedMedia = (bool)showDeletedSubmittedMedia;

            // Save any changes the user has made to the SubmittedMedia item
            if (pageNumber != -1) _pageHelper.SetPage(pageNumber);

            return View("Index", getSubmittedMediaListModel());
            }

        [CMSAuthorize("UseCMS")]
        public PartialViewResult Search(string searchText, int skip, MediaSearchType searchType)
            {
            SearchCount = skip + DefaultPageSize;
            return PartialView("SubmittedMediaList", search(searchText, skip, DefaultPageSize, searchType));
            }

        #endregion Index & Page

        #region Create & Edit

        [CMSAuthorize("UseCMS")]
        public JsonResult CancelJson()
            {
            _SubmittedMediaEditHelper.ClearCache();
            return Json(new { success = true });
            }

        [CMSAuthorize("UseCMS")]
        public ActionResult ChangeStatus(string id, SubmissionStatus? status, int? inDays)
            {
            if (string.IsNullOrWhiteSpace(id)) throw new ArgumentNullException("id", "Id must be supplied to the ChangeStatus action method");
            var SubmittedMedia = _SubmittedMediaService.FindByIds(null, id).FirstOrDefault();
            if (status != null) SubmittedMedia.Status = status.Value;
            if (inDays != null)
                {
                SubmittedMedia.LiveFrom = DateTime.Today.AddDays(inDays.Value);
                SubmittedMedia.LiveTo = null;
                }
            _SubmittedMediaService.Save(SubmittedMedia);
            return RedirectToAction("Index");
            }

        [CMSAuthorize("UseCMS")]
        public ActionResult Create(int relatedEventId = 0, string relatedDisciplineNameSlug = null)
            {
            if (Request.QueryString["bucket"] != null)
                return SaveAudioFile(Request.QueryString["key"]);

            _SubmittedMediaEditHelper.ClearCache();
            var model = new SubmittedMediaEditModel
            {
                Media = _SubmittedMediaEditHelper.Cache,
            };
            model.Media.ShowInMedia = true;
            model.Media.LiveFrom = DateTime.UtcNow;

            return View("Edit", model);
            }

        [CMSAuthorize("UseCMS")]
        [HttpGet]
        public JsonResult CreateJson()
            {
            _SubmittedMediaEditHelper.ClearCache();
            return Json(_SubmittedMediaEditHelper.Cache, JsonRequestBehavior.AllowGet);
            }

        [CMSAuthorize("UseCMS")]
        public ActionResult Delete(string id, FormCollection collection)
            {
            try
                {
                // Get the SubmittedMedia to delete
                var SubmittedMedia = _SubmittedMediaService.FindByIds(null, id).FirstOrDefault();
                if (SubmittedMedia == null)
                    {
                    _responseHelper.AddUserResponse(ResponseType.Error, "Could not find the specified SubmittedMedia to delete");
                    return RedirectToAction("Index");
                    }

                // Not working. Temporarily using save instead TODO: Use the DeleteById service method
                //_SubmittedMediaService.Delete(SubmittedMedia.UrlSlug);
                SubmittedMedia.Status = SubmissionStatus.Deleted;
                _SubmittedMediaService.Save(SubmittedMedia);

                // Clear the SubmittedMedia cache
                _SubmittedMediaEditHelper.ClearCache();
                _responseHelper.AddUserResponse(ResponseType.Notice, string.Format("Successfully deleted SubmittedMedia: {0}", SubmittedMedia.Title));
                return RedirectToAction("Index");
                }
            catch (Exception ex)
                {
                _responseHelper.AddUserResponse(ResponseType.Error, "Error", ex.Message);
                return RedirectToAction("Index");
                }
            }

        /// <summary>
        /// Disables a SubmittedMedia profile for the current SubmittedMedia item and tries to remove the image from disk
        /// </summary>
        /// <param name="profileName">The name of the SubmittedMedia profile to remove</param>
        /// <returns>The partial view SubmittedMedia/MediaProfileEditItem.cshtml</returns>
        [CMSAuthorize("UseCMS")]
        public PartialViewResult DisableMediaRatio(MediaRatio? ratio, string id)
            {
            if (ratio == null) throw new ArgumentException("a MediaRatio to disable must be specified", "ratio");
            // Try to get the SubmittedMedia item
            var SubmittedMedia = _SubmittedMediaEditHelper.Cache;
            // Check that the SubmittedMedia item has the ratio available to start with
            if (SubmittedMedia.AvailableRatios.Contains(ratio.Value))
                {
                // Get all profiles for the given ratio
                foreach (var profileConfig in ITOKConfig.Instance.MediaProfiles.GetProfileConfigsFromRatio(ratio.Value))
                    {
                    if (profileConfig.ProfileName == MediaProfile.Original) continue;
                    if (S3Utilities.MediaExists(profileConfig, SubmittedMedia.FileName))
                        S3Utilities.DeleteMedia(profileConfig, SubmittedMedia.FileName);
                    }
                // Remove the ratio from the SubmittedMedia items available ratios
                SubmittedMedia.AvailableRatios.Remove(ratio.Value);
                _SubmittedMediaEditHelper.Cache = SubmittedMedia;
                }
            // Return a new MediaProfileEditItem view to swap in for the old one on the ui (js)
            var partialModel = new SubmittedMediaRatioEditItemModel
            {
                Media = SubmittedMedia,
                RatioConfig = ITOKConfig.Instance.MediaRatios.GetRatioConfig(ratio.Value)
            };

            return PartialView("MediaRatioEditItem", partialModel);
            }

        [CMSAuthorize("UseCMS")]
        public ActionResult Edit(string id)
            {
            var model = new SubmittedMediaEditModel();

            if (string.IsNullOrEmpty(id) || _SubmittedMediaEditHelper.Cache.Id == id)
                {
                model.Media = _SubmittedMediaEditHelper.Cache;
                }
            else
                {
                // Find the SubmittedMedia to edit
                model.Media = _SubmittedMediaService.FindByIds(new SubmittedMediaHydrationSettings
                {
                    CreatedBy = new UserHydrationSettings(),
                    UpdatedBy = new UserHydrationSettings()
                }, id).FirstOrDefault();
                if (model.Media == null)
                    {
                    _responseHelper.AddUserResponse(ResponseType.Error, "Could not find the specified SubmittedMedia to edit.");
                    return View("Index", getSubmittedMediaListModel());
                    }
                }

            model.VersionHistory = _SubmittedMediaService.GetVersionHistory(model.Media.Id, new VersionHydrationSettings
            {
                ModifiedBy = new UserHydrationSettings()
            });
            model.MediaRatioEditModels = buildMediaRatioEditModel(model.Media);
            model.Responses = _responseHelper.UserResponses;
            _SubmittedMediaEditHelper.Cache = model.Media;
            return View("Edit", model);
            }

        [CMSAuthorize("UseCMS")]
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult EditAction(SubmittedMedia SubmittedMedia, string submit, FormCollection formCollection, string versionComment)
            {
            mergeBasicChanges(SubmittedMedia);
            switch (submit)
                {
                case "Upload SubmittedMedia": return traditionalUploadSubmittedMedia();
                case "Save": return traditionalSave(null, versionComment);
                case "Publish": return traditionalSave(SubmissionStatus.InProgress, versionComment);
                case "Un-Publish": return traditionalSave(SubmissionStatus.Submitted, versionComment);
                case "Cancel": return cancel();
                default: return traditionalUploadSubmittedMedia();
                //default: throw new Exception("Unrecognised edit action");
                }
            }

        [CMSAuthorize("UseCMS")]
        public PartialViewResult EditMediaRatio(MediaRatio? ratio)
            {
            if (ratio == null) throw new ArgumentException("a MediaRatio to edit must be specified", "ratio");
            // Try to get the SubmittedMedia item
            var SubmittedMedia = _SubmittedMediaEditHelper.Cache;

            var ratioConfig = ITOKConfig.Instance.MediaRatios.GetRatioConfig(ratio.Value);

            var partialModel = new SubmittedMediaRatioEditPanelModel
            {
                Media = SubmittedMedia,
                CropBoxEditWidth = GetCropBoxEditWidth(ratio.Value, SubmittedMedia),
                CropBoxPreviewWidth = GetCropBoxPreviewWidth(ratio.Value),
                RatioConfig = ratioConfig
            };
            return PartialView("MediaRatioEditPanel", partialModel);
            }

        //[CMSAuthorize("UseCMS & EditSubmittedMedia")]
        //public ActionResult Feature(string id, int position)
        //    {
        //    _SubmittedMediaService.InsertNewFeatured(id, FeaturedArea.SubmittedMedia, position);
        //    return RedirectToAction("Index");
        //    }

        [CMSAuthorize("UseCMS")]
        public JsonResult GetProfileConfig(string profileName)
            {
            var profile = MediaUtils.MediaProfileFromName(profileName);
            var profileConfig = ITOKConfig.Instance.MediaProfiles.GetProfileConfig(profile);

            return Json(profileConfig, JsonRequestBehavior.AllowGet);
            }

        [CMSAuthorize("UseCMS")]
        public ActionResult HardDelete(string id)
            {
            // Get the SubmittedMedia to delete
            var SubmittedMedia = _SubmittedMediaService.FindByIds(null, id).FirstOrDefault();
            if (SubmittedMedia == null)
                {
                _responseHelper.AddUserResponse(ResponseType.Error, "Could not find the specified SubmittedMedia to delete");
                return RedirectToAction("Index");
                }

            _SubmittedMediaService.DeleteById(id, false);

            // Clear the SubmittedMedia cache
            _SubmittedMediaEditHelper.ClearCache();
            _responseHelper.AddUserResponse(ResponseType.Notice, string.Format("Successfully deleted SubmittedMedia: {0}", SubmittedMedia.Title));
            return RedirectToAction("Index");
            }

        [CMSAuthorize("UseCMS")]
        public ActionResult Restore(string id)
            {
            // Get the SubmittedMedia to delete
            var SubmittedMedia = _SubmittedMediaService.FindByIds(null, id).FirstOrDefault();
            if (SubmittedMedia == null)
                {
                _responseHelper.AddUserResponse(ResponseType.Error, "Could not find the specified SubmittedMedia to restore");
                return RedirectToAction("Index");
                }

            // Not working. Temporarily using save instead
            //_SubmittedMediaService.Delete(SubmittedMedia.UrlSlug);
            SubmittedMedia.Status = SubmissionStatus.InProgress;
            _SubmittedMediaService.Save(SubmittedMedia);

            // Clear the SubmittedMedia cache
            _SubmittedMediaEditHelper.ClearCache();
            _responseHelper.AddUserResponse(ResponseType.Notice, string.Format("Successfully restored SubmittedMedia: {0}", SubmittedMedia.Title));
            return RedirectToAction("Index");
            }

        [CMSAuthorize("UseCMS")]
        public ActionResult RevertToVersion(string versionId)
            {
            if (string.IsNullOrEmpty(versionId)) throw new ArgumentException("Version Id is required to perform this action");
            var version = _SubmittedMediaService.GetVersion(versionId, null);
            if (version == null)
                {
                _responseHelper.AddUserResponse(ResponseType.Notice, "Couldn't find that version", "If this persists, contact the iaafsupport@haymarket.com");
                return RedirectToAction("Edit");
                }
            var SubmittedMediaVersion = _SubmittedMediaService.GetEntityVersion(versionId);
            try
                {
                _SubmittedMediaService.RevertToVersion(versionId);
                _responseHelper.AddUserResponse(ResponseType.Notice, "Successfully reverted SubmittedMedia", string.Format("To the version backed-up on {0} at {1}",
                    version.ModifiedOn.ToString("dd MMMM"),
                    version.ModifiedOn.ToString("H:mm")));
                _SubmittedMediaEditHelper.ClearCache();
                }
            catch (Exception ex)
                {
                _responseHelper.AddUserResponse(ResponseType.Notice, "Error reverting SubmittedMedia", string.Format("To the version of the {0}. Error message: {1}",
                    version.ModifiedOn.ToString("dd MMMM"),
                    ex.Message));
                }

            return RedirectToAction("Edit", new { id = SubmittedMediaVersion.Id });
            }

        [CMSAuthorize("UseCMS")]
        [HttpPost]
        public JsonResult SaveJson(SubmittedMedia media)
            {
            mergeBasicChanges(media);
            if (!validate())
                {
                var model = new JsonSubmittedMediaSaveModel
                {
                    Responses = _responseHelper.UserResponses,
                    Success = false
                };
                return Json(model);
                }
            if (!save(SubmissionStatus.InProgress))
                {
                var model = new JsonSubmittedMediaSaveModel
                {
                    Responses = _responseHelper.UserResponses,
                    Success = false
                };
                return Json(model);
                }
            else
                {
                var model = new JsonSubmittedMediaSaveModel
                {
                    Responses = _responseHelper.UserResponses,
                    Success = true,
                    MediaId = _SubmittedMediaEditHelper.Cache.Id
                };
                // Clear the cache
                _SubmittedMediaEditHelper.ClearCache();
                return Json(model);
                }
            }

        /// <summary>
        /// TODO: Remove any files that have been written to disk
        /// </summary>
        /// <returns></returns>
        /// <summary>
        /// For use when ajax uploading is required
        /// </summary>
        /// <returns></returns>

        [CMSAuthorize("UseCMS")]
        public PartialViewResult SaveMediaRatio(MediaRatio? ratio, int cropX1, int cropY1, int cropX2, int cropY2)
            {
            if (ratio == null) throw new ArgumentException("a MediaRatio to save must be specified", "ratio");

            // First try to find the currently selected SubmittedMedia items original file on disk
            //Image originalImage = Image.FromFile(Server.MapPath(MediaHelper.GetSubmittedMediaPath(MediaProfile.Original, _SubmittedMediaEditHelper.Cache)));

            var originalProfileConfig = ITOKConfig.Instance.MediaProfiles.GetProfileConfig(MediaProfile.Original);
            var originalImage = S3Utilities.GetImage(originalProfileConfig, _SubmittedMediaEditHelper.Cache.FileName);

            /* Quite important to make sure that the ratio is added to the SubmittedMedia item cache here because otherwise,
             * when we come to crop (and save) below, the default SubmittedMedia path will be resolved and the default image
             * will be overwritten.
             */
            var SubmittedMedia = _SubmittedMediaEditHelper.Cache;
            if (!SubmittedMedia.AvailableRatios.Contains(ratio.Value))
                {
                SubmittedMedia.AvailableRatios.Add(ratio.Value);
                _SubmittedMediaEditHelper.Cache = SubmittedMedia;
                }

            // Scale the coordinates for the original image size against the size of the client crop box
            var s = (decimal)originalImage.Width / (decimal)GetCropBoxEditWidth(ratio.Value, SubmittedMedia);
            cropX1 = (int)Math.Round((decimal)cropX1 * s);
            cropX2 = (int)Math.Round((decimal)cropX2 * s);
            cropY1 = (int)Math.Round((decimal)cropY1 * s);
            cropY2 = (int)Math.Round((decimal)cropY2 * s);

            crop(originalImage, ratio.Value, cropX1, cropY1, cropX2, cropY2);
            // Return a new MediaProfileEditItem view to swap in for the old one on the ui (js)
            var partialModel = new SubmittedMediaRatioEditItemModel
            {
               Media = _SubmittedMediaEditHelper.Cache,
                RatioConfig = ITOKConfig.Instance.MediaRatios.GetRatioConfig(ratio.Value)
            };
            return PartialView("MediaRatioEditItem", partialModel);
            }

        [CMSAuthorize("UseCMS")]
        public PartialViewResult SaveSubmittedMediaResize(MediaRatio? ratio, int cropX1, int cropY1, int cropX2, int cropY2)
            {
            if (ratio == null) throw new ArgumentException("a MediaRatio to save must be specified", "ratio");

            // First try to find the currently selected SubmittedMedia items original file on disk
            //Image originalImage = Image.FromFile(Server.MapPath(MediaHelper.GetSubmittedMediaPath(MediaProfile.Original, _SubmittedMediaEditHelper.Cache)));

            var originalProfileConfig = ITOKConfig.Instance.MediaProfiles.GetProfileConfig(MediaProfile.Original);
            var originalImage = S3Utilities.GetImage(originalProfileConfig, _SubmittedMediaEditHelper.Cache.FileName);

            /* Quite important to make sure that the ratio is added to the SubmittedMedia item cache here because otherwise,
             * when we come to crop (and save) below, the default SubmittedMedia path will be resolved and the default image
             * will be overwritten.
             */
            var SubmittedMedia = _SubmittedMediaEditHelper.Cache;
            if (!SubmittedMedia.AvailableRatios.Contains(ratio.Value))
                {
                SubmittedMedia.AvailableRatios.Add(ratio.Value);
                _SubmittedMediaEditHelper.Cache = SubmittedMedia;
                }

            // Scale the coordinates for the original image size against the size of the client crop box
            var s = (decimal)originalImage.Width / (decimal)GetCropBoxEditWidth(ratio.Value, SubmittedMedia);
            cropX1 = (int)Math.Round((decimal)cropX1 * s);
            cropX2 = (int)Math.Round((decimal)cropX2 * s);
            cropY1 = (int)Math.Round((decimal)cropY1 * s);
            cropY2 = (int)Math.Round((decimal)cropY2 * s);

            resize(originalImage, ratio.Value, cropX1, cropY1, cropX2, cropY2);
            // Return a new MediaProfileEditItem view to swap in for the old one on the ui (js)
            var partialModel = new SubmittedMediaRatioEditItemModel
            {
                Media = _SubmittedMediaEditHelper.Cache,
                RatioConfig = ITOKConfig.Instance.MediaRatios.GetRatioConfig(ratio.Value)
            };
            return PartialView("MediaRatioEditItem", partialModel);
            }

        [CMSAuthorize("UseCMS")]
        public JsonResult UploadSubmittedMediaJson(string qqFile)
        {
            if (uploadSubmittedMedia(qqFile))
                return Json(new JsonSubmittedMediaUploadModel
                {
                    Success = true,
                    Media = _SubmittedMediaEditHelper.Cache,
                    Responses = _responseHelper.UserResponses
                });
            return Json(new JsonSubmittedMediaUploadModel
            {
                Success = false,
               Media = _SubmittedMediaEditHelper.Cache,
                Responses = _responseHelper.UserResponses
            });
        }

        /// <summary>
        /// Returns the profile configuration, in json format, for the specified profile
        /// </summary>
        /// <param name="profileName">The name of the profile to return the configuration for</param>
        /// <returns>The <see cref="MediaProfileElement"/>, in json format, that relates to the specified profile</returns>
        /// <summary>
        /// Crops and resizes the original image and saves to disk. Also marks the SubmittedMedia profile as enabled for the current SubmittedMedia item if it isnt already.
        /// </summary>
        /// <param name="profileName">The name of the SubmittedMedia profile to save for the current SubmittedMedia item</param>
        /// <returns>The partial view SubmittedMedia/MediaProfileEditItem.cshtml</returns>

       

        #endregion Create & Edit

        #region Private Methods

        /// <summary>
        /// Auto crops the current SubmittedMedia item for all ratios and profiles
        /// </summary>
        private void autoCrop()
            {
            var SubmittedMedia = _SubmittedMediaEditHelper.Cache;

            // First try to find the currently selected SubmittedMedia items original file on disk
            var profileConfig = ITOKConfig.Instance.MediaProfiles.GetProfileConfig(MediaProfile.Original);
            var originalImage = S3Utilities.GetImage(profileConfig, SubmittedMedia.FileName);

            // If we can't find it then throw an exceptions saying so. From the UI POV, you shouldnt be able to save an image profile before uploading the original
            if (originalImage == null) throw new Exception("Couldn't find the original image on disk. Please upload the original image before auto-cropping it");

            foreach (MediaRatioElement ratioConfig in ITOKConfig.Instance.MediaRatios)
                {
                var targetWidth = originalImage.Width;
                var targetHeight = originalImage.Height;

                if (((decimal)originalImage.Width / (decimal)originalImage.Height) > ((decimal)ratioConfig.Width / (decimal)ratioConfig.Height))
                    targetWidth = (int)Math.Floor((((decimal)ratioConfig.Width / (decimal)ratioConfig.Height) * originalImage.Height));
                else if (((decimal)originalImage.Width / (decimal)originalImage.Height) <= ((decimal)ratioConfig.Width / (decimal)ratioConfig.Height))
                    targetHeight = (int)Math.Floor((((decimal)ratioConfig.Height / (decimal)ratioConfig.Width) * originalImage.Width));

                // Get the crop coordinates from the target width and height values above
                var cropX1 = (int)Math.Round((((decimal)originalImage.Width - (decimal)targetWidth) / 2), 0, MidpointRounding.AwayFromZero);
                var cropY1 = (int)Math.Round((((decimal)originalImage.Height - (decimal)targetHeight) / 2), 0, MidpointRounding.AwayFromZero);
                var cropX2 = cropX1 + targetWidth;
                var cropY2 = cropY1 + targetHeight;

                crop(originalImage, ratioConfig.RatioName, cropX1, cropY1, cropX2, cropY2);
                }
            }

        private IList<SubmittedMediaRatioEditItemModel> buildMediaRatioEditModel(SubmittedMedia SubmittedMediaItem)
            {
            IList<SubmittedMediaRatioEditItemModel> model = new List<SubmittedMediaRatioEditItemModel>();
            // Build SubmittedMedia Profile view model data
            foreach (MediaRatio ratio in Enum.GetValues(typeof(MediaRatio)))
                {
                var ratioConfig = ITOKConfig.Instance.MediaRatios.GetRatioConfig(ratio);

                model.Add(new SubmittedMediaRatioEditItemModel
                {
                    Media = SubmittedMediaItem,
                    RatioConfig = ratioConfig
                });
                }
            return model;
            }

        /// <summary>
        /// TODO: Remove any files that have been written to disk
        /// </summary>
        /// <returns></returns>
        private ActionResult cancel()
            {
            _SubmittedMediaEditHelper.ClearCache();
            return RedirectToAction("Index");
            }

        private void CapHeightGetWidth(SubmittedMedia SubmittedMedia, ref int width, ref int height)
            {
            //decimal aspectRatio = SubmittedMedia.SourceHeight > SubmittedMedia.SourceWidth ?
            //        ((decimal)SubmittedMedia.SourceHeight / (decimal)SubmittedMedia.SourceWidth) :
            //        ((decimal)SubmittedMedia.SourceWidth / (decimal)SubmittedMedia.SourceHeight);
            var aspectRatio = ((decimal)SubmittedMedia.SourceWidth / (decimal)SubmittedMedia.SourceHeight);
            height = (int)Math.Floor(CropBoxEditHeightCap);
            width = (int)Math.Floor(height * aspectRatio);
            }

        private void CapWidthGetWidth(SubmittedMedia SubmittedMedia, ref int width, ref int height)
            {
            //decimal aspectRatio = SubmittedMedia.SourceHeight < SubmittedMedia.SourceWidth ?
            //        ((decimal)SubmittedMedia.SourceHeight / (decimal)SubmittedMedia.SourceWidth) :
            //        ((decimal)SubmittedMedia.SourceWidth / (decimal)SubmittedMedia.SourceHeight);
            var aspectRatio = ((decimal)SubmittedMedia.SourceHeight / (decimal)SubmittedMedia.SourceWidth);
            width = (int)Math.Floor(CropBoxEditWidthCap);
            height = (int)Math.Floor(width * aspectRatio);
            }

        private void crop(Image originalImage, MediaRatio ratio, int cropX1, int cropY1, int cropX2, int cropY2)
            {
            var SubmittedMedia = _SubmittedMediaEditHelper.Cache;

            // For each profile using the supplied ratio
            var profileConfigs = ITOKConfig.Instance.MediaProfiles.GetProfileConfigsFromRatio(ratio);
            if (profileConfigs.Count == 0) return;
            var largestProfile = profileConfigs.Where(x => x.ProfileName != MediaProfile.Original
                ).OrderByDescending(x => x.Width).FirstOrDefault();

            var profiledSubmittedMedia = MediaUtils.CropImage(originalImage, largestProfile.Width, largestProfile.Height, cropX1, cropX2, false, cropY1, cropY2);

            if (profileConfigs.Count == 0) return;
            foreach (var profileConfig in profileConfigs)
                {
                if (profileConfig.ProfileName == MediaProfile.Original) continue;
                //ImageBuilder.Current.Build(profiledSubmittedMedia, Server.MapPath(MediaHelper.GetSubmittedMediaPath(profileConfig.ProfileName, SubmittedMedia)),
                //    new ResizeSettings(String.Format("maxwidth={0}&format=jpg", profileConfig.Width)), false, true);

                S3Utilities.SaveImage(profiledSubmittedMedia, profileConfig, SubmittedMedia.FileName);//, SubmittedMedia.Title, SubmittedMedia.OriginalFileName);
                }
            // Enable this ratio for the selected SubmittedMedia item if it isnt already
            if (!SubmittedMedia.AvailableRatios.Contains(ratio))
                {
                SubmittedMedia.AvailableRatios.Add(ratio);
                _SubmittedMediaEditHelper.Cache = SubmittedMedia;
                }
            }

        private void resize(Image originalImage, MediaRatio ratio, int cropX1, int cropY1, int cropX2, int cropY2)
            {
            var SubmittedMedia = _SubmittedMediaEditHelper.Cache;

            // For each profile using the supplied ratio
            var profileConfigs = ITOKConfig.Instance.MediaProfiles.GetProfileConfigsFromRatio(ratio);
            if (profileConfigs.Count == 0) return;
            var largestProfile = profileConfigs.Where(x => x.ProfileName != MediaProfile.Original
                ).OrderByDescending(x => x.Width).FirstOrDefault();

            var profiledSubmittedMedia = MediaUtils.ResizeImage(originalImage, largestProfile.Width, largestProfile.Height);

            if (profileConfigs.Count == 0) return;
            foreach (var profileConfig in profileConfigs)
                {
                if (profileConfig.ProfileName == MediaProfile.Original) continue;
                //ImageBuilder.Current.Build(profiledSubmittedMedia, Server.MapPath(MediaHelper.GetSubmittedMediaPath(profileConfig.ProfileName, SubmittedMedia)),
                //    new ResizeSettings(String.Format("maxwidth={0}&format=jpg", profileConfig.Width)), false, true);

                S3Utilities.SaveImageNoResize(profiledSubmittedMedia, profileConfig, SubmittedMedia.FileName);//, SubmittedMedia.Title, SubmittedMedia.OriginalFileName);
                }
            // Enable this ratio for the selected SubmittedMedia item if it isnt already
            if (!SubmittedMedia.AvailableRatios.Contains(ratio))
                {
                SubmittedMedia.AvailableRatios.Add(ratio);
                _SubmittedMediaEditHelper.Cache = SubmittedMedia;
                }
            }

        private int GetCropBoxEditWidth(MediaRatio ratio, SubmittedMedia SubmittedMedia)
            {
            var newWidth = SubmittedMedia.SourceWidth;
            var newHeight = SubmittedMedia.SourceHeight;
            if (SubmittedMedia.SourceWidth > CropBoxEditWidthCap)
                {
                CapWidthGetWidth(SubmittedMedia, ref newWidth, ref newHeight);
                if (newHeight > CropBoxEditHeightCap) CapHeightGetWidth(SubmittedMedia, ref newWidth, ref newHeight);
                }
            else if (newHeight > CropBoxEditHeightCap)
                {
                CapHeightGetWidth(SubmittedMedia, ref newWidth, ref newHeight);
                if (newWidth > CropBoxEditWidthCap) CapWidthGetWidth(SubmittedMedia, ref newWidth, ref newHeight);
                }
            if (newHeight > CropBoxEditHeightCap || newWidth > CropBoxEditWidthCap) throw new Exception("CropBox edit capping failed somehow");
            return newWidth;
            }

        private int GetCropBoxPreviewWidth(MediaRatio ratio)
            {
            var previewWidth = 0;
            var ratioConfig = ITOKConfig.Instance.MediaRatios.GetRatioConfig(ratio);
            if (ratioConfig.Width > ratioConfig.Height)
                {
                previewWidth = (int)Math.Floor(CropBoxEditWidthCap);
                }
            else
                {
                previewWidth = (int)Math.Floor(CropBoxEditHeightCap * ((decimal)ratioConfig.Width / (decimal)ratioConfig.Height));
                }
            if (previewWidth > CropBoxEditWidthCap) throw new Exception("CropBox preview capping failed somehow");
            return previewWidth;
            }

        private SubmittedMediaListModel getSubmittedMediaListModel(int page = -1)
            {
            var model = new SubmittedMediaListModel();

            var showWithStatus = ShowDeletedSubmittedMedia ?
                new[] { SubmissionStatus.Deleted } :
                new[] { SubmissionStatus.InProgress, SubmissionStatus.Submitted };

            model.FilterType = SubmittedMediaFilter;

            if (SubmittedMediaFilter == MediaFilterType.Image ||
                SubmittedMediaFilter == MediaFilterType.Video ||
                SubmittedMediaFilter == MediaFilterType.Audio ||
                SubmittedMediaFilter == MediaFilterType.Document)
                {
                // We're displaying a SubmittedMedia list
                var formatGroup = MediaHelper.FileFormatGroupFromMediaFilterType(SubmittedMediaFilter);
                model.Media = _SubmittedMediaService.GetPagedOrderedByCreatedOn(GlobalVariables.CurrentBrand,
                    DefaultPageSize * (_pageHelper.PaginationModel.CurrentPage - 1),
                   DefaultPageSize,
                    formatGroup,
                    showWithStatus,
                    false,
                    false);

                _pageHelper.SetTotalItemCount(_SubmittedMediaService.GetPagedCount(formatGroup, showWithStatus, ShowDeletedSubmittedMedia));
                model.ShowDeletedMedia = ShowDeletedSubmittedMedia;
                }

            model.Pagination = _pageHelper.PaginationModel;
            model.Responses = _responseHelper.UserResponses;

            // If a search was previously made then reload the results
            if (SearchType != MediaSearchType.None && !string.IsNullOrWhiteSpace(SearchText))
                {
                model.SearchType = SearchType;
                model.SearchText = SearchText;
                model.SearchResults = search(SearchText, 0, SearchCount, SearchType);
                }

            return model;
            }

        /// <summary>
        /// Merge any basic changes that have been made with the cached SubmittedMedia item.
        /// We need to skip out the more complex members because they're managed
        /// independently by methods such as SaveMediaProfile, AddRelatedDiscipline, etc...
        /// </summary>
        /// <param name="changedSubmittedMedia"></param>
        /// <returns></returns>
        private void mergeBasicChanges(SubmittedMedia changedSubmittedMedia)
            {
            var cachedSubmittedMedia = _SubmittedMediaEditHelper.Cache;

            cachedSubmittedMedia = Mapper.Map<SubmittedMedia, SubmittedMedia>(changedSubmittedMedia, cachedSubmittedMedia);
            cachedSubmittedMedia.Hosting = changedSubmittedMedia.Hosting;

            _SubmittedMediaEditHelper.Cache = cachedSubmittedMedia;
            }

        private bool save(SubmissionStatus? SubmissionStatus, string versionComment = null)
            {
            var SubmittedMedia = _SubmittedMediaEditHelper.Cache;

            //hacky, but no way of knowing when it's in brightcove
            if (SubmittedMedia.Hosting == MediaHosting.RemoteBrightCove)
                SubmittedMedia.Format = FileFormat.AVI;

            // Update publication status if requested
            if (SubmissionStatus != null)
                {
                SubmittedMedia.Status = SubmissionStatus.Value;
                _SubmittedMediaEditHelper.Cache = SubmittedMedia;
                }

            try
                {
                // Save the cached SubmittedMedia item
                _SubmittedMediaService.Save(SubmittedMedia, versionComment);
                // Update the cache (mainly so that we can access the new id if it was inserted)
                _SubmittedMediaEditHelper.Cache = SubmittedMedia;
                _responseHelper.AddUserResponse(ResponseType.Notice, string.Format("Successfully saved SubmittedMedia: {0}", SubmittedMedia.Title));
                return true;
                }
            catch (Exception ex)
                {
                _responseHelper.AddUserResponse(ResponseType.Error, "Error", ex.Message);
                return false;
                }
            }

        private ActionResult SaveAudioFile(string filename)
            {
            // Get the SubmittedMedia item we're editing from the cache
            var SubmittedMedia = _SubmittedMediaEditHelper.Cache;

            // Update the cached SubmittedMedia item
            SubmittedMedia.FileName = filename.Replace("audio/", "");
            SubmittedMedia.Format = FileFormat.MP3;

            _responseHelper.AddUserResponse(ResponseType.Notice, "Successfully uploaded SubmittedMedia");
            // Update the SubmittedMedia cache
            _SubmittedMediaEditHelper.Cache = SubmittedMedia;

            return RedirectToAction("Edit");
            }

        private IList<SubmittedMedia> search(string searchText, int skip, int take, MediaSearchType searchType)
            {
            SubmissionStatus[] showWithStatus;
            if (ShowDeletedSubmittedMedia) showWithStatus = new[] { SubmissionStatus.Deleted };
            else showWithStatus = new[] { SubmissionStatus.InProgress, SubmissionStatus.Submitted };

            IList<SubmittedMedia> SubmittedMedia;

            switch (searchType)
                {
                case MediaSearchType.ByTitle:
                    SubmittedMedia = _SubmittedMediaService.Search(GlobalVariables.CurrentBrand, searchText, skip, take, true, false, showWithStatus, false);
                    break;

                default:
                    throw new ArgumentException("Unrecognised MediaSearchType: " + searchType.ToString());
                }

            SearchText = searchText;
            SearchType = searchType;

            return SubmittedMedia;
            }

        private ActionResult traditionalSave(SubmissionStatus? SubmissionStatus, string versionComment)
            {
            var model = new SubmittedMediaEditModel { Media = _SubmittedMediaEditHelper.Cache};

            //enforce brand
            model.Media.Brand = GlobalVariables.CurrentBrand;

            if (!validate())
                {
                model.Responses = _responseHelper.UserResponses;
                return View("Edit", model);
                }

            if (!save(SubmissionStatus, versionComment))
                {
                model.Responses = _responseHelper.UserResponses;
                return View("Edit", model);
                }

            model.Responses = _responseHelper.UserResponses;
            model.MediaRatioEditModels = buildMediaRatioEditModel(model.Media);
            model.VersionHistory = _SubmittedMediaService.GetVersionHistory(_SubmittedMediaEditHelper.Cache, new VersionHydrationSettings
            {
                ModifiedBy = new UserHydrationSettings()
            });
            return View("Edit", model); // Success
            }

        private ActionResult traditionalUploadSubmittedMedia()
            {
            if (uploadSubmittedMedia())
                return RedirectToAction("Edit");
            else
                return RedirectToAction("Edit");
            }

        private bool uploadSubmittedMedia(string originalFilename = null)
            {
            var stream = Request.InputStream;
            byte[] buffer = null;
            try
                {
                if (originalFilename == null)
                    {
                    if (Request.Files[0].ContentLength > MaxFileSize)
                        {
                        _responseHelper.AddUserResponse(ResponseType.Error, "Error", "Maximum file size is 100MB. ");
                        return false;
                        }
                    // IE or traditional form post used with Webkit, Mozilla
                    var postedFile = Request.Files[0];
                    stream = postedFile.InputStream;
                    originalFilename = Path.GetFileName(Request.Files[0].FileName);
                    }
                else
                    {
                    //Webkit, Mozilla and valums file upload was used
                    //...
                    }

                buffer = new byte[stream.Length];
                stream.Read(buffer, 0, buffer.Length);
                }
            catch (Exception ex)
                {
                _responseHelper.AddUserResponse(ResponseType.Error, "Error", "There was a problem getting file data from input stream: " + ex.Message);
                return false;
                }

            // Get the SubmittedMedia item we're editing from the cache
            var SubmittedMedia = _SubmittedMediaEditHelper.Cache;
            SubmittedMedia.OriginalFileName = originalFilename;
            SubmittedMedia.Brand = GlobalVariables.CurrentBrand;
            if (buffer.Length == 0 || string.IsNullOrWhiteSpace(originalFilename))
                {
                _responseHelper.AddUserResponse(ResponseType.Error, "No File specified", "Please select a SubmittedMedia file to upload and then select upload");
                return false;
                }

            // Get configuration for the Original SubmittedMedia profile
            var originalProfileConfig = ITOKConfig.Instance.MediaProfiles.GetProfileConfig(MediaProfile.Original);
            // Check if this SubmittedMedia item already has an original file on disk
            if (!string.IsNullOrWhiteSpace(SubmittedMedia.FileName))
                {
                if (S3Utilities.MediaExists(originalProfileConfig, SubmittedMedia.FileName))
                    S3Utilities.DeleteMedia(originalProfileConfig, SubmittedMedia.FileName);
                }

            // Make sure the SubmittedMedia item is of a recognised format
            if (!FileUtilities.ExtensionIsRecognised(Path.GetExtension(originalFilename)))
                {
                _responseHelper.AddUserResponse(ResponseType.Error, "The extension of the file you uploaded has not been recognised");
                return false;
                }
            // Update the cached SubmittedMedia item
            SubmittedMedia.FileName = Guid.NewGuid().ToString() + Path.GetExtension(originalFilename).ToLower();
            SubmittedMedia.Format = FileUtilities.FormatFromFilename(originalFilename);
            SubmittedMedia.CreatedOn = DateTime.Now;
            try
                {
                // Save to disk
                if (FileUtilities.FormatIsInGroup(SubmittedMedia.Format, FileFormatGroup.Image))
                    {
                    S3Utilities.SaveImage(stream, originalProfileConfig, SubmittedMedia.FileName);//, SubmittedMedia.Title, originalFilename);
                    }
                else if (FileUtilities.FormatIsInGroup(SubmittedMedia.Format, FileFormatGroup.Document))
                    {
                    S3Utilities.SaveDocument(stream, SubmittedMedia.FileName);//, SubmittedMedia.Title, originalFilename);
                    SubmittedMedia.Hosting = MediaHosting.Document;
                    }
                else if (FileUtilities.FormatIsInGroup(SubmittedMedia.Format, FileFormatGroup.Audio))
                    {
                    S3Utilities.SaveAudio(stream, SubmittedMedia.FileName); //, SubmittedMedia.Title, originalFilename);
                    }
                else if (FileUtilities.FormatIsInGroup(SubmittedMedia.Format, FileFormatGroup.Video))
                    {
                    throw new NotImplementedException("Local video uploading is not implemented. Use remote hosting.");
                    }
                }
            catch (Exception ex)
                {
                _responseHelper.AddUserResponse(ResponseType.Error, "Error", ex.Message);
                return false;
                }

            // If the SubmittedMedia is an image then load it from disk so we can access its dimensions
            if (FileUtilities.FormatIsInGroup(SubmittedMedia.Format, FileFormatGroup.Image))
                {
                var originalImage = S3Utilities.GetImage(originalProfileConfig, SubmittedMedia.FileName);
                SubmittedMedia.SourceWidth = originalImage.Width;
                SubmittedMedia.SourceHeight = originalImage.Height;
                }

            _responseHelper.AddUserResponse(ResponseType.Notice, "Successfully uploaded SubmittedMedia");
            // Update the SubmittedMedia cache
            _SubmittedMediaEditHelper.Cache = SubmittedMedia;

            // If the SubmittedMedia is an image then Auto crop for all ratios and profiles
            if (FileUtilities.FormatIsInGroup(SubmittedMedia.Format, FileFormatGroup.Image)) autoCrop();

            return true;
            }

        private bool validate()
            {
            if (!ModelState.IsValid)
                {
                _responseHelper.AddValidationResponses(ModelState);
                return false;
                }

            var SubmittedMedia = _SubmittedMediaEditHelper.Cache;

            if (SubmittedMedia.Hosting == MediaHosting.Local && FileUtilities.FormatIsInGroup(SubmittedMedia.Format, FileFormatGroup.Video))
                {
                _responseHelper.AddUserResponse(ResponseType.Error, string.Format("{0} SubmittedMedia cannot be hosted locally. The SubmittedMedia you upload must be an image.", SubmittedMedia.Format.ToString()));
                return false;
                }
            if (SubmittedMedia.Hosting == MediaHosting.Document && FileUtilities.FormatIsInGroup(SubmittedMedia.Format, FileFormatGroup.Video))
                {
                _responseHelper.AddUserResponse(ResponseType.Error, string.Format("{0} SubmittedMedia cannot be hosted locally. The SubmittedMedia you upload must be a document.", SubmittedMedia.Format.ToString()));
                return false;
                }
            if ((SubmittedMedia.Hosting != MediaHosting.Local && SubmittedMedia.Hosting != MediaHosting.Document) &&
                string.IsNullOrWhiteSpace(SubmittedMedia.RemoteItemCode))
                {
                _responseHelper.AddUserResponse(ResponseType.Error, string.Format("Remote item code is required for remotely hosted SubmittedMedia"));
                return false;
                }

            return true;
            }

        #endregion Private Methods

        #region Private Properties

        private MediaFilterType SubmittedMediaFilter
            {
            get
                {
                if (Session[MediaFilterTypeCacheKey] == null)
                    Session[MediaFilterTypeCacheKey] = MediaFilterType.Image;
                return (MediaFilterType)Session[MediaFilterTypeCacheKey];
                }
            set
                {
                Session[MediaFilterTypeCacheKey] = value;
                // Reset the pagination model everytime the SubmittedMediaFilter is changed so that we start at the beginning again.
                _pageHelper.Reset();
                }
            }

        private int SearchCount
            {
            get
                {
                if (Session[SubmittedMediaSearchCountCacheKey] == null)
                    Session[SubmittedMediaSearchCountCacheKey] = DefaultPageSize;
                return (int)Session[SubmittedMediaSearchCountCacheKey];
                }
            set
                {
                Session[SubmittedMediaSearchCountCacheKey] = value;
                }
            }

        private string SearchText
            {
            get
                {
                if (Session[SubmittedMediaSearchTextCacheKey] == null) return string.Empty;
                return (string)Session[SubmittedMediaSearchTextCacheKey];
                }
            set
                {
                Session[SubmittedMediaSearchTextCacheKey] = value;
                }
            }

        private MediaSearchType SearchType
            {
            get
                {
                if (Session[MediaSearchTypeCacheKey] == null)
                    Session[MediaSearchTypeCacheKey] = MediaSearchType.None;
                return (MediaSearchType)Session[MediaSearchTypeCacheKey];
                }
            set
                {
                Session[MediaSearchTypeCacheKey] = value;
                }
            }

        private bool ShowDeletedSubmittedMedia
            {
            get
                {
                if (Session[ShowDeletedSubmittedMediaCacheKey] == null) return false;
                else return (bool)Session[ShowDeletedSubmittedMediaCacheKey];
                }
            set
                {
                Session[ShowDeletedSubmittedMediaCacheKey] = value;
                }
            }

        #endregion Private Properties
        }
    }