﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using CMS.Helpers;
using CMS.Helpers.Authorise;
using CMS.Models;
using CMS.Models.Constants;
using ITOK.Core.Common;
using ITOK.Core.Common.Config;
using ITOK.Core.Common.Constants;
using ITOK.Core.Common.Utils;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using ITOK.Core.Data.Model.HS;
using ITOK.Core.Services.CMS;
using ITOK.Core.ViewModels;
using ITOK.Core.Services.Security;
using Newtonsoft.Json;

namespace CMS.Controllers
{
    public class MediaBulkController : BaseController
    {
        private const decimal CropBoxEditHeightCap = 400;
        private const decimal CropBoxEditWidthCap = 400;
        private const decimal CropBoxPreviewHeightCap = 400;
        private const decimal CropBoxPreviewWidthCap = 400;
        private const int DefaultPageRange = 10;
        private const int DefaultPageSize = 20;
        private const int DefaultStartPage = 1;
        private const int MaxFileSize = 104857600;
        private const string MediaFilterTypeCacheKey = "MediaFilterTypeCache";
        private const string MediaSearchCountCacheKey = "MediaSearchCountCache";
        private const string MediaSearchTextCacheKey = "MediaSearchTextCache";
        private const string MediaSearchTypeCacheKey = "MediaSearchTypeCache";
        private const string PaginationBaseUrl = "/Media/";
        private const string PaginationInstanceKey = "MediaIndex";
        private const string ShowDeletedMediaCacheKey = "ShowDeletedMediaCache";
        private const string UserResponseInstanceKey = "Media";
        /* 100 MB */

        // Helpers
        private readonly EntityEditHelper<Media> _mediaEditHelper = new EntityEditHelper<Media>();
        private readonly IEntityEditHelperBulk<Media> _mediaEditHelperBulk; // = new EntityEditHelper<Media>();

        private readonly PaginationHelper _pageHelper;
        private readonly UserResponseHelper _responseHelper;

        private IMongoRepository1 mongoRepository;

        private ArticleService _articleService;

        // Services
        private MediaService _mediaService;

        public MediaBulkController()
        {
            mongoRepository = AppKernel.GetInstance<IMongoRepository1>();
            _articleService = new ArticleService(mongoRepository);
            _mediaService = new MediaService(mongoRepository);
            _pageHelper = new PaginationHelper(PaginationInstanceKey, PaginationBaseUrl, DefaultPageSize);
            _responseHelper = new UserResponseHelper(UserResponseInstanceKey);
            _mediaEditHelperBulk = new EntityEditHelperBulk<Media>();
        }

        public MediaBulkController(IMongoRepository1 _mongoRepository, IEntityEditHelperBulk<Media> mediaEditHelperBulk = null)
        {
            this.mongoRepository = _mongoRepository;
            _articleService = new ArticleService(mongoRepository);
            _mediaService = new MediaService(mongoRepository);
            _pageHelper = new PaginationHelper(PaginationInstanceKey, PaginationBaseUrl, DefaultPageSize);
            _responseHelper = new UserResponseHelper(UserResponseInstanceKey);
            if (mediaEditHelperBulk == null)
            {
                mediaEditHelperBulk = new EntityEditHelperBulk<Media>();
            }
            _mediaEditHelperBulk = mediaEditHelperBulk;
        }

        #region Index & Page

        [CMSAuthorize("UseCMS")]
        public void CancelSearch()
        {
            SearchText = string.Empty;
            SearchCount = 0;
            SearchType = MediaSearchType.None;
        }

        [CMSAuthorize("UseCMS")]
        public ActionResult Filter(MediaFilterType type)
        {
            MediaFilter = type;
            return RedirectToAction("Index");
        }

        [CMSAuthorize("UseCMS")]
        [HttpPost]
        public JsonResult GetTotalSearchResults(string searchText, MediaSearchType searchType)
        {
            PublicationStatus[] showWithStatus;
            if (ShowDeletedMedia) showWithStatus = new[] { PublicationStatus.Deleted };
            else showWithStatus = new[] { PublicationStatus.Published, PublicationStatus.UnPublished };

            var resultCount = 0;

            switch (searchType)
            {
                case MediaSearchType.ByTitle:
                    resultCount = _mediaService.SearchCount(searchText, true, false, showWithStatus, false);
                    break;

                default:
                    throw new ArgumentException("Unrecognised MediaSearchType: " + searchType.ToString());
            }
            return Json(resultCount);
        }

        [CMSAuthorize("UseCMS")]
        public ActionResult Index()
        {
            return Page();
        }

        [CMSAuthorize("UseCMS")]
        public ActionResult Page(int pageNumber = -1, bool? showDeletedMedia = null)
        {
            if (showDeletedMedia != null) ShowDeletedMedia = (bool)showDeletedMedia;

            // Save any changes the user has made to the media item
            if (pageNumber != -1) _pageHelper.SetPage(pageNumber);

            return View("Index", getMediaListModel());
        }

        [CMSAuthorize("UseCMS")]
        public PartialViewResult Search(string searchText, int skip, MediaSearchType searchType)
        {
            SearchCount = skip + DefaultPageSize;
            return PartialView("MediaList", search(searchText, skip, DefaultPageSize, searchType));
        }

        #endregion Index & Page

        #region Create & Edit

        [CMSAuthorize("UseCMS")]
        public JsonResult CancelJson()
        {
            _mediaEditHelper.ClearCache();
            _mediaEditHelperBulk.ClearCache();
            return Json(new { success = true });
        }

        [CMSAuthorize("UseCMS")]
        public ActionResult ChangeStatus(string id, PublicationStatus? status, int? inDays)
        {
            if (string.IsNullOrWhiteSpace(id)) throw new ArgumentNullException("id", "Id must be supplied to the ChangeStatus action method");
            var media = _mediaService.FindByIds(null, id).FirstOrDefault();
            if (status != null) media.Status = status.Value;
            if (inDays != null)
            {
                media.LiveFrom = DateTime.Today.AddDays(inDays.Value);
                media.LiveTo = null;
            }
            _mediaService.Save(media);
            return RedirectToAction("Index");
        }

        [CMSAuthorize("UseCMS")]
        public ActionResult Create(int relatedEventId = 0, string relatedDisciplineNameSlug = null)
        {
            if (Request.QueryString["bucket"] != null)
                return SaveAudioFile(Request.QueryString["key"]);

            _mediaEditHelper.ClearCache();
            var model = new MediaEditModel
            {
                Media = _mediaEditHelper.Cache
            };
            model.Media.ShowInMedia = true;
            model.Media.LiveFrom = DateTime.UtcNow;

            return View("Edit", model);
        }

        [CMSAuthorize("UseCMS")]
        [HttpGet]
        public JsonResult CreateJson()
        {
            _mediaEditHelper.ClearCache();
            _mediaEditHelperBulk.ClearCache();
            return Json(_mediaEditHelper.Cache, JsonRequestBehavior.AllowGet);
        }

        [CMSAuthorize("UseCMS")]
        public ActionResult Delete(string id, FormCollection collection)
        {
            try
            {
                // Get the media to delete
                var media = _mediaService.FindByIds(null, id).FirstOrDefault();
                if (media == null)
                {
                    _responseHelper.AddUserResponse(ResponseType.Error, "Could not find the specified media to delete");
                    return RedirectToAction("Index");
                }

                // Not working. Temporarily using save instead TODO: Use the DeleteById service method
                //_mediaService.Delete(media.UrlSlug);
                media.Status = PublicationStatus.Deleted;
                _mediaService.Save(media);

                // Clear the media cache
                _mediaEditHelper.ClearCache();
                _responseHelper.AddUserResponse(ResponseType.Notice, string.Format("Successfully deleted media: {0}", media.Title));
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                _responseHelper.AddUserResponse(ResponseType.Error, "Error", ex.Message);
                return RedirectToAction("Index");
            }
        }

        /// <summary>
        /// Disables a media profile for the current media item and tries to remove the image from disk
        /// </summary>
        /// <param name="profileName">The name of the media profile to remove</param>
        /// <returns>The partial view Media/MediaProfileEditItem.cshtml</returns>
        [CMSAuthorize("UseCMS")]
        public PartialViewResult DisableMediaRatio(MediaRatio? ratio, string id)
        {
            if (ratio == null) throw new ArgumentException("a MediaRatio to disable must be specified", "ratio");
            // Try to get the media item
            var media = _mediaEditHelper.Cache;
            // Check that the media item has the ratio available to start with
            if (media.AvailableRatios.Contains(ratio.Value))
            {
                // Get all profiles for the given ratio
                foreach (var profileConfig in ITOKConfig.Instance.MediaProfiles.GetProfileConfigsFromRatio(ratio.Value))
                {
                    if (profileConfig.ProfileName == MediaProfile.Original) continue;
                    if (S3Utilities.MediaExists(profileConfig, media.FileName))
                        S3Utilities.DeleteMedia(profileConfig, media.FileName);
                }
                // Remove the ratio from the media items available ratios
                media.AvailableRatios.Remove(ratio.Value);
                _mediaEditHelper.Cache = media;
            }
            // Return a new MediaProfileEditItem view to swap in for the old one on the ui (js)
            var partialModel = new MediaRatioEditItemModel
            {
                Media = media,
                RatioConfig = ITOKConfig.Instance.MediaRatios.GetRatioConfig(ratio.Value)
            };

            return PartialView("MediaRatioEditItem", partialModel);
        }

        [CMSAuthorize("UseCMS")]
        public ActionResult Edit(string id)
        {
            var model = new MediaEditModel();

            if (string.IsNullOrEmpty(id) || _mediaEditHelper.Cache.Id == id)
            {
                model.Media = _mediaEditHelper.Cache;
            }
            else
            {
                // Find the media to edit
                model.Media = _mediaService.FindByIds(new MediaHydrationSettings
                {
                    RelatedArticles = new ArticleHydrationSettings
                    {
                        PrimaryMedia = new MediaHydrationSettings(),
                        RelatedMedia = new MediaHydrationSettings()
                    },

                    CreatedBy = new UserHydrationSettings(),
                    UpdatedBy = new UserHydrationSettings()
                }, id).FirstOrDefault();
                if (model.Media == null)
                {
                    _responseHelper.AddUserResponse(ResponseType.Error, "Could not find the specified media to edit.");
                    return View("Index", getMediaListModel());
                }
            }

            model.VersionHistory = _mediaService.GetVersionHistory(model.Media.Id, new VersionHydrationSettings
            {
                ModifiedBy = new UserHydrationSettings()
            });
            model.MediaRatioEditModels = buildMediaRatioEditModel(model.Media);
            model.Responses = _responseHelper.UserResponses;
            _mediaEditHelper.Cache = model.Media;
            return View("Edit", model);
        }

        [CMSAuthorize("UseCMS")]
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult EditAction(Media media, string submit, FormCollection formCollection, string versionComment)
        {
            mergeBasicChanges(media);
            switch (submit)
            {
                case "Upload Media": return traditionalUploadMedia();
                case "Save": return traditionalSave(null, versionComment);
                case "Publish": return traditionalSave(PublicationStatus.Published, versionComment);
                case "Un-Publish": return traditionalSave(PublicationStatus.UnPublished, versionComment);
                case "Cancel": return cancel();
                default: return traditionalUploadMedia();
                    //default: throw new Exception("Unrecognised edit action");
            }
        }

        [CMSAuthorize("UseCMS")]
        public PartialViewResult EditMediaRatio(MediaRatio? ratio)
        {
            if (ratio == null) throw new ArgumentException("a MediaRatio to edit must be specified", "ratio");
            // Try to get the media item
            var media = _mediaEditHelper.Cache;

            var ratioConfig = ITOKConfig.Instance.MediaRatios.GetRatioConfig(ratio.Value);

            var partialModel = new MediaRatioEditPanelModel
            {
                Media = media,
                CropBoxEditWidth = GetCropBoxEditWidth(ratio.Value, media),
                CropBoxPreviewWidth = GetCropBoxPreviewWidth(ratio.Value),
                RatioConfig = ratioConfig
            };
            return PartialView("MediaRatioEditPanel", partialModel);
        }

        //[CMSAuthorize("UseCMS & EditMedia")]
        //public ActionResult Feature(string id, int position)
        //    {
        //    _mediaService.InsertNewFeatured(id, FeaturedArea.Media, position);
        //    return RedirectToAction("Index");
        //    }

        [CMSAuthorize("UseCMS")]
        public JsonResult GetProfileConfig(string profileName)
        {
            var profile = MediaUtils.MediaProfileFromName(profileName);
            var profileConfig = ITOKConfig.Instance.MediaProfiles.GetProfileConfig(profile);

            return Json(profileConfig, JsonRequestBehavior.AllowGet);
        }

        [CMSAuthorize("UseCMS")]
        public ActionResult HardDelete(string id)
        {
            // Get the media to delete
            var media = _mediaService.FindByIds(null, id).FirstOrDefault();
            if (media == null)
            {
                _responseHelper.AddUserResponse(ResponseType.Error, "Could not find the specified media to delete");
                return RedirectToAction("Index");
            }

            _mediaService.DeleteById(id, false);

            // Clear the media cache
            _mediaEditHelper.ClearCache();
            _responseHelper.AddUserResponse(ResponseType.Notice, string.Format("Successfully deleted media: {0}", media.Title));
            return RedirectToAction("Index");
        }

        [CMSAuthorize("UseCMS")]
        public ActionResult Restore(string id)
        {
            // Get the media to delete
            var media = _mediaService.FindByIds(null, id).FirstOrDefault();
            if (media == null)
            {
                _responseHelper.AddUserResponse(ResponseType.Error, "Could not find the specified media to restore");
                return RedirectToAction("Index");
            }

            // Not working. Temporarily using save instead
            //_mediaService.Delete(media.UrlSlug);
            media.Status = PublicationStatus.UnPublished;
            _mediaService.Save(media);

            // Clear the media cache
            _mediaEditHelper.ClearCache();
            _responseHelper.AddUserResponse(ResponseType.Notice, string.Format("Successfully restored media: {0}", media.Title));
            return RedirectToAction("Index");
        }

        [CMSAuthorize("UseCMS")]
        public ActionResult RevertToVersion(string versionId)
        {
            if (string.IsNullOrEmpty(versionId)) throw new ArgumentException("Version Id is required to perform this action");
            var version = _mediaService.GetVersion(versionId, null);
            if (version == null)
            {
                _responseHelper.AddUserResponse(ResponseType.Notice, "Couldn't find that version", "If this persists, contact the iaafsupport@haymarket.com");
                return RedirectToAction("Edit");
            }
            var mediaVersion = _mediaService.GetEntityVersion(versionId);
            try
            {
                _mediaService.RevertToVersion(versionId);
                _responseHelper.AddUserResponse(ResponseType.Notice, "Successfully reverted media", string.Format("To the version backed-up on {0} at {1}",
                    version.ModifiedOn.ToString("dd MMMM"),
                    version.ModifiedOn.ToString("H:mm")));
                _mediaEditHelper.ClearCache();
            }
            catch (Exception ex)
            {
                _responseHelper.AddUserResponse(ResponseType.Notice, "Error reverting media", string.Format("To the version of the {0}. Error message: {1}",
                    version.ModifiedOn.ToString("dd MMMM"),
                    ex.Message));
            }

            return RedirectToAction("Edit", new { id = mediaVersion.Id });
        }

        [CMSAuthorize("UseCMS")]
        [HttpPost]
        public JsonResult SaveJson(Media media)
        {
            mergeBasicChanges(media);
            if (!validate())
            {
                var model = new JsonMediaSaveModel
                {
                    Responses = _responseHelper.UserResponses,
                    Success = false
                };
                return Json(model);
            }
            if (!SaveBulk(PublicationStatus.Published, media.Title))
            {
                var model = new JsonMediaSaveModel
                {
                    Responses = _responseHelper.UserResponses,
                    Success = false
                };
                return Json(model);
            }
            else
            {
                //Get media ids from cache
                var _mediaCache = _mediaEditHelperBulk.GetCache();
                string _mediaIds = string.Join(",", _mediaCache.Select(x=>x.Id));
                

                var model = new JsonMediaSaveModel
                {
                    Responses = _responseHelper.UserResponses,
                    Success = true,
                    //MediaId = _mediaEditHelper.Cache.Id  //commented by Santosh
                    MediaId=_mediaIds

                };
                // Clear the cache
                _mediaEditHelperBulk.ClearCache();
                _mediaEditHelper.ClearCache();
                return Json(model);
            }
        }

        /// <summary>
        /// TODO: Remove any files that have been written to disk
        /// </summary>
        /// <returns></returns>
        /// <summary>
        /// For use when ajax uploading is required
        /// </summary>
        /// <returns></returns>

        [CMSAuthorize("UseCMS")]
        public PartialViewResult SaveMediaRatio(MediaRatio? ratio, int cropX1, int cropY1, int cropX2, int cropY2)
        {
            if (ratio == null) throw new ArgumentException("a MediaRatio to save must be specified", "ratio");

            // First try to find the currently selected media items original file on disk
            //Image originalImage = Image.FromFile(Server.MapPath(MediaHelper.GetMediaPath(MediaProfile.Original, _mediaEditHelper.Cache)));

            var originalProfileConfig = ITOKConfig.Instance.MediaProfiles.GetProfileConfig(MediaProfile.Original);
            var originalImage = S3Utilities.GetImage(originalProfileConfig, _mediaEditHelper.Cache.FileName);

            /* Quite important to make sure that the ratio is added to the media item cache here because otherwise,
             * when we come to crop (and save) below, the default media path will be resolved and the default image
             * will be overwritten.
             */
            var media = _mediaEditHelper.Cache;
            if (!media.AvailableRatios.Contains(ratio.Value))
            {
                media.AvailableRatios.Add(ratio.Value);
                _mediaEditHelper.Cache = media;
            }

            // Scale the coordinates for the original image size against the size of the client crop box
            var s = (decimal)originalImage.Width / (decimal)GetCropBoxEditWidth(ratio.Value, media);
            cropX1 = (int)Math.Round((decimal)cropX1 * s);
            cropX2 = (int)Math.Round((decimal)cropX2 * s);
            cropY1 = (int)Math.Round((decimal)cropY1 * s);
            cropY2 = (int)Math.Round((decimal)cropY2 * s);

            crop(originalImage, ratio.Value, cropX1, cropY1, cropX2, cropY2);
            // Return a new MediaProfileEditItem view to swap in for the old one on the ui (js)
            var partialModel = new MediaRatioEditItemModel
            {
                Media = _mediaEditHelper.Cache,
                RatioConfig = ITOKConfig.Instance.MediaRatios.GetRatioConfig(ratio.Value)
            };
            return PartialView("MediaRatioEditItem", partialModel);
        }

        [CMSAuthorize("UseCMS")]
        public PartialViewResult SaveMediaResize(MediaRatio? ratio, int cropX1, int cropY1, int cropX2, int cropY2)
        {
            if (ratio == null) throw new ArgumentException("a MediaRatio to save must be specified", "ratio");

            // First try to find the currently selected media items original file on disk
            //Image originalImage = Image.FromFile(Server.MapPath(MediaHelper.GetMediaPath(MediaProfile.Original, _mediaEditHelper.Cache)));

            var originalProfileConfig = ITOKConfig.Instance.MediaProfiles.GetProfileConfig(MediaProfile.Original);
            var originalImage = S3Utilities.GetImage(originalProfileConfig, _mediaEditHelper.Cache.FileName);

            /* Quite important to make sure that the ratio is added to the media item cache here because otherwise,
             * when we come to crop (and save) below, the default media path will be resolved and the default image
             * will be overwritten.
             */
            var media = _mediaEditHelper.Cache;
            if (!media.AvailableRatios.Contains(ratio.Value))
            {
                media.AvailableRatios.Add(ratio.Value);
                _mediaEditHelper.Cache = media;
            }

            // Scale the coordinates for the original image size against the size of the client crop box
            var s = (decimal)originalImage.Width / (decimal)GetCropBoxEditWidth(ratio.Value, media);
            cropX1 = (int)Math.Round((decimal)cropX1 * s);
            cropX2 = (int)Math.Round((decimal)cropX2 * s);
            cropY1 = (int)Math.Round((decimal)cropY1 * s);
            cropY2 = (int)Math.Round((decimal)cropY2 * s);

            resize(originalImage, ratio.Value, cropX1, cropY1, cropX2, cropY2);
            // Return a new MediaProfileEditItem view to swap in for the old one on the ui (js)
            var partialModel = new MediaRatioEditItemModel
            {
                Media = _mediaEditHelper.Cache,
                RatioConfig = ITOKConfig.Instance.MediaRatios.GetRatioConfig(ratio.Value)
            };
            return PartialView("MediaRatioEditItem", partialModel);
        }

        [CMSAuthorize("UseCMS")]
        public JsonResult UploadMediaJson(string qqFile)
        {

            var medissss= _mediaEditHelperBulk.GetCache();
           // _mediaEditHelperBulk.ClearCache();
            if (uploadMedia(qqFile))
                return Json(new JsonMediaUploadModel
                {
                    Success = true,
                    Media = _mediaEditHelper.Cache,
                    Responses = _responseHelper.UserResponses
                });
            return Json(new JsonMediaUploadModel
            {
                Success = false,
                Media = _mediaEditHelper.Cache,
                Responses = _responseHelper.UserResponses
            });
        }

        /// <summary>
        /// Returns the profile configuration, in json format, for the specified profile
        /// </summary>
        /// <param name="profileName">The name of the profile to return the configuration for</param>
        /// <returns>The <see cref="MediaProfileElement"/>, in json format, that relates to the specified profile</returns>
        /// <summary>
        /// Crops and resizes the original image and saves to disk. Also marks the media profile as enabled for the current media item if it isnt already.
        /// </summary>
        /// <param name="profileName">The name of the media profile to save for the current media item</param>
        /// <returns>The partial view Media/MediaProfileEditItem.cshtml</returns>

        #region Tags

        [CMSAuthorize("UseCMS")]
        public PartialViewResult AddRelatedArticle(string id)
        {
            var media = _mediaEditHelper.Cache;
            // Only add the related article if it isnt already tagged
            if (media.RelatedArticles.All(x => x.Id != id))
            {
                var relatedArticle = _articleService.FindByIds(GlobalVariables.CurrentBrand, new ArticleHydrationSettings
                {
                    PrimaryMedia = new MediaHydrationSettings(),
                    RelatedMedia = new MediaHydrationSettings()
                }, id).SingleOrDefault();
                media.RelatedArticles.Add(relatedArticle);
                _mediaEditHelper.Cache = media;
                return PartialView("EditArticleItem", relatedArticle);
            }
            return null;
        }

        /// <summary>

        // RelatedArticles
        [CMSAuthorize("UseCMS")]
        public PartialViewResult ListRelatedArticles()
        {
            var media = _mediaEditHelper.Cache;
            return PartialView("EditArticles", media.RelatedArticles);
        }

        // RelatedCompetitions

        [CMSAuthorize("UseCMS")]
        public void RemoveRelatedArticle(string id)
        {
            var media = _mediaEditHelper.Cache;
            var relatedArticleToRemove = media.RelatedArticles.SingleOrDefault(x => x.Id == id.ToLower());
            if (relatedArticleToRemove != null)
            {
                media.RelatedArticles.Remove(relatedArticleToRemove);
                _mediaEditHelper.Cache = media;
            }
        }

        #endregion Tags

        #endregion Create & Edit

        #region Private Methods

        /// <summary>
        /// Auto crops the current media item for all ratios and profiles
        /// </summary>
        private void autoCrop()
        {
            var media = _mediaEditHelper.Cache;

            // First try to find the currently selected media items original file on disk
            var profileConfig = ITOKConfig.Instance.MediaProfiles.GetProfileConfig(MediaProfile.Original);
            var originalImage = S3Utilities.GetImage(profileConfig, media.FileName);

            // If we can't find it then throw an exceptions saying so. From the UI POV, you shouldnt be able to save an image profile before uploading the original
            if (originalImage == null) throw new Exception("Couldn't find the original image on disk. Please upload the original image before auto-cropping it");

            foreach (MediaRatioElement ratioConfig in ITOKConfig.Instance.MediaRatios)
            {
                var targetWidth = originalImage.Width;
                var targetHeight = originalImage.Height;

                if (((decimal)originalImage.Width / (decimal)originalImage.Height) > ((decimal)ratioConfig.Width / (decimal)ratioConfig.Height))
                    targetWidth = (int)Math.Floor((((decimal)ratioConfig.Width / (decimal)ratioConfig.Height) * originalImage.Height));
                else if (((decimal)originalImage.Width / (decimal)originalImage.Height) <= ((decimal)ratioConfig.Width / (decimal)ratioConfig.Height))
                    targetHeight = (int)Math.Floor((((decimal)ratioConfig.Height / (decimal)ratioConfig.Width) * originalImage.Width));

                // Get the crop coordinates from the target width and height values above
                var cropX1 = (int)Math.Round((((decimal)originalImage.Width - (decimal)targetWidth) / 2), 0, MidpointRounding.AwayFromZero);
                var cropY1 = (int)Math.Round((((decimal)originalImage.Height - (decimal)targetHeight) / 2), 0, MidpointRounding.AwayFromZero);
                var cropX2 = cropX1 + targetWidth;
                var cropY2 = cropY1 + targetHeight;

                crop(originalImage, ratioConfig.RatioName, cropX1, cropY1, cropX2, cropY2);
            }
        }

        private IList<MediaRatioEditItemModel> buildMediaRatioEditModel(Media mediaItem)
        {
            IList<MediaRatioEditItemModel> model = new List<MediaRatioEditItemModel>();
            // Build Media Profile view model data
            foreach (MediaRatio ratio in Enum.GetValues(typeof(MediaRatio)))
            {
                var ratioConfig = ITOKConfig.Instance.MediaRatios.GetRatioConfig(ratio);

                model.Add(new MediaRatioEditItemModel
                {
                    Media = mediaItem,
                    RatioConfig = ratioConfig
                });
            }
            return model;
        }

        /// <summary>
        /// TODO: Remove any files that have been written to disk
        /// </summary>
        /// <returns></returns>
        private ActionResult cancel()
        {
            _mediaEditHelper.ClearCache();
            return RedirectToAction("Index");
        }

        private void CapHeightGetWidth(Media media, ref int width, ref int height)
        {
            //decimal aspectRatio = media.SourceHeight > media.SourceWidth ?
            //        ((decimal)media.SourceHeight / (decimal)media.SourceWidth) :
            //        ((decimal)media.SourceWidth / (decimal)media.SourceHeight);
            var aspectRatio = ((decimal)media.SourceWidth / (decimal)media.SourceHeight);
            height = (int)Math.Floor(CropBoxEditHeightCap);
            width = (int)Math.Floor(height * aspectRatio);
        }

        private void CapWidthGetWidth(Media media, ref int width, ref int height)
        {
            //decimal aspectRatio = media.SourceHeight < media.SourceWidth ?
            //        ((decimal)media.SourceHeight / (decimal)media.SourceWidth) :
            //        ((decimal)media.SourceWidth / (decimal)media.SourceHeight);
            var aspectRatio = ((decimal)media.SourceHeight / (decimal)media.SourceWidth);
            width = (int)Math.Floor(CropBoxEditWidthCap);
            height = (int)Math.Floor(width * aspectRatio);
        }

        private void crop(Image originalImage, MediaRatio ratio, int cropX1, int cropY1, int cropX2, int cropY2)
        {
            var media = _mediaEditHelper.Cache;

            // For each profile using the supplied ratio
            var profileConfigs = ITOKConfig.Instance.MediaProfiles.GetProfileConfigsFromRatio(ratio);
            if (profileConfigs.Count == 0) return;
            var largestProfile = profileConfigs.Where(x => x.ProfileName != MediaProfile.Original
                ).OrderByDescending(x => x.Width).FirstOrDefault();

            var profiledMedia = MediaUtils.CropImage(originalImage, largestProfile.Width, largestProfile.Height, cropX1, cropX2, false, cropY1, cropY2);

            if (profileConfigs.Count == 0) return;
            foreach (var profileConfig in profileConfigs)
            {
                if (profileConfig.ProfileName == MediaProfile.Original) continue;
                //ImageBuilder.Current.Build(profiledMedia, Server.MapPath(MediaHelper.GetMediaPath(profileConfig.ProfileName, media)),
                //    new ResizeSettings(String.Format("maxwidth={0}&format=jpg", profileConfig.Width)), false, true);

                S3Utilities.SaveImage(profiledMedia, profileConfig, media.FileName);//, media.Title, media.OriginalFileName);
            }
            // Enable this ratio for the selected media item if it isnt already
            if (!media.AvailableRatios.Contains(ratio))
            {
                media.AvailableRatios.Add(ratio);
                _mediaEditHelper.Cache = media;
            }
        }

        private void resize(Image originalImage, MediaRatio ratio, int cropX1, int cropY1, int cropX2, int cropY2)
        {
            var media = _mediaEditHelper.Cache;

            // For each profile using the supplied ratio
            var profileConfigs = ITOKConfig.Instance.MediaProfiles.GetProfileConfigsFromRatio(ratio);
            if (profileConfigs.Count == 0) return;
            var largestProfile = profileConfigs.Where(x => x.ProfileName != MediaProfile.Original
                ).OrderByDescending(x => x.Width).FirstOrDefault();

            var profiledMedia = MediaUtils.ResizeImage(originalImage, largestProfile.Width, largestProfile.Height);

            if (profileConfigs.Count == 0) return;
            foreach (var profileConfig in profileConfigs)
            {
                if (profileConfig.ProfileName == MediaProfile.Original) continue;
                //ImageBuilder.Current.Build(profiledMedia, Server.MapPath(MediaHelper.GetMediaPath(profileConfig.ProfileName, media)),
                //    new ResizeSettings(String.Format("maxwidth={0}&format=jpg", profileConfig.Width)), false, true);

                S3Utilities.SaveImageNoResize(profiledMedia, profileConfig, media.FileName);//, media.Title, media.OriginalFileName);
            }
            // Enable this ratio for the selected media item if it isnt already
            if (!media.AvailableRatios.Contains(ratio))
            {
                media.AvailableRatios.Add(ratio);
                _mediaEditHelper.Cache = media;
            }
        }

        private int GetCropBoxEditWidth(MediaRatio ratio, Media media)
        {
            var newWidth = media.SourceWidth;
            var newHeight = media.SourceHeight;
            if (media.SourceWidth > CropBoxEditWidthCap)
            {
                CapWidthGetWidth(media, ref newWidth, ref newHeight);
                if (newHeight > CropBoxEditHeightCap) CapHeightGetWidth(media, ref newWidth, ref newHeight);
            }
            else if (newHeight > CropBoxEditHeightCap)
            {
                CapHeightGetWidth(media, ref newWidth, ref newHeight);
                if (newWidth > CropBoxEditWidthCap) CapWidthGetWidth(media, ref newWidth, ref newHeight);
            }
            if (newHeight > CropBoxEditHeightCap || newWidth > CropBoxEditWidthCap) throw new Exception("CropBox edit capping failed somehow");
            return newWidth;
        }

        private int GetCropBoxPreviewWidth(MediaRatio ratio)
        {
            var previewWidth = 0;
            var ratioConfig = ITOKConfig.Instance.MediaRatios.GetRatioConfig(ratio);
            if (ratioConfig.Width > ratioConfig.Height)
            {
                previewWidth = (int)Math.Floor(CropBoxEditWidthCap);
            }
            else
            {
                previewWidth = (int)Math.Floor(CropBoxEditHeightCap * ((decimal)ratioConfig.Width / (decimal)ratioConfig.Height));
            }
            if (previewWidth > CropBoxEditWidthCap) throw new Exception("CropBox preview capping failed somehow");
            return previewWidth;
        }

        private MediaListModel getMediaListModel(int page = -1)
        {
            var model = new MediaListModel();

            var showWithStatus = ShowDeletedMedia ?
                new[] { PublicationStatus.Deleted } :
                new[] { PublicationStatus.Published, PublicationStatus.UnPublished };

            model.FilterType = MediaFilter;

            if (MediaFilter == MediaFilterType.Image ||
                MediaFilter == MediaFilterType.Video ||
                MediaFilter == MediaFilterType.Audio ||
                MediaFilter == MediaFilterType.Document)
            {
                // We're displaying a media list
                var formatGroup = MediaHelper.FileFormatGroupFromMediaFilterType(MediaFilter);
                model.Media = _mediaService.GetPagedOrderedByCreatedOn(GlobalVariables.CurrentBrand,
                    DefaultPageSize * (_pageHelper.PaginationModel.CurrentPage - 1),
                   DefaultPageSize,
                    formatGroup,
                    showWithStatus,
                    false,
                    false);

                _pageHelper.SetTotalItemCount(_mediaService.GetPagedCount(formatGroup, showWithStatus, ShowDeletedMedia));
                model.ShowDeletedMedia = ShowDeletedMedia;
            }

            model.Pagination = _pageHelper.PaginationModel;
            model.Responses = _responseHelper.UserResponses;

            // If a search was previously made then reload the results
            if (SearchType != MediaSearchType.None && !string.IsNullOrWhiteSpace(SearchText))
            {
                model.SearchType = SearchType;
                model.SearchText = SearchText;
                model.SearchResults = search(SearchText, 0, SearchCount, SearchType);
            }

            return model;
        }

        /// <summary>
        /// Merge any basic changes that have been made with the cached media item.
        /// We need to skip out the more complex members because they're managed
        /// independently by methods such as SaveMediaProfile, AddRelatedDiscipline, etc...
        /// </summary>
        /// <param name="changedMedia"></param>
        /// <returns></returns>
        private void mergeBasicChanges(Media changedMedia)
        {
            List<Media> lstMedia = new List<Media>();
            lstMedia= _mediaEditHelperBulk.GetCache();
            var cachedMedia = _mediaEditHelper.Cache;

            cachedMedia = Mapper.Map<Media, Media>(changedMedia, cachedMedia);
            cachedMedia.Hosting = changedMedia.Hosting;

            _mediaEditHelper.Cache = cachedMedia;
            //lstMedia.Add(cachedMedia);
            //_mediaEditHelperBulk.SetCache(lstMedia);
        }

        private bool save(PublicationStatus? publicationStatus, string versionComment = null)
        {
            var media = _mediaEditHelper.Cache;

            //hacky, but no way of knowing when it's in brightcove
            if (media.Hosting == MediaHosting.RemoteBrightCove || media.Hosting == MediaHosting.RemoteYoutube)
                media.Format = FileFormat.AVI;

            // Update publication status if requested
            if (publicationStatus != null)
            {
                media.Status = publicationStatus.Value;
                _mediaEditHelper.Cache = media;
            }

            try
            {
                // Save the cached media item
                _mediaService.Save(media, versionComment);
                // Update the cache (mainly so that we can access the new id if it was inserted)
                _mediaEditHelper.Cache = media;
                _responseHelper.AddUserResponse(ResponseType.Notice, string.Format("Successfully saved media: {0}", media.Title));
                return true;
            }
            catch (Exception ex)
            {
                _responseHelper.AddUserResponse(ResponseType.Error, "Error", ex.Message);
                return false;
            }
        }

        private bool SaveBulk(PublicationStatus? publicationStatus, string title, string versionComment = null)
        {
            var medialst = _mediaEditHelperBulk.GetCache();
            try
            {
                foreach (var media in medialst)
                {
                    media.Title = title;
                //hacky, but no way of knowing when it's in brightcove
                if (media.Hosting == MediaHosting.RemoteBrightCove)
                    media.Format = FileFormat.AVI;

                if (string.IsNullOrEmpty(media.Title))
                {
                    media.Title = media.OriginalFileName.Substring(0, media.OriginalFileName.LastIndexOf('.'));
                }
                if (string.IsNullOrEmpty(media.SEOTitle))
                {
                    media.SEOTitle = media.OriginalFileName.Substring(0, media.OriginalFileName.LastIndexOf('.'));
                }
                if (media.Hosting == MediaHosting.RemoteYoutube && !string.IsNullOrEmpty(media.RemoteItemCode))
                    media.Format = FileFormat.MP4;
                // Update publication status if requested
                if (publicationStatus != null)
                {
                    media.Status = publicationStatus.Value;
                    _mediaEditHelper.Cache=media;
                }
                    // Save the cached media item
                    _mediaService.Save(media, versionComment);
                    // Update the cache (mainly so that we can access the new id if it was inserted)
                    _mediaEditHelper.Cache=media;
            }
                _mediaEditHelperBulk.ClearCache();
                _mediaEditHelperBulk.SetCache(medialst);
                return true;
            }
            catch (Exception ex)
            {
                _responseHelper.AddUserResponse(ResponseType.Error, "Error", ex.Message);
                return false;
            }


        }

        private ActionResult SaveAudioFile(string filename)
        {
            // Get the media item we're editing from the cache
            var media = _mediaEditHelper.Cache;

            // Update the cached media item
            media.FileName = filename.Replace("audio/", "");
            media.Format = FileFormat.MP3;

            _responseHelper.AddUserResponse(ResponseType.Notice, "Successfully uploaded media");
            // Update the media cache
            _mediaEditHelper.Cache = media;

            return RedirectToAction("Edit");
        }

        private IList<Media> search(string searchText, int skip, int take, MediaSearchType searchType)
        {
            PublicationStatus[] showWithStatus;
            if (ShowDeletedMedia) showWithStatus = new[] { PublicationStatus.Deleted };
            else showWithStatus = new[] { PublicationStatus.Published, PublicationStatus.UnPublished };

            IList<Media> media;

            switch (searchType)
            {
                case MediaSearchType.ByTitle:
                    media = _mediaService.Search(GlobalVariables.CurrentBrand, searchText, skip, take, true, false, showWithStatus, false);
                    break;

                default:
                    throw new ArgumentException("Unrecognised MediaSearchType: " + searchType.ToString());
            }

            SearchText = searchText;
            SearchType = searchType;

            return media;
        }

        private ActionResult traditionalSave(PublicationStatus? publicationStatus, string versionComment)
        {
            var model = new MediaEditModel { Media = _mediaEditHelper.Cache };

            //enforce brand
            model.Media.Brand = GlobalVariables.CurrentBrand;

            if (!validate())
            {
                model.Responses = _responseHelper.UserResponses;
                return View("Edit", model);
            }

            if (!save(publicationStatus, versionComment))
            {
                model.Responses = _responseHelper.UserResponses;
                return View("Edit", model);
            }

            model.Responses = _responseHelper.UserResponses;
            model.MediaRatioEditModels = buildMediaRatioEditModel(model.Media);
            model.VersionHistory = _mediaService.GetVersionHistory(_mediaEditHelper.Cache, new VersionHydrationSettings
            {
                ModifiedBy = new UserHydrationSettings()
            });
            return View("Edit", model); // Success
        }

        private ActionResult traditionalUploadMedia()
        {
            if (uploadMedia())
                return RedirectToAction("Edit");
            else
                return RedirectToAction("Edit");
        }

        private bool uploadMedia(string originalFilename = null)
        {
            var stream = Request.InputStream;
            byte[] buffer = null;
            List<Media> lstMedia = new List<Media>();
          

            var medialst= _mediaEditHelperBulk.GetCache();
            if(medialst!=null)
            {
                lstMedia = medialst;
            }

            //lstMedia = _mediaEditHelperBulk.GetCache();
            try
            {
                if (originalFilename == null)
                {
                    if (Request.Files[0].ContentLength > MaxFileSize)
                    {
                        _responseHelper.AddUserResponse(ResponseType.Error, "Error", "Maximum file size is 100MB. ");
                        return false;
                    }
                    // IE or traditional form post used with Webkit, Mozilla
                    var postedFile = Request.Files[0];
                    stream = postedFile.InputStream;
                    originalFilename = Path.GetFileName(Request.Files[0].FileName);
                }
                else
                {
                    //Webkit, Mozilla and valums file upload was used
                    //...
                }
                buffer = new byte[stream.Length];
                stream.Read(buffer, 0, buffer.Length);
            }
            catch (Exception ex)
            {
                _responseHelper.AddUserResponse(ResponseType.Error, "Error", "There was a problem getting file data from input stream: " + ex.Message);
                return false;
            }

            // Get the media item we're editing from the cache
            var media = _mediaEditHelper.Cache;
            media.OriginalFileName = originalFilename;
            media.Brand = GlobalVariables.CurrentBrand;
            if (buffer.Length == 0 || string.IsNullOrWhiteSpace(originalFilename))
            {
                _responseHelper.AddUserResponse(ResponseType.Error, "No File specified", "Please select a media file to upload and then select upload");
                return false;
            }

            // Get configuration for the Original media profile
            var originalProfileConfig = ITOKConfig.Instance.MediaProfiles.GetProfileConfig(MediaProfile.Original);
            // Check if this media item already has an original file on disk
            if (!string.IsNullOrWhiteSpace(media.FileName))
            {
                if (S3Utilities.MediaExists(originalProfileConfig, media.FileName))
                    S3Utilities.DeleteMedia(originalProfileConfig, media.FileName);
            }

            // Make sure the media item is of a recognised format
            if (!FileUtilities.ExtensionIsRecognised(Path.GetExtension(originalFilename)))
            {
                _responseHelper.AddUserResponse(ResponseType.Error, "The extension of the file you uploaded has not been recognised");
                return false;
            }
            // Update the cached media item
            media.FileName = Guid.NewGuid().ToString() + Path.GetExtension(originalFilename).ToLower();
            media.Format = FileUtilities.FormatFromFilename(originalFilename);
            media.CreatedOn = DateTime.Now;
            try
            {
                // Save to disk
                if (FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Image))
                {
                    S3Utilities.SaveImage(stream, originalProfileConfig, media.FileName);//, media.Title, originalFilename);
                }
                else if (FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Document))
                {
                    S3Utilities.SaveDocument(stream, media.FileName);//, media.Title, originalFilename);
                    media.Hosting = MediaHosting.Document;
                }
                else if (FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Audio))
                {
                    S3Utilities.SaveAudio(stream, media.FileName); //, media.Title, originalFilename);
                }
                else if (FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Video))
                {
                    throw new NotImplementedException("Local video uploading is not implemented. Use remote hosting.");
                }
            }
            catch (Exception ex)
            {
                _responseHelper.AddUserResponse(ResponseType.Error, "Error", ex.Message);
                return false;
            }

            // If the media is an image then load it from disk so we can access its dimensions
            if (FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Image))
            {
                var originalImage = S3Utilities.GetImage(originalProfileConfig, media.FileName);
                media.SourceWidth = originalImage.Width;
                media.SourceHeight = originalImage.Height;
            }

            _responseHelper.AddUserResponse(ResponseType.Notice, "Successfully uploaded media");
            // Update the media cache
            _mediaEditHelper.Cache = media;

            // Update the media cache
            lstMedia.Add(media);

            // If the media is an image then Auto crop for all ratios and profiles
            if (FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Image)) autoCrop();
            _mediaEditHelper.ClearCache();
            _mediaEditHelperBulk.SetCache(lstMedia);
            return true;
        }




        private bool UploadMediaBulk(string obj, string originalFilename = null)
        {
            var fileNames = JsonConvert.DeserializeObject<List<MediaFileName>>(obj);
            foreach (var itm in fileNames)
            {
                itm.txt = itm.txt.Replace('^', ' ');
            }
            var stream = Request.InputStream;

            if (stream.Length > MaxFileSize)
            {
                _responseHelper.AddUserResponse(ResponseType.Error, "Error", "Maximum file size is 100MB. ");
                return false;
            }
            byte[] buffer;
            //dynamic media;
            //dynamic originalImage;
            List<Media> lstMedia = new List<Media>();
            for (var cnt = 0; cnt <= Request.Files.Count - 1; cnt++)
            {
                try
                {


                    // IE or traditional form post used with Webkit, Mozilla
                    var postedFile = Request.Files[cnt];
                    stream = postedFile.InputStream;
                    originalFilename = Path.GetFileName(Request.Files[cnt].FileName);


                    buffer = new byte[stream.Length];
                    stream.Read(buffer, 0, buffer.Length);
                }
                catch (Exception ex)
                {
                    _responseHelper.AddUserResponse(ResponseType.Error, "Error", "There was a problem getting file data from input stream: " + ex.Message);
                    return false;
                }

                // Get the media item we're editing from the cache
                var media = new Media();
                media.OriginalFileName = fileNames.Where(a => a.txt == originalFilename.Substring(0, originalFilename.LastIndexOf('.'))).FirstOrDefault().newTxt == null ? originalFilename : fileNames.Where(a => a.txt == originalFilename).FirstOrDefault().newTxt;
                media.MetaDescription = fileNames.Where(a => a.txt == originalFilename.Substring(0, originalFilename.LastIndexOf('.'))).FirstOrDefault().caption == null ? "" : fileNames.Where(a => a.txt == originalFilename).FirstOrDefault().caption;
                media.FileName = originalFilename;
                media.Brand = GlobalVariables.CurrentBrand;
                if (buffer.Length == 0 || string.IsNullOrWhiteSpace(originalFilename))
                {
                    _responseHelper.AddUserResponse(ResponseType.Error, "No File specified", "Please select a media file to upload and then select upload");
                    return false;
                }

                // Get configuration for the Original media profile
                var originalProfileConfig = ITOKConfig.Instance.MediaProfiles.GetProfileConfig(MediaProfile.Original);
                // Check if this media item already has an original file on disk
                if (!string.IsNullOrWhiteSpace(media.FileName))
                {
                    if (S3Utilities.MediaExists(originalProfileConfig, media.FileName))
                        S3Utilities.DeleteMedia(originalProfileConfig, media.FileName);
                }

                // Make sure the media item is of a recognised format
                if (!FileUtilities.ExtensionIsRecognised(Path.GetExtension(originalFilename)))
                {
                    _responseHelper.AddUserResponse(ResponseType.Error, "The extension of the file you uploaded has not been recognised");
                    return false;
                }
                // Update the cached media item
                media.FileName = Guid.NewGuid().ToString() + Path.GetExtension(originalFilename).ToLower();
                media.Format = FileUtilities.FormatFromFilename(originalFilename);
                media.CreatedOn = DateTime.Now;
                try
                {
                    // Save to disk
                    if (FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Image))
                    {
                        S3Utilities.SaveImage(stream, originalProfileConfig, media.FileName);//, media.Title, originalFilename);
                    }
                    else if (FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Document))
                    {
                        S3Utilities.SaveDocument(stream, media.FileName);//, media.Title, originalFilename);
                        media.Hosting = MediaHosting.Document;
                    }
                    else if (FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Audio))
                    {
                        S3Utilities.SaveAudio(stream, media.FileName); //, media.Title, originalFilename);
                    }
                    else if (FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Video))
                    {
                        throw new NotImplementedException("Local video uploading is not implemented. Use remote hosting.");
                    }
                }
                catch (Exception ex)
                {
                    _responseHelper.AddUserResponse(ResponseType.Error, "Error", ex.Message);
                    return false;
                }
                  _mediaEditHelper.Cache=media;
                // If the media is an image then load it from disk so we can access its dimensions
                if (FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Image))
                {
                    var originalImage = S3Utilities.GetImage(originalProfileConfig, media.FileName);
                    media.SourceWidth = originalImage.Width;
                    media.SourceHeight = originalImage.Height;
                }


                // Update the media cache
                lstMedia.Add(media);


                // If the media is an image then Auto crop for all ratios and profiles
                if (FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Image)) autoCrop();
                _mediaEditHelper.ClearCache();
            }
            _responseHelper.AddUserResponse(ResponseType.Notice, "Successfully uploaded media");
            _mediaEditHelperBulk.SetCache(lstMedia);
            return true;
        }

        private bool validate()
        {
            if (!ModelState.IsValid)
            {
                _responseHelper.AddValidationResponses(ModelState);
                return false;
            }

            var media = _mediaEditHelper.Cache;

            if (media.Hosting == MediaHosting.Local && FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Video))
            {
                _responseHelper.AddUserResponse(ResponseType.Error, string.Format("{0} media cannot be hosted locally. The media you upload must be an image.", media.Format.ToString()));
                return false;
            }
            if (media.Hosting == MediaHosting.Document && FileUtilities.FormatIsInGroup(media.Format, FileFormatGroup.Video))
            {
                _responseHelper.AddUserResponse(ResponseType.Error, string.Format("{0} media cannot be hosted locally. The media you upload must be a document.", media.Format.ToString()));
                return false;
            }
            if ((media.Hosting != MediaHosting.Local && media.Hosting != MediaHosting.Document) &&
                string.IsNullOrWhiteSpace(media.RemoteItemCode))
            {
                _responseHelper.AddUserResponse(ResponseType.Error, string.Format("Remote item code is required for remotely hosted media"));
                return false;
            }

            return true;
        }

        #endregion Private Methods

        #region Private Properties

        private MediaFilterType MediaFilter
        {
            get
            {
                if (Session[MediaFilterTypeCacheKey] == null)
                    Session[MediaFilterTypeCacheKey] = MediaFilterType.Image;
                return (MediaFilterType)Session[MediaFilterTypeCacheKey];
            }
            set
            {
                Session[MediaFilterTypeCacheKey] = value;
                // Reset the pagination model everytime the MediaFilter is changed so that we start at the beginning again.
                _pageHelper.Reset();
            }
        }

        private int SearchCount
        {
            get
            {
                if (Session[MediaSearchCountCacheKey] == null)
                    Session[MediaSearchCountCacheKey] = DefaultPageSize;
                return (int)Session[MediaSearchCountCacheKey];
            }
            set
            {
                Session[MediaSearchCountCacheKey] = value;
            }
        }

        private string SearchText
        {
            get
            {
                if (Session[MediaSearchTextCacheKey] == null) return string.Empty;
                return (string)Session[MediaSearchTextCacheKey];
            }
            set
            {
                Session[MediaSearchTextCacheKey] = value;
            }
        }

        private MediaSearchType SearchType
        {
            get
            {
                if (Session[MediaSearchTypeCacheKey] == null)
                    Session[MediaSearchTypeCacheKey] = MediaSearchType.None;
                return (MediaSearchType)Session[MediaSearchTypeCacheKey];
            }
            set
            {
                Session[MediaSearchTypeCacheKey] = value;
            }
        }

        private bool ShowDeletedMedia
        {
            get
            {
                if (Session[ShowDeletedMediaCacheKey] == null) return false;
                else return (bool)Session[ShowDeletedMediaCacheKey];
            }
            set
            {
                Session[ShowDeletedMediaCacheKey] = value;
            }
        }

        #endregion Private Properties
    }


    public class MediaFileName
    {
        public string txt { get; set; }
        public string newTxt { get; set; }
        public string caption { get; set; }
    }
}