﻿using System.Web.Mvc;
using CMS.Helpers.Authorise;
using ITOK.Core.Services.Security;

namespace CMS.Controllers
    {
    public class MediaQuickUploadController : BaseController
        {
        [CMSAuthorize("UseCMS")]
        public PartialViewResult Load()
            {
            return PartialView("MediaQuickUpload");
            }

        /// <summary>
        /// RenderAction behaves incoherently when the page is posted back to.
        /// When the page is first loaded the HttpGet Action is requested.
        /// If the page posts back to itself, RenderAction then requests the
        /// HttpPost action method...
        /// </summary>
        /// <param name="nowt">Will always be null. Included to provide a
        /// signature that's distinct from the HttpGet version of this method.</param>
        /// <returns></returns>
        [CMSAuthorize("UseCMS")]
        [HttpPost]
        public PartialViewResult Load(string nowt)
            {
            return PartialView("MediaQuickUpload");
            }
        }
    }