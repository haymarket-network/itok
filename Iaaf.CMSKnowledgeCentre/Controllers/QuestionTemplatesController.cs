﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CMS.Helpers;
using ITOK.Core.Common;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data.Model;
using ITOK.Core.Services.CMS;
using ITOK.Core.Services.Core;

namespace CMS.Controllers
{
    public class QuestionTemplatesController : Controller
    {
        private readonly IQuestionTemplateService _questionTemplateService;
        private readonly IQuestionService _questionService;
        private readonly IUserService _userService;
        private readonly IAnswerService _answerService;
        private readonly ITagsService _tagsService;

        public QuestionTemplatesController()
        {
            _questionTemplateService = AppKernel.GetInstance<IQuestionTemplateService>();
            _questionService = AppKernel.GetInstance<IQuestionService>();
            _userService = AppKernel.GetInstance<IUserService>();
            _answerService = AppKernel.GetInstance<IAnswerService>();
            _tagsService = AppKernel.GetInstance<ITagsService>();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Template1()
        {
            return View();
        }


        public ActionResult TransportationTemplate()
        {
            return View();
        }


        public ActionResult GetVolunteersPerArea(string userId)
        {
            var model = new QuestionTemplate { isReadOnly = false };
            var answerString1 = "Number of positions (taken by volunteers)";
            var answerString2 = "Number of volunteers allocated per area to cover all shifts";

            var questionRow = new QuestionRow
            {
                AnswersRows = new List<AnswerRow>
                {
                    new AnswerRow
                    {
                        Title = answerString1,
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = answerString2,
                        AnswerDataType = AnswerDataType.Numbers
                    }
                }
            };

            var questionRow2 = new QuestionRow
            {
                AnswersRows = new List<AnswerRow>
                {
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    }
                }
            };

            questionRow.Question = "Planning & Coordination";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Administration & Finance";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Human Ressources";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "IT/Telecommunications";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Legal";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Accommodation";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Accreditation";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Transport";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Volunteering";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Protocol & Hospitality";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Venue Management";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Commercial & Sponsorship";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Marketing";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Ticketing Operations";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Communications";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Media Operations (Broadcast & Press)";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Competition, Event Presentation, Team Services";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Security";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Visas";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Logistics";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Medical";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Anti-Doping";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Knowledge Management";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow2.Question = "Calculated";
            questionRow2.AnswerDataType = AnswerDataType.CalculatedHorizontally;
            model.Questions.Add(questionRow2.ShallowCopy());

            return PartialView("_VolunteersPerArea", model);
        }

        [HttpPost]
        public ActionResult VolunteersPerArea(QuestionTemplate model)
        {
            return View("Template1", model);
        }

        public ActionResult VolunteersPerArea()
        {
            return View();
        }

        public ActionResult GetTransportationTemplate(string userId)
        {
            var model = _questionTemplateService.GetTransportationTemplateObject(false, "", "", "", "");

            return PartialView("_TransportationTemplate", model);
        }


        [HttpPost]
        public ActionResult TemplateSave(QuestionTemplate model)
        {
            var question = _questionService.FindById(model.QuestionId);
            var dbAnswer = _answerService.Find(model.QuestionId, model.EventId, model.YearId, model.AreaId);

            var user = _userService.FindById(MembershipHelper.GetActiveUserId);

            var userEvent = user.UserEvents
                .FirstOrDefault(x => x.AreaId == model.AreaId && x.YearId == model.YearId && x.EventId == model.EventId);
            if (userEvent == null)
            {
                // itok leader case
                var Answereduser = _userService.FindById(dbAnswer.AnsweredBy);
                userEvent = Answereduser.UserEvents
                    .FirstOrDefault(x => x.AreaId == model.AreaId && x.YearId == model.YearId && x.EventId == model.EventId);
            }
            var eventquestion = userEvent.QuestionList.FirstOrDefault(x => x.QuestionId.Trim() == model.QuestionId);
            if (model != null)
            {
                eventquestion.isAnswered = true;
            }

            Answer answer = new Answer
            {
                QuestionId = model.QuestionId,
                UserId = MembershipHelper.GetActiveUserId,
                AnswerType = AnswerType.Template,
                TemplateType = model.TemplateType,
                EventId = model.EventId,
                YearId = model.YearId,
                AreaId = model.AreaId,
                QuestionTemplate = model,
                QuestionText = question.QuestionText
            };
            //get category from area id and include in answertags array
            var area = _tagsService.FindById(userEvent.AreaId);
            var tagIds = new List<string>();
            tagIds.Add(userEvent.AreaId);
            tagIds.Add(userEvent.EventId);
            tagIds.Add(userEvent.YearId);
            tagIds.Add(area.ParentId);
            var tags = _tagsService.FindByIds(tagIds.ToArray());

            answer.AnswerTags = tags.Select(x => new AnswerTag { Text = x.DisplayText, UrlSlug = x.UrlSlug }).ToList();


            if (dbAnswer != null)
            {
                answer.Id = dbAnswer.Id;
            }


            if (string.IsNullOrEmpty(answer.Id))
            {
                answer.AnsweredBy = MembershipHelper.GetActiveUserId;
                answer.AnsweredDate = DateTime.UtcNow;
                answer.UpdatedBy = MembershipHelper.GetActiveUserId;
                answer.UpdatedDate = DateTime.UtcNow;
            }
            else
            {
                answer.AnsweredBy = dbAnswer.AnsweredBy;
                answer.AnsweredDate = dbAnswer.AnsweredDate;
                answer.UpdatedBy = MembershipHelper.GetActiveUserId;
                answer.UpdatedDate = DateTime.UtcNow;
            }

            _userService.Save(user);
            _answerService.Save(answer);

            if (user.IsItokLeader)
            {
                return RedirectToAction("LeaderIndex", "Home");
            }

            return RedirectToAction("Questions", "Home", new { @yearId = model.YearId, @areaId = model.AreaId, @eventId = model.EventId });
        }


        public ActionResult GetStaffHiredInLOC(string userId)
        {
            var model = new QuestionTemplate { isReadOnly = false };

            var questionRow1 = new QuestionRow
            {
                AnswersRows = new List<AnswerRow>
                {
                    new AnswerRow
                    {
                        Title = "25",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "24",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "23",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "22",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "21",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "20",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "19",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "18",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "17",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "16",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "15",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "14",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "13",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "12",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "11",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "10",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "9",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "8",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "7",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "6",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "5",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "4",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "3",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "2",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "1",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "Calculated",
                        AnswerDataType = AnswerDataType.CalculatedVertically
                    }
                }
            };

            var questionRow2 = new QuestionRow
            {
                AnswersRows = new List<AnswerRow>
                {
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    }
                }
            };



            questionRow1.Question = "Planning & Coordination";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Administration & Finance";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Human Ressources";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "IT/Telecommunications";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Legal";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Accommodation";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Accreditation";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Transport";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Volunteering";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Protocol & Hospitality";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Venue Management";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Commercial & Sponsorship";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Marketing";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Ticketing Operations";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Communications";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Media Operations (Broadcast & Press)";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Competition, Event Presentation, Team Services";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Security";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Visas";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Logistics";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Medical";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Anti-Doping";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Knowledge Management";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow2.Question = "Calculated";
            questionRow2.AnswerDataType = AnswerDataType.CalculatedHorizontally;
            model.Questions.Add(questionRow2.ShallowCopy());


            return PartialView("_StaffHiredInLOC", model);
        }

        [HttpPost]
        public ActionResult StaffHiredInLOC(QuestionTemplate model)
        {
            return View("Template1", model);
        }


        public ActionResult GetSiteVisitsSummary(string userId)
        {
            var model = new QuestionTemplate { isReadOnly = false };

            var questionRow1 = new QuestionRow
            {
                AnswersRows = new List<AnswerRow>
                {
                    new AnswerRow
                    {
                        Title = "IAAF Kick-Off",
                        AnswerDataType = AnswerDataType.Date
                    },
                    new AnswerRow
                    {
                        Title = "IAAF Presentation",
                        AnswerDataType = AnswerDataType.Date
                    },
                    new AnswerRow
                    {
                        Title = "LOC Presentation",
                        AnswerDataType = AnswerDataType.Date
                    }
                }
            };

            var questionRow2 = new QuestionRow
            {
                AnswersRows = new List<AnswerRow>
                {
                    new AnswerRow
                    {
                        Title = "IAAF Kick-Off",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "IAAF Presentation",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "LOC Presentation",
                        AnswerDataType = AnswerDataType.Numbers
                    }
                }
            };

            var questionRow3 = new QuestionRow
            {
                AnswersRows = new List<AnswerRow>
                {
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    }
                }
            };

            questionRow1.Question = "Date of Site Visit";
            questionRow1.AnswerDataType = AnswerDataType.Date;
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow2.Question = "Planning & Coordination";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());

            questionRow2.Question = "Administration & Finance";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());

            questionRow2.Question = "Human Ressources";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());

            questionRow2.Question = "IT/Telecommunications";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());

            questionRow2.Question = "Legal";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());

            questionRow2.Question = "Event Operations (Accreditation, Accommodation, Transportation, Visas, Volunteers, Security, Venue)";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());

            questionRow2.Question = "Protocol";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());

            questionRow2.Question = "Commercial & Sponsorship";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());

            questionRow2.Question = "Marketing";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());

            questionRow2.Question = "Ticketing Operations & Hospitality";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());

            questionRow2.Question = "Communications";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());

            questionRow2.Question = "Media Operations (Broadcast & Press)";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());

            questionRow2.Question = "Competition";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());

            questionRow2.Question = "Event Presentation";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());

            questionRow2.Question = "Team Services";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());

            questionRow2.Question = "Medical";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());

            questionRow2.Question = "Anti-Doping";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());

            questionRow2.Question = "Knowledge Management (Observer Programme, Impact Studies, Data Capture)";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());

            questionRow3.Question = "Calculated";
            questionRow3.AnswerDataType = AnswerDataType.CalculatedHorizontally;
            model.Questions.Add(questionRow3.ShallowCopy());


            return PartialView("_SiteVisitsSummary", model);
        }

        [HttpPost]
        public ActionResult SiteVisitsSummary(QuestionTemplate model)
        {
            return View("Template1", model);
        }


        public ActionResult GetCarpoolUsagePerDay(string userId)
        {
            var model = new QuestionTemplate { isReadOnly = false };

            var questionRow1 = new QuestionRow
            {
                AnswersRows = new List<AnswerRow>
                {
                    new AnswerRow
                    {
                        Title = "10",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "9",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "8",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "7",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "6",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "5",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "4",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "3",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "2",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "1",
                        AnswerDataType = AnswerDataType.Numbers
                    },new AnswerRow
                    {
                        Title = "1",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "2",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "3",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "4",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "5",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "6",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "7",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "8",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "9",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "10",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "11",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "12",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "13",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "14",
                        AnswerDataType = AnswerDataType.Numbers
                    }
                }
            };

 


            questionRow1.Question = "Number of car movements";
            questionRow1.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow1.ShallowCopy());

       

            //questionRow1.Question = "Calculated";
            //questionRow1.AnswerDataType = AnswerDataType.Numbers;
            //model.Questions.Add(questionRow1.ShallowCopy());
  

            return PartialView("_CarpoolUsagePerDay", model);
        }

        [HttpPost]
        public ActionResult CarpoolUsagePerDay(QuestionTemplate model)
        {
            return View("Template1", model);
        }



        public ActionResult GetAccommodationFormUpdated(string userId)
        {
            var model = new QuestionTemplate { isReadOnly = false };

            var questionRow1 = new QuestionRow
            {
                AnswersRows = new List<AnswerRow>
                {
                    new AnswerRow
                    {
                        Title = "Hotel 1",
                        AnswerDataType = AnswerDataType.FreeText
                    },
                    new AnswerRow
                    {
                        Title = "Hotel 2",
                        AnswerDataType = AnswerDataType.FreeText
                    },
                    new AnswerRow
                    {
                        Title = "Hotel 3",
                        AnswerDataType = AnswerDataType.FreeText
                    }
                }
            };

            var questionRow2 = new QuestionRow
            {
                AnswersRows = new List<AnswerRow>
                {
                    new AnswerRow
                    {
                        Title = "Twin/double",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "Single",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "Twin/double",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "Single",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "Twin/double",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "Single",
                        AnswerDataType = AnswerDataType.Numbers
                    }

                }
            };

            questionRow1.Question = "Client Group";
            questionRow1.AnswerDataType = AnswerDataType.FreeText;
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Name of accommodation";
            questionRow1.AnswerDataType = AnswerDataType.FreeText;
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Standard of accommodation (*)";
            questionRow1.AnswerDataType = AnswerDataType.FreeText;
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Website";
            questionRow1.AnswerDataType = AnswerDataType.FreeText;
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Distance (in km) and travel time by bus between accommodation and airport(s)";
            questionRow1.AnswerDataType = AnswerDataType.FreeText;
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Distance (in km) and travel time by bus between accommodation and venue/course";
            questionRow1.AnswerDataType = AnswerDataType.FreeText;
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Maximum capacity of accommodation (no. of beds)";
            questionRow1.AnswerDataType = AnswerDataType.FreeText;
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Number of single rooms that have been reserved for the duration of the event";
            questionRow1.AnswerDataType = AnswerDataType.FreeText;
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Number of single rooms that have been used for the duration of the event";
            questionRow1.AnswerDataType = AnswerDataType.FreeText;
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Number of twin/double rooms that have been reserved for the duration of the event";
            questionRow1.AnswerDataType = AnswerDataType.FreeText;
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Number of twin/double rooms that have been used for the duration of the event";
            questionRow1.AnswerDataType = AnswerDataType.FreeText;
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Number of meeting rooms available";
            questionRow1.AnswerDataType = AnswerDataType.FreeText;
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Description of cancellation policy";
            questionRow1.AnswerDataType = AnswerDataType.FreeText;
            model.Questions.Add(questionRow1.ShallowCopy());


            questionRow2.Question = "Description of cancellation policy";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());


            return PartialView("_AccommodationFormUpdated", model);
        }

        [HttpPost]
        public ActionResult AccommodationFormUpdated(QuestionTemplate model)
        {
            return View("Template1", model);
        }
    }
}