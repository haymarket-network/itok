﻿using System;
using System.Security.Cryptography.X509Certificates;
using System.Web.Hosting;
using System.Web.Mvc;
using CMS.Helpers;
using CMS.Helpers.Authorise;
using Google.Apis.Analytics.v3;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using ITOK.Core.Common;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using ITOK.Core.Services.CMS;
using ITOK.Core.Services.Security;

namespace CMS.Controllers
    {
    public class DashboardController : Controller
        {
        private IMongoRepository1 mongoRepository;

        // Services
        private readonly ArticleService _articleService;

        private static readonly string[] scopes = new string[] { AnalyticsService.Scope.Analytics, AnalyticsService.Scope.AnalyticsReadonly };
        // view and manage your Google Analytics data

        private static string keyFilePath = "~/Assets/ITOK Dashboard-568f4681d116.p12";// Downloaded from https://console.developers.google.com

        private static string serviceAccountEmail = "desktop@ITOK-dashboard.iam.gserviceaccount.com";// found https://console.developers.google.com

        //loading the Key file
        private static readonly X509Certificate2 certificate = new X509Certificate2(HostingEnvironment.MapPath(keyFilePath), "notasecret",
            X509KeyStorageFlags.Exportable);

        private static readonly ServiceAccountCredential credential =
            new ServiceAccountCredential(new ServiceAccountCredential.Initializer(serviceAccountEmail)
            {
                Scopes = scopes
            }.FromCertificate(certificate));

        private readonly AnalyticsService _service = new AnalyticsService(new BaseClientService.Initializer()
        {
            HttpClientInitializer = credential,
            ApplicationName = "Desktop",
            ApiKey = "fd0490e638d509af6ce869f5dcb297c5ce3a6c79",
        });

        public DashboardController()
            {
            mongoRepository = AppKernel.GetInstance<IMongoRepository1>();

            _articleService = new ArticleService(mongoRepository);
            }

        public DashboardController(IMongoRepository1 _mongoRepository)
            {
            this.mongoRepository = _mongoRepository;
            _articleService = new ArticleService(mongoRepository);
            }

        [CMSAuthorize("UseCMS")]
        public ActionResult ArticleStats(string id = "")
            {
            var article = new Article();
            try
                {
                var auth = CMS.Helpers.GoogleLogin.GetAccessToken(serviceAccountEmail,
                    Server.MapPath(keyFilePath), AnalyticsService.Scope.AnalyticsReadonly);
                ViewBag.Token = auth.access_token;

                if (!string.IsNullOrEmpty(id))
                    {
                    //get the page
                    article = _articleService.FindById(GlobalVariables.CurrentBrand, id, new ITOK.Core.Data.Model.HS.ArticleHydrationSettings());
                    ViewBag.urlSlug = article.UrlSlug;
                    }
                }
            catch (Exception ex)
                {
                ViewBag.errorMsg = ex.Message;
                if (ex.InnerException != null)
                    ViewBag.errorMsg += ex.InnerException.Message;
                }

            return View(article);
            }

        [CMSAuthorize("UseCMS")]
        public ActionResult SiteStats(string id = "")
            {
            var article = new Article();
            try
                {
                var auth = CMS.Helpers.GoogleLogin.GetAccessToken(serviceAccountEmail,
                    Server.MapPath(keyFilePath), AnalyticsService.Scope.AnalyticsReadonly);
                ViewBag.Token = auth.access_token;

                if (!string.IsNullOrEmpty(id))
                    {
                    //get the page
                    article = _articleService.FindById(GlobalVariables.CurrentBrand, id, new ITOK.Core.Data.Model.HS.ArticleHydrationSettings());
                    ViewBag.urlSlug = article.UrlSlug;
                    }
                }
            catch (Exception ex)
                {
                ViewBag.errorMsg = ex.Message;
                if (ex.InnerException != null)
                    ViewBag.errorMsg += ex.InnerException.Message;
                }

            return View(article);
            }

        // GET: Dashboard/Test
        [CMSAuthorize("UseCMS")]
        public ActionResult Test()
            {
            var request = _service.Data.Ga.Get("ga:89711282", "2016-01-01", "2016-02-02",
                    "ga:sessions");
            request.Filters = "ga:browser=~^Firefox";
            request.MaxResults = 1000;
            var result = request.Execute();

            //DataResource.RealtimeResource.GetRequest request = service.Data.Realtime.Get(String.Format("ga:{0}", profileId), "rt:activeUsers");
            //RealtimeData feed = request.Execute();

            return View(result);
            }
        }
    }