﻿using AutoMapper;
using CMS.Helpers;
using CMS.Models;
using ITOK.Core.Common;
using ITOK.Core.Common.Config;
using ITOK.Core.Common.Constants;
using ITOK.Core.Common.Utils;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using ITOK.Core.Services.Core;
using ITOK.Core.Services.Security;
using ITOK.Core.ViewModels;
using Newtonsoft.Json;
using Omu.Encrypto;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace CMS.Controllers
{
    public class UserController : BaseController
    {
        private const int MaxResults = 12;
        private const string PaginationInstanceKey = "UserIndex";
        private const string UserResponseInstanceKey = "User";

        // Helpers
        private readonly EntityEditHelper<User> _userEditHelper = new EntityEditHelper<User>();

        private readonly PaginationHelper _pageHelper;
        private readonly UserResponseHelper _responseHelper;
        private readonly ITagsService _tagsService;
        private readonly IQuestionService _questionService;
        // Services

        private readonly ITOK.Core.Services.CMS.IUserService _userService;
        private IMongoRepository1 mongoRepository;

        public UserController()
        {
            mongoRepository = AppKernel.GetInstance<IMongoRepository1>();
            _userService = AppKernel.GetInstance<ITOK.Core.Services.CMS.IUserService>();
            _pageHelper = new PaginationHelper(PaginationInstanceKey, "/User/", MaxResults);
            _tagsService = AppKernel.GetInstance<ITagsService>();
            _questionService = AppKernel.GetInstance<IQuestionService>();
            _responseHelper = new UserResponseHelper(UserResponseInstanceKey);
        }

        public UserController(IMongoRepository1 _mongoRepository)
        {
            this.mongoRepository = _mongoRepository;
            _pageHelper = new PaginationHelper(PaginationInstanceKey, "/User/", MaxResults);
            _responseHelper = new UserResponseHelper(UserResponseInstanceKey);
        }

        #region User management

        #region Index & Page

        [CMSAuthorize("UseCMS")]
        public ActionResult Index()
        {
            return Page();
        }

        [CMSAuthorize("UseCMS")]
        public ActionResult Page(int pageNumber = -1)
        {
            if (pageNumber != -1)
            {
                _pageHelper.SetPage(pageNumber);
            }

            return View("Index", getUserListModel());
        }

        [CMSAuthorize("UseCMS")]
        public PartialViewResult Search(string searchText)
        {
            var foundUsers = _userService.Search(searchText, true, true, true, MaxResults);
            return PartialView("UserList", foundUsers);
        }

        #endregion Index & Page

        #region Create & Edit

        [CMSAuthorize("UseCMS")]
        public ActionResult Create()
        {
            _userEditHelper.ClearCache();

            var model = new UserEditModel
            {
                User = _userEditHelper.Cache
            };
            var allTags = _tagsService.All();

            var selectListItems = from c in allTags.Where(i => i.TagType == TagType.Area).OrderBy(i => i.DisplayText)
                                  select new SelectListItem { Text = c.DisplayText, Value = c.Id };

            ViewBag.TagsList = selectListItems.ToList();

            model.User.Eventtags = allTags.Where(i => i.TagType == TagType.Events).OrderBy(i => i.DisplayText)
                    .Select(x => new Tag { DisplayText = x.DisplayText, Id = x.Id, TagType = x.TagType, ParentId = x.ParentId, IsChecked = false })
                    .ToList();

            model.AllPermissionConfigurations = ITOKConfig.Instance.Permissions.ToList();
            return View("Edit", model);
        }

        [CMSAuthorize("UseCMS")]
        public ActionResult Edit(string id)
        {
            var model = new UserEditModel();
            var allTags = _tagsService.All();
            model.AllPermissionConfigurations = ITOKConfig.Instance.Permissions.ToList();
            var selectListItems = from c in allTags.Where(i => i.TagType == TagType.Area).OrderBy(i => i.DisplayText)
                                  select new SelectListItem { Text = c.DisplayText, Value = c.Id };

            ViewBag.TagsList = selectListItems.ToList();
            string tr = "";
            if (_userEditHelper.Cache.Id == id)
            {
                model.User = _userEditHelper.Cache;
                if (model.User.UserEvents != null)
                {
                    foreach (var itm in model.User.UserEvents)
                    {
                        var qId = "";
                        foreach (var t in itm.QuestionList)
                        {
                            qId = qId + t.QuestionId + ",";
                        }
                        qId = qId.Substring(0, qId.Length - 1);
                        tr = tr + "  <tr ><td id = " + itm.AreaId + " > " + itm.AreaTitle + " </ td ><td id = " + itm.EventId + " > " + itm.EventTitle + " </ td ><td id = " + itm.YearId + " > " + itm.YearTitle + " </ td><td id = '" + qId + "' >";
                        foreach (var Qitm in itm.QuestionList)
                        {
                            tr = tr + " <span class='tile' qid='" + Qitm.QuestionId + "'>" + Qitm.QuestionText + "<i onclick='deleteQuestion(this)' class='fa fa-close'></i></span>";
                        }
                        tr = tr + " </td><td><input class='btn btn-danger'  name='submit' type='button' value='Delete' onclick='deleteRow(this)' /></td></tr>";

                    }
                    model.tags = tr;
                }
            }
            else
            {
                model.User = _userService.FindById(id);
                if (model.User.UserEvents != null)
                {
                    foreach (var itm in model.User.UserEvents)
                    {
                        var qId = "";
                        foreach (var t in itm.QuestionList)
                        {
                            qId = qId + t.QuestionId + ",";
                        }
                        qId = qId.Substring(0, qId.Length - 1);
                        tr = tr + "  <tr ><td id = " + itm.AreaId + " > " + itm.AreaTitle + " </ td ><td id = " + itm.EventId + " > " + itm.EventTitle + " </ td ><td id = " + itm.YearId + " > " + itm.YearTitle + " </ td><td id = '" + qId + "' >";
                        foreach (var Qitm in itm.QuestionList)
                        {
                            tr = tr + " <span class='tile'>" + Qitm.QuestionText + "<i onclick='deleteQuestion(this)' class='fa fa-close'></i></span>";
                        }
                        tr = tr + " </td><td><input class='btn btn-danger'  name='submit' type='button' value='Delete' onclick='deleteRow(this)' /></td></tr>";

                    }
                    model.tags = tr;
                }

                if (model.User == null)
                {
                    _responseHelper.AddUserResponse(ResponseType.Error, "Could not find the specified user to edit.");
                    return RedirectToAction("Index");
                }
                else
                {
                    _userEditHelper.Cache = model.User;
                }
            }
            return View("Edit", model);
        }

        [CMSAuthorize("UseCMS")]
        [HttpPost]
        public ActionResult EditAction(User user, string newPassword, string newPasswordConfirmation, string submit)
        {
            switch (submit)
            {
                case "Save": return save(user, newPassword, newPasswordConfirmation);
                case "Cancel": return cancel();
            }
            throw new Exception("Unrecognised EditAction");
        }

        [CMSAuthorize("UseCMS")]
        public ActionResult Delete(string id)
        {
            try
            {
                var user = _userService.FindById(id);

                if (user == null)
                {
                    _responseHelper.AddUserResponse(ResponseType.Error, "Could not find the specified user to delete");
                    return RedirectToAction("Index");
                }
                _userService.DeleteById(id);

                _responseHelper.AddUserResponse(ResponseType.Notice, string.Format("Successfully deleted user: {0} {1}", user.Forename, user.Surname));
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                _responseHelper.AddUserResponse(ResponseType.Error, "Error", ex.Message);
                return RedirectToAction("Index");
            }
        }

        [CMSAuthorize("UseCMS")]
        public ActionResult AddCMSPermissions(string id)
        {
            try
            {
                var user = _userService.FindById(id);

                if (user == null)
                {
                    _responseHelper.AddUserResponse(ResponseType.Error, "Could not find the specified user");
                    return RedirectToAction("Index");
                }

                var Permissions = new List<Permission>();

                Permissions.Add(Permission.UseCMS);
                Permissions.Add(Permission.EditMedia);

                user.Permissions = Permissions;

                _userService.Save(user);

                _responseHelper.AddUserResponse(ResponseType.Notice, string.Format("Successfully added basic CMS Permissions: {0} {1}", user.Forename, user.Surname));
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                _responseHelper.AddUserResponse(ResponseType.Error, "Error", ex.Message);
                return RedirectToAction("Index");
            }
        }

        [CMSAuthorize("UseCMS")]
        public ActionResult RemoveCMSPermissions(string id)
        {
            try
            {
                var user = _userService.FindById(id);

                if (user == null)
                {
                    _responseHelper.AddUserResponse(ResponseType.Error, "Could not find the specified user to delete");
                    return RedirectToAction("Index");
                }

                user.Permissions.Remove(Permission.UseCMS);

                _userService.Save(user);

                _responseHelper.AddUserResponse(ResponseType.Notice, string.Format("Successfully removed CMS Permissions: {0} {1}", user.Forename, user.Surname));
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                _responseHelper.AddUserResponse(ResponseType.Error, "Error", ex.Message);
                return RedirectToAction("Index");
            }
        }

        [HttpGet]
        [CMSAuthorize("UseCMS")]
        public JsonResult BindYearByType(string eventids)
        {
            string[] ids = eventids.Split(',');
            //int tagtype = 1;
            var model = new UserEditModel
            {
                Yeartags = _tagsService.FindByTypeAndParent(TagType.Year, ids)
                    .Select(x => new Tag { DisplayText = x.DisplayText, Id = x.Id, TagType = x.TagType, ParentId = x.ParentId, IsChecked = false })
                    .ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [CMSAuthorize("UseCMS")]
        public JsonResult BindQuestionByTag(string areaids)
        {
            //string[] ids = areaids.Split(',');
            var allTags = _tagsService.All();
            var model = new UserEditModel
            {
                Questions = _questionService.FindByParentId(areaids)
                            .Select(x => new Tag { DisplayText = x.QuestionText, Id = x.Id, TagType = TagType.Question, ParentId = x.ParentTagId, IsChecked = false })
                    .ToList(),
                Yeartags = allTags.Where(i => i.TagType == TagType.Year).OrderBy(i => i.DisplayText)
                    .Select(x => new Tag { DisplayText = x.DisplayText, Id = x.Id, TagType = x.TagType, ParentId = x.ParentId, IsChecked = false })
                    .ToList(),
                Eventtags = allTags.Where(i => i.TagType == TagType.Events).OrderBy(i => i.DisplayText)
                        .Select(x => new Tag { DisplayText = x.DisplayText, Id = x.Id, TagType = x.TagType, ParentId = x.ParentId, IsChecked = false })
                        .ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [CMSAuthorize("UseCMS")]

        public JsonResult SaveAllUserEvent(string obj, string user)
        {
            User usr = new User();
            var stat = false;
            usr = JsonConvert.DeserializeObject<User>(user);
            if (validate(usr.Password, usr.Password, usr))
            {


                usr.Permissions = new List<Permission>();
                var pArr = usr.PermissionsStr.Split(',');
                usr.PermissionsStr = null;
                for (var x = 0; x < pArr.Length - 1; x++)
                {
                    var enm = Permission.UseFrontEnd;
                    #region Switch
                    switch (pArr[x].ToString())
                    {
                        case "UseCMS":
                            enm = Permission.UseCMS;
                            break;

                        case "EditMedia":
                            enm = Permission.EditMedia;
                            break;


                        case "EditTags":
                            enm = Permission.EditTags;
                            break;
                        case "EditUsers":
                            enm = Permission.EditUsers;
                            break;
                        case "UseFrontEnd":
                            enm = Permission.UseFrontEnd;
                            break;

                    }
                    #endregion
                    usr.Permissions.Add(enm);
                }
                if (usr.IsItokLeader == false && !string.IsNullOrEmpty(obj))
                {
                    // Getting user event from db, if exists, to get isAnswered, isCommented, isFlagSet,FlagColor
                    var OldUserEvent = string.IsNullOrEmpty(usr.Id) ? null : _userService.FindById(usr.Id).UserEvents;

                    var lstUserEvent = new List<UserEvent>();
                    obj = obj.Substring(0, obj.Length - 3);
                    var arr = obj.Split(new string[] { "^~^" }, StringSplitOptions.None);

                    for (int o = 0; o <= arr.Length - 1; o++)
                    {
                        var j = JsonConvert.DeserializeObject<UserEvent>(arr[o]);
                        j.QuestionList = new List<UserQuestion>();
                        var qArr = j.QuestionIds.Split(',');
                        var qtxtArr = j.QuestionTexts.Split(new string[] { "#~#" }, StringSplitOptions.None);
                        for (var x = 0; x <= qArr.Length - 1; x++)
                        {
                            var itm = new UserQuestion();
                            itm.QuestionId = qArr[x].Trim();
                            itm.QuestionText = qtxtArr[x];

                            if (OldUserEvent != null)
                            {
                                // this will run while update user
                                var oldDetails = GetQuestionDetailsIfExists(OldUserEvent, j.EventId, j.YearId, j.AreaId, itm.QuestionId);
                                if (oldDetails != null)
                                {
                                    itm.isAnswered = oldDetails.isAnswered;
                                    itm.isCommented = oldDetails.isCommented;
                                    itm.isFlagSet = oldDetails.isFlagSet;
                                    itm.FlagColor = oldDetails.FlagColor;
                                }
                            }
                            j.QuestionList.Add(itm);
                        }
                        j.QuestionIds = null;
                        j.QuestionTexts = null;
                        lstUserEvent.Add(j);
                    }
                    usr.UserEvents = lstUserEvent;
                }
                else
                {
                    usr.UserEvents = null;
                }
                usr.Questions = null;
                usr.Yeartags = null;
                var t = save(usr, usr.Password, usr.Password);
                stat = true;
            }
            var model = new UserEditModel();
            return Json("{Success:'" + stat + "' }", JsonRequestBehavior.AllowGet);
        }
        #endregion Create & Edit

        #region Private Methods
        private UserQuestion GetQuestionDetailsIfExists(List<UserEvent> oldUserEvent, string eventId, string yearId, string areaId, string questionId)
        {
            var userEvent = oldUserEvent.Where(x => x.EventId == eventId && x.YearId == yearId && x.AreaId == areaId).FirstOrDefault();
            if (userEvent != null)
            {
                var question = userEvent.QuestionList.Where(x => x.QuestionId.Trim() == questionId).FirstOrDefault();
                if (question != null)
                {
                    return question;
                }
            }
            return null;
        }

        private ActionResult save(User user, string newPassword, string newPasswordConfirmation)
        {
            mergeBasicChanges(user);

            var model = new UserEditModel();
            model.AllPermissionConfigurations = ITOKConfig.Instance.Permissions.ToList();
            model.User = _userEditHelper.Cache;
            if (!validate(newPassword, newPasswordConfirmation, model.User))
            {
                model.Responses = _responseHelper.UserResponses;
                return View("Edit", model);
            }

            // If no password has been given for a new user then generate one for them
            if (string.IsNullOrEmpty(_userEditHelper.Cache.Id) && string.IsNullOrWhiteSpace(newPassword))
            {
                newPassword = Guid.NewGuid().ToString() + DateTime.Now.Ticks.ToString();
                _responseHelper.AddUserResponse(ResponseType.Warning, "Generated a random password for the new user.");
            }

            // If a new password has been given then change the users password
            if (!string.IsNullOrWhiteSpace(newPassword))
            {
                var cachedUser = _userEditHelper.Cache;
                var hasher = new Hasher();
                hasher.SaltSize = 10;
                cachedUser.Password = hasher.Encrypt(newPassword);
                _userEditHelper.Cache = cachedUser;
            }

            try
            {
                var allTags = (model.User.Eventtags.Where(i => i.IsChecked)).Concat(model.User.Yeartags.Where(i => i.IsChecked))
                                    .Concat(model.User.Areatags.Where(i => i.IsChecked)).Concat(model.User.Questions.Where(i => i.IsChecked))
                                    .ToList();
                model.User.tags = allTags;
                model.User.Eventtags = new List<Tag>();
                model.User.Yeartags = new List<Tag>();
                model.User.Areatags = new List<Tag>();
                model.User.Questions = new List<Tag>();

                _userService.Save(model.User);
                _userEditHelper.Cache = model.User;
                _responseHelper.AddUserResponse(ResponseType.Notice, string.Format("Successfully updated user: {0} {1}", model.User.Forename, model.User.Surname));
                model.Responses = _responseHelper.UserResponses;
                return View("Edit", model);
            }
            catch (Exception ex)
            {
                _responseHelper.AddUserResponse(ResponseType.Error, "Error", ex.Message);
                model.Responses = _responseHelper.UserResponses;
                return View("Edit", model);
            }
        }

        private bool validate(string newPassword, string newPasswordConfirmation, User user)
        {
            if (!ModelState.IsValid)
            {
                _responseHelper.AddValidationResponses(ModelState);
                return false;
            }

            // Check password
            if (!string.IsNullOrWhiteSpace(newPassword) && !string.IsNullOrWhiteSpace(newPasswordConfirmation))
            {
                //Password is being changed
                if (newPassword != newPasswordConfirmation)
                {
                    _responseHelper.AddUserResponse(ResponseType.Error, "The new password and new password confirmation that you supplied do not match.");
                    return false;
                }
                if (!ValidatePasswordStregth(newPassword))
                {
                    _responseHelper.AddUserResponse(ResponseType.Error, "The password must contain Upper and Lowercase letters and a number. Min lenght 8 chars.");
                    return false;
                }
            }
            else if (string.IsNullOrWhiteSpace(newPassword) && string.IsNullOrWhiteSpace(newPasswordConfirmation))
            {
                // Password is being left alone
            }
            else
            {
                // One password is empty and one isn't. This is an error
                _responseHelper.AddUserResponse(ResponseType.Error, "The password and confirmation password do not match");
                return false;
            }

            // Any other validation...

            return true;
        }


        private bool ValidatePasswordStregth(string newPassword)
        {
            var reStength = new Regex(@"^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[\d]).*$");

            return reStength.IsMatch(newPassword);
        }

        private ActionResult cancel()
        {
            _userEditHelper.ClearCache();
            return RedirectToAction("Index");
        }

        private void mergeBasicChanges(User changedUser)
        {
            var user = _userEditHelper.Cache;
            user = Mapper.Map<User, User>(changedUser, user);
            _userEditHelper.Cache = user;
        }

        private UserListModel getUserListModel()
        {
            var model = new UserListModel();
            model.Users = _userService.GetPagedOrderedBySurname(
                _pageHelper.PaginationModel.PageSize * (_pageHelper.PaginationModel.CurrentPage - 1),
                _pageHelper.PaginationModel.PageSize);

            _pageHelper.SetTotalItemCount(_userService.GetCount());

            model.Pagination = _pageHelper.PaginationModel;
            model.Responses = _responseHelper.UserResponses;
            return model;
        }

        #endregion Private Methods

        #endregion User management

        #region Login, Logout & Reset

        public ActionResult Login(string key)
        {
            if (!string.IsNullOrWhiteSpace(key) && _userService.PasswordResetTicketIsValid(key))
            {
                ViewBag.PasswordResetTicketKey = key.ToLower();
            }
            if (!string.IsNullOrWhiteSpace(key) && !_userService.PasswordResetTicketIsActive(key))
            {
                ViewBag.PasswordResetTicketHasTimedOut = true;
            }
            return View();
        }

        [HttpPost]
        public ActionResult Login(string username, string password, bool? rememberMe)
        {

            if (MembershipHelper.Login(username, password, rememberMe == true))
            {
                if (MembershipHelper.IsItokLeader)
                {
                    return RedirectToAction("LeaderIndex", "Home");
                }
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ViewBag.ErrorMessage = "Invalid username and/or password. Please check the spelling and try again";
                return View();
            }
        }

        public ActionResult Logout()
        {
            MembershipHelper.Logout();
            return RedirectToAction("Login", "User");
        }

        public ActionResult AccessDenied()
        {
            return View();
        }

        [HttpPost]
        public JsonResult CompletePasswordReset(string newPassword, string newPasswordConfirmation, string key)
        {
            if (newPassword.ToLower() != newPasswordConfirmation.ToLower())
            {
                return Json(new { Success = false, Message = "The new password and confirmation passwords don't match" });
            }

            try
            {
                _userService.ChangeUserPassword(key, newPassword);
            }
            catch (Exception)
            {
                return Json(new { Success = false, Message = "There was a problem resetting your password" });
            }

            return Json(new { Success = true, Message = "Changed your password" });
        }

        [HttpPost]
        public JsonResult RequestPasswordReset(string email)
        {
            if (!_userService.UsernameExists(email))
            {
                return Json(new { Success = false, Message = string.Format("No user with email {0} could be found. Please make sure it's spelled correctly and that it's the email address associated with this account", email.ToLower()) });
            }

            var ticketKey = HttpUtility.UrlEncode(_userService.CreatePasswordResetTicket(email));

            var user = _userService.FindByEmail(email);

            // Create and send The ticket
            try
            {
                SESUtilities.SendTemplateEmail<PasswordResetModel>(
                    email,
                    "Your new ITOK password.",
                    "~/Views/Email/PasswordResetText.cshtml",
                    "~/Views/Email/PasswordResetHtml.cshtml",
                    new PasswordResetModel { PasswordResetTicketKey = ticketKey, User = user, BaseUrl = ConfigurationManager.AppSettings["CMSurl"] },
                    "andrew.macharg@haymarket.com",
                    null);
            }
            catch (Exception)
            {
                return Json(new { Success = false, Message = "There was a problem sending the confirmation email" });
            }
            return Json(new { Success = true, Message = "An email has been sent to your inbox with details about resetting your password" });
        }

        #endregion Login, Logout & Reset
    }

}