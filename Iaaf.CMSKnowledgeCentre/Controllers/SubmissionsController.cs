﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using CMS.Helpers;
using CMS.Helpers.Authorise;
using CMS.Models;
using ITOK.Core.Common;
using ITOK.Core.Common.Constants;
using ITOK.Core.Common.Utilities;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using ITOK.Core.Data.Model.HS;
using ITOK.Core.Services.CMS;
using ITOK.Core.Services.Security;
using ITOK.Core.ViewModels;
using ITagsService = ITOK.Core.Services.Core.ITagsService;


namespace CMS.Controllers
    {
    public class SubmissionsController : BaseController
        {
        //private const string ArticleModelsCacheKey = "ArticleModelsCache";
        private const int MaxMediaResultsPerTabView = 12;

        private const int MaxDiaristSearchResults = 12;
        private const string PaginationMainIndexInstanceKey = "SubmittedArticleMainIndex";
        private const string PaginationFeaturedIndexInstanceKey = "SubmittedArticleFeaturedIndex"; // Not used yet but probably will be in the future
        private const string UserResponseInstanceKey = "SubmittedArticle";
        private const string ShowDeletedArticlesCacheKey = "ShowDeletedSubmittedArticlesCache";
        private const string ShowShowArticleTypeCacheKey = "ShowShowSubmittedArticleTypeCache";
        private const string PaginationBaseUrl = "/submissions/";
        private const string ArticleFilterTypeCacheKey = "SubmittedArticleFilterTypeCache";

        private const string ArticleSearchTextCacheKey = "SubmittedArticleSearchTextCache";
        private const string ArticleSearchCountCacheKey = "SubmittedArticleSearchCountCache";
        private const string EventFilterCacheKey = "SubmittedArticleFilterCacheKey";

        private const string SelectedBrandCacheKey = "SubmittedSelectedBrandCacheKey";

        // Helpers
        private readonly PaginationHelper _mainlistPageHelper;

        private readonly UserResponseHelper _responseHelper;

        private IMongoRepository1 mongoRepository;

        // Services
        private readonly SubmittedArticleService _articleService;
        private readonly ArticleService _asService;
        private readonly SubmittedMediaService _mediaService;
        private readonly MediaService _amService;
        private readonly ITagsService _tagsService;
        private readonly UserService _userService;

        private readonly ArticleCommentService _articleCommentService;
        private readonly UserService _UserService;

        // Hydration settings
        private static readonly SubmittedArticleHydrationSettings _articleHydrationSettings;

        static SubmissionsController()
            {
            _articleHydrationSettings = new SubmittedArticleHydrationSettings
            {
                PrimaryMedia = new SubmittedMediaHydrationSettings(),
                RelatedMedia = new SubmittedMediaHydrationSettings(),
                RelatedArticles = new SubmittedArticleHydrationSettings(),               
                CreatedBy = new UserHydrationSettings(),
                UpdatedBy = new UserHydrationSettings(),
            };
            }

        public SubmissionsController()
            {
            mongoRepository = AppKernel.GetInstance<IMongoRepository1>();

            _articleService = new SubmittedArticleService(mongoRepository);
            _asService = new ArticleService(mongoRepository);
            _mediaService = new SubmittedMediaService(mongoRepository);
            _amService = new MediaService(mongoRepository);
            _tagsService = AppKernel.GetInstance<ITagsService>();
            _userService = new UserService(mongoRepository);
            _articleCommentService = new ArticleCommentService(mongoRepository);

            _mainlistPageHelper = new PaginationHelper(PaginationMainIndexInstanceKey, PaginationBaseUrl);
            _responseHelper = new UserResponseHelper(UserResponseInstanceKey);

            _UserService = new UserService(mongoRepository);
            }

        public SubmissionsController(IMongoRepository1 _mongoRepository)
            {
            this.mongoRepository = _mongoRepository;
            _articleService = new SubmittedArticleService(mongoRepository);
            _asService = new ArticleService(mongoRepository);
            _mediaService = new SubmittedMediaService(mongoRepository);
            _amService = new MediaService(mongoRepository);
            _tagsService = AppKernel.GetInstance<ITagsService>();
            _userService = new UserService(mongoRepository);

            _mainlistPageHelper = new PaginationHelper(PaginationMainIndexInstanceKey, PaginationBaseUrl);
            _responseHelper = new UserResponseHelper(UserResponseInstanceKey);
            }

        #region Index & Page

        [CMSAuthorize("UseCMS & ViewSubmittedContent | EditSubmittedContent")]
        public ActionResult Index()
        {
            SubmissionsSelectTagModel model = new SubmissionsSelectTagModel();
            model.EventTags = _tagsService.All().Where(i => i.TagType == TagType.Events).OrderBy(i => i.DisplayText).ToList();
            model.YearTags = _tagsService.All().Where(i => i.TagType == TagType.Year).OrderBy(i => i.DisplayText).ToList();

            return View(model);
        }
        [HttpPost]
        [CMSAuthorize("UseCMS & ViewSubmittedContent | EditSubmittedContent")]
        public ActionResult Index(string eventid, string yearid)
        {
           
            return RedirectToAction("Overview", new { eventid = eventid, yearid = yearid});
        }


        [CMSAuthorize("UseCMS & ViewSubmittedContent | EditSubmittedContent")]
        public ActionResult Overview(string eventid, string yearid)
            {
            SelectedBrand = "";

            var eventtag = _tagsService.FindById(eventid);
            var yeartag = _tagsService.FindById(yearid);

            //get all the questions
            var showWithStatus = ShowDeletedArticles ? new[] { SubmissionStatus.Deleted } : new[] { SubmissionStatus.InProgress, SubmissionStatus.Approved, SubmissionStatus.Submitted, SubmissionStatus.Rejected };

            var allTopics = _tagsService.All().Where(i => i.TagType == 0).OrderBy(i => i.DisplayText).ToList();
            var allSubmissions = _articleService.GetPagedOrderedBy(
              GlobalVariables.CurrentBrand, SortOrder.UpdatedOn, 0, 200,
              new string[] {eventtag.UrlSlug, yeartag.UrlSlug },
              showWithStatus,
              false, false, null,
              new SubmittedArticleHydrationSettings
              {
                  PrimaryMedia = new SubmittedMediaHydrationSettings(),
                  RelatedTags = new TagHydrationSettings(),
                  CreatedBy = new UserHydrationSettings(),
                  UpdatedBy = new UserHydrationSettings()
              }, true, MembershipHelper.GetActiveUserId
                ).ToList();


            SubmissionsOverviewModel model = new SubmissionsOverviewModel();
            model.EventTag = eventtag;
            model.YearTag = yeartag;
            foreach(var topic in allTopics)
            {
                foreach (var topicHeader in topic.TopicHeaders)
                {
                    KeyValuePair<string, string> top = topicHeader;
                    SubmissionOverview _so = new SubmissionOverview();
                    _so.tagName = topic.DisplayText;
                    _so.tagId = topic.Id;
                    _so.topicHeader = top.Value;
                    _so.topicId = top.Key;
                    _so.article = allSubmissions.Where(i => i.Title == top.Value).FirstOrDefault();
                    model.overviewList.Add(_so);
                    
                }
            }
            return View("Summary", model);
            //return Page();
            }



        [CMSAuthorize("UseCMS & ApproveSubmittedContent")]
        public ActionResult Approvals()
        {
           
            return PageApproved();
        }

        [CMSAuthorize("UseCMS & ApproveSubmittedContent")]
        public PartialViewResult BySubmissionStatus(SubmissionStatus Status, int pageNumber = -1, bool? showDeletedArticles = null, string selectedBrand = "")
            {
            if (showDeletedArticles != null) ShowDeletedArticles = (bool)showDeletedArticles;
            if (!string.IsNullOrEmpty(selectedBrand)) SelectedBrand = selectedBrand;

            submissionStatus = Status;
            // Update current page if necessary
            if (pageNumber != -1)
                {
                _mainlistPageHelper.SetPage(pageNumber);
                }

            return PartialView("Approvals", getArticleListModel(-1, false));
            }

        [CMSAuthorize("UseCMS & ViewSubmittedContent | EditSubmittedContent")]
        public PartialViewResult ByStatus(SubmissionStatus Status, int pageNumber = -1, bool? showDeletedArticles = null, string selectedBrand = "")
        {
            if (showDeletedArticles != null) ShowDeletedArticles = (bool)showDeletedArticles;
            if (!string.IsNullOrEmpty(selectedBrand)) SelectedBrand = selectedBrand;

            submissionStatus = Status;
            // Update current page if necessary
            if (pageNumber != -1)
            {
                _mainlistPageHelper.SetPage(pageNumber);
            }

            return PartialView("Index", getArticleListModel(-1));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="newPage"></param>
        /// <param name="selectedArticle">The current state of the selected article to keep track of any changes made by the user</param>
        /// <returns></returns>

        [CMSAuthorize("UseCMS & ViewSubmittedContent | EditSubmittedContent")]
        public ActionResult Page(int pageNumber = -1, bool? showDeletedArticles = null)
            {
            if (showDeletedArticles != null) ShowDeletedArticles = (bool)showDeletedArticles;

            // Update current page if necessary
            if (pageNumber != -1)
                {
                _mainlistPageHelper.SetPage(pageNumber);
                }

            return View("Index", getArticleListModel(pageNumber));
            }

        [CMSAuthorize("UseCMS & ApproveSubmittedContent")]
        public ActionResult PageApproved(int pageNumber = -1, bool? showDeletedArticles = null)
        {
            if (showDeletedArticles != null) ShowDeletedArticles = (bool)showDeletedArticles;

            // Update current page if necessary
            if (pageNumber != -1)
            {
                _mainlistPageHelper.SetPage(pageNumber);
            }

            return View("Approvals", getArticleListModel(pageNumber, false));
        }


        //[CMSAuthorize("UseCMS & ViewSubmittedContent | EditSubmittedContent")]
        //public PartialViewResult ByArticleType(ArticleType articleType, int pageNumber = -1, bool? showDeletedArticles = null)
        //{
        //    if (showDeletedArticles != null) ShowDeletedArticles = (bool)showDeletedArticles;

        //    ArticleFilter = articleType;

        //    // Update current page if necessary
        //    if (pageNumber != -1)
        //    {
        //        _mainlistPageHelper.SetPage(pageNumber);
        //    }

        //    return PartialView("Index", getArticleListModel(-1));
        //}

        [CMSAuthorize("UseCMS & ViewSubmittedContent | EditSubmittedContent | ApproveSubmittedContent")]
        public PartialViewResult Search(string searchText, int skip)
            {
            var showWithStatus = ShowDeletedArticles ?
                new[] { PublicationStatus.Deleted } :
                new[] { PublicationStatus.Published, PublicationStatus.UnPublished };

            SearchCount = skip + 15;

            var foundArticles = search(searchText, skip, 15);//_articleService.Search(searchText,skip, 15, true, false, false, showWithStatus);


            if (LiveUser.Can(Permission.ApproveSubmittedContent))
            {
                return PartialView("ApprovalList", foundArticles);

            }
            else
                return PartialView("SubmittedArticleList", foundArticles);
            }

        [HttpPost]
        [CMSAuthorize("UseCMS & ViewSubmittedContent | EditSubmittedContent | ApproveSubmittedContent")]
        public JsonResult GetTotalSearchResults(string searchText)
            {
            PublicationStatus[] showWithStatus;
            if (ShowDeletedArticles) showWithStatus = new[] { PublicationStatus.Deleted };
            else showWithStatus = new[] { PublicationStatus.Published, PublicationStatus.UnPublished };

            var resultCount = 0;
            //  resultCount = _articleService.SearchCount(searchText, true, false, showWithStatus, false);

            return Json(resultCount);
            }

        [HttpPost]
        [CMSAuthorize("UseCMS & ViewSubmittedContent | EditSubmittedContent")]
        public void CancelSearch()
            {
            SearchText = string.Empty;
            SearchCount = 0;
            }

        #endregion Index & Page

        #region Create & Edit
        //[CMSAuthorize("UseCMS & ViewSubmittedContent | EditSubmittedContent")]
        //public ActionResult SelectTag(string type)
        //{
        //    var model = _tagsService.All().Where(i => i.TagType == 0).ToList();
            
        //    return View(model);
        //}
        [CMSAuthorize("UseCMS & ViewSubmittedContent | EditSubmittedContent")]
        public ActionResult SelectTag(string tagid, string topicId, string eventid, string yearid)
        {
            var model = _tagsService.All().Where(i => i.TagType == 0).ToList();

            return RedirectToAction("Create", new { tagid = tagid, topicid = topicId, eventid=eventid, yearid=yearid });
        }

        [CMSAuthorize("UseCMS & ViewSubmittedContent | EditSubmittedContent")]
        public ActionResult Create(string tagid, string topicId, string eventid, string yearid)
            {
            using (var editCache = new EntityEditCache<SubmittedArticle>(x => x.Id))
                {
                var article = new SubmittedArticle();
                var tag = _tagsService.FindById(tagid);
                var eventtag = _tagsService.FindById(eventid);
                var yeartag = _tagsService.FindById(yearid);

                var topicHeader = tag.TopicHeaders.Where(i => i.Key == topicId).FirstOrDefault();
                if (topicHeader.Key != null)
                    article.Title = topicHeader.Value;
                article.Tags.Add(tag.UrlSlug);
                article.Tags.Add(eventtag.UrlSlug);
                article.Tags.Add(yeartag.UrlSlug);
                article.RelatedTags.Add(tag);
                article.RelatedTags.Add(eventtag);
                article.RelatedTags.Add(yeartag);

                var model = new SubmissionEditModel { Article = article };
                model.eventTag = eventtag;
                model.yearTag = yeartag;

                article.ArticleType = ArticleType.article;
                //related event
                model.availableTags = _tagsService.All().Where(i => i.Brand == GlobalVariables.CurrentBrand).ToList();
                editCache.SetEntity(article);

                return View("Edit", model);
                }
            }

        [CMSAuthorize("UseCMS & ViewSubmittedContent | EditSubmittedContent | ApproveSubmittedContent")]
        public ActionResult Edit(string id)
            {
            using (var editCache = new EntityEditCache<SubmittedArticle>(x => x.Id))
                {
                var model = new SubmissionEditModel { Article = _articleService.FindById(GlobalVariables.CurrentBrand, id, _articleHydrationSettings) };

                // Find the article to edit
                if (model.Article == null)
                    {
                    // The article wasnt found
                    _responseHelper.AddUserResponse(ResponseType.Error, "Could not find the specified article to edit.");
                    return View("Index", getArticleListModel(-1));
                    }
                // Add the entity to the edit cache
                editCache.SetEntity(model.Article);
                // }
                model.VersionHistory = _articleService.GetVersionHistory(model.Article.Id, new VersionHydrationSettings
                {
                    ModifiedBy = new UserHydrationSettings()
                });

                model.availableTags = _tagsService.All().Where(i => i.Brand == GlobalVariables.CurrentBrand).ToList();
                model.Responses = _responseHelper.UserResponses;

                //presumes all submissions to have both kinds of tags
                if (model.Article.RelatedTags.Where(i => i.TagType == TagType.Events).Any())
                    model.eventTag = _tagsService.FindByUrlSlug(model.Article.RelatedTags.Where(i => i.TagType == TagType.Events).First().UrlSlug, Brand.itok);
                if (model.Article.RelatedTags.Where(i => i.TagType == TagType.Year).Any())
                    model.yearTag = _tagsService.FindByUrlSlug(model.Article.RelatedTags.Where(i => i.TagType == TagType.Year).First().UrlSlug, Brand.itok);

                if (!string.IsNullOrEmpty(model.Article.AuthorUserId))
                    {
                    var user = _UserService.FindById(model.Article.AuthorUserId);
                    if (user != null)
                        {
                        model.AuthorUserName = user.Forename + " " + user.Surname;
                        model.AuthorUserId = user.Id.ToString();
                        }
                    }

               

                return View("Edit", model);
                }
            }

        [CMSAuthorize("UseCMS & ViewSubmittedContent | EditSubmittedContent")]
        public void RefreshEdit(string articleId)
            {
            using (var editHelper = new EntityEditCache<SubmittedArticle>(x => x.Id))
                {
                if (!editHelper.EntityIsAvailable(articleId))
                    editHelper.SetEntity(_articleService.FindById(GlobalVariables.CurrentBrand, articleId, _articleHydrationSettings));
                }
            }

        [HttpGet]
        public JsonResult GroupArticleStats()
            {
            try
                {
                //  _articleService.GroupArticleStats();
                return Json(new { success = true, msg = "Successfully re Grouped Article Stats" }, JsonRequestBehavior.AllowGet);
                }
            catch (Exception ex)
                {
                return Json(new { success = false, msg = "Error: " + ex.Message, stack = ex.StackTrace }, JsonRequestBehavior.AllowGet);
                }
            }

        [HttpGet]
        public JsonResult CleanStats()
            {
            try
                {
                // _articleService.CleanStatistics();
                return Json(new { success = true, msg = "Successfully cleaned out old statistics" }, JsonRequestBehavior.AllowGet);
                }
            catch (Exception ex)
                {
                return Json(new { success = false, msg = "Error: " + ex.Message, stack = ex.StackTrace }, JsonRequestBehavior.AllowGet);
                }
            }

        #region Primary Media

        [CMSAuthorize("UseCMS & ViewSubmittedContent | EditSubmittedContent")]
        public void RemovePrimaryMedia(string articleId)
            {
            using (var editHelper = new EntityEditCache<SubmittedArticle>(x => x.Id))
                {
                // Remove the primary media item from the cached article
                var article = editHelper.GetEntity(articleId);
                article.PrimaryMedia = null;
                }
            }

        [CMSAuthorize("UseCMS & ViewSubmittedContent | EditSubmittedContent")]
        public PartialViewResult AddPrimaryMedia(string articleId, string mediaId)
            {
            if (string.IsNullOrEmpty(mediaId)) throw new ArgumentException("A Media Id is required", "mediaId");
            using (var editHelper = new EntityEditCache<SubmittedArticle>(x => x.Id))
                {
                var article = editHelper.GetEntity(articleId);
                if (article == null) throw new Exception("Article is not in cache");
                // Find the specified media item
                var media = _mediaService.FindByIds(null, mediaId).FirstOrDefault();
                if (media == null) throw new Exception("Couldn't find the specified media item with id: " + mediaId);

                article.PrimaryMedia = media;

                return PartialView("EditSubmittedMediaItem", media);
                }
            }

        #endregion Primary Media

        #region RelatedMedia

        [CMSAuthorize("UseCMS & ViewSubmittedContent | EditSubmittedContent")]
        public void RemoveRelatedMedia(string articleId, string mediaId)
            {
            if (string.IsNullOrEmpty(mediaId)) throw new ArgumentException("A Media Id is required", "mediaId");
            using (var editHelper = new EntityEditCache<SubmittedArticle>(x => x.Id))
                {
                var article = editHelper.GetEntity(articleId);
                if (article == null) throw new Exception("Article is not in cache");
                var media = article.RelatedMedia.SingleOrDefault(x => x.Id == mediaId);
                if (media == null) throw new Exception("Could not find the media item with the specified media ID");
                article.RelatedMedia.Remove(media);
                }
            }

        [CMSAuthorize("UseCMS & ViewSubmittedContent | EditSubmittedContent")]
        public PartialViewResult AddRelatedMedia(string articleId, string mediaId)
            {
            if (string.IsNullOrEmpty(mediaId)) throw new ArgumentException("A Media Id is required", "mediaId");
            using (var editHelper = new EntityEditCache<SubmittedArticle>(x => x.Id))
                {
                var article = editHelper.GetEntity(articleId);
                if (article == null) throw new Exception("Submission is not in cache");
                // Check that the current article doesnt already have the specified media item with profile
                if (article.RelatedMedia.Any(x => x.Id == mediaId))
                    throw new Exception("The specified media item already exist for the current article");
                // Get the media item by id
                var media = _mediaService.FindByIds(null, mediaId).FirstOrDefault();
                if (media == null) throw new Exception("Could not find a media item with the specified ID");
                // Add the media item to the current article
                article.RelatedMedia.Add(media);
                // Render a new partial for the client
                return PartialView("EditMediaItem", media);
                }
            }

        #endregion RelatedMedia


      

        /// <summary>
        ///
        /// </summary>
        /// <param name="articleId">The id of the article being edited</param>
        /// <param name="article">Partial data of the article being edited. This must be mapped onto the existing mapped article.</param>
        /// <param name="submit"></param>
        /// <returns></returns>
        [CMSAuthorize("UseCMS & ViewSubmittedContent | EditSubmittedContent | ApproveSubmittedContent")]
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult EditAction(string articleId, SubmittedArticle article, string submit, string versionComment, string UpdateVersion, string authorUserId, string[] tags)
            {
            
            switch (submit)
                {
                case "Approve": return approve(article, articleId, versionComment, UpdateVersion == "yes" ? true : false, authorUserId, tags);
                case "Save": return save(article, articleId, versionComment, UpdateVersion == "yes" ? true : false, authorUserId, tags);
                case "Cancel": return cancel(articleId);
                }
            throw new Exception("Unrecognised EditAction");
            }

        [CMSAuthorize("UseCMS & ViewSubmittedContent | EditSubmittedContent | ApproveSubmittedContent")]
        public ActionResult Delete(string articleId)
            {
            try
                {
                // Get the article to delete
                var article = _articleService.FindByIds(GlobalVariables.CurrentBrand, new SubmittedArticleHydrationSettings()
                {
                    PrimaryMedia = new SubmittedMediaHydrationSettings(),
                    RelatedMedia = new SubmittedMediaHydrationSettings(),

                    RelatedArticles = new SubmittedArticleHydrationSettings(),
                }, articleId).FirstOrDefault();
                if (article == null)
                    {
                    _responseHelper.AddUserResponse(ResponseType.Error, "Could not find the specified article to delete");
                    return RedirectToAction("Index");
                    }

                // Delete from cache if it exists
                var editHelper = new EntityEditCache<Article>(x => x.Id);
                if (editHelper.EntityIsAvailable(articleId)) editHelper.ClearCache(articleId);

                article.Status = SubmissionStatus.Deleted;
                _articleService.Save(article);

                _responseHelper.AddUserResponse(ResponseType.Notice, string.Format("Successfully deleted article: {0}", article.Title));
                return RedirectToAction("Index");
                }
            catch (Exception ex)
                {
                _responseHelper.AddUserResponse(ResponseType.Error, "Error", ex.Message);
                return RedirectToAction("Index");
                }
            }

        [CMSAuthorize("UseCMS & ApproveSubmittedContent")]
        public ActionResult HardDelete(string articleId)
            {
            // Get the article to delete
            var article = _articleService.FindByIds(GlobalVariables.CurrentBrand, null, articleId).FirstOrDefault();
            if (article == null)
                {
                _responseHelper.AddUserResponse(ResponseType.Error, "Could not find the specified article to delete");
                return RedirectToAction("Index");
                }

            _articleService.DeleteById(articleId, false);

            _responseHelper.AddUserResponse(ResponseType.Notice, string.Format("Successfully deleted article: {0}", article.Title));
            return RedirectToAction("Index");
            }

        [CMSAuthorize("UseCMS & ApproveSubmittedContent")]
        public ActionResult Restore(string articleId)
            {
            var article = _articleService.FindByIds(GlobalVariables.CurrentBrand, new SubmittedArticleHydrationSettings
            {
                PrimaryMedia = new SubmittedMediaHydrationSettings(),
                RelatedMedia = new SubmittedMediaHydrationSettings(),
                RelatedArticles = new SubmittedArticleHydrationSettings(),
            }, articleId).FirstOrDefault();
            if (article == null)
                {
                _responseHelper.AddUserResponse(ResponseType.Error, "Could not find the specified article to restore");
                return RedirectToAction("Index");
                }
            article.Status = SubmissionStatus.InProgress;
            _articleService.Save(article);
            _responseHelper.AddUserResponse(ResponseType.Notice, string.Format("Successfully restored article: {0}", article.Title));
            return RedirectToAction("Index");
            }

        [CMSAuthorize("UseCMS & ViewSubmittedContent | EditSubmittedContent")]
        public ActionResult RevertToVersion(string versionId)
            {
            if (string.IsNullOrEmpty(versionId)) throw new ArgumentException("Version Id is required to perform this action");
            var version = _articleService.GetVersion(versionId, null);
            if (version == null)
                {
                _responseHelper.AddUserResponse(ResponseType.Notice, "Couldn't find that version", "If this persists, contact the iaafsupport@haymarket.com");
                return RedirectToAction("Edit");
                }
            var articleVersion = _articleService.GetEntityVersion(versionId);
            try
                {
                using (var editHelper = new EntityEditCache<Article>(x => x.Id))
                    {
                    _articleService.RevertToVersion(versionId);
                    _responseHelper.AddUserResponse(ResponseType.Notice, "Successfully reverted article", string.Format("To the version backed-up on {0} at {1}",
                        version.ModifiedOn.ToString("dd MMMM"),
                        version.ModifiedOn.ToString("H:mm")));
                    editHelper.ClearCache(articleVersion.Id);
                    }
                }
            catch (Exception ex)
                {
                _responseHelper.AddUserResponse(ResponseType.Notice, "Error reverting article", string.Format("To the version of the {0}. Error message: {1}",
                    version.ModifiedOn.ToString("dd MMMM"),
                    ex.Message));
                }

            return RedirectToAction("Edit", new { id = articleVersion.Id });
            }

        #region Related Documents

        //[CMSAuthorize("UseCMS & ViewSubmittedContent | EditSubmittedContent")]
        //public PartialViewResult ListRelatedDocuments(string articleId)
        //    {
        //    var editHelper = new EntityEditCache<Article>(x => x.Id);
        //    Article article = editHelper.GetEntity(articleId);
        //    return PartialView("EditDocuments", article.RelatedDocuments);
        //    }

        //[CMSAuthorize("UseCMS & EditSubmittedContent")]
        //public PartialViewResult AddRelatedDocument(string articleId, string documentId)
        //    {
        //    if (string.IsNullOrEmpty(documentId)) throw new ArgumentException("A Document Id is required", "documentId");

        //    using (var editHelper = new EntityEditCache<Article>(x => x.Id))
        //        {
        //        Article article = editHelper.GetEntity(articleId);
        //        if (article.RelatedDocuments == null) article.RelatedDocuments = new List<Document>();
        //        if (!article.RelatedDocuments.Any(x => x.Id == documentId))
        //            {
        //            Document document = _standaloneDocumentService.FindByIds(documentId).SingleOrDefault();
        //            if (document != null)
        //                {
        //                article.RelatedDocuments.Add(document);
        //                return PartialView("EditDocumentItem", document);
        //                }
        //            else
        //                {
        //                throw new Exception(string.Format("Could not find a Document with the id {0}", documentId));
        //                }
        //            }
        //        else throw new Exception(string.Format("A document with the id {0} is already related to this article", documentId));
        //        }
        //    }

        //[CMSAuthorize("UseCMS & EditSubmittedContent")]
        //public void RemoveRelatedDocument(string articleId, string documentId)
        //    {
        //    if (string.IsNullOrEmpty(documentId)) throw new ArgumentException("A Document Id is required", "documentId");

        //    using (var editHelper = new EntityEditCache<Article>(x => x.Id))
        //        {
        //        Article article = editHelper.GetEntity(articleId);
        //        if (article.RelatedDocuments.Any(x => x.Id == documentId))
        //            {
        //            Document document = article.RelatedDocuments.SingleOrDefault(x => x.Id == documentId);
        //            article.RelatedDocuments.Remove(document);
        //            }
        //        else throw new Exception(string.Format("No document with id {0} is related to this article", documentId));
        //        }
        //    }

        #endregion Related Documents

        #region Tags

        // RelatedArticles
        [CMSAuthorize("UseCMS & ViewSubmittedContent | EditSubmittedContent")]
        public PartialViewResult ListRelatedArticles(string articleId)
            {
            var editHelper = new EntityEditCache<SubmittedArticle>(x => x.Id);
            var article = editHelper.GetEntity(articleId);
            return PartialView("EditSubmittedContent", article.RelatedArticles);
            }

        [CMSAuthorize("UseCMS & ViewSubmittedContent | EditSubmittedContent")]
        public PartialViewResult AddRelatedArticle(string articleId, string relatedArticleId)
            {
            if (string.IsNullOrEmpty(relatedArticleId)) throw new ArgumentException("A [related] Article Id is required", "relatedArticleId");
            using (var editHelper = new EntityEditCache<SubmittedArticle>(x => x.Id))
                {
                var article = editHelper.GetEntity(articleId);
                // Only add the related article if it isnt already tagged
                if (article.RelatedArticles.Any(x => x.Id == relatedArticleId)) return null;
                var relatedArticle = _articleService.FindByIds(GlobalVariables.CurrentBrand, new SubmittedArticleHydrationSettings
                {
                    PrimaryMedia = new SubmittedMediaHydrationSettings()
                }, relatedArticleId).SingleOrDefault();
                article.RelatedArticles.Add(relatedArticle);
                return PartialView("EditArticleItem", relatedArticle);
                }
            return null;
            }

        [CMSAuthorize("UseCMS & ViewSubmittedContent | EditSubmittedContent")]
        public void RemoveRelatedArticle(string articleId, string relatedArticleId)
            {
            if (string.IsNullOrEmpty(relatedArticleId)) throw new ArgumentException("A [related] Article Id is required", "relatedArticleId");
            using (var editHelper = new EntityEditCache<SubmittedArticle>(x => x.Id))
                {
                var article = editHelper.GetEntity(articleId);
                var relatedArticleToRemove = article.RelatedArticles.SingleOrDefault(x => x.Id == relatedArticleId.ToLower());
                if (relatedArticleToRemove != null) article.RelatedArticles.Remove(relatedArticleToRemove);
                }
            }

        #endregion Tags

        #endregion Create & Edit

        #region Private Methods
        private ActionResult approve(SubmittedArticle article, string cachedArticleId, string versionComment, bool UpdateVersion, string AuthorUserId, string[] tags)
        {
            using (var editHelper = new EntityEditCache<SubmittedArticle>(x => x.Id))
            {
                // Check for session expiry
                if (!string.IsNullOrEmpty(cachedArticleId) && !editHelper.EntityIsAvailable(cachedArticleId))
                {
                    _responseHelper.AddUserResponse(ResponseType.Error, "Session has expired.", "Unfortunately we were unable to save your changes. Please select the article and try again.");
                    return RedirectToAction("Index");
                }

                // Update the article cache with the latest changes
                mergeBasicChanges(article, editHelper, cachedArticleId);

                var model = new SubmissionEditModel
                {
                    Article = editHelper.GetEntity(cachedArticleId),
                    availableTags = _tagsService.All().Where(i => i.Brand == GlobalVariables.CurrentBrand).ToList(),
                    Versioned = (bool)UpdateVersion
                };

                //enforce the brand
                model.Article.brand = GlobalVariables.CurrentBrand;

                if (model.Article.CreatedOn == DateTime.MinValue)
                    model.Article.CreatedOn = DateTime.UtcNow;

                if (model.Versioned || string.IsNullOrEmpty(model.Article.Id))
                    model.Article.VersionDate = DateTime.UtcNow;
                else if (model.Article.VersionDate == null)
                {
                    model.Article.VersionDate = article.CreatedOn;
                }

                model.Article.AuthorUserId = AuthorUserId;
                if (model.Article.Tags == null)
                    model.Article.Tags = new List<string>();

                model.Article.UpdatedOn = DateTime.Now;
                if (model.Article.CreatedOn == null)
                    model.Article.CreatedOn = DateTime.Now;
                model.Article.UpdatedBy = MembershipHelper.GetActiveUser();
               // if (model.Article.CreatedBy == null)
              //      model.Article.CreatedBy = MembershipHelper.GetActiveUser();

                if (!validate(model.Article))
                {
                    model.Responses = _responseHelper.UserResponses;
                    return View("Edit", model);
                }
                if (tags == null || tags.Length == 0)
                {
                    _responseHelper.AddUserResponse(ResponseType.Error, "Error", "You must Select At Least One Tag");
                    model.Responses = _responseHelper.UserResponses;
                    return View("Edit", model);
                }
                try
                {
                    var _tags = _tagsService.FindByIds(tags);
                    _tags.ToList().ForEach(item => item.Description = null); //ones with descriptions seem to cause errors
                    model.Article.RelatedTags = _tags;
                    model.Article.Tags.Clear();
                    foreach (var _tag in _tags)
                    {
                        model.Article.Tags.Add(_tag.UrlSlug);
                    }
                   
                    _articleService.Save(model.Article, versionComment);
                    //create the actual article
                   
                    var art = _articleService.FindById(Brand.itok, model.Article.Id,
                        new SubmittedArticleHydrationSettings()
                        {
                            PrimaryMedia = new SubmittedMediaHydrationSettings(),
                            RelatedMedia = new SubmittedMediaHydrationSettings(),
                            CreatedBy = new UserHydrationSettings(),
                            UpdatedBy = new UserHydrationSettings()
                        });
                    Article approved = new Article()
                    {
                        Title = art.Title,
                        SEOTitle = art.Title,
                        Body = art.Body,
                        ArticleType = art.ArticleType,
                        Authors = null,
                        brand = art.brand,
                        BrandArticleNumber = art.BrandArticleNumber,
                        CreatedBy = art.CreatedBy,
                        CreatedOn = art.CreatedOn,
                        EnableGotItBlock = false,
                        EnableMessageAuthorBlock = false,
                        LiveFrom = DateTime.UtcNow,
                        LiveTo = DateTime.UtcNow.AddYears(20),
                        MetaDescription = art.StandFirst,
                        PlainTextBody = art.PlainTextBody,                    
                        RelatedTags = art.RelatedTags,
                        ResultCard = ResultCard.Standard,
                        StandFirst = art.StandFirst,
                        Status = PublicationStatus.Published,
                        Tags = art.Tags,
                        UpdatedBy = LiveUser.Get(),
                        UpdatedOn = DateTime.UtcNow,
                        VersionDate = DateTime.UtcNow
                    };
                    if (art.PrimaryMedia != null)
                        approved.PrimaryMedia = CopyMedia(art.PrimaryMedia);
                    if (art.RelatedMedia.Any())
                        approved.RelatedMedia = CopyMedia(art.RelatedMedia.ToList());// copy across related media

                    //save as new published article
                    _asService.Save(approved, "Approved");
                    _responseHelper.AddUserResponse(ResponseType.Notice, string.Format("Article: {0} has been approved", model.Article.Title));

                    //if it's successfully created the copy, update the article to approved and save
                    model.Article.Status = SubmissionStatus.Approved;
                    _articleService.Save(model.Article, versionComment);


                    if (model.Article.RelatedTags.Where(i => i.TagType == TagType.Events).Any())
                        model.eventTag = _tagsService.FindByUrlSlug(model.Article.RelatedTags.Where(i => i.TagType == TagType.Events).First().UrlSlug, Brand.itok);
                    if (model.Article.RelatedTags.Where(i => i.TagType == TagType.Year).Any())
                        model.yearTag = _tagsService.FindByUrlSlug(model.Article.RelatedTags.Where(i => i.TagType == TagType.Year).First().UrlSlug, Brand.itok);


                    //if saving new article
                    if (string.IsNullOrEmpty(cachedArticleId))
                        editHelper.SetEntity(model.Article);
                    else
                    {
                        model.VersionHistory = _articleService.GetVersionHistory(cachedArticleId, new VersionHydrationSettings
                        {
                            ModifiedBy = new UserHydrationSettings()
                        });
                    }
                    model.Responses = _responseHelper.UserResponses;                
                    model.Versioned = false;
                    return View("Edit", model);
                }
                catch (Exception ex)
                {
                    _responseHelper.AddUserResponse(ResponseType.Error, "Error", ex.Message);
                    model.Responses = _responseHelper.UserResponses;
                    return View("Edit", model);
                }
            }
        }

        private Media CopyMedia(SubmittedMedia s)
        {
            Media m = new Media();

            m.AvailableRatios = s.AvailableRatios;
            m.Brand = s.Brand;
            m.Complete = s.Complete;
            m.CreatedBy = s.CreatedBy;
            m.CreatedOn = s.CreatedOn;
            m.Credit = s.Credit;
            m.FileName = s.FileName;
            m.Format = s.Format;
            m.Hosting = s.Hosting;
            m.LiveFrom = s.LiveFrom;
            m.MetaDescription = s.MetaDescription;
            m.OriginalFileName = s.OriginalFileName;
            m.RemoteItemCode = s.RemoteItemCode;
            m.SEOTitle = s.SEOTitle;
            m.SourceHeight = s.SourceHeight;
            m.SourceWidth = s.SourceWidth;
            m.Status = PublicationStatus.Published;
            m.Title = s.Title;
            m.UpdatedBy = LiveUser.Get();
            m.UpdatedOn = DateTime.UtcNow;
            m.UrlSlug = s.UrlSlug;

            
            string id = _amService.Save(m, "Approved");
           // ITOK.Core.Services.Core.MediaService ms = new ITOK.Core.Services.Core.MediaService(mongoRepository);
            Media newM = _amService.FindByIds(new MediaHydrationSettings
            {
                RelatedArticles = new ArticleHydrationSettings
                {
                    PrimaryMedia = new MediaHydrationSettings(),
                    RelatedMedia = new MediaHydrationSettings()
                },

                CreatedBy = new UserHydrationSettings(),
                UpdatedBy = new UserHydrationSettings()
            }, id).FirstOrDefault();
            return newM;


        }
        private List<Media> CopyMedia(List<SubmittedMedia> media)
        {
            List<Media> results = new List<Media>();
            foreach(SubmittedMedia med in media)
            {
                results.Add(CopyMedia(med));
            }
            return (results);
        }

        private ActionResult save(SubmittedArticle article, string cachedArticleId, string versionComment, bool UpdateVersion, string AuthorUserId, string[] tags)
            {

          
            using (var editHelper = new EntityEditCache<SubmittedArticle>(x => x.Id))
                {
                // Check for session expiry
                if (!string.IsNullOrEmpty(cachedArticleId) && !editHelper.EntityIsAvailable(cachedArticleId))
                    {
                    _responseHelper.AddUserResponse(ResponseType.Error, "Session has expired.", "Unfortunately we were unable to save your changes. Please select the article and try again.");
                    return RedirectToAction("Index");
                    }

                // Update the article cache with the latest changes
                mergeBasicChanges(article, editHelper, cachedArticleId);

                var model = new SubmissionEditModel
                {
                    Article = editHelper.GetEntity(cachedArticleId),
                    availableTags = _tagsService.All().Where(i => i.Brand == GlobalVariables.CurrentBrand).ToList(),
                    Versioned = (bool)UpdateVersion
                };

                //enforce the brand
                model.Article.brand = GlobalVariables.CurrentBrand;

                if (model.Article.CreatedOn == DateTime.MinValue)
                    model.Article.CreatedOn = DateTime.UtcNow;

                if (model.Versioned || string.IsNullOrEmpty(model.Article.Id))
                    model.Article.VersionDate = DateTime.UtcNow;
                else if (model.Article.VersionDate == null)
                    {
                    model.Article.VersionDate = article.CreatedOn;
                    }

                model.Article.AuthorUserId = AuthorUserId;
                if (model.Article.Tags == null)
                    model.Article.Tags = new List<string>();

                model.Article.UpdatedOn = DateTime.Now;
                if (model.Article.CreatedOn == null)
                    model.Article.CreatedOn = DateTime.Now;
                model.Article.UpdatedBy = MembershipHelper.GetActiveUser();
                if (model.Article.CreatedBy == null)
                    model.Article.CreatedBy = MembershipHelper.GetActiveUser();

                if (article.Status == SubmissionStatus.Rejected && !LiveUser.CanDoAny(Permission.ApproveSubmittedContent))
                {
                    _responseHelper.AddUserResponse(ResponseType.Error, "Permission denied", "Unfortunately we were unable to save your changes, you do not have permission to reject articles");
                    model.Responses = _responseHelper.UserResponses;
                    return View("Edit", model);
                }
                else if (article.Status == SubmissionStatus.Rejected && LiveUser.CanDoAny(Permission.ApproveSubmittedContent) && string.IsNullOrEmpty(versionComment))
                {
                    _responseHelper.AddUserResponse(ResponseType.Error, "Insufficient information", "Please enter a comment to explain why it was rejected");
                    model.Responses = _responseHelper.UserResponses;
                    return View("Edit", model);
                }

                if (!validate(model.Article))
                    {
                    model.Responses = _responseHelper.UserResponses;
                    return View("Edit", model);
                    }
                if (tags == null || tags.Length == 0)
                    {
                    _responseHelper.AddUserResponse(ResponseType.Error, "Error", "You must Select At Least One Tag");
                    model.Responses = _responseHelper.UserResponses;
                    return View("Edit", model);
                    }
                try
                    {
                    var _tags = _tagsService.FindByIds(tags);
                    _tags.ToList().ForEach(item => item.Description = null); //ones with descriptions seem to cause errors
                    model.Article.RelatedTags = _tags;
                    model.Article.Tags.Clear();
                    foreach (var _tag in _tags)
                        {
                        model.Article.Tags.Add(_tag.UrlSlug);
                        }

                    // Save the cached article item
                    //    model.Article.LiveFrom = ((DateTime)model.Article.LiveFrom).ToUniversalTime();
                    _articleService.Save(model.Article, versionComment);
                    _responseHelper.AddUserResponse(ResponseType.Notice, string.Format("Successfully updated the article: {0}", model.Article.Title));


                    //set the tags

                    if (model.Article.RelatedTags.Where(i => i.TagType == TagType.Events).Any())
                        model.eventTag = _tagsService.FindByUrlSlug(model.Article.RelatedTags.Where(i => i.TagType == TagType.Events).First().UrlSlug, Brand.itok);
                    if (model.Article.RelatedTags.Where(i => i.TagType == TagType.Year).Any())
                        model.yearTag = _tagsService.FindByUrlSlug(model.Article.RelatedTags.Where(i => i.TagType == TagType.Year).First().UrlSlug, Brand.itok);

                    //if saving new article
                    if (string.IsNullOrEmpty(cachedArticleId))
                        editHelper.SetEntity(model.Article);
                    else
                        {
                        model.VersionHistory = _articleService.GetVersionHistory(cachedArticleId, new VersionHydrationSettings
                        {
                            ModifiedBy = new UserHydrationSettings()
                        });
                        }

                    model.Responses = _responseHelper.UserResponses;
                    //var typeList = from c in model.articleTypes
                    //                 select new SelectListItem { Text = c.ArticleTypeName, Value = c.Id, Selected = c.Id == model.Article.articleType.Id };

                    //ViewBag.myTypeList = typeList;
                    //return RedirectToAction("Edit", new { id = model.Article.Id });
                    model.Versioned = false;
                    return View("Edit", model);
                    }
                catch (Exception ex)
                    {
                    _responseHelper.AddUserResponse(ResponseType.Error, "Error", ex.Message);
                    model.Responses = _responseHelper.UserResponses;
                    return View("Edit", model);
                    }
                }
            }

        private bool validate(SubmittedArticle article)
            {
            var isValid = true;
            if (ModelState.IsValid) return isValid;
            //_responseHelper.AddValidationResponses(ModelState);
            _responseHelper.AddValidationResponses(ModelState);
            isValid = false;

            // Any custom validation
            // TODO: Uncomment the following lines once the Article.LiveFrom... properties have been removed
            //if (article.Status == PublicationStatus.Published && article.LiveFrom == null)
            //{
            //    _responseHelper.AddValidationResponse("LiveFrom", "A published article requries a LiveFrom date.");
            //    isValid = false;
            //}

            return isValid;
            }

        private ActionResult cancel(string articleId)
            {
            (new EntityEditCache<SubmittedArticle>(x => x.Id)).ClearCache(articleId);
            return RedirectToAction("Index");
            }

        /// <summary>
        /// Persists an Article entity while skipping all properties that are managed
        /// individually (e.g. RelatedArticles) or automatically (e.g. AuditDetails)
        /// </summary>
        /// <param name="changedArticle"></param>
        private void mergeBasicChanges(SubmittedArticle changedArticle, EntityEditCache<SubmittedArticle> editHelper, string articleId)
            {
            var article = editHelper.GetEntity(articleId);
            article = Mapper.Map<SubmittedArticle, SubmittedArticle>(changedArticle, article);
            article.Status = changedArticle.Status;
            article.ArticleType = changedArticle.ArticleType;
            }

        /// <summary>
        /// TODO: This method should also get a list of featured articles to display alongside the main list
        /// </summary>
        /// <returns></returns>
        private SubmissionsListModel getArticleListModel(int page = -1, bool restricted = true)
            {
            var model = new SubmissionsListModel { SubmissionStatus = submissionStatus };
            var showWithStatus = ShowDeletedArticles ? new[] { SubmissionStatus.Deleted } : new[] { SubmissionStatus.InProgress, SubmissionStatus.Approved, SubmissionStatus.Submitted, SubmissionStatus.Rejected };

            model.Articles = _articleService.GetPagedOrderedBy(
              GlobalVariables.CurrentBrand, SortOrder.UpdatedOn,
              _mainlistPageHelper.PaginationModel.PageSize * (_mainlistPageHelper.PaginationModel.CurrentPage - 1),
              _mainlistPageHelper.PaginationModel.PageSize,
              new string[] { },
              showWithStatus,
              false, false, submissionStatus,
              new SubmittedArticleHydrationSettings
              {
                  PrimaryMedia = new SubmittedMediaHydrationSettings(),
                  RelatedTags = new TagHydrationSettings(),
                  CreatedBy = new UserHydrationSettings(),
                  UpdatedBy = new UserHydrationSettings()
              }, true, restricted ? MembershipHelper.GetActiveUserId : ""
                ).ToList(); //restrict standard methods to only the users own items


            
            // Put the view model together
            _mainlistPageHelper.SetTotalItemCount(_articleService.GetCount(GlobalVariables.CurrentBrand, new string[] { }, showWithStatus, false, submissionStatus,true, restricted ? MembershipHelper.GetActiveUserId : ""));

            model.Pagination = _mainlistPageHelper.PaginationModel;
            model.Pagination.BaseUrl = PaginationBaseUrl;

            model.Responses = _responseHelper.UserResponses;
            model.ShowDeletedArticles = ShowDeletedArticles;
            

            // If a search was previously made then reload the results
            if (string.IsNullOrWhiteSpace(SearchText)) return model;
            model.SearchText = SearchText;
            model.SearchResults = search(SearchText, 0, SearchCount);

            return model;
            }

        /// <summary>
        /// TODO: This method should also get a list of featured articles to display alongside the main list
        /// </summary>
        /// <returns></returns>
        private SubmissionsListModel getApprovedArticleListModel(int page = -1)
        {
            var model = new SubmissionsListModel { SubmissionStatus = submissionStatus };
            var showWithStatus = ShowDeletedArticles ? new[] { SubmissionStatus.Deleted } : new[] { SubmissionStatus.InProgress, SubmissionStatus.Approved, SubmissionStatus.Submitted };

            model.Articles = _articleService.GetPagedOrderedBy(
              GlobalVariables.CurrentBrand, SortOrder.UpdatedOn,
              _mainlistPageHelper.PaginationModel.PageSize * (_mainlistPageHelper.PaginationModel.CurrentPage - 1),
              _mainlistPageHelper.PaginationModel.PageSize,
              new string[] { },
              showWithStatus,
              false, false, submissionStatus,
              new SubmittedArticleHydrationSettings
              {
                  PrimaryMedia = new SubmittedMediaHydrationSettings(),
                  RelatedTags = new TagHydrationSettings(),
                  CreatedBy = new UserHydrationSettings(),
                  UpdatedBy = new UserHydrationSettings()
              }
                ); //can see all

            // Put the view model together
            _mainlistPageHelper.SetTotalItemCount(_articleService.GetCount(GlobalVariables.CurrentBrand, new string[] { }, showWithStatus, false, submissionStatus));

            model.Pagination = _mainlistPageHelper.PaginationModel;
            model.Pagination.BaseUrl = PaginationBaseUrl;

            model.Responses = _responseHelper.UserResponses;
            model.ShowDeletedArticles = ShowDeletedArticles;

            // If a search was previously made then reload the results
            if (string.IsNullOrWhiteSpace(SearchText)) return model;
            model.SearchText = SearchText;
            model.SearchResults = search(SearchText, 0, SearchCount);

            return model;
        }

        private IList<SubmittedArticle> search(string searchText, int skip, int take)
            {
            var showWithStatus = ShowDeletedArticles ? new[] { SubmissionStatus.Deleted } : new[] { SubmissionStatus.InProgress, SubmissionStatus.Submitted, SubmissionStatus.Approved, SubmissionStatus.Deleted, SubmissionStatus.Submitted };
            bool restricted = true;
            if (LiveUser.CanDoAny(Permission.ApproveSubmittedContent))
                restricted = false;

            var articles = _articleService.Search(GlobalVariables.CurrentBrand, searchText, skip, take, true, false, false, null, false, null, 
                new SubmittedArticleHydrationSettings() { CreatedBy = new UserHydrationSettings(), UpdatedBy = new UserHydrationSettings() });

            SearchText = searchText;
            return !string.IsNullOrEmpty(SelectedBrand) ? articles.Where(i => i.brand == GlobalVariables.CurrentBrand).ToList() : articles;
            }

        #endregion Private Methods

        #region Private Properties

        private string SelectedBrand
            {
            get
                {
                if (Session[SelectedBrandCacheKey] == null)
                    return "";
                else
                    return Session[SelectedBrandCacheKey].ToString();
                }
            set
                {
                Session[SelectedBrandCacheKey] = value;
                }
            }

        private bool ShowDeletedArticles
            {
            get
                {
                if (Session[ShowDeletedArticlesCacheKey] == null) return false;
                else return (bool)Session[ShowDeletedArticlesCacheKey];
                }
            set
                {
                Session[ShowDeletedArticlesCacheKey] = value;
                }
            }

        private SubmissionStatus submissionStatus
            {
            get
                {
                if (Session[ArticleFilterTypeCacheKey] == null)
                    Session[ArticleFilterTypeCacheKey] = SubmissionStatus.Submitted;

                return (SubmissionStatus)Session[ArticleFilterTypeCacheKey];
                }
            set
                {
                Session[ArticleFilterTypeCacheKey] = value;
                // Reset the pagination model everytime the MediaFilter is changed so that we start at the beginning again.
                _mainlistPageHelper.Reset();
                }
            }

        private string SearchText
            {
            get
                {
                if (Session[ArticleSearchTextCacheKey] == null) return string.Empty;
                return (string)Session[ArticleSearchTextCacheKey];
                }
            set
                {
                Session[ArticleSearchTextCacheKey] = value;
                }
            }

        private int SearchCount
            {
            get
                {
                if (Session[ArticleSearchCountCacheKey] == null)
                    Session[ArticleSearchCountCacheKey] = MaxMediaResultsPerTabView;
                return (int)Session[ArticleSearchCountCacheKey];
                }
            set
                {
                Session[ArticleSearchCountCacheKey] = value;
                }
            }

        #endregion Private Properties
        }
    }