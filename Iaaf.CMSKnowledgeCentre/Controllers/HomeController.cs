﻿using CMS.Helpers;
using ITOK.Core.Common;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using ITOK.Core.Services.CMS;
using ITOK.Core.Services.Core;
using ITOK.Core.Services.Security;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace CMS.Controllers
{
    [CMSAuthorize("UseFrontEnd")]
    public class HomeController : Controller
    {
        private readonly IMongoRepository1 _mongoRepository;
        private readonly IUserService _userService;
        private readonly IUserEventService _userEventService;
        private readonly IQuestionService _questionService;
        private readonly ITagsService _tagsService;
        private readonly IAnswerService _answerService;
        private readonly ISiteSettingsService _settingsService;
        private readonly IQuestionTemplateService _questionTemplateService;
        private readonly IQuestionCommentService _questionCommentService;
        private readonly IAnswerFlagService _answerFlagService;

        public HomeController()
        {
            _mongoRepository = AppKernel.GetInstance<IMongoRepository1>();
            _userService = AppKernel.GetInstance<IUserService>();
            _userEventService = AppKernel.GetInstance<IUserEventService>();
            _questionService = AppKernel.GetInstance<IQuestionService>();
            _tagsService = AppKernel.GetInstance<ITagsService>();
            _answerService = AppKernel.GetInstance<IAnswerService>();
            _settingsService = AppKernel.GetInstance<ISiteSettingsService>();
            _questionTemplateService = AppKernel.GetInstance<IQuestionTemplateService>();
            _questionCommentService = AppKernel.GetInstance<QuestionCommentService>();
            _answerFlagService = AppKernel.GetInstance<AnswerFlagService>();
        }


        [CMSAuthorize("UseCMS")]
        public ActionResult Index()
        {
            var user = _userService.FindById(MembershipHelper.GetActiveUserId);
            if (user.IsItokLeader)
            {
                return RedirectToAction("LeaderIndex", "Home");
            }
            var model = _userEventService.GetUserEvents(user);
            ViewBag.ItokLeaderEmail = _settingsService.ByKey("ITOK.Leader.Email").Value;
            return View(model);
        }

        [CMSAuthorize("UseCMS")]
        public ActionResult LeaderIndex()
        {
            var alltag = _tagsService.All();
            var model = new UserEvent
            {
                Events = alltag.Where(i => i.TagType == TagType.Events).OrderBy(i => i.DisplayText)
                         .Select(x => new Tags { DisplayText = x.DisplayText, Id = x.Id }).ToList(),
                Categories = alltag.Where(i => i.TagType == TagType.Category).OrderBy(i => i.DisplayText)
                            .Select(x => new Tags { DisplayText = x.DisplayText, Id = x.Id }).ToList(),
                Areas = alltag.Where(i => i.TagType == TagType.Area).OrderBy(i => i.DisplayText)
                    .Select(x => new Tags { DisplayText = x.DisplayText, Id = x.Id }).ToList()
            };
            model.QuestionList = new List<UserQuestion>();
            var answer = _answerService.All();
            bool IsAttachment = false;
            bool isAnswered = false;
            foreach (var item in answer)
            {
                IsAttachment = false;
                if (item.AnswerType == AnswerType.Attachment)
                {
                    isAnswered = !string.IsNullOrEmpty(item.AttachmentId) ? true : false;
                    IsAttachment = true;
                }

                // for empty answers
                if (string.IsNullOrEmpty(item.AnswerValue) && item.AnswerType != AnswerType.Attachment && item.AnswerType != AnswerType.Template)
                {
                    isAnswered = false;
                }

                if (item.AnswerType == AnswerType.Template)
                {
                    isAnswered = true;
                }

                if (!string.IsNullOrEmpty(item.AnswerValue) && (item.AnswerType == AnswerType.Numeric || item.AnswerType == AnswerType.Text || item.AnswerType == AnswerType.Options))
                {
                    isAnswered = true;
                }

                var username = "";
                if (!string.IsNullOrEmpty(item.UpdatedBy))
                {
                    var user = _userService.FindById(item.UpdatedBy);
                    if (user != null)
                    {
                        username = user.Forename + " " + user.Surname;
                    }
                }
                model.QuestionList.Add(new UserQuestion()
                {
                    EventId = item.EventId,
                    YearId = item.YearId,
                    AreaId = item.AreaId,
                    QuestionId = item.QuestionId,
                    UpdatedBy = username,
                    UpdatedDate = item.UpdatedDate,
                    isAnswered = isAnswered,
                    Question = new Question()
                    {
                        QuestionText = item.QuestionText,
                        Id = item.QuestionId,
                        IsAttachment = IsAttachment,
                        AnswerType = item.AnswerType
                    },
                    AnswerId = item.Id,

                });


            }
            FillAnswerFlagAndCheckCommentForItokLeader(model.QuestionList);
            return View("ItokLeaderIndex", model);
        }
        [CMSAuthorize("UseCMS")]
        public ActionResult Questions(string yearId, string areaId, string eventId)
        {
            var model = _userEventService.GetUserEventWithQuestions(MembershipHelper.GetActiveUserId, yearId, areaId, eventId);
            if (model != null)
            {
                FillAnswerFlagAndCheckComment(areaId, eventId, yearId, model.QuestionList);
            }

            return View(model);
        }

        public string RemoveAttachment(string questionId, string eventId, string yearId, string areaId, string attachmentId)
        {
            var model = _answerService.Find(questionId, eventId, yearId, areaId);

            if (model != null)
            {
                List<string> attachmentsList = new List<string>();
                attachmentsList.AddRange(model.AttachmentId.Split(','));

                attachmentsList.Remove(attachmentId);
                model.AttachmentId = string.Join(",", attachmentsList.ToArray());

                _answerService.Save(model);
            }

            return "ok";
        }


        public ActionResult GetAttachments(string questionId, string eventId, string yearId, string areaId)
        {
            var model = _answerService.Find(questionId, eventId, yearId, areaId);
            var _mediaService = new ITOK.Core.Services.CMS.MediaService(new MongoRepository());
            if (model != null)
            {
                if (!string.IsNullOrEmpty(model.AttachmentId))
                {
                    model.Attachments = _mediaService.FindByIds(null, model.AttachmentId.Split(','));
                }
            }
            else
            {
                return null;
            }

            return PartialView("_GetAttachments", model);
        }

        public ActionResult GetQuestionDetail(string questionId, string eventId, string yearId, string areaId)
        {
            Answer model = null;
            var question = _questionService.FindById(questionId);
            var user = _userService.FindById(MembershipHelper.GetActiveUserId);
            var userEvent = user.UserEvents
                .FirstOrDefault(x => x.AreaId == areaId && x.YearId == yearId && x.EventId == eventId);
            if (userEvent == null)
            {
                // itok leader case
                model = _answerService.Find(questionId, eventId, yearId, areaId);
                user = _userService.FindById(model.AnsweredBy);
                userEvent = user.UserEvents
                    .FirstOrDefault(x => x.AreaId == areaId && x.YearId == yearId && x.EventId == eventId);
            }
            var eventquestion = userEvent.QuestionList.FirstOrDefault(x => x.QuestionId.Trim() == questionId);

            if (eventquestion.isAnswered && model == null)
            {
                model = _answerService.Find(questionId, eventId, yearId, areaId);
            }

            if (model == null)
            {
                model = new Answer
                {
                    QuestionId = question.Id,
                    UserId = MembershipHelper.GetActiveUserId,
                    QuestionText = question.QuestionText,
                    OptionType = question.OptionType,
                    AnswerType = question.AnswerType,
                    TemplateType = question.QuestionTemplates
                };

                var tagEvent = _tagsService.FindById(eventId);
                var tagYear = _tagsService.FindById(yearId);
                var tagArea = _tagsService.FindById(areaId);
                model.AnswerTags.Add(new AnswerTag { Text = tagEvent.DisplayText, UrlSlug = tagEvent.UrlSlug });
                model.AnswerTags.Add(new AnswerTag { Text = tagYear.DisplayText, UrlSlug = tagYear.UrlSlug });
                model.AnswerTags.Add(new AnswerTag { Text = tagArea.DisplayText, UrlSlug = tagArea.UrlSlug });
                model.EventId = tagEvent.Id;
                model.YearId = tagYear.Id;
                model.AreaId = tagArea.Id;
            }
            if (question.Options != null)
            {
                model.OptionList = question.Options.Split(',').ToList();
            }

            if (model.AnswerValue == null)
            {
                model.AnswerValue = "";
            }

            if (model.AnswerType == AnswerType.Template)
            {
                if (model.TemplateType == QuestionTemplateType.TransportationTemplate)
                {
                    if (model.QuestionTemplate == null)
                    {
                        model.QuestionTemplate = _questionTemplateService.GetTransportationTemplateObject(false, model.AreaId, model.EventId, model.YearId, model.QuestionId);
                    }

                    return PartialView("_TransportationTemplate", model);
                }
                else if (model.TemplateType == QuestionTemplateType.VolunteersPerArea)
                {
                    if (model.QuestionTemplate == null)
                    {
                        model.QuestionTemplate = _questionTemplateService.GetVolunteersPerAreaObject(false, model.AreaId, model.EventId, model.YearId, model.QuestionId);
                    }

                    return PartialView("_VolunteersPerArea", model);
                }
                else if (model.TemplateType == QuestionTemplateType.StaffHiredInLOC)
                {
                    if (model.QuestionTemplate == null)
                    {
                        model.QuestionTemplate = _questionTemplateService.GetStaffHiredInLOCObject(false, model.AreaId, model.EventId, model.YearId, model.QuestionId);
                    }

                    return PartialView("_StaffHiredInLOC", model);
                }
                else if (model.TemplateType == QuestionTemplateType.SiteVisitsSummary)
                {
                    if (model.QuestionTemplate == null)
                    {
                        model.QuestionTemplate = _questionTemplateService.GetSiteVisitsSummaryObject(false, model.AreaId, model.EventId, model.YearId, model.QuestionId);
                    }

                    return PartialView("_SiteVisitsSummary", model);
                }
                else if (model.TemplateType == QuestionTemplateType.CarpoolUsagePerDay)
                {
                    if (model.QuestionTemplate == null)
                    {
                        model.QuestionTemplate = _questionTemplateService.GetCarpoolUsagePerDayObject(false, model.AreaId, model.EventId, model.YearId, model.QuestionId);
                    }

                    return PartialView("_CarpoolUsagePerDay", model);
                }
                else if (model.TemplateType == QuestionTemplateType.AccommodationFormUpdatedbyEndofEvent)
                {
                    if (model.QuestionTemplate == null)
                    {
                        model.QuestionTemplate = _questionTemplateService.GetAccommodationFormUpdatedObject(false, model.AreaId, model.EventId, model.YearId, model.QuestionId);
                    }

                    return PartialView("_AccommodationFormUpdated", model);
                }

                else
                {
                    return PartialView("", null);
                }

            }
            else
            {
                return PartialView("_AnswerEdit", model);
            }
        }

        public ActionResult GetQuestionCommentsByAnswerId(string answerId, string questionText)
        {
            QuestionComment model = null;
            if (answerId != null)
            {
                model = new QuestionComment
                {
                    AnswerId = answerId
                };

                var comments = _questionCommentService.FindByAnswerId(answerId);
                if (comments != null)
                {
                    foreach (var ucomment in comments.UsersComment)
                    {
                        var user = _userService.FindById(ucomment.UserId);
                        ucomment.UserName = user.Forename + " " + user.Surname;
                    }
                    model = comments;
                }
                model.QuestionText = questionText;
            }
            return PartialView("_QuestionComment", model);
        }

        public ActionResult GetQuestionComments(string questionId, string questionText, string eventId, string yearId, string areaId)
        {
            QuestionComment model = null;
            var answerIfAvailable = _answerService.Find(questionId, eventId, yearId, areaId);
            if (answerIfAvailable != null)
            {
                model = new QuestionComment
                {
                    AnswerId = answerIfAvailable.Id
                };

                var comments = _questionCommentService.FindByAnswerId(answerIfAvailable.Id);

                if (comments != null)
                {
                    foreach (var ucomment in comments.UsersComment)
                    {
                        var user = _userService.FindById(ucomment.UserId);
                        ucomment.UserName = user.Forename + " " + user.Surname;
                    }
                    model = comments;
                }
                model.QuestionText = questionText;
            }
            return PartialView("_QuestionComment", model);
        }

        public ActionResult GetAnswerDetail(string questionId)
        {
            Answer model = null;
            model = _answerService.FindByQues(questionId);
            var question = _questionService.FindById(questionId);
            if (model == null)
            {
                model = new Answer
                {
                    QuestionId = question.Id,
                    UserId = MembershipHelper.GetActiveUserId,
                    QuestionText = question.QuestionText,
                    OptionType = question.OptionType,
                    AnswerType = question.AnswerType
                };

            }
            if (question.Options != null)
            {
                model.OptionList = question.Options.Split(',').ToList();
            }

            if (model.AnswerValue == null)
            {
                model.AnswerValue = "";
            }
            return PartialView("_AnswerEdit", model);
        }
        [HttpPost]
        public JsonResult SaveAnswer(Answer answer)
        {
            var question = _questionService.FindById(answer.QuestionId);
            var answerIfAvailable = _answerService.Find(answer.QuestionId, answer.EventId, answer.YearId, answer.AreaId);
            var user = _userService.FindById(MembershipHelper.GetActiveUserId);

            var userEvent = user.UserEvents
                .FirstOrDefault(x => x.AreaId == answer.AreaId && x.YearId == answer.YearId && x.EventId == answer.EventId);
            if (userEvent == null)
            {
                // itok leader case
                user = _userService.FindById(answerIfAvailable.AnsweredBy);
                userEvent = user.UserEvents
                    .FirstOrDefault(x => x.AreaId == answer.AreaId && x.YearId == answer.YearId && x.EventId == answer.EventId);
            }
            var eventquestion = userEvent.QuestionList.FirstOrDefault(x => x.QuestionId.Trim() == answer.QuestionId);
            //get category from area id and include in answertags array
            var area = _tagsService.FindById(userEvent.AreaId);
            var areaCategory = _tagsService.FindById(area.ParentId);
            if (areaCategory != null)
            {
                var category = new AnswerTag()
                {
                    TagType = TagType.Category,
                    Text = areaCategory.DisplayText,
                    UrlSlug = areaCategory.UrlSlug
                };
                answer.AnswerTags.Add(category);
            }
            if (eventquestion != null)
            {
                if (answer.AnswerValues.Count > 0 | !string.IsNullOrEmpty(answer.AnswerValue))
                {
                    eventquestion.isAnswered = true;
                }
                else if (answer.AnswerType == AnswerType.Attachment && !string.IsNullOrEmpty(answer.AttachmentId))
                {
                    eventquestion.isAnswered = true;
                    answer.QuestionText = question.QuestionText;
                    answer.QuestionId = question.Id;
                }
                else
                {
                    eventquestion.isAnswered = false;
                }
            }
            if (question.Options != null)
            {
                answer.OptionList = question.Options.Split(',').ToList();
            }

            _userService.Save(user);

            answer.UserId = MembershipHelper.GetActiveUserId;

            if (string.IsNullOrEmpty(answer.Id))
            {
                answer.AnsweredBy = MembershipHelper.GetActiveUserId;
                answer.AnsweredDate = DateTime.UtcNow;
                answer.UpdatedBy = MembershipHelper.GetActiveUserId;
                answer.UpdatedDate = DateTime.UtcNow;
            }
            else
            {
                if (answer.AnswerType == AnswerType.Attachment && !string.IsNullOrEmpty(answer.AttachmentId)
                    && !string.IsNullOrEmpty(answerIfAvailable.AttachmentId))
                {
                    answer.AttachmentId = answerIfAvailable.AttachmentId + "," + answer.AttachmentId;
                }

                answer.AnsweredBy = answerIfAvailable.AnsweredBy;
                answer.AnsweredDate = answerIfAvailable.AnsweredDate;
                answer.UpdatedBy = MembershipHelper.GetActiveUserId;
                answer.UpdatedDate = DateTime.UtcNow;
            }

            _answerService.Save(answer);

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveComment(QuestionComment questionComment)
        {
            try
            {
                var _usersComment = new UsersComment
                {
                    Comment = questionComment.CommentText,
                    CommentDate = DateTime.UtcNow,
                    UserId = MembershipHelper.GetActiveUserId
                };
                if (!string.IsNullOrEmpty(questionComment.Id))
                {
                    questionComment.UsersComment = _questionCommentService.FindByCommentId(questionComment.Id).UsersComment;
                }

                questionComment.UsersComment.Add(_usersComment);
                _questionCommentService.Save(questionComment);
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UpdateComment(QuestionComment questionComment)
        {
            try
            {
                if (!string.IsNullOrEmpty(questionComment.Id))
                {
                    var _comments = _questionCommentService.FindByCommentId(questionComment.Id);
                    var selectedDate = Convert.ToDateTime(questionComment.UpdateCommentDate);
                    if (_comments != null)
                    {
                        foreach (var item in _comments.UsersComment)
                        {
                            if (item.CommentDate.ToString("MM/dd/yyyy hh:mm:ss") == selectedDate.ToString("MM/dd/yyyy hh:mm:ss"))
                            {
                                item.Comment = questionComment.CommentText;
                                item.UserId = MembershipHelper.GetActiveUserId;
                            }
                        }
                    }
                    _questionCommentService.Save(_comments);
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DeleteComment(QuestionComment questionComment)
        {
            try
            {
                var totalUserCommentCount = 1;
                if (!string.IsNullOrEmpty(questionComment.Id))
                {
                    var _comments = _questionCommentService.FindByCommentId(questionComment.Id);
                    var selectedDate = Convert.ToDateTime(questionComment.UpdateCommentDate);

                    if (_comments != null)
                    {
                        totalUserCommentCount = _comments.UsersComment.Count;
                        foreach (var item in _comments.UsersComment)
                        {
                            if (item.CommentDate.ToString("MM/dd/yyyy hh:mm:ss") == selectedDate.ToString("MM/dd/yyyy hh:mm:ss"))
                            {
                                _comments.UsersComment.Remove(item);
                                totalUserCommentCount = totalUserCommentCount - 1;
                                break;
                            }
                        }
                    }
                    _questionCommentService.Save(_comments);
                }
                return Json(new { success = true, totalUserCommentCount = totalUserCommentCount }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SetFlagColor(AnswerFlag answerFlag)
        {
            try
            {
                if (string.IsNullOrEmpty(answerFlag.Id))
                {
                    answerFlag.SetColorBy = MembershipHelper.GetActiveUserId;
                    answerFlag.SetColorDate = DateTime.UtcNow;
                    answerFlag.UpdatedBy = MembershipHelper.GetActiveUserId;
                    answerFlag.UpdatedDate = DateTime.UtcNow;
                }
                else
                {
                    answerFlag.SetColorBy = MembershipHelper.GetActiveUserId;
                    answerFlag.SetColorDate = DateTime.UtcNow;
                    answerFlag.UpdatedBy = MembershipHelper.GetActiveUserId;
                    answerFlag.UpdatedDate = DateTime.UtcNow;
                }
                _answerFlagService.Save(answerFlag);
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SaveAnswerAttachment(string questionId, string attachmentId, string eventId, string yearId, string areaId)
        {
            int attachmentCount = 0;
            var model = new Answer
            {
                QuestionId = questionId,
                AttachmentId = attachmentId,
                AnswerType = AnswerType.Attachment,
                EventId = eventId,
                YearId = yearId,
                AreaId = areaId
            };

            var dbAnswer = _answerService.Find(questionId, eventId, yearId, areaId);

            if (dbAnswer != null)
            {
                model.Id = dbAnswer.Id;

                if (dbAnswer.AnswerType == AnswerType.Attachment && !string.IsNullOrEmpty(attachmentId))
                {
                    if (string.IsNullOrEmpty(dbAnswer.AttachmentId))
                    {
                        attachmentCount = 1;
                    }
                    else
                    {
                        var _attachmentIds = attachmentId + "," + dbAnswer.AttachmentId;
                        attachmentCount = _attachmentIds.Split(',').Count();
                    }
                }
            }

            var tagEvent = _tagsService.FindById(eventId);
            var tagYear = _tagsService.FindById(yearId);
            var tagArea = _tagsService.FindById(areaId);
            if (tagEvent != null)
            {
                model.AnswerTags.Add(new AnswerTag { Text = tagEvent.DisplayText, UrlSlug = tagEvent.UrlSlug });
            }

            if (tagYear != null)
            {
                model.AnswerTags.Add(new AnswerTag { Text = tagYear.DisplayText, UrlSlug = tagYear.UrlSlug });
            }

            if (tagArea != null)
            {
                model.AnswerTags.Add(new AnswerTag { Text = tagArea.DisplayText, UrlSlug = tagArea.UrlSlug });
            }

            SaveAnswer(model);

            return Json(new { success = true, attachmentCount = attachmentCount }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveTemplateAnswer(QuestionTemplate model)
        {

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public JsonResult BindYearByEvent(string eventid)
        {
            var model = new UserEvent
            {
                Years = _tagsService.FindByParentId(TagType.Year, eventid)
                    .Select(x => new Tags { DisplayText = x.DisplayText, Id = x.Id })
                    .ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        [CMSAuthorize("UseCMS")]
        public JsonResult BindAreaByCategory(string categoryId)
        {
            var model = new UserEvent
            {
                Years = _tagsService.FindByParentId(TagType.Area, categoryId)
                    .Select(x => new Tags { DisplayText = x.DisplayText, Id = x.Id })
                    .ToList()
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        [CMSAuthorize("UseCMS")]
        public JsonResult BindSearch(string SearchText, string EventId, string YearId, string AreaId)
        {
            var model = new UserEvent();
            model.QuestionList = new List<UserQuestion>();
            var answer = _answerService.FindSearch(SearchText, EventId, YearId, AreaId);
            bool IsAttachment = false;
            bool isAnswered = false;
            foreach (var item in answer)
            {
                IsAttachment = false;
                if (item.AnswerType == AnswerType.Attachment)
                {
                    isAnswered = !string.IsNullOrEmpty(item.AttachmentId) ? true : false;
                    IsAttachment = true;
                }

                // for empty answers
                if (string.IsNullOrEmpty(item.AnswerValue) && item.AnswerType != AnswerType.Attachment && item.AnswerType != AnswerType.Template)
                {
                    isAnswered = false;
                }

                if (item.AnswerType == AnswerType.Template)
                {
                    isAnswered = true;
                }

                if (!string.IsNullOrEmpty(item.AnswerValue) && (item.AnswerType == AnswerType.Numeric || item.AnswerType == AnswerType.Text || item.AnswerType == AnswerType.Options))
                {
                    isAnswered = true;
                }

                var username = "";
                if (!string.IsNullOrEmpty(item.UpdatedBy))
                {
                    var user = _userService.FindById(item.UpdatedBy);
                    username = user.Forename + " " + user.Surname;
                }
                model.QuestionList.Add(new UserQuestion()
                {
                    EventId = item.EventId,
                    YearId = item.YearId,
                    AreaId = item.AreaId,
                    QuestionId = item.QuestionId,
                    UpdatedBy = username,
                    UpdatedDate = item.UpdatedDate,
                    isAnswered = isAnswered,
                    Question = new Question()
                    {
                        QuestionText = item.QuestionText,
                        Id = item.QuestionId,
                        IsAttachment = IsAttachment,
                        AnswerType = item.AnswerType
                    },
                    AnswerId = item.Id

                });
            }
            FillAnswerFlagAndCheckCommentForItokLeader(model.QuestionList);

            string partialString = string.Empty;
            foreach (var item in model.QuestionList)
            {
                partialString = partialString + ConvertViewToString("_AnswerItemItokLeader", item);
            }

            return Json(partialString, JsonRequestBehavior.AllowGet);

        }
        private string ConvertViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (StringWriter writer = new StringWriter())
            {
                ViewEngineResult vResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext vContext = new ViewContext(this.ControllerContext, vResult.View, ViewData, new TempDataDictionary(), writer);
                vResult.View.Render(vContext, writer);
                return writer.ToString();
            }
        }

        private void FillAnswerFlagAndCheckComment(string areaId, string eventId, string yearId, List<UserQuestion> questions)
        {
            foreach (var item in questions)
            {
                var ansModel = _answerService.Find(item.QuestionId.Trim(), eventId, yearId, areaId);
                if (ansModel != null)
                {
                    var ansId = ansModel.Id;
                    item.AnswerId = ansId;

                    #region Check Iscomment
                    var comments = _questionCommentService.FindByAnswerId(ansId);
                    if (comments != null)
                    {
                        if (comments.UsersComment.Count > 0)
                        {
                            item.isCommented = true;
                        }
                    }


                    #endregion

                    #region Get flag data
                    var flagData = _answerFlagService.FindByAnswerId(ansId);
                    if (flagData != null)
                    {
                        item.isFlagSet = true;
                        item.FlagColor = flagData.FlagColor;
                        item.AnswerFlagId = flagData.Id;
                    }
                    #endregion

                    #region Get attachment count

                    if (ansModel.AnswerType == AnswerType.Attachment)
                    {
                        if (!string.IsNullOrEmpty(ansModel.AttachmentId))
                        {
                            item.AttachmentCount = ansModel.AttachmentId.Split(',').Count();
                        }
                        else
                        {
                            item.isAnswered = false;
                        }
                    }

                    // for empty answers
                    if (string.IsNullOrEmpty(ansModel.AnswerValue) && ansModel.AnswerType != AnswerType.Attachment && ansModel.AnswerType != AnswerType.Template)
                    {
                        item.isAnswered = false;
                    }
                    if (ansModel.AnswerType == AnswerType.Template)
                    {
                        if (isTemplateAnswerAvailable(ansModel.QuestionTemplate.Questions))
                        {
                            item.isAnswered = true;
                        }
                        else
                        {
                            item.isAnswered = false;
                        }
                    }

                    #endregion
                }
                else
                {
                    item.isAnswered = false;
                }
            }
        }
        private void FillAnswerFlagAndCheckCommentForItokLeader(List<UserQuestion> questions)
        {
            foreach (var item in questions)
            {
                if (item.isAnswered)
                {
                    string answerId = item.AnswerId;

                    #region Check Iscomment
                    var comments = _questionCommentService.FindByAnswerId(answerId);
                    if (comments != null)
                    {
                        if (comments.UsersComment.Count > 0)
                        {
                            item.isCommented = true;
                        }
                    }

                    #endregion

                    #region Get flag data

                    var flagData = _answerFlagService.FindByAnswerId(answerId);
                    if (flagData != null)
                    {
                        item.isFlagSet = true;
                        item.FlagColor = flagData.FlagColor;
                        item.AnswerFlagId = flagData.Id;
                    }
                    #endregion

                    #region Get attachment count
                    var ansModel = _answerService.FindById(answerId);
                    if (ansModel.AnswerType == AnswerType.Attachment && !string.IsNullOrEmpty(ansModel.AttachmentId))
                    {
                        item.AttachmentCount = ansModel.AttachmentId.Split(',').Count();
                    }
                    #endregion

                }
            }
        }
        
        private bool isTemplateAnswerAvailable(List<QuestionRow> questions)
        {
            foreach (var question in questions)
            {
                foreach (var answers in question.AnswersRows)
                {
                    if (answers.AnswerNumber != null || answers.AnswerString != null)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}