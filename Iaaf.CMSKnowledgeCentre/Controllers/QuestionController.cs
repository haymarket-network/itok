﻿using ITOK.Core.Common;
using ITOK.Core.Data;
using ITOK.Core.Services.CMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITOK.Core.Data.Model;
using ITOK.Core.Services.Core;

namespace CMS.Controllers
{
    public class QuestionController : Controller
    {
        private readonly IQuestionService _questionService;

        private IMongoRepository1 mongoRepository;


        public QuestionController()
        {
            mongoRepository = AppKernel.GetInstance<IMongoRepository1>();
            _questionService = AppKernel.GetInstance<IQuestionService>();

        }

        public QuestionController(IMongoRepository1 _mongoRepository)
        {
            this.mongoRepository = _mongoRepository;
            _questionService = AppKernel.GetInstance<IQuestionService>();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetQuestionDetail(string ParentTagSlug, string ParentTagId, string questionId = "")
        {
            Question question = new Question();
            if (!string.IsNullOrEmpty(questionId))
            {
                question = _questionService.FindById(questionId);
            }
            else
            {
                question.ParentTagId = ParentTagId;
                question.ParentTagSlug = ParentTagSlug;
            }



            return PartialView(question);
        }

        [HttpPost]
        public ActionResult GetQuestionDetail(Question question)
        {
            _questionService.Save(question);

            List<Question> allQuestions = _questionService.All().Where(i => i.ParentTagId == question.ParentTagId).ToList();

            return PartialView("QuestionList", allQuestions);
        }
        public ActionResult RemoveQuestion(string tagId, string questionid)
        {
            Question qToDelete = _questionService.FindById(questionid);
            _questionService.Delete(questionid);

            List<Question> allQuestions = _questionService.All().Where(i => i.ParentTagId == qToDelete.ParentTagId).ToList();

            return PartialView("QuestionList", allQuestions);
        }


    }
}