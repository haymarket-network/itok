﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using CMS.Helpers;
using CMS.Helpers.Authorise;
using CMS.Models;
using ITOK.Core.Common;
using ITOK.Core.Common.Constants;
using ITOK.Core.Common.Utilities;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using ITOK.Core.Data.Model.HS;
using ITOK.Core.Services.CMS;
using ITOK.Core.Services.Security;
using ITOK.Core.ViewModels;
using ITagsService = ITOK.Core.Services.Core.ITagsService;

namespace CMS.Controllers
    {
    public class ArticleController : BaseController
        {
        //private const string ArticleModelsCacheKey = "ArticleModelsCache";
        private const int MaxMediaResultsPerTabView = 12;

        private const int MaxDiaristSearchResults = 12;
        private const string PaginationMainIndexInstanceKey = "ArticleMainIndex";
        private const string PaginationFeaturedIndexInstanceKey = "ArticleFeaturedIndex"; // Not used yet but probably will be in the future
        private const string UserResponseInstanceKey = "Article";
        private const string ShowDeletedArticlesCacheKey = "ShowDeletedArticlesCache";
        private const string ShowShowArticleTypeCacheKey = "ShowShowArticleTypeCache";
        private const string PaginationBaseUrl = "/Article/";
        private const string ArticleFilterTypeCacheKey = "ArticleFilterTypeCache";

        private const string ArticleSearchTextCacheKey = "ArticleSearchTextCache";
        private const string ArticleSearchCountCacheKey = "ArticleSearchCountCache";
        private const string EventFilterCacheKey = "ArticleFilterCacheKey";

        private const string SelectedBrandCacheKey = "SelectedBrandCacheKey";

        // Helpers
        private readonly PaginationHelper _mainlistPageHelper;

        private readonly UserResponseHelper _responseHelper;

        private IMongoRepository1 mongoRepository;

        // Services
        private readonly ArticleService _articleService;

        private readonly MediaService _mediaService;
        private readonly ITagsService _tagsService;
        private readonly UserService _userService;

        private readonly ArticleCommentService _articleCommentService;
        private readonly UserService _UserService;
        private readonly JobRoleService _jobRoleService;

        // Hydration settings
        private static readonly ArticleHydrationSettings _articleHydrationSettings;

        static ArticleController()
            {
            _articleHydrationSettings = new ArticleHydrationSettings
            {
                PrimaryMedia = new MediaHydrationSettings(),
                RelatedMedia = new MediaHydrationSettings(),
                RelatedArticles = new ArticleHydrationSettings(),
                RelatedJobs = new JobRoleHydrationSettings(),
                CreatedBy = new UserHydrationSettings(),
                UpdatedBy = new UserHydrationSettings(),
            };
            }

        public ArticleController()
            {
            mongoRepository = AppKernel.GetInstance<IMongoRepository1>();

            _articleService = new ArticleService(mongoRepository);
            _mediaService = new MediaService(mongoRepository);
            _tagsService = AppKernel.GetInstance<ITagsService>();
            _userService = new UserService(mongoRepository);
            _articleCommentService = new ArticleCommentService(mongoRepository);

            _mainlistPageHelper = new PaginationHelper(PaginationMainIndexInstanceKey, PaginationBaseUrl);
            _responseHelper = new UserResponseHelper(UserResponseInstanceKey);

            _UserService = new UserService(mongoRepository);

            _jobRoleService = new JobRoleService(mongoRepository);
            }

        public ArticleController(IMongoRepository1 _mongoRepository)
            {
            this.mongoRepository = _mongoRepository;
            _articleService = new ArticleService(mongoRepository);
            _mediaService = new MediaService(mongoRepository);
            _tagsService = AppKernel.GetInstance<ITagsService>();
            _userService = new UserService(mongoRepository);

            _mainlistPageHelper = new PaginationHelper(PaginationMainIndexInstanceKey, PaginationBaseUrl);
            _responseHelper = new UserResponseHelper(UserResponseInstanceKey);
            }

        #region Index & Page

        [CMSAuthorize("UseCMS")]
        public ActionResult Index()
            {
            SelectedBrand = "";
            return Page();
            }

        [CMSAuthorize("UseCMS")]
        public PartialViewResult ByArticleType(ArticleType articleType, int pageNumber = -1, bool? showDeletedArticles = null, string selectedBrand = "")
            {
            if (showDeletedArticles != null) ShowDeletedArticles = (bool)showDeletedArticles;
            if (!string.IsNullOrEmpty(selectedBrand)) SelectedBrand = selectedBrand;

            ArticleFilter = articleType;
            // Update current page if necessary
            if (pageNumber != -1)
                {
                _mainlistPageHelper.SetPage(pageNumber);
                }

            return PartialView("Index", getArticleListModel(-1));
            }

        /// <summary>
        ///
        /// </summary>
        /// <param name="newPage"></param>
        /// <param name="selectedArticle">The current state of the selected article to keep track of any changes made by the user</param>
        /// <returns></returns>

        [CMSAuthorize("UseCMS")]
        public ActionResult Page(int pageNumber = -1, bool? showDeletedArticles = null)
            {
            if (showDeletedArticles != null) ShowDeletedArticles = (bool)showDeletedArticles;

            // Update current page if necessary
            if (pageNumber != -1)
                {
                _mainlistPageHelper.SetPage(pageNumber);
                }

            return View("Index", getArticleListModel(pageNumber));
            }

        //[CMSAuthorize("UseCMS")]
        //public PartialViewResult ByArticleType(ArticleType articleType, int pageNumber = -1, bool? showDeletedArticles = null)
        //{
        //    if (showDeletedArticles != null) ShowDeletedArticles = (bool)showDeletedArticles;

        //    ArticleFilter = articleType;

        //    // Update current page if necessary
        //    if (pageNumber != -1)
        //    {
        //        _mainlistPageHelper.SetPage(pageNumber);
        //    }

        //    return PartialView("Index", getArticleListModel(-1));
        //}

        [CMSAuthorize("UseCMS")]
        public PartialViewResult Search(string searchText, int skip)
            {
            var showWithStatus = ShowDeletedArticles ?
                new[] { PublicationStatus.Deleted } :
                new[] { PublicationStatus.Published, PublicationStatus.UnPublished };

            SearchCount = skip + 15;

            var foundArticles = search(searchText, skip, 15);//_articleService.Search(searchText,skip, 15, true, false, false, showWithStatus);
            return PartialView("ArticleList", foundArticles);
            }

        [HttpPost]
        [CMSAuthorize("UseCMS")]
        public JsonResult GetTotalSearchResults(string searchText)
            {
            PublicationStatus[] showWithStatus;
            if (ShowDeletedArticles) showWithStatus = new[] { PublicationStatus.Deleted };
            else showWithStatus = new[] { PublicationStatus.Published, PublicationStatus.UnPublished };

            var resultCount = 0;
            //  resultCount = _articleService.SearchCount(searchText, true, false, showWithStatus, false);

            return Json(resultCount);
            }

        [HttpPost]
        [CMSAuthorize("UseCMS")]
        public void CancelSearch()
            {
            SearchText = string.Empty;
            SearchCount = 0;
            }

        #endregion Index & Page

        #region Create & Edit

        [CMSAuthorize("UseCMS")]
        public ActionResult Create(string type)
            {
            using (var editCache = new EntityEditCache<Article>(x => x.Id))
                {
                var article = new Article();
                var model = new ArticleEditModel { Article = article };

                article.ArticleType = ArticleUtilities.ArticleTypeFromString(type);
                //related event
                model.availableTags = _tagsService.All().Where(i => i.Brand == GlobalVariables.CurrentBrand).ToList();
                editCache.SetEntity(article);

                return View("Edit", model);
                }
            }

        [CMSAuthorize("UseCMS")]
        public ActionResult Edit(string id)
            {
            using (var editCache = new EntityEditCache<Article>(x => x.Id))
                {
                var model = new ArticleEditModel { Article = _articleService.FindById(GlobalVariables.CurrentBrand, id, _articleHydrationSettings) };

                // Find the article to edit
                if (model.Article == null)
                    {
                    // The article wasnt found
                    _responseHelper.AddUserResponse(ResponseType.Error, "Could not find the specified article to edit.");
                    return View("Index", getArticleListModel(-1));
                    }
                // Add the entity to the edit cache
                editCache.SetEntity(model.Article);


                var _article = editCache.GetEntity(id);
                // }
                model.VersionHistory = _articleService.GetVersionHistory(model.Article.Id, new VersionHydrationSettings
                {
                    ModifiedBy = new UserHydrationSettings()
                });

                model.availableTags = _tagsService.All().Where(i => i.Brand == GlobalVariables.CurrentBrand).ToList();
                model.Responses = _responseHelper.UserResponses;

                model.Comments = new List<ArticleCommentItem>();
                var Comments = _articleCommentService.FindByArticleID(id);
                var CommentsUsers = _userService.FindByIds(Comments.Select(x => x.UserIdentification).ToArray());

                
                foreach (var comment in Comments)
                {
                    var userThatMadeComment = CommentsUsers.FirstOrDefault(x => x.Id == comment.UserIdentification);
                    model.Comments.Add(new ArticleCommentItem()
                    {
                        Comment = comment.Comment,
                        UserName = userThatMadeComment.Forename + " " + userThatMadeComment.Surname,
                        UserEmail = userThatMadeComment.Email,
                        CreatedOn = comment.CreatedOn,
                        UpdatedOn = comment.UpdatedOn
                    });
                }


                if (!string.IsNullOrEmpty(model.Article.AuthorUserId))
                    {
                    var user = _UserService.FindById(model.Article.AuthorUserId);
                    if (user != null)
                        {
                        model.AuthorUserName = user.Forename + " " + user.Surname;
                        model.AuthorUserId = user.Id.ToString();
                        }
                    }

               

                return View("Edit", model);
                }
            }

        [CMSAuthorize("UseCMS")]
        public void RefreshEdit(string articleId)
            {
            using (var editHelper = new EntityEditCache<Article>(x => x.Id))
                {
                if (!editHelper.EntityIsAvailable(articleId))
                    editHelper.SetEntity(_articleService.FindById(GlobalVariables.CurrentBrand, articleId, _articleHydrationSettings));
                }
            }

        [HttpGet]
        public JsonResult GroupArticleStats()
            {
            try
                {
                //  _articleService.GroupArticleStats();
                return Json(new { success = true, msg = "Successfully re Grouped Article Stats" }, JsonRequestBehavior.AllowGet);
                }
            catch (Exception ex)
                {
                return Json(new { success = false, msg = "Error: " + ex.Message, stack = ex.StackTrace }, JsonRequestBehavior.AllowGet);
                }
            }

        [HttpGet]
        public JsonResult CleanStats()
            {
            try
                {
                // _articleService.CleanStatistics();
                return Json(new { success = true, msg = "Successfully cleaned out old statistics" }, JsonRequestBehavior.AllowGet);
                }
            catch (Exception ex)
                {
                return Json(new { success = false, msg = "Error: " + ex.Message, stack = ex.StackTrace }, JsonRequestBehavior.AllowGet);
                }
            }

        #region Primary Media

        [CMSAuthorize("UseCMS")]
        public void RemovePrimaryMedia(string articleId)
            {
            using (var editHelper = new EntityEditCache<Article>(x => x.Id))
                {
                // Remove the primary media item from the cached article
                var article = editHelper.GetEntity(articleId);
                article.PrimaryMedia = null;
                }
            }

        [CMSAuthorize("UseCMS")]
        public PartialViewResult AddPrimaryMedia(string articleId, string mediaId)
            {
            if (string.IsNullOrEmpty(mediaId)) throw new ArgumentException("A Media Id is required", "mediaId");
            using (var editHelper = new EntityEditCache<Article>(x => x.Id))
                {
                var article = editHelper.GetEntity(articleId);
                if (article == null) throw new Exception("Article is not in cache");
                // Find the specified media item
                var media = _mediaService.FindByIds(null, mediaId).FirstOrDefault();
                if (media == null) throw new Exception("Couldn't find the specified media item with id: " + mediaId);

                article.PrimaryMedia = media;

                return PartialView("EditMediaItem", media);
                }
            }

        #endregion Primary Media

        #region RelatedMedia

        [CMSAuthorize("UseCMS")]
        public void RemoveRelatedMedia(string articleId, string mediaId)
            {
            if (string.IsNullOrEmpty(mediaId)) throw new ArgumentException("A Media Id is required", "mediaId");
            using (var editHelper = new EntityEditCache<Article>(x => x.Id))
                {
                var article = editHelper.GetEntity(articleId);
                if (article == null) throw new Exception("Article is not in cache");
                var media = article.RelatedMedia.SingleOrDefault(x => x.Id == mediaId);
                if (media == null) throw new Exception("Could not find the media item with the specified media ID");
                article.RelatedMedia.Remove(media);
                }
            }

        [CMSAuthorize("UseCMS")]
        public PartialViewResult AddRelatedMedia(string articleId, string mediaId)
            {
            if (string.IsNullOrEmpty(mediaId)) throw new ArgumentException("A Media Id is required", "mediaId");
            using (var editHelper = new EntityEditCache<Article>(x => x.Id))
                {
                var article = editHelper.GetEntity(articleId);
                if (article == null) throw new Exception("Article is not in cache");
                // Check that the current article doesnt already have the specified media item with profile
                if (article.RelatedMedia.Any(x => x.Id == mediaId))
                    throw new Exception("The specified media item already exist for the current article");
                // Get the media item by id
                var media = _mediaService.FindByIds(null, mediaId).FirstOrDefault();
                if (media == null) throw new Exception("Could not find a media item with the specified ID");
                // Add the media item to the current article
                article.RelatedMedia.Add(media);
                // Render a new partial for the client
                return PartialView("EditMediaItem", media);
                }
            }

        #endregion RelatedMedia


        [CMSAuthorize("UseCMS")]
        public ActionResult CloneArticle (string articleId)
        {
            var clone =  _articleService.FindById(GlobalVariables.CurrentBrand, articleId, _articleHydrationSettings);

            clone.Id = null;
            clone.Title = clone.Title + " Cloned";
            clone.SEOTitle = clone.SEOTitle + " Cloned";
            clone.UrlSlug = null;
            clone.Status = PublicationStatus.UnPublished;
            _articleService.Save(clone, "Clone Created");
            


            return RedirectToAction("Edit", new { id = clone.Id });
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="articleId">The id of the article being edited</param>
        /// <param name="article">Partial data of the article being edited. This must be mapped onto the existing mapped article.</param>
        /// <param name="submit"></param>
        /// <returns></returns>
        [CMSAuthorize("UseCMS")]
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult EditAction(string articleId, Article article, string submit, string versionComment, string UpdateVersion, string authorUserId, string[] tags)
            {
            
            switch (submit)
                {
                case "Save": return save(article, articleId, versionComment, UpdateVersion == "yes" ? true : false, authorUserId, tags);
                case "Cancel": return cancel(articleId);
                }
            throw new Exception("Unrecognised EditAction");
            }

        [CMSAuthorize("UseCMS")]
        public ActionResult Delete(string articleId)
            {
            try
                {
                // Get the article to delete
                var article = _articleService.FindByIds(GlobalVariables.CurrentBrand, new ArticleHydrationSettings()
                {
                    PrimaryMedia = new MediaHydrationSettings(),
                    RelatedMedia = new MediaHydrationSettings(),

                    RelatedArticles = new ArticleHydrationSettings(),
                }, articleId).FirstOrDefault();
                if (article == null)
                    {
                    _responseHelper.AddUserResponse(ResponseType.Error, "Could not find the specified article to delete");
                    return RedirectToAction("Index");
                    }

                // Delete from cache if it exists
                var editHelper = new EntityEditCache<Article>(x => x.Id);
                if (editHelper.EntityIsAvailable(articleId)) editHelper.ClearCache(articleId);

                article.Status = PublicationStatus.Deleted;
                _articleService.Save(article);

                _responseHelper.AddUserResponse(ResponseType.Notice, string.Format("Successfully deleted article: {0}", article.Title));
                return RedirectToAction("Index");
                }
            catch (Exception ex)
                {
                _responseHelper.AddUserResponse(ResponseType.Error, "Error", ex.Message);
                return RedirectToAction("Index");
                }
            }

        [CMSAuthorize("UseCMS")]
        public ActionResult HardDelete(string articleId)
            {
            // Get the article to delete
            var article = _articleService.FindByIds(GlobalVariables.CurrentBrand, null, articleId).FirstOrDefault();
            if (article == null)
                {
                _responseHelper.AddUserResponse(ResponseType.Error, "Could not find the specified article to delete");
                return RedirectToAction("Index");
                }

            _articleService.DeleteById(articleId, false);

            _responseHelper.AddUserResponse(ResponseType.Notice, string.Format("Successfully deleted article: {0}", article.Title));
            return RedirectToAction("Index");
            }

        [CMSAuthorize("UseCMS")]
        public ActionResult Restore(string articleId)
            {
            var article = _articleService.FindByIds(GlobalVariables.CurrentBrand, new ArticleHydrationSettings
            {
                PrimaryMedia = new MediaHydrationSettings(),
                RelatedMedia = new MediaHydrationSettings(),
                RelatedArticles = new ArticleHydrationSettings(),
            }, articleId).FirstOrDefault();
            if (article == null)
                {
                _responseHelper.AddUserResponse(ResponseType.Error, "Could not find the specified article to restore");
                return RedirectToAction("Index");
                }
            article.Status = PublicationStatus.UnPublished;
            _articleService.Save(article);
            _responseHelper.AddUserResponse(ResponseType.Notice, string.Format("Successfully restored article: {0}", article.Title));
            return RedirectToAction("Index");
            }

        [CMSAuthorize("UseCMS")]
        public ActionResult RevertToVersion(string versionId)
            {
            if (string.IsNullOrEmpty(versionId)) throw new ArgumentException("Version Id is required to perform this action");
            var version = _articleService.GetVersion(versionId, null);
            if (version == null)
                {
                _responseHelper.AddUserResponse(ResponseType.Notice, "Couldn't find that version", "If this persists, contact the iaafsupport@haymarket.com");
                return RedirectToAction("Edit");
                }
            var articleVersion = _articleService.GetEntityVersion(versionId);
            try
                {
                using (var editHelper = new EntityEditCache<Article>(x => x.Id))
                    {
                    _articleService.RevertToVersion(versionId);
                    _responseHelper.AddUserResponse(ResponseType.Notice, "Successfully reverted article", string.Format("To the version backed-up on {0} at {1}",
                        version.ModifiedOn.ToString("dd MMMM"),
                        version.ModifiedOn.ToString("H:mm")));
                    editHelper.ClearCache(articleVersion.Id);
                    }
                }
            catch (Exception ex)
                {
                _responseHelper.AddUserResponse(ResponseType.Notice, "Error reverting article", string.Format("To the version of the {0}. Error message: {1}",
                    version.ModifiedOn.ToString("dd MMMM"),
                    ex.Message));
                }

            return RedirectToAction("Edit", new { id = articleVersion.Id });
            }

        #region Related Documents

        //[CMSAuthorize("UseCMS")]
        //public PartialViewResult ListRelatedDocuments(string articleId)
        //    {
        //    var editHelper = new EntityEditCache<Article>(x => x.Id);
        //    Article article = editHelper.GetEntity(articleId);
        //    return PartialView("EditDocuments", article.RelatedDocuments);
        //    }

        //[CMSAuthorize("UseCMS & EditArticles")]
        //public PartialViewResult AddRelatedDocument(string articleId, string documentId)
        //    {
        //    if (string.IsNullOrEmpty(documentId)) throw new ArgumentException("A Document Id is required", "documentId");

        //    using (var editHelper = new EntityEditCache<Article>(x => x.Id))
        //        {
        //        Article article = editHelper.GetEntity(articleId);
        //        if (article.RelatedDocuments == null) article.RelatedDocuments = new List<Document>();
        //        if (!article.RelatedDocuments.Any(x => x.Id == documentId))
        //            {
        //            Document document = _standaloneDocumentService.FindByIds(documentId).SingleOrDefault();
        //            if (document != null)
        //                {
        //                article.RelatedDocuments.Add(document);
        //                return PartialView("EditDocumentItem", document);
        //                }
        //            else
        //                {
        //                throw new Exception(string.Format("Could not find a Document with the id {0}", documentId));
        //                }
        //            }
        //        else throw new Exception(string.Format("A document with the id {0} is already related to this article", documentId));
        //        }
        //    }

        //[CMSAuthorize("UseCMS & EditArticles")]
        //public void RemoveRelatedDocument(string articleId, string documentId)
        //    {
        //    if (string.IsNullOrEmpty(documentId)) throw new ArgumentException("A Document Id is required", "documentId");

        //    using (var editHelper = new EntityEditCache<Article>(x => x.Id))
        //        {
        //        Article article = editHelper.GetEntity(articleId);
        //        if (article.RelatedDocuments.Any(x => x.Id == documentId))
        //            {
        //            Document document = article.RelatedDocuments.SingleOrDefault(x => x.Id == documentId);
        //            article.RelatedDocuments.Remove(document);
        //            }
        //        else throw new Exception(string.Format("No document with id {0} is related to this article", documentId));
        //        }
        //    }

        #endregion Related Documents

        #region Tags

        // RelatedArticles
        [CMSAuthorize("UseCMS")]
        public PartialViewResult ListRelatedArticles(string articleId)
            {
            var editHelper = new EntityEditCache<Article>(x => x.Id);
            var article = editHelper.GetEntity(articleId);
            return PartialView("EditArticles", article.RelatedArticles);
            }

        [CMSAuthorize("UseCMS")]
        public PartialViewResult AddRelatedArticle(string articleId, string relatedArticleId)
            {
            if (string.IsNullOrEmpty(relatedArticleId)) throw new ArgumentException("A [related] Article Id is required", "relatedArticleId");
            using (var editHelper = new EntityEditCache<Article>(x => x.Id))
                {
                var article = editHelper.GetEntity(articleId);
                // Only add the related article if it isnt already tagged
                if (article.RelatedArticles.Any(x => x.Id == relatedArticleId)) return null;
                var relatedArticle = _articleService.FindByIds(GlobalVariables.CurrentBrand, new ArticleHydrationSettings
                {
                    PrimaryMedia = new MediaHydrationSettings()
                }, relatedArticleId).SingleOrDefault();
                article.RelatedArticles.Add(relatedArticle);
                return PartialView("EditArticleItem", relatedArticle);
                }
            return null;
            }

        [CMSAuthorize("UseCMS")]
        public void RemoveRelatedArticle(string articleId, string relatedArticleId)
            {
            if (string.IsNullOrEmpty(relatedArticleId)) throw new ArgumentException("A [related] Article Id is required", "relatedArticleId");
            using (var editHelper = new EntityEditCache<Article>(x => x.Id))
                {
                var article = editHelper.GetEntity(articleId);
                var relatedArticleToRemove = article.RelatedArticles.SingleOrDefault(x => x.Id == relatedArticleId.ToLower());
                if (relatedArticleToRemove != null) article.RelatedArticles.Remove(relatedArticleToRemove);
                }
            }

        #endregion Tags

        #endregion Create & Edit

        #region Private Methods

        private ActionResult save(Article article, string cachedArticleId, string versionComment, bool UpdateVersion, string AuthorUserId, string[] tags)
            {
            using (var editHelper = new EntityEditCache<Article>(x => x.Id))
                {
                // Check for session expiry
                if (!string.IsNullOrEmpty(cachedArticleId) && !editHelper.EntityIsAvailable(cachedArticleId))
                    {
                    _responseHelper.AddUserResponse(ResponseType.Error, "Session has expired.", "Unfortunately we were unable to save your changes. Please select the article and try again.");
                    return RedirectToAction("Index");
                    }

                // Update the article cache with the latest changes
                mergeBasicChanges(article, editHelper, cachedArticleId);

                var model = new ArticleEditModel
                {
                    Article = editHelper.GetEntity(cachedArticleId),
                    availableTags = _tagsService.All().Where(i => i.Brand == GlobalVariables.CurrentBrand).ToList(),
                    Versioned = (bool)UpdateVersion
                };

                //enforce the brand
                model.Article.brand = GlobalVariables.CurrentBrand;

                if (model.Article.CreatedOn == DateTime.MinValue)
                    model.Article.CreatedOn = DateTime.UtcNow;

                if (model.Versioned || string.IsNullOrEmpty(model.Article.Id))
                    model.Article.VersionDate = DateTime.UtcNow;
                else if (model.Article.VersionDate == null)
                    {
                    model.Article.VersionDate = article.CreatedOn;
                    }

                model.Article.AuthorUserId = AuthorUserId;
                if (model.Article.Tags == null)
                    model.Article.Tags = new List<string>();

                model.Article.UpdatedOn = DateTime.Now;
                if (model.Article.UpdatedBy == null)
                    model.Article.UpdatedBy = MembershipHelper.GetActiveUser();
                if (model.CreatedBy == null)
                    model.Article.CreatedBy = MembershipHelper.GetActiveUser();
                if (!validate(model.Article))
                    {
                    model.Responses = _responseHelper.UserResponses;
                    return View("Edit", model);
                    }
                if (tags == null || tags.Length == 0)
                    {
                    _responseHelper.AddUserResponse(ResponseType.Error, "Error", "You must Select At Least One Tag");
                    model.Responses = _responseHelper.UserResponses;
                    return View("Edit", model);
                    }
                try
                    {
                    var _tags = _tagsService.FindByIds(tags);
                    _tags.ToList().ForEach(item => item.Description = null); //ones with descriptions seem to cause errors
                    model.Article.RelatedTags = _tags;
                    model.Article.Tags.Clear();
                    foreach (var _tag in _tags)
                        {
                        model.Article.Tags.Add(_tag.UrlSlug);
                        }

                    // Save the cached article item
                    //    model.Article.LiveFrom = ((DateTime)model.Article.LiveFrom).ToUniversalTime();
                    _articleService.Save(model.Article, versionComment);
                    _responseHelper.AddUserResponse(ResponseType.Notice, string.Format("Successfully updated the article: {0}", model.Article.Title));

                    //if saving new article
                    if (string.IsNullOrEmpty(cachedArticleId))
                        editHelper.SetEntity(model.Article);
                    else
                        {
                        model.VersionHistory = _articleService.GetVersionHistory(cachedArticleId, new VersionHydrationSettings
                        {
                            ModifiedBy = new UserHydrationSettings()
                        });
                        }

                    model.Responses = _responseHelper.UserResponses;
                    //var typeList = from c in model.articleTypes
                    //                 select new SelectListItem { Text = c.ArticleTypeName, Value = c.Id, Selected = c.Id == model.Article.articleType.Id };

                    //ViewBag.myTypeList = typeList;
                    //return RedirectToAction("Edit", new { id = model.Article.Id });
                    model.Versioned = false;
                    return View("Edit", model);
                    }
                catch (Exception ex)
                    {
                    _responseHelper.AddUserResponse(ResponseType.Error, "Error", ex.Message);
                    model.Responses = _responseHelper.UserResponses;
                    return View("Edit", model);
                    }
                }
            }

        private bool validate(Article article)
            {
            var isValid = true;
            if (ModelState.IsValid) return isValid;
            //_responseHelper.AddValidationResponses(ModelState);
            _responseHelper.AddValidationResponses(ModelState);
            isValid = false;

            // Any custom validation
            // TODO: Uncomment the following lines once the Article.LiveFrom... properties have been removed
            //if (article.Status == PublicationStatus.Published && article.LiveFrom == null)
            //{
            //    _responseHelper.AddValidationResponse("LiveFrom", "A published article requries a LiveFrom date.");
            //    isValid = false;
            //}

            return isValid;
            }

        private ActionResult cancel(string articleId)
            {
            (new EntityEditCache<Article>(x => x.Id)).ClearCache(articleId);
            return RedirectToAction("Index");
            }

        /// <summary>
        /// Persists an Article entity while skipping all properties that are managed
        /// individually (e.g. RelatedArticles) or automatically (e.g. AuditDetails)
        /// </summary>
        /// <param name="changedArticle"></param>
        private void mergeBasicChanges(Article changedArticle, EntityEditCache<Article> editHelper, string articleId)
            {
            var article = editHelper.GetEntity(articleId);
            article = Mapper.Map<Article, Article>(changedArticle, article);
            article.Status = changedArticle.Status;
            article.ArticleType = changedArticle.ArticleType;
            }

        /// <summary>
        /// TODO: This method should also get a list of featured articles to display alongside the main list
        /// </summary>
        /// <returns></returns>
        private ArticleListModel getArticleListModel(int page = -1)
            {
            var model = new ArticleListModel { FilterType = ArticleFilter };
            var showWithStatus = ShowDeletedArticles ? new[] { PublicationStatus.Deleted } : new[] { PublicationStatus.Published, PublicationStatus.UnPublished };

            model.Articles = _articleService.GetPagedOrderedBy(
              GlobalVariables.CurrentBrand, SortOrder.UpdatedOn,
              _mainlistPageHelper.PaginationModel.PageSize * (_mainlistPageHelper.PaginationModel.CurrentPage - 1),
              _mainlistPageHelper.PaginationModel.PageSize,
              new string[] { },
              showWithStatus,
              false, false, ArticleFilter,
              new ArticleHydrationSettings
              {
                  PrimaryMedia = new MediaHydrationSettings(),
                  RelatedJobs = new JobRoleHydrationSettings(),
                  RelatedTags = new TagHydrationSettings()
              }
                );

            // Put the view model together
            _mainlistPageHelper.SetTotalItemCount(_articleService.GetCount(GlobalVariables.CurrentBrand, new string[] { }, showWithStatus, false, ArticleFilter));

            model.Pagination = _mainlistPageHelper.PaginationModel;
            model.Pagination.BaseUrl = PaginationBaseUrl;

            model.Responses = _responseHelper.UserResponses;
            model.ShowDeletedArticles = ShowDeletedArticles;

            // If a search was previously made then reload the results
            if (string.IsNullOrWhiteSpace(SearchText)) return model;
            model.SearchText = SearchText;
            model.SearchResults = search(SearchText, 0, SearchCount);

            return model;
            }

        private IList<Article> search(string searchText, int skip, int take)
            {
            var showWithStatus = ShowDeletedArticles ? new[] { PublicationStatus.Deleted } : new[] { PublicationStatus.Published, PublicationStatus.UnPublished };

            var articles = _articleService.Search(GlobalVariables.CurrentBrand, searchText, skip, take, true, false, false, showWithStatus, false);

            SearchText = searchText;
            return !string.IsNullOrEmpty(SelectedBrand) ? articles.Where(i => i.brand == GlobalVariables.CurrentBrand).ToList() : articles;
            }

        #endregion Private Methods

        #region Private Properties

        private string SelectedBrand
            {
            get
                {
                if (Session[SelectedBrandCacheKey] == null)
                    return "";
                else
                    return Session[SelectedBrandCacheKey].ToString();
                }
            set
                {
                Session[SelectedBrandCacheKey] = value;
                }
            }

        private bool ShowDeletedArticles
            {
            get
                {
                if (Session[ShowDeletedArticlesCacheKey] == null) return false;
                else return (bool)Session[ShowDeletedArticlesCacheKey];
                }
            set
                {
                Session[ShowDeletedArticlesCacheKey] = value;
                }
            }

        private ArticleType ArticleFilter
            {
            get
                {
                if (Session[ArticleFilterTypeCacheKey] == null)
                    Session[ArticleFilterTypeCacheKey] = ArticleType.article;

                return (ArticleType)Session[ArticleFilterTypeCacheKey];
                }
            set
                {
                Session[ArticleFilterTypeCacheKey] = value;
                // Reset the pagination model everytime the MediaFilter is changed so that we start at the beginning again.
                _mainlistPageHelper.Reset();
                }
            }

        private string SearchText
            {
            get
                {
                if (Session[ArticleSearchTextCacheKey] == null) return string.Empty;
                return (string)Session[ArticleSearchTextCacheKey];
                }
            set
                {
                Session[ArticleSearchTextCacheKey] = value;
                }
            }

        private int SearchCount
            {
            get
                {
                if (Session[ArticleSearchCountCacheKey] == null)
                    Session[ArticleSearchCountCacheKey] = MaxMediaResultsPerTabView;
                return (int)Session[ArticleSearchCountCacheKey];
                }
            set
                {
                Session[ArticleSearchCountCacheKey] = value;
                }
            }

        #endregion Private Properties
        }
    }