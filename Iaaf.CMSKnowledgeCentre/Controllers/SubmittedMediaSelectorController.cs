﻿using System.Linq;
using System.Web.Mvc;
using CMS.Helpers;
using CMS.Helpers.Authorise;
using CMS.Models;
using ITOK.Core.Common;
using ITOK.Core.Common.Constants;
using ITOK.Core.Common.Utils;
using ITOK.Core.Data;
using ITOK.Core.Services.CMS;
using ITOK.Core.Services.Security;

namespace CMS.Controllers
    {
    public class SubmittedMediaSelectorController : BaseController
        {
        private const int SearchResultsAtATime = 50;

        private SubmittedMediaService _mediaService;
        private IMongoRepository1 mongoRepository;

        public SubmittedMediaSelectorController()
            {
            mongoRepository = AppKernel.GetInstance<IMongoRepository1>();
            _mediaService = new SubmittedMediaService(mongoRepository);
            }

        public SubmittedMediaSelectorController(IMongoRepository1 _mongoRepository)
            {
            this.mongoRepository = _mongoRepository;
            _mediaService = new SubmittedMediaService(mongoRepository);
            }

        ///// <summary>
        /////
        ///// </summary>
        ///// <param name="addMediaActionName">The action to call when the user adds a media item</param>
        ///// <param name="addMediaControllerName">The controller to call when the user adds a media item</param>
        ///// <param name="updateElementSelector">The element to update when the media item has been added.
        ///// The return value of the action specified above should be a partial view that can be inserted into this element.</param>
        ///// <returns></returns>
        //[HttpGet]
        //public JsonResult Load(string addMediaActionName, string addMediaControllerName, string updateElementSelector)
        //{
        //}
        [CMSAuthorize("UseCMS")]
        public PartialViewResult Load()
            {
            return PartialView("MediaSelector");
            }

        /// <summary>
        /// RenderAction behaves incoherently when the page is posted back to.
        /// When the page is first loaded the HttpGet Action is requested.
        /// If the page posts back to itself, RenderAction then requests the
        /// HttpPost action method...
        /// </summary>
        /// <param name="nowt">Will always be null. Included to provide a
        /// signature that's distinct from the HttpGet version of this method.</param>
        /// <returns></returns>
        [CMSAuthorize("UseCMS")]
        [HttpPost]
        public PartialViewResult Load(string nowt)
            {
            return PartialView("MediaSelector");
            }

        [CMSAuthorize("UseCMS")]
        public JsonResult RecentMedia()
            {
            var model = new SubmittedMediaResults { Type = ResultSetType.MediaRecent };

            if (!LiveUser.CanDoAny(Permission.EditMedia))
            {
                //if they cannot approve content, they can only see their own
                model.Results = _mediaService.GetPagedOrderedByCreatedOnForUser(GlobalVariables.CurrentBrand, 0, SearchResultsAtATime, LiveUser.Get().Id, null,
              new[] { SubmissionStatus.InProgress, SubmissionStatus.Submitted }, false, false, false, new ITOK.Core.Data.Model.HS.SubmittedMediaHydrationSettings())
              .Select(x => new SubmittedMediaResultItem { Media = x })
              .ToList();

            }
            else
            {
                model.Results = _mediaService.GetPagedOrderedByCreatedOn(GlobalVariables.CurrentBrand, 0, SearchResultsAtATime, null,
              new[] { SubmissionStatus.InProgress, SubmissionStatus.Submitted }, false, false, false, new ITOK.Core.Data.Model.HS.SubmittedMediaHydrationSettings())
              .Select(x => new SubmittedMediaResultItem { Media = x })
              .ToList();
            }

          
            //return PartialView("MediaSelector", model);
            return Json(model, JsonRequestBehavior.AllowGet);
            }

     
        [CMSAuthorize("UseCMS")]
        public JsonResult SearchByTitle(string searchText, int skip)
            {
            var model = new SubmittedMediaResults
            {
                //restrict the search if they cannot view all
                Results =
                    _mediaService.Search(GlobalVariables.CurrentBrand, searchText, skip, SearchResultsAtATime, true, false,
                        new[] { SubmissionStatus.InProgress, SubmissionStatus.Submitted }, true, null, !LiveUser.CanDoAny(Permission.EditMedia) ? LiveUser.Get().Id : null)
                        .Select(x => new SubmittedMediaResultItem { Media = x, ThumbnailUrl = x.Hosting == MediaHosting.RemoteBrightCove ? MediaUtils.GetBrightCoveThumbnailUri(x.RemoteItemCode) : "" })
                        .ToList()
            };

            //return PartialView("AvailableMedia", model);
            return Json(model, JsonRequestBehavior.AllowGet);
            }

        }
    }