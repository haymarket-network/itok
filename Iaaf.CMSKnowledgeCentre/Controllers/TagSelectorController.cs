﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CMS.Helpers;
using CMS.Helpers.Authorise;
using ITOK.Core.Common;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using ITOK.Core.Data.Model.HS;
using ITOK.Core.Services.CMS;
using ITOK.Core.Services.Security;
using ITagsService = ITOK.Core.Services.Core.ITagsService;

namespace CMS.Controllers
    {
    public class TagSelectorController : Controller
        {
        private const int MaxArticleSearchResults = 100;

        private const int MaxEventSearchResults = 100;

        private IMongoRepository1 mongoRepository;

        public TagSelectorController()
            {
            mongoRepository = AppKernel.GetInstance<IMongoRepository1>();
            }

        public TagSelectorController(IMongoRepository1 _mongoRepository)
            {
            this.mongoRepository = _mongoRepository;
            }

        [CMSAuthorize("UseCMS")]
        public PartialViewResult Load()
            {
            return PartialView("TagSelector");
            }

        [CMSAuthorize("UseCMS")]
        public PartialViewResult SearchArticles(string searchText)
            {
            var articleService = new ArticleService(mongoRepository);
            IList<Article> articles = articleService.Search(GlobalVariables.CurrentBrand,
                searchText,
                true,
                true,
                true,
                MaxArticleSearchResults,
                new[] { PublicationStatus.Published, PublicationStatus.UnPublished },
                false,
                null,
                new ArticleHydrationSettings
                {
                    PrimaryMedia = new MediaHydrationSettings()
                });
            return PartialView("ArticleResultList", articles.OrderByDescending(i => i.LiveFrom).ToList());
            }

        [CMSAuthorize("UseCMS")]
        public PartialViewResult SearchTags(string searchText)
            {
            var tagsService = AppKernel.GetInstance<ITagsService>();
            var tags = tagsService.Search(searchText, true, MaxEventSearchResults);
            return PartialView("TagList", tags);
            }
        }
    }