﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CMS.Helpers;
using CMS.Helpers.Authorise;
using ITOK.Core.Common;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using ITOK.Core.Common.Constants;
using ITOK.Core.Services.Security;
using System;
using ITagsService = ITOK.Core.Services.Core.ITagsService;
using ITOK.Core.Common.Constants;
using ITOK.Core.Services.Core;
using ArticleService = ITOK.Core.Services.CMS.ArticleService;

namespace CMS.Controllers
{
    public class TagsController : Controller
    {
        private readonly ITagsService _tagsService;
        private readonly IQuestionService _questionService;
        private readonly ArticleService _articleService;

        private IMongoRepository1 mongoRepository;

        public TagsController()
        {
            mongoRepository = AppKernel.GetInstance<IMongoRepository1>();
            _tagsService = AppKernel.GetInstance<ITagsService>();
            _questionService = _questionService = AppKernel.GetInstance<IQuestionService>();
            _articleService = new ArticleService(mongoRepository);
        }

        public TagsController(IMongoRepository1 _mongoRepository)
        {
            this.mongoRepository = _mongoRepository;
            _tagsService = AppKernel.GetInstance<ITagsService>();
            _questionService = _questionService = AppKernel.GetInstance<IQuestionService>();
            _articleService = new ArticleService(mongoRepository);
        }

        // GET: Tags
        [CMSAuthorize("UseCMS & EditTags")]
        public ActionResult Index()
        {
            var tags = _tagsService.All().Where(i => i.Brand == GlobalVariables.CurrentBrand).ToList();
            return View(tags);
        }

        [CMSAuthorize("UseCMS & EditTags")]
        public void set(string beforeid, string childid)
        {
            var tag = _tagsService.FindByIds(new string[] { childid, beforeid });
            var childTag = tag.FirstOrDefault(i => i.Id == childid);
            var beforeTag = tag.FirstOrDefault(i => i.Id == beforeid);
            if (childTag == null) return;
            if (beforeTag != null)
            {
                childTag.ParentId = beforeTag.ParentId;
                childTag.ParentSlug = beforeTag.ParentSlug;
                _tagsService.Save(childTag);
            }
            else
            {
                childTag.ParentId = "";
                childTag.ParentSlug = "";
                _tagsService.Save(childTag);
            }
        }

        [CMSAuthorize("UseCMS & EditTags")]
        public ActionResult List()
        {
            return View(_tagsService.All().Where(i => i.Brand == GlobalVariables.CurrentBrand).OrderBy(x => x.DisplayText));
        }

        [CMSAuthorize("UseCMS & EditTags")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Tags tag, string Id, string JobRoleName)
        {
            try
            {
                var deDupe = _tagsService.FindByUrlSlug(tag.UrlSlug, GlobalVariables.CurrentBrand);
                var deDupe2 = _tagsService.FindByDisplayText(tag.DisplayText, GlobalVariables.CurrentBrand);
                if (deDupe != null && (!string.IsNullOrEmpty(deDupe.Id) && deDupe.Id != tag.Id))
                {
                    ViewBag.errMsg = "That urlSlug already exists, please try again. Your changes were NOT saved";
                }
                if (deDupe2 != null && (!string.IsNullOrEmpty(deDupe2.Id) && deDupe2.Id != tag.Id))
                {
                    ViewBag.errMsg = "That name already exists, please try again. Your changes were NOT saved";
                }
                if (!string.IsNullOrEmpty(ViewBag.errMsg))
                {
                    var selectListItems = from c in _tagsService.All().Where(i => i.Brand == GlobalVariables.CurrentBrand).OrderBy(i => i.DisplayText)
                                          select new SelectListItem { Text = c.DisplayText, Value = c.Id };

                    ViewBag.TagsList = selectListItems.ToList();

                    var tagTypeList = new List<SelectListItem>
                        {
                            new SelectListItem() {Text = "Areas", Value = "0"},
                            new SelectListItem() {Text = "Year", Value = "1"},
                            new SelectListItem() {Text = "Event", Value = "2"},
                             new SelectListItem() {Text = "Category", Value = "3"}
                        };
                    ViewBag.TypeList = tagTypeList.ToList();

                    return View(tag);
                }

                if (!string.IsNullOrEmpty(tag.ParentId))
                {
                    var parent = _tagsService.FindById(tag.ParentId);
                    tag.ParentSlug = parent.UrlSlug;
                }

                tag.Brand = GlobalVariables.CurrentBrand;
                _tagsService.Save(tag);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [CMSAuthorize("UseCMS & EditTags")]
        public ActionResult Create()
        {
            var tag = new Tags { TagType = TagType.Events };
            if (Request.QueryString["ParentId"] != null)
            {
                tag.ParentId = Request.QueryString["ParentId"];
                tag.ParentSlug = Request.QueryString["ParentSlug"];
            }
            var selectListItems = from c in _tagsService.All().Where(i => i.Brand == GlobalVariables.CurrentBrand).OrderBy(i => i.DisplayText)
                                  select new SelectListItem { Text = c.DisplayText, Value = c.Id };

            ViewBag.TagsList = selectListItems.ToList();

            var tagTypeList = new List<SelectListItem>
            {
                  new SelectListItem() {Text = "Areas", Value = "0"},
                  new SelectListItem() {Text = "Year", Value = "1"},
                  new SelectListItem() {Text = "Event", Value = "2"},
                  new SelectListItem() {Text = "Category", Value = "3"}
            };
            ViewBag.TypeList = tagTypeList.ToList();

            tag.Brand = GlobalVariables.CurrentBrand;

            return View(tag);
        }

        [CMSAuthorize("UseCMS & EditTags")]
        public ActionResult Edit(string id)
        {

            Models.TagEditModel tagEditModel = new Models.TagEditModel();
            var tag = _tagsService.FindById(id);
            var selectListItems = new List<SelectListItem>();
            if (tag.TagType == 0)
            {
                selectListItems = (from c in _tagsService.All().Where(i => i.Brand == GlobalVariables.CurrentBrand).Where(i => i.TagType == TagType.Category).OrderBy(i => i.DisplayText)
                                   select new SelectListItem { Text = c.DisplayText, Value = c.Id }).ToList();
            }
            else if (tag.TagType == TagType.Year)
            {
                selectListItems = (from c in _tagsService.All().Where(i => i.Brand == GlobalVariables.CurrentBrand).Where(i => i.TagType == TagType.Events).OrderBy(i => i.DisplayText)
                                   select new SelectListItem { Text = c.DisplayText, Value = c.Id }).ToList();
            }
            ViewBag.TagsList = selectListItems;

            var tagTypeList = new List<SelectListItem>
            {
                  new SelectListItem() {Text = "Areas", Value = "0"},
                  new SelectListItem() {Text = "Year", Value = "1"},
                  new SelectListItem() {Text = "Event", Value = "2"},
                  new SelectListItem() {Text = "Category", Value = "3"}
            };
            ViewBag.TypeList = tagTypeList.ToList();
            tag.Brand = GlobalVariables.CurrentBrand;

            tagEditModel.parentTags = selectListItems;
            tagEditModel.Tag = tag;
            tagEditModel.questions = _questionService.All().Where(i => i.ParentTagId == tag.Id).ToList();
            return View("Edit", tagEditModel);
        }

        [ValidateInput(false)]
        [CMSAuthorize("UseCMS & EditTags")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(string id, Tags tag)
        {
            var deDupe = _tagsService.FindByUrlSlug(tag.UrlSlug, GlobalVariables.CurrentBrand);
            var deDupe2 = _tagsService.FindByDisplayText(tag.DisplayText, GlobalVariables.CurrentBrand);

            if (deDupe != null && (!string.IsNullOrEmpty(deDupe.Id) && deDupe.Id != tag.Id))
            {
                ViewBag.errMsg = "That urlSlug already exists, please try again. Your changes were NOT saved";
            }
            if (deDupe2 != null && (!string.IsNullOrEmpty(deDupe2.Id) && deDupe2.Id != tag.Id))
            {
                ViewBag.errMsg = "That name already exists, please try again. Your changes were NOT saved";
            }

            var selectListItems = from c in _tagsService.All().Where(i => i.Brand == GlobalVariables.CurrentBrand).OrderBy(i => i.DisplayText)
                                  select new SelectListItem { Text = (c.DisplayText + (c.TagType == 0 ? " (Primary Role)" : "") + (c.TagType == TagType.Year ? " (Secondary Role)" : "")), Value = c.Id };
            ViewBag.TagsList = selectListItems.ToList();

            var tagTypeList = new List<SelectListItem>
            {
                  new SelectListItem() {Text = "Areas", Value = "0"},
                  new SelectListItem() {Text = "Year", Value = "1"},
                  new SelectListItem() {Text = "Event", Value = "2"}
            };
            ViewBag.TypeList = tagTypeList.ToList();

            if (!string.IsNullOrEmpty(ViewBag.errMsg))
            {
                return View(tag);
            }

            if (!string.IsNullOrEmpty(tag.ParentId))
            {
                var parent = _tagsService.FindById(tag.ParentId);
                tag.ParentSlug = parent.UrlSlug;
            }
            var tagWithTopics = _tagsService.FindById(id);

            tag.Brand = GlobalVariables.CurrentBrand;
            tag.TopicHeaders = tagWithTopics.TopicHeaders;
            _tagsService.Save(tag);

            ViewBag.Msg = "Tag Saved";



            return RedirectToAction("Edit", new { id = tag.Id });
        }





        [CMSAuthorize("UseCMS & EditTags")]
        // [HttpPost]
        public ActionResult Delete(string id)
        {
            //var deleteTag = _tagsService.FindByIds(id).FirstOrDefault();
            // var articles = _articleService.FindRelatedTagsForDelete(GlobalVariables.CurrentBrand, deleteTag.UrlSlug, null, false, false);
            //foreach (var article in articles)
            //    {
            //    article.RelatedTags.Remove(article.RelatedTags.FirstOrDefault(i => i.UrlSlug == deleteTag.UrlSlug));
            //    _articleService.Save(article, "Tag Auto removed");
            //    }

            _tagsService.Delete(_tagsService.FindByIds(id).FirstOrDefault().Id);

            return RedirectToAction("Index");
        }

        [CMSAuthorize("UseCMS & EditTags")]
        // [HttpPost]
        public ActionResult AddTopicHeader(string tagId, string topicHeader)
        {
            var tag = _tagsService.FindByIds(tagId).FirstOrDefault();
            Guid newG = Guid.NewGuid();
            tag.TopicHeaders.Add(newG.ToString(), topicHeader);

            _tagsService.Save(tag);
            tag = _tagsService.FindByIds(tagId).FirstOrDefault();
            return View("TopicHeaderList", tag);
        }
        public ActionResult RemoveTopicHeader(string tagId, string guid)
        {
            var tag = _tagsService.FindByIds(tagId).FirstOrDefault();
            tag.TopicHeaders.Remove(guid);

            _tagsService.Save(tag);

            return View("TopicHeaderList", tag);
        }


    }
}