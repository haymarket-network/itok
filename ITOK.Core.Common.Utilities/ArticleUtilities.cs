﻿using System;
using System.Collections.Generic;
using System.Text;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data.Model;

namespace ITOK.Core.Common.Utilities
    {
    public static partial class ArticleUtilities
        {
        private readonly static IDictionary<string, ArticleType> _stringToArticleTypeMap;
        private readonly static IDictionary<ArticleType, string> _articleTypeToUrlStringMap;
        private readonly static IDictionary<ArticleType, string> _articleTypeToPrettyStringMap;

        static ArticleUtilities()
            {
            _stringToArticleTypeMap = new Dictionary<string, ArticleType>
            {
                { "article", ArticleType.article },
                { "video", ArticleType.video },
                { "document", ArticleType.document },
                { "page", ArticleType.page },
                { "group", ArticleType.group },
                { "news", ArticleType.news },
                { "bulletin", ArticleType.bulletin }
            };

            _articleTypeToUrlStringMap = new Dictionary<ArticleType, string>
            {
                { ArticleType.article, "article" },
                { ArticleType.video, "video" },
                { ArticleType.document, "document" },
                { ArticleType.page, "page" },
                { ArticleType.group, "group" },
                { ArticleType.news, "news" },
                { ArticleType.bulletin, "bulletin" }
            };

            _articleTypeToPrettyStringMap = new Dictionary<ArticleType, string>
            {
                { ArticleType.article, "article" },
                { ArticleType.video, "video" },
                { ArticleType.document, "document" },
                { ArticleType.page, "page" },
                { ArticleType.group, "group" },
                { ArticleType.news, "news" },
                { ArticleType.bulletin, "bulletin" }
            };
            }

        public static string PrettifyArticleType(ArticleType articleType)
            {
            if (!_articleTypeToPrettyStringMap.ContainsKey(articleType)) throw new ArgumentException("No pretty type name defined for ArticleType: " + articleType.ToString(), "articleType");
            return _articleTypeToPrettyStringMap[articleType];
            }

        public static string UrlStringFromArticleType(ArticleType articleType)
            {
            return _articleTypeToUrlStringMap[articleType];
            }

        public static ArticleType ArticleTypeFromString(string articleTypeString)
            {
            if (!_stringToArticleTypeMap.ContainsKey(articleTypeString.ToLower())) throw new ArgumentException("Could not find an ArticleType associated with the string: " + articleTypeString.ToLower(), "articleTypeString");
            return _stringToArticleTypeMap[articleTypeString.ToLower()];
            }

        public static bool IsValidArticleTypeString(string articleTypeString)
            {
            return _stringToArticleTypeMap.ContainsKey(articleTypeString.ToLower());
            }

        public static string BuildMetaData(Article article, bool competitionArticle = false)
            {
            var sb = new StringBuilder();

            switch (article.ArticleType)
                {
                //case ArticleType.PressRelease: return View("PressReleaseDetail", model);
                case ArticleType.article:
                    if (article.Authors != null)
                        ///TODO:whatever else
                        sb.Append(String.Format("<meta name=\"Author\" content=\"{0}\"  />\r\n", article.Authors[0].DisplayName));
                    break;
                }

            return sb.ToString();
            }

        public static string GetArticleUri(Article article)
            {
            return article != null ? string.Format("/{1}/{2}", article.brand, UrlStringFromArticleType(article.ArticleType), article.UrlSlug) : "";
            }
        }
    }