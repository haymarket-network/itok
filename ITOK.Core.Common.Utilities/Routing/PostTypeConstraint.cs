﻿using System.Web;
using System.Web.Routing;

namespace ITOK.Core.Common.Utilities.Routing
    {
    public class PostTypeConstraint : IRouteConstraint
        {
        public bool Match(HttpContextBase httpContext,
             Route route,
             string parameterName,
             RouteValueDictionary values,
             RouteDirection routeDirection)
            {
            // Get the value called "parameterName" from the
            // RouteValueDictionary called "value"
            var value = values[parameterName].ToString();

            return ArticleUtilities.IsValidArticleTypeString(value);
            }
        }
    }