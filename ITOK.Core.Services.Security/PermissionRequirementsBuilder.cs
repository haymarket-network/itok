﻿using System.Collections.Generic;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data.Model;

namespace ITOK.Core.Services.Security
{
    public class PermissionRequirementsBuilder
    {
        private IList<PermissionRequirementsSegment> _segments = new List<PermissionRequirementsSegment>();
        private User _activeUser;

        public PermissionRequirementsBuilder()
        {
        }

        public PermissionRequirementsBuilder(User activeUser)
        {
            _activeUser = activeUser;
        }

        public static implicit operator bool(PermissionRequirementsBuilder builder)
        {
            return builder.check();
        }

        public PermissionRequirementsBuilder Or(params Permission[] permissions)
        {
            if (permissions != null) _segments.Add(new PermissionRequirementsSegment(SegmentType.Or, permissions));
            return this;
        }

        public PermissionRequirementsBuilder OrAll(params Permission[] permissions)
        {
            if (permissions != null) _segments.Add(new PermissionRequirementsSegment(SegmentType.OrAll, permissions));
            return this;
        }

        public PermissionRequirementsBuilder And(params Permission[] permissions)
        {
            if (permissions != null) _segments.Add(new PermissionRequirementsSegment(SegmentType.And, permissions));
            return this;
        }

        public PermissionRequirementsBuilder AndAny(params Permission[] permissions)
        {
            if (permissions != null) _segments.Add(new PermissionRequirementsSegment(SegmentType.AndAny, permissions));
            return this;
        }

        /// <summary>
        /// For use when the active user is only known when the check is actually performed (i.e. not during build process)
        /// </summary>
        /// <param name="activeUser"></param>
        /// <returns></returns>
        public bool Check(User activeUser)
        {
            _activeUser = activeUser;
            return this.check();
        }

        private bool check()
        {
            if (_segments.Count == 0) return true;
            var result = _segments[0].Check(_activeUser.Permissions); // get starting value
            SegmentType activeSegmentType;
            for (var i = 1; i < _segments.Count; i++)
            {
                var segment = _segments[i];
                activeSegmentType = segment.Type;
                if (activeSegmentType == SegmentType.Or || activeSegmentType == SegmentType.OrAll)
                    result = result || segment.Check(_activeUser.Permissions);
                else
                {
                    result = result && segment.Check(_activeUser.Permissions);
                    if (!result) break;
                }

                //result = activeSegmentType == SegmentType.Or || activeSegmentType == SegmentType.OrAll ?
                //    result || segment.Check(_activeUser.Permissions) :
                //    result && segment.Check(_activeUser.Permissions);
            }
            return result;
        }
    }
}
