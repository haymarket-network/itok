﻿using ITOK.Core.Common.Constants;
using ITOK.Core.Common.Interfaces;
using ITOK.Core.Data.Model;

namespace ITOK.Core.Services.Security
{
    public static class LiveUser
    {
        private static User _activeUser;
        private static IMembershipProvider _membershipProvider;

        public static void SetMembershipProvider(IMembershipProvider membershipProvider)
        {
            _membershipProvider = membershipProvider;
        }

        public static void SetActiveUser(User activeUser)
        {
            _activeUser = activeUser;
        }

        public static User Get()
        {
            return _membershipProvider.ActiveUser;
        }

        public static PermissionRequirementsBuilder Can(params Permission[] permissions)
        {
            User activeUser = _activeUser;
            if (activeUser == null) activeUser = _membershipProvider.ActiveUser;
            var builder = new PermissionRequirementsBuilder(activeUser);
            builder.And(permissions);
            return builder;
        }

        public static PermissionRequirementsBuilder CanDoAny(params Permission[] permissions)
        {
            User activeUser = _activeUser;
            if (activeUser == null) activeUser = _membershipProvider.ActiveUser;
            var builder = new PermissionRequirementsBuilder(activeUser);
            builder.Or(permissions);
            return builder;
        }
    }


}