﻿using System;
using System.Collections.Generic;
using ITOK.Core.Common.Constants;

namespace ITOK.Core.Services.Security
{
    public static class PermissionUtilities
    {
        private static IDictionary<string, Permission> _permissionStringMap;
        private static object _lockObj = new object();

        static PermissionUtilities()
        {
            if (_permissionStringMap == null)
            {
                lock (_lockObj)
                {
                    _permissionStringMap = new Dictionary<string, Permission>();
                    foreach (Permission permission in Enum.GetValues(typeof(Permission)))
                    {
                        _permissionStringMap.Add(permission.ToString(), permission);
                    }
                }
            }
        }

        public static Permission PermissionFromString(string permissionString)
        {
#if DEBUG
            if (!_permissionStringMap.ContainsKey(permissionString)) throw new ArgumentException(string.Format("'{0}' is not a valid Permission string. Ensure it's PascalCase and if you've added a new permission, restart the application", permissionString), "permissionString");
#endif
            return _permissionStringMap[permissionString];
        }

        public static Permission[] PermissionsFromString(string[] permissionStrings)
        {
            var permissions = new Permission[permissionStrings.Length];
            for (var i = 0; i < permissionStrings.Length; i++)
            {
                permissions[i] = PermissionFromString(permissionStrings[i]);
            }
            return permissions;
        }
    }
}
