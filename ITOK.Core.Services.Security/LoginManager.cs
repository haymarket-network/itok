﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITOK.Core.Common;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data.Model;
using ITOK.Core.Services.Core;

namespace ITOK.Core.Services.Security
{
  
  

    public class SecondaryRole
    {
        public string SecondaryRoleId;
        public string SecondaryRoleName;
    }
  

    public class Favourite
    {
        public int ArticleRef;
        public int order;
    }

    public static class LoginManager
    {
        private const string UserDataSessionKey = "User";

        public static ITOK.Core.Data.Model.User CurrentUser
        {
            get
            {
                if (HttpContext.Current.Session[UserDataSessionKey] != null &&
                    HttpContext.Current.Session[UserDataSessionKey] is User)
                    return (ITOK.Core.Data.Model.User)HttpContext.Current.Session[UserDataSessionKey];
                else
                    return new ITOK.Core.Data.Model.User();
            }
            set
            {
                HttpContext.Current.Session[UserDataSessionKey] = value;
            }
        }

        public static bool IsLoggedIn
        {
            get
            {
                return !String.IsNullOrEmpty(CurrentUser.Id);
            }
        }
       
     
        public static void LogoutUser()
        {
            CurrentUser = new ITOK.Core.Data.Model.User();
        }

        public static string GetProfileLink()
        {
            return String.Format("<a href=\"{0}\" class=\"prev\">{1}</a>", VirtualPathUtility.ToAbsolute("~/preferences"), CurrentUser.Forename);
        }

        //public static User GetUserFromRequest(HttpRequest Request)
        //{
        //var brand = (Brand)System.Enum.ToObject(typeof(Brand), ConfigHelper.CurrentBrand);

        //    return GetUserFromRequest(Request, brand);
        //}

        //public static User GetUserFromRequest(HttpRequest Request, Brand brand)
        //{

        //    if (LoginManager.CurrentUser.UniqueIdentification != null && Request.Headers["SSO_memberOf"] != null)
        //    {
        //        // check for a current one first, if so leave it
        //    }
        //    else if (Request.Headers["SSO_memberOf"] != null) //headers are passed on every request, rebuild from headers
        //    {
        //        try
        //        {
                    
        //            var user = new User
        //            {
        //                permissions = new List<BrandInfo>(),
        //                UniqueIdentification = Request.Headers["SSO_uid"].Replace(";", ""),
        //                UserName =
        //                    string.Format("{0} {1}",
        //                        Request.Headers["SSO_givenname"].Replace(";", "") ?? string.Empty,
        //                        Request.Headers["SSO_sn"].Replace(";", "") ?? string.Empty),
        //                emailAddress = Request.Headers["SSO_mail"].Replace(";", "") ?? string.Empty,
        //                Title = Request.Headers["SSO_title"] ?? string.Empty,
        //                TelephoneNumber = Request.Headers["SSO_telephoneNumber"] ?? string.Empty,
        //                FirstName = Request.Headers["SSO_givenname"] ?? string.Empty,
        //                Surname = Request.Headers["SSO_sn"] ?? string.Empty,
        //                RetailerID = Request.Headers["SSO_parentOu"] ?? string.Empty,
        //                brands = new List<Brand>()
        //            };
        //            var brands = new Brand[]
        //            {Brand.itok};

        //            var HeadOfficeIDs = new string[]
        //            {"vwuk", "vwpcuk", "vwcvuk", "vwfsuk", "tpsuk", "skodauk", "seatuk", "audiuk"};
        //            //just got to check for matches

        //            //check brand first. Should have been re-directed to correct desktop to start with
        //            //string[] brandNames = Request.Headers.GetValues("SSO_memberOf");

        //            var brandNames = Request.Headers["SSO_memberOf"].Split('|', ',')
        //                .Where(a => a.Split('=')[0] == "cn")
        //                .Select(b => b.Split('=')[1]).ToArray();
        //            var CurrentJobRole = "";
        //            var brandMatch = false;
        //            var isBusinessManagement = false;
        //            var isEditor = false;

        //            isBusinessManagement = (HeadOfficeIDs.Any(user.RetailerID.Contains));
        //            bool isGlobalSet = false;
        //            foreach (var brandstring in brandNames)
        //            {
        //                var brandInfo = brandstring.Replace("brandDesktop", "");
        //                brandInfo = brandInfo.Replace("CMS", "");

        //                var bi = new BrandInfo
        //                {
        //                    IsEditor = brandInfo.Contains("Editor"),
        //                    IsAgency = brandInfo.Contains("Agency")
        //                };
        //                if (brandInfo.ToLower().Contains("businessmanagement"))
        //                    isBusinessManagement = true;
        //                if (brandInfo.ToLower().Contains("editor"))
        //                    isEditor = true;

        //                brandInfo = brandInfo.Replace("Editor", "");
        //                brandInfo = brandInfo.Replace("User", "");
        //                brandInfo = brandInfo.Replace("Agency", "");

                        
        //                foreach (
        //                    var brandName in
        //                        brands.Where(_brandName => brandInfo.ToLower().Contains(_brandName.ToString())))
        //                {
        //                    if (brandName == Brand.tps) continue;
        //                    if (user.brands == null)
        //                        user.brands = new List<Brand>();
        //                    if (brandName == Brand.vw && brandInfo.ToLower().Contains("vwcv")) continue;
        //                    if (!user.brands.Contains(brandName))
        //                    {
        //                        user.brands.Add(brandName);
        //                        bi.BrandName = brandName.ToString();
        //                        bi.JobRole = brandInfo.ToLower().Replace(brandName.ToString(), "");
        //                        user.permissions.Add(bi);
        //                    }
        //                    //Should direct them automatically to first brand in list of permissions (sufficient for most)
        //                    if (isGlobalSet) continue;

        //                    HttpContext.Current.Session["ChangeBrand"] = brandName;
        //                    user.CurrentBrand = brandName;
        //                    isGlobalSet = true;
        //                }
        //            }

        //            var UserService = AppKernel.GetInstance<UserService>();
        //            var tagService = AppKernel.GetInstance<TagsService>();
        //            var retailerService = AppKernel.GetInstance<RetailerService>();
        //            //find the user
        //            var _user = UserService.FindByUniqueID(user.UniqueIdentification);

                   


        //            if (_user == null)
        //            {

        //                //add new user
        //                var newUser = new ITOK.Core.Data.Model.User
        //                {
        //                    UserIdentification = user.UniqueIdentification,
        //                    UserName = user.UserName,
        //                    UserEmail = user.emailAddress,
        //                    LastLoggedOn = DateTime.Now.AddDays(-14),
        //                    LogOnTime = DateTime.Now,
        //                    GivenName = user.FirstName,
        //                    RetailerID = user.RetailerID,
        //                    Telephone = user.TelephoneNumber,
        //                    Title = user.Title,
        //                    JobRoleId = CurrentJobRole,
        //                    IsBusinessManagement = isBusinessManagement,
        //                    IsEditorial = isEditor,
        //                    DefaultBrand = user.CurrentBrand //save initial brand as default
        //                };
        //                if (newUser.UserRoles == null)
        //                    newUser.UserRoles = new List<UserRole>();
        //                    //new user, add all their brands
        //                foreach(var brandName in user.brands)
        //                {
        //                    newUser.UserRoles.Add(new UserRole() { Brand = brandName });
        //                }

        //                if (!string.IsNullOrEmpty(newUser.RetailerID))
        //                {
        //                    var retailer = retailerService.FindByRetailerId(newUser.RetailerID, user.CurrentBrand);
        //                    if (retailer != null)
        //                    {
        //                        newUser.RetailerName = retailer.RetailerName;
        //                    }
        //                }


        //                UserService.Save(newUser);
        //                _user = newUser;
        //                user.LastLoginDate = DateTime.Now.AddDays(-14);
        //                user.UserID = newUser.UserIdentification;
        //                user.FirstLoad = true;
        //                user.favouriteTags = new List<FavouriteTag>();

        //            }
        //            else
        //            {
        //                if (_user.UserRoles == null)
        //                    _user.UserRoles = new List<UserRole>();

        //                _user.UserIdentification = user.UniqueIdentification;
        //                _user.UserEmail = user.emailAddress;
        //                _user.UserName = user.UserName;
        //                _user.LastLoggedOn = _user.LogOnTime == null ? DateTime.Now.AddDays(-14) : _user.LogOnTime;
        //                user.LastLoginDate = _user.LastLoggedOn;
        //                _user.LogOnTime = DateTime.Now;
        //                _user.GivenName = user.FirstName;
        //                _user.RetailerID = user.RetailerID;
        //                _user.Telephone = user.TelephoneNumber;
        //                ///TODO: do we overwrite here or just read it from what's been set our side??
        //                if (!string.IsNullOrEmpty(CurrentJobRole))
        //                    _user.JobRoleId = CurrentJobRole;

        //                _user.Title = user.Title;
        //                user.favouriteTags = _user.favouriteTags;
        //                _user.IsBusinessManagement = user.IsBusinessManagement = isBusinessManagement;
        //                _user.IsEditorial = user.IsEditorial = isEditor;
        //                //if no default brand then save it
        //                //_user.DefaultBrand = user.CurrentBrand;

        //                //check - all users are going in fresh so should have a brands
        //                user.CurrentBrand = _user.DefaultBrand;

        //                //role cleaning
        //                //add any new ones
                        
        //                ///TODO
        //                foreach(var brandName in user.brands)
        //                {
        //                    if (_user.UserRoles.All(i => i.Brand != brandName))
        //                        _user.UserRoles.Add(new UserRole() { Brand = brandName });
        //                }
        //                //remove any no longer presents
        //                foreach(var userRole in _user.UserRoles.ToList())
        //                {
        //                    if (user.brands.All(i => i != userRole.Brand))
        //                    {
        //                        _user.UserRoles.Remove(userRole);
        //                    }
        //                }

        //                if (!string.IsNullOrEmpty(_user.RetailerID))
        //                { 
        //                    var retailer = retailerService.FindByRetailerId(_user.RetailerID, user.CurrentBrand);
        //                    if (retailer != null)
        //                    {
        //                        user.RetailerName = retailer.RetailerName;
        //                    }
        //                }

        //                //save changes in roles back to the database
        //                UserService.Save(_user);                        
        //                user.userRoles = _user.UserRoles.ToList();
        //                user.UserID = _user.UserIdentification;
        //            }
        //            try
        //            {
        //                    //get user role for current brand and set up the single values used round the site
        //                var currentBrandRole = _user.UserRoles.First(i => i.Brand == user.CurrentBrand);
        //                if (currentBrandRole != null)
        //                {
        //                    var jobRoleTag = tagService.FindById(currentBrandRole.PrimaryRoleId);
        //                    user.JobRole = currentBrandRole.PrimaryRoleId;
        //                    user.JobRoleName = jobRoleTag.DisplayText;
        //                    user.JobRoleUrlSlug = jobRoleTag.UrlSlug;

        //                    var secondaryJobRoleTag = tagService.FindById(currentBrandRole.SecondaryRoleId);
        //                    user.SecondaryJobRole = secondaryJobRoleTag.Id;
        //                    user.SecondaryJobRoleName = secondaryJobRoleTag.DisplayText;
        //                    user.SecondaryJobRoleUrlSlug = secondaryJobRoleTag.UrlSlug;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //            }

        //            //try and get retailer
                  
        //            LoginManager.CurrentUser = user;

        //        }
        //        catch (Exception ex)
        //        {
        //            throw new Exception(ex.Message + ex.StackTrace);

        //        }
        //    }

        //    return LoginManager.CurrentUser;

        //}

        public static string GetJobRoleByname(string jobRoleName)
        {
            if (!string.IsNullOrEmpty(jobRoleName))
            {
                var jobService = AppKernel.GetInstance<JobRoleService>();
                var job = jobService.FindByName(jobRoleName).FirstOrDefault();
                if (job != null)
                    return job.Id.ToString();
                else
                    return "";
             
            }

            return "";
        }
    }

    public class WebPageSecurity : System.Web.UI.Page
    {
        protected override void OnPreInit(EventArgs e)
        {
            //check for user
            if (HasValidUser())
                base.OnPreInit(e);
            else
                Response.Redirect("~/accessdenied.aspx", true);
        }

        private bool HasValidUser()
        {
            return LoginManager.IsLoggedIn;

        }

        /// <summary>
        /// Load test user Desktop
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
   
        protected override System.Collections.Specialized.NameValueCollection DeterminePostBackMode()
        {
            try
            {
                return base.DeterminePostBackMode();
            }
            catch (HttpRequestValidationException ex)
            {
                Response.Redirect("~/Dangerousvalues.aspx", true);
                //            do the handling here

            }
            return null;
        }
    }



    public class AgencyPageSecurity : System.Web.UI.Page
    {
        protected override void OnPreInit(EventArgs e)
        {
            //check for user
            if (HasValidUser())
                base.OnPreInit(e);
            else
                Response.Redirect("~/accessdenied.aspx", true);
        }

        private bool HasValidUser()
        {
            return true;
        }
        protected override System.Collections.Specialized.NameValueCollection DeterminePostBackMode()
        {
            try
            {
                return base.DeterminePostBackMode();
            }
            catch (HttpRequestValidationException ex)
            {
                Response.Redirect("~/Dangerousvalues.aspx", true);
                //            do the handling here
            }
            return null;
        }

    }

    public class DummyWebPageSecurity : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            //check for user
            if (HasValidUser())
                base.OnPreInit(e);
            else
                Response.Redirect("~/accessdenied.aspx", true);
        }

        private bool HasValidUser()
        {
            return true;
        }
        protected override System.Collections.Specialized.NameValueCollection DeterminePostBackMode()
        {
            try
            {
                return base.DeterminePostBackMode();
            }
            catch (HttpRequestValidationException ex)
            {
                Response.Redirect("~/Dangerousvalues.aspx", true);
                //            do the handling here

            }
            return null;
        }
    }
}
