﻿using System.ComponentModel;

namespace ITOK.Core.Common.Constants
{
    public enum QuestionOptionType
    {
        [Description("Radio")]
        Radio = 0,
        [Description("Checkbox")]
        Checkbox = 1,
    }
}
