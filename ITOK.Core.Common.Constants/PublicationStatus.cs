﻿namespace ITOK.Core.Common.Constants
    {
    public enum PublicationStatus
        {
        Deleted,
        UnPublished,
        Published
        }

    public enum SubmissionStatus
    {
        InProgress,
        Submitted,
        Approved,
        Deleted,
        Rejected
    }
}