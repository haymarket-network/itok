﻿namespace ITOK.Core.Common.Constants
    {
    public enum HistoryGroup
        {
        Today,
        Yesterday,
        LastWeek,
        LastMonth,
        Older
        }
    }