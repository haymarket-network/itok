﻿using System.ComponentModel;

namespace ITOK.Core.Common.Constants
    {
    /// <summary>
    /// The options for where a piece of media can be hosted.
    /// Affects wich Media properties will be used to display the media item.
    /// For Example:
    /// If an image is hosted locally then the default
    /// </summary>
    public enum MediaHosting
        {
        /// <summary>
        /// Media is stored on the IAAF server
        /// </summary>
        [Description("Images and Non streamed Media")]
        Local,

        [Description("Youtube Video Code")]
        RemoteYoutube,

        [Description("Vimeo Video Code")]
        RemoteVimeo,

        //RemoteGoogleVideo,
        [Description("Streamed BrightCove Video or Audio")]
        RemoteBrightCove,

        [Description("Document i.e. PDF or DOC")]
        Document,

        //RemoteFlickr,
        //RemoteSoundCloud
        }
    }