﻿using System.ComponentModel;

namespace ITOK.Core.Common.Constants
    {
    /// <summary>
    /// Describes a . Dimensions and other associated configuration are defined in <see cref="HN.Core.Common.Config.HNConfig"/>.
    /// </summary>
    public enum MediaProfile
        {
        [Description("Original")]
        Original,

        [Description("800x400")]
        size_800x400,


        [Description("600x300")]
        size_600x300,

        [Description("300x200")]
        size_300x200,

        [Description("240x160")]
        size_240x160,

        [Description("100x67")]
        size_100x67,

        [Description("500x500")]
        size_500x500
        }
    }