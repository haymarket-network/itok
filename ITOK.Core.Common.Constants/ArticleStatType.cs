﻿namespace ITOK.Core.Common.Constants
    {
    public enum ArticleStatType
        {
        View,
        Share
        }

    public enum GotItStatType
        {
        Yes,
        No
        }

    public enum FavoriteStatType
        {
        Yes,
        No
        }

    public enum ReadingStatType
        {
        Yes,
        No
        }
    }