﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITOK.Core.Common.Constants
{
   public enum AnswerType
    {
        [Description("Numeric")]
        Numeric =0,
        [Description("Text")]
        Text =1,
        [Description("Options")]
        Options =3,
        [Description("Attachment")]
        Attachment =4,
        [Description("Template")]
        Template =5
    }
}
