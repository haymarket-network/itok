﻿namespace ITOK.Core.Common.Constants
    {
    public enum Permission
        {
        UseCMS = 0, //0
        EditUsers = 2, //1,2
        EditMedia = 6, //5,6
        EditTags = 8,
        UseFrontEnd = 12
        }
    }