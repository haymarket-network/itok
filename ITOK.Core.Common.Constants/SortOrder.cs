﻿namespace ITOK.Core.Common.Constants
    {
    public enum SortOrder
        {
        CreatedOn,
        UpdatedOn,
        LiveFrom,
        VersionDate,
        Relevance,
        }
    }