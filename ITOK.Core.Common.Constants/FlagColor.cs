﻿using System.ComponentModel;

namespace ITOK.Core.Common.Constants
{
    public enum FlagColor
    {
        [Description("Default")]
        Default = 0,
        [Description("Red")]
        Red=1,
        [Description("Green")]
        Green =2,
        [Description("Amber")]
        Amber =3,
    }
}
