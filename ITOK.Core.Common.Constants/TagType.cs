﻿namespace ITOK.Core.Common.Constants
{
    public enum TagType
    {
        Area=0,
        Year=1,
        Category=3,
        Events=2,
        Question=4
    }
}
