﻿using System.ComponentModel.DataAnnotations;

namespace ITOK.Core.Common.Constants
    {
    public enum ResultCard
        {
        Standard,

        [Display(Name = "Small Image")]
        SmallImage,

        [Display(Name = "Do not show in article")]
        HideInArticle,
    }
    }