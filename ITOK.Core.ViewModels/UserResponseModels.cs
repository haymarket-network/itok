﻿using System;

namespace ITOK.Core.ViewModels
{
    [Serializable]
    public class UserResponseItem
    {
        public string Headline;
        public string Details;
        public ResponseType Type;
    }
    [Serializable]
    public enum ResponseType
    {
        Notice,
        Warning,
        Error
    }
}