﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITOK.Core.ViewModels
{
    public class RelatedTag
    {
        public string id { get; set; }
        public string urlSlug { get; set; }
        public string DisplayText { get; set; }

        public int Count { get; set; }
    }
    public class RelatedTagsModel
    {
        public RelatedTagsModel()
        {
            tags = new List<RelatedTag>();
        }
        public List<RelatedTag> tags { get; set; }

    }
}
