﻿using System;
namespace ITOK.Core.Data
{
    public interface IMongoRepository1
    {
        System.Linq.IQueryable<T> AsQueryable<T>() where T : class;
        bool CollectionExists<T>(string collectionName = null) where T : class;
        void DropCollection<T>(string collectionName = null) where T : class;
        MongoDB.Driver.IMongoCollection<T> GetCollection<T>(string collectionName = null) where T : class;
        void Insert<T>(T data, string collectionName = null) where T : class;
        void InsertBatch<T>(System.Collections.Generic.IEnumerable<T> data, string collectionName = null) where T : class;
        void Remove<T>(T data, object id) where T : class;
        void Remove<T>(T data, object id, string name) where T : class;
        void RemoveByGuid<T>(T data, Guid id) where T : class;
        void RemoveWithComment<T>(T data, object id, string name, string versionComment = null) where T : class;
        void RemoveWithComment<T>(T data, object id, string versionComment = null) where T : class;
        MongoDB.Driver.ReplaceOneResult Update<T>(T data) where T : class;
        MongoDB.Driver.ReplaceOneResult UpdateWithComment<T>(T data, string versionComment = null) where T : class;
    }
}
