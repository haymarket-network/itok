﻿using System;
using ITOK.Core.Common.Constants;
using ITOK.Core.Common.Interfaces;
using ITOK.Core.Common.Utils;

namespace ITOK.Core.Data.DTOs
    {
    public class Retailer : IAuditable
        {
        public virtual string Id { get; set; }
        public virtual string RetailerId { get; set; }
        public virtual string RetailerName { get; set; }

        public virtual Brand Brand { get; set; }
        // IAuditable
        public string CreatedById { get; set; }

        private DateTime _createdOn;

        public DateTime CreatedOn { get { return _createdOn; } set { _createdOn = DateTimeUtilities.ToUtcPreserved(value); } }

        public string UpdatedById { get; set; }

        private DateTime _updatedOn;

        public DateTime UpdatedOn { get { return _updatedOn; } set { _updatedOn = DateTimeUtilities.ToUtcPreserved(value); } }
        }
    }