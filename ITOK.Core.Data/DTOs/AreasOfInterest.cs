﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using ITOK.Core.Common.Constants;

namespace ITOK.Core.Data.DTOs
{
    [Serializable]
    public class AreasOfInterest
    {
        public virtual string Id { get; set; }

        public virtual string ParentSlug { get; set; }

        public virtual string ParentId { get; set; }

        public virtual string InterestTag { get; set; }

        public virtual string InterestTagName { get; set; }

        public virtual string InterestTagId { get; set; }

        public virtual Brand brand { get; set; }

    }
}
