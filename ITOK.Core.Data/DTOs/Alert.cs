﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using ITOK.Core.Common.Constants;

namespace ITOK.Core.Data.DTOs
{
    [Serializable]
    public class Alert
    {
        public virtual string Id { get; set; }

        public virtual string Headline { get; set; }

        public virtual string AlertText { get; set; }
        public virtual IList<ITOK.Core.Data.Model.Tags> RoleTags { get; set; }
        public virtual Brand Brand { get; set; }
        public virtual DateTime ExpiryDate { get; set; }

        public virtual AlertStatus status {get; set;}

        public virtual string BrandArticleNumber { get; set; }

        public virtual DateTime CreatedOn { get; set; }
        public virtual DateTime UpdatedOn { get; set; }
   
    }
}
