﻿
using MongoDB.Bson.Serialization.Attributes;

namespace ITOK.Core.Data.DTOs
{

    public class ArticleStatsGroup
    {
        public string Id { get; set; }

        [BsonElement("value")]
        public double Value { get; set; }
    }
}

