﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ITOK.Core.Common.Constants;
using ITOK.Core.Common.Utils;

namespace ITOK.Core.Data.DTOs
{
    public class ReadingList
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public Boolean IsPublic { get; set; }

        private DateTime _timestamp;

        public DateTime Timestamp { get { return _timestamp; } set { _timestamp = DateTimeUtilities.ToUtcPreserved(value); } }

        public string UserId { get; set; }

        public string JobRoleId { get; set; }

        public string RetailerId { get; set; }

        public Brand brand { get; set; }
    }
}
