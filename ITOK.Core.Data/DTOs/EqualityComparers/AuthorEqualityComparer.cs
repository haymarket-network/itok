﻿using System.Collections.Generic;

namespace ITOK.Core.Data.DTOs.EqualityComparers
    {
    public class AuthorEqualityComparer : IEqualityComparer<Author>
        {
        public bool Equals(Author x, Author y)
            {
            return x.Id == y.Id;
            }

        public int GetHashCode(Author obj)
            {
            return base.GetHashCode();
            }
        }
    }