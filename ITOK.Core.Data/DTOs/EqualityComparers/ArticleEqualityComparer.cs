﻿using System.Collections.Generic;

namespace ITOK.Core.Data.DTOs.EqualityComparers
    {
    public class ArticleEqualityComparer : IEqualityComparer<Article>
        {
        public bool Equals(Article x, Article y)
            {
            return x.Id == y.Id;
            }

        public int GetHashCode(Article obj)
            {
            return base.GetHashCode();
            }
        }
    }