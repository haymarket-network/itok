﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace ITOK.Core.Data.DTOs
{
    [Serializable]
    public class TagTranslation
    {
        public virtual string Id { get; set; }

        public virtual string OriginalTag { get; set; }

        public virtual string OriginalSlug { get; set; }

        public virtual bool Ignore { get; set; }

        public virtual string NewTagSlugs { get; set; }

    }
}
