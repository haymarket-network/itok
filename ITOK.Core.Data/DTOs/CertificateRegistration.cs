﻿using System;
using ITOK.Core.Common.Constants;
using ITOK.Core.Common.Utils;

namespace ITOK.Core.Data
    {
   public class CertificateRegistration
        {

         public string Id { get; set; }
        public DateTime DateCampaignPerformed { get; set; }
        
        public string VIN { get; set; }

        
        public string vehicleRegistrationNumber { get; set; }
        
        public string vehicleModel { get; set; }


       

        

        private DateTime _timestamp;

        public DateTime Timestamp { get { return _timestamp; } set { _timestamp = DateTimeUtilities.ToUtcPreserved(value); } }
        public string UserId { get; set; }
        public string JobRoleId { get; set; }
        public string DealerId { get; set; }
        public Brand brand { get; set; }


       

        }
    }
