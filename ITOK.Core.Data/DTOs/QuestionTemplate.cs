﻿using ITOK.Core.Data.Model;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITOK.Core.Data.DTOs
{
    public class QuestionTemplate
    {
        [BsonId(IdGenerator = typeof(StringObjectIdGenerator))]
        public string Id { get; set; }
        public bool isReadOnly { get; set; }
        public List<QuestionRow> Questions { get; set; }

        public QuestionTemplate()
        {
            Questions = new List<QuestionRow>();
        }
    }

    public class QuestionRow
    {
        public string Question { get; set; }
        public AnswerDataType AnswerDataType { get; set; }
        public List<AnswerRow> AnswersRows { get; set; }
        public string MvcItemName { get; set; }

        public QuestionRow()
        {
            AnswersRows = new List<AnswerRow>();
        }
    }

    public class AnswerRow
    {
        public string Title { get; set; }
        public AnswerDataType AnswerDataType { get; set; }
        public String AnswerString { get; set; }
        public bool AnswerBool { get; set; }
        public int? AnswerNumber { get; set; }
        public DateTime AnswerDate { get; set; }
        public Decimal AnwerMoney { get; set; }
        public string MvcItemName { get; set; }
        public int QuestionVerticallyIndex { get; set; }
        public int QuestionHorizontallyIndex { get; set; }
    }
}
