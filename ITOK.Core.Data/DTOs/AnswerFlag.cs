﻿using ITOK.Core.Common.Constants;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;
using System;

namespace ITOK.Core.Data.DTOs
{
    [Serializable]
    public class AnswerFlag
    {
        [BsonId(IdGenerator = typeof(StringObjectIdGenerator))]
        public string Id { get; set; }
        public string AnswerId { get; set; }
        public FlagColor FlagColor { get; set; }
        public string SetColorBy { get; set; }
        public DateTime SetColorDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
