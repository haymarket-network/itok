﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ITOK.Core.Common.Constants;
using ITOK.Core.Common.Utils;

namespace ITOK.Core.Data.DTOs
{
    public class Search
    {
        public string Id { get; set; }

        public string tags { get; set; }
        public Brand brand { get; set; }

        public string searchText { get; set; }
        public SearchFilter searchFilter { get; set; }
        public SortOrder searchOrder { get; set; }

        private DateTime? _start { get; set; }
        private DateTime? _end { get; set; }
        
        public DateTime? StartFilter { get { return _start; } set { _start = DateTimeUtilities.ToUtcPreserved(value); } }
        public DateTime? EndFilter { get { return _end; } set { _end = DateTimeUtilities.ToUtcPreserved(value); } }
        private DateTime _timestamp;
        public DateTime Timestamp { get { return _timestamp; } set { _timestamp = DateTimeUtilities.ToUtcPreserved(value); } }
        public string UserId { get; set; }
        public string JobRoleId { get; set; }
        public int TotalItemCount { get; set; }
        
    }
}
