﻿using ITOK.Core.Common.Constants;
using System;
using ITOK.Core.Data.Model;

namespace ITOK.Core.Data.DTOs
{
    [Serializable]
    public class Question
    {
        public virtual string Id { get; set; }

        public virtual string QuestionText { get; set; }
        public virtual AnswerType AnswerType { get; set; }
        public virtual string Options { get; set; }
        public virtual string ParentTagSlug { get; set; }
        public virtual string ParentTagId { get; set; }
        public QuestionTemplateType QuestionTemplates { get; set; }
        public virtual Brand Brand { get; set; }
        public bool Show { get; set; }
        public int Order { get; set; }
        public bool IsAttachment { get; set; }
        public QuestionOptionType OptionType { get; set; }
    }
}
