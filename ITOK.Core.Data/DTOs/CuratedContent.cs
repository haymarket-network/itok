﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using ITOK.Core.Common.Constants;

namespace ITOK.Core.Data.DTOs
{
    [Serializable]
    public class CuratedContent
    {
        public virtual string Id { get; set; }

        public virtual string ArticleId { get; set; }

        public virtual string PrimaryJobRole { get; set; }
        public virtual int Order { get; set; }
        public virtual IList<ITOK.Core.Data.Model.Tags> RelatedTags { get; set; }
        public virtual Brand Brand { get; set; }

    }
}
