﻿using System;

namespace ITOK.Core.Data.DTOs
{
    [Serializable]
    public class MongoOutputCacheItem
    {
        public string Id { get; set; }

        public byte[] Item { get; set; }

        public string KeyUrl { get; set; }

        public DateTime Expiration { get; set; }

        public DateTime LastModified { get; set; }
    }
}
