﻿using System;
using System.Collections.Generic;
using ITOK.Core.Common.Constants;
using ITOK.Core.Common.Interfaces;
using ITOK.Core.Common.Utils;
using ITOK.Core.Data.Model;

namespace ITOK.Core.Data.DTOs
    {
    public class SubmittedMedia : IPublishable, IAuditable, IVersionable
        {
        public virtual string Id { get; set; }


        //needed to cross reference during import
        public virtual string ImportId { get; set; }
        /// <summary>
        /// The name used to list this media item in the cms
        /// </summary>
        public virtual string Title { get; set; }
        public virtual Brand Brand { get; set; }
        /// <summary>
        /// The page title
        /// </summary>
        public virtual string SEOTitle { get; set; }

        /// <summary>
        /// The name of this media item used to route requests to the media detail page. So http://www.iaaf.org/media/usain-bolt-in-london-1 can be resolved to a particular media entity.
        /// </summary>
        public virtual string UrlSlug { get; set; }

        /// <summary>
        /// Search engine friendly description of the article
        /// </summary>
        public virtual string MetaDescription { get; set; }

        public virtual string Credit { get; set; }

        public virtual bool ShowInMedia { get; set; }

        public virtual bool Complete { get; set; }

        /// <summary>
        /// The media item filename which takes the form of a GUID to avoid conflicts.
        /// Only necessary when hosted locally and if not present then it's assumed nothing has been uploaded for this media item yet.
        /// </summary>
        public virtual string FileName { get; set; }
        public virtual string OriginalFileName { get; set; }

        /// <summary>
        /// What is the media item.
        /// </summary>
        //public virtual MediaType Type { get; set; }

        /// <summary>
        /// The file format (e.g. png, avi, mp3, etc...)
        /// </summary>
        public virtual FileFormat Format { get; set; }

        /// <summary>
        /// How this media item is hosted: Locally, Remotely and if the latter then which service?
        /// </summary>
        public virtual MediaHosting Hosting { get; set; }

        /// <summary>
        /// Links related to this media item
        /// </summary>
 
        /// <summary>
        /// When hosted remotely (YouTube, Vimeo, etc..) this url is used to embed the video in IAAF pages
        /// </summary>
        public virtual string RemoteItemCode { get; set; }

        public virtual string PlayerId { get; set; }
        public string Base64Image { get; set; }

       
        public virtual string FileNameUrl { get; set; }
        
        /// <summary>
        /// <para>In the case of images the this property reflects the original width of the image. Not used when displaying the image.</para>
        /// <para>
        ///     For videos the SourceWidth will be used to generate the embed code.
        ///     i.e. videos are not resized from their orinial dimensions.
        /// </para>
        /// </summary>
        public virtual int SourceWidth { get; set; }

        /// <summary>
        /// Same as SourceWidth
        /// </summary>
        public virtual int SourceHeight { get; set; }

        /// <summary>
        /// The ArticleMediaTypes that this item is suitable for
        /// </summary>
        public virtual IList<MediaRatio> AvailableRatios { get; set; }
      
        // IPublishable
        public PublicationStatus Status { get; set; }

        private DateTime _liveFrom;

        public DateTime LiveFrom { get { return _liveFrom; } set { _liveFrom = DateTimeUtilities.ToUtcPreserved(value); } }

        private DateTime? _liveTo;

        public DateTime? LiveTo { get { return _liveTo; } set { _liveTo = DateTimeUtilities.ToUtcPreserved(value); } }

        // IAuditable
        public string CreatedById { get; set; }

        private DateTime _createdOn;

        public DateTime CreatedOn { get { return _createdOn; } set { _createdOn = DateTimeUtilities.ToUtcPreserved(value); } }

        public string UpdatedById { get; set; }

        private DateTime _updatedOn;

        public DateTime UpdatedOn { get { return _updatedOn; } set { _updatedOn = DateTimeUtilities.ToUtcPreserved(value); } }

        public SubmittedMedia()
            {
            AvailableRatios = new List<MediaRatio>();
           
            }
        }
    }