﻿using System.Collections.Generic;

namespace ITOK.Core.Data.DTOs
{
    public class UserEvent
    {
        public string EventId { get; set; }
        public string YearId { get; set; }
        public string AreaId { get; set; }
        public List<UserQuestion> QuestionList { get; set; }
    }

    public class UserQuestion
    {
        public string QuestionId { get; set; }
        public bool isAnswered { get; set; }
    }
}