﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace ITOK.Core.Data.DTOs
{
    [Serializable]
    public class GotItCount
    {
        public virtual string Id { get; set; }

        public virtual string ArticleId { get; set; }

        public virtual int YesCount { get; set; }

        public virtual int NoCount { get; set; }
    }
}
