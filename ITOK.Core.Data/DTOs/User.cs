﻿using ITOK.Core.Common.Constants;
using ITOK.Core.Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITOK.Core.Data.DTOs
{
    [Serializable]
    public class User
    {

        public string Id { get; set; }

         public virtual string Forename { get; set; }

          public virtual string Surname { get; set; }

        public virtual string Email { get; set; }

        public virtual bool AcceptedRegulations { get; set; }

        /// <summary>
        /// Salt and hash!
        /// </summary>
        public virtual string Password { get; set; }

        public virtual int LoginAttempts { get; set; }

        public string Organisation { get; set; }
        public virtual IList<Permission> Permissions { get; set; }
         public DateTime LastLoggedOn { get; set; }
        public DateTime LastLoginDate { get; set; }
        // IAuditable
        public string CreatedById { get; set; }

        private DateTime _createdOn;

        public virtual DateTime CreatedOn { get { return _createdOn; } set { _createdOn = DateTimeUtilities.ToUtcPreserved(value); } }

        public string UpdatedById { get; set; }

        private DateTime _updatedOn;

        public virtual DateTime UpdatedOn { get { return _updatedOn; } set { _updatedOn = DateTimeUtilities.ToUtcPreserved(value); } }
        public List<UserEvent> UserEvents { get; set; }
        public bool IsItokLeader { get; set; }
    }
   

}
