﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using ITOK.Core.Common.Interfaces;
using ITOK.Core.Common.Utils;

namespace ITOK.Core.Data.DTOs
{
    [Serializable]
    public class UserGroupMember : IAuditable
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public virtual string Id { get; set; }
        public virtual string UserGroupID { get; set; }
        public virtual string UserID { get; set; }

        // IAuditable
        public string CreatedById { get; set; }

        private DateTime _createdOn;

        public DateTime CreatedOn { get { return _createdOn; } set { _createdOn = DateTimeUtilities.ToUtcPreserved(value); } }

        public string UpdatedById { get; set; }

        private DateTime _updatedOn;

        public DateTime UpdatedOn { get { return _updatedOn; } set { _updatedOn = DateTimeUtilities.ToUtcPreserved(value); } }
    }
}