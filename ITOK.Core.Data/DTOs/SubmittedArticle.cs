﻿using ITOK.Core.Common.Utils;
using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;
using ITOK.Core.Common.Constants;
using ITOK.Core.Common.Interfaces;

namespace ITOK.Core.Data.DTOs
{
    [Serializable]
    public class SubmittedArticle : IAuditable, IVersionable
    {
        public virtual string Id { get; set; }

        public virtual string Title { get; set; }

        public virtual string SEOTitle { get; set; }

        public ArticleType ArticleType { get; set; }

        public virtual Brand brand { get; set; }
        public virtual string BrandArticleNumber { get; set; }

        public virtual string UrlSlug { get; set; }

        public virtual string OriginalUrlSlug { get; set; }

        public virtual string MetaDescription { get; set; }

        public virtual string StandFirst { get; set; }

        public virtual string Body { get; set; }

        public virtual IList<Brand> brands { get; set; }

        public virtual string PlainTextBody { get; set; }

        public virtual string ArticleTypeId { get; set; }

        public virtual string AuthorUserId { get; set; }

        public virtual string PrimaryMediaId { get; set; }

        public virtual ResultCard ResultCard { get; set; }


        public virtual IList<string> RelatedMediaIds { get; set; } // TODO: Return read-only list

        public virtual IList<string> RelatedGalleryIds { get; set; } // TODO: Return read-only list

        public virtual IList<string> RelatedDownloadIds { get; set; } // TODO: Return read-only list


        public virtual IList<string> RelatedArticleIds { get; set; }// TODO: Return read-only list

        public virtual IList<ITOK.Core.Data.Model.Tags> RelatedTags { get; set; }// TODO: Return read-only list

         public IList<string> Tags { get; set; }
 
        public virtual int LiveFromDay { get { return LiveFrom.Day; } }

        public virtual int LiveFromMonth { get { return LiveFrom.Month; } }

        public virtual int LiveFromYear { get { return LiveFrom.Year; } }

        // IPublishable
        public virtual SubmissionStatus Status { get; set; }

        private DateTime? _actionDate { get; set; }

        public virtual DateTime? ActionDate { get { return _actionDate; } set { _actionDate = DateTimeUtilities.ToUtcPreserved(value); } }


        private DateTime? _versionDate { get; set; }

        public virtual DateTime? VersionDate { get { return _versionDate; } set { _versionDate = DateTimeUtilities.ToUtcPreserved(value); } }

        private DateTime _liveFrom;

        public virtual DateTime LiveFrom { get { return _liveFrom; } set { _liveFrom = DateTimeUtilities.ToUtcPreserved(value); } }

        private DateTime? _liveTo;

        public virtual DateTime? LiveTo { get { return _liveTo; } set { _liveTo = DateTimeUtilities.ToUtcPreserved(value); } }

        // IAuditable
        public string CreatedById { get; set; }

        private DateTime _createdOn;

        public DateTime CreatedOn { get { return _createdOn; } set { _createdOn = DateTimeUtilities.ToUtcPreserved(value); } }

        public string UpdatedById { get; set; }

        private DateTime _updatedOn { get; set; }

        public DateTime UpdatedOn { get { return _updatedOn; } set { _updatedOn = DateTimeUtilities.ToUtcPreserved(value); } }

        public int ViewCount { get; set; }

        public virtual Boolean EnableGotItBlock { get; set; }
        
        public virtual Boolean EnableMessageAuthorBlock { get; set; }


        [BsonIgnoreIfNull]
        public double? TextMatchScore { get; set; }

        public SubmittedArticle()
        {           
            RelatedArticleIds = new List<string>();
            RelatedMediaIds = new List<string>();
            RelatedTags = new List<ITOK.Core.Data.Model.Tags>();
            Status = SubmissionStatus.InProgress;
            EnableGotItBlock = true;
            EnableMessageAuthorBlock = true;
        }



        public List<string> AuthorIds { get; set; }
    }
}
