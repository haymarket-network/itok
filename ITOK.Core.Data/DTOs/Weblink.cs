﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using ITOK.Core.Common.Constants;

namespace ITOK.Core.Data.DTOs
{
    [Serializable]
    public class Weblink
    {
        public virtual string Id { get; set; }

        public virtual string DisplayText { get; set; }

        public virtual string Url { get; set; }
        public virtual IList<ITOK.Core.Data.Model.Tags> RoleTags { get; set; }
        public virtual Brand Brand { get; set; }
      


    }
}
