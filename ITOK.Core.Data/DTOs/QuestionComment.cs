﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;
using System;
using System.Collections.Generic;

namespace ITOK.Core.Data.DTOs
{
    [Serializable]
    public class QuestionComment
    {
        [BsonId(IdGenerator = typeof(StringObjectIdGenerator))]
        public string Id { get; set; }
        public string AnswerId { get; set; }
        public string AreaId { get; set; }
        public string EventId { get; set; }
        public string YearId { get; set; }

        public IList<UsersComment> UsersComment;

        public QuestionComment()
        {
            UsersComment = new List<UsersComment>();
        }
    }

    public class UsersComment
    {
        public string UserId { get; set; }
        public string Comment { get; set; }
        public DateTime CommentDate { get; set; }
    }
}
