﻿using System;
using ITOK.Core.Common.Constants;
using ITOK.Core.Common.Utils;

namespace ITOK.Core.Data.DTOs
    {
    public class ArticleStat
        {
        public string Id { get; set; }

       public string ArticleId { get; set; }

        public ArticleType ArticleType { get; set; }

        public ArticleStatType StatisticType { get; set; }

        private DateTime _timestamp;

        public DateTime Timestamp { get { return _timestamp; } set { _timestamp = DateTimeUtilities.ToUtcPreserved(value); } }
        public string UserId { get; set; }
        public string JobRoleId { get; set; }



        public Brand Brand { get; set; }
        }
    }