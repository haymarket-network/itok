﻿using MongoDB.Bson;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using ITOK.Core.Common.Constants;

namespace ITOK.Core.Data.DTOs
{
    [Serializable]
    public class Tags
    {
        public virtual string Id { get; set; }

        public virtual string DisplayText { get; set; }

        public virtual string UrlSlug { get; set; }

        public virtual string ParentSlug { get; set; }

        public virtual string ParentId { get; set; }

        public virtual IDictionary<string, string> TopicHeaders { get; set; }

        public virtual  IList<string> ArticleUrls { get; set; }

        public virtual string Description { get; set; }

        public virtual int TagType { get; set; }
        public virtual Brand Brand { get; set; }

        public virtual string ResponsibleName { get; set;  }
        public virtual string ResponsibleTitle { get; set; }
        public virtual string ResponsiblePhone { get; set; }
        public virtual string ResponsibleEmail { get; set; }

        public virtual string ContactEmail { get; set; }

        public bool Show { get; set; }

        public int Order { get; set; }
    }
}
