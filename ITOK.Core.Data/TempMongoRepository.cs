﻿using System;
using System.Configuration;
using System.Linq;
using ITOK.Core.Common.Config;
using ITOK.Core.Data.DTOs;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;

namespace ITOK.Core.Data
{
    /// <summary>
    /// Much stripped down version of MongoRepository used to
    /// temporarily store [view model] entities that are being edited.
    /// </summary>
    public class TempMongoRepository<T> where T : class
    {
        private const string CollectionPrefix = "temp_";
        private const string ConnectionStringKey = "mongo";
        private const string NullIdReplacement = "null_entity_id";
        private string _key = "";
        private Func<T, object> _idExpr;
        protected static IMongoDatabase _db;

        public TempMongoRepository(Func<T, object> idExpr, params string[] keys)
        {
            if (keys == null || keys.Length == 0) throw new ArgumentException("At least one key must be provided", "keys");
            if (idExpr == null) throw new ArgumentException("An idExpr must be provided", "idExpr");

            _key = string.Join("_", keys);
            _idExpr = idExpr;

            var connectionString = !string.IsNullOrEmpty(ConfigurationManager.ConnectionStrings[ConnectionStringKey].ConnectionString)
                ? ConfigurationManager.ConnectionStrings[ConnectionStringKey].ConnectionString
                : ConfigurationManager.AppSettings[ConnectionStringKey];

                      var client = new MongoClient(connectionString);
            _db = client.GetDatabase(ITOKConfig.Instance.Database.Name); //ToDo:Change this

            if (!BsonClassMap.IsClassMapRegistered(typeof(TempWrapperFor<T>)))
            {
                /*
                 * Register the type. Doing this in the constructor as opposed
                 * to loading static methods as per MongoRepository allows us
                 * to throw any type of class at this repo.
                 */
                BsonClassMap.RegisterClassMap<TempWrapperFor<T>>(cm =>
                {
                    cm.AutoMap();
                });
            }
        }

        public void Set(T entity)
        {
            var id = getEntityWrapperId(entity);
            var wrappedEntity = new TempWrapperFor<T>
            {
                Entity = entity,
                Id = id
            };
            // Check if this entity already exists in the collection

            var filter = Builders<TempWrapperFor<T>>.Filter.Eq("Id", id);
            ReplaceOneResult result = getCollection().ReplaceOne(filter, wrappedEntity, new UpdateOptions { IsUpsert = true });

            //getCollection().Save<TempWrapperFor<T>>(wrappedEntity);
        }

        public T Get(object entityId)
        {
            var id = getEntityWrapperId(entityId);
            var wrappedEntity = asQueryable().SingleOrDefault(x => x.Id == id);
            return wrappedEntity != null ? wrappedEntity.Entity : null;
        }

        public void Remove(object entityId)
        {           
            var id = getEntityWrapperId(entityId);
            var filter = Builders<TempWrapperFor<T>>.Filter.Eq("Id", id);
            getCollection().DeleteOne(filter);
        }

        public bool Exists(object entityId)
        {
            var id = getEntityWrapperId(entityId);
            return asQueryable().Any(x => x.Id == id);
        }

        #region Private Methods

        private IMongoCollection<TempWrapperFor<T>> getCollection()
        {
            return _db.GetCollection<TempWrapperFor<T>>(CollectionPrefix + typeof(T).FullName);
        }

        private IQueryable<TempWrapperFor<T>> asQueryable()
        {
            return getCollection().AsQueryable();
        }

        private string getEntityWrapperId(T entity)
        {
            return getEntityWrapperId(getId(entity));
        }

        private string getEntityWrapperId(object id)
        {
            if (id == null) id = NullIdReplacement;
            return id + _key;
        }

        private string getId(T entity)
        {
            var id = _idExpr(entity);
            return id != null ? id.ToString() : null;
        }

        #endregion Private Methods
    }
}