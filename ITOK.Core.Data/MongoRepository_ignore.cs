﻿/// <summary>
/// A MongoDB repository. Maps to a collection with the same name
/// as type TEntity.
/// </summary>
/// <typeparam name="T">Entity type for this repository</typeparam>
//public class MongoDbRepositoryIGNORE<TEntity> :
//    IRepository<TEntity> where
//        TEntity : EntityBase
//{
//    private IMongoDatabase database;
//    private IMongoCollection<TEntity> collection;

//    public MongoDbRepositoryIGNORE()
//    {
//        GetDatabase();
//        GetCollection();
//    }

//    public bool Insert(TEntity entity)
//    {
//        entity.Id = Guid.NewGuid();
//        collection.InsertOne(entity);
//        return true;

//    }

//    public bool Update(TEntity entity)
//    {
//        if (entity.Id == null)
//            return Insert(entity);

//        var result =  collection
//            .ReplaceOne(x => x.Id == entity.Id, 
//                entity, 
//                new UpdateOptions { IsUpsert = true});
//        return result.MatchedCount > 0;
//    }

//    public bool Delete(TEntity entity)
//    {
//        return collection
//            .DeleteOne(x => x.Id == entity.Id)
//                .DeletedCount > 0;
//    }

//    public IList<TEntity>
//        SearchFor(Expression<Func<TEntity, bool>> predicate)
//    {
//        return collection
//            .AsQueryable<TEntity>()
//                .Where(predicate.Compile())
//                    .ToList();
//    }

//    public IList<TEntity> GetAll()
//    {
//        //should return all
//        return collection.Find<TEntity>(_ => true).ToList();
//    }

//    public TEntity GetById(Guid id)
//    {
              
//        //srsly?
//        var item = collection.Find<TEntity>(x => x.Id == id);
//        return (TEntity)item;
//    }

//    #region Private Helper Methods
//    private void GetDatabase()
//    {
//        var client = new MongoClient(GetConnectionString());
//        database = client.GetDatabase(GetDatabaseName());
//    }

//    private string GetConnectionString()
//    {
//        return ConfigurationManager
//            .AppSettings
//                .Get("MongoDbConnectionString")
//                    .Replace("{DB_NAME}", GetDatabaseName());
//    }

//    private string GetDatabaseName()
//    {
//        return ConfigurationManager
//            .AppSettings
//                .Get("MongoDbDatabaseName");
//    }

//    private void GetCollection()
//    {
//        collection = database
//            .GetCollection<TEntity>(typeof(TEntity).Name);
//    }
//    #endregion
//}