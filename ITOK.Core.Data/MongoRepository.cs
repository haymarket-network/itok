﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using ITOK.Core.Common;
using ITOK.Core.Common.Config;
using ITOK.Core.Common.Interfaces;
//using ITOK.Core.Data.VersionInterceptors;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;
using ITOK.Core.Data.VersionInterceptors;
using MongoDB.Bson.Serialization;

namespace ITOK.Core.Data
{
    public class MongoRepository : BaseMongoRepository, ITOK.Core.Data.IMongoRepository1//, IMongoRepository
    {

        private static bool _mongoInitialized;

        /// <summary>
        /// Interceptors should implement the "interested in" model adopted in version interceptors
        /// </summary>

        #region Interceptors

        ///TODO
        private static IList<IInterceptor> _updateInterceptors = new List<IInterceptor>();
        private static IList<IInterceptor> _insertInterceptors = new List<IInterceptor>();
        private static IList<IInterceptor> _removeInterceptors = new List<IInterceptor>();
        private static IList<IInterceptor> _postBatchInsertInterceptors = new List<IInterceptor>();

        #endregion Interceptors

        #region Versioning

        /// <summary>
        /// TODO
        /// </summary>
        private static IDictionary<Type, IVersionInterceptor> _updateVersionInterceptors = new Dictionary<Type, IVersionInterceptor>();
        private static IDictionary<Type, IVersionInterceptor> _removeVersionInterceptors = new Dictionary<Type, IVersionInterceptor>();

        #endregion Versioning

        /// <summary>
        ///
        /// </summary>
        static MongoRepository()
        {
            InitializeMongo();

            var connectionString = !string.IsNullOrEmpty(ConfigurationManager.ConnectionStrings["mongo"].ConnectionString)
                ? ConfigurationManager.ConnectionStrings["mongo"].ConnectionString
                : ConfigurationManager.AppSettings["mongo"];

            var client = new MongoClient(connectionString);
            _db = client.GetDatabase(ITOKConfig.Instance.Database.Name); //ToDo:Change this

            //EnsureIndexes();
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collectionName"></param>
        /// <returns></returns>
        public virtual bool CollectionExists<T>(string collectionName = null) where T : class
        {
            var filter = new BsonDocument("name", collectionName);
            //filter by collection name
            var collections = _db.ListCollectionsAsync(new ListCollectionsOptions { Filter = filter });
            //check for existence
  
            return (collections.GetAwaiter().GetResult().Any());
        }

        /// <summary>
        /// Be very careful when using this method! Any entity modification will bypass interceptor actions.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collectionName"></param>
        /// <returns></returns>
        public virtual IMongoCollection<T> GetCollection<T>(string collectionName = null) where T : class
        {
            return _db.GetCollection<T>(getCollectionName<T>(collectionName));
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collectionName"></param>
        public virtual void DropCollection<T>(string collectionName = null) where T : class
        {
           // GetCollection<T>(collectionName).DeleteOne();
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <param name="collectionName"></param>
        /// <returns></returns>
        public void Insert<T>(T data, string collectionName = null) where T : class
        {
            applyIntercepts(data, _insertInterceptors);


            var result = GetCollection<T>(collectionName)
                .InsertOneAsync(data);

            var success = result.GetAwaiter().IsCompleted;
        
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <param name="collectionName"></param>
        public virtual void InsertBatch<T>(IEnumerable<T> data, string collectionName = null) where T : class
        {
            if (!IEnumerables.IsNullOrEmpty(data))
            {
                //var result = GetCollection<T>(collectionName)
                //    .InsertBatch(data.Distinct(), new MongoInsertOptions { SafeMode = SafeMode.True });

                    GetCollection<T>(collectionName)
                   .InsertMany(data, new InsertManyOptions { });
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <param name="versionComment"></param>
        /// <returns></returns>
        public virtual ReplaceOneResult UpdateWithComment<T>(T data, string versionComment = null) where T : class
        {
            applyIntercepts(data, _updateInterceptors);
            applyVersionIntercepts<T>(data, _updateVersionInterceptors, versionComment);

            var id = data.GetType().GetProperty("Id").GetValue(data, null);
            var filter = Builders<T>.Filter.Eq("Id", id);
            return GetCollection<T>()
                .ReplaceOne(filter,
                data,
                new UpdateOptions { IsUpsert = true });
        }
        
        /// <summary>
        /// DO NOT use this method for inserts! We cannot enforce this because of ID inconsistency.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        public virtual ReplaceOneResult Update<T>(T data) where T : class
        {
            applyIntercepts(data, _updateInterceptors);
            applyVersionIntercepts<T>(data, _updateVersionInterceptors);
            var id = data.GetType().GetProperty("Id").GetValue(data, null);
            var filter = Builders<T>.Filter.Eq("Id", id);
            return GetCollection<T>()
                .ReplaceOne(filter,
                data,
                new UpdateOptions { IsUpsert = true });
        }

        //public virtual T FindModify<T>(T data, IMongoQuery query, IMongoSortBy sortBy, IMongoUpdate update) where T : class
        //{
        //    var exists = GetCollection<T>().Find(query).Count();

        //    if (exists == 0)
        //    {
        //        GetCollection<T>()
        //          .Save(data);
        //        return data;
        //    }
        //    else
        //    {
        //        var result = GetCollection<T>().FindOneAndUpdate(
        //                                            query,
        //                                            sortBy,
        //                                            update,
        //                                            true // return new document
        //                                            );
        //        return BsonSerializer.Deserialize<T>(result.ModifiedDocument);
        //    }
        //}

        //public virtual void FindAndModify<T>(T data, IMongoQuery query, IMongoSortBy sortBy, IMongoUpdate update) where T : class
        //{
        //    var exists = GetCollection<T>().Find(query).Count();

        //    if (exists == 0)
        //    {
        //        GetCollection<T>()
        //          .Save(data);
        //        // return data;
        //    }
        //    else
        //    {
        //        var result = GetCollection<T>().FindAndModify(
        //                                            query,
        //                                            sortBy,
        //                                            update,
        //                                            false// return new document
        //                                            );
        //    }
        //}

        //public void FindAndModify<T>(IMongoQuery mongoQuery, IMongoSortBy mongoSortBy, UpdateBuilder update) where T : class
        //{
        //    var result = GetCollection<T>().FindAndModify(
        //                                        mongoQuery,
        //                                        mongoSortBy,
        //                                        update,
        //                                        false// return new document
        //                                       );
        //}

        //public virtual MapReduceResult MapReduce<T>(T data, IMongoQuery query, BsonJavaScript map, BsonJavaScript reduce, MapReduceOptionsBuilder options) where T : class
        //{
        //    return GetCollection<T>().MapReduce(query, map, reduce, options);
        //}

        //public virtual MapReduceResult MapReduce<T>(IMongoQuery query, BsonJavaScript map, BsonJavaScript reduce, MapReduceOptionsBuilder options) where T : class
        //{
        //    return GetCollection<T>().MapReduce(query, map, reduce, options);
        //}

        public virtual void RemoveWithComment<T>(T data, object id, string versionComment = null) where T : class
        {
            applyIntercepts(data, _removeInterceptors);
            applyVersionIntercepts<T>(data, _removeVersionInterceptors, versionComment);
            var filter = Builders<T>.Filter.Eq("Id", data.GetType().GetProperty("Id").GetValue(this, null));
            GetCollection<T>().DeleteOne(filter);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <param name="id">Required because IDs are implemented inconsistently</param>
        public virtual void Remove<T>(T data, object id) where T : class
        {
            applyIntercepts(data, _removeInterceptors);
            applyVersionIntercepts<T>(data, _removeVersionInterceptors);
            var otherProp = typeof(T).GetProperty("Id");
            var valFirst = otherProp.GetValue(data, null);
            var filter = Builders<T>.Filter.Eq("Id", valFirst);
            GetCollection<T>().DeleteOne(filter);
        }

        public virtual void RemoveByGuid<T>(T data, Guid id) where T : class
        {
            applyIntercepts(data, _removeInterceptors);
            applyVersionIntercepts<T>(data, _removeVersionInterceptors);
            var filter = Builders<T>.Filter.Eq("Id", data.GetType().GetProperty("Id").GetValue(data, null));
            GetCollection<T>().DeleteOne(filter);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="versionComment"></param>
        public virtual void RemoveWithComment<T>(T data, object id, string name, string versionComment = null) where T : class
        {
            applyIntercepts(data, _removeInterceptors);
            applyVersionIntercepts<T>(data, _removeVersionInterceptors, versionComment);
            var filter = Builders<T>.Filter.Eq("Id", data.GetType().GetProperty("Id").GetValue(this, null));

            GetCollection<T>().DeleteOne(filter);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <param name="id"></param>
        /// <param name="name"></param>
        public virtual void Remove<T>(T data, object id, string name) where T : class
        {
            applyIntercepts(data, _removeInterceptors);
            applyVersionIntercepts<T>(data, _removeVersionInterceptors);
            var filter = Builders<T>.Filter.Eq("Id", data.GetType().GetProperty("Id").GetValue(this, null));

            GetCollection<T>().DeleteOne(filter);
            //if (id is int)
            //    GetCollection<T>().Remove(Query.EQ(name, BsonInt32.Create(id)));
            //else
            //    GetCollection<T>().Remove(Query.EQ(name, BsonString.Create(id)));
        }

        //public virtual void Remove<T>(IMongoQuery eq) where T : class
        //{
        //    GetCollection<T>().Remove(eq);
        //}

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public virtual IQueryable<T> AsQueryable<T>() where T : class
        {
            return GetCollection<T>().AsQueryable();
        }

        /// <summary>
        ///
        /// </summary>
        public static void InitializeMongo()
        {
            if (_mongoInitialized) return;

            //var profile = new ConventionProfile();
            //profile.SetIgnoreIfNullConvention(new IgnoreIfNullConvention());
            //profile.SetIdMemberConvention(new NamedIdMemberConvention("Id"));

            //BsonClassMap.RegisterConventions(profile, t => true);

            var types = System.Reflection.Assembly.GetAssembly(typeof(MongoRepository))
                .GetTypes()
                .Where(x => x.Namespace.Contains("ITOK.Core.Data.Mapping"));

            var pack = new ConventionPack();
            pack.Add(new NamedIdMemberConvention("Id"));
            pack.Add(new IgnoreIfNullConvention(true));
            ConventionRegistry.Register("basic", pack, t => true);

            foreach (var type in types)
                Activator.CreateInstance(type);

            _mongoInitialized = true;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="interceptor"></param>
        public static void RegisterUpdateInterceptor(IInterceptor interceptor)
        {
            _updateInterceptors.Add(interceptor);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="interceptor"></param>
        public static void RegisterInsertInterceptor(IInterceptor interceptor)
        {
            _insertInterceptors.Add(interceptor);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="interceptor"></param>
        public static void RegisterRemoveInterceptor(IInterceptor interceptor)
        {
            _removeInterceptors.Add(interceptor);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="membershipProvider"></param>
        /// <param name="idExpr"></param>
        public static void ConfigureVersioning<T>(IMembershipProvider membershipProvider, Func<T, object> idExpr) where T : class, IVersionable
        {
            var entityType = typeof(T);
            _updateVersionInterceptors.Add(entityType, new UpdateVersionInterceptor<T>(membershipProvider, idExpr));
        }

        /// <summary>
        ///
        /// </summary>
        public static void EnsureIndexes()
        {


        }

       

        #region Private/Protected Methods

        /// <summary>
        ///
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="interceptors"></param>
        private void applyIntercepts(object entity, IList<IInterceptor> interceptors)
        {
            foreach (var interceptor in interceptors)
            {
                if (interceptor.IsInterestedIn(entity)) interceptor.Intercept(entity);
            }
        }

        private void applyVersionIntercepts<T>(object entity, IDictionary<Type, IVersionInterceptor> versionInterceptors, string versionComment = null)
        {
            var entityType = typeof(T);
            if (versionInterceptors.ContainsKey(entityType))
            {
                versionInterceptors[entityType].Intercept(entity, versionComment);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collectionName"></param>
        /// <returns></returns>
        protected virtual string getCollectionName<T>(string collectionName)
        {
            return string.IsNullOrWhiteSpace(collectionName) ? typeof(T).Name.ToLower() : collectionName;
        }

        #endregion Private/Protected Methods
    }
}