﻿using MongoDB.Driver;

namespace ITOK.Core.Data
{
    public class BaseMongoRepository
    {
        protected static IMongoDatabase _db;

        public IMongoDatabase MongoDatabase
        {
            get { return _db; }
        }
    }
}
