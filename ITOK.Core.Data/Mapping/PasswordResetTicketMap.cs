﻿using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using ITOK.Core.Data.DTOs;

namespace ITOK.Core.Data.Mapping
    {
    internal class PasswordResetTicketMap
        {
        public PasswordResetTicketMap()
            {
            BsonClassMap.RegisterClassMap<PasswordResetTicket>(cm =>
            {
                cm.AutoMap();
                cm.MapIdProperty(x => x.Id).SetIdGenerator(new StringObjectIdGenerator());
            });
            }
        }
    }