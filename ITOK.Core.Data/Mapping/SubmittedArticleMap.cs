﻿using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using ITOK.Core.Data.DTOs;

namespace ITOK.Core.Data.Mapping
{
    public partial class SubmittedArticleMap
    {
        public SubmittedArticleMap()
        {
            BsonClassMap.RegisterClassMap<SubmittedArticle>(cm =>
            {
                cm.AutoMap();
                cm.MapIdProperty(x => x.Id).SetIdGenerator(new StringObjectIdGenerator());

                cm.MapField(x => x.LiveFromDay);
                cm.MapField(x => x.LiveFromMonth);
                cm.MapField(x => x.LiveFromYear);
            });
        }
    }
}