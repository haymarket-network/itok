﻿using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;

namespace ITOK.Core.Data.Mapping
    {
   public  class CertificateRegistrationMap
        {
       public CertificateRegistrationMap()
            {
            BsonClassMap.RegisterClassMap<CertificateRegistration>(cm =>
            {
                cm.AutoMap();
                cm.MapIdProperty(x => x.Id).SetIdGenerator(new StringObjectIdGenerator());
            });
            }
        }
    }
