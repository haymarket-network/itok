﻿using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using ITOK.Core.Data.Model;

namespace ITOK.Core.Data.Mapping
    {
    internal class SiteSettingsMap
        {
        public SiteSettingsMap()
            {
            BsonClassMap.RegisterClassMap<SiteSettings>(cm =>
               {
                   cm.AutoMap();
                   cm.MapIdProperty(x => x.Id)
                     .SetIdGenerator(new StringObjectIdGenerator());
               });
            }
        }
    }