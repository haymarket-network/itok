﻿using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using ITOK.Core.Data.DTOs;

namespace ITOK.Core.Data.Mapping
{
    public class FavoriteStatMap
    {
        public FavoriteStatMap()
        {
            BsonClassMap.RegisterClassMap<FavoriteStat>(cm =>
            {
                cm.AutoMap();
                cm.MapIdProperty(x => x.Id).SetIdGenerator(new StringObjectIdGenerator());
            });
        }
    }
}