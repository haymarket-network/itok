﻿using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using ITOK.Core.Data.DTOs;

namespace ITOK.Core.Data.Mapping
    {
    internal class MediaMap
        {
        public MediaMap()
            {
            BsonClassMap.RegisterClassMap<Media>(cm =>
            {
                cm.AutoMap();
                cm.MapIdProperty(x => x.Id).SetIdGenerator(new StringObjectIdGenerator());
            });
            }
        }
    }