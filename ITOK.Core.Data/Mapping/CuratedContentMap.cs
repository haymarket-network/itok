﻿using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using ITOK.Core.Data.DTOs;

namespace ITOK.Core.Data.Mapping
{
    public partial class CuratedContentMap
    {
        public CuratedContentMap()
        {
            BsonClassMap.RegisterClassMap<CuratedContent>(cm =>
            {
                cm.AutoMap();
                cm.MapIdProperty(x => x.Id).SetIdGenerator(new StringObjectIdGenerator());

            });
        }
    }
}