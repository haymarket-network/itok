﻿using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using ITOK.Core.Data.DTOs;

namespace ITOK.Core.Data.Mapping
{
    public class ReadingStatMap
    {
        public ReadingStatMap()
        {
            BsonClassMap.RegisterClassMap<ReadingStat>(cm =>
            {
                cm.AutoMap();
                cm.MapIdProperty(x => x.Id).SetIdGenerator(new StringObjectIdGenerator());
            });
        }
    }
}