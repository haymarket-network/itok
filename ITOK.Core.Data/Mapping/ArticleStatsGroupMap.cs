﻿using MongoDB.Bson.Serialization;
using ITOK.Core.Data.DTOs;

namespace ITOK.Core.Data.Mapping
    {
    public class ArticleStatsGroupMap
        {
        public ArticleStatsGroupMap()
            {
            BsonClassMap.RegisterClassMap<ArticleStatsGroup>(cm =>
            {
                cm.AutoMap();
                cm.MapIdProperty(x => x.Id);
                cm.MapField(x => x.Value);
            });
            }
        }
    }