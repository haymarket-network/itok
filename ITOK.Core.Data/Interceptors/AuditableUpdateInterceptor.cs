﻿using System;
using ITOK.Core.Common.Interfaces;

namespace ITOK.Core.Data.Interceptors
    {
    public class AuditableUpdateInterceptor : IInterceptor
        {
        private readonly IMembershipProvider _membershipProvider;

        public AuditableUpdateInterceptor(IMembershipProvider membershipProvider)
            {
            _membershipProvider = membershipProvider;
            }

        public bool IsInterestedIn(object entity)
            {
            return entity is IAuditable;
            }

        public void Intercept(object entity)
            {
            (entity as IAuditable).UpdatedById = _membershipProvider.ActiveUserId;
            (entity as IAuditable).UpdatedOn = DateTime.UtcNow;
            }
        }
    }