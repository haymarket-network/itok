﻿using System;
using ITOK.Core.Common.Interfaces;

namespace ITOK.Core.Data.Interceptors
    {
    public class AuditableInsertInterceptor : IInterceptor
        {
        private readonly IMembershipProvider _membershipProvider;

        public AuditableInsertInterceptor(IMembershipProvider membershipProvider)
            {
            _membershipProvider = membershipProvider;
            }

        public bool IsInterestedIn(object entity)
            {
            return entity is IAuditable;
            }

        public void Intercept(object entity)
            {
            var auditableEntity = entity as IAuditable;
            auditableEntity.CreatedOn = auditableEntity.UpdatedOn = DateTime.UtcNow;
            auditableEntity.CreatedById = auditableEntity.UpdatedById = _membershipProvider.ActiveUserId;
            }
        }
    }