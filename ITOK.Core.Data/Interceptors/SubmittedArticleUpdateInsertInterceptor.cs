﻿using System.Linq;
using ITOK.Core.Common.Interfaces;
using ITOK.Core.Data.DTOs;

namespace ITOK.Core.Data.Interceptors
    {
    public class SubmittedArticleUpdateInsertInterceptor : IInterceptor
        {
        private readonly MongoRepository _mongoRepo = new MongoRepository();

        public bool IsInterestedIn(object entity)
            {
            return entity is SubmittedArticle;
            }

        public void Intercept(object entity)
            {
            var article = entity as Article;
            bool updatePlainText = false;
            if (!string.IsNullOrEmpty(article.Id))
                {
                var existing = _mongoRepo.AsQueryable<SubmittedArticle>()
                    .SingleOrDefault(x => x.Id == article.Id);
                if (existing == null) updatePlainText = true; // Something has gone quite wrong but update plaintext any way TODO: Log!
                if (existing.Body != article.Body) updatePlainText = true;
                }
            else updatePlainText = true;
            //Todo:readd
            //if (updatePlainText && !string.IsNullOrEmpty(article.Body))
            //    {
            //    article.PlainTextBody = HtmlToText.ConvertHtml(article.Body);
            //    }
            }
        }
    }