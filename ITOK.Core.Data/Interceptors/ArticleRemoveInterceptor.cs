﻿using System.Collections.Generic;
using System.Linq;
using ITOK.Core.Common.Interfaces;
using ITOK.Core.Data.DTOs;

namespace ITOK.Core.Data.Interceptors
    {
    public class ArticleRemoveInterceptor : IInterceptor
        {
        private readonly MongoRepository _mongoRepo = new MongoRepository();

        public bool IsInterestedIn(object entity)
            {
            return entity is Article;
            }

        public void Intercept(object entity)
            {
            Article article = entity as Article;
            // Check referential integrity with the following entities:

            // - Article
            IList<Article> relatedArticles = _mongoRepo.AsQueryable<Article>()
                .Where(x => x.RelatedArticleIds.Contains(article.Id))
                .ToList();
            foreach (Article relatedArticle in relatedArticles)
                {
                relatedArticle.RelatedArticleIds.Remove(article.Id);
                _mongoRepo.Update<Article>(relatedArticle);
                }

           
            }
        }
    }