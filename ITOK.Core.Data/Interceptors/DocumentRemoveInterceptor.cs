﻿using System.Collections.Generic;
using System.Linq;
using ITOK.Core.Common.Interfaces;

namespace ITOK.Core.Data.Interceptors
    {
    public class DocumentRemoveInterceptor : IInterceptor
        {
        private readonly MongoRepository _mongoRepo = new MongoRepository();

        public bool IsInterestedIn(object entity)
            {
            return entity is Model.Document;
            }

        public void Intercept(object entity)
            {
            var document = entity as Model.Document;
            // Check referential integrity with the following entities:
            // - SingleDocumentContent & DocumentSetContent
            //IList<Model.Template> documentTemplates = _mongoRepo.AsQueryable<Model.Template>()
            //    .Where(x => x.ContentAreaDefinitions.Any(d => d.Type == ContentAreaType.DocumentSet || d.Type == ContentAreaType.SingleDocument))
            //    .ToList();
            //foreach (Model.Template template in documentTemplates)
            //    {
            //    // Get template pages
            //    IList<Data.DTOs.Page> documentPages = _mongoRepo.AsQueryable<Data.DTOs.Page>()
            //        .Where(x => x.TemplateId == template.Id)
            //        .ToList();
            //    foreach (Data.DTOs.Page docPage in documentPages)
            //        {
            //        bool pageAltered = false;
            //        // Run through page contents looking for document id
            //        foreach (Data.DTOs.IContent content in docPage.Content)
            //            {
            //            if (content is Data.DTOs.SingleDocumentContent)
            //                {
            //                var docContent = content as Data.DTOs.SingleDocumentContent;
            //                if (docContent.DocumentId == document.Id)
            //                    {
            //                    docContent.DocumentId = null;
            //                    pageAltered = true;
            //                    }
            //                }
            //            else if (content is Data.DTOs.DocumentSetContent)
            //                {
            //                var docContent = content as Data.DTOs.DocumentSetContent;
            //                if (docContent.DocumentIds.Contains(document.Id))
            //                    {
            //                    docContent.DocumentIds.Remove(document.Id);
            //                    pageAltered = true;
            //                    }
            //                }
            //            }
            //        if (pageAltered) _mongoRepo.Update<Data.DTOs.Page>(docPage);
            //        }
            //    }

            // - Article
            //IList<Data.DTOs.Article> relatedArticles = _mongoRepo.AsQueryable<Data.DTOs.Article>()
            //    .Where(x => x.RelatedDocumentIds.Contains(document.Id))
            //    .ToList();
            //foreach (Data.DTOs.Article article in relatedArticles)
            //    {
            //    article.RelatedDocumentIds.Remove(document.Id);
            //    _mongoRepo.Update<Data.DTOs.Article>(article);
            //    }
            }
        }
    }