﻿using System;
namespace ITOK.Core.Data
{
    public interface IMongoOutputCacheRepository
    {
        System.Linq.IQueryable<ITOK.Core.Data.DTOs.MongoOutputCacheItem> AsQueryable();
        MongoDB.Driver.IMongoCollection<T> GetCollection<T>(string collectionName = null) where T : class;
        void Insert(ITOK.Core.Data.DTOs.MongoOutputCacheItem cacheItem);
        void Remove(string cacheItemId);
        void Save(ITOK.Core.Data.DTOs.MongoOutputCacheItem cacheItem);
        bool Update(ITOK.Core.Data.DTOs.MongoOutputCacheItem entity);
    }
}
