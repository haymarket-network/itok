﻿namespace ITOK.Core.Data.VersionInterceptors
{
    public interface IVersionInterceptor
    {
        void Intercept(object entity, string versionComment);
    }
}