﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using ITOK.Core.Common.Constants;
using ITOK.Core.Common.Extensions.Strings;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using ITOK.Core.Data.Model.HS;
using ITOK.Core.Services.Core;
using ITOK.Core.Services.Core.Wrapped;

namespace ITOK.Core.Services.CMS
{
    public class SubmittedArticleService : Core.SubmittedArticleService
    {

        private readonly ISubmittedArticleServiceWrapped _articleServiceWrapped;

        public SubmittedArticleService(IMongoRepository1 mongoRepository)
            : base(mongoRepository)
        {
            _versionService = new VersionService<Data.DTOs.SubmittedArticle>(mongoRepository);
            _articleServiceWrapped = new SubmittedArticleServiceWrapped(mongoRepository);
        }

        /// <summary>
        /// Hardcoding this for now. If this model of featured articles is used more
        /// then we should consider creating configuration properties for each feature
        /// area and getting the figure from there.
        /// </summary>
        private const int MaxFeaturedArticles = 7;//JonFindThis

        private const int MaxUniqueUrlSlugChecks = 100; // Prevent infinate loops
        private VersionService<Data.DTOs.SubmittedArticle> _versionService;

        public void Save(SubmittedArticle viewModel, string versionComment = null)
        {
        var article = _articleServiceWrapped.deHydrate(viewModel);

            //don't need url slug for submitted content
            // Update the url slug with check for duplicates
            //if (article.UrlSlug == null)
            //{
            //    article.UrlSlug = article.Id.GenerateSlug();
            //    var checkCount = 1;
            //    var urlSlugIsUnique = false;
            //    while (!urlSlugIsUnique && checkCount < MaxUniqueUrlSlugChecks)
            //    {
            //        if (MongoRepository.AsQueryable<Data.DTOs.SubmittedArticle>().Any(x => x.UrlSlug == article.UrlSlug && x.Id != article.Id))
            //            article.UrlSlug = article.SEOTitle.GenerateSlug() + checkCount;
            //        else urlSlugIsUnique = true;
            //        checkCount++;
            //    }
            //    if (!urlSlugIsUnique) article.UrlSlug = article.SEOTitle.GenerateSlug() + article.Id;

            //    article.UrlSlug = article.UrlSlug.ToLower();

            //    viewModel.UrlSlug = article.UrlSlug;
            //}
            //else
            //{
            //    article.UrlSlug = article.UrlSlug.ToLower();
            //}

           // article.LiveFrom = new DateTime(article.LiveFrom.Ticks).ToUniversalTime();

            if (string.IsNullOrEmpty(article.Id)) MongoRepository.Insert(article);
            else MongoRepository.UpdateWithComment(article, versionComment);

            viewModel.Id = article.Id;
        }

        public void AddRelatedMedia(string articleId, string mediaId)
        {
            var article = MongoRepository.AsQueryable<Data.DTOs.SubmittedArticle>()
                .SingleOrDefault(x => x.Id == articleId);
            if (!article.RelatedMediaIds.Contains(mediaId))
            {
                article.RelatedMediaIds.Add(mediaId);
            }
            MongoRepository.Update(article);
        }

        public void DeleteById(string id, bool soft = true)
        {
            var article = MongoRepository.AsQueryable<Data.DTOs.SubmittedArticle>()
                    .SingleOrDefault(x => x.Id == id);

            if (article == null) throw new Exception(string.Format("Couldn't find a submission with id {0} to delete.", id));

            if (soft)
            {
                article.Status = SubmissionStatus.Deleted;
                MongoRepository.Update(article);
            }
            else MongoRepository.Remove(article, article.Id);
        }

        #region Version

        public IList<VersionWrapper> GetVersionHistory(SubmittedArticle article, VersionHydrationSettings versionHydrationSettings)
        {
            return GetVersionHistory(article.Id, versionHydrationSettings);
        }

        public IList<VersionWrapper> GetVersionHistory(string id, VersionHydrationSettings versionHydrationSettings)
        {
            return _versionService.GetVersions(id, versionHydrationSettings);
        }

        public VersionWrapper GetVersion(string versionId, VersionHydrationSettings versionHydrationSettings)
        {
            return _versionService.GetVersion(versionId, versionHydrationSettings);
        }

        public Data.Model.SubmittedArticle GetEntityVersion(string versionId)
        {
            var articleDto = _versionService.GetVersionEntity(versionId);
            return _articleServiceWrapped.hydrate(articleDto, new SubmittedArticleHydrationSettings
            {
                PrimaryMedia = new Data.Model.HS.SubmittedMediaHydrationSettings(),
                RelatedMedia = new Data.Model.HS.SubmittedMediaHydrationSettings(),
                RelatedArticles = new Data.Model.HS.SubmittedArticleHydrationSettings(),
                CreatedBy = new Data.Model.HS.UserHydrationSettings(),
                UpdatedBy = new Data.Model.HS.UserHydrationSettings()
            });
        }

        public void RevertToVersion(string versionId)
        {
            _versionService.Revert(versionId);
        }

        #endregion Version
    }
}