﻿using MongoDB.Driver;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;

namespace ITOK.Core.Services.CMS
{
    public class UserGroupMemberService : Core.UserGroupMemberService
    {
        public UserGroupMemberService(IMongoRepository1 mongoRepository)
            : base(mongoRepository)
        {
        }

        public void DeleteById(string id)
        {
            var filter = Builders<UserGroupMember>.Filter.Eq("Id", id);
            MongoRepository.GetCollection<UserGroupMember>().DeleteOne(filter);
        }
    }
}