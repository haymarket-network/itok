﻿using MongoDB.Driver;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;

namespace ITOK.Core.Services.CMS
{
    public class ArticleCommentService : Core.ArticleCommentService
    {
        public ArticleCommentService(IMongoRepository1 mongoRepository)
            : base(mongoRepository)
        {
        }

        public void DeleteById(string id)
        {
            var filter = Builders<ArticleComment>.Filter.Eq("Id", id);
            MongoRepository.GetCollection<ArticleComment>().DeleteOne(filter);
        }
    }
}