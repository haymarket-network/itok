﻿using MongoDB.Driver;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;

namespace ITOK.Core.Services.CMS
{
    public class UserGroupService : Core.UserGroupService
    {
        public UserGroupService(IMongoRepository1 mongoRepository)
            : base(mongoRepository)
        {
        }

        public void DeleteById(string id)
        {
            var filter = Builders<ITOK.Core.Data.DTOs.UserGroup>.Filter.Eq("Id", id);
            MongoRepository.GetCollection<ITOK.Core.Data.DTOs.UserGroup>().DeleteOne(filter);
            var filterMembers = Builders<ITOK.Core.Data.DTOs.UserGroupMember>.Filter.Eq("UserGroupID", id);
            MongoRepository.GetCollection<ITOK.Core.Data.DTOs.UserGroupMember>().DeleteMany(filterMembers);


        }
    }
}