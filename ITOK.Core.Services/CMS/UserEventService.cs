﻿using ITOK.Core.Data.Model;
using ITOK.Core.Services;
using ITOK.Core.Services.CMS;
using System.Collections.Generic;
using System.Linq;
using ITOK.Core.Services.Core;
using ITagsService = ITOK.Core.Services.Core.ITagsService;


namespace ITOK.Core.Services.CMS
{
    public class UserEventService : BaseService, IUserEventService
    {
        private readonly IUserService _userService;
        private readonly ITagsService _tagsService;
        private readonly IQuestionService _questionService;

        public UserEventService(IUserService userService, ITagsService tagsService, IQuestionService questionService)
        {
            _userService = userService;
            _tagsService = tagsService;
            _questionService = questionService;
        }

        public IList<UserEvent> GetUserEvents(User user)
        {
            FillUserEvents(user);
            return user.UserEvents;
        }

        public UserEvent GetUserEventWithQuestions(string userId, string yearId, string areaId, string eventId)
        {
            var user = _userService.FindById(userId);
            var userEvent = user.UserEvents
                .FirstOrDefault(x => x.AreaId == areaId && x.EventId == eventId && x.YearId == yearId);

            if (userEvent == null && userEvent.QuestionList == null)
            {
                return null;
            }

            FillUserEvent(userEvent, true);
            return userEvent;
        }

        private void FillUserEvents(User user)
        {
            if (user.UserEvents != null)
            {
                foreach (var iteUserEvent in user.UserEvents)
                {
                    FillUserEvent(iteUserEvent, false);

                    var answeredCount = iteUserEvent.QuestionList.Count(x => x.isAnswered);
                    var questionCount = iteUserEvent.QuestionList.Count;

                    if (answeredCount == 0)
                    {
                        iteUserEvent.Status = EventAnswerStatus.Unstarted;
                    }
                    else if (answeredCount == questionCount)
                    {
                        iteUserEvent.Status = EventAnswerStatus.Complete;
                    }
                    else if (answeredCount > 0)
                    {
                        iteUserEvent.Status = EventAnswerStatus.InProgress;
                    }
                }
            }
       }

        private void FillUserEvent(UserEvent userEvent, bool includeQuestions)
        {
            var areaTitle = _tagsService.FindById(userEvent.AreaId);
            var eventTitle = _tagsService.FindById(userEvent.EventId);
            var yearTitle = _tagsService.FindById(userEvent.YearId);

            if (areaTitle != null)
            {
                userEvent.AreaTitle = areaTitle.DisplayText;
            }
            if (eventTitle != null)
            {
                userEvent.EventTitle = eventTitle.DisplayText;
            }
            if (yearTitle != null)
            {
                userEvent.YearTitle = yearTitle.DisplayText;
            }
 


            if (includeQuestions)
            {
                FillQuestions(userEvent.QuestionList);
            }
        }

        private void FillQuestions(List<UserQuestion> questions)
        {
            foreach (var item in questions)
            {
                item.Question = _questionService.FindById(item.QuestionId.Trim());
            }
        }

    }

    public interface IUserEventService
    {
        IList<UserEvent> GetUserEvents(User user);
        UserEvent GetUserEventWithQuestions(string userId, string yearId, string areaId, string eventId);
    }
}
