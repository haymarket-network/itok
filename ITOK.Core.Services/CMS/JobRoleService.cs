﻿using MongoDB.Driver;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;

namespace ITOK.Core.Services.CMS
{
    public class JobRoleService : Core.JobRoleService
    {
        public JobRoleService(IMongoRepository1 mongoRepository)
            : base(mongoRepository)
        {
        }

        public void DeleteById(string id)
        {
            var filter = Builders<JobRole>.Filter.Eq("Id", id);
            MongoRepository.GetCollection<JobRole>().DeleteOne(filter);
        }
    }
}