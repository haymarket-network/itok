﻿using System;
using System.Collections.Generic;
using System.Linq;
using ITOK.Core.Common.Constants;
using ITOK.Core.Common.Extensions.Strings;
using ITOK.Core.Data;
using ITOK.Core.Data.Model.HS;
using ITOK.Core.Services.Core;

namespace ITOK.Core.Services.CMS
{
    public class MediaService : Core.MediaService
    {
        public MediaService(IMongoRepository1 mongoRepository)
            : base(mongoRepository)
        {
            _versionService = new VersionService<Data.DTOs.Media>(mongoRepository);
            _articleService = new ArticleService(mongoRepository);
        }

        private const int MaxUniqueUrlSlugChecks = 100;
        private const int MaxFeaturedMedia = 3;
        private VersionService<Data.DTOs.Media> _versionService;

        private ArticleService _articleService;

        /// <summary>
        /// Model.Media contains bi-directional relationships which need to be persisted
        /// (i.e. a discipline has primary and related media and those media items are related to that discipline item)
        /// Relationship enforcement must occur after the media item is saved because, in the case of an insert, it won't have an id
        /// which is required to update the entities in which the relationships are defined.
        /// The enforcement process for each type of relationship must do the following:
        /// - Remove relationships that no longer exist
        /// - Add new relationships that have been created
        ///
        /// TBH this smells quite bad.
        /// </summary>
        /// <param name="viewModel"></param>
        public string Save(ITOK.Core.Data.Model.Media viewModel, string versionComment = null)
        {
            var media = deHydrate(viewModel);

            //Check that a liveFrom date has been supplied
            if (media.LiveFrom.Year == 0001)
                media.LiveFrom = DateTime.Today;

            //Update the url slug with check for duplicates
            media.UrlSlug = media.Title.GenerateSlug();
            var checkCount = 1;
            var urlSlugIsUnique = false;
            while (!urlSlugIsUnique && checkCount < MaxUniqueUrlSlugChecks)
            {
                if (MongoRepository.AsQueryable<Data.DTOs.Article>().Any(x => x.UrlSlug == media.UrlSlug && x.Id != media.Id))
                    media.UrlSlug = media.Title.GenerateSlug() + checkCount;
                else urlSlugIsUnique = true;
                checkCount++;
            }

            // Save the media item so we know it has an id
            if (string.IsNullOrEmpty(media.Id)) MongoRepository.Insert(media);
            else MongoRepository.UpdateWithComment(media, versionComment);

            viewModel.Id = media.Id;

            // Enfoce it's relationships
            enforceArticleRelationships(viewModel);

            return media.Id;
        }
    
        public void DeleteById(string id, bool soft = true)
        {
            // We need to remove the article from any entities that reference it so:
            // - Hydrate the media item
            var media = FindByIds(new MediaHydrationSettings
            {
                RelatedArticles = new ArticleHydrationSettings(),
            }, id).SingleOrDefault();

            if (media == null) throw new Exception(string.Format("Couldn't find a media item with id {0} to delete.", id));

            // - Clear the view models relationships
            if (media.RelatedArticles != null) media.RelatedArticles.Clear();

            if (soft)
            {
                // - Change the status to deleted
                media.Status = PublicationStatus.Deleted;
                // - Update the media item
                Save(media);
            }
            else
            {
                // - Enforce relationships
                enforceArticleRelationships(media);

                // - Delete from db
                var mediaDto = MongoRepository.AsQueryable<Data.DTOs.Media>()
                    .SingleOrDefault(x => x.Id == media.Id);
                MongoRepository.Remove(mediaDto, mediaDto.Id);
            }
        }

        #region Version History

        public IList<ITOK.Core.Data.Model.VersionWrapper> GetVersionHistory(Data.Model.Media media, VersionHydrationSettings versionHydrationSettings)
        {
            return GetVersionHistory(media.Id, versionHydrationSettings);
        }

        public IList<ITOK.Core.Data.Model.VersionWrapper> GetVersionHistory(string id, VersionHydrationSettings versionHydrationSettings)
        {
            return _versionService.GetVersions(id, versionHydrationSettings);
        }

        public ITOK.Core.Data.Model.VersionWrapper GetVersion(string versionId, VersionHydrationSettings versionHydrationSettings)
        {
            return _versionService.GetVersion(versionId, versionHydrationSettings);
        }

        public Data.Model.Media GetEntityVersion(string versionId)
        {
            var mediaDto = _versionService.GetVersionEntity(versionId);
            return hydrate(mediaDto, new MediaHydrationSettings
            {
                RelatedArticles = new ArticleHydrationSettings
                {
                    PrimaryMedia = new MediaHydrationSettings(),
                    RelatedMedia = new MediaHydrationSettings()
                },

                CreatedBy = new UserHydrationSettings(),
                UpdatedBy = new UserHydrationSettings()
            });
        }

        public void RevertToVersion(string versionId)
        {
            _versionService.Revert(versionId);
        }

        #endregion Version History

        private void enforceArticleRelationships(Data.Model.Media viewModel)
        {
            //// Related Articles
            //var existingArticles = _articleService.FindWithRelatedMedia(new ArticleHydrationSettings
            //{
            //    PrimaryMedia = new MediaHydrationSettings(),
            //    RelatedMedia = new MediaHydrationSettings(),
            //    RelatedArticles = new ArticleHydrationSettings(),
            //}, viewModel.Id);

            //var articlesToRemove = existingArticles.Where(x => !viewModel.RelatedArticles.Any(a => a.Id == x.Id)).ToList();
            //var articlesToAdd = viewModel.RelatedArticles.Where(x => !existingArticles.Any(a => a.Id == x.Id)).ToList();
            //foreach (var article in articlesToRemove)
            //{
            //    var relatedMedia = article.RelatedMedia.SingleOrDefault(x => x.Id == viewModel.Id);
            //    if (relatedMedia != null) article.RelatedMedia.Remove(relatedMedia);
            //    _articleService.Save(article);
            //}
            //foreach (var article in articlesToAdd)
            //{
            //    var articleDB = _articleService.FindById(article.Id.ToString(), new ArticleHydrationSettings
            //    {
            //        PrimaryMedia = new MediaHydrationSettings(),
            //        RelatedMedia = new MediaHydrationSettings(),
            //        RelatedArticles = new ArticleHydrationSettings(),
            //    });

            //    if (articleDB.RelatedMedia != null && articleDB.RelatedMedia.All(x => x.Id != viewModel.Id))
            //    {
            //        articleDB.RelatedMedia.Add(viewModel);
            //        _articleService.Save(articleDB);
            //    }
            //}
        }
    }
}