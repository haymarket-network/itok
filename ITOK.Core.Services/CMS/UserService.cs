﻿using MongoDB.Driver;
using Omu.Encrypto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;

namespace ITOK.Core.Services.CMS
{
    public class UserService : Core.UserService, IUserService
    {
        public UserService(IMongoRepository1 mongoRepository)
            : base(mongoRepository)
        {
        }

        private const int PasswordResetTicketTimeout = 30;

        public void Save(Data.Model.User user)
        {
            if (string.IsNullOrEmpty(user.Password) && !string.IsNullOrEmpty(user.Id))
            {
                // Get user from database
                var existingUser = MongoRepository.AsQueryable<User>()
                    .FirstOrDefault(x => x.Id == user.Id);
                user.Password = existingUser.Password;
            }

            if (string.IsNullOrWhiteSpace(user.Id)) MongoRepository.Insert(user);
            else MongoRepository.Update(user);
        }

        public void DeleteById(string id)
        {
            var filter = Builders<User>.Filter.Eq("Id", id);
            MongoRepository.GetCollection<User>().DeleteOne(filter);
        }

        public User FindByEmail(string emailAddress)
        {
            IList<User> users = MongoRepository.AsQueryable<Data.Model.User>()
                .Where(x => x.Email == emailAddress.ToLower())
                .ToList();
            if (users.Count == 0) return null;
            if (users.Count > 1) throw new Exception(string.Format("Found more than one user with the email address: {0}. This situation should not occur!", emailAddress));
            var user = users[0];
            user.Password = string.Empty; // Password doesnt leave the service layer!
            return user;
        }

        public User FindWithCredentials(string username, string password, params Permission[] permissions)
        {
            username = username.ToLower();
            IEnumerable<Permission> permissionsList = permissions;
            var user = MongoRepository.AsQueryable<Data.Model.User>()
                .FirstOrDefault(x => x.Email == username);

            if (user == null) return null;

            // Check password
            var hasher = new Hasher();
            hasher.SaltSize = 10;

            if (string.IsNullOrEmpty(user.Password)) return null;
            if (!hasher.CompareStringToHash(password, user.Password)) return null;

            // Check permissions
            if (!permissions.All(x => user.Permissions.Contains(x))) return null;

            return user;
        }
        public User FindByEmailWithCredentials(string email, params Permission[] permissions)
        {
            IEnumerable<Permission> permissionsList = permissions;
            var user = MongoRepository.AsQueryable<Data.Model.User>()
                .SingleOrDefault(x => x.Email == email);

            if (user == null) return null;

            // Check password
            var hasher = new Hasher();
            hasher.SaltSize = 10;
                       

            // Check permissions
            if (!permissions.All(x => user.Permissions.Contains(x))) return null;

            return user;
        }

        public IList<Data.Model.User> GetPagedOrderedBySurname(int skip, int take, bool ascending = true)
        {
            var query = MongoRepository.AsQueryable<User>();
            query = ascending ? query.OrderBy(x => x.Surname) : query.OrderByDescending(x => x.Surname);
            query = query.Skip(skip);

            return query
                .ToList().Take(take).ToList();
        }

        public int GetCount()
        {
            return MongoRepository.AsQueryable<User>().Count();
        }

        public List<User> GetAll()
        {
            return MongoRepository.AsQueryable<User>().ToList();
        }

        public IList<Data.Model.User> Search(string searchText, bool searchForename, bool searchSurname, bool searchEmail, int resultLimit)
        {
            if (string.IsNullOrWhiteSpace(searchText) || (!searchForename && !searchSurname && !searchEmail)) return new List<User>();

            var foundUsers = new List<Data.Model.User>();
            var searchTerms = searchText.ToLower().Split(' ');
            var sb = new StringBuilder("^");
            foreach (var term in searchTerms)
            {
                if (string.IsNullOrWhiteSpace(term)) continue;
                sb.Append(string.Format("(?=.*?({0}))", term));
            }
            sb.Append(".*$");
            var re = new Regex(sb.ToString(), RegexOptions.IgnoreCase | RegexOptions.Multiline);
            var query = MongoRepository.AsQueryable<User>();

            if (searchForename)
            {
                var nameQuery = query
                    .Where(x => re.IsMatch(x.Forename));
                foundUsers.AddRange(nameQuery.ToList());
            }

            if (searchSurname)
            {
                var nameQuery = query
                    .Where(x => re.IsMatch(x.Surname));
                foundUsers.AddRange(nameQuery.ToList());
            }

            if (searchEmail)
            {
                var nameQuery = query
                    .Where(x => re.IsMatch(x.Email));
                foundUsers.AddRange(nameQuery.ToList());
            }

            // Remove duplicates and limit result count
            foundUsers = foundUsers
                .Distinct(new Data.Model.EqualityComparers.UserEqualityComparer())
                .Take(resultLimit)
                .ToList();

            return foundUsers;
        }

        public bool UsernameExists(string username, string excludeUserId = null)
        {
            var query = MongoRepository.AsQueryable<Data.Model.User>()
                .Where(x => x.Email == username.ToLower());
            if (excludeUserId != null) query = query
                 .Where(x => x.Id != excludeUserId);
            return query.Any();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns>Returns the key that will be used to retrieve the ticket when the user follows the link from the notification email</returns>
        public string CreatePasswordResetTicket(string emailAddress)
        {
            var user = FindByEmail(emailAddress);

            if (user == null) throw new ArgumentException(string.Format("Couldnt find a user with the email address: {0}. Please use the .UsernameExists(string username) first to check that the user exists"));

            // Remove any existing tickets for this user
            IList<Data.DTOs.PasswordResetTicket> existingTickets = MongoRepository.AsQueryable<Data.DTOs.PasswordResetTicket>()
                .Where(x => x.UserId == user.Id && x.CompletedOn == null)
                .ToArray();

            foreach (var existingTicket in existingTickets)
            {
                MongoRepository.Remove(existingTicket, existingTicket.Id);
            }
            var hasher = new Hasher();
            hasher.SaltSize = 10;
            var passResetTicket = new Data.DTOs.PasswordResetTicket
            {
                Key = Regex.Replace(hasher.Encrypt(Guid.NewGuid().ToString() + user.Id).ToLower(), @"[^a-z0-9\s-]", ""), // Generate as random and unpredictable key as possible. TODO: Could probably do with being more random... (randomisation service?)
                UserId = user.Id,
                CreatedOn = DateTime.Now
            };

            MongoRepository.Insert(passResetTicket);

            return passResetTicket.Key;
        }

        public bool PasswordResetTicketIsValid(string key)
        {
            return MongoRepository.AsQueryable<Data.DTOs.PasswordResetTicket>()
                .Any(x =>
                    x.Key == key.ToLower() &&
                    x.CompletedOn == null);
        }

        public bool PasswordResetTicketIsActive(string key)
        {
            return MongoRepository.AsQueryable<Data.DTOs.PasswordResetTicket>()
                .Any(x =>
                    x.Key == key.ToLower() &&
                    x.CreatedOn > DateTime.Now.AddMinutes(-PasswordResetTicketTimeout) &&
                    x.CompletedOn == null);
        }

        public void ChangeUserPassword(string passwordResetTicketKey, string newPassword)
        {
            // Get the ticket
            var ticket = MongoRepository.AsQueryable<Data.DTOs.PasswordResetTicket>()
                .SingleOrDefault(x => x.Key == passwordResetTicketKey.ToLower() && x.CreatedOn > DateTime.Now.AddMinutes(-45));
            if (ticket == null) throw new ArgumentException(string.Format("Couldnt find a password reset ticket with the key {0}", passwordResetTicketKey.ToLower()));

            // Get the user
            var user = MongoRepository.AsQueryable<Data.Model.User>()
                .SingleOrDefault(x => x.Id == ticket.UserId);
            if (user == null) throw new Exception(string.Format("Couldnt find a user for the password reset ticket with key {0}", passwordResetTicketKey.ToLower()));

            var hasher = new Hasher();
            hasher.SaltSize = 10;
            user.Password = hasher.Encrypt(newPassword);

            // Update the user
            MongoRepository.Update(user);

            // Mark the ticket as complete (update)
            ticket.CompletedOn = DateTime.Now;
            MongoRepository.Update(ticket);
        }
    }

    public interface IUserService
    {
        void Save(Data.Model.User user);
        void DeleteById(string id);
        User FindByEmail(string emailAddress);
        User FindWithCredentials(string username, string password, params Permission[] permissions);
        User FindByEmailWithCredentials(string email, params Permission[] permissions);
        IList<Data.Model.User> GetPagedOrderedBySurname(int skip, int take, bool ascending = true);
        int GetCount();
        List<User> GetAll();
        IList<Data.Model.User> Search(string searchText, bool searchForename, bool searchSurname, bool searchEmail, int resultLimit);
        bool UsernameExists(string username, string excludeUserId = null);

        /// <summary>
        ///
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns>Returns the key that will be used to retrieve the ticket when the user follows the link from the notification email</returns>
        string CreatePasswordResetTicket(string emailAddress);

        bool PasswordResetTicketIsValid(string key);
        bool PasswordResetTicketIsActive(string key);
        void ChangeUserPassword(string passwordResetTicketKey, string newPassword);
        User FindById(string id);
        List<User> FindByIds(params string[] IDs);
    }
}