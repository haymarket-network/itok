﻿using MongoDB.Driver;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;

namespace ITOK.Core.Services.CMS
{
    public class RetailerService : Core.RetailerService
    {
        public RetailerService(IMongoRepository1 mongoRepository)
            : base(mongoRepository)
        {
        }

        public void DeleteById(string id)
        {
            var filter = Builders<Retailer>.Filter.Eq("Id", id);
            MongoRepository.GetCollection<Retailer>().DeleteOne(filter);
        }
    }
}