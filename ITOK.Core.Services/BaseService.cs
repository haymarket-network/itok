﻿using ITOK.Core.Data;

namespace ITOK.Core.Services
{
    public abstract class BaseService
    {
        protected Data.IMongoRepository1 _mongoRepository;

        protected BaseService()
        {
            //this is for applying dependency injection on derived class
        }

        protected BaseService(IMongoRepository1 mongoRepository)
        {
            _mongoRepository = mongoRepository;
        }

        protected Data.IMongoRepository1 MongoRepository { get { return _mongoRepository; } }
    }
}