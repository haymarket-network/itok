﻿using AutoMapper;
using System;
using System.Linq;

namespace ITOK.Core.Services.Helpers
{
    public class EntityMapper
    {
        #region By Owain Wrags

        public static T Map<T>(params object[] sources) where T : class
        {
            if (!sources.Any())
            {
                return default(T);
            }

            var initialSource = sources[0];

            var mappingResult = Map<T>(initialSource);

            // Now map the remaining source objects
            if (sources.Count() > 1)
            {
                Map(mappingResult, sources.Skip(1).ToArray());
            }

            return mappingResult;
        }

        private static void Map(object destination, params object[] sources)
        {
            if (!sources.Any())
            {
                return;
            }

            var destinationType = destination.GetType();

            foreach (var source in sources)
            {
                if (source == null) continue;
                var sourceType = source.GetType();
                Mapper.Map(source, destination, sourceType, destinationType);
            }
        }

        private static T Map<T>(object source) where T : class
        {
            var destinationType = typeof(T);
            var sourceType = source.GetType();
            var mappingResult = Mapper.Map(source, sourceType, destinationType);
            return mappingResult as T;
        }

        #endregion By Owain Wrags

        public static void InitialiseMappings()
        {
            ArticleMappings();
            ArticleFilterMappings();
            AnswerFlagMappings();
            TagMappings();
            QuestionMappings();
            AnswerMappings();
            QuestionTemplateMappings();
            AlertMappings();
            WeblinkMappings();
            TagTranslationMappings();
            AreasOfInterestMappings();
            CuratedContentMappings();
            UserMappings();
            MediaMappings();

            RetailerMappings();

            JobRoleMappings();

            GotItMappings();

            AuthorMappings();

            FavoriteMappings();
            UserGroupMappings();
            UserGroupMemberMappings();

            ArticleCommentMappings();
            QuestionCommentMappings();

            ReadingListMappings();

            SubmittedArticleMappings();
            SubmittedMediaMappings();
        }



        private static void AuthorMappings()
        {
            Mapper.CreateMap<Data.Model.Author, Data.Model.Author>()
                 .ForMember(x => x.Id, opt => opt.Ignore());

            Mapper.CreateMap<Data.DTOs.Author, Data.Model.Author>();
            Mapper.CreateMap<Data.Model.Author, Data.DTOs.Author>();
            Mapper.CreateMap<ITOK.Core.Data.Model.Author, string>().ConvertUsing(x => (x == null) ? null : x.Id);
        }

        private static void TagMappings()
        {
            Mapper.CreateMap<Data.Model.Tags, Data.Model.Tags>()
                 .ForMember(x => x.Id, opt => opt.Ignore());

            Mapper.CreateMap<Data.DTOs.Tags, Data.Model.Tags>();
            Mapper.CreateMap<Data.Model.Tags, Data.DTOs.Tags>();
            Mapper.CreateMap<ITOK.Core.Data.Model.Tags, string>().ConvertUsing(x => (x == null) ? null : x.Id);
        }

        private static void QuestionMappings()
        {
            Mapper.CreateMap<Data.Model.Question, Data.Model.Question>()
                 .ForMember(x => x.Id, opt => opt.Ignore());

            Mapper.CreateMap<Data.DTOs.Question, Data.Model.Question>();
            Mapper.CreateMap<Data.Model.Question, Data.DTOs.Question>();
            Mapper.CreateMap<ITOK.Core.Data.Model.Question, string>().ConvertUsing(x => (x == null) ? null : x.Id);
        }

        private static void AnswerMappings()
        {
            Mapper.CreateMap<Data.Model.Answer, Data.Model.Answer>()
                .ForMember(x => x.Id, opt => opt.Ignore());

            Mapper.CreateMap<Data.DTOs.Answer, Data.Model.Answer>();
            Mapper.CreateMap<Data.Model.Answer, Data.DTOs.Answer>();
            Mapper.CreateMap<ITOK.Core.Data.Model.Answer, string>().ConvertUsing(x => (x == null) ? null : x.Id);

            Mapper.CreateMap<Data.DTOs.AnswerTag, Data.Model.AnswerTag>();
            Mapper.CreateMap<Data.Model.AnswerTag, Data.DTOs.AnswerTag>();
        }

        private static void QuestionTemplateMappings()
        {
            //Mapper.CreateMap<Data.Model.QuestionTemplate, Data.Model.QuestionTemplate>().ForMember(x => x.Id, opt => opt.Ignore());

            Mapper.CreateMap<Data.DTOs.QuestionTemplate, Data.Model.QuestionTemplate>();
            Mapper.CreateMap<Data.Model.QuestionTemplate, Data.DTOs.QuestionTemplate>();
            //Mapper.CreateMap<ITOK.Core.Data.Model.QuestionTemplate, string>().ConvertUsing(x => (x == null) ? null : x.Id);

            Mapper.CreateMap<Data.DTOs.QuestionRow, Data.Model.QuestionRow>();
            Mapper.CreateMap<Data.Model.QuestionRow, Data.DTOs.QuestionRow>();

            Mapper.CreateMap<Data.DTOs.AnswerRow, Data.Model.AnswerRow>();
            Mapper.CreateMap<Data.Model.AnswerRow, Data.DTOs.AnswerRow>();
        }

        private static void ArticleFilterMappings()
        {
            Mapper.CreateMap<Data.DTOs.Answer, Data.Model.AnswerFilterData>();
        }

        private static void WeblinkMappings()
        {
            Mapper.CreateMap<Data.Model.Weblink, Data.Model.Weblink>()
                 .ForMember(x => x.Id, opt => opt.Ignore());

            Mapper.CreateMap<Data.DTOs.Weblink, Data.Model.Weblink>();
            Mapper.CreateMap<Data.Model.Weblink, Data.DTOs.Weblink>();
            Mapper.CreateMap<ITOK.Core.Data.Model.Weblink, string>().ConvertUsing(x => (x == null) ? null : x.Id);
        }
        private static void RetailerMappings()
        {
            Mapper.CreateMap<Data.Model.Retailer, Data.Model.Retailer>()
                 .ForMember(x => x.Id, opt => opt.Ignore());

            Mapper.CreateMap<Data.DTOs.Retailer, Data.Model.Retailer>();
            Mapper.CreateMap<Data.Model.Retailer, Data.DTOs.Retailer>();
            Mapper.CreateMap<ITOK.Core.Data.Model.Retailer, string>().ConvertUsing(x => (x == null) ? null : x.Id);
        }
        private static void AlertMappings()
        {
            Mapper.CreateMap<Data.Model.Alert, Data.Model.Alert>()
                 .ForMember(x => x.Id, opt => opt.Ignore());

            Mapper.CreateMap<Data.DTOs.Alert, Data.Model.Alert>();
            Mapper.CreateMap<Data.Model.Alert, Data.DTOs.Alert>();
            Mapper.CreateMap<ITOK.Core.Data.Model.Alert, string>().ConvertUsing(x => (x == null) ? null : x.Id);
        }

        private static void TagTranslationMappings()
        {
            Mapper.CreateMap<Data.Model.TagTranslation, Data.Model.TagTranslation>()
                 .ForMember(x => x.Id, opt => opt.Ignore());

            Mapper.CreateMap<Data.DTOs.TagTranslation, Data.Model.TagTranslation>();
            Mapper.CreateMap<Data.Model.TagTranslation, Data.DTOs.TagTranslation>();
            Mapper.CreateMap<ITOK.Core.Data.Model.TagTranslation, string>().ConvertUsing(x => (x == null) ? null : x.Id);
        }

        private static void AreasOfInterestMappings()
        {
            Mapper.CreateMap<Data.Model.AreasOfInterest, Data.Model.AreasOfInterest>()
                 .ForMember(x => x.Id, opt => opt.Ignore());

            Mapper.CreateMap<Data.DTOs.AreasOfInterest, Data.Model.AreasOfInterest>();
            Mapper.CreateMap<Data.Model.AreasOfInterest, Data.DTOs.AreasOfInterest>();
            Mapper.CreateMap<ITOK.Core.Data.Model.AreasOfInterest, string>().ConvertUsing(x => (x == null) ? null : x.Id);
        }
        private static void CuratedContentMappings()
        {
            Mapper.CreateMap<Data.Model.CuratedContent, Data.Model.CuratedContent>()
                 .ForMember(x => x.Id, opt => opt.Ignore());

            Mapper.CreateMap<Data.DTOs.CuratedContent, Data.Model.CuratedContent>();
            Mapper.CreateMap<Data.Model.CuratedContent, Data.DTOs.CuratedContent>();
            Mapper.CreateMap<ITOK.Core.Data.Model.CuratedContent, string>().ConvertUsing(x => (x == null) ? null : x.Id);
        }
        private static void GotItMappings()
        {
            Mapper.CreateMap<Data.Model.GotItCount, Data.Model.GotItCount>()
                 .ForMember(x => x.Id, opt => opt.Ignore());

            Mapper.CreateMap<Data.DTOs.GotItCount, Data.Model.GotItCount>();
            Mapper.CreateMap<Data.Model.GotItCount, Data.DTOs.GotItCount>();
            Mapper.CreateMap<ITOK.Core.Data.Model.GotItCount, string>().ConvertUsing(x => (x == null) ? null : x.Id);
        }

        private static void ArticleMappings()
        {
            Mapper.CreateMap<Data.Model.Article, Data.Model.Article>()
                .ForMember(x => x.Id, opt => opt.Ignore())

                .ForMember(dest => dest.PrimaryMedia, opt => opt.Ignore())
                .ForMember(dest => dest.ArticleType, opt => opt.Ignore())
                .ForMember(dest => dest.RelatedMedia, opt => opt.Ignore())
                .ForMember(dest => dest.RelatedArticles, opt => opt.Ignore())
                .ForMember(dest => dest.RelatedGallery, opt => opt.Ignore())
                .ForMember(dest => dest.RelatedDownload, opt => opt.Ignore())
                ////.ForMember(dest => dest.RelatedTags, opt => opt.Ignore())
                .ForMember(dest => dest.RelatedJobRoles, opt => opt.Ignore())
                .ForMember(dest => dest.Status, opt => opt.Ignore())
                .ForMember(dest => dest.ArticleType, opt => opt.Ignore())
                // //.ForMember(dest => dest.UrlSlug, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedOn, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedBy, opt => opt.Ignore())
                .ForMember(dest => dest.UpdatedOn, opt => opt.Ignore())
                .ForMember(dest => dest.UpdatedBy, opt => opt.Ignore())
                ;

            Mapper.CreateMap<Data.DTOs.Article, Data.Model.Article>();
            Mapper.CreateMap<Data.Model.Article, Data.DTOs.Article>()

                .ForMember(x => x.PrimaryMediaId, opt => opt.MapFrom(origin => origin.PrimaryMedia))
                .ForMember(x => x.ArticleTypeId, opt => opt.MapFrom(origin => origin.ArticleType))
                .ForMember(x => x.RelatedMediaIds, opt => opt.MapFrom(origin => origin.RelatedMedia))
                .ForMember(x => x.RelatedArticleIds, opt => opt.MapFrom(origin => origin.RelatedArticles))
                .ForMember(x => x.RelatedGalleryIds, opt => opt.MapFrom(origin => origin.RelatedGallery))
                .ForMember(x => x.RelatedDownloadIds, opt => opt.MapFrom(origin => origin.RelatedDownload))
                //.ForMember(x => x.RelatedTagIds, opt => opt.MapFrom(origin => origin.RelatedTags))
                .ForMember(x => x.RelatedJobRoleIds, opt => opt.MapFrom(origin => origin.RelatedJobRoles))
                .ForMember(x => x.CreatedById,
                    opt => opt.MapFrom(origin => origin.CreatedBy != null ? origin.CreatedBy.Id : null))
                .ForMember(x => x.UpdatedById,
                    opt => opt.MapFrom(origin => origin.UpdatedBy != null ? origin.UpdatedBy.Id : null));

            Mapper.CreateMap<Data.Model.Article, string>().ConvertUsing(x => (x == null) ? null : x.Id);
        }

        private static void SubmittedArticleMappings()
        {
            Mapper.CreateMap<Data.Model.SubmittedArticle, Data.Model.SubmittedArticle>()
                .ForMember(x => x.Id, opt => opt.Ignore())

                .ForMember(dest => dest.PrimaryMedia, opt => opt.Ignore())
                .ForMember(dest => dest.ArticleType, opt => opt.Ignore())
                .ForMember(dest => dest.RelatedMedia, opt => opt.Ignore())
                .ForMember(dest => dest.RelatedArticles, opt => opt.Ignore())
                .ForMember(dest => dest.RelatedGallery, opt => opt.Ignore())
                .ForMember(dest => dest.RelatedDownload, opt => opt.Ignore())
                .ForMember(dest => dest.Status, opt => opt.Ignore())
                .ForMember(dest => dest.ArticleType, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedOn, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedBy, opt => opt.Ignore())
                .ForMember(dest => dest.UpdatedOn, opt => opt.Ignore())
                .ForMember(dest => dest.UpdatedBy, opt => opt.Ignore())
                ;

            Mapper.CreateMap<Data.DTOs.SubmittedArticle, Data.Model.SubmittedArticle>();
            Mapper.CreateMap<Data.Model.SubmittedArticle, Data.DTOs.SubmittedArticle>()

                .ForMember(x => x.PrimaryMediaId, opt => opt.MapFrom(origin => origin.PrimaryMedia))
                .ForMember(x => x.ArticleTypeId, opt => opt.MapFrom(origin => origin.ArticleType))
                .ForMember(x => x.RelatedMediaIds, opt => opt.MapFrom(origin => origin.RelatedMedia))
                .ForMember(x => x.RelatedArticleIds, opt => opt.MapFrom(origin => origin.RelatedArticles))
                .ForMember(x => x.RelatedGalleryIds, opt => opt.MapFrom(origin => origin.RelatedGallery))
                .ForMember(x => x.RelatedDownloadIds, opt => opt.MapFrom(origin => origin.RelatedDownload))
                //.ForMember(x => x.RelatedTagIds, opt => opt.MapFrom(origin => origin.RelatedTags))
                .ForMember(x => x.CreatedById,
                    opt => opt.MapFrom(origin => origin.CreatedBy != null ? origin.CreatedBy.Id : null))
                .ForMember(x => x.UpdatedById,
                    opt => opt.MapFrom(origin => origin.UpdatedBy != null ? origin.UpdatedBy.Id : null));

            Mapper.CreateMap<Data.Model.SubmittedArticle, string>().ConvertUsing(x => (x == null) ? null : x.Id);
        }


        private static void MediaMappings()
        {
            Mapper.CreateMap<Data.Model.Media, Data.Model.Media>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(dest => dest.RelatedArticles, opt => opt.Ignore())

                .ForMember(dest => dest.AvailableRatios, opt => opt.Ignore())
                .ForMember(dest => dest.FileName, opt => opt.Ignore())
                .ForMember(dest => dest.Format, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedOn, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedBy, opt => opt.Ignore())
                .ForMember(dest => dest.UpdatedOn, opt => opt.Ignore())
                .ForMember(dest => dest.UpdatedBy, opt => opt.Ignore())
                .ForMember(dest => dest.Status, opt => opt.Ignore());
            Mapper.CreateMap<Data.DTOs.Media, Data.Model.Media>();
            Mapper.CreateMap<Data.Model.Media, Data.DTOs.Media>()
                .ForMember(x => x.CreatedById, opt => opt.MapFrom(origin => origin.CreatedBy != null ? origin.CreatedBy.Id : null))
                .ForMember(x => x.UpdatedById, opt => opt.MapFrom(origin => origin.UpdatedBy != null ? origin.UpdatedBy.Id : null));
            Mapper.CreateMap<Data.Model.Media, string>().ConvertUsing(x => (x == null) ? null : x.Id);
        }

        private static void SubmittedMediaMappings()
        {
            Mapper.CreateMap<Data.Model.SubmittedMedia, Data.Model.SubmittedMedia>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(dest => dest.AvailableRatios, opt => opt.Ignore())
                .ForMember(dest => dest.FileName, opt => opt.Ignore())
                .ForMember(dest => dest.Format, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedOn, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedBy, opt => opt.Ignore())
                .ForMember(dest => dest.UpdatedOn, opt => opt.Ignore())
                .ForMember(dest => dest.UpdatedBy, opt => opt.Ignore())
                .ForMember(dest => dest.Status, opt => opt.Ignore());
            Mapper.CreateMap<Data.DTOs.SubmittedMedia, Data.Model.SubmittedMedia>();
            Mapper.CreateMap<Data.Model.SubmittedMedia, Data.DTOs.SubmittedMedia>()
                .ForMember(x => x.CreatedById, opt => opt.MapFrom(origin => origin.CreatedBy != null ? origin.CreatedBy.Id : null))
                .ForMember(x => x.UpdatedById, opt => opt.MapFrom(origin => origin.UpdatedBy != null ? origin.UpdatedBy.Id : null));
            Mapper.CreateMap<Data.Model.SubmittedMedia, string>().ConvertUsing(x => (x == null) ? null : x.Id);
        }

        private static void UserMappings()
        {
            Mapper.CreateMap<Data.Model.User, Data.Model.User>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedOn, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedById, opt => opt.Ignore())
                .ForMember(dest => dest.UpdatedOn, opt => opt.Ignore())
                .ForMember(dest => dest.UpdatedById, opt => opt.Ignore())
                .ForMember(dest => dest.Password, opt => opt.Ignore());
            Mapper.CreateMap<Data.Model.User, string>().ConvertUsing(x => (x == null) ? null : x.Id);
        }
              

        private static void JobRoleMappings()
        {
            Mapper.CreateMap<Data.Model.JobRole, Data.Model.JobRole>()
                 .ForMember(x => x.Id, opt => opt.Ignore());

            Mapper.CreateMap<Data.DTOs.JobRole, Data.Model.JobRole>();
            Mapper.CreateMap<Data.Model.JobRole, Data.DTOs.JobRole>();
            Mapper.CreateMap<ITOK.Core.Data.Model.JobRole, Object>().ConvertUsing(x => (x == null) ? null : x.Id);
        }

        private static void FavoriteMappings()
        {
            Mapper.CreateMap<Data.Model.FavoriteCount, Data.Model.FavoriteCount>()
                 .ForMember(x => x.Id, opt => opt.Ignore());

            Mapper.CreateMap<Data.DTOs.FavoriteCount, Data.Model.FavoriteCount>();
            Mapper.CreateMap<Data.Model.FavoriteCount, Data.DTOs.FavoriteCount>();
            Mapper.CreateMap<ITOK.Core.Data.Model.FavoriteCount, string>().ConvertUsing(x => (x == null) ? null : x.Id);
        }

        private static void UserGroupMappings()
        {
            Mapper.CreateMap<Data.Model.UserGroup, Data.Model.UserGroup>()
                 .ForMember(x => x.Id, opt => opt.Ignore());

            Mapper.CreateMap<Data.DTOs.UserGroup, Data.Model.UserGroup>();
            Mapper.CreateMap<Data.Model.UserGroup, Data.DTOs.UserGroup>();
            Mapper.CreateMap<ITOK.Core.Data.Model.UserGroup, string>().ConvertUsing(x => (x == null) ? null : x.Id);
        }

        private static void UserGroupMemberMappings()
        {
            Mapper.CreateMap<Data.Model.UserGroupMember, Data.Model.UserGroupMember>()
                 .ForMember(x => x.Id, opt => opt.Ignore());

            Mapper.CreateMap<Data.DTOs.UserGroupMember, Data.Model.UserGroupMember>();
            Mapper.CreateMap<Data.Model.UserGroupMember, Data.DTOs.UserGroupMember>();
            Mapper.CreateMap<ITOK.Core.Data.Model.UserGroupMember, string>().ConvertUsing(x => (x == null) ? null : x.Id);
        }



        private static void ArticleCommentMappings()
        {
            Mapper.CreateMap<Data.Model.ArticleComment, Data.Model.ArticleComment>()
                 .ForMember(x => x.Id, opt => opt.Ignore());

            Mapper.CreateMap<Data.DTOs.ArticleComment, Data.Model.ArticleComment>();
            Mapper.CreateMap<Data.Model.ArticleComment, Data.DTOs.ArticleComment>();
            Mapper.CreateMap<ITOK.Core.Data.Model.ArticleComment, Object>().ConvertUsing(x => (x == null) ? null : x.Id);
        }

        private static void QuestionCommentMappings()
        {
            Mapper.CreateMap<Data.Model.QuestionComment, Data.Model.QuestionComment>()
                 .ForMember(x => x.Id, opt => opt.Ignore());

            Mapper.CreateMap<Data.DTOs.QuestionComment, Data.Model.QuestionComment>();
            Mapper.CreateMap<Data.Model.QuestionComment, Data.DTOs.QuestionComment>();
            Mapper.CreateMap<Data.DTOs.UsersComment, Data.Model.UsersComment>();
            Mapper.CreateMap<Data.Model.UsersComment, Data.DTOs.UsersComment>();
            Mapper.CreateMap<Data.Model.QuestionComment, Object>().ConvertUsing(x => (x == null) ? null : x.Id);
        }

        private static void AnswerFlagMappings()
        {
            Mapper.CreateMap<Data.Model.AnswerFlag, Data.Model.AnswerFlag>()
                 .ForMember(x => x.Id, opt => opt.Ignore());

            Mapper.CreateMap<Data.DTOs.AnswerFlag, Data.Model.AnswerFlag>();
            Mapper.CreateMap<Data.Model.AnswerFlag, Data.DTOs.AnswerFlag>();
            Mapper.CreateMap<Data.Model.AnswerFlag, Object>().ConvertUsing(x => (x == null) ? null : x.Id);
        }

        private static void ReadingListMappings()
        {

            Mapper.CreateMap<Data.Model.ReadingList, Data.Model.ReadingList>()
                .ForMember(dest => dest.User, opt => opt.Ignore());

            Mapper.CreateMap<Data.DTOs.ReadingList, Data.Model.ReadingList>();
            Mapper.CreateMap<Data.Model.ReadingList, Data.DTOs.ReadingList>()

                .ForMember(x => x.UserId, opt => opt.MapFrom(origin => origin.User));

            Mapper.CreateMap<Data.Model.ReadingList, string>().ConvertUsing(x => (x == null) ? null : x.Id);
        }


    }
}