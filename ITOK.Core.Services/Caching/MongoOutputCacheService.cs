﻿using DevTrends.MvcDonutCaching;
using MongoDB.Driver;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web.Routing;
using ITOK.Core.Data;
using ITOK.Core.Data.DTOs;

namespace ITOK.Core.Services.Caching
{
    public class MongoOutputCacheService : IEnumerable<KeyValuePair<string, object>>
    {
        private IMongoOutputCacheRepository _repo;
        private readonly OutputCacheManager _cacheManager = new OutputCacheManager();
        private readonly IMongoCollection<MongoOutputCacheItem> _cacheItems;

        public MongoOutputCacheService(IMongoOutputCacheRepository repo)
        {
            this._repo = repo;
        }

        //public MongoOutputCacheService()
        //{
        //    _cacheItems = _repo.GetMongoCollection<MongoOutputCacheItem>(typeof(MongoOutputCacheItem).ToString());
        //}

        //public byte[] FindByKey(string key)
        //{
        //    MongoOutputCacheItem cacheItem = _repo.AsQueryable()
        //        .FirstOrDefault(x => x.Id == key);
        //    if (cacheItem == null) return null;
        //    return cacheItem.Item;
        //}
        //public byte[] FindExistingUnexpired(string key)
        //{
        //    MongoOutputCacheItem cacheItem = _repo.AsQueryable()
        //        .OrderByDescending(x => x.Expiration)
        //        .Where(x =>
        //            x.Expiration <= DateTime.UtcNow &&
        //            x.Id == key)
        //        .FirstOrDefault();
        //    if (cacheItem == null) return null;
        //    return cacheItem.Item;
        //}

        public object FindByKey(string key)
        {
            var cacheItem = _repo.AsQueryable()
                .FirstOrDefault(x => x.KeyUrl == key);
            if (cacheItem == null) return null;

            if (cacheItem.Expiration.ToUniversalTime() <= DateTime.UtcNow)
            {
                _repo.Remove(cacheItem.Id);
                return null;
            }

            return Deserialize(cacheItem.Item);
        }

        public bool FindExistingKey(string key)
        {
            var cacheItem = _repo.AsQueryable()
                .FirstOrDefault(x => x.KeyUrl == key);
            if (cacheItem == null) return true;

            return false;
        }

        public object FindExistingUnexpired(string key)
        {
            //Changed from   x.Expiration <= DateTime.UtcNow && to   x.Expiration >= DateTime.UtcNow && to get the unexpired content
            var cacheItem = _repo.AsQueryable()
                .OrderByDescending(x => x.Expiration)
                .Where(x =>
                    x.Expiration >= DateTime.UtcNow &&
                    x.KeyUrl == key)
                .FirstOrDefault();
            if (cacheItem == null) return null;
            return Deserialize(cacheItem.Item);
        }

        public void Insert(string key, object data, DateTime utcExpiry)
        {
            if (FindExistingKey(key))
                _repo.Insert(new MongoOutputCacheItem
                {
                    Item = Serialize(data),
                    Expiration = utcExpiry,
                    KeyUrl = key
                });
        }

        public void InsertOrUpdate(string key, object data, DateTime utcExpiry)
        {
            var cacheItem = _repo.AsQueryable()
                .FirstOrDefault(x => x.KeyUrl == key);

            if (cacheItem == null)
            {
                Insert(key, data, utcExpiry);
                return;
            }

            cacheItem.Item = Serialize(data);
            cacheItem.Expiration = utcExpiry;
            cacheItem.LastModified = DateTime.UtcNow.ToUniversalTime();

            _repo.Save(cacheItem);
        }

        public static byte[] Serialize(object entry)
        {
            var formatter = new BinaryFormatter();
            var stream = new MemoryStream();
            formatter.Serialize(stream, entry);

            return Compress(stream.ToArray());
        }

        public static object Deserialize(byte[] serializedEntry)
        {
            var formatter = new BinaryFormatter();
            var stream = new MemoryStream(Decompress(serializedEntry));

            return formatter.Deserialize(stream);
        }

        public static byte[] Compress(byte[] input)
        {
            byte[] output;
            using (var ms = new MemoryStream())
            {
                using (var gs = new GZipStream(ms, CompressionMode.Compress))
                {
                    gs.Write(input, 0, input.Length);
                    gs.Close();
                    output = ms.ToArray();
                }
                ms.Close();
            }
            return output;
        }

        public static byte[] Decompress(byte[] input)
        {
            var output = new List<byte>();
            using (var ms = new MemoryStream(input))
            {
                using (var gs = new GZipStream(ms, CompressionMode.Decompress))
                {
                    var readByte = gs.ReadByte();
                    while (readByte != -1)
                    {
                        output.Add((byte)readByte);
                        readByte = gs.ReadByte();
                    }
                    gs.Close();
                }
                ms.Close();
            }
            return output.ToArray();
        }

        public void DeleteByKey(string key)
        {
            IList<MongoOutputCacheItem> cacheItems = _repo.AsQueryable()
                .Where(x => x.KeyUrl == key)
                .ToList();

            foreach (var cacheItem in cacheItems)
            {
                _repo.Remove(cacheItem.Id);
            }
        }

        public void RemoveExpired(string key)
        {
            IList<MongoOutputCacheItem> expiredCacheItems = _repo.AsQueryable()
                .Where(x =>
                    x.KeyUrl == key &&
                    x.Expiration <= DateTime.UtcNow)
                .ToList();
            foreach (var cacheItem in expiredCacheItems)
            {
                _repo.Remove(cacheItem.Id);
            }
        }

        /// <summary>
        /// Removes single output cache item according to the controller and action.
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="action"></param>
        public void RemoveItem(string controller)
        {
            RemoveItem(controller, null, null);
        }

        /// <summary>
        /// Removes single output cache item according to the controller and action.
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="action"></param>
        public void RemoveItem(string controller, string action)
        {
            RemoveItem(controller, action, null);
        }

        /// <summary>
        /// Removes single output cache item according to the controller name, action name and route value
        /// </summary>
        /// <param name="controller"></param>
        public void RemoveItem(string controller, string action, params object[] routeValues)
        {
            _cacheManager.RemoveItem(controller, action, new RouteValueDictionary(routeValues));
        }

        /// <summary>
        /// Removes all output cacheitems according to the controller name
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="action"></param>
        public void RemoveItems(string controller)
        {
            RemoveItems(controller, null, null);
        }

        /// <summary>
        /// Removes all output cacheitems according to the controller and action name.
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="action"></param>
        public void RemoveItems(string controller, string action)
        {
            RemoveItems(controller, action, null);
        }

        /// <summary>
        /// Remove all output cacheitems according to the controller name, action name and route value
        /// </summary>
        /// <param name="controller"></param>
        public void RemoveItems(string controller, string action, params object[] routeValues)
        {
            _cacheManager.RemoveItems(controller, action, new RouteValueDictionary(routeValues));
        }

        #region IEnumerable<KeyValuePair<string,object>> Members

        public IEnumerator<KeyValuePair<string, object>> GetEnumerator()
        {
            foreach (var item in _repo.AsQueryable())
            {
                yield return new KeyValuePair<string, object>(item.KeyUrl, item.Item);
            }
        }

        #endregion IEnumerable<KeyValuePair<string,object>> Members

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion IEnumerable Members
    }
}