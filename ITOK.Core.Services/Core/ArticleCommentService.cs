﻿using AutoMapper;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using ITOK.Core.Data.Model.HS;
using ITOK.Core.Services.Core.Wrapped;

namespace ITOK.Core.Services.Core
{
    public class ArticleCommentService : BaseService
    {
       
        public ArticleCommentService(IMongoRepository1 mongoRepository)
            : base(mongoRepository)
        {
          
        }

        public IList<ArticleComment> FindByIds(ArticleCommentHydrationSettings hydrationSettings = null, params string[] ids)
        {
            if (ids == null)
                return new List<ArticleComment>();
            return MongoRepository.AsQueryable<Data.DTOs.ArticleComment>()
            .Where(x => ids.Contains(x.Id)).ToList().AsQueryable()
            .Select(x => hydrate(x, hydrationSettings))
            .ToList();
        }

        public List<ArticleComment> FindByArticleID(string ArticleId, ArticleCommentHydrationSettings hydrationSettings = null)
        {
            return MongoRepository.AsQueryable<Data.DTOs.ArticleComment>()
                .Where(x => x.ArticleId == ArticleId).ToList().AsQueryable()
                .Select(x => hydrate(x, hydrationSettings))
                .ToList();
        }

     

        public ArticleComment FindByID(string ID, ArticleCommentHydrationSettings hydrationSettings = null)
        {
            return MongoRepository.AsQueryable<Data.DTOs.ArticleComment>()
                .Where(x => x.Id == ID).ToList().AsQueryable()
                .Select(x => hydrate(x, hydrationSettings))
                .FirstOrDefault();
        }

       

        public int GetCount()
        {
            return MongoRepository.AsQueryable<Data.DTOs.ArticleComment>().Count();
        }

        public IList<ArticleComment> GetPagedOrderedByComment(int skip, int take, bool ascending = true, ArticleCommentHydrationSettings hydrationSettings = null)
        {
            var query = MongoRepository.AsQueryable<Data.DTOs.ArticleComment>();
            query = ascending ? query.OrderBy(x => x.Comment) : query.OrderByDescending(x => x.Comment);
            query = query.Skip(skip).Take(take);

            return query
                .Select(x => hydrate(x, hydrationSettings))
                .ToList();
        }

        public void Save(Data.Model.ArticleComment jobrole)
        {
            if (jobrole.Id == null) MongoRepository.Insert(jobrole);
            else MongoRepository.Update(jobrole);
        }

        #region Private Methods

        protected ArticleComment hydrate(Data.DTOs.ArticleComment ArticleComment, ArticleCommentHydrationSettings hydrationSettings)
        {
            if (ArticleComment == null) return null;
            // If hydration settings arent supplied then use defaults
            if (hydrationSettings == null) hydrationSettings = new ArticleCommentHydrationSettings();

            var viewModel = Mapper.Map<Data.DTOs.ArticleComment, ArticleComment>(ArticleComment);

            if (hydrationSettings.CreatedBy != null && !string.IsNullOrEmpty(ArticleComment.CreatedById))
            {
                var userService = new UserService(base.MongoRepository);
                viewModel.CreatedBy = userService.FindById(ArticleComment.CreatedById);
            }

            if (hydrationSettings.UpdatedBy != null && !string.IsNullOrEmpty(ArticleComment.UpdatedById))
            {
                var userService = new UserService(base.MongoRepository);
                viewModel.UpdatedBy = userService.FindById(ArticleComment.UpdatedById);
            }

            return viewModel;
        }

        protected Data.DTOs.ArticleComment deHydrate(ArticleComment viewModel)
        {
            var ArticleComment = Mapper.Map<ArticleComment, Data.DTOs.ArticleComment>(viewModel);
            return ArticleComment;
        }

        #endregion Private Methods
    }
}