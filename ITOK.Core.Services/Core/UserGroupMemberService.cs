﻿using AutoMapper;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using ITOK.Core.Data.Model.HS;
using ITOK.Core.Services.Core.Wrapped;

namespace ITOK.Core.Services.Core
{
    public class UserGroupMemberService : BaseService
    {

        public UserGroupMemberService(IMongoRepository1 mongoRepository)
            : base(mongoRepository)
        {
       
        }

        public IList<UserGroupMember> FindByIds(UserGroupMemberHydrationSettings hydrationSettings = null, params string[] ids)
        {
            if (ids == null)
                return new List<UserGroupMember>();
            return MongoRepository.AsQueryable<Data.DTOs.UserGroupMember>()
            .Where(x => ids.Contains(x.Id)).ToList().AsQueryable()
            .Select(x => hydrate(x, hydrationSettings))
            .ToList();
        }


        public UserGroupMember FindByUserAndUserGroup(string UserId, string userGroupId, UserGroupMemberHydrationSettings hydrationSettings = null)
        {
            return MongoRepository.AsQueryable<Data.DTOs.UserGroupMember>()
                    .Where(x => x.UserID == UserId && x.UserGroupID == userGroupId)
                    .ToList().Select(x => hydrate(x, hydrationSettings)).FirstOrDefault();
        }

        public UserGroupMember FindByID(string ID, UserGroupMemberHydrationSettings hydrationSettings = null)
        {
            return MongoRepository.AsQueryable<Data.DTOs.UserGroupMember>()
                .Where(x => x.Id == ID).ToList().AsQueryable()
                .Select(x => hydrate(x, hydrationSettings))
                .FirstOrDefault();
        }


        public IList<UserGroupMember> FindByUserGroup(string ID, UserGroupMemberHydrationSettings hydrationSettings = null)
        {
            //var resultTest = MongoRepository.AsQueryable<Data.DTOs.UserGroupMember>().ToList();
            return MongoRepository.AsQueryable<Data.DTOs.UserGroupMember>()
                .Where(x => x.UserGroupID == ID).ToList()
                .Select(x => hydrate(x, hydrationSettings)).ToList();
        }


        ///
        public List<UserGroupMember> GetAll(UserGroupMemberHydrationSettings hydrationSettings = null)
        {
            //return _jobRoleServiceWrapped.GetAll(hydrationSettings);
            return MongoRepository.AsQueryable<Data.DTOs.UserGroupMember>().ToList()
                .Select(x => hydrate(x, hydrationSettings)).ToList();
        }

        //public List<JobRole> GetTopLevel(JobRoleHydrationSettings hydrationSettings = null)
        //{
        //    //return _jobRoleServiceWrapped.GetTopLevel(hydrationSettings);
        //}

        public int GetCount()
        {
            return MongoRepository.AsQueryable<Data.DTOs.UserGroupMember>().Count();
        }

        public IList<UserGroupMember> GetPagedOrderedByName(int skip, int take, bool ascending = true, UserGroupMemberHydrationSettings hydrationSettings = null)
        {
            var query = MongoRepository.AsQueryable<Data.DTOs.UserGroupMember>();
            //query = ascending ? query.OrderBy(x => x.) : query.OrderByDescending(x => x.Name);
            query = query.Skip(skip).Take(take);

            return query
                .Select(x => hydrate(x, hydrationSettings))
                .ToList();
        }

        public void Save(Data.Model.UserGroupMember userGroupMember)
        {
            if (userGroupMember.Id == null) MongoRepository.Insert(userGroupMember);
            else MongoRepository.Update(userGroupMember);
        }

        #region Private Methods

        protected UserGroupMember hydrate(Data.DTOs.UserGroupMember userGroupMember, UserGroupMemberHydrationSettings hydrationSettings)
        {
            if (userGroupMember == null) return null;
            // If hydration settings arent supplied then use defaults
            if (hydrationSettings == null) hydrationSettings = new UserGroupMemberHydrationSettings();

            var viewModel = Mapper.Map<Data.DTOs.UserGroupMember, UserGroupMember>(userGroupMember);

            if (hydrationSettings.CreatedBy != null && !string.IsNullOrEmpty(userGroupMember.CreatedById))
            {
                var userService = new UserService(base.MongoRepository);
                viewModel.CreatedBy = userService.FindById(userGroupMember.CreatedById);
            }

            if (hydrationSettings.UpdatedBy != null && !string.IsNullOrEmpty(userGroupMember.UpdatedById))
            {
                var userService = new UserService(base.MongoRepository);
                viewModel.UpdatedBy = userService.FindById(userGroupMember.UpdatedById);
            }

            return viewModel;
        }

        protected Data.DTOs.User deHydrate(User viewModel)
        {
            var User = Mapper.Map<User, Data.DTOs.User>(viewModel);
            return User;
        }

        #endregion Private Methods
    }
}