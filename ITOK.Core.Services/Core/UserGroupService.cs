﻿using AutoMapper;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using ITOK.Core.Data.Model.HS;
using ITOK.Core.Services.Core.Wrapped;

namespace ITOK.Core.Services.Core
{
    public class UserGroupService : BaseService
    {
        public UserGroupService(IMongoRepository1 mongoRepository)
            : base(mongoRepository)
        {
       
        }

        public IList<UserGroup> FindByIds(UserGroupHydrationSettings hydrationSettings = null, params string[] ids)
        {
            if (ids == null)
                return new List<UserGroup>();
            return MongoRepository.AsQueryable<Data.DTOs.UserGroup>()
            .Where(x => ids.Contains(x.Id)).ToList().AsQueryable()
            .Select(x => hydrate(x, hydrationSettings))
            .ToList();
        }


        public IList<UserGroup> FindByName(string Name, UserGroupHydrationSettings hydrationSettings = null)
        {
            return MongoRepository.AsQueryable<Data.DTOs.UserGroup>()
                .Where(x => x.Name.ToLower() == Name.ToLower()).ToList().AsQueryable()
                .Select(x => hydrate(x, hydrationSettings))
                .ToList();
        }

        public UserGroup FindByID(string ID, UserGroupHydrationSettings hydrationSettings = null)
        {
            return MongoRepository.AsQueryable<Data.DTOs.UserGroup>()
                .Where(x => x.Id == ID).ToList().AsQueryable()
                .Select(x => hydrate(x, hydrationSettings))
                .FirstOrDefault();
        }

        ///
        public List<UserGroup> GetAll(UserGroupHydrationSettings hydrationSettings = null)
        {
           
             return MongoRepository.AsQueryable<Data.DTOs.UserGroup>().ToList()
                .Select(x => hydrate(x, hydrationSettings)).ToList();
        }

        //public List<JobRole> GetTopLevel(JobRoleHydrationSettings hydrationSettings = null)
        //{
        //    //return _jobRoleServiceWrapped.GetTopLevel(hydrationSettings);
        //}

        public int GetCount()
        {
            return MongoRepository.AsQueryable<Data.DTOs.UserGroup>().Count();
        }

        public IList<UserGroup> GetPagedOrderedByName(int skip, int take, bool ascending = true, UserGroupHydrationSettings hydrationSettings = null)
        {
            var query = MongoRepository.AsQueryable<Data.DTOs.UserGroup>();
            query = ascending ? query.OrderBy(x => x.Name) : query.OrderByDescending(x => x.Name);
            query = query.Skip(skip).Take(take);

            return query
                .Select(x => hydrate(x, hydrationSettings))
                .ToList();
        }

        public void Save(Data.DTOs.UserGroup userGroup)
        {
            var DTOuserGroup = userGroup;//deHydrate(userGroup);
            if (DTOuserGroup.Id == null) MongoRepository.Insert(DTOuserGroup);
            else MongoRepository.Update(DTOuserGroup);
        }

        #region Private Methods

        protected UserGroup hydrate(Data.DTOs.UserGroup UserGroup, UserGroupHydrationSettings hydrationSettings)
        {
            if (UserGroup == null) return null;
            // If hydration settings arent supplied then use defaults
            if (hydrationSettings == null) hydrationSettings = new UserGroupHydrationSettings();

            var viewModel = Mapper.Map<Data.DTOs.UserGroup, UserGroup>(UserGroup);

            if (hydrationSettings.CreatedBy != null && !string.IsNullOrEmpty(UserGroup.CreatedById))
            {
                var userService = new UserService(base.MongoRepository);
                viewModel.CreatedBy = userService.FindById(UserGroup.CreatedById);
            }

            if (hydrationSettings.UpdatedBy != null && !string.IsNullOrEmpty(UserGroup.UpdatedById))
            {
                var userService = new UserService(base.MongoRepository);
                viewModel.UpdatedBy = userService.FindById(UserGroup.UpdatedById);
            }

            return viewModel;
        }

        protected Data.DTOs.UserGroup deHydrate(UserGroup viewModel)
        {
            var model = Mapper.Map<UserGroup, Data.DTOs.UserGroup>(viewModel);
            return model;
        }

        #endregion Private Methods
    }
}