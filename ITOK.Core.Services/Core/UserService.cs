﻿using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using System.Collections.Generic;
using System.Linq;

namespace ITOK.Core.Services.Core
{
    public class UserService : BaseService
    {
        public UserService(IMongoRepository1 mongoRepository)
            : base(mongoRepository)
        {
        }

        public User FindById(string id)
        {

            var user = MongoRepository.AsQueryable<User>()
                .SingleOrDefault(x => x.Id == id.ToLower());
            if (user == null)
            {
                return null;
            }
            user.Password = string.Empty;
            return user;
        }

        public List<User> FindByIds(params string[] IDs)
        {
            if (IDs == null || !IDs.Any())
            {
                return new List<User>();
            }

            return MongoRepository.AsQueryable<User>().Where(p => IDs.Contains(p.Id)).ToList();


        }
    }
}