﻿using AutoMapper;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using ITOK.Core.Common.Constants;
using ITOK.Core.Common.Utils;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using ITOK.Core.Data.Model.HS;
using ITOK.Core.Common;

namespace ITOK.Core.Services.Core.Wrapped
{
    public class ArticleServiceWrapped : ITOK.Core.Services.Core.Wrapped.IArticleServiceWrapped
    {
        private IMongoRepository1 _mongoRepository;

        
        private readonly FavoriteCountService _favoriteCountService;
        private readonly ReadingListService _readingListService;

        public ArticleServiceWrapped(IMongoRepository1 mongoRepository)
        {
            _mongoRepository = mongoRepository;
            _favoriteCountService = new FavoriteCountService(mongoRepository);
            _readingListService = new ReadingListService(mongoRepository);
           
        }

        public IFindFluent<Data.DTOs.Article, Data.DTOs.Article> GenerateBasicQuery(Brand brand,
            PublicationStatus[] showWithStatus = null,
            bool showOnlyLive = true, ArticleType? articleType = null,  int? skip = null,
            int? take = null)
        {
            var filter = Builders<Data.DTOs.Article>.Filter.Empty;


           // filter = filter & filter & Builders<Data.DTOs.Article>.Filter.Eq("brand", brand);


            if (showWithStatus != null)
            {
                var showWithStatusInts = showWithStatus.Select(x => (int) x);

                filter = filter & Builders<Data.DTOs.Article>.Filter.In("Status", showWithStatusInts.ToList());
            }
            if (showOnlyLive)
            {
                var dt = new BsonDateTime(DateTime.UtcNow);

                //this line causes the serialization error
                filter = filter & Builders<Data.DTOs.Article>.Filter.Lt("LiveFrom", new BsonDateTime(DateTime.UtcNow));

                ///todo:filter out archived content NH-130
                filter = filter & Builders<Data.DTOs.Article>.Filter.Gte("LiveTo", new BsonDateTime(DateTime.UtcNow));
                filter = filter & Builders<Data.DTOs.Article>.Filter.Eq("Status", PublicationStatus.Published);
            }

            if (articleType != null)
                filter = filter & Builders<Data.DTOs.Article>.Filter.Eq("ArticleType", (ArticleType) articleType);

            var query = _mongoRepository.GetCollection<Data.DTOs.Article>().Find(filter, new FindOptions() {});

            // query = query.Skip(skip);

            //query = query.Limit(take);

            return query;
        }

      
        public Article FindBySlug(Brand brand, string urlSlug, ArticleHydrationSettings hydrationSettings, PublicationStatus[] showWithStatus = null, bool showOnlyLive = false)
        {
            urlSlug = urlSlug.ToLower();

            var query = GenerateBasicQuery(brand,showWithStatus, showOnlyLive, null);

            var filter = query.Filter & Builders<Data.DTOs.Article>.Filter.Eq("UrlSlug", urlSlug);

            query.Filter = filter;

            var article = query.FirstOrDefault();

            return hydrate(article, hydrationSettings);
        }
        public List<Article> FindAllBySlug(Brand brand,string urlSlug, ArticleHydrationSettings hydrationSettings, PublicationStatus[] showWithStatus = null, bool showOnlyLive = false)
        {
            urlSlug = urlSlug.ToLower();

            var query = GenerateBasicQuery(brand,showWithStatus, showOnlyLive, null);

            var filter = query.Filter & Builders<Data.DTOs.Article>.Filter.Eq("UrlSlug", urlSlug);

            query.Filter = filter;

            return query.ToList().Select(x => hydrate(x, hydrationSettings)).ToList();
        }
        public IEnumerable<Article> FindByBrandArticleNumber(Brand brand,string brandarticlenumber, ArticleHydrationSettings hydrationSettings, PublicationStatus[] showWithStatus = null, bool showOnlyLive = false)
        {
            // brandarticlenumber = brandarticlenumber.ToLower();

            var query = GenerateBasicQuery(brand ,showWithStatus, showOnlyLive, null);

            var filter = query.Filter & Builders<Data.DTOs.Article>.Filter.Regex("BrandArticleNumber", new BsonRegularExpression("/^" + brandarticlenumber + "$/i"));

            query.Filter = filter;

            return query.ToList().Select(x => hydrate(x, hydrationSettings)).ToList();
        }


        public IEnumerable<Article> FindBySEOTitle(Brand brand,string seoTitle,  ArticleHydrationSettings hydrationSettings, PublicationStatus[] showWithStatus = null, bool showOnlyLive = false)
        {
            // brandarticlenumber = brandarticlenumber.ToLower();

            var query = GenerateBasicQuery(brand,showWithStatus, showOnlyLive, null);

            var filter = query.Filter & Builders<Data.DTOs.Article>.Filter.Regex("SEOTitle", new BsonRegularExpression("/^" + seoTitle + "$/i"));
            
            
            //filter = filter & filter & Builders<Data.DTOs.Article>.Filter.Eq("brand", brand);

            query.Filter = filter;

            

            return query.ToList().Select(x => hydrate(x, hydrationSettings)).ToList();
        }


        public IList<Article> FindByIds(  ArticleHydrationSettings hydrationSettings = null, params string[] ids)
            {
            if (ids == null || !ids.Any()) return new List<Article>();

            var filter = Builders<Data.DTOs.Article>.Filter.In("_id", ids.ToList());
            return _mongoRepository.GetCollection<Data.DTOs.Article>().Find(filter).ToList()
                .Select(x => hydrate(x, hydrationSettings))
                .ToList();
            }

        public IList<Article> FindByIds(Brand brand, ArticleHydrationSettings hydrationSettings = null, params string[] ids)
        {
            if (ids == null || !ids.Any()) return new List<Article>();

            var filter = Builders<Data.DTOs.Article>.Filter.In("_id", ids.ToList());

            filter =  filter & Builders<Data.DTOs.Article>.Filter.Eq("brand", brand);
            
            var list = _mongoRepository.GetCollection<Data.DTOs.Article>().Find(filter).ToList();
                

            return list
               .Select(x => hydrate(x, hydrationSettings))
               .ToList();

        }

        public IEnumerable<Article> FindByUrlSlugs(Brand brand, ArticleHydrationSettings hydrationSettings = null, params string[] urlSlugs)
        {
            if (urlSlugs == null || !urlSlugs.Any()) return new List<Article>();



            var filter = Builders<Data.DTOs.Article>.Filter.Eq("brand", brand);
            
            filter = filter & Builders<Data.DTOs.Article>.Filter.In("UrlSlug", new BsonArray(urlSlugs));
           
            return _mongoRepository.GetCollection<Data.DTOs.Article>().Find(filter).ToList()
                 .Select(x => hydrate(x, hydrationSettings))
                 .ToList();
        }



        public IEnumerable<Article> FindByUrlSlugsWithArticleStatDetails(Brand brand,IList<Data.DTOs.ArticleStat> articleStatList, ArticleHydrationSettings hydrationSettings = null, params string[] articleIds)
        {
        if (articleIds == null || !articleIds.Any()) return new List<Article>();

            var filter = Builders<Data.DTOs.Article>.Filter.In("_id", new BsonArray(articleIds));
            filter = filter &  Builders<Data.DTOs.Article>.Filter.Eq("brand", brand);

            var articles = _mongoRepository.GetCollection<Data.DTOs.Article>().Find(filter).ToList()
                 .Select(x => hydrate(x, hydrationSettings))
                 .ToArray();

            return articleStatList.Select(stat => SetNewArticleWithSeenTimeStamp(articles.FirstOrDefault(x => x.Id == stat.ArticleId), stat)).ToList();
        }

        private Data.Model.Article SetNewArticleWithSeenTimeStamp(Data.Model.Article article, Data.DTOs.ArticleStat stat)
        {
       
            var resultArticle = Mapper.Map<Article, Article>( article);
            resultArticle.SeenTimeStamp = stat.Timestamp;
            return resultArticle;
         
        }

        public IList<Article> GetPagedOrderedBy(Brand brand, SortOrder sortOrder, int skip, int take, string[] tags, 
            PublicationStatus[] showWithStatus = null, bool showOnlyLive = false, bool ascending = false,
            ArticleType? articleType = null, ArticleHydrationSettings hydrationSettings = null, bool allTags = true)
        {
            var query = GenerateBasicQuery(brand,showWithStatus, showOnlyLive, articleType);

            var filter = query.Filter;

            var orfilter = Builders<Data.DTOs.Article>.Filter.Empty;

            if (tags.Any())
                {
                if (allTags)
                    {
                    filter = tags.Aggregate(filter,
                        (current, tag) => current & Builders<Data.DTOs.Article>.Filter.Eq("Tags", tag));
                    }
                else
                    {
                    foreach (var tag in tags)
                    {
                        if (orfilter == FilterDefinition<Data.DTOs.Article>.Empty)
                            orfilter = Builders<Data.DTOs.Article>.Filter.Eq("Tags", tag);
                        else
                            orfilter = orfilter | Builders<Data.DTOs.Article>.Filter.Eq("Tags", tag);
                    }
                }
                //orfilter = tags.Aggregate(orfilter,
                //    (current, tag) => current | Builders<Data.DTOs.Article>.Filter.Eq("Tags", tag));
            }
                
            //query = query.Where(x => tags.Contains(x.Tags));

            switch (sortOrder)
            {
                case SortOrder.CreatedOn:

                    var sortCreatedOn = ascending ? Builders<Data.DTOs.Article>.Sort.Ascending("CreatedOn") : Builders<Data.DTOs.Article>.Sort.Descending("CreatedOn");

                    query = query.Sort(sortCreatedOn);
                    break;

                case SortOrder.UpdatedOn:
                    var sortUpdatedOn = ascending ? Builders<Data.DTOs.Article>.Sort.Ascending("UpdatedOn") : Builders<Data.DTOs.Article>.Sort.Descending("UpdatedOn");

                    query = query.Sort(sortUpdatedOn);

                    break;

                case SortOrder.LiveFrom:
                    var sortLiveFrom = ascending ? Builders<Data.DTOs.Article>.Sort.Ascending("LiveFrom") : Builders<Data.DTOs.Article>.Sort.Descending("LiveFrom");

                    query = query.Sort(sortLiveFrom);

                    break;

                case SortOrder.VersionDate:
                    var sortVersionDate = ascending ? Builders<Data.DTOs.Article>.Sort.Ascending("VersionDate") : Builders<Data.DTOs.Article>.Sort.Descending("VersionDate");

                    query = query.Sort(sortVersionDate);

                    break;
                default:
                    break;
            }

            query.Filter = filter & orfilter ;
            //if (!allTags)
            //    query = query.ToList().Where(x => tags.Any(x.Tags.Contains)));

            query = query.Skip(skip).Limit(take);

            var list = query.ToList();
            //list = list.Where(p => p.Tags.Any(tag => tags.Contains(tag))).ToList();

            //list = list.Skip(skip).Take(take).ToList();
            return list
                .Select(x => hydrate(x, hydrationSettings))
                .ToList();
        }



        public IList<Article> GetPagedInDateRangeOrderedBy(Brand brand, SortOrder sortOrder, int daysAgo, int skip, int? take, string[] tags, List<string> ids,
         PublicationStatus[] showWithStatus = null, bool showOnlyLive = false, bool ascending = false,
         ArticleType? articleType = null, ArticleHydrationSettings hydrationSettings = null, bool allTags = true )
        {
            var query = GenerateBasicQuery(brand,showWithStatus, true, articleType);

            var filter = query.Filter;

            filter = filter & Builders<Data.DTOs.Article>.Filter.Eq("Status", PublicationStatus.Published);
           
            switch (daysAgo)
                {

                case -1:

                    filter = filter &
                             Builders<Data.DTOs.Article>.Filter.Lt("VersionDate", new BsonDateTime(DateTime.UtcNow));
                    filter = filter & Builders<Data.DTOs.Article>.Filter.Gte("VersionDate", new BsonDateTime(DateTime.UtcNow.Date));
                    break;

                case -2:


                    filter = filter & Builders<Data.DTOs.Article>.Filter.Lt("VersionDate",
                                 new BsonDateTime(DateTime.UtcNow.Date));
                    filter = filter & Builders<Data.DTOs.Article>.Filter.Gte("VersionDate", new BsonDateTime(DateTime.UtcNow.AddDays(-1).Date));

                    break;

                default:
                    filter = filter & Builders<Data.DTOs.Article>.Filter.Lt("VersionDate", new BsonDateTime(DateTime.UtcNow));
                    filter = filter & Builders<Data.DTOs.Article>.Filter.Gte("VersionDate", new BsonDateTime(DateTime.UtcNow.AddDays(daysAgo)));
                    break;
                    
            }

            var orfilter = Builders<Data.DTOs.Article>.Filter.Empty;

           

            if (tags.Any())
                {
                if (allTags)
                    {
                    filter = tags.Aggregate(filter,
                        (current, tag) => current & Builders<Data.DTOs.Article>.Filter.Eq("Tags", tag));
                    }
                else
                    {
                    foreach (var tag in tags)
                        {
                        if (orfilter == FilterDefinition<Data.DTOs.Article>.Empty)
                            orfilter = Builders<Data.DTOs.Article>.Filter.Eq("Tags", tag);
                        else
                            orfilter = orfilter | Builders<Data.DTOs.Article>.Filter.Eq("Tags", tag);
                        }
                    }
                }




            if (ids.Any())
            {
                filter = filter & Builders<Data.DTOs.Article>.Filter.Nin("_id", ids);
            }
            //query = query.Where(x => tags.Contains(x.Tags));

            switch (sortOrder)
            {
                case SortOrder.CreatedOn:

                    var sortCreatedOn = ascending ? Builders<Data.DTOs.Article>.Sort.Ascending("CreatedOn") : Builders<Data.DTOs.Article>.Sort.Descending("CreatedOn");

                    query = query.Sort(sortCreatedOn);
                    break;

                case SortOrder.UpdatedOn:
                    var sortUpdatedOn = ascending ? Builders<Data.DTOs.Article>.Sort.Ascending("UpdatedOn") : Builders<Data.DTOs.Article>.Sort.Descending("UpdatedOn");

                    query = query.Sort(sortUpdatedOn);

                    break;

                case SortOrder.LiveFrom:
                    var sortLiveFrom = ascending ? Builders<Data.DTOs.Article>.Sort.Ascending("LiveFrom") : Builders<Data.DTOs.Article>.Sort.Descending("LiveFrom");

                    query = query.Sort(sortLiveFrom);

                    break;

                case SortOrder.VersionDate:
                    var sortVersionDate = ascending ? Builders<Data.DTOs.Article>.Sort.Ascending("VersionDate") : Builders<Data.DTOs.Article>.Sort.Descending("VersionDate");

                    query = query.Sort(sortVersionDate);

                    break;
                default:
                    break;
            }

            query.Filter = filter & orfilter;

            query = query.Skip(skip).Limit(take);

            var list = query.ToList();

            return list
                .Select(x => hydrate(x, hydrationSettings))
                .ToList();
        }


        public IList<Article> GetUnreadOrderedBy(Brand brand, SortOrder sortOrder, int skip, int? take, string[] tags, List<string> ids,
         PublicationStatus[] showWithStatus = null, bool showOnlyLive = false, bool ascending = false,
         ArticleType? articleType = null, ArticleHydrationSettings hydrationSettings = null, bool allTags = true )
        {
            var query = GenerateBasicQuery(brand,showWithStatus, true, articleType);

            var filter = query.Filter;

            filter = filter & Builders<Data.DTOs.Article>.Filter.Eq("Status", PublicationStatus.Published);
           
            

            var orfilter = Builders<Data.DTOs.Article>.Filter.Empty;

           

            if (tags.Any())
                {
                if (allTags)
                    {
                    filter = tags.Aggregate(filter,
                        (current, tag) => current & Builders<Data.DTOs.Article>.Filter.Eq("Tags", tag));
                    }
                else
                    {
                    foreach (var tag in tags)
                        {
                        if (orfilter == FilterDefinition<Data.DTOs.Article>.Empty)
                            orfilter = Builders<Data.DTOs.Article>.Filter.Eq("Tags", tag);
                        else
                            orfilter = orfilter | Builders<Data.DTOs.Article>.Filter.Eq("Tags", tag);
                        }
                    }
                }




            if (ids.Any())
            {
                filter = filter & Builders<Data.DTOs.Article>.Filter.Nin("_id", ids);
            }
            //query = query.Where(x => tags.Contains(x.Tags));

            switch (sortOrder)
            {
                case SortOrder.CreatedOn:

                    var sortCreatedOn = ascending ? Builders<Data.DTOs.Article>.Sort.Ascending("CreatedOn") : Builders<Data.DTOs.Article>.Sort.Descending("CreatedOn");

                    query = query.Sort(sortCreatedOn);
                    break;

                case SortOrder.UpdatedOn:
                    var sortUpdatedOn = ascending ? Builders<Data.DTOs.Article>.Sort.Ascending("UpdatedOn") : Builders<Data.DTOs.Article>.Sort.Descending("UpdatedOn");

                    query = query.Sort(sortUpdatedOn);

                    break;

                case SortOrder.LiveFrom:
                    var sortLiveFrom = ascending ? Builders<Data.DTOs.Article>.Sort.Ascending("LiveFrom") : Builders<Data.DTOs.Article>.Sort.Descending("LiveFrom");

                    query = query.Sort(sortLiveFrom);

                    break;

                case SortOrder.VersionDate:
                    var sortVersionDate = ascending ? Builders<Data.DTOs.Article>.Sort.Ascending("VersionDate") : Builders<Data.DTOs.Article>.Sort.Descending("VersionDate");

                    query = query.Sort(sortVersionDate);

                    break;
                default:
                    break;
            }

            query.Filter = filter & orfilter;

            query = query.Skip(skip).Limit(take);

            var list = query.ToList();

            return list
                .Select(x => hydrate(x, hydrationSettings))
                .ToList();
        }
        



        #region Actionable/Important

        public int GetImportantCount(Brand brand, string[] tags, List<string> ids, PublicationStatus[] showWithStatus = null,
       bool showOnlyLive = false,
       ArticleType? articleType = null, bool allTags = true)
            {

            var query = GenerateBasicQuery(brand,showWithStatus, showOnlyLive, null);
            var filter = query.Filter;
            var orfilter = Builders<Data.DTOs.Article>.Filter.Empty;


            filter = filter & Builders<Data.DTOs.Article>.Filter.Gt("ActionDate", new BsonDateTime(DateTime.UtcNow));


            if (ids.Any())
                {
                filter = filter & Builders<Data.DTOs.Article>.Filter.Nin("_id", ids);
                }

            if (tags.Any())
                {
                if (allTags)
                    {
                    filter = tags.Aggregate(filter,
                        (current, tag) => current & Builders<Data.DTOs.Article>.Filter.Eq("Tags", tag));
                    }
                else
                    {
                    foreach (var tag in tags)
                        {
                        if (orfilter == FilterDefinition<Data.DTOs.Article>.Empty)
                            orfilter = Builders<Data.DTOs.Article>.Filter.Eq("Tags", tag);
                        else
                            orfilter = orfilter | Builders<Data.DTOs.Article>.Filter.Eq("Tags", tag);
                        }
                    }
                }

            orfilter = orfilter | Builders<Data.DTOs.Article>.Filter.Eq("ArticleType", articleType);


            query.Filter = filter & orfilter;
            return int.Parse(query.Count().ToString());
            }
        

        public IList<Article> GetActionableOrderedBy(Brand brand, SortOrder sortOrder, int skip, int take, string[] tags, List<string> ids,
         PublicationStatus[] showWithStatus = null, bool showOnlyLive = false, bool ascending = false,
         ArticleType? articleType = null, ArticleHydrationSettings hydrationSettings = null, bool allTags = true)
        {
            var query = GenerateBasicQuery(brand,showWithStatus, showOnlyLive, null);

            var filter = query.Filter;
            var dt = new BsonDateTime(DateTime.UtcNow);

            //get those with Action date in the future
            filter = filter & Builders<Data.DTOs.Article>.Filter.Gt("ActionDate", new BsonDateTime(DateTime.UtcNow));


            if (ids.Any())
                {
                filter = filter & Builders<Data.DTOs.Article>.Filter.Nin("_id", ids);
                }

            var orfilter = Builders<Data.DTOs.Article>.Filter.Empty;
            if (tags.Any())
            {
                if (allTags)
                {
                    filter = tags.Aggregate(filter,
                        (current, tag) => current & Builders<Data.DTOs.Article>.Filter.Eq("Tags", tag));
                }
                else
                {
                foreach (var tag in tags)
                    {
                    if (orfilter == FilterDefinition<Data.DTOs.Article>.Empty)
                        orfilter = Builders<Data.DTOs.Article>.Filter.Eq("Tags", tag);
                    else
                        orfilter = orfilter | Builders<Data.DTOs.Article>.Filter.Eq("Tags", tag);
                    }
                }
            }

            orfilter = orfilter | Builders<Data.DTOs.Article>.Filter.Eq("ArticleType", articleType);


            switch (sortOrder)
            {
                case SortOrder.CreatedOn:

                    var sortCreatedOn = ascending ? Builders<Data.DTOs.Article>.Sort.Ascending("CreatedOn") : Builders<Data.DTOs.Article>.Sort.Descending("CreatedOn");

                    query = query.Sort(sortCreatedOn);
                    break;

                case SortOrder.UpdatedOn:
                    var sortUpdatedOn = ascending ? Builders<Data.DTOs.Article>.Sort.Ascending("UpdatedOn") : Builders<Data.DTOs.Article>.Sort.Descending("UpdatedOn");

                    query = query.Sort(sortUpdatedOn);

                    break;

                case SortOrder.LiveFrom:
                    var sortLiveFrom = ascending ? Builders<Data.DTOs.Article>.Sort.Ascending("LiveFrom") : Builders<Data.DTOs.Article>.Sort.Descending("LiveFrom");

                    query = query.Sort(sortLiveFrom);

                    break;

                case SortOrder.VersionDate:
                    var sortVersionDate = ascending ? Builders<Data.DTOs.Article>.Sort.Ascending("VersionDate") : Builders<Data.DTOs.Article>.Sort.Descending("VersionDate");

                    query = query.Sort(sortVersionDate);

                    break;
                default:
                    break;
            }

            query.Filter = filter & orfilter ;

            query = query.Skip(skip).Limit(take);

            var list = query.ToList();

            return list
                .Select(x => hydrate(x, hydrationSettings))
                .ToList();
        }


        #endregion

        public int GetCount(Brand brand, string[] tags, PublicationStatus[] showWithStatus = null,
            bool showOnlyLive = false,ArticleType? articleType = null, bool allTags = true)
        {
            
            var query = GenerateBasicQuery(brand,showWithStatus, showOnlyLive, articleType);
            var filter = query.Filter;
            var orfilter = Builders<Data.DTOs.Article>.Filter.Empty;





            if (tags.Any())
                {
                if (allTags)
                    {
                    filter = tags.Aggregate(filter,
                        (current, tag) => current & Builders<Data.DTOs.Article>.Filter.Eq("Tags", tag));
                    }
                else
                    {
                    foreach (var tag in tags)
                        {
                        if (orfilter == FilterDefinition<Data.DTOs.Article>.Empty)
                            orfilter = Builders<Data.DTOs.Article>.Filter.Eq("Tags", tag);
                        else
                            orfilter = orfilter | Builders<Data.DTOs.Article>.Filter.Eq("Tags", tag);
                        }
                    }
                }

            query.Filter = filter & orfilter;
            return int.Parse(query.Count().ToString());
        }



        public int GetCountInDateRange(Brand brand, int daysAgo, string[] tags, List<string> ids,
            Common.Constants.PublicationStatus[] showWithStatus, bool showOnlyLive,
            Common.Constants.ArticleType? articleType, bool allTags = true
            )
        {

        var query = GenerateBasicQuery(brand,showWithStatus, true, articleType);

        var filter = query.Filter;



        filter = filter & Builders<Data.DTOs.Article>.Filter.Eq("Status", PublicationStatus.Published);

            switch (daysAgo)
            {
                case -1:

                    filter = filter &
                             Builders<Data.DTOs.Article>.Filter.Lt("VersionDate", new BsonDateTime(DateTime.UtcNow));
                    filter = filter & Builders<Data.DTOs.Article>.Filter.Gte("VersionDate", new BsonDateTime(DateTime.UtcNow.Date));
                    break;

                case -2:


                    filter = filter &Builders<Data.DTOs.Article>.Filter.Lt("VersionDate",
                                 new BsonDateTime(DateTime.UtcNow.Date));
                    filter = filter & Builders<Data.DTOs.Article>.Filter.Gte("VersionDate", new BsonDateTime(DateTime.UtcNow.AddDays(-1).Date));
                    
                    break;

                default:
                    filter = filter & Builders<Data.DTOs.Article>.Filter.Lt("VersionDate", new BsonDateTime(DateTime.UtcNow));
                    filter = filter & Builders<Data.DTOs.Article>.Filter.Gte("VersionDate", new BsonDateTime(DateTime.UtcNow.AddDays(daysAgo)));
                    break;
            }

            query.Filter = filter;
           



            //filter = filter & Builders<Data.DTOs.Article>.Filter.Gte("LiveFrom", new BsonDateTime(DateTime.UtcNow.AddDays(daysAgo)));
            var orfilter = Builders<Data.DTOs.Article>.Filter.Empty;
        if (tags.Any())
            {
            if (allTags)
                {
                filter = tags.Aggregate(filter,
                    (current, tag) => current & Builders<Data.DTOs.Article>.Filter.Eq("Tags", tag));
                }
            else
                {
                foreach (var tag in tags)
                    {
                    if (orfilter == FilterDefinition<Data.DTOs.Article>.Empty)
                        orfilter = Builders<Data.DTOs.Article>.Filter.Eq("Tags", tag);
                    else
                        orfilter = orfilter | Builders<Data.DTOs.Article>.Filter.Eq("Tags", tag);
                    }
                }
            }

        if (ids.Any())
            {
            filter = filter & Builders<Data.DTOs.Article>.Filter.Nin("_id", ids);
            }

        query.Filter = filter & orfilter;



        return int.Parse(query.Count().ToString());

      

        }


        public int GetUnreadDateRangeCount(Brand brand,string[] tags, List<string> ids,
           Common.Constants.PublicationStatus[] showWithStatus, bool showOnlyLive,
           Common.Constants.ArticleType? articleType, bool allTags = true
           )
            {

            var query = GenerateBasicQuery(brand, showWithStatus, true, articleType);

            var filter = query.Filter;



            filter = filter & Builders<Data.DTOs.Article>.Filter.Eq("Status", PublicationStatus.Published);

            

            query.Filter = filter;


            //filter = filter & Builders<Data.DTOs.Article>.Filter.Gte("LiveFrom", new BsonDateTime(DateTime.UtcNow.AddDays(daysAgo)));
            var orfilter = Builders<Data.DTOs.Article>.Filter.Empty;
            if (tags.Any())
                {
                if (allTags)
                    {
                    filter = tags.Aggregate(filter,
                        (current, tag) => current & Builders<Data.DTOs.Article>.Filter.Eq("Tags", tag));
                    }
                else
                    {
                    foreach (var tag in tags)
                        {
                        if (orfilter == FilterDefinition<Data.DTOs.Article>.Empty)
                            orfilter = Builders<Data.DTOs.Article>.Filter.Eq("Tags", tag);
                        else
                            orfilter = orfilter | Builders<Data.DTOs.Article>.Filter.Eq("Tags", tag);
                        }
                    }
                }

            if (ids.Any())
                {
                filter = filter & Builders<Data.DTOs.Article>.Filter.Nin("_id", ids);
                }

            query.Filter = filter & orfilter;



            return int.Parse(query.Count().ToString());



            }

       





        public int FindByArticleIdCount(string articleId)
            {
            return  _mongoRepository.AsQueryable<Data.DTOs.ArticleStat>()
                    .Where(x => x.ArticleId == articleId)
                    .Select(x => x.UserId)
                    .Distinct()
                    .Count();
            }






        public IList<Data.Model.Article> SearchArticles(Brand brand, string searchText, int take,
            ArticleHydrationSettings hydrationSettings)
            {
            return SearchArticles(brand, searchText, SearchFilter.Phrase, DateTime.UtcNow.AddYears(-1), null, new string[] { }, SortOrder.Relevance, 25, 0, hydrationSettings);
            }




        public IList<Data.Model.Article> SearchArticles(Brand brand, string searchText, SearchFilter searchFilter, DateTime? start, DateTime? end, string[] tags, SortOrder sortOrder, int take, int? skip,
           ArticleHydrationSettings hydrationSettings)
            {
            var query = GenerateBasicQuery(brand, new PublicationStatus[] { PublicationStatus.Published, }, true, null);

            var filter = query.Filter;


            if (start.HasValue)
                filter = filter & Builders<Data.DTOs.Article>.Filter.Gt("LiveFrom", new BsonDateTime(start.Value));

            if (end.HasValue)
                filter = filter & Builders<Data.DTOs.Article>.Filter.Lt("LiveFrom", new BsonDateTime(end.Value));


            



            if (tags.Any())
                filter = tags.Aggregate(filter,
                        (current, tag) => current & Builders<Data.DTOs.Article>.Filter.Eq("Tags", tag));

            if (!string.IsNullOrEmpty(searchText))
            {


                switch (searchFilter)
                {
                    case SearchFilter.And:

                        var splitSearch = searchText.Split(' ').ToArray();
                        searchText = splitSearch.Aggregate(string.Empty,
                            (current, item) => current + ("\"" + item + "\" "));

                        break;

                    case SearchFilter.Phrase:

                        searchText = "\"" + searchText + "\"";

                        break;
                    default:
                        break;
                }
                
                var F = Builders<Data.DTOs.Article>.Filter.Text(searchText, "en");
                query.Filter = filter & F;
            }
            else
                query.Filter = filter;


            var P = Builders<Data.DTOs.Article>.Projection.MetaTextScore("TextMatchScore");
            var S = Builders<Data.DTOs.Article>.Sort.MetaTextScore("TextMatchScore");

            query = query.Project<Data.DTOs.Article>(P);


            switch (sortOrder)
                {

                case SortOrder.Relevance:
                    query = query.Sort(S);
                    break;

                default:
                    query = query.Sort(Builders<Data.DTOs.Article>.Sort.Descending("VersionDate"));
                    break;
                }


            query = query.Skip(skip).Limit(take);

            return query.ToList()
               .Select(x => hydrate(x, hydrationSettings))
               .ToList();
            }


        public int SearchArticlesCount(Brand brand, string searchText, SearchFilter searchFilter, DateTime? start,
            DateTime? end, string[] tags)
        {
            var query = GenerateBasicQuery(brand, new PublicationStatus[] {PublicationStatus.Published,}, true, null);

            var filter = query.Filter;


            if (start.HasValue)
                filter = filter & Builders<Data.DTOs.Article>.Filter.Gt("LiveFrom", new BsonDateTime(start.Value));

            if (end.HasValue)
                filter = filter & Builders<Data.DTOs.Article>.Filter.Lt("LiveFrom", new BsonDateTime(end.Value));

            if (tags.Any())
                filter = tags.Aggregate(filter,
                    (current, tag) => current & Builders<Data.DTOs.Article>.Filter.Eq("Tags", tag));

            if (!string.IsNullOrEmpty(searchText))
            {
                switch (searchFilter)
                {
                    case SearchFilter.And:

                        var splitSearch = searchText.Split(' ').ToArray();
                        searchText = splitSearch.Aggregate(String.Empty,
                            (current, item) => current + ("\"" + item + "\" "));

                        break;

                    case SearchFilter.Phrase:

                        searchText = "\"" + searchText + "\"";

                        break;
                    default:
                        break;
                }

                query.Filter = filter & Builders<Data.DTOs.Article>.Filter.Text(searchText, "en");
            }
            else
                query.Filter = filter;


            return int.Parse(query.Count().ToString());

        }


        public List<BsonDocument> FindRelatedTagsForSearch(Brand brand, string searchText, SearchFilter searchFilter, DateTime? start, DateTime? end, string[] tags)
            {
            var query = GenerateBasicQuery(brand, new PublicationStatus[] { PublicationStatus.Published, }, true, null);

            var filter = query.Filter;


            if (start.HasValue)
                filter = filter & Builders<Data.DTOs.Article>.Filter.Gt("LiveFrom", new BsonDateTime(start.Value));

            if (end.HasValue)
                filter = filter & Builders<Data.DTOs.Article>.Filter.Lt("LiveFrom", new BsonDateTime(end.Value));

            if (tags.Any())
                filter = tags.Aggregate(filter,
                        (current, tag) => current & Builders<Data.DTOs.Article>.Filter.Eq("Tags", tag));

            if (!string.IsNullOrEmpty(searchText))
            {
                switch (searchFilter)
                {
                    case SearchFilter.And:

                        var splitSearch = searchText.Split(' ').ToArray();
                        searchText = splitSearch.Aggregate(String.Empty,
                            (current, item) => current + ("\"" + item + "\" "));

                        break;

                    case SearchFilter.Phrase:

                        searchText = "\"" + searchText + "\"";

                        break;
                    default:
                        break;
                }


                filter = filter & Builders<Data.DTOs.Article>.Filter.Text(searchText, "en");

            }
           

            var collection = _mongoRepository.GetCollection<Data.DTOs.Article>();

            var aggregate = collection.Aggregate()
                .Match(filter)
                .Unwind("RelatedTags")
                //.Project()
                .Group(new BsonDocument { { "_id", new BsonDocument { { "UrlSlug", "$RelatedTags.UrlSlug" }, { "DisplayText", "$RelatedTags.DisplayText" } } }, { "count", new BsonDocument("$sum", 1) } })
                .Sort(new BsonDocument("count", -1));

            return aggregate.ToList();
            }






        public IList<Data.Model.Article> Search(Brand brand,string searchText, int skip, int take, bool searchTitle = true,
           bool searchMeta = false, bool searchBody = false, PublicationStatus[] showWithStatus = null,
           bool showOnlyLive = false, ArticleType? articleType = null,
           ArticleHydrationSettings hydrationSettings = null)
       {
            if (string.IsNullOrWhiteSpace(searchText) || (!searchTitle && !searchMeta && !searchBody))
                return new List<Data.Model.Article>();
            var foundArticles = new List<Data.Model.Article>();
            var searchTerms = searchText.ToLower().Split(' ');
            var sb = new StringBuilder("^");
            foreach (var term in searchTerms.Where(term => !string.IsNullOrEmpty(term)))
            {
                sb.Append(string.Format("(?=.*?({0}))", term));
            }
            sb.Append(".*$");
            var re = new Regex(sb.ToString(), RegexOptions.IgnoreCase | RegexOptions.Multiline);
            var query = GenerateBasicQuery(brand,showWithStatus, showOnlyLive, articleType);

            if (searchTitle)
            {
                var nameQuery = query.ToList()
                    .Where(x => re.IsMatch(x.Title));
                foundArticles.AddRange(nameQuery
                    .Select(x => hydrate(x, hydrationSettings))
                    .ToList());
            }

            // Remove duplicates and limit result count
            foundArticles = foundArticles
                .Distinct(new Data.Model.EqualityComparers.ArticleEqualityComparer())
                //.OrderByDescending(x => x.LiveFrom)
                .Skip(skip)
                .Take(take)
                .ToList();

            return foundArticles;
        }

        public IList<Data.Model.Article> Search(Brand brand,string searchText, bool searchTitle = true, bool searchMeta = false,
            bool searchBody = false, int resultLimit = 10, PublicationStatus[] showWithStatus = null,
            bool showOnlyLive = false, ArticleType? articleType = null,
            ArticleHydrationSettings hydrationSettings = null)
        {
            if (string.IsNullOrWhiteSpace(searchText) || (!searchTitle && !searchMeta && !searchBody))
                return new List<Data.Model.Article>();
            var foundArticles = new List<Data.Model.Article>();
            var searchTerms = searchText.ToLower().Split(' ');
            var sb = new StringBuilder("^");
            foreach (var term in searchTerms.Where(term => !string.IsNullOrEmpty(term)))
            {
                sb.Append(string.Format("(?=.*?({0}))", term));
            }
            sb.Append(".*$");
            var re = new Regex(sb.ToString(), RegexOptions.IgnoreCase | RegexOptions.Multiline);
            var query = GenerateBasicQuery(brand,showWithStatus, showOnlyLive, articleType);
            
                    

            if (searchTitle)
            {
                var nameQuery = query.ToList()
                    .Where(x => re.IsMatch(x.Title));


               
                foundArticles.AddRange(nameQuery
                    .Select(x => hydrate(x, hydrationSettings))
                    .ToList());
            }

            // Remove duplicates and limit result count
            foundArticles = foundArticles
                .Distinct(new ITOK.Core.Data.Model.EqualityComparers.ArticleEqualityComparer())
                //.OrderByDescending(x => x.LiveFrom)
                .Take(resultLimit)
                .ToList();

            return foundArticles;
        }
        public List<Article> FindRelatedTagsForDelete(Brand brand, string tag, PublicationStatus[] showWithStatus, bool showOnlyLive, bool b1)
        {
            var query = GenerateBasicQuery(brand ,showWithStatus, showOnlyLive, null);

            var filter = query.Filter;

            filter = filter & Builders<Data.DTOs.Article>.Filter.Eq("Tags", tag);
           
            query.Filter = filter;

            return query.ToList().Select(x => hydrate(x, new ArticleHydrationSettings() {  RelatedTags = new TagHydrationSettings()})).ToList();
        }
        public List<BsonDocument> FindRelatedTags(Brand brand, string[] tags, PublicationStatus[] showWithStatus, bool showOnlyLive, bool b1, List<string> ids = null )
        {
            var query = GenerateBasicQuery(brand, showWithStatus, showOnlyLive, null);

            var filter = query.Filter;

            if (tags.Any())
            {
                filter = tags.Aggregate(filter, (current, tag) => current & Builders<Data.DTOs.Article>.Filter.Eq("Tags", tag));
            }



            if (ids != null && ids.Any())
                {
                filter = filter & Builders<Data.DTOs.Article>.Filter.Nin("_id", ids);
                }






            var collection = _mongoRepository.GetCollection<Data.DTOs.Article>();

            var aggregate = collection.Aggregate()
                .Match(filter)
                .Unwind("RelatedTags")
                //.Project()
                .Group(new BsonDocument { { "_id", new BsonDocument { { "UrlSlug", "$RelatedTags.UrlSlug" }, { "DisplayText", "$RelatedTags.DisplayText" } } }, { "count", new BsonDocument("$sum", 1) } })
                .Sort(new BsonDocument("count", -1));

            return aggregate.ToList();
        }

        #region Private Methods

        public Data.Model.Article hydrate(Data.DTOs.Article article, ArticleHydrationSettings hydrationSettings)
        {
            if (article == null) return null;
            // If hydration settings arent supplied then use defaults
            if (hydrationSettings == null) hydrationSettings = new ArticleHydrationSettings();

            var viewModel = Mapper.Map<Data.DTOs.Article, Article>(article);

            if (hydrationSettings.PrimaryMedia != null || hydrationSettings.RelatedMedia != null)
            {
                var mediaService = new MediaService(_mongoRepository);
                if (hydrationSettings.PrimaryMedia != null)
                    viewModel.PrimaryMedia =
                        mediaService.FindByIds(hydrationSettings.PrimaryMedia, article.PrimaryMediaId).FirstOrDefault();
                if (hydrationSettings.RelatedMedia != null && article.RelatedMediaIds != null)
                    viewModel.RelatedMedia = mediaService.FindByIds(hydrationSettings.RelatedMedia,
                        article.RelatedMediaIds.ToArray());
            }

            if (hydrationSettings.RelatedArticles != null && article.RelatedArticleIds != null)
                viewModel.RelatedArticles = FindByIds(hydrationSettings.RelatedArticles,
                    article.RelatedArticleIds.ToArray());

            if (hydrationSettings.RelatedGallery != null && article.RelatedGalleryIds != null)
            {
                var mediaServiceG = new MediaService(_mongoRepository);
                viewModel.RelatedGallery = mediaServiceG.FindByIds(hydrationSettings.RelatedGallery,
                        article.RelatedGalleryIds.ToArray());
            }

            if (hydrationSettings.RelatedDownload != null && article.RelatedDownloadIds != null)
            {
                var mediaServiceG = new MediaService(_mongoRepository);
                viewModel.RelatedDownload = mediaServiceG.FindByIds(hydrationSettings.RelatedDownload,
                        article.RelatedDownloadIds.ToArray());
            }

            //if (hydrationSettings.RelatedTags != null && article.RelatedTagIds != null)
            //{
            //    var tagService = new TagsService();
            //    viewModel.RelatedTags = tagService.FindByIds(article.RelatedTagIds.ToArray());
            //}

            if (hydrationSettings.RelatedTags != null)
                {
                var tagService = AppKernel.GetInstance<ITagsService>();
                viewModel.RelatedTags = tagService.FindByUrlSlug(article.Tags.ToArray());
                

                }

            if (hydrationSettings.RelatedJobs != null && article.RelatedJobRoleIds != null)
            {
                var jobService = new JobRoleService(_mongoRepository);
                viewModel.RelatedJobRoles = jobService.FindByIds(hydrationSettings.RelatedJobs, article.RelatedJobRoleIds.ToArray());
            }

            if (hydrationSettings.CreatedBy != null && !string.IsNullOrEmpty(article.CreatedById))
            {
                var userService = new UserService(_mongoRepository);
                viewModel.CreatedBy = userService.FindById(article.CreatedById);
            }
            if (hydrationSettings.UpdatedBy != null && !string.IsNullOrEmpty(article.UpdatedById))
            {
                var userService = new UserService(_mongoRepository);
                viewModel.UpdatedBy = userService.FindById(article.UpdatedById);
            }

            viewModel.LiveFrom = (article.LiveFrom == DateTime.MinValue) ? null : article.LiveFrom as DateTime?;

            if (article.LiveFrom == DateTime.MinValue) viewModel.LiveFrom = null;
            else viewModel.LiveFrom = article.LiveFrom;

            //include viewcount

            viewModel.ViewCount = FindByArticleIdCount(article.Id);
          
            viewModel.FavCount = _favoriteCountService.FindByArticleIdCount(article.Id);

            var readingListCount = _readingListService.ReadingListCount(article.Id);
            viewModel.ReadingListCount = readingListCount ?? 0;

            return viewModel;
        }

        public Data.DTOs.Article deHydrate(Data.Model.Article viewModel)
        {
            var article = Mapper.Map<Data.Model.Article, Data.DTOs.Article>(viewModel);

            article.LiveFrom = viewModel.LiveFrom ?? DateTime.MinValue;

            DateTimeUtilities.ToUtcPreserved(article.LiveFrom);
            DateTimeUtilities.ToUtcPreserved(article.LiveTo);

            return article;
        }

        #endregion Private Methods
    }
}