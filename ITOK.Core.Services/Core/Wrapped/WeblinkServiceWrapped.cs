﻿using AutoMapper;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;

namespace ITOK.Core.Services.Core.Wrapped
{
    public class WeblinkServiceWrapped : ITOK.Core.Services.Core.Wrapped.IWeblinkServiceWrapped
    {
        private readonly IMongoRepository1 _mongoRepository;

        public WeblinkServiceWrapped(IMongoRepository1 mongoRepository)
        {
            _mongoRepository = mongoRepository;
        }

        public IList<Weblink> All()
        {
            var query = GenerateBasicQuery();

            var tags = query.ToList();
            return tags.ToList().Select(hydrate).ToList();
        }

        public IList<Weblink> FindByIds(params string[] ids)
        {
            if (ids == null || ids.Length == 0) return new List<Weblink>();

            var filter = Builders<Data.DTOs.Weblink>.Filter.In("Id", new BsonArray(ids));

            return _mongoRepository.GetCollection<Data.DTOs.Weblink>().Find(filter).ToList()
                .Select(hydrate)
                .OrderBy(x => x.DisplayText)
                .ToList();
        }

        public Weblink FindById(string id)
        {
            var query = GenerateBasicQuery();

            var filter = query.Filter & Builders<Data.DTOs.Weblink>.Filter.Eq("Id", id);

            query.Filter = filter;

            var article = query.FirstOrDefault();

            return hydrate(article);
        }

        public IList<Weblink> FindByBrand(Brand brand)
        {
            var filter =  Builders<Data.DTOs.Weblink>.Filter.Eq("Brand", brand);
            return _mongoRepository.GetCollection<Data.DTOs.Weblink>().Find(filter).ToList()
                .Select(hydrate)
                .OrderBy(x => x.DisplayText)
                .ToList();
        }
              
      
        protected IFindFluent<Data.DTOs.Weblink, Data.DTOs.Weblink> GenerateBasicQuery()
        {
            var filter = Builders<Data.DTOs.Weblink>.Filter.Empty;

            var query = _mongoRepository.GetCollection<Data.DTOs.Weblink>().Find(filter, new FindOptions() { });

            return query;
        }

        public Data.DTOs.Weblink deHydrate(Data.Model.Weblink viewModel)
        {
            var tag = Mapper.Map<Data.Model.Weblink, Data.DTOs.Weblink>(viewModel);
            return tag;
        }

        public Data.Model.Weblink hydrate(Data.DTOs.Weblink tag)
        {
            if (tag == null) return null;
            // If hydration settings arent supplied then use defaults

            var viewModel = Mapper.Map<Data.DTOs.Weblink, Weblink>(tag);


           


            //include viewcount

            return viewModel;
        }
    }
}