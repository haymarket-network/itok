﻿using AutoMapper;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;

namespace ITOK.Core.Services.Core.Wrapped
{
    public class QuestionServiceWrapped : ITOK.Core.Services.Core.Wrapped.IQuestionServiceWrapped
    {
        private readonly IMongoRepository1 _mongoRepository;

        public QuestionServiceWrapped(IMongoRepository1 mongoRepository)
        {
            _mongoRepository = mongoRepository;
        }

        public IList<Question> All()
        {
            var query = GenerateBasicQuery();

            var questions = query.ToList();
            return questions.ToList().Select(hydrate).ToList();
        }

        public IList<Question> FindByIds(params string[] ids)
        {
            if (ids == null || ids.Length == 0) return new List<Question>();

            var filter = Builders<Data.DTOs.Question>.Filter.In("Id", new BsonArray(ids));

            return _mongoRepository.GetCollection<Data.DTOs.Question>().Find(filter).ToList()
                .Select(hydrate)
                .OrderBy(x => x.Order)
                .ToList();
        }
        public IList<Question> FindByParentId(string ids)
        {
            if (ids == null || ids.Length == 0) return new List<Question>();

            var filter = Builders<Data.DTOs.Question>.Filter.Eq("ParentTagId", ids);

            return _mongoRepository.GetCollection<Data.DTOs.Question>().Find(filter).ToList()
                .Select(hydrate)
                .OrderBy(x => x.Order)
                .ToList();
        }

        public List<Data.Model.Question> FindMenuItems(Brand brand)
        {
            var query = GenerateBasicQuery();

            var filter = Builders<Data.DTOs.Question>.Filter.Eq("Brand", brand);
            filter = filter & Builders<Data.DTOs.Question>.Filter.Eq("Show", true);
            query.Filter = filter;

            var questions = query.ToList().Select(hydrate);

            return questions.ToList();
        }

        public Question FindById(string id)
        {
            var query = GenerateBasicQuery();

            var filter = query.Filter & Builders<Data.DTOs.Question>.Filter.Eq("Id", id);

            query.Filter = filter;

            var article = query.FirstOrDefault();

            return hydrate(article);
        }
             

        public List<Data.Model.Question> FindByParentUrlSlug(string[] urlSlugs, Brand brand)
        {
            var query = GenerateBasicQuery();

            var filter = Builders<Data.DTOs.Question>.Filter.In("ParentUrlSlug", new BsonArray(urlSlugs));
            filter = filter & Builders<Data.DTOs.Question>.Filter.Eq("Brand", brand);
            //var filter = query.Filter & Builders<Data.DTOs.Tags>.Filter.Eq("UrlSlug", UrlSlug);

            query.Filter = filter;

            var     questions = query.ToList().Select(hydrate);

            return questions.ToList();
        }

        public Question FindByDisplayText(string displayText, Brand brand)
        {
            var query = GenerateBasicQuery();

            //var filter = query.Filter & Builders<Data.DTOs.Tags>.Filter.Eq("DisplayText", displayText);
            var filter = query.Filter & Builders<Data.DTOs.Question>.Filter.Regex("DisplayText", new BsonRegularExpression("/^" + displayText + "$/i"));
            filter = filter & Builders<Data.DTOs.Question>.Filter.Eq("Brand", brand);
            query.Filter = filter;

            var questions = query.FirstOrDefault();


            return questions == null ? new Question() : hydrate(questions);

            
        }

        protected IFindFluent<Data.DTOs.Question, Data.DTOs.Question> GenerateBasicQuery()
        {
            var filter = Builders<Data.DTOs.Question>.Filter.Empty;

            var query = _mongoRepository.GetCollection<Data.DTOs.Question>().Find(filter, new FindOptions() { });

            return query;
        }

        public Data.DTOs.Question deHydrate(Data.Model.Question viewModel)
        {
            var question = Mapper.Map<Data.Model.Question, Data.DTOs.Question>(viewModel);
            return question;
        }

        public Data.Model.Question hydrate(Data.DTOs.Question question)
        {
            if (question == null) return null;
            // If hydration settings arent supplied then use defaults

            var viewModel = Mapper.Map<Data.DTOs.Question, Question>(question);

            //include viewcount

            return viewModel;
        }
    }
}