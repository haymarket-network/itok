﻿using AutoMapper;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;

namespace ITOK.Core.Services.Core.Wrapped
{
    public class AnswerServiceWrapped : ITOK.Core.Services.Core.Wrapped.IAnswerServiceWrapped
    {
        private readonly IMongoRepository1 _mongoRepository;

        public AnswerServiceWrapped(IMongoRepository1 mongoRepository)
        {
            _mongoRepository = mongoRepository;
        }

        public IList<Answer> All()
        {
            var query = GenerateBasicQuery();

            var answer = query.ToList();
            return answer.Select(hydrate).ToList();
        }

        protected IFindFluent<Data.DTOs.Answer, Data.DTOs.Answer> GenerateBasicQuery()
        {
            var filter = Builders<Data.DTOs.Answer>.Filter.Empty;

            var query = _mongoRepository.GetCollection<Data.DTOs.Answer>().Find(filter, new FindOptions() { });

            return query;
        }

        public Data.DTOs.Answer deHydrate(Data.Model.Answer viewModel)
        {
            var answer = Mapper.Map<Data.Model.Answer, Data.DTOs.Answer>(viewModel);
            return answer;
        }

        public Data.Model.Answer hydrate(Data.DTOs.Answer answer)
        {
            if (answer == null) return null;
            // If hydration settings arent supplied then use defaults

            var viewModel = Mapper.Map<Data.DTOs.Answer, Answer>(answer);

            //include viewcount

            return viewModel;
        }
    }
}