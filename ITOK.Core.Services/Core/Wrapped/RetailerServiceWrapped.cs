﻿using AutoMapper;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;

namespace ITOK.Core.Services.Core.Wrapped
{
    public class RetailerServiceWrapped : ITOK.Core.Services.Core.Wrapped.IRetailerServiceWrapped
    {
        private readonly IMongoRepository1 _mongoRepository;

        public RetailerServiceWrapped(IMongoRepository1 mongoRepository)
        {
            _mongoRepository = mongoRepository;
        }

        public IList<Retailer> All()
        {
            var query = GenerateBasicQuery();

            var tags = query.ToList();
            return tags.ToList().Select(hydrate).ToList();
        }

        public IList<Retailer> FindByIds(params string[] ids)
        {
            if (ids == null || ids.Length == 0) return new List<Retailer>();

            var filter = Builders<Data.DTOs.Retailer>.Filter.In("Id", new BsonArray(ids));

            return _mongoRepository.GetCollection<Data.DTOs.Retailer>().Find(filter).ToList()
                .Select(hydrate)
                .OrderBy(x => x.RetailerName)
                .ToList();
        }

        public Retailer FindById(string id)
        {
            var query = GenerateBasicQuery();

            var filter = query.Filter & Builders<Data.DTOs.Retailer>.Filter.Eq("Id", id);

            query.Filter = filter;

            var article = query.FirstOrDefault();

            return hydrate(article);
        }

        public Retailer FindByRetailerId(string retailerid, Brand brand)
        {
            var query = GenerateBasicQuery();

            var filter = query.Filter & Builders<Data.DTOs.Retailer>.Filter.Eq("RetailerId", retailerid);
            filter = filter & Builders<Data.DTOs.Retailer>.Filter.Eq("Brand", brand);
            query.Filter = filter;

            var article = query.FirstOrDefault();

            return hydrate(article);
        }

        public IList<Retailer> FindByBrand(Brand brand)
        {
            var filter =  Builders<Data.DTOs.Retailer>.Filter.Eq("Brand", brand);
            return _mongoRepository.GetCollection<Data.DTOs.Retailer>().Find(filter).ToList()
                .Select(hydrate)
                .OrderBy(x => x.RetailerName)
                .ToList();
        }
              
      
        protected IFindFluent<Data.DTOs.Retailer, Data.DTOs.Retailer> GenerateBasicQuery()
        {
            var filter = Builders<Data.DTOs.Retailer>.Filter.Empty;

            var query = _mongoRepository.GetCollection<Data.DTOs.Retailer>().Find(filter, new FindOptions() { });

            return query;
        }

        public Data.DTOs.Retailer deHydrate(Data.Model.Retailer viewModel)
        {
            var tag = Mapper.Map<Data.Model.Retailer, Data.DTOs.Retailer>(viewModel);
            return tag;
        }

        public Data.Model.Retailer hydrate(Data.DTOs.Retailer tag)
        {
            if (tag == null) return null;
            // If hydration settings arent supplied then use defaults

            var viewModel = Mapper.Map<Data.DTOs.Retailer, Retailer>(tag);


           


            //include viewcount

            return viewModel;
        }
    }
}