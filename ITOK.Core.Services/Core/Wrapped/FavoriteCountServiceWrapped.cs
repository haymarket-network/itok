﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using MongoDB.Driver;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data;
using ITOK.Core.Data.DTOs;
using FavoriteCount = ITOK.Core.Data.Model.FavoriteCount;

namespace ITOK.Core.Services.Core.Wrapped
{
    public class FavoriteCountServiceWrapped : ITOK.Core.Services.Core.Wrapped.IFavoriteCountServiceWrapped
    {
        private readonly IMongoRepository1 _mongoRepository;

        public FavoriteCountServiceWrapped(IMongoRepository1 mongoRepository)
        {
            _mongoRepository = mongoRepository;
        }

        public IList<FavoriteCount> All()
        {
            var query = GenerateBasicQuery();

            var articles = query.ToList();
            return articles.ToList().Select(x => hydrate(x)).ToList();
        }

        public IList<FavoriteCount> FindByIds(params string[] ids)
        {
            if (ids == null || ids.Length == 0) return new List<FavoriteCount>();
            return _mongoRepository.AsQueryable<Data.DTOs.FavoriteCount>()
                .Where(x => ids.Contains(x.Id))
                .Select(x => hydrate(x))
                .ToList();
        }

        public FavoriteCount FindById(string id)
        {
            var query = GenerateBasicQuery();

            var filter = query.Filter & Builders<Data.DTOs.FavoriteCount>.Filter.Eq("Id", id);

            query.Filter = filter;

            var article = query.FirstOrDefault();

            return hydrate(article);
        }

        public FavoriteCount FindByArticleId(string id)
        {
            var query = GenerateBasicQuery();

            var filter = query.Filter & Builders<Data.DTOs.FavoriteCount>.Filter.Eq("ArticleId", id);

            query.Filter = filter;

            var article = query.FirstOrDefault();

            return hydrate(article);
        }


        public int FindByArticleIdCount(string articleId)
        {
            return
                _mongoRepository.AsQueryable<FavoriteStat>()
                    .Count(x => x.ArticleId == articleId && x.StatisticType == FavoriteStatType.Yes);


        }




        #region Private Methods

        public Data.DTOs.FavoriteCount deHydrate(Data.Model.FavoriteCount count)
        {
            var article = Mapper.Map<Data.Model.FavoriteCount, Data.DTOs.FavoriteCount>(count);
            return article;
        }

        public Data.Model.FavoriteCount hydrate(Data.DTOs.FavoriteCount count)
        {
            if (count == null) return null;
            // If hydration settings arent supplied then use defaults

            var viewModel = Mapper.Map<Data.DTOs.FavoriteCount, Data.Model.FavoriteCount>(count);

            //include viewcount

            return viewModel;
        }

        #endregion Private Methods

        protected IFindFluent<Data.DTOs.FavoriteCount, Data.DTOs.FavoriteCount> GenerateBasicQuery()
        {
            var filter = Builders<Data.DTOs.FavoriteCount>.Filter.Empty;

            var query = _mongoRepository.GetCollection<Data.DTOs.FavoriteCount>().Find(filter, new FindOptions() { });

            return query;
        }
    }
}