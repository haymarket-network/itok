﻿using AutoMapper;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using ITOK.Core.Common.Constants;
using ITOK.Core.Common.Utils;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using ITOK.Core.Data.Model.HS;
using ITOK.Core.Common;

namespace ITOK.Core.Services.Core.Wrapped
{
    public class SubmittedArticleServiceWrapped : ITOK.Core.Services.Core.Wrapped.ISubmittedArticleServiceWrapped
    {
        private IMongoRepository1 _mongoRepository;

        
        private readonly FavoriteCountService _favoriteCountService;
        private readonly ReadingListService _readingListService;

        public SubmittedArticleServiceWrapped(IMongoRepository1 mongoRepository)
        {
            _mongoRepository = mongoRepository;
            _favoriteCountService = new FavoriteCountService(mongoRepository);
            _readingListService = new ReadingListService(mongoRepository);
           
        }

        public IFindFluent<Data.DTOs.SubmittedArticle, Data.DTOs.SubmittedArticle> GenerateBasicQuery(Brand brand,
            SubmissionStatus[] showWithStatus = null,
            bool showOnlyLive = true, SubmissionStatus? submissionStatus = null,  int? skip = null,
            int? take = null)
        {
            var filter = Builders<Data.DTOs.SubmittedArticle>.Filter.Empty;


            filter = filter & filter & Builders<Data.DTOs.SubmittedArticle>.Filter.Eq("brand", brand);


            if (showWithStatus != null)
            {
                var showWithStatusInts = showWithStatus.Select(x => (int) x);

                filter = filter & Builders<Data.DTOs.SubmittedArticle>.Filter.In("Status", showWithStatusInts.ToList());
            }
            if (showOnlyLive)
            {
                var dt = new BsonDateTime(DateTime.UtcNow);

                //this line causes the serialization error
                filter = filter & Builders<Data.DTOs.SubmittedArticle>.Filter.Lt("LiveFrom", new BsonDateTime(DateTime.UtcNow));

                ///todo:filter out archived content NH-130
                filter = filter & Builders<Data.DTOs.SubmittedArticle>.Filter.Gte("LiveTo", new BsonDateTime(DateTime.UtcNow));
                filter = filter & Builders<Data.DTOs.SubmittedArticle>.Filter.Eq("Status", SubmissionStatus.InProgress);
            }

            if (submissionStatus != null)
                filter = filter & Builders<Data.DTOs.SubmittedArticle>.Filter.Eq("Status", (SubmissionStatus)submissionStatus);

            var query = _mongoRepository.GetCollection<Data.DTOs.SubmittedArticle>().Find(filter, new FindOptions() {});

            // query = query.Skip(skip);

            //query = query.Limit(take);

            return query;
        }

        public SubmittedArticle FindBySlug(Brand brand, string urlSlug, SubmittedArticleHydrationSettings hydrationSettings, SubmissionStatus[] showWithStatus = null, bool showOnlyLive = false)
        {
            urlSlug = urlSlug.ToLower();

            var query = GenerateBasicQuery(brand,showWithStatus, showOnlyLive, null);

            var filter = query.Filter & Builders<Data.DTOs.SubmittedArticle>.Filter.Eq("UrlSlug", urlSlug);

            query.Filter = filter;

            var article = query.FirstOrDefault();

            return hydrate(article, hydrationSettings);
        }
        public List<SubmittedArticle> FindAllBySlug(Brand brand,string urlSlug, SubmittedArticleHydrationSettings hydrationSettings, SubmissionStatus[] showWithStatus = null, bool showOnlyLive = false)
        {
            urlSlug = urlSlug.ToLower();

            var query = GenerateBasicQuery(brand,showWithStatus, showOnlyLive, null);

            var filter = query.Filter & Builders<Data.DTOs.SubmittedArticle>.Filter.Eq("UrlSlug", urlSlug);

            query.Filter = filter;

            return query.ToList().Select(x => hydrate(x, hydrationSettings)).ToList();
        }
        public IEnumerable<SubmittedArticle> FindByBrandArticleNumber(Brand brand,string brandarticlenumber, SubmittedArticleHydrationSettings hydrationSettings, SubmissionStatus[] showWithStatus = null, bool showOnlyLive = false)
        {
            // brandarticlenumber = brandarticlenumber.ToLower();

            var query = GenerateBasicQuery(brand ,showWithStatus, showOnlyLive, null);

            var filter = query.Filter & Builders<Data.DTOs.SubmittedArticle>.Filter.Regex("BrandArticleNumber", new BsonRegularExpression("/^" + brandarticlenumber + "$/i"));

            query.Filter = filter;

            return query.ToList().Select(x => hydrate(x, hydrationSettings)).ToList();
        }


        public IEnumerable<SubmittedArticle> FindBySEOTitle(Brand brand,string seoTitle,  SubmittedArticleHydrationSettings hydrationSettings, SubmissionStatus[] showWithStatus = null, bool showOnlyLive = false)
        {
            // brandarticlenumber = brandarticlenumber.ToLower();

            var query = GenerateBasicQuery(brand,showWithStatus, showOnlyLive, null);

            var filter = query.Filter & Builders<Data.DTOs.SubmittedArticle>.Filter.Regex("SEOTitle", new BsonRegularExpression("/^" + seoTitle + "$/i"));
            
            
            //filter = filter & filter & Builders<Data.DTOs.Article>.Filter.Eq("brand", brand);

            query.Filter = filter;

            

            return query.ToList().Select(x => hydrate(x, hydrationSettings)).ToList();
        }


        public IList<SubmittedArticle> FindByIds(  SubmittedArticleHydrationSettings hydrationSettings = null, params string[] ids)
            {
            if (ids == null || !ids.Any()) return new List<SubmittedArticle>();

            var filter = Builders<Data.DTOs.SubmittedArticle>.Filter.In("_id", ids.ToList());
            return _mongoRepository.GetCollection<Data.DTOs.SubmittedArticle>().Find(filter).ToList()
                .Select(x => hydrate(x, hydrationSettings))
                .ToList();
            }

        public IList<SubmittedArticle> FindByIds(Brand brand, SubmittedArticleHydrationSettings hydrationSettings = null, params string[] ids)
        {
            if (ids == null || !ids.Any()) return new List<SubmittedArticle>();

            var filter = Builders<Data.DTOs.SubmittedArticle>.Filter.In("_id", ids.ToList());

            filter =  filter & Builders<Data.DTOs.SubmittedArticle>.Filter.Eq("brand", brand);
            
            var list = _mongoRepository.GetCollection<Data.DTOs.SubmittedArticle>().Find(filter).ToList();
                

            return list
               .Select(x => hydrate(x, hydrationSettings))
               .ToList();

        }

        public IEnumerable<SubmittedArticle> FindByUrlSlugs(Brand brand, SubmittedArticleHydrationSettings hydrationSettings = null, params string[] urlSlugs)
        {
            if (urlSlugs == null || !urlSlugs.Any()) return new List<SubmittedArticle>();



            var filter = Builders<Data.DTOs.SubmittedArticle>.Filter.Eq("brand", brand);
            
            filter = filter & Builders<Data.DTOs.SubmittedArticle>.Filter.In("UrlSlug", new BsonArray(urlSlugs));
           
            return _mongoRepository.GetCollection<Data.DTOs.SubmittedArticle>().Find(filter).ToList()
                 .Select(x => hydrate(x, hydrationSettings))
                 .ToList();
        }





        public IList<SubmittedArticle> GetPagedOrderedBy(Brand brand, SortOrder sortOrder, int skip, int take, string[] tags,
            SubmissionStatus[] showWithStatus = null, bool showOnlyLive = false, bool ascending = false,
            SubmissionStatus? submissionStatus = null, SubmittedArticleHydrationSettings hydrationSettings = null, bool allTags = true, string userID = "")
        {
            var query = GenerateBasicQuery(brand,showWithStatus, showOnlyLive, submissionStatus);

            var filter = query.Filter;

            var orfilter = Builders<Data.DTOs.SubmittedArticle>.Filter.Empty;
            if (!String.IsNullOrEmpty(userID))
            {
                filter = filter & Builders<Data.DTOs.SubmittedArticle>.Filter.Eq("CreatedById", userID);
            }

            if (tags.Any())
                {
                if (allTags)
                    {
                    filter = tags.Aggregate(filter,
                        (current, tag) => current & Builders<Data.DTOs.SubmittedArticle>.Filter.Eq("Tags", tag));




                }
                else
                    {
                    //orfilter = tags.Aggregate(filter,
                    //    (current, tag) => current | Builders<Data.DTOs.SubmittedArticle>.Filter.Eq("Tags", tag));
                    foreach (var tag in tags)
                    {
                        if (orfilter == FilterDefinition<Data.DTOs.SubmittedArticle>.Empty)
                            orfilter = Builders<Data.DTOs.SubmittedArticle>.Filter.Eq("Tags", tag);
                        else
                            orfilter = orfilter | Builders<Data.DTOs.SubmittedArticle>.Filter.Eq("Tags", tag);
                    }
                }
                }
            //query = query.Where(x => tags.Contains(x.Tags));

            switch (sortOrder)
            {
                case SortOrder.CreatedOn:

                    var sortCreatedOn = ascending ? Builders<Data.DTOs.SubmittedArticle>.Sort.Ascending("CreatedOn") : Builders<Data.DTOs.SubmittedArticle>.Sort.Descending("CreatedOn");

                    query = query.Sort(sortCreatedOn);
                    break;

                case SortOrder.UpdatedOn:
                    var sortUpdatedOn = ascending ? Builders<Data.DTOs.SubmittedArticle>.Sort.Ascending("UpdatedOn") : Builders<Data.DTOs.SubmittedArticle>.Sort.Descending("UpdatedOn");

                    query = query.Sort(sortUpdatedOn);

                    break;

                case SortOrder.LiveFrom:
                    var sortLiveFrom = ascending ? Builders<Data.DTOs.SubmittedArticle>.Sort.Ascending("LiveFrom") : Builders<Data.DTOs.SubmittedArticle>.Sort.Descending("LiveFrom");

                    query = query.Sort(sortLiveFrom);

                    break;

                case SortOrder.VersionDate:
                    var sortVersionDate = ascending ? Builders<Data.DTOs.SubmittedArticle>.Sort.Ascending("VersionDate") : Builders<Data.DTOs.SubmittedArticle>.Sort.Descending("VersionDate");

                    query = query.Sort(sortVersionDate);

                    break;
                default:
                    break;
            }

            query.Filter = filter & orfilter ;

            query = query.Skip(skip).Limit(take);

            var list = query.ToList();

            return list
                .Select(x => hydrate(x, hydrationSettings))
                .ToList();
        }



        public IList<SubmittedArticle> GetPagedInDateRangeOrderedBy(Brand brand, SortOrder sortOrder, int daysAgo, int skip, int? take, string[] tags, List<string> ids,
         SubmissionStatus[] showWithStatus = null, bool showOnlyLive = false, bool ascending = false,
         SubmissionStatus? submissionStatus = null, SubmittedArticleHydrationSettings hydrationSettings = null, bool allTags = true )
        {
            var query = GenerateBasicQuery(brand,showWithStatus, true, submissionStatus);

            var filter = query.Filter;

            filter = filter & Builders<Data.DTOs.SubmittedArticle>.Filter.Eq("Status", SubmissionStatus.InProgress);
           
            switch (daysAgo)
                {

                case -1:

                    filter = filter &
                             Builders<Data.DTOs.SubmittedArticle>.Filter.Lt("VersionDate", new BsonDateTime(DateTime.UtcNow));
                    filter = filter & Builders<Data.DTOs.SubmittedArticle>.Filter.Gte("VersionDate", new BsonDateTime(DateTime.UtcNow.Date));
                    break;

                case -2:


                    filter = filter & Builders<Data.DTOs.SubmittedArticle>.Filter.Lt("VersionDate",
                                 new BsonDateTime(DateTime.UtcNow.Date));
                    filter = filter & Builders<Data.DTOs.SubmittedArticle>.Filter.Gte("VersionDate", new BsonDateTime(DateTime.UtcNow.AddDays(-1).Date));

                    break;

                default:
                    filter = filter & Builders<Data.DTOs.SubmittedArticle>.Filter.Lt("VersionDate", new BsonDateTime(DateTime.UtcNow));
                    filter = filter & Builders<Data.DTOs.SubmittedArticle>.Filter.Gte("VersionDate", new BsonDateTime(DateTime.UtcNow.AddDays(daysAgo)));
                    break;
                    
            }

            var orfilter = Builders<Data.DTOs.SubmittedArticle>.Filter.Empty;

           

            if (tags.Any())
                {
                if (allTags)
                    {
                    filter = tags.Aggregate(filter,
                        (current, tag) => current & Builders<Data.DTOs.SubmittedArticle>.Filter.Eq("Tags", tag));
                    }
                else
                    {
                    foreach (var tag in tags)
                        {
                        if (orfilter == FilterDefinition<Data.DTOs.SubmittedArticle>.Empty)
                            orfilter = Builders<Data.DTOs.SubmittedArticle>.Filter.Eq("Tags", tag);
                        else
                            orfilter = orfilter | Builders<Data.DTOs.SubmittedArticle>.Filter.Eq("Tags", tag);
                        }
                    }
                }




            if (ids.Any())
            {
                filter = filter & Builders<Data.DTOs.SubmittedArticle>.Filter.Nin("_id", ids);
            }
            //query = query.Where(x => tags.Contains(x.Tags));

            switch (sortOrder)
            {
                case SortOrder.CreatedOn:

                    var sortCreatedOn = ascending ? Builders<Data.DTOs.SubmittedArticle>.Sort.Ascending("CreatedOn") : Builders<Data.DTOs.SubmittedArticle>.Sort.Descending("CreatedOn");

                    query = query.Sort(sortCreatedOn);
                    break;

                case SortOrder.UpdatedOn:
                    var sortUpdatedOn = ascending ? Builders<Data.DTOs.SubmittedArticle>.Sort.Ascending("UpdatedOn") : Builders<Data.DTOs.SubmittedArticle>.Sort.Descending("UpdatedOn");

                    query = query.Sort(sortUpdatedOn);

                    break;

                case SortOrder.LiveFrom:
                    var sortLiveFrom = ascending ? Builders<Data.DTOs.SubmittedArticle>.Sort.Ascending("LiveFrom") : Builders<Data.DTOs.SubmittedArticle>.Sort.Descending("LiveFrom");

                    query = query.Sort(sortLiveFrom);

                    break;

                case SortOrder.VersionDate:
                    var sortVersionDate = ascending ? Builders<Data.DTOs.SubmittedArticle>.Sort.Ascending("VersionDate") : Builders<Data.DTOs.SubmittedArticle>.Sort.Descending("VersionDate");

                    query = query.Sort(sortVersionDate);

                    break;
                default:
                    break;
            }

            query.Filter = filter & orfilter;

            query = query.Skip(skip).Limit(take);

            var list = query.ToList();

            return list
                .Select(x => hydrate(x, hydrationSettings))
                .ToList();
        }

   

      

        public int GetCount(Brand brand, string[] tags, SubmissionStatus[] showWithStatus = null,
            bool showOnlyLive = false,SubmissionStatus? submissionStatus = null, bool allTags = true, string userID = "")
        {
            
            var query = GenerateBasicQuery(brand,showWithStatus, showOnlyLive, submissionStatus);
            var filter = query.Filter;
            var orfilter = Builders<Data.DTOs.SubmittedArticle>.Filter.Empty;

            if (!String.IsNullOrEmpty(userID))
            {
                filter = filter & Builders<Data.DTOs.SubmittedArticle>.Filter.Eq("CreatedById", userID);
            }



            if (tags.Any())
                {
                if (allTags)
                    {
                    filter = tags.Aggregate(filter,
                        (current, tag) => current & Builders<Data.DTOs.SubmittedArticle>.Filter.Eq("Tags", tag));
                    }
                else
                    {
                    foreach (var tag in tags)
                        {
                        if (orfilter == FilterDefinition<Data.DTOs.SubmittedArticle>.Empty)
                            orfilter = Builders<Data.DTOs.SubmittedArticle>.Filter.Eq("Tags", tag);
                        else
                            orfilter = orfilter | Builders<Data.DTOs.SubmittedArticle>.Filter.Eq("Tags", tag);
                        }
                    }
                }

            query.Filter = filter & orfilter;
            return int.Parse(query.Count().ToString());
        }



        public int GetCountInDateRange(Brand brand, int daysAgo, string[] tags, List<string> ids,
            Common.Constants.SubmissionStatus[] showWithStatus, bool showOnlyLive,
            Common.Constants.SubmissionStatus? submissionStatus, bool allTags = true
            )
        {

        var query = GenerateBasicQuery(brand,showWithStatus, true, submissionStatus);

        var filter = query.Filter;



        filter = filter & Builders<Data.DTOs.SubmittedArticle>.Filter.Eq("Status", SubmissionStatus.InProgress);

            switch (daysAgo)
            {
                case -1:

                    filter = filter &
                             Builders<Data.DTOs.SubmittedArticle>.Filter.Lt("VersionDate", new BsonDateTime(DateTime.UtcNow));
                    filter = filter & Builders<Data.DTOs.SubmittedArticle>.Filter.Gte("VersionDate", new BsonDateTime(DateTime.UtcNow.Date));
                    break;

                case -2:


                    filter = filter &Builders<Data.DTOs.SubmittedArticle>.Filter.Lt("VersionDate",
                                 new BsonDateTime(DateTime.UtcNow.Date));
                    filter = filter & Builders<Data.DTOs.SubmittedArticle>.Filter.Gte("VersionDate", new BsonDateTime(DateTime.UtcNow.AddDays(-1).Date));
                    
                    break;

                default:
                    filter = filter & Builders<Data.DTOs.SubmittedArticle>.Filter.Lt("VersionDate", new BsonDateTime(DateTime.UtcNow));
                    filter = filter & Builders<Data.DTOs.SubmittedArticle>.Filter.Gte("VersionDate", new BsonDateTime(DateTime.UtcNow.AddDays(daysAgo)));
                    break;
            }

            query.Filter = filter;
           



            //filter = filter & Builders<Data.DTOs.Article>.Filter.Gte("LiveFrom", new BsonDateTime(DateTime.UtcNow.AddDays(daysAgo)));
            var orfilter = Builders<Data.DTOs.SubmittedArticle>.Filter.Empty;
        if (tags.Any())
            {
            if (allTags)
                {
                filter = tags.Aggregate(filter,
                    (current, tag) => current & Builders<Data.DTOs.SubmittedArticle>.Filter.Eq("Tags", tag));
                }
            else
                {
                foreach (var tag in tags)
                    {
                    if (orfilter == FilterDefinition<Data.DTOs.SubmittedArticle>.Empty)
                        orfilter = Builders<Data.DTOs.SubmittedArticle>.Filter.Eq("Tags", tag);
                    else
                        orfilter = orfilter | Builders<Data.DTOs.SubmittedArticle>.Filter.Eq("Tags", tag);
                    }
                }
            }

        if (ids.Any())
            {
            filter = filter & Builders<Data.DTOs.SubmittedArticle>.Filter.Nin("_id", ids);
            }

        query.Filter = filter & orfilter;



        return int.Parse(query.Count().ToString());

      

        }
        


        public int FindByArticleIdCount(string articleId)
            {
            return  _mongoRepository.AsQueryable<Data.DTOs.ArticleStat>()
                    .Where(x => x.ArticleId == articleId)
                    .Select(x => x.UserId)
                    .Distinct()
                    .Count();
            }






        public IList<Data.Model.SubmittedArticle> SearchArticles(Brand brand, string searchText, int take,
            SubmittedArticleHydrationSettings hydrationSettings)
            {
            return SearchArticles(brand, searchText, SearchFilter.Phrase, DateTime.UtcNow.AddYears(-1), null, new string[] { }, SortOrder.Relevance, 25, 0, hydrationSettings);
            }




        public IList<Data.Model.SubmittedArticle> SearchArticles(Brand brand, string searchText, SearchFilter searchFilter, DateTime? start, DateTime? end, string[] tags, SortOrder sortOrder, int take, int? skip,
           SubmittedArticleHydrationSettings hydrationSettings)
            {
            var query = GenerateBasicQuery(brand, new SubmissionStatus[] { SubmissionStatus.InProgress, }, true, null);

            var filter = query.Filter;


            if (start.HasValue)
                filter = filter & Builders<Data.DTOs.SubmittedArticle>.Filter.Gt("LiveFrom", new BsonDateTime(start.Value));

            if (end.HasValue)
                filter = filter & Builders<Data.DTOs.SubmittedArticle>.Filter.Lt("LiveFrom", new BsonDateTime(end.Value));


            



            if (tags.Any())
                filter = tags.Aggregate(filter,
                        (current, tag) => current & Builders<Data.DTOs.SubmittedArticle>.Filter.Eq("Tags", tag));

            if (!string.IsNullOrEmpty(searchText))
            {


                switch (searchFilter)
                {
                    case SearchFilter.And:

                        var splitSearch = searchText.Split(' ').ToArray();
                        searchText = splitSearch.Aggregate(string.Empty,
                            (current, item) => current + ("\"" + item + "\" "));

                        break;

                    case SearchFilter.Phrase:

                        searchText = "\"" + searchText + "\"";

                        break;
                    default:
                        break;
                }
                
                var F = Builders<Data.DTOs.SubmittedArticle>.Filter.Text(searchText, "en");
                query.Filter = filter & F;
            }
            else
                query.Filter = filter;


            var P = Builders<Data.DTOs.SubmittedArticle>.Projection.MetaTextScore("TextMatchScore");
            var S = Builders<Data.DTOs.SubmittedArticle>.Sort.MetaTextScore("TextMatchScore");

            query = query.Project<Data.DTOs.SubmittedArticle>(P);


            switch (sortOrder)
                {

                case SortOrder.Relevance:
                    query = query.Sort(S);
                    break;

                default:
                    query = query.Sort(Builders<Data.DTOs.SubmittedArticle>.Sort.Descending("VersionDate"));
                    break;
                }


            query = query.Skip(skip).Limit(take);

            return query.ToList()
               .Select(x => hydrate(x, hydrationSettings))
               .ToList();
            }


        public int SearchArticlesCount(Brand brand, string searchText, SearchFilter searchFilter, DateTime? start,
            DateTime? end, string[] tags)
        {
            var query = GenerateBasicQuery(brand, new SubmissionStatus[] {SubmissionStatus.InProgress,}, true, null);

            var filter = query.Filter;


            if (start.HasValue)
                filter = filter & Builders<Data.DTOs.SubmittedArticle>.Filter.Gt("LiveFrom", new BsonDateTime(start.Value));

            if (end.HasValue)
                filter = filter & Builders<Data.DTOs.SubmittedArticle>.Filter.Lt("LiveFrom", new BsonDateTime(end.Value));

            if (tags.Any())
                filter = tags.Aggregate(filter,
                    (current, tag) => current & Builders<Data.DTOs.SubmittedArticle>.Filter.Eq("Tags", tag));

            if (!string.IsNullOrEmpty(searchText))
            {
                switch (searchFilter)
                {
                    case SearchFilter.And:

                        var splitSearch = searchText.Split(' ').ToArray();
                        searchText = splitSearch.Aggregate(String.Empty,
                            (current, item) => current + ("\"" + item + "\" "));

                        break;

                    case SearchFilter.Phrase:

                        searchText = "\"" + searchText + "\"";

                        break;
                    default:
                        break;
                }

                query.Filter = filter & Builders<Data.DTOs.SubmittedArticle>.Filter.Text(searchText, "en");
            }
            else
                query.Filter = filter;


            return int.Parse(query.Count().ToString());

        }





        public List<SubmittedArticle> FindRelatedTagsForDelete(Brand brand, string tag, SubmissionStatus[] showWithStatus, bool showOnlyLive, bool b1)
        {
            var query = GenerateBasicQuery(brand ,showWithStatus, showOnlyLive, null);

            var filter = query.Filter;

            filter = filter & Builders<Data.DTOs.SubmittedArticle>.Filter.Eq("Tags", tag);
           
            query.Filter = filter;

            return query.ToList().Select(x => hydrate(x, new SubmittedArticleHydrationSettings() {  RelatedTags = new TagHydrationSettings()})).ToList();
        }
        public List<BsonDocument> FindRelatedTags(Brand brand, string[] tags, SubmissionStatus[] showWithStatus, bool showOnlyLive, bool b1, List<string> ids = null )
        {
            var query = GenerateBasicQuery(brand, showWithStatus, showOnlyLive, null);

            var filter = query.Filter;

            if (tags.Any())
            {
                filter = tags.Aggregate(filter, (current, tag) => current & Builders<Data.DTOs.SubmittedArticle>.Filter.Eq("Tags", tag));
            }



            if (ids != null && ids.Any())
                {
                filter = filter & Builders<Data.DTOs.SubmittedArticle>.Filter.Nin("_id", ids);
                }






            var collection = _mongoRepository.GetCollection<Data.DTOs.SubmittedArticle>();

            var aggregate = collection.Aggregate()
                .Match(filter)
                .Unwind("RelatedTags")
                //.Project()
                .Group(new BsonDocument { { "_id", new BsonDocument { { "UrlSlug", "$RelatedTags.UrlSlug" }, { "DisplayText", "$RelatedTags.DisplayText" } } }, { "count", new BsonDocument("$sum", 1) } })
                .Sort(new BsonDocument("count", -1));

            return aggregate.ToList();
        }

        #region Private Methods

        public Data.Model.SubmittedArticle hydrate(Data.DTOs.SubmittedArticle article, SubmittedArticleHydrationSettings hydrationSettings)
        {
            if (article == null) return null;
            // If hydration settings arent supplied then use defaults
            if (hydrationSettings == null) hydrationSettings = new SubmittedArticleHydrationSettings();

            var viewModel = Mapper.Map<Data.DTOs.SubmittedArticle, SubmittedArticle>(article);

            if (hydrationSettings.PrimaryMedia != null || hydrationSettings.RelatedMedia != null)
            {
                var mediaService = new SubmittedMediaService(_mongoRepository);
                if (hydrationSettings.PrimaryMedia != null)
                    viewModel.PrimaryMedia =
                        mediaService.FindByIds(hydrationSettings.PrimaryMedia, article.PrimaryMediaId).FirstOrDefault();
                if (hydrationSettings.RelatedMedia != null && article.RelatedMediaIds != null)
                    viewModel.RelatedMedia = mediaService.FindByIds(hydrationSettings.RelatedMedia,
                        article.RelatedMediaIds.ToArray());
            }

            if (hydrationSettings.RelatedArticles != null && article.RelatedArticleIds != null)
                viewModel.RelatedArticles = FindByIds(hydrationSettings.RelatedArticles,
                    article.RelatedArticleIds.ToArray());

            if (hydrationSettings.RelatedGallery != null && article.RelatedGalleryIds != null)
            {
                var mediaServiceG = new SubmittedMediaService(_mongoRepository);
                viewModel.RelatedGallery = mediaServiceG.FindByIds(hydrationSettings.RelatedGallery,
                        article.RelatedGalleryIds.ToArray());
            }

            if (hydrationSettings.RelatedDownload != null && article.RelatedDownloadIds != null)
            {
                var mediaServiceG = new SubmittedMediaService(_mongoRepository);
                viewModel.RelatedDownload = mediaServiceG.FindByIds(hydrationSettings.RelatedDownload,
                        article.RelatedDownloadIds.ToArray());
            }

            if (hydrationSettings.RelatedTags != null)
                {
                var tagService = AppKernel.GetInstance<ITagsService>();
                viewModel.RelatedTags = tagService.FindByUrlSlug(article.Tags.ToArray());
                

                }

         
            if (hydrationSettings.CreatedBy != null && !string.IsNullOrEmpty(article.CreatedById))
            {
                var userService = new UserService(_mongoRepository);
                viewModel.CreatedBy = userService.FindById(article.CreatedById);
            }
            if (hydrationSettings.UpdatedBy != null && !string.IsNullOrEmpty(article.UpdatedById))
            {
                var userService = new UserService(_mongoRepository);
                viewModel.UpdatedBy = userService.FindById(article.UpdatedById);
            }

            viewModel.LiveFrom = (article.LiveFrom == DateTime.MinValue) ? null : article.LiveFrom as DateTime?;

            if (article.LiveFrom == DateTime.MinValue) viewModel.LiveFrom = null;
            else viewModel.LiveFrom = article.LiveFrom;

            //include viewcount

            viewModel.ViewCount = FindByArticleIdCount(article.Id);
          
            viewModel.FavCount = _favoriteCountService.FindByArticleIdCount(article.Id);

            var readingListCount = _readingListService.ReadingListCount(article.Id);
            viewModel.ReadingListCount = readingListCount ?? 0;

            return viewModel;
        }

        public Data.DTOs.SubmittedArticle deHydrate(Data.Model.SubmittedArticle viewModel)
        {
            var article = Mapper.Map<Data.Model.SubmittedArticle, Data.DTOs.SubmittedArticle>(viewModel);

            article.LiveFrom = viewModel.LiveFrom ?? DateTime.MinValue;

            DateTimeUtilities.ToUtcPreserved(article.LiveFrom);
            DateTimeUtilities.ToUtcPreserved(article.LiveTo);

            return article;
        }

        #endregion Private Methods


        public IList<Data.Model.SubmittedArticle> Search(Brand brand, string searchText, int skip, int take, bool searchTitle = true,
           bool searchMeta = false, bool searchBody = false, SubmissionStatus[] showWithStatus = null,
           bool showOnlyLive = false, SubmissionStatus? submissionStatus = null,
           SubmittedArticleHydrationSettings hydrationSettings = null, string userID = "")
        {
            if (string.IsNullOrWhiteSpace(searchText) || (!searchTitle && !searchMeta && !searchBody))
                return new List<Data.Model.SubmittedArticle>();
            var foundArticles = new List<Data.Model.SubmittedArticle>();
            var searchTerms = searchText.ToLower().Split(' ');
            var sb = new StringBuilder("^");
            foreach (var term in searchTerms.Where(term => !string.IsNullOrEmpty(term)))
            {
                sb.Append(string.Format("(?=.*?({0}))", term));
            }
            sb.Append(".*$");
            var re = new Regex(sb.ToString(), RegexOptions.IgnoreCase | RegexOptions.Multiline);
            var query = GenerateBasicQuery(brand, showWithStatus, showOnlyLive, submissionStatus);

            if (searchTitle)
            {
                var nameQuery = query.ToList()
                    .Where(x => re.IsMatch(x.Title));
                foundArticles.AddRange(nameQuery
                    .Select(x => hydrate(x, hydrationSettings))
                    .ToList());
            }
            if (!string.IsNullOrEmpty(userID))
                foundArticles = foundArticles.Where(i => i.CreatedBy.Id == userID).ToList();
            // Remove duplicates and limit result count
            foundArticles = foundArticles
                .Distinct(new Data.Model.EqualityComparers.SubmittedArticleEqualityComparer())
                //.OrderByDescending(x => x.LiveFrom)
                .Skip(skip)
                .Take(take)
                .ToList();

            return foundArticles;
        }

        public IList<Data.Model.SubmittedArticle> Search(Brand brand, string searchText, bool searchTitle = true, bool searchMeta = false,
            bool searchBody = false, int resultLimit = 10, SubmissionStatus[] showWithStatus = null,
            bool showOnlyLive = false, SubmissionStatus? submissionStatus = null,
            SubmittedArticleHydrationSettings hydrationSettings = null, string userID = "")
        {
            if (string.IsNullOrWhiteSpace(searchText) || (!searchTitle && !searchMeta && !searchBody))
                return new List<Data.Model.SubmittedArticle>();
            var foundArticles = new List<Data.Model.SubmittedArticle>();
            var searchTerms = searchText.ToLower().Split(' ');
            var sb = new StringBuilder("^");
            foreach (var term in searchTerms.Where(term => !string.IsNullOrEmpty(term)))
            {
                sb.Append(string.Format("(?=.*?({0}))", term));
            }
            sb.Append(".*$");
            var re = new Regex(sb.ToString(), RegexOptions.IgnoreCase | RegexOptions.Multiline);
            var query = GenerateBasicQuery(brand, showWithStatus, showOnlyLive, submissionStatus);



            if (searchTitle)
            {
                var nameQuery = query.ToList()
                    .Where(x => re.IsMatch(x.Title));



                foundArticles.AddRange(nameQuery
                    .Select(x => hydrate(x, hydrationSettings))
                    .ToList());
            }
            if (!string.IsNullOrEmpty(userID))
                foundArticles = foundArticles.Where(i => i.CreatedBy.Id == userID).ToList();
            // Remove duplicates and limit result count
            foundArticles = foundArticles
                .Distinct(new ITOK.Core.Data.Model.EqualityComparers.SubmittedArticleEqualityComparer())
                //.OrderByDescending(x => x.LiveFrom)
                .Take(resultLimit)
                .ToList();

            return foundArticles;
        }
    }
}