﻿using System;
using AutoMapper;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;

namespace ITOK.Core.Services.Core.Wrapped
{
    public class AlertServiceWrapped : ITOK.Core.Services.Core.Wrapped.IAlertServiceWrapped
    {
        private readonly IMongoRepository1 _mongoRepository;

        public AlertServiceWrapped(IMongoRepository1 mongoRepository)
        {
            _mongoRepository = mongoRepository;
        }

        public IList<Alert> All()
        {
            var query = GenerateBasicQuery();

            var tags = query.ToList();
            return tags.ToList().Select(hydrate).ToList();
        }

        public IList<Alert> FindByIds(params string[] ids)
        {
            if (ids == null || ids.Length == 0) return new List<Alert>();

            var filter = Builders<Data.DTOs.Alert>.Filter.In("Id", new BsonArray(ids));

            return _mongoRepository.GetCollection<Data.DTOs.Alert>().Find(filter).ToList()
                .Select(hydrate)
                .OrderBy(x => x.ExpiryDate)
                .ToList();
        }

        public Alert FindById(string id)
        {
            var query = GenerateBasicQuery();

            var filter = query.Filter & Builders<Data.DTOs.Alert>.Filter.Eq("Id", id);

            query.Filter = filter;

            var article = query.FirstOrDefault();

            return hydrate(article);
        }

        public IList<Alert> FindByBrand(Brand brand, bool onlyActive = false)
        {
            var filter =  Builders<Data.DTOs.Alert>.Filter.Eq("Brand", brand);

            if (onlyActive)
            filter = filter & Builders<Data.DTOs.Alert>.Filter.Gt("ExpiryDate", new BsonDateTime(DateTime.UtcNow));


            return _mongoRepository.GetCollection<Data.DTOs.Alert>().Find(filter).ToList()
                .Select(hydrate)
                .OrderBy(x => x.ExpiryDate)
                .ToList();
        }
              
      
        protected IFindFluent<Data.DTOs.Alert, Data.DTOs.Alert> GenerateBasicQuery()
        {
            var filter = Builders<Data.DTOs.Alert>.Filter.Empty;

            var query = _mongoRepository.GetCollection<Data.DTOs.Alert>().Find(filter, new FindOptions() { });

            return query;
        }

        public Data.DTOs.Alert deHydrate(Data.Model.Alert viewModel)
        {
            var tag = Mapper.Map<Data.Model.Alert, Data.DTOs.Alert>(viewModel);
            return tag;
        }

        public Data.Model.Alert hydrate(Data.DTOs.Alert tag)
        {
            if (tag == null) return null;
            // If hydration settings arent supplied then use defaults

            var viewModel = Mapper.Map<Data.DTOs.Alert, Alert>(tag);


           


            //include viewcount

            return viewModel;
        }
    }
}