﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using MongoDB.Driver;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data;
using ITOK.Core.Data.DTOs;
using FavoriteCount = ITOK.Core.Data.Model.FavoriteCount;

namespace ITOK.Core.Services.Core
{
    public class CertificateRegistrationServiceWrapped : ITOK.Core.Services.Core.Wrapped.ICertificateRegistrationServiceWrapped
    {
        private readonly IMongoRepository1 _mongoRepository;

        public CertificateRegistrationServiceWrapped(IMongoRepository1 mongoRepository)
        {
            _mongoRepository = mongoRepository;
        }

        
    }
}