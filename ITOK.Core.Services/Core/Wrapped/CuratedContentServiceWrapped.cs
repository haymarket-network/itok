﻿using AutoMapper;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using ITOK.Core.Data.Model.HS;
using ITOK.Core.Common;

namespace ITOK.Core.Services.Core.Wrapped
{
    public class CuratedContentServiceWrapped : ITOK.Core.Services.Core.Wrapped.ICuratedContentServiceWrapped
    {
        private IMongoRepository1 _mongoRepository;

        public CuratedContentServiceWrapped(IMongoRepository1 mongoRepository)
        {
            _mongoRepository = mongoRepository;
        }

        public IList<CuratedContent> All(CuratedContentHydrationSettings settings)
        {
            var query = GenerateBasicQuery();

            var _tagTranslation = query.ToList();
            return _tagTranslation.ToList().Select(x => hydrate(x, settings)).ToList();
        }

        public CuratedContent FindById(string id, CuratedContentHydrationSettings settings)
        {
            var query = GenerateBasicQuery();

            var filter = query.Filter & Builders<Data.DTOs.CuratedContent>.Filter.Eq("Id", id);

            query.Filter = filter;

            var curatedContent = query.FirstOrDefault();

            return hydrate(curatedContent, settings);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="JobRole">sales, aftersales, technical. The tag slug</param>
        /// <returns></returns>
        public List<Data.Model.CuratedContent> FindByBrandAndRole(Brand brand, string JobRole, CuratedContentHydrationSettings settings)
        {
            var query = GenerateBasicQuery();

            var filter = Builders<Data.DTOs.CuratedContent>.Filter.Eq("Brand", brand);
            filter = filter & Builders<Data.DTOs.CuratedContent>.Filter.Eq("PrimaryJobRole", JobRole);

            query.Filter = filter;

            var curatedContent = query.ToList().Select(x => hydrate(x, settings));

            return curatedContent.ToList();
        }


        protected IFindFluent<Data.DTOs.CuratedContent, Data.DTOs.CuratedContent> GenerateBasicQuery()
        {
            var filter = Builders<Data.DTOs.CuratedContent>.Filter.Empty;

            var query = _mongoRepository.GetCollection<Data.DTOs.CuratedContent>().Find(filter, new FindOptions() { });

            return query;
        }

        public Data.DTOs.CuratedContent deHydrate(Data.Model.CuratedContent viewModel)
        {
        var curatedContent = Mapper.Map<Data.Model.CuratedContent, Data.DTOs.CuratedContent>(viewModel);
        return curatedContent;
        }

        public Data.Model.CuratedContent hydrate(Data.DTOs.CuratedContent curatedContent, CuratedContentHydrationSettings hydrationSettings)
        {
            if (curatedContent == null) return null;
            // If hydration settings arent supplied then use defaults

            var viewModel = Mapper.Map<Data.DTOs.CuratedContent, CuratedContent>(curatedContent);


            if (hydrationSettings.Article == null || curatedContent.ArticleId == null) return viewModel;
            var articleService = new ArticleService(_mongoRepository);

            viewModel.Article = articleService.FindById(curatedContent.Brand, curatedContent.ArticleId, hydrationSettings.Article);

            var tagService = AppKernel.GetInstance<ITagsService>();

            viewModel.roleTag = tagService.FindById(curatedContent.PrimaryJobRole);


            return viewModel;
        }
    }
}