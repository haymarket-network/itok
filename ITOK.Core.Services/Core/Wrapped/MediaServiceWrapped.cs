﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using ITOK.Core.Common.Constants;
using ITOK.Core.Common.Utils;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using ITOK.Core.Data.Model.HS;

namespace ITOK.Core.Services.Core.Wrapped
{
    public class MediaServiceWrapped : ITOK.Core.Services.Core.Wrapped.IMediaServiceWrapped
    {
        private IMongoRepository1 _mongoRepository;

        public MediaServiceWrapped(IMongoRepository1 mongoRepository)
        {
            _mongoRepository = mongoRepository;
        }

        public IList<Data.Model.Media> GetPagedOrderedByCreatedOn(int skip, int take, FileFormatGroup? showFromFileFormatGroup = null, PublicationStatus[] showWithStatus = null, bool showOnlyLive = false, bool ascending = true, bool? showOnlyMultiMedia = null, MediaHydrationSettings hydrationSettings = null, Brand? brand = null)
        {
            var query = GenerateBasicQuery(showWithStatus, showOnlyLive, showOnlyMultiMedia, brand);

            if (showFromFileFormatGroup != null)
            {
                var showFileFormats = FileUtilities.FileFormatsFromGroup(showFromFileFormatGroup.Value);
                query = query.Where(x => showFileFormats.Contains(x.Format));
            }
            

            query = ascending ? query.OrderBy(x => x.CreatedOn) : query.OrderByDescending(x => x.CreatedOn);

            query = query.Skip(skip).Take(take);

            return query.ToList()
                .Select(x => hydrate(x, hydrationSettings))
                .ToList();
        }
        public IList<Data.Model.Media> GetPagedOrderedByCreatedOnForUser(int skip, int take, string CurrentUserId, FileFormatGroup? showFromFileFormatGroup = null, PublicationStatus[] showWithStatus = null, bool showOnlyLive = false, bool ascending = true, bool? showOnlyMultiMedia = null, MediaHydrationSettings hydrationSettings = null, Brand? brand = null)
        {
            var query = GenerateBasicQuery(showWithStatus, showOnlyLive, showOnlyMultiMedia, brand);

            query = query.Where(i => i.CreatedById == CurrentUserId);
            if (showFromFileFormatGroup != null)
            {
                var showFileFormats = FileUtilities.FileFormatsFromGroup(showFromFileFormatGroup.Value);
                query = query.Where(x => showFileFormats.Contains(x.Format));
            }


            query = ascending ? query.OrderBy(x => x.CreatedOn) : query.OrderByDescending(x => x.CreatedOn);

            query = query.Skip(skip).Take(take);

            return query.ToList()
                .Select(x => hydrate(x, hydrationSettings))
                .ToList();
        }
        public int GetPagedCount(FileFormatGroup? showFromFileFormatGroup = null, PublicationStatus[] showWithStatus = null, bool showOnlyLive = false, bool? showOnlyMultiMedia = null, Brand? brand = null)
        {
            var query = GenerateBasicQuery(showWithStatus, showOnlyLive, showOnlyMultiMedia, brand);

            if (!showFromFileFormatGroup.HasValue) return query.Count();
            var showFileFormats = FileUtilities.FileFormatsFromGroup(showFromFileFormatGroup.Value);
            query = query.Where(x => showFileFormats.Contains(x.Format));
            return query.Count();
        }

        public IList<Media> GetForDay(DateTime dayDate, int take, Brand? brand)
        {
            var startDateTime = new DateTime(dayDate.Year, dayDate.Month, dayDate.Day, 0, 0, 0);
            var endDateTime = new DateTime(dayDate.Year, dayDate.Month, dayDate.Day, 23, 59, 59);
            var query = GenerateBasicQuery(new[] { PublicationStatus.Published }, true, true, brand);

            query = query.Where(x =>
                x.LiveFrom >= startDateTime &&
                x.LiveFrom <= endDateTime);

            return query
                .Take(take)
                .Select(x => hydrate(x, null))
                .ToList();
        }

        #region Private Methods

        protected Media hydrate(Data.DTOs.Media media, MediaHydrationSettings hydrationSettings)
        {
            if (media == null) return null;
            // If hydration settings arent supplied then use defaults
            if (hydrationSettings == null) hydrationSettings = new MediaHydrationSettings();

            var viewModel = Mapper.Map<Data.DTOs.Media, Media>(media);

            //if (hydrationSettings.RelatedArticles != null)
            //    viewModel.RelatedArticles = FindRelatedArticles(hydrationSettings.RelatedArticles, media.Id);

            if (hydrationSettings.CreatedBy != null && !string.IsNullOrEmpty(media.CreatedById))
            {
                var userService = new UserService(_mongoRepository);
                viewModel.CreatedBy = userService.FindById(media.CreatedById);
            }
            if (hydrationSettings.UpdatedBy != null && !string.IsNullOrEmpty(media.UpdatedById))
            {
                var userService = new UserService(_mongoRepository);
                viewModel.UpdatedBy = userService.FindById(media.UpdatedById);
            }
            return viewModel;
        }

        public Data.DTOs.Media deHydrate(Media viewModel)
        {
            return Mapper.Map<Media, Data.DTOs.Media>(viewModel);
        }

        private IQueryable<Data.DTOs.Media> GenerateBasicQuery(IEnumerable<PublicationStatus> showWithStatus,
            bool showOnlyLive, bool? showOnlyMultiMedia, Brand? brand)
        {
            var query = _mongoRepository.AsQueryable<Data.DTOs.Media>();

            if (showWithStatus != null)
                query = query.Where(x => showWithStatus.Contains(x.Status));


            if (brand!=null)
                query = query.Where(x => x.Brand == brand.Value);
            

            if (showOnlyMultiMedia.HasValue)
                query = query.Where(x => x.ShowInMedia == showOnlyMultiMedia.Value);

            if (showOnlyLive)
                query = query.Where(x => x.LiveFrom < DateTime.UtcNow &&
                                         (x.LiveTo == null ||
                                          x.LiveTo > DateTime.UtcNow));

            return query;
        }

        // Helper methods for resolving related entities
        //private IList<Article> FindRelatedArticles(ArticleHydrationSettings hydrationSettings, string mediaId)
        //    {
        //    //var articles = new List<Model.Article>(_articleService.FindWithPrimaryMedia(hydrationSettings.RelatedArticles, mediaId));
        //    //articles.AddRange(_articleService.FindWithRelatedMedia(hydrationSettings, mediaId));
        //    var articles = _articleService.FindWithRelatedMedia(hydrationSettings, mediaId);
        //    return articles
        //        .Distinct(new ArticleEqualityComparer())
        //        .ToList();
        //    }

        #endregion Private Methods
    }
}