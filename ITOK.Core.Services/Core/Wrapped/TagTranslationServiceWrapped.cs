﻿using AutoMapper;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;

namespace ITOK.Core.Services.Core.Wrapped
{
    public class TagTranslationServiceWrapped : ITOK.Core.Services.Core.Wrapped.ITagTranslationServiceWrapped
    {
        private readonly IMongoRepository1 _mongoRepository;

        public TagTranslationServiceWrapped(IMongoRepository1 mongoRepository)
        {
            _mongoRepository = mongoRepository;
        }

        public IList<TagTranslation> All()
        {
            var query = GenerateBasicQuery();

            var _tagTranslation = query.ToList();
            return _tagTranslation.ToList().Select(hydrate).ToList();
        }

        public IList<TagTranslation> FindByIds(params string[] ids)
        {
            if (ids == null || ids.Length == 0) return new List<TagTranslation>();

            var filter = Builders<Data.DTOs.TagTranslation>.Filter.In("Id", new BsonArray(ids));

            return _mongoRepository.GetCollection<Data.DTOs.TagTranslation>().Find(filter).ToList()
                .Select(hydrate)
                .OrderByDescending(x => x.OriginalTag)
                .ToList();
        }

        public TagTranslation FindById(string id)
        {
            var query = GenerateBasicQuery();

            var filter = query.Filter & Builders<Data.DTOs.TagTranslation>.Filter.Eq("Id", id);

            query.Filter = filter;

            var article = query.FirstOrDefault();

            return hydrate(article);
        }

        public TagTranslation FindByUrlSlug(string UrlSlug, Brand brand)
        {
            var query = GenerateBasicQuery();

            var filter = query.Filter & Builders<Data.DTOs.TagTranslation>.Filter.Eq("OriginalSlug", UrlSlug);
            filter = filter & Builders<Data.DTOs.TagTranslation>.Filter.Eq("Brand", brand);
            query.Filter = filter;

            var tag = query.FirstOrDefault();

            return hydrate(tag);
        }

        public List<Data.Model.TagTranslation> FindByUrlSlug(string[] urlSlugs, Brand brand)
        {
            var query = GenerateBasicQuery();

            var filter = Builders<Data.DTOs.TagTranslation>.Filter.In("OriginalSlug", new BsonArray(urlSlugs));
            filter = filter & Builders<Data.DTOs.TagTranslation>.Filter.Eq("Brand", brand);
            //var filter = query.Filter & Builders<Data.DTOs.TagTranslation>.Filter.Eq("UrlSlug", UrlSlug);

            query.Filter = filter;

            var TagTranslation = query.ToList().Select(hydrate);

            return TagTranslation.ToList();
        }

        public TagTranslation FindByOriginalTag(string displayText, Brand brand)
        {
            var query = GenerateBasicQuery();

            //var filter = query.Filter & Builders<Data.DTOs.TagTranslation>.Filter.Eq("DisplayText", displayText);
            var filter = query.Filter & Builders<Data.DTOs.TagTranslation>.Filter.Regex("OriginalTag", new BsonRegularExpression("/^" + displayText + "$/i"));
            filter = filter & Builders<Data.DTOs.TagTranslation>.Filter.Eq("Brand", brand);
            query.Filter = filter;

            var article = query.FirstOrDefault();


            return article == null ? new TagTranslation() : hydrate(article);

            //return MongoRepository.AsQueryable<Data.DTOs.TagTranslation>()
            //    .Select(x => hydrate(x)).FirstOrDefault(x => x.DisplayText == displayText);
        }

        protected IFindFluent<Data.DTOs.TagTranslation, Data.DTOs.TagTranslation> GenerateBasicQuery()
        {
            var filter = Builders<Data.DTOs.TagTranslation>.Filter.Empty;

            var query = _mongoRepository.GetCollection<Data.DTOs.TagTranslation>().Find(filter, new FindOptions() { });

            return query;
        }

        public Data.DTOs.TagTranslation deHydrate(Data.Model.TagTranslation viewModel)
        {
            var article = Mapper.Map<Data.Model.TagTranslation, Data.DTOs.TagTranslation>(viewModel);
            return article;
        }

        public Data.Model.TagTranslation hydrate(Data.DTOs.TagTranslation tag)
        {
            if (tag == null) return null;
            // If hydration settings arent supplied then use defaults

            var viewModel = Mapper.Map<Data.DTOs.TagTranslation, TagTranslation>(tag);

            //include viewcount

            return viewModel;
        }
    }
}