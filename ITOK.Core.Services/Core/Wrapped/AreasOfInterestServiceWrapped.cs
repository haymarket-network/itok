﻿using AutoMapper;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;

namespace ITOK.Core.Services.Core.Wrapped
{
    public class AreasOfInterestServiceWrapped : ITOK.Core.Services.Core.Wrapped.IAreasOfInterestServiceWrapped
    {
        private IMongoRepository1 _mongoRepository;

        public AreasOfInterestServiceWrapped(IMongoRepository1 mongoRepository)
        {
            _mongoRepository = mongoRepository;
        }

        public IList<AreasOfInterest> All()
        {
            var query = GenerateBasicQuery();

            var _tagTranslation = query.ToList();
            return _tagTranslation.ToList().Select(x => hydrate(x)).ToList();
        }

        public IList<AreasOfInterest> FindByIds(params string[] ids)
        {
            if (ids == null || ids.Length == 0) return new List<AreasOfInterest>();

            var filter = Builders<Data.DTOs.AreasOfInterest>.Filter.In("Id", new BsonArray(ids));

            return _mongoRepository.GetCollection<Data.DTOs.AreasOfInterest>().Find(filter).ToList()
                .Select(x => hydrate(x))
                .OrderByDescending(x => x.InterestTag)
                .ToList();
        }

        public AreasOfInterest FindById(string id)
        {
            var query = GenerateBasicQuery();

            var filter = query.Filter & Builders<Data.DTOs.AreasOfInterest>.Filter.Eq("Id", id);

            query.Filter = filter;

            var article = query.FirstOrDefault();

            return hydrate(article);
        }

        public AreasOfInterest FindByUrlSlug(string UrlSlug)
        {
            var query = GenerateBasicQuery();

            var filter = query.Filter & Builders<Data.DTOs.AreasOfInterest>.Filter.Eq("InterestTag", UrlSlug);

            query.Filter = filter;

            var tag = query.FirstOrDefault();

            return hydrate(tag);
        }

        public List<Data.Model.AreasOfInterest> FindByParent(string urlSlug)
        {
            var query = GenerateBasicQuery();

            var filter = Builders<Data.DTOs.AreasOfInterest>.Filter.Eq("ParentSlug", urlSlug);

         
            query.Filter = filter;

            var TagTranslation = query.ToList().Select(x => hydrate(x));

            return TagTranslation.ToList();
        }


        protected IFindFluent<Data.DTOs.AreasOfInterest, Data.DTOs.AreasOfInterest> GenerateBasicQuery()
        {
            var filter = Builders<Data.DTOs.AreasOfInterest>.Filter.Empty;

            var query = _mongoRepository.GetCollection<Data.DTOs.AreasOfInterest>().Find(filter, new FindOptions() { });

            return query;
        }

        public Data.DTOs.AreasOfInterest deHydrate(Data.Model.AreasOfInterest viewModel)
        {
            var article = Mapper.Map<Data.Model.AreasOfInterest, Data.DTOs.AreasOfInterest>(viewModel);
            return article;
        }

        public Data.Model.AreasOfInterest hydrate(Data.DTOs.AreasOfInterest tag)
        {
            if (tag == null) return null;
            // If hydration settings arent supplied then use defaults

            var viewModel = Mapper.Map<Data.DTOs.AreasOfInterest, AreasOfInterest>(tag);

            //include viewcount

            return viewModel;
        }
    }
}