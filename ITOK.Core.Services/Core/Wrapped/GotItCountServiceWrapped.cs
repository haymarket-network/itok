﻿using AutoMapper;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;

namespace ITOK.Core.Services.Core.Wrapped
{
    public class GotItCountServiceWrapped : ITOK.Core.Services.Core.Wrapped.IGotItCountServiceWrapped
    {
        private IMongoRepository1 _mongoRepository;

        public GotItCountServiceWrapped(IMongoRepository1 mongoRepository)
        {
            _mongoRepository = mongoRepository;
        }

        public IList<GotItCount> All()
        {
            var query = GenerateBasicQuery();

            var articles = query.ToList();
            return articles.ToList().Select(x => hydrate(x)).ToList();
        }

        public IList<GotItCount> FindByIds(params string[] ids)
        {
            if (ids == null || ids.Length == 0) return new List<GotItCount>();
            return _mongoRepository.AsQueryable<Data.DTOs.GotItCount>()
                .Where(x => ids.Contains(x.Id))
                .Select(x => hydrate(x))
                .ToList();
        }

        public GotItCount FindById(string id)
        {
            var query = GenerateBasicQuery();

            var filter = query.Filter & Builders<Data.DTOs.GotItCount>.Filter.Eq("Id", id);

            query.Filter = filter;

            var article = query.FirstOrDefault();

            return hydrate(article);
        }

        public GotItCount FindByArticleId(string articleId)
        {
            var query = GenerateBasicQuery();

            var filter = query.Filter & Builders<Data.DTOs.GotItCount>.Filter.Eq("ArticleId", articleId);

            query.Filter = filter;

            var article = query.FirstOrDefault();

            return hydrate(article);
        }

        #region Private Methods

        public Data.DTOs.GotItCount deHydrate(Data.Model.GotItCount count)
        {
            var article = Mapper.Map<Data.Model.GotItCount, Data.DTOs.GotItCount>(count);
            return article;
        }

        public Data.Model.GotItCount hydrate(Data.DTOs.GotItCount count)
        {
            if (count == null) return null;
            // If hydration settings arent supplied then use defaults

            var viewModel = Mapper.Map<Data.DTOs.GotItCount, Data.Model.GotItCount>(count);

            //include viewcount

            return viewModel;
        }

        #endregion Private Methods

        protected IFindFluent<Data.DTOs.GotItCount, Data.DTOs.GotItCount> GenerateBasicQuery()
        {
            var filter = Builders<Data.DTOs.GotItCount>.Filter.Empty;

            var query = _mongoRepository.GetCollection<Data.DTOs.GotItCount>().Find(filter, new FindOptions() { });

            return query;
        }
    }
}