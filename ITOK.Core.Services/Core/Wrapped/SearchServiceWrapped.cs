﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using MongoDB.Driver;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data;
using ITOK.Core.Data.DTOs;
using FavoriteCount = ITOK.Core.Data.Model.FavoriteCount;

namespace ITOK.Core.Services.Core.Wrapped
{
    public class SearchServiceWrapped : ITOK.Core.Services.Core.Wrapped.ISearchServiceWrapped
    {
        private readonly IMongoRepository1 _mongoRepository;

        public SearchServiceWrapped(IMongoRepository1 mongoRepository)
        {
            _mongoRepository = mongoRepository;
        }

        
    }
}