﻿using AutoMapper;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;

namespace ITOK.Core.Services.Core.Wrapped
{
    public class TagsServiceWrapped : ITOK.Core.Services.Core.Wrapped.ITagsServiceWrapped
    {
        private readonly IMongoRepository1 _mongoRepository;

        public TagsServiceWrapped(IMongoRepository1 mongoRepository)
        {
            _mongoRepository = mongoRepository;
        }

        public IList<Tags> All()
        {
            var query = GenerateBasicQuery();

            var tags = query.ToList();
            return tags.Select(hydrate).ToList();
        }

        public IList<Tags> FindByIds(params string[] ids)
        {
            if (ids == null || ids.Length == 0) return new List<Tags>();

            var filter = Builders<Data.DTOs.Tags>.Filter.In("Id", new BsonArray(ids));

            return _mongoRepository.GetCollection<Data.DTOs.Tags>().Find(filter).ToList()
                .Select(hydrate)
                .OrderByDescending(x => x.DisplayText)
                .ToList();
        }

        public IList<Tags> FindByParentId(TagType tagtype, string id)
        {
            var query = GenerateBasicQuery();
            var filter = Builders<Data.DTOs.Tags>.Filter.Eq("TagType", tagtype);
            filter = filter & Builders<Data.DTOs.Tags>.Filter.Eq("ParentId", id);
            query.Filter = filter;
            var tgs = query.ToList();
            var tags = query.ToList().Select(hydrate);
            return tags.ToList();
        }
        public IList<Tags> FindByTypeAndParent(TagType tagType, params string[] ids)
        {
            if (ids == null || ids.Length == 0) return new List<Tags>();
            var query = GenerateBasicQuery();
            var filter = Builders<Data.DTOs.Tags>.Filter.Eq("TagType", tagType);

            filter = filter & Builders<Data.DTOs.Tags>.Filter.In("ParentId", ids.ToList());
            query.Filter = filter;
            var tgs = query.ToList();
            var tags = query.ToList().Select(hydrate);
            return tags.ToList();
        }

        public List<Data.Model.Tags> FindMenuItems(Brand brand)
        {
            var query = GenerateBasicQuery();

            var filter = Builders<Data.DTOs.Tags>.Filter.Eq("Brand", brand);
            filter = filter & Builders<Data.DTOs.Tags>.Filter.Eq("Show", true);
            query.Filter = filter;

            var tags = query.ToList().Select(hydrate);

            return tags.ToList();
        }

        public Tags FindById(string id)
        {
            var query = GenerateBasicQuery();

            var filter = query.Filter & Builders<Data.DTOs.Tags>.Filter.Eq("Id", id);

            query.Filter = filter;

            var article = query.FirstOrDefault();

            return hydrate(article);
        }

        public Tags FindByUrlSlug(string urlSlug)
        {
            var query = GenerateBasicQuery();

            // var filter = query.Filter & Builders<Data.DTOs.Tags>.Filter.Eq("UrlSlug", UrlSlug);
            var filter = query.Filter & Builders<Data.DTOs.Tags>.Filter.Regex("UrlSlug", new BsonRegularExpression("/^" + urlSlug + "$/i"));
            //filter = filter & Builders<Data.DTOs.Tags>.Filter.Eq("Brand", brand);
            query.Filter = filter;

            var tag = query.FirstOrDefault();

            return hydrate(tag);
        }

        public List<Data.Model.Tags> FindByUrlSlug(string[] urlSlugs)
        {
            var query = GenerateBasicQuery();

            var filter = Builders<Data.DTOs.Tags>.Filter.In("UrlSlug", new BsonArray(urlSlugs));
            //var filter = query.Filter & Builders<Data.DTOs.Tags>.Filter.Eq("UrlSlug", UrlSlug);

            query.Filter = filter;

            var tags = query.ToList().Select(hydrate);

            return tags.ToList();
        }

        public Tags FindByDisplayText(string displayText, Brand brand)
        {
            var query = GenerateBasicQuery();

            //var filter = query.Filter & Builders<Data.DTOs.Tags>.Filter.Eq("DisplayText", displayText);
            var filter = query.Filter & Builders<Data.DTOs.Tags>.Filter.Regex("DisplayText", new BsonRegularExpression("/^" + displayText + "$/i"));
            filter = filter & Builders<Data.DTOs.Tags>.Filter.Eq("Brand", brand);
            query.Filter = filter;

            var tags = query.FirstOrDefault();


            return tags == null ? new Tags() : hydrate(tags);


        }

        protected IFindFluent<Data.DTOs.Tags, Data.DTOs.Tags> GenerateBasicQuery()
        {
            var filter = Builders<Data.DTOs.Tags>.Filter.Empty;

            var query = _mongoRepository.GetCollection<Data.DTOs.Tags>().Find(filter, new FindOptions() { });

            return query;
        }

        public Data.DTOs.Tags deHydrate(Data.Model.Tags viewModel)
        {
            var tag = Mapper.Map<Data.Model.Tags, Data.DTOs.Tags>(viewModel);
            return tag;
        }

        public Data.Model.Tags hydrate(Data.DTOs.Tags tag)
        {
            if (tag == null) return null;
            // If hydration settings arent supplied then use defaults

            var viewModel = Mapper.Map<Data.DTOs.Tags, Tags>(tag);

            //include viewcount

            return viewModel;
        }
    }
}