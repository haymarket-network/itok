﻿using System;
using ITOK.Core.Common.Constants;

namespace ITOK.Core.Services.Core.Wrapped
{
    public interface IMediaServiceWrapped
    {
        ITOK.Core.Data.DTOs.Media deHydrate(ITOK.Core.Data.Model.Media viewModel);

        System.Collections.Generic.IList<ITOK.Core.Data.Model.Media> GetForDay(DateTime dayDate, int take, Brand? brand = null);

        int GetPagedCount(ITOK.Core.Common.Constants.FileFormatGroup? showFromFileFormatGroup = null, ITOK.Core.Common.Constants.PublicationStatus[] showWithStatus = null, bool showOnlyLive = false, bool? showOnlyMultiMedia = null, Brand? brand = null);

        System.Collections.Generic.IList<ITOK.Core.Data.Model.Media> GetPagedOrderedByCreatedOn(int skip, int take, ITOK.Core.Common.Constants.FileFormatGroup? showFromFileFormatGroup = null, ITOK.Core.Common.Constants.PublicationStatus[] showWithStatus = null, bool showOnlyLive = false, bool ascending = true, bool? showOnlyMultiMedia = null, ITOK.Core.Data.Model.HS.MediaHydrationSettings hydrationSettings = null, Brand? brand = null);


        System.Collections.Generic.IList<ITOK.Core.Data.Model.Media> GetPagedOrderedByCreatedOnForUser(int skip, int take, string CurrentUserId, ITOK.Core.Common.Constants.FileFormatGroup? showFromFileFormatGroup = null, ITOK.Core.Common.Constants.PublicationStatus[] showWithStatus = null, bool showOnlyLive = false, bool ascending = true, bool? showOnlyMultiMedia = null, ITOK.Core.Data.Model.HS.MediaHydrationSettings hydrationSettings = null, Brand? brand = null);

    }
}