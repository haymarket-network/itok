﻿using ITOK.Core.Common.Constants;

namespace ITOK.Core.Services.Core.Wrapped
{
    public interface IWeblinkServiceWrapped
    {
        System.Collections.Generic.IList<ITOK.Core.Data.Model.Weblink> All();

        ITOK.Core.Data.DTOs.Weblink deHydrate(ITOK.Core.Data.Model.Weblink viewModel);

        System.Collections.Generic.IList<ITOK.Core.Data.Model.Weblink> FindByBrand(Brand brand);

        ITOK.Core.Data.Model.Weblink FindById(string id);

        System.Collections.Generic.IList<ITOK.Core.Data.Model.Weblink> FindByIds(params string[] ids);

        ITOK.Core.Data.Model.Weblink hydrate(ITOK.Core.Data.DTOs.Weblink tag);
    }
}