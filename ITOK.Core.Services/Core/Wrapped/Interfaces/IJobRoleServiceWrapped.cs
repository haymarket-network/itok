﻿namespace ITOK.Core.Services.Core.Wrapped
{
    public interface IJobRoleServiceWrapped
    {
        System.Collections.Generic.List<ITOK.Core.Data.Model.JobRole> GetAll(ITOK.Core.Data.Model.HS.JobRoleHydrationSettings hydrationSettings = null);

        System.Collections.Generic.List<ITOK.Core.Data.Model.JobRole> GetTopLevel(ITOK.Core.Data.Model.HS.JobRoleHydrationSettings hydrationSettings = null);
    }
}