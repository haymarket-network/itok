﻿using ITOK.Core.Common.Constants;
using ITOK.Core.Data.Model.HS;

namespace ITOK.Core.Services.Core.Wrapped
{
    public interface ICuratedContentServiceWrapped
    {
        System.Collections.Generic.IList<ITOK.Core.Data.Model.CuratedContent> All(CuratedContentHydrationSettings settings);

        ITOK.Core.Data.DTOs.CuratedContent deHydrate(ITOK.Core.Data.Model.CuratedContent viewModel);
       
        ITOK.Core.Data.Model.CuratedContent FindById(string id, CuratedContentHydrationSettings settings);

        System.Collections.Generic.List<ITOK.Core.Data.Model.CuratedContent> FindByBrandAndRole(Brand brand, string UrlSlug, CuratedContentHydrationSettings settings);        

        ITOK.Core.Data.Model.CuratedContent hydrate(ITOK.Core.Data.DTOs.CuratedContent curatedContent, CuratedContentHydrationSettings settings);
    }
}