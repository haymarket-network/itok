﻿using ITOK.Core.Common.Constants;

namespace ITOK.Core.Services.Core.Wrapped
{
    public interface IAnswerServiceWrapped
    {
        System.Collections.Generic.IList<ITOK.Core.Data.Model.Answer> All();

        ITOK.Core.Data.DTOs.Answer deHydrate(ITOK.Core.Data.Model.Answer viewModel);
        ITOK.Core.Data.Model.Answer hydrate(ITOK.Core.Data.DTOs.Answer answer);
    }
}