﻿using ITOK.Core.Common.Constants;

namespace ITOK.Core.Services.Core.Wrapped
{
    public interface IAlertServiceWrapped
    {
        System.Collections.Generic.IList<ITOK.Core.Data.Model.Alert> All();

        ITOK.Core.Data.DTOs.Alert deHydrate(ITOK.Core.Data.Model.Alert viewModel);

        System.Collections.Generic.IList<ITOK.Core.Data.Model.Alert> FindByBrand(Brand brand ,bool onlyActive);

        ITOK.Core.Data.Model.Alert FindById(string id);

        System.Collections.Generic.IList<ITOK.Core.Data.Model.Alert> FindByIds(params string[] ids);

        ITOK.Core.Data.Model.Alert hydrate(ITOK.Core.Data.DTOs.Alert tag);
    }
}