﻿namespace ITOK.Core.Services.Core.Wrapped
{
    public interface IAreasOfInterestServiceWrapped
    {
        System.Collections.Generic.IList<ITOK.Core.Data.Model.AreasOfInterest> All();

        ITOK.Core.Data.DTOs.AreasOfInterest deHydrate(ITOK.Core.Data.Model.AreasOfInterest viewModel);
       
        ITOK.Core.Data.Model.AreasOfInterest FindById(string id);

        System.Collections.Generic.IList<ITOK.Core.Data.Model.AreasOfInterest> FindByIds(params string[] ids);

        ITOK.Core.Data.Model.AreasOfInterest FindByUrlSlug(string UrlSlug);

        System.Collections.Generic.List<ITOK.Core.Data.Model.AreasOfInterest> FindByParent(string urlSlug);

        ITOK.Core.Data.Model.AreasOfInterest hydrate(ITOK.Core.Data.DTOs.AreasOfInterest tag);
    }
}