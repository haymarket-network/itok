﻿namespace ITOK.Core.Services.Core.Wrapped
{
    public interface IFavoriteCountServiceWrapped
    {
        System.Collections.Generic.IList<ITOK.Core.Data.Model.FavoriteCount> All();

        ITOK.Core.Data.DTOs.FavoriteCount deHydrate(ITOK.Core.Data.Model.FavoriteCount count);

        ITOK.Core.Data.Model.FavoriteCount FindByArticleId(string id);

        int FindByArticleIdCount(string articleId);

        ITOK.Core.Data.Model.FavoriteCount FindById(string id);

        System.Collections.Generic.IList<ITOK.Core.Data.Model.FavoriteCount> FindByIds(params string[] ids);

        ITOK.Core.Data.Model.FavoriteCount hydrate(ITOK.Core.Data.DTOs.FavoriteCount count);
    }
}