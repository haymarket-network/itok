﻿using ITOK.Core.Common.Constants;

namespace ITOK.Core.Services.Core.Wrapped
{
    public interface IQuestionServiceWrapped
    {
        System.Collections.Generic.IList<ITOK.Core.Data.Model.Question> All();

        ITOK.Core.Data.DTOs.Question deHydrate(ITOK.Core.Data.Model.Question viewModel);

        ITOK.Core.Data.Model.Question FindByDisplayText(string displayText, Brand brand);

        ITOK.Core.Data.Model.Question FindById(string id);

        System.Collections.Generic.IList<ITOK.Core.Data.Model.Question> FindByIds(params string[] ids);

        System.Collections.Generic.List<ITOK.Core.Data.Model.Question> FindByParentUrlSlug(string[] urlSlugs, Brand brand);
      
        ITOK.Core.Data.Model.Question hydrate(ITOK.Core.Data.DTOs.Question tag);

        System.Collections.Generic.IList<ITOK.Core.Data.Model.Question> FindByParentId(string ids);
    }
}