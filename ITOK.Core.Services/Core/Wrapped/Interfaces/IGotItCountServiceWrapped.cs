﻿namespace ITOK.Core.Services.Core.Wrapped
{
    public interface IGotItCountServiceWrapped
    {
        System.Collections.Generic.IList<ITOK.Core.Data.Model.GotItCount> All();

        ITOK.Core.Data.DTOs.GotItCount deHydrate(ITOK.Core.Data.Model.GotItCount count);

        ITOK.Core.Data.Model.GotItCount FindByArticleId(string articleId);

        ITOK.Core.Data.Model.GotItCount FindById(string id);

        System.Collections.Generic.IList<ITOK.Core.Data.Model.GotItCount> FindByIds(params string[] ids);

        ITOK.Core.Data.Model.GotItCount hydrate(ITOK.Core.Data.DTOs.GotItCount count);
    }
}