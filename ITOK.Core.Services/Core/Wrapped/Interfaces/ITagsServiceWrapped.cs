﻿using ITOK.Core.Common.Constants;

namespace ITOK.Core.Services.Core.Wrapped
{
    public interface ITagsServiceWrapped
    {
        System.Collections.Generic.IList<ITOK.Core.Data.Model.Tags> All();

        ITOK.Core.Data.DTOs.Tags deHydrate(ITOK.Core.Data.Model.Tags viewModel);

        ITOK.Core.Data.Model.Tags FindByDisplayText(string displayText, Brand brand);

        ITOK.Core.Data.Model.Tags FindById(string id);

        System.Collections.Generic.IList<ITOK.Core.Data.Model.Tags> FindByIds(params string[] ids);

        ITOK.Core.Data.Model.Tags FindByUrlSlug(string urlSlug);

        System.Collections.Generic.List<ITOK.Core.Data.Model.Tags> FindByUrlSlug(string[] urlSlugs);
        System.Collections.Generic.List<ITOK.Core.Data.Model.Tags> FindMenuItems(Brand brand);

        ITOK.Core.Data.Model.Tags hydrate(ITOK.Core.Data.DTOs.Tags tag);
        System.Collections.Generic.IList<ITOK.Core.Data.Model.Tags> FindByTypeAndParent(TagType tagType,params string[] ids);
        System.Collections.Generic.IList<ITOK.Core.Data.Model.Tags> FindByParentId(TagType tagtype,string id);
    }
}