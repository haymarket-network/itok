﻿using ITOK.Core.Common.Constants;

namespace ITOK.Core.Services.Core.Wrapped
{
    public interface IRetailerServiceWrapped
    {
        System.Collections.Generic.IList<ITOK.Core.Data.Model.Retailer> All();

        ITOK.Core.Data.DTOs.Retailer deHydrate(ITOK.Core.Data.Model.Retailer viewModel);

        System.Collections.Generic.IList<ITOK.Core.Data.Model.Retailer> FindByBrand(Brand brand);

        ITOK.Core.Data.Model.Retailer FindById(string id);

        ITOK.Core.Data.Model.Retailer FindByRetailerId(string retailerid, Brand brand);

        System.Collections.Generic.IList<ITOK.Core.Data.Model.Retailer> FindByIds(params string[] ids);

        ITOK.Core.Data.Model.Retailer hydrate(ITOK.Core.Data.DTOs.Retailer tag);
    }
}