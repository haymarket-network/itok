﻿using System;
using ITOK.Core.Common.Constants;

namespace ITOK.Core.Services.Core.Wrapped
{
    public interface ISubmittedMediaServiceWrapped
    {
        ITOK.Core.Data.DTOs.SubmittedMedia deHydrate(ITOK.Core.Data.Model.SubmittedMedia viewModel);

         int GetPagedCount(ITOK.Core.Common.Constants.FileFormatGroup? showFromFileFormatGroup = null, ITOK.Core.Common.Constants.SubmissionStatus[] showWithStatus = null, bool showOnlyLive = false, bool? showOnlyMultiMedia = null, Brand? brand = null);

        System.Collections.Generic.IList<ITOK.Core.Data.Model.SubmittedMedia> GetPagedOrderedByCreatedOn(int skip, int take, ITOK.Core.Common.Constants.FileFormatGroup? showFromFileFormatGroup = null, ITOK.Core.Common.Constants.SubmissionStatus[] showWithStatus = null, bool showOnlyLive = false, bool ascending = true, bool? showOnlyMultiMedia = null, ITOK.Core.Data.Model.HS.SubmittedMediaHydrationSettings hydrationSettings = null, Brand? brand = null);
        System.Collections.Generic.IList<ITOK.Core.Data.Model.SubmittedMedia> GetPagedOrderedByCreatedOnForUser(int skip, int take, string CurrentUserId, ITOK.Core.Common.Constants.FileFormatGroup? showFromFileFormatGroup = null, ITOK.Core.Common.Constants.SubmissionStatus[] showWithStatus = null, bool showOnlyLive = false, bool ascending = true, bool? showOnlyMultiMedia = null, ITOK.Core.Data.Model.HS.SubmittedMediaHydrationSettings hydrationSettings = null, Brand? brand = null);

    }
}