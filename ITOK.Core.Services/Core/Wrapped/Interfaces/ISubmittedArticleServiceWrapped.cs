﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data.Model.HS;

namespace ITOK.Core.Services.Core.Wrapped
{
    public interface ISubmittedArticleServiceWrapped
    {
        ITOK.Core.Data.DTOs.SubmittedArticle deHydrate(ITOK.Core.Data.Model.SubmittedArticle viewModel);



        System.Collections.Generic.IList<ITOK.Core.Data.Model.SubmittedArticle> FindByIds(
            ITOK.Core.Data.Model.HS.SubmittedArticleHydrationSettings hydrationSettings = null, params string[] ids);


        System.Collections.Generic.IList<Data.Model.SubmittedArticle> FindByIds(Brand brand, SubmittedArticleHydrationSettings hydrationSettings, string[] ids);


        ITOK.Core.Data.Model.SubmittedArticle FindBySlug(Brand brand, string urlSlug,
            ITOK.Core.Data.Model.HS.SubmittedArticleHydrationSettings hydrationSettings,
            ITOK.Core.Common.Constants.SubmissionStatus[] showWithStatus = null, bool showOnlyLive = false);
        System.Collections.Generic.List<ITOK.Core.Data.Model.SubmittedArticle> FindAllBySlug(Brand brand, string urlSlug,
          ITOK.Core.Data.Model.HS.SubmittedArticleHydrationSettings hydrationSettings,
          ITOK.Core.Common.Constants.SubmissionStatus[] showWithStatus = null, bool showOnlyLive = false);

        System.Collections.Generic.IEnumerable<ITOK.Core.Data.Model.SubmittedArticle> FindByBrandArticleNumber(Brand brand, string urlSlug,
            ITOK.Core.Data.Model.HS.SubmittedArticleHydrationSettings hydrationSettings,
            ITOK.Core.Common.Constants.SubmissionStatus[] showWithStatus = null, bool showOnlyLive = false);

        System.Collections.Generic.IEnumerable<ITOK.Core.Data.Model.SubmittedArticle> FindBySEOTitle( Brand brand,string seoTitle,
         ITOK.Core.Data.Model.HS.SubmittedArticleHydrationSettings hydrationSettings,
         ITOK.Core.Common.Constants.SubmissionStatus[] showWithStatus = null, bool showOnlyLive = false);

        System.Collections.Generic.IEnumerable<ITOK.Core.Data.Model.SubmittedArticle> FindByUrlSlugs(Brand brand,
            ITOK.Core.Data.Model.HS.SubmittedArticleHydrationSettings hydrationSettings = null, params string[] urlSlugs);

        System.Collections.Generic.List<MongoDB.Bson.BsonDocument> FindRelatedTags(Brand brand, string[] tags,
            ITOK.Core.Common.Constants.SubmissionStatus[] showWithStatus, bool showOnlyLive, bool b1,
            List<string> ids = null);

        System.Collections.Generic.List<ITOK.Core.Data.Model.SubmittedArticle> FindRelatedTagsForDelete(Brand brand, string tag,
       ITOK.Core.Common.Constants.SubmissionStatus[] showWithStatus, bool showOnlyLive, bool b1);


        MongoDB.Driver.IFindFluent<ITOK.Core.Data.DTOs.SubmittedArticle, ITOK.Core.Data.DTOs.SubmittedArticle> GenerateBasicQuery(Brand brand,
            ITOK.Core.Common.Constants.SubmissionStatus[] showWithStatus = null, bool showOnlyLive = true,
            ITOK.Core.Common.Constants.SubmissionStatus? submissionStatus = null,  int? skip = null,
            int? take = null);

        int GetCount(Brand brand, string[] tags, ITOK.Core.Common.Constants.SubmissionStatus[] showWithStatus = null,
            bool showOnlyLive = false, ITOK.Core.Common.Constants.SubmissionStatus? submissionStatus = null, bool allTags = true, string userID = "");




        System.Collections.Generic.IList<ITOK.Core.Data.Model.SubmittedArticle> Search(Brand brand, string searchText, bool searchTitle = true,
            bool searchMeta = false, bool searchBody = false, int resultLimit = 10,
            ITOK.Core.Common.Constants.SubmissionStatus[] showWithStatus = null, bool showOnlyLive = false,
            ITOK.Core.Common.Constants.SubmissionStatus? submissionStatus = null,
            ITOK.Core.Data.Model.HS.SubmittedArticleHydrationSettings hydrationSettings = null, string userID = "");

        System.Collections.Generic.IList<ITOK.Core.Data.Model.SubmittedArticle> Search(Brand brand, string searchText, int skip, int take,
            bool searchTitle = true, bool searchMeta = false, bool searchBody = false,
            ITOK.Core.Common.Constants.SubmissionStatus[] showWithStatus = null, bool showOnlyLive = false,
            ITOK.Core.Common.Constants.SubmissionStatus? submissionStatus = null,
            ITOK.Core.Data.Model.HS.SubmittedArticleHydrationSettings hydrationSettings = null, string userID = "");



        int GetCountInDateRange(Brand brand, int daysAgo, string[] tags, List<string> ids,
            Common.Constants.SubmissionStatus[] showWithStatus, bool showOnlyLive,
            Common.Constants.SubmissionStatus? submissionStatus, bool allTags = true);



        System.Collections.Generic.IList<ITOK.Core.Data.Model.SubmittedArticle> GetPagedOrderedBy(Brand brand,
            ITOK.Core.Common.Constants.SortOrder sortOrder, int skip, int take, string[] tags,
            ITOK.Core.Common.Constants.SubmissionStatus[] showWithStatus = null, bool showOnlyLive = false,
            bool ascending = false, ITOK.Core.Common.Constants.SubmissionStatus? submissionStatus = null,
            ITOK.Core.Data.Model.HS.SubmittedArticleHydrationSettings hydrationSettings = null, bool allTags = true, string userID = "");

        System.Collections.Generic.IList<ITOK.Core.Data.Model.SubmittedArticle> GetPagedInDateRangeOrderedBy(Brand brand,
            ITOK.Core.Common.Constants.SortOrder sortOrder, int daysAgo, int skip, int? take, string[] tags, List<string> ids,
            ITOK.Core.Common.Constants.SubmissionStatus[] showWithStatus = null, bool showOnlyLive = false,
            bool ascending = false, ITOK.Core.Common.Constants.SubmissionStatus? submissionStatus = null,
            ITOK.Core.Data.Model.HS.SubmittedArticleHydrationSettings hydrationSettings = null, bool allTags = true);

   
        ITOK.Core.Data.Model.SubmittedArticle hydrate(ITOK.Core.Data.DTOs.SubmittedArticle article,
            ITOK.Core.Data.Model.HS.SubmittedArticleHydrationSettings hydrationSettings);
 
        
        

        IList<Data.Model.SubmittedArticle> SearchArticles(Brand brand, string filter, int pageSize, SubmittedArticleHydrationSettings _hydrationSettings);

        IList<Data.Model.SubmittedArticle> SearchArticles(Brand brand, string searchText, SearchFilter searchFilter, DateTime? start, DateTime? end, string[] tags, SortOrder sortOrder, int take, int? skip,
          SubmittedArticleHydrationSettings hydrationSettings);


        int SearchArticlesCount(Brand brand, string searchText, SearchFilter searchFilter, DateTime? start, DateTime? end, string[] tags);


        
    }
}