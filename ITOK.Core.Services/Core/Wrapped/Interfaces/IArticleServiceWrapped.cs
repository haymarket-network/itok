﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data.Model.HS;

namespace ITOK.Core.Services.Core.Wrapped
{
    public interface IArticleServiceWrapped
    {
        ITOK.Core.Data.DTOs.Article deHydrate(ITOK.Core.Data.Model.Article viewModel);



        System.Collections.Generic.IList<ITOK.Core.Data.Model.Article> FindByIds(
            ITOK.Core.Data.Model.HS.ArticleHydrationSettings hydrationSettings = null, params string[] ids);


        System.Collections.Generic.IList<Data.Model.Article> FindByIds(Brand brand, ArticleHydrationSettings hydrationSettings, string[] ids);


        ITOK.Core.Data.Model.Article FindBySlug(Brand brand, string urlSlug,
            ITOK.Core.Data.Model.HS.ArticleHydrationSettings hydrationSettings,
            ITOK.Core.Common.Constants.PublicationStatus[] showWithStatus = null, bool showOnlyLive = false);
        System.Collections.Generic.List<ITOK.Core.Data.Model.Article> FindAllBySlug(Brand brand, string urlSlug,
          ITOK.Core.Data.Model.HS.ArticleHydrationSettings hydrationSettings,
          ITOK.Core.Common.Constants.PublicationStatus[] showWithStatus = null, bool showOnlyLive = false);

        System.Collections.Generic.IEnumerable<ITOK.Core.Data.Model.Article> FindByBrandArticleNumber(Brand brand, string urlSlug,
            ITOK.Core.Data.Model.HS.ArticleHydrationSettings hydrationSettings,
            ITOK.Core.Common.Constants.PublicationStatus[] showWithStatus = null, bool showOnlyLive = false);

        System.Collections.Generic.IEnumerable<ITOK.Core.Data.Model.Article> FindBySEOTitle( Brand brand,string seoTitle,
         ITOK.Core.Data.Model.HS.ArticleHydrationSettings hydrationSettings,
         ITOK.Core.Common.Constants.PublicationStatus[] showWithStatus = null, bool showOnlyLive = false);

        System.Collections.Generic.IEnumerable<ITOK.Core.Data.Model.Article> FindByUrlSlugs(Brand brand,
            ITOK.Core.Data.Model.HS.ArticleHydrationSettings hydrationSettings = null, params string[] urlSlugs);

        System.Collections.Generic.List<MongoDB.Bson.BsonDocument> FindRelatedTags(Brand brand, string[] tags,
            ITOK.Core.Common.Constants.PublicationStatus[] showWithStatus, bool showOnlyLive, bool b1,
            List<string> ids = null);

        System.Collections.Generic.List<ITOK.Core.Data.Model.Article> FindRelatedTagsForDelete(Brand brand, string tag,
       ITOK.Core.Common.Constants.PublicationStatus[] showWithStatus, bool showOnlyLive, bool b1);


        MongoDB.Driver.IFindFluent<ITOK.Core.Data.DTOs.Article, ITOK.Core.Data.DTOs.Article> GenerateBasicQuery(Brand brand,
            ITOK.Core.Common.Constants.PublicationStatus[] showWithStatus = null, bool showOnlyLive = true,
            ITOK.Core.Common.Constants.ArticleType? articleType = null,  int? skip = null,
            int? take = null);

        int GetCount(Brand brand, string[] tags, ITOK.Core.Common.Constants.PublicationStatus[] showWithStatus = null,
            bool showOnlyLive = false, ITOK.Core.Common.Constants.ArticleType? articleType = null, bool allTags = true);


        int GetImportantCount(Brand brand, string[] tags, List<string> ids, ITOK.Core.Common.Constants.PublicationStatus[] showWithStatus = null,
            bool showOnlyLive = false, ITOK.Core.Common.Constants.ArticleType? articleType = null, bool allTags = true);

        



        int GetCountInDateRange(Brand brand, int daysAgo, string[] tags, List<string> ids,
            Common.Constants.PublicationStatus[] showWithStatus, bool showOnlyLive,
            Common.Constants.ArticleType? articleType, bool allTags = true);


        int GetUnreadDateRangeCount(Brand brand, string[] tags, List<string> ids, PublicationStatus[] showWithStatus, bool showOnlyLive, ArticleType? articleType, bool allTags);

        System.Collections.Generic.IList<ITOK.Core.Data.Model.Article> GetPagedOrderedBy(Brand brand,
            ITOK.Core.Common.Constants.SortOrder sortOrder, int skip, int take, string[] tags,
            ITOK.Core.Common.Constants.PublicationStatus[] showWithStatus = null, bool showOnlyLive = false,
            bool ascending = false, ITOK.Core.Common.Constants.ArticleType? articleType = null,
            ITOK.Core.Data.Model.HS.ArticleHydrationSettings hydrationSettings = null, bool allTags = true);

        System.Collections.Generic.IList<ITOK.Core.Data.Model.Article> GetActionableOrderedBy(Brand brand,
            ITOK.Core.Common.Constants.SortOrder sortOrder, int skip, int take, string[] tags, List<string> ids,
            ITOK.Core.Common.Constants.PublicationStatus[] showWithStatus = null, bool showOnlyLive = false,
            bool ascending = false, ITOK.Core.Common.Constants.ArticleType? articleType = null,
            ITOK.Core.Data.Model.HS.ArticleHydrationSettings hydrationSettings = null, bool allTags = true);

        System.Collections.Generic.IList<ITOK.Core.Data.Model.Article> GetPagedInDateRangeOrderedBy(Brand brand,
            ITOK.Core.Common.Constants.SortOrder sortOrder, int daysAgo, int skip, int? take, string[] tags, List<string> ids,
            ITOK.Core.Common.Constants.PublicationStatus[] showWithStatus = null, bool showOnlyLive = false,
            bool ascending = false, ITOK.Core.Common.Constants.ArticleType? articleType = null,
            ITOK.Core.Data.Model.HS.ArticleHydrationSettings hydrationSettings = null, bool allTags = true);

        System.Collections.Generic.IList<ITOK.Core.Data.Model.Article> GetUnreadOrderedBy(Brand brand,
            ITOK.Core.Common.Constants.SortOrder sortOrder,  int skip, int? take, string[] tags, List<string> ids,
            ITOK.Core.Common.Constants.PublicationStatus[] showWithStatus = null, bool showOnlyLive = false,
            bool ascending = false, ITOK.Core.Common.Constants.ArticleType? articleType = null,
            ITOK.Core.Data.Model.HS.ArticleHydrationSettings hydrationSettings = null, bool allTags = true);


        ITOK.Core.Data.Model.Article hydrate(ITOK.Core.Data.DTOs.Article article,
            ITOK.Core.Data.Model.HS.ArticleHydrationSettings hydrationSettings);

        System.Collections.Generic.IList<ITOK.Core.Data.Model.Article> Search(Brand brand, string searchText, bool searchTitle = true,
            bool searchMeta = false, bool searchBody = false, int resultLimit = 10,
            ITOK.Core.Common.Constants.PublicationStatus[] showWithStatus = null, bool showOnlyLive = false,
            ITOK.Core.Common.Constants.ArticleType? articleType = null,
            ITOK.Core.Data.Model.HS.ArticleHydrationSettings hydrationSettings = null);

        System.Collections.Generic.IList<ITOK.Core.Data.Model.Article> Search(Brand brand, string searchText, int skip, int take,
            bool searchTitle = true, bool searchMeta = false, bool searchBody = false,
            ITOK.Core.Common.Constants.PublicationStatus[] showWithStatus = null, bool showOnlyLive = false,
            ITOK.Core.Common.Constants.ArticleType? articleType = null,
            ITOK.Core.Data.Model.HS.ArticleHydrationSettings hydrationSettings = null);

        IEnumerable<ITOK.Core.Data.Model.Article> FindByUrlSlugsWithArticleStatDetails(Brand brand,
            IList<Data.DTOs.ArticleStat> articleStatList, ArticleHydrationSettings hydrationSettings = null,
            params string[] articleIds);

        //Data.Model.Article SetSeenTimeStamp(Data.Model.Article article, Data.DTOs.ArticleStat stat);




        

        IList<Data.Model.Article> SearchArticles(Brand brand, string filter, int pageSize, ArticleHydrationSettings _hydrationSettings);

        IList<Data.Model.Article> SearchArticles(Brand brand, string searchText, SearchFilter searchFilter, DateTime? start, DateTime? end, string[] tags, SortOrder sortOrder, int take, int? skip,
          ArticleHydrationSettings hydrationSettings);


        int SearchArticlesCount(Brand brand, string searchText, SearchFilter searchFilter, DateTime? start, DateTime? end, string[] tags);

        List<BsonDocument> FindRelatedTagsForSearch(Brand brand, string searchText, SearchFilter searchFilter,
            DateTime? start, DateTime? end, string[] tags);

        
    }
}