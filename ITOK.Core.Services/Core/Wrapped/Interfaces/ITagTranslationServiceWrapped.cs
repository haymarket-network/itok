﻿using ITOK.Core.Common.Constants;

namespace ITOK.Core.Services.Core.Wrapped
{
    public interface ITagTranslationServiceWrapped
    {
        System.Collections.Generic.IList<ITOK.Core.Data.Model.TagTranslation> All();

        ITOK.Core.Data.DTOs.TagTranslation deHydrate(ITOK.Core.Data.Model.TagTranslation viewModel);

        ITOK.Core.Data.Model.TagTranslation FindByOriginalTag(string displayText, Brand brand);

        ITOK.Core.Data.Model.TagTranslation FindById(string id);

        System.Collections.Generic.IList<ITOK.Core.Data.Model.TagTranslation> FindByIds(params string[] ids);

        ITOK.Core.Data.Model.TagTranslation FindByUrlSlug(string UrlSlug, Brand brand);

        System.Collections.Generic.List<ITOK.Core.Data.Model.TagTranslation> FindByUrlSlug(string[] urlSlugs, Brand brand);

        ITOK.Core.Data.Model.TagTranslation hydrate(ITOK.Core.Data.DTOs.TagTranslation tag);
    }
}