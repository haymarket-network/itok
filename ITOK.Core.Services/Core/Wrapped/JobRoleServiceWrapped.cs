﻿using AutoMapper;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using ITOK.Core.Data.Model.HS;

namespace ITOK.Core.Services.Core.Wrapped
{
    public class JobRoleServiceWrapped : ITOK.Core.Services.Core.Wrapped.IJobRoleServiceWrapped
    {
        private IMongoRepository1 _mongoRepository;

        public JobRoleServiceWrapped(IMongoRepository1 mongoRepository)
        {
            _mongoRepository = mongoRepository;
        }

        public List<JobRole> GetAll(JobRoleHydrationSettings hydrationSettings = null)
        {
            var query = GenerateBasicQuery();
            var list = query.ToList();
            return list
                .Select(x => hydrate(x, hydrationSettings))
                .ToList();
        }

        public List<JobRole> GetTopLevel(JobRoleHydrationSettings hydrationSettings = null)
        {
            var query = GenerateBasicQuery();
            var list = query.ToList().Where(i => string.IsNullOrEmpty(i.ParentRoleId));
            return list
                .Select(x => hydrate(x, hydrationSettings))
                .ToList();
        }

        protected IFindFluent<Data.DTOs.JobRole, Data.DTOs.JobRole> GenerateBasicQuery()
        {
            var filter = Builders<Data.DTOs.JobRole>.Filter.Empty;

            var query = _mongoRepository.GetCollection<Data.DTOs.JobRole>().Find(filter, new FindOptions() { });

            return query;
        }

        #region Private Methods

        protected JobRole hydrate(Data.DTOs.JobRole JobRole, JobRoleHydrationSettings hydrationSettings)
        {
            if (JobRole == null) return null;
            // If hydration settings arent supplied then use defaults
            if (hydrationSettings == null) hydrationSettings = new JobRoleHydrationSettings();

            var viewModel = Mapper.Map<Data.DTOs.JobRole, JobRole>(JobRole);

            if (hydrationSettings.CreatedBy != null && !string.IsNullOrEmpty(JobRole.CreatedById))
            {
                var userService = new UserService(_mongoRepository);
                viewModel.CreatedBy = userService.FindById(JobRole.CreatedById);
            }

            if (hydrationSettings.UpdatedBy != null && !string.IsNullOrEmpty(JobRole.UpdatedById))
            {
                var userService = new UserService(_mongoRepository);
                viewModel.UpdatedBy = userService.FindById(JobRole.UpdatedById);
            }

            return viewModel;
        }

        protected Data.DTOs.User deHydrate(User viewModel)
        {
            var User = Mapper.Map<User, Data.DTOs.User>(viewModel);
            return User;
        }

        #endregion Private Methods
    }
}