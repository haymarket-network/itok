﻿using AutoMapper;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using System.Linq;

namespace ITOK.Core.Services.Core
{
    public class QuestionCommentService : IQuestionCommentService
    {
        private IMongoRepository1 _mongoRepository;
        public QuestionCommentService(IMongoRepository1 mongoRepository)
        {
            _mongoRepository = mongoRepository;
        }

        public QuestionComment FindByAnswerId(string answerId)
        {
            var commentDto = _mongoRepository.AsQueryable<ITOK.Core.Data.DTOs.QuestionComment>().FirstOrDefault(x =>
                x.AnswerId == answerId);

            return hydrate(commentDto);
        }

        public QuestionComment FindByCommentId(string commentId)
        {
            var commentDto = _mongoRepository.AsQueryable<ITOK.Core.Data.DTOs.QuestionComment>().FirstOrDefault(x => x.Id == commentId);

            return hydrate(commentDto);
        }

        public void Save(QuestionComment model)
        {
            var dto = deHydrate(model);

            if (string.IsNullOrEmpty(dto.Id))
            {
                _mongoRepository.Insert(dto);
            }
            else
            {
                _mongoRepository.Update(dto);
            }
            
        }

        private QuestionComment hydrate(ITOK.Core.Data.DTOs.QuestionComment question)
        {
            if (question == null) return null;
            var viewModel = Mapper.Map<ITOK.Core.Data.DTOs.QuestionComment, QuestionComment>(question);
            
            return viewModel;
        }

        private ITOK.Core.Data.DTOs.QuestionComment deHydrate(QuestionComment viewModel)
        {
            var question = Mapper.Map<QuestionComment, ITOK.Core.Data.DTOs.QuestionComment>(viewModel);
            return question;
        }
    }

    public interface IQuestionCommentService
    {
        void Save(QuestionComment model);
        QuestionComment FindByAnswerId(string answerId);
        QuestionComment FindByCommentId(string commentId);
    }
}
