﻿using AutoMapper;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using ITOK.Core.Services.Core.Wrapped;

namespace ITOK.Core.Services.Core
{
    public class GotItCountService : BaseService
    {
        private IGotItCountServiceWrapped _gotItCountServiceWrapped;

        public GotItCountService(IMongoRepository1 mongoRepository, IGotItCountServiceWrapped gotItCountServiceWrapped = null)
            : base(mongoRepository)
        {
            if (gotItCountServiceWrapped == null)
                gotItCountServiceWrapped = new GotItCountServiceWrapped(mongoRepository);
            _gotItCountServiceWrapped = gotItCountServiceWrapped;
        }

        public IList<GotItCount> All()
        {
            return _gotItCountServiceWrapped.All();
        }

        public IList<GotItCount> FindByIds(params string[] ids)
        {
            if (ids == null || ids.Length == 0) return new List<GotItCount>();
            return MongoRepository.AsQueryable<Data.DTOs.GotItCount>()
                .Where(x => ids.Contains(x.Id))
                .Select(x => hydrate(x))
                .ToList();
        }

        public GotItCount FindById(string id)
        {
            return _gotItCountServiceWrapped.FindById(id);
        }

        public GotItCount FindByArticleId(string id)
        {
            return _gotItCountServiceWrapped.FindByArticleId(id);
        }

        public Data.DTOs.GotItCount deHydrate(Data.Model.GotItCount count)
        {
            var article = Mapper.Map<Data.Model.GotItCount, Data.DTOs.GotItCount>(count);
            return article;
        }

        public Data.Model.GotItCount hydrate(Data.DTOs.GotItCount count)
        {
            if (count == null) return null;
            // If hydration settings arent supplied then use defaults

            var viewModel = Mapper.Map<Data.DTOs.GotItCount, GotItCount>(count);

            //include viewcount

            return viewModel;
        }

        public void Save(GotItCount count)
        {
            var _count = deHydrate(count);

            if (_count.Id == null) MongoRepository.Insert(_count);
            else MongoRepository.Update(_count);
        }

        public void Insert(GotItCount count)
        {
            var _count = deHydrate(count);
            MongoRepository.Insert(_count);
        }

        public void Delete(string id)
        {
            var filter = Builders<GotItCount>.Filter.Eq("Id", id);
            MongoRepository.GetCollection<GotItCount>().DeleteOne(filter);
        }
    }
}