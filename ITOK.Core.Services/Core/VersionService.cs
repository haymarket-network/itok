﻿using System.Collections.Generic;
using System.Linq;
using ITOK.Core.Common.Interfaces;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using ITOK.Core.Data.Model.HS;

namespace ITOK.Core.Services.Core
{
    internal class VersionService<T> : BaseService where T : class, IVersionable
    {
        public VersionService(IMongoRepository1 mongoRepository)
            : base(mongoRepository)
        {
            _versionMongoRepo = new VersioningMongoRepository<T>();
        }

        private VersioningMongoRepository<T> _versionMongoRepo;

        internal IList<Data.Model.VersionWrapper> GetVersions(int id, VersionHydrationSettings hydrationSettings = null)
        {
            var dtoVersions = _versionMongoRepo.GetVersions(id);
            return hydrate(dtoVersions, hydrationSettings);
        }

        internal IList<VersionWrapper> GetVersions(string id, VersionHydrationSettings hydrationSettings = null)
        {
            var dtoVersions = _versionMongoRepo.GetVersions(id);
            return hydrate(dtoVersions, hydrationSettings);
        }

        internal VersionWrapper GetVersion(string versionId, VersionHydrationSettings hydrationSettings = null)
        {
            var dtoVersion = _versionMongoRepo.GetVersion(versionId);
            return hydrate(dtoVersion, hydrationSettings);
        }

        internal T GetVersionEntity(string versionId)
        {
            var version = _versionMongoRepo.GetVersion(versionId);
            return version != null ? version.Entity : null;
        }

        internal void Revert(string versionId)
        {
            var version = _versionMongoRepo.GetVersion(versionId);

            // Strip out any previous "(reverted from..." messages
            var comment = version.Comment;
            if (!string.IsNullOrEmpty(comment) && comment.Last() == ')' && comment.Contains("(reverted to the version backed-up on "))
            {
                var msgIndex = comment.IndexOf("(reverted to the version backed-up on ");
                comment = comment.Substring(0, comment.Length - (comment.Length - msgIndex));
            }
            MongoRepository.UpdateWithComment(version.Entity,
                string.Format("{0} (reverted to the version backed-up on {1})",
                    comment,
                    version.ModifiedOn.ToString("dd MMM yyyy - H:mm")));
        }

        #region Private/Protected Methods

        private IList<VersionWrapper> hydrate(IList<Data.DTOs.VersionWrapperFor<T>> dtos, VersionHydrationSettings hydrationSettings)
        {
            IList<VersionWrapper> hydratedVersions = new List<VersionWrapper>();
            foreach (var dto in dtos)
            {
                hydratedVersions.Add(hydrate(dto, hydrationSettings));
            }
            return hydratedVersions;
        }

        private Data.Model.VersionWrapper hydrate(Data.DTOs.VersionWrapperFor<T> dto, VersionHydrationSettings hydrationSettings)
        {
            if (hydrationSettings == null) hydrationSettings = new VersionHydrationSettings();

            var version = new VersionWrapper
            {
                Id = dto.Id,
                ModifiedOn = dto.ModifiedOn,
                RecordType = dto.RecordType,
                Comment = dto.Comment
            };

            if (hydrationSettings.ModifiedBy != null && !string.IsNullOrEmpty(dto.ModifiedById))
            {
                var userService = new UserService(MongoRepository);
                version.ModifiedBy = userService.FindById(dto.ModifiedById);
            }
            return version;
        }

        #endregion Private/Protected Methods
    }
}