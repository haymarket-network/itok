﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ITOK.Core.Common;
using ITOK.Core.Data;
using ITOK.Core.Services.Core.Wrapped;

namespace ITOK.Core.Services.Core
{
    public class ReportingService : BaseService
    {
        private readonly StatsService _statService;

        private readonly ITagsService _tagsSerice;

        private readonly FavoriteCountService _favoriteCountService;
        private readonly ReadingListService _readingListService;

        private readonly IArticleServiceWrapped _articleServiceWrapped;

        public ReportingService(IMongoRepository1 mongoRepository, IArticleServiceWrapped articleServiceWrapped = null)
            : base(mongoRepository)
        {
            _statService = new StatsService(mongoRepository);
            _tagsSerice = AppKernel.GetInstance<ITagsService>();
            _favoriteCountService = new FavoriteCountService(mongoRepository);
            _readingListService = new ReadingListService(mongoRepository);
            if (articleServiceWrapped == null)
                articleServiceWrapped = new ArticleServiceWrapped(mongoRepository);
            _articleServiceWrapped = articleServiceWrapped;
        }

    }


}
  
