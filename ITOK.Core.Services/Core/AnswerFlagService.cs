﻿using AutoMapper;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using System.Linq;

namespace ITOK.Core.Services.Core
{
    public class AnswerFlagService : IAnswerFlagService
    {
        private IMongoRepository1 _mongoRepository;
        public AnswerFlagService(IMongoRepository1 mongoRepository)
        {
            _mongoRepository = mongoRepository;
        }

        public AnswerFlag FindByAnswerId(string answerId)
        {
            var flagDto = _mongoRepository.AsQueryable<ITOK.Core.Data.DTOs.AnswerFlag>().FirstOrDefault(x =>
                x.AnswerId == answerId);

            return hydrate(flagDto);
        }

        public AnswerFlag FindByAnswerFlagId(string answerFlagId)
        {
            var flagDto = _mongoRepository.AsQueryable<ITOK.Core.Data.DTOs.AnswerFlag>().FirstOrDefault(x => x.Id == answerFlagId);

            return hydrate(flagDto);
        }

        public void Save(AnswerFlag model)
        {
            var dto = deHydrate(model);

            if (string.IsNullOrEmpty(dto.Id))
            {
                _mongoRepository.Insert(dto);
            }
            else
            {
                _mongoRepository.Update(dto);
            }
        }

        private AnswerFlag hydrate(ITOK.Core.Data.DTOs.AnswerFlag answerFlag)
        {
            if (answerFlag == null) return null;
            var viewModel = Mapper.Map<ITOK.Core.Data.DTOs.AnswerFlag, AnswerFlag>(answerFlag);
            
            return viewModel;
        }

        private ITOK.Core.Data.DTOs.AnswerFlag deHydrate(AnswerFlag viewModel)
        {
            var question = Mapper.Map<AnswerFlag, ITOK.Core.Data.DTOs.AnswerFlag>(viewModel);
            return question;
        }
    }

    public interface IAnswerFlagService
    {
        void Save(AnswerFlag model);
        AnswerFlag FindByAnswerId(string answerId);
        AnswerFlag FindByAnswerFlagId(string answerFlagId);
    }
}
