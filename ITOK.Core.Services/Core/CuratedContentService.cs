﻿using AutoMapper;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using ITOK.Core.Data.Model.EqualityComparers;
using ITOK.Core.Data.Model.HS;
using ITOK.Core.Services.Core.Wrapped;

namespace ITOK.Core.Services.Core
{
    public class CuratedContentService : BaseService
    {
        private readonly ICuratedContentServiceWrapped _curatedContentServiceWrapped;

        public CuratedContentService(IMongoRepository1 mongoRepository, ICuratedContentServiceWrapped curatedContentServiceWrapped = null)
            : base(mongoRepository)
        {
            if (curatedContentServiceWrapped == null)
                curatedContentServiceWrapped = new CuratedContentServiceWrapped(mongoRepository);
            _curatedContentServiceWrapped = curatedContentServiceWrapped;
        }

        public IList<CuratedContent> All(CuratedContentHydrationSettings settings)
        {
            return _curatedContentServiceWrapped.All(settings);
        }
 



        public CuratedContent FindById(string id)
        {
            return _curatedContentServiceWrapped.FindById(id, new CuratedContentHydrationSettings() { Article = new ArticleHydrationSettings() });
        }

        public List<CuratedContent> FindByBrandAndRole(Brand brand, string JobRole, CuratedContentHydrationSettings settings)
        {
        return _curatedContentServiceWrapped.FindByBrandAndRole(brand, JobRole, settings);
        }

    

     

        public Data.DTOs.CuratedContent deHydrate(Data.Model.CuratedContent viewModel)
        {
            var article = Mapper.Map<Data.Model.CuratedContent, Data.DTOs.CuratedContent>(viewModel);
            return article;
        }

        public Data.Model.CuratedContent hydrate(Data.DTOs.CuratedContent tag)
        {
            if (tag == null) return null;
            // If hydration settings arent supplied then use defaults

            var viewModel = Mapper.Map<Data.DTOs.CuratedContent, CuratedContent>(tag);

            //include viewcount

            return viewModel;
        }

        public void Save(CuratedContent area)
        {
            var _tag = deHydrate(area);

           

            //#region Bulk Update Tags in Articles
            //if (previousTag != null)
            //{ 
            ////UPDATES RELATED TAGS
            //var filter = Builders<Article>.Filter.ElemMatch(x => x.RelatedTags, x => x.DisplayText == previousTag.DisplayText);
            //var update = Builders<Article>.Update.Set(x => x.RelatedTags[-1].DisplayText, tag.DisplayText)
            //    .Set(x => x.RelatedTags[-1].UrlSlug, tag.UrlSlug);
           
            //    MongoRepository.GetCollection<Article>().UpdateMany(filter, update);



            ////UPDATES TAGS ARRAY
            //var filterForUrlSlug = Builders<Article>.Filter.AnyIn(x => x.Tags, new[] { previousTag.UrlSlug });
            //var updateForUrlSlugPush = Builders<Article>.Update.Push(x => x.Tags, tag.UrlSlug);
            //var updateForUrlSlugPull = Builders<Article>.Update.Pull(x => x.Tags, previousTag.UrlSlug);


            //    MongoRepository.GetCollection<Article>().UpdateMany(filterForUrlSlug, updateForUrlSlugPush);
            //MongoRepository.GetCollection<Article>().UpdateMany(filterForUrlSlug, updateForUrlSlugPull);

            //}
            //#endregion

            if (area.Id == null) MongoRepository.Insert(_tag);
            else MongoRepository.Update(_tag);
        }

        public void Insert(CuratedContent tag)
        {
            var _tag = deHydrate(tag);
            MongoRepository.Insert(_tag);
        }

        public void Delete(string id)
        {
            var filter = Builders<CuratedContent>.Filter.Eq("_id", id);
            MongoRepository.GetCollection<CuratedContent>().DeleteOne(filter);
        }
    }
}