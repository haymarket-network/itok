﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data;
using ITOK.Core.Data.DTOs;
using ITOK.Core.Data.Model.HS;

namespace ITOK.Core.Services.Core
{
    public class ReadingListService : BaseService
    {
        public ReadingListService(IMongoRepository1 mongoRepository)
           : base(mongoRepository)
        {
        }

      

        public bool Insert(string name, Boolean IsPublic, string userId, string jobRoleId, string retailerId, Brand brand)
        {
            var itemFound = FindReadingListByNameAndUserId(name, userId);

            if (itemFound != null) return false;
            var item = new ReadingList
            {
                UserId = userId,
                JobRoleId = jobRoleId,
                Timestamp = DateTime.UtcNow,
                Name = name,
                RetailerId = retailerId,
                IsPublic = IsPublic,
                brand = brand
            };

            MongoRepository.Insert(item);
            return true;
        }

        public bool Update(string id, string name, Boolean IsPublic, string userId, string jobRoleId, string retailerId, Brand brand)
        {
            var itemFoundWithTheSameName = FindReadingListByNameAndUserId(name, userId);
            if (itemFoundWithTheSameName != null && itemFoundWithTheSameName.Id != id)
                return false;

            var itemFound = FindReadingListById(id);

            if (itemFound == null) return false;
            itemFound.IsPublic = IsPublic;
            itemFound.Name = name;
            itemFound.brand = brand;
            MongoRepository.Update(itemFound);
            return true;
        }

        //public List<BsonDocument> FindReadingList(string jobRoleId, DateTime fitlerTime, int take = 10)
        //{
        //    //var query = GenerateBasicQuery(showWithStatus, showOnlyLive);

        //    //var filter = Builders<Data.DTOs.HubStat>.Filter.Empty;

        //    var filter = Builders<Data.DTOs.ReadingStat>.Filter.Gte("Timestamp", new BsonDateTime(fitlerTime));

        //    if (!string.IsNullOrEmpty(jobRoleId))

        //        filter = filter & Builders<Data.DTOs.ReadingStat>.Filter.Eq("JobRoleId", jobRoleId);

        //    var collection = MongoRepository.GetCollection<Data.DTOs.ReadingStat>();

        //    var aggregate = collection.Aggregate()
        //        .Match(filter)
        //        //.Unwind("RelatedTags")
        //        //.Project()
        //        .Group(new BsonDocument { { "_id", new BsonDocument { { "UrlSlug", "$UrlSlug" } } }, { "count", new BsonDocument("$sum", 1) } })
        //        .Sort(new BsonDocument("count", -1))
        //        .Limit(take);

        //    return aggregate.ToList();

        //}

        public ReadingList FindReadingListById(string Id)
        {
            return MongoRepository
                 .AsQueryable<ReadingList>()
                 .FirstOrDefault(x => x.Id == Id);
        }

        public ReadingList FindReadingListByName(string name)
        {
            return MongoRepository
                 .AsQueryable<ReadingList>()
                 .FirstOrDefault(x => x.Name.ToLower() == name.ToLower());
        }

        public ReadingList FindReadingListByNameAndUserId(string name, string userId)
        {
            return MongoRepository
                 .AsQueryable<ReadingList>()
                  .Where(x => x.Name.ToLower() == name.ToLower())
                 .FirstOrDefault(x => x.UserId == userId);
        }

        public IList<ReadingList> FindReadingListByRetailerId(string RetailerId)
        {
            return MongoRepository.AsQueryable<Data.DTOs.ReadingList>()
                 .Where(x => x.RetailerId == RetailerId).ToList();
        }

        public IList<Data.Model.ReadingList> FindAccessibleReadingLists(string RetailerId, string userId, Brand brand, ReadingListHydrationSettings hs)
        {
            return MongoRepository.AsQueryable<Data.DTOs.ReadingList>()
                 .Where(x => (x.UserId == userId && x.brand == brand))

                 .ToList().Select(x => hydrate(x, hs)).ToList();
        }

        public IList<ReadingList> FindAccessibleAllowAddingArticleToLists(string userId, Brand brand)
        {
            return MongoRepository.AsQueryable<Data.DTOs.ReadingList>()
                 .Where(x => x.UserId == userId && x.brand == brand)

                 .ToList();
        }


        public int? ReadingListCount(string urlSlug)
        {

            return MongoRepository.AsQueryable<ReadingStat>().Count(x => x.ArticleId == urlSlug);
            

        }


        public void Delete(string Id)
        {
            #region Deleting first the reading Stats related to the list
            IList<Data.DTOs.ReadingStat> readingStatsList = MongoRepository.AsQueryable<Data.DTOs.ReadingStat>()
                 .Where(x => x.ReadingListId == Id).ToArray();
            foreach (var readingStat in readingStatsList)
            {
                MongoRepository.Remove<Data.DTOs.ReadingStat>(readingStat, readingStat.Id);
            }
#endregion

            var readingList = MongoRepository
                 .AsQueryable<ReadingList>().FirstOrDefault(x => x.Id == Id);

            if (readingList != null)
                MongoRepository.Remove(readingList, Id);
        }

        #region Private Methods

        protected Data.Model.ReadingList hydrate(Data.DTOs.ReadingList readingList, ReadingListHydrationSettings hydrationSettings)
        {
            if (readingList == null) return null;
            // If hydration settings arent supplied then use defaults
            if (hydrationSettings == null) hydrationSettings = new ReadingListHydrationSettings();

            var viewModel = Mapper.Map<Data.DTOs.ReadingList, Data.Model.ReadingList>(readingList);

            if (hydrationSettings.User != null && !string.IsNullOrEmpty(readingList.UserId))
            {
                var userService = new UserService(base.MongoRepository);
                viewModel.User = userService.FindById(readingList.UserId);
            }

            return viewModel;
        }

        protected Data.DTOs.ReadingList deHydrate(ReadingList viewModel)
        {
            var ReadingList = Mapper.Map<ReadingList, Data.DTOs.ReadingList>(viewModel);
            return ReadingList;
        }

        #endregion Private Methods
    }
}