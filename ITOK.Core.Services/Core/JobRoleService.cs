﻿using AutoMapper;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using ITOK.Core.Data.Model.HS;
using ITOK.Core.Services.Core.Wrapped;

namespace ITOK.Core.Services.Core
{
    public class JobRoleService : BaseService
    {
        private IJobRoleServiceWrapped _jobRoleServiceWrapped;

        public JobRoleService(IMongoRepository1 mongoRepository, IJobRoleServiceWrapped jobRoleServiceWrapped = null)
            : base(mongoRepository)
        {
            if (jobRoleServiceWrapped == null)
                jobRoleServiceWrapped = new JobRoleServiceWrapped(mongoRepository);
            _jobRoleServiceWrapped = jobRoleServiceWrapped;
        }

        public IList<JobRole> FindByIds(JobRoleHydrationSettings hydrationSettings = null, params string[] ids)
        {
            if (ids == null)
                return new List<JobRole>();
            return MongoRepository.AsQueryable<Data.DTOs.JobRole>()
            .Where(x => ids.Contains(x.Id)).ToList().AsQueryable()
            .Select(x => hydrate(x, hydrationSettings))
            .ToList();
        }

        public List<JobRole> FindByParentID(string ParentRoleId, JobRoleHydrationSettings hydrationSettings = null)
        {
            return MongoRepository.AsQueryable<Data.DTOs.JobRole>()
                .Where(x => x.ParentRoleId == ParentRoleId).ToList().AsQueryable()
                .Select(x => hydrate(x, hydrationSettings))
                .ToList();
        }

        public IList<JobRole> FindByName(string JobRoleName, JobRoleHydrationSettings hydrationSettings = null)
        {
            return MongoRepository.AsQueryable<Data.DTOs.JobRole>()
                .Where(x => x.JobRoleName.ToLower() == JobRoleName.ToLower()).ToList().AsQueryable()
                .Select(x => hydrate(x, hydrationSettings))
                .ToList();
        }

        public JobRole FindByID(string ID, JobRoleHydrationSettings hydrationSettings = null)
        {
            return MongoRepository.AsQueryable<Data.DTOs.JobRole>()
                .Where(x => x.Id == ID).ToList().AsQueryable()
                .Select(x => hydrate(x, hydrationSettings))
                .FirstOrDefault();
        }

        ///
        public List<JobRole> GetAll(JobRoleHydrationSettings hydrationSettings = null)
        {
            return _jobRoleServiceWrapped.GetAll(hydrationSettings);
        }

        public List<JobRole> GetTopLevel(JobRoleHydrationSettings hydrationSettings = null)
        {
            return _jobRoleServiceWrapped.GetTopLevel(hydrationSettings);
        }

        public int GetCount()
        {
            return MongoRepository.AsQueryable<Data.DTOs.JobRole>().Count();
        }

        public IList<JobRole> GetPagedOrderedByName(int skip, int take, bool ascending = true, JobRoleHydrationSettings hydrationSettings = null)
        {
            var query = MongoRepository.AsQueryable<Data.DTOs.JobRole>();
            query = ascending ? query.OrderBy(x => x.JobRoleName) : query.OrderByDescending(x => x.JobRoleName);
            query = query.Skip(skip).Take(take);

            return query
                .Select(x => hydrate(x, hydrationSettings))
                .ToList();
        }

        public void Save(Data.Model.JobRole jobrole)
        {
            if (jobrole.Id == null) MongoRepository.Insert(jobrole);
            else MongoRepository.Update(jobrole);
        }

        #region Private Methods

        protected JobRole hydrate(Data.DTOs.JobRole JobRole, JobRoleHydrationSettings hydrationSettings)
        {
            if (JobRole == null) return null;
            // If hydration settings arent supplied then use defaults
            if (hydrationSettings == null) hydrationSettings = new JobRoleHydrationSettings();

            var viewModel = Mapper.Map<Data.DTOs.JobRole, JobRole>(JobRole);

            if (hydrationSettings.CreatedBy != null && !string.IsNullOrEmpty(JobRole.CreatedById))
            {
                var userService = new UserService(base.MongoRepository);
                viewModel.CreatedBy = userService.FindById(JobRole.CreatedById);
            }

            if (hydrationSettings.UpdatedBy != null && !string.IsNullOrEmpty(JobRole.UpdatedById))
            {
                var userService = new UserService(base.MongoRepository);
                viewModel.UpdatedBy = userService.FindById(JobRole.UpdatedById);
            }

            return viewModel;
        }

        protected Data.DTOs.User deHydrate(User viewModel)
        {
            var User = Mapper.Map<User, Data.DTOs.User>(viewModel);
            return User;
        }

        #endregion Private Methods
    }
}