﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using ITOK.Core.Data.Model.EqualityComparers;
using ITOK.Core.Data.Model.HS;

namespace ITOK.Core.Services.Core
{
    public class AuthorService : BaseService
    {
        public AuthorService(IMongoRepository1 mongoRepository)
            : base(mongoRepository)
        {
        }

        public IList<Author> FindByIds(AuthorHydrationSettings hydrationSettings = null, params string[] ids)
        {
            return MongoRepository.AsQueryable<Data.DTOs.Author>()
            .Where(x => ids.Contains(x.Id)).ToList().AsQueryable()
            .Select(x => hydrate(x, hydrationSettings))
            .ToList();
        }

        public int GetCount()
        {
            return MongoRepository.AsQueryable<Data.DTOs.Author>().Count();
        }

        public IList<Author> Search(string searchText, bool searchForename, bool searchSurname, bool searchDisplayName, int resultLimit, AuthorHydrationSettings hydrationSettings = null)
        {
            if (string.IsNullOrWhiteSpace(searchText) || (!searchForename && !searchSurname && !searchDisplayName)) return new List<Author>();

            var foundAuthors = new List<Author>();
            var searchTerms = searchText.ToLower().Split(' ');
            var sb = new StringBuilder("^");
            foreach (var term in searchTerms)
            {
                if (string.IsNullOrWhiteSpace(term)) continue;
                sb.Append(string.Format("(?=.*?({0}))", term));
            }
            sb.Append(".*$");
            var re = new Regex(sb.ToString(), RegexOptions.IgnoreCase | RegexOptions.Multiline);
            var query = MongoRepository.AsQueryable<Data.DTOs.Author>();

            if (searchForename)
            {
                var nameQuery = query
                    .Where(x => re.IsMatch(x.Forename));
                foundAuthors.AddRange(nameQuery
                    .Select(x => hydrate(x, hydrationSettings))
                    .ToList());
            }

            if (searchSurname)
            {
                var nameQuery = query
                    .Where(x => re.IsMatch(x.Surname));
                foundAuthors.AddRange(nameQuery
                    .Select(x => hydrate(x, hydrationSettings))
                    .ToList());
            }

            if (searchDisplayName)
            {
                var nameQuery = query
                    .Where(x => re.IsMatch(x.DisplayName));
                foundAuthors.AddRange(nameQuery
                    .Select(x => hydrate(x, hydrationSettings))
                    .ToList());
            }

            // Remove duplicates and limit result count
            foundAuthors = foundAuthors
                .Distinct(new AuthorEqualityComparer())
                .Take(resultLimit)
                .ToList();

            return foundAuthors;
        }

        public IList<Author> GetPagedOrderedBySurname(int skip, int take, bool ascending = true, AuthorHydrationSettings hydrationSettings = null)
        {
            var query = MongoRepository.AsQueryable<Data.DTOs.Author>();
            query = ascending ? query.OrderBy(x => x.Surname) : query.OrderByDescending(x => x.Surname);
            query = query.Skip(skip).Take(take);

            return query
                .Select(x => hydrate(x, hydrationSettings))
                .ToList();
        }

        #region Private Methods

        protected Author hydrate(Data.DTOs.Author author, AuthorHydrationSettings hydrationSettings)
        {
            if (author == null) return null;
            // If hydration settings arent supplied then use defaults
            if (hydrationSettings == null) hydrationSettings = new AuthorHydrationSettings();

            var viewModel = Mapper.Map<Data.DTOs.Author, Author>(author);

            if (hydrationSettings.CreatedBy != null && !string.IsNullOrEmpty(author.CreatedById))
            {
                var userService = new UserService(base.MongoRepository);
                viewModel.CreatedBy = userService.FindById(author.CreatedById);
            }

            if (hydrationSettings.UpdatedBy != null && !string.IsNullOrEmpty(author.UpdatedById))
            {
                var userService = new UserService(base.MongoRepository);
                viewModel.UpdatedBy = userService.FindById(author.UpdatedById);
            }

            return viewModel;
        }

        protected Data.DTOs.Author deHydrate(Author viewModel)
        {
            var author = Mapper.Map<Author, Data.DTOs.Author>(viewModel);
            return author;
        }

        #endregion Private Methods
    }
}