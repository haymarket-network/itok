﻿using ITOK.Core.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITOK.Core.Services.Core
{
    public class QuestionTemplateService : IQuestionTemplateService
    {
        public QuestionTemplate GetTransportationTemplateObject(bool isReadOnly, string areaId, string eventId, string yearId, string questionId)
        {
            var model = new QuestionTemplate { isReadOnly = isReadOnly, AreaId = areaId, EventId = eventId, YearId = yearId, QuestionId = questionId };
            var answerString1 = "IAAF Family";
            var answerString2 = "LOC Family";
            var answerString3 = "Teams";
            var answerString4 = "Press";
            var answerString5 = "Host Broadcasters";
            var answerString6 = "International Broadcasters";
            var answerString7 = "Volunteers";
            var answerString8 = "Competition Officials";

            var questionRow1 = new QuestionRow
            {
                AnswersRows = new List<AnswerRow>
                {
                    new AnswerRow
                    {
                        Title = answerString1,
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = answerString2,
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = answerString3,
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = answerString4,
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = answerString5,
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = answerString6,
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = answerString7,
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = answerString8,
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "Total",
                        AnswerDataType = AnswerDataType.CalculatedVertically
                    }
                }
            };

            var questionRow2 = new QuestionRow
            {
                AnswersRows = new List<AnswerRow>
                {
                    new AnswerRow
                    {
                        Title = answerString1,
                        AnswerDataType = AnswerDataType.YesNo
                    },
                    new AnswerRow
                    {
                        Title = answerString2,
                        AnswerDataType = AnswerDataType.YesNo
                    },
                    new AnswerRow
                    {
                        Title = answerString3,
                        AnswerDataType = AnswerDataType.YesNo
                    },
                    new AnswerRow
                    {
                        Title = answerString4,
                        AnswerDataType = AnswerDataType.YesNo
                    },
                    new AnswerRow
                    {
                        Title = answerString5,
                        AnswerDataType = AnswerDataType.YesNo
                    },
                    new AnswerRow
                    {
                        Title = answerString6,
                        AnswerDataType = AnswerDataType.YesNo
                    },
                    new AnswerRow
                    {
                        Title = answerString7,
                        AnswerDataType = AnswerDataType.YesNo
                    },
                    new AnswerRow
                    {
                        Title = answerString8,
                        AnswerDataType = AnswerDataType.YesNo
                    }
                }
            };

            questionRow1.Question = "#of Dedicated cars";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "# of cars in the carpool";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "# of buses";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "# of bikes";
            model.Questions.Add(questionRow1.ShallowCopy());



            questionRow2.Question = "Free Public Transportation";
            model.Questions.Add(questionRow2.ShallowCopy());

            return model;
        }

        public QuestionTemplate GetVolunteersPerAreaObject(bool isReadOnly, string areaId, string eventId, string yearId, string questionId)
        {
            var model = new QuestionTemplate { isReadOnly = isReadOnly, AreaId = areaId, EventId = eventId, YearId = yearId, QuestionId = questionId };
            var answerString1 = "Number of positions (taken by volunteers)";
            var answerString2 = "Number of volunteers allocated per area to cover all shifts";

            var questionRow = new QuestionRow
            {
                AnswersRows = new List<AnswerRow>
                {
                    new AnswerRow
                    {
                        Title = answerString1,
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = answerString2,
                        AnswerDataType = AnswerDataType.Numbers
                    }
                }
            };

            var questionRow2 = new QuestionRow
            {
                AnswersRows = new List<AnswerRow>
                {
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    }
                }
            };

            questionRow.Question = "Planning & Coordination";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Administration & Finance";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Human Ressources";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "IT/Telecommunications";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Legal";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Accommodation";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Accreditation";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Transport";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Volunteering";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Protocol & Hospitality";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Venue Management";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Commercial & Sponsorship";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Marketing";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Ticketing Operations";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Communications";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Media Operations (Broadcast & Press)";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Competition, Event Presentation, Team Services";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Security";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Visas";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Logistics";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Medical";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Anti-Doping";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow.Question = "Knowledge Management";
            model.Questions.Add(questionRow.ShallowCopy());

            questionRow2.Question = "Calculated";
            questionRow2.AnswerDataType = AnswerDataType.CalculatedHorizontally;
            model.Questions.Add(questionRow2.ShallowCopy());

            return model;
        }

        public QuestionTemplate GetStaffHiredInLOCObject(bool isReadOnly, string areaId, string eventId, string yearId, string questionId)
        {
            var model = new QuestionTemplate { isReadOnly = isReadOnly, AreaId = areaId, EventId = eventId, YearId = yearId, QuestionId = questionId };

            var questionRow1 = new QuestionRow
            {
                AnswersRows = new List<AnswerRow>
                {
                    new AnswerRow
                    {
                        Title = "25",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "24",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "23",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "22",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "21",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "20",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "19",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "18",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "17",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "16",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "15",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "14",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "13",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "12",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "11",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "10",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "9",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "8",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "7",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "6",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "5",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "4",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "3",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "2",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "1",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "Calculated",
                        AnswerDataType = AnswerDataType.CalculatedVertically
                    }
                }
            };

            var questionRow2 = new QuestionRow
            {
                AnswersRows = new List<AnswerRow>
                {
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    }
                }
            };



            questionRow1.Question = "Planning & Coordination";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Administration & Finance";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Human Ressources";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "IT/Telecommunications";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Legal";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Accommodation";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Accreditation";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Transport";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Volunteering";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Protocol & Hospitality";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Venue Management";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Commercial & Sponsorship";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Marketing";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Ticketing Operations";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Communications";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Media Operations (Broadcast & Press)";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Competition, Event Presentation, Team Services";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Security";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Visas";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Logistics";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Medical";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Anti-Doping";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Knowledge Management";
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow2.Question = "Calculated";
            questionRow2.AnswerDataType = AnswerDataType.CalculatedHorizontally;
            model.Questions.Add(questionRow2.ShallowCopy());


            return model;
        }

        public QuestionTemplate GetSiteVisitsSummaryObject(bool isReadOnly, string areaId, string eventId, string yearId, string questionId)
        {
            var model = new QuestionTemplate { isReadOnly = isReadOnly, AreaId = areaId, EventId = eventId, YearId = yearId, QuestionId = questionId };

            var questionRow1 = new QuestionRow
            {
                AnswersRows = new List<AnswerRow>
                {
                    new AnswerRow
                    {
                        Title = "IAAF Kick-Off",
                        AnswerDataType = AnswerDataType.Date
                    },
                    new AnswerRow
                    {
                        Title = "IAAF Presentation",
                        AnswerDataType = AnswerDataType.Date
                    },
                    new AnswerRow
                    {
                        Title = "LOC Presentation",
                        AnswerDataType = AnswerDataType.Date
                    }
                }
            };

            var questionRow2 = new QuestionRow
            {
                AnswersRows = new List<AnswerRow>
                {
                    new AnswerRow
                    {
                        Title = "IAAF Kick-Off",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "IAAF Presentation",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "LOC Presentation",
                        AnswerDataType = AnswerDataType.Numbers
                    }
                }
            };

            var questionRow3 = new QuestionRow
            {
                AnswersRows = new List<AnswerRow>
                {
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    },
                    new AnswerRow
                    {
                        Title = "",
                        AnswerDataType = AnswerDataType.CalculatedHorizontally
                    }
                }
            };

            questionRow1.Question = "Date of Site Visit";
            questionRow1.AnswerDataType = AnswerDataType.Date;
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow2.Question = "Planning & Coordination";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());

            questionRow2.Question = "Administration & Finance";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());

            questionRow2.Question = "Human Ressources";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());

            questionRow2.Question = "IT/Telecommunications";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());

            questionRow2.Question = "Legal";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());

            questionRow2.Question = "Event Operations (Accreditation, Accommodation, Transportation, Visas, Volunteers, Security, Venue)";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());

            questionRow2.Question = "Protocol";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());

            questionRow2.Question = "Commercial & Sponsorship";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());

            questionRow2.Question = "Marketing";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());

            questionRow2.Question = "Ticketing Operations & Hospitality";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());

            questionRow2.Question = "Communications";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());

            questionRow2.Question = "Media Operations (Broadcast & Press)";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());

            questionRow2.Question = "Competition";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());

            questionRow2.Question = "Event Presentation";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());

            questionRow2.Question = "Team Services";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());

            questionRow2.Question = "Medical";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());

            questionRow2.Question = "Anti-Doping";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());

            questionRow2.Question = "Knowledge Management (Observer Programme, Impact Studies, Data Capture)";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());

            questionRow3.Question = "Calculated";
            questionRow3.AnswerDataType = AnswerDataType.CalculatedHorizontally;
            model.Questions.Add(questionRow3.ShallowCopy());


            return model;
        }

        public QuestionTemplate GetCarpoolUsagePerDayObject(bool isReadOnly, string areaId, string eventId, string yearId, string questionId)
        {
            var model = new QuestionTemplate { isReadOnly = isReadOnly, AreaId = areaId, EventId = eventId, YearId = yearId, QuestionId = questionId };

            var questionRow1 = new QuestionRow
            {
                AnswersRows = new List<AnswerRow>
                {
                    new AnswerRow
                    {
                        Title = "10",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "9",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "8",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "7",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "6",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "5",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "4",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "3",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "2",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "1",
                        AnswerDataType = AnswerDataType.Numbers
                    },new AnswerRow
                    {
                        Title = "1",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "2",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "3",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "4",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "5",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "6",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "7",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "8",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "9",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "10",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "11",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "12",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "13",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "14",
                        AnswerDataType = AnswerDataType.Numbers
                    }
                }
            };

            questionRow1.Question = "Number of car movements";
            questionRow1.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow1.ShallowCopy());


            return model;
        }


        public QuestionTemplate GetAccommodationFormUpdatedObject(bool isReadOnly, string areaId, string eventId, string yearId, string questionId)
        {
            var model = new QuestionTemplate { isReadOnly = isReadOnly, AreaId = areaId, EventId = eventId, YearId = yearId, QuestionId = questionId };

            var questionRow1 = new QuestionRow
            {
                AnswersRows = new List<AnswerRow>
                {
                    new AnswerRow
                    {
                        Title = "Hotel 1",
                        AnswerDataType = AnswerDataType.FreeText
                    },
                    new AnswerRow
                    {
                        Title = "Hotel 2",
                        AnswerDataType = AnswerDataType.FreeText
                    },
                    new AnswerRow
                    {
                        Title = "Hotel 3",
                        AnswerDataType = AnswerDataType.FreeText
                    }
                }
            };

            var questionRow2 = new QuestionRow
            {
                AnswersRows = new List<AnswerRow>
                {
                    new AnswerRow
                    {
                        Title = "Twin/double",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "Single",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "Twin/double",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "Single",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "Twin/double",
                        AnswerDataType = AnswerDataType.Numbers
                    },
                    new AnswerRow
                    {
                        Title = "Single",
                        AnswerDataType = AnswerDataType.Numbers
                    }

                }
            };

            questionRow1.Question = "Client Group";
            questionRow1.AnswerDataType = AnswerDataType.FreeText;
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Name of accommodation";
            questionRow1.AnswerDataType = AnswerDataType.FreeText;
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Standard of accommodation (*)";
            questionRow1.AnswerDataType = AnswerDataType.FreeText;
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Website";
            questionRow1.AnswerDataType = AnswerDataType.FreeText;
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Distance (in km) and travel time by bus between accommodation and airport(s)";
            questionRow1.AnswerDataType = AnswerDataType.FreeText;
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Distance (in km) and travel time by bus between accommodation and venue/course";
            questionRow1.AnswerDataType = AnswerDataType.FreeText;
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Maximum capacity of accommodation (no. of beds)";
            questionRow1.AnswerDataType = AnswerDataType.FreeText;
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Number of single rooms that have been reserved for the duration of the event";
            questionRow1.AnswerDataType = AnswerDataType.FreeText;
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Number of single rooms that have been used for the duration of the event";
            questionRow1.AnswerDataType = AnswerDataType.FreeText;
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Number of twin/double rooms that have been reserved for the duration of the event";
            questionRow1.AnswerDataType = AnswerDataType.FreeText;
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Number of twin/double rooms that have been used for the duration of the event";
            questionRow1.AnswerDataType = AnswerDataType.FreeText;
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Number of meeting rooms available";
            questionRow1.AnswerDataType = AnswerDataType.FreeText;
            model.Questions.Add(questionRow1.ShallowCopy());

            questionRow1.Question = "Description of cancellation policy";
            questionRow1.AnswerDataType = AnswerDataType.FreeText;
            model.Questions.Add(questionRow1.ShallowCopy());


            questionRow2.Question = "Description of cancellation policy";
            questionRow2.AnswerDataType = AnswerDataType.Numbers;
            model.Questions.Add(questionRow2.ShallowCopy());



            return model;
        }
    }

    public interface IQuestionTemplateService
    {
        QuestionTemplate GetTransportationTemplateObject(bool isReadOnly, string areaId, string eventId, string yearId,
            string questionId);

        QuestionTemplate GetVolunteersPerAreaObject(bool isReadOnly, string areaId, string eventId, string yearId,
            string questionId);

        QuestionTemplate GetStaffHiredInLOCObject(bool isReadOnly, string areaId, string eventId, string yearId,
            string questionId);

        QuestionTemplate GetSiteVisitsSummaryObject(bool isReadOnly, string areaId, string eventId, string yearId,
            string questionId);

        QuestionTemplate GetCarpoolUsagePerDayObject(bool isReadOnly, string areaId, string eventId, string yearId,
            string questionId);

        QuestionTemplate GetAccommodationFormUpdatedObject(bool isReadOnly, string areaId, string eventId,
            string yearId, string questionId);
    }
}
