﻿using AutoMapper;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using ITOK.Core.Services;
using ITOK.Core.Services.CMS;
using ITOK.Core.Services.Core;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace ITOK.Core.Services.Core
{
    public class AnswerService : BaseService, IAnswerService
    {
        private readonly IUserService _userService;
        private readonly ITagsService _tagsService;
        private readonly IQuestionService _questionService;

        public AnswerService(IMongoRepository1 mongoRepository, IUserService userService, ITagsService tagsService, IQuestionService questionService)
        {
            _userService = userService;
            _tagsService = tagsService;
            _questionService = questionService;
            _mongoRepository = mongoRepository;
        }

        public void Save(Answer model)
        {
            var dto = deHydrate(model);

            if (string.IsNullOrEmpty(dto.Id))
            {
                _mongoRepository.Insert(dto);
            }
            else
            {
                _mongoRepository.Update(dto);
            }
        }


        public ITOK.Core.Data.Model.Answer Find(string questionId, string eventId, string yearId, string areaId)
        {
            var answerDto = _mongoRepository.AsQueryable<ITOK.Core.Data.DTOs.Answer>().FirstOrDefault(x =>
                x.AreaId == areaId && x.EventId == eventId && x.YearId == yearId && x.QuestionId == questionId);

            return hydrate(answerDto);
        }

        public Answer FindById(string answerId)
        {
            var answerDto = _mongoRepository.AsQueryable<ITOK.Core.Data.DTOs.Answer>().FirstOrDefault(x =>
                x.Id == answerId);

            return hydrate(answerDto);
        }

        public ITOK.Core.Data.Model.Answer FindByQues(string questionId)
        {
            var answerDto = _mongoRepository.AsQueryable<ITOK.Core.Data.DTOs.Answer>().FirstOrDefault(x => x.QuestionId == questionId);

            return hydrate(answerDto);
        }
        public IList<Answer> All()
        {
            var filter = Builders<ITOK.Core.Data.DTOs.Answer>.Filter.Empty;
            var query = _mongoRepository.GetCollection<ITOK.Core.Data.DTOs.Answer>().Find(filter, new FindOptions() { });
            var answer = query.ToList();
            return answer.Select(hydrate).ToList();
        }
        public IList<Answer> FindSearch(string searchtext, string eventId, string yearId, string areaId)
        {
            if ((string.IsNullOrWhiteSpace(searchtext) || searchtext == "undefined") && (string.IsNullOrWhiteSpace(eventId) || eventId == "undefined") && (string.IsNullOrWhiteSpace(yearId) || yearId == "undefined") && (string.IsNullOrWhiteSpace(areaId) || areaId == "undefined"))
            {
                return new List<Answer>();
            }
            var filter = Builders<ITOK.Core.Data.DTOs.Answer>.Filter.Empty;
            if (!string.IsNullOrEmpty(eventId))
            {
                filter = filter & Builders<ITOK.Core.Data.DTOs.Answer>.Filter.Eq("EventId", eventId);
            }
            if (!string.IsNullOrEmpty(yearId))
            {
                filter = filter & Builders<ITOK.Core.Data.DTOs.Answer>.Filter.Eq("YearId", yearId);
            }
            if (!string.IsNullOrEmpty(areaId))
            {
                filter = filter & Builders<ITOK.Core.Data.DTOs.Answer>.Filter.Eq("AreaId", areaId);
            }
            if (!string.IsNullOrWhiteSpace(searchtext) && searchtext != "undefined")
            {
                var searchTerms = searchtext.ToLower().Split(' ');
                var sb = new StringBuilder("^");
                foreach (var term in searchTerms.Where(term => !string.IsNullOrEmpty(term)))
                {
                    sb.Append(string.Format("(?=.*?({0}))", term));
                }
                sb.Append(".*$");
                var re = new Regex(sb.ToString(), RegexOptions.IgnoreCase | RegexOptions.Multiline);

                filter = filter & Builders<ITOK.Core.Data.DTOs.Answer>.Filter.Regex(x => x.QuestionText, re);
            }
            var query = _mongoRepository.GetCollection<ITOK.Core.Data.DTOs.Answer>().Find(filter, new FindOptions() { });
            var answer = query.ToList();
            return answer.Select(hydrate).ToList();
        }
        protected IFindFluent<ITOK.Core.Data.DTOs.Answer, ITOK.Core.Data.DTOs.Answer> GenerateBasicQuery()
        {
            var filter = Builders<ITOK.Core.Data.DTOs.Answer>.Filter.Empty;

            var query = _mongoRepository.GetCollection<ITOK.Core.Data.DTOs.Answer>().Find(filter, new FindOptions() { });

            return query;
        }
        private ITOK.Core.Data.DTOs.Answer deHydrate(ITOK.Core.Data.Model.Answer viewModel)
        {
            var question = Mapper.Map<ITOK.Core.Data.Model.Answer, ITOK.Core.Data.DTOs.Answer>(viewModel);
            return question;
        }

        private ITOK.Core.Data.Model.Answer hydrate(ITOK.Core.Data.DTOs.Answer dtoModel)
        {
            var question = Mapper.Map<ITOK.Core.Data.DTOs.Answer, ITOK.Core.Data.Model.Answer>(dtoModel);
            return question;
        }
    }

    public interface IAnswerService
    {
        void Save(Answer model);
        ITOK.Core.Data.Model.Answer Find(string questionId, string eventId, string yearId, string areaId);
        IList<Answer> All();
        Answer FindById(string answerId);
        IList<Answer> FindSearch(string searchtext, string eventId, string yearId, string areaId);
        ITOK.Core.Data.Model.Answer FindByQues(string questionId);
    }
}
