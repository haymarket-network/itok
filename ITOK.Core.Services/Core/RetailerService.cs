﻿using AutoMapper;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using ITOK.Core.Data.Model.HS;
using ITOK.Core.Services.Core.Wrapped;

namespace ITOK.Core.Services.Core
{
    public class RetailerService : BaseService
    {
        private readonly IRetailerServiceWrapped _retailerServiceWrapped;

        public RetailerService(IMongoRepository1 mongoRepository, IRetailerServiceWrapped RetailerServiceWrapped = null)
            : base(mongoRepository)
        {
            if (RetailerServiceWrapped == null)
                RetailerServiceWrapped = new RetailerServiceWrapped(mongoRepository);
            _retailerServiceWrapped = RetailerServiceWrapped;
        }

        public IList<Retailer> All()
        {
            return _retailerServiceWrapped.All();
        }
        public Retailer FindById(string id)
        {
            return _retailerServiceWrapped.FindById(id);
        }

        public Retailer FindByRetailerId(string retailerid, Brand brand)
        {
            return _retailerServiceWrapped.FindByRetailerId(retailerid, brand);
        }
        public IList<Retailer> FindByIds(params string[] ids)
        {
            return _retailerServiceWrapped.FindByIds(ids);
        }
        public void Delete(string id)
        {
          
            var filter = Builders<Retailer>.Filter.Eq("_id", id);
            MongoRepository.GetCollection<Retailer>().DeleteOne(filter);
        }

        public int GetCount()
        {
            return MongoRepository.AsQueryable<Data.DTOs.Retailer>().Count();
        }

        public IList<Retailer> GetPagedOrderedByName(int skip, int take, bool ascending = true, RetailerHydrationSettings hydrationSettings = null)
        {
            var query = MongoRepository.AsQueryable<Data.DTOs.Retailer>();
            query = ascending ? query.OrderBy(x => x.RetailerName) : query.OrderByDescending(x => x.RetailerName);
            query = query.Skip(skip).Take(take);

            return query
                .Select(x => hydrate(x, hydrationSettings))
                .ToList();
        }

        public void Save(Data.Model.Retailer retailer)
        {
            if (string.IsNullOrWhiteSpace(retailer.Id)) MongoRepository.Insert(retailer);
            else MongoRepository.Update(retailer);
        }

        #region Private Methods

        protected Retailer hydrate(Data.DTOs.Retailer retailer, RetailerHydrationSettings hydrationSettings)
        {
            if (retailer == null) return null;
            // If hydration settings arent supplied then use defaults
            if (hydrationSettings == null) hydrationSettings = new RetailerHydrationSettings();

            var viewModel = Mapper.Map<Data.DTOs.Retailer, Retailer>(retailer);

            if (hydrationSettings.CreatedBy != null && !string.IsNullOrEmpty(retailer.CreatedById))
            {
                var userService = new UserService(base.MongoRepository);
                viewModel.CreatedBy = userService.FindById(retailer.CreatedById);
            }

            if (hydrationSettings.UpdatedBy != null && !string.IsNullOrEmpty(retailer.UpdatedById))
            {
                var userService = new UserService(base.MongoRepository);
                viewModel.UpdatedBy = userService.FindById(retailer.UpdatedById);
            }

            return viewModel;
        }

        protected Data.DTOs.Retailer deHydrate(Retailer viewModel)
        {
            var Retailer = Mapper.Map<Retailer, Data.DTOs.Retailer>(viewModel);
            return Retailer;
        }

        #endregion Private Methods
    }
}