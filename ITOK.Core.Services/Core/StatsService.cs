﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data;
using ITOK.Core.Data.DTOs;
using ITOK.Core.Data.Model.HS;
using ITOK.Core.Services.Core.Wrapped;

namespace ITOK.Core.Services.Core
{
    public class StatsService : BaseService
    {
        public StatsService(IMongoRepository1 mongoRepository, IArticleServiceWrapped articleServiceWrapped = null)
            : base(mongoRepository)
        {
            if (articleServiceWrapped == null)
                articleServiceWrapped = new ArticleServiceWrapped(mongoRepository);
            _articleServiceWrapped = articleServiceWrapped;
        }

        private readonly IArticleServiceWrapped _articleServiceWrapped;

        
        #region Article Stats

        public void RegisterArticleViewed(string userId, string jobRoleId, string articleId, Brand brand)
        {
            var articleStat = new ArticleStat
            {
                ArticleType = ArticleType.article,
                UserId = userId,
                JobRoleId = jobRoleId,
                Timestamp = DateTime.UtcNow,
                StatisticType = ArticleStatType.View,
                ArticleId = articleId,
                Brand = brand
            };

            MongoRepository.Insert(articleStat);
        }

        public List<BsonDocument> FindArticleList(string jobRoleId, DateTime fitlerTime, int take = 10)
        {
            var filter = Builders<Data.DTOs.ArticleStat>.Filter.Gte("Timestamp", new BsonDateTime(fitlerTime));

            if (!string.IsNullOrEmpty(jobRoleId))
                filter = filter & Builders<Data.DTOs.ArticleStat>.Filter.Eq("JobRoleId", jobRoleId);

            var collection = MongoRepository.GetCollection<Data.DTOs.ArticleStat>();

            var aggregate = collection.Aggregate()
                .Match(filter)
                //.Unwind("RelatedTags")
                //.Project()
                .Group(new BsonDocument { { "_id", new BsonDocument { { "_id", "$ArticleId" } } }, { "count", new BsonDocument("$sum", 1) } })
                .Sort(new BsonDocument("count", -1))
                .Limit(take);

            return aggregate.ToList();
        }

        public IList<ITOK.Core.Data.Model.Article> GetViewedArticlesByUserId(Brand brand,string userId, int skip, int take, ArticleHydrationSettings hydrationSettings = null)
        {
            var query = MongoRepository.AsQueryable<Data.DTOs.ArticleStat>()
               .Where(i => i.UserId == userId && i.Brand == brand).OrderByDescending(x => x.Timestamp).Skip(skip).Take(take);
            var articleIdList = query.Select(x => x.ArticleId).ToArray();
            var articleStatList = query.ToArray();

            var articles = FindByUrlSlugsWithArticleStatDetails(brand,articleStatList, hydrationSettings, articleIdList).ToList();

            return articles;
        }

        public IList<ITOK.Core.Data.Model.Article> GetUniqueViewedArticlesByUserId(Brand brand, string userId, int skip, int take, ArticleHydrationSettings hydrationSettings = null)
        {
            var query = MongoRepository.AsQueryable<Data.DTOs.ArticleStat>()
               .Where(i => i.UserId == userId && i.Brand == brand).OrderByDescending(x => x.Timestamp).Skip(skip).Take(take);
            var articleIdList = query.GroupBy(i => i.ArticleId).Select(y => y.First()).Select(x => x.ArticleId).ToArray();
            var articleStatList = query.ToArray();

            var articles = FindByIds(hydrationSettings, articleIdList).ToList();

            return articles;
        }
        public IList<ITOK.Core.Data.DTOs.ArticleStat> GetViewedArticleStatsByUserId(string userId, int daysAgo = 0, ArticleHydrationSettings hydrationSettings = null)
        {
            var query = MongoRepository.AsQueryable<Data.DTOs.ArticleStat>();

                if(daysAgo!=0)
               query = query.Where(i => i.UserId == userId).Where(i => i.Timestamp > DateTime.Now.AddDays(daysAgo)).OrderByDescending(x => x.Timestamp);

            //  query.Select(x => x.UrlSlug).Distinct().ToArray();
            //var articleStatList = query.ToArray();

            return query.ToList();
        }


        /// <summary>
        /// Returns Article ID
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="hydrationSettings"></param>
        /// <param name="days"></param>
        /// <param name="brand"></param>
        /// <returns></returns>
        public IList<string> GetViewedArticlesByUserId(string userId, ArticleHydrationSettings hydrationSettings = null, int? days= null, Brand? brand = null)
            {
            var query = MongoRepository.AsQueryable<Data.DTOs.ArticleStat>();
            var filter = Builders<Data.DTOs.ArticleStat>.Filter.Eq("UserId", userId);


            if (brand != null)
            {
            filter = filter & filter & Builders<Data.DTOs.ArticleStat>.Filter.Eq("Brand", brand);
            }

            if (days !=null)
            {
                filter = filter & Builders<Data.DTOs.ArticleStat>.Filter.Gt("Timestamp", new BsonDateTime(DateTime.Now.AddDays((int)days)));

            }
            var queryNew = MongoRepository.GetCollection<Data.DTOs.ArticleStat>().Find(filter, new FindOptions() { });

            return queryNew.ToList().Select(x => x.ArticleId).Distinct().ToArray();
           
            }


        public Boolean IsArticleViewed(string articleId, string userId, ArticleHydrationSettings hydrationSettings = null)
        {
            return MongoRepository.AsQueryable<Data.DTOs.ArticleStat>()
               .Any(i => i.UserId == userId && i.ArticleId == articleId);
          
        }


        public int FindByArticleIdCount(string articleId)
        {
            return
                MongoRepository.AsQueryable<Data.DTOs.ArticleStat>()
                    .Where(x => x.ArticleId == articleId)
                    .Select(x => x.UserId)
                    .Distinct()
                    .Count();
        }


        public IEnumerable<ITOK.Core.Data.Model.Article> FindByUrlSlugs(Brand brand, ArticleHydrationSettings hydrationSettings = null, params string[] articleIds)
        {
        return _articleServiceWrapped.FindByUrlSlugs(brand, hydrationSettings, articleIds);
        }
        public IEnumerable<ITOK.Core.Data.Model.Article> FindByIds(ArticleHydrationSettings hydrationSettings = null, params string[] articleIds)
        {
            return _articleServiceWrapped.FindByIds(hydrationSettings, articleIds);
        }
        public IEnumerable<ITOK.Core.Data.Model.Article> FindByUrlSlugsWithArticleStatDetails(Brand brand, Data.DTOs.ArticleStat[] articleStatList, ArticleHydrationSettings hydrationSettings = null, params string[] articleIds)
        {
        return _articleServiceWrapped.FindByUrlSlugsWithArticleStatDetails(brand,articleStatList, hydrationSettings, articleIds);
        }

        #endregion Article Stats

        #region Hub

        public void RegisterHubViewed(Brand brand, string userId, string jobRoleId, string urlSlug)
        {
            var hubStat = new HubStat
            {
                Brand = brand,
                UserId = userId,
                JobRoleId = jobRoleId,
                Timestamp = DateTime.UtcNow,
                StatisticType = ArticleStatType.View,
                UrlSlug = urlSlug//string.Join( "+",  urlSlugs.OrderBy(x=>x))
            };

            MongoRepository.Insert(hubStat);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="jobRoleId"></param>
        /// <param name="fitlerTime"></param>
        /// <returns></returns>
        public List<BsonDocument> FindHubsList(Brand brand, string jobRoleId, DateTime fitlerTime, int take = 10)
        {
            //var query = GenerateBasicQuery(showWithStatus, showOnlyLive);

            //var filter = Builders<Data.DTOs.HubStat>.Filter.Empty;

            var filter = Builders<Data.DTOs.HubStat>.Filter.Gte("Timestamp", new BsonDateTime(fitlerTime));


            filter = filter & Builders<Data.DTOs.HubStat>.Filter.Eq("Brand", brand);

            if (!string.IsNullOrEmpty(jobRoleId))
                filter = filter & Builders<Data.DTOs.HubStat>.Filter.Eq("JobRoleId", jobRoleId);

            var collection = MongoRepository.GetCollection<Data.DTOs.HubStat>();

            var aggregate = collection.Aggregate()
                .Match(filter)
                //.Unwind("RelatedTags")
                //.Project()
                .Group(new BsonDocument { { "_id", new BsonDocument { { "UrlSlug", "$UrlSlug" } } }, { "count", new BsonDocument("$sum", 1) } })
                .Sort(new BsonDocument("count", -1))
                 .Limit(take);

            return aggregate.ToList();
        }

        #endregion Hub

        #region GotIt Stats

        public void RegisterGotIt(Brand brand,string userId, string jobRoleId, string articleId, int YesNo, string currentUserId )
        {
        var articleStatFound = FindByArticleAndUser(articleId, currentUserId);

            if (articleStatFound == null)
            {
                var articleStat = new GotItStat
                {
                    UserId = userId,
                    JobRoleId = jobRoleId,
                    Timestamp = DateTime.UtcNow,
                    StatisticType = (GotItStatType)YesNo,
                    ArticleId = articleId,
                    Brand = brand,
                };

                MongoRepository.Insert(articleStat);
            }
            else
            {
                articleStatFound.StatisticType = (GotItStatType)YesNo;
                MongoRepository.Update(articleStatFound);
            }
        }

        public List<BsonDocument> FindGotItStatList(string jobRoleId, DateTime fitlerTime, int take = 10)
        {
            //var query = GenerateBasicQuery(showWithStatus, showOnlyLive);

            //var filter = Builders<Data.DTOs.HubStat>.Filter.Empty;

            var filter = Builders<Data.DTOs.GotItStat>.Filter.Gte("Timestamp", new BsonDateTime(fitlerTime));

            if (!string.IsNullOrEmpty(jobRoleId))

                filter = filter & Builders<Data.DTOs.GotItStat>.Filter.Eq("JobRoleId", jobRoleId);

            var collection = MongoRepository.GetCollection<Data.DTOs.GotItStat>();

            var aggregate = collection.Aggregate()
                .Match(filter)
                //.Unwind("RelatedTags")
                //.Project()
                .Group(new BsonDocument { { "_id", new BsonDocument { { "UrlSlug", "$UrlSlug" } } }, { "count", new BsonDocument("$sum", 1) } })
                .Sort(new BsonDocument("count", -1))
                .Limit(take);

            return aggregate.ToList();
        }

        public GotItStat FindByArticleAndUser(string articleId, string UserId)
        {
            return MongoRepository
                 .AsQueryable<GotItStat>().Where(x => x.ArticleId == articleId)
                 .FirstOrDefault(i => i.UserId == UserId);
        }

        #endregion GotIt Stats

        #region Favorite Stats

        public void RegisterFavorite(string userId, string jobRoleId, string articleId, int YesNo, string currentUserId, Brand brand)
        {
        var articleStatFound = FindFavoriteByArticleAndUser(articleId, currentUserId);

            if (articleStatFound == null)
            {
                var articleStat = new FavoriteStat
                {
                    UserId = userId,
                    JobRoleId = jobRoleId,
                    Timestamp = DateTime.UtcNow,
                    StatisticType = (FavoriteStatType)YesNo,
                    ArticleId = articleId,
                    brand = brand
                   
                };

                MongoRepository.Insert(articleStat);
            }
            else
            {
                articleStatFound.StatisticType = (FavoriteStatType)YesNo;
                MongoRepository.Update(articleStatFound);
            }
        }

        public List<BsonDocument> FindFavoriteStatList(string jobRoleId, DateTime fitlerTime, int take = 10)
        {
            //var query = GenerateBasicQuery(showWithStatus, showOnlyLive);

            //var filter = Builders<Data.DTOs.HubStat>.Filter.Empty;

            var filter = Builders<Data.DTOs.FavoriteStat>.Filter.Gte("Timestamp", new BsonDateTime(fitlerTime));

            if (!string.IsNullOrEmpty(jobRoleId))

                filter = filter & Builders<Data.DTOs.FavoriteStat>.Filter.Eq("JobRoleId", jobRoleId);

            var collection = MongoRepository.GetCollection<Data.DTOs.FavoriteStat>();

            var aggregate = collection.Aggregate()
                .Match(filter)
                //.Unwind("RelatedTags")
                //.Project()
                .Group(new BsonDocument { { "_id", new BsonDocument { { "ArticleId", "$ArticleId" } } }, { "count", new BsonDocument("$sum", 1) } })
                .Sort(new BsonDocument("count", -1))
                .Limit(take);

            return aggregate.ToList();
        }

        public FavoriteStat FindFavoriteByArticleAndUser(string articleId, string UserId)
        {
            return MongoRepository
                 .AsQueryable<FavoriteStat>().Where(x => x.ArticleId == articleId)
                 .FirstOrDefault(i => i.UserId == UserId);
        }

        #endregion Favorite Stats

        #region Reading Stats

        public void RegisterReading(string userId, string jobRoleId, string articleId, int YesNo, string currentUserId, string readingListId, Brand brand)
        {
        var articleStatFound = FindReadingByArticleAndReadingList(articleId, readingListId);

            if (articleStatFound == null)
            {
                var articleStat = new ReadingStat
                {
                    UserId = userId,
                    JobRoleId = jobRoleId,
                    Timestamp = DateTime.UtcNow,
                    StatisticType = (ReadingStatType)YesNo,
                    ArticleId = articleId,
                    ReadingListId = readingListId, 
                    brand = brand
                };

                MongoRepository.Insert(articleStat);
            }
            else
            {
                articleStatFound.StatisticType = (ReadingStatType)YesNo;
                MongoRepository.Update(articleStatFound);
            }
        }

        public List<BsonDocument> FindReadingStatList(string jobRoleId, DateTime fitlerTime, int take = 10)
        {
            //var query = GenerateBasicQuery(showWithStatus, showOnlyLive);

            //var filter = Builders<Data.DTOs.HubStat>.Filter.Empty;

            var filter = Builders<Data.DTOs.ReadingStat>.Filter.Gte("Timestamp", new BsonDateTime(fitlerTime));

            if (!string.IsNullOrEmpty(jobRoleId))

                filter = filter & Builders<Data.DTOs.ReadingStat>.Filter.Eq("JobRoleId", jobRoleId);

            var collection = MongoRepository.GetCollection<Data.DTOs.ReadingStat>();

            var aggregate = collection.Aggregate()
                .Match(filter)
                //.Unwind("RelatedTags")
                //.Project()
                .Group(new BsonDocument { { "_id", new BsonDocument { { "ArticleId", "$ArticleId" } } }, { "count", new BsonDocument("$sum", 1) } })
                .Sort(new BsonDocument("count", -1))
                .Limit(take);

            return aggregate.ToList();
        }

        public ReadingStat FindReadingByArticleAndReadingList(string articleId, string readingListId)
        {
            return MongoRepository
                 .AsQueryable<ReadingStat>().Where(x => x.ArticleId == articleId)
                 .FirstOrDefault(i => i.ReadingListId == readingListId);
        }
        public void RemoveReading(string articleId, string currentUserId, string readingListId)
        {
        var articleStatFound = FindReadingByArticleAndReadingList(articleId, readingListId);

            MongoRepository.Remove<ReadingStat>(articleStatFound,articleStatFound.Id);
         
        }

        #endregion Reading Stats

       
    }
}