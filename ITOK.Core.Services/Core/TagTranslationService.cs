﻿using AutoMapper;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using ITOK.Core.Data.Model.EqualityComparers;
using ITOK.Core.Services.Core.Wrapped;

namespace ITOK.Core.Services.Core
{
    public class TagTranslationService : BaseService
    {
        private readonly ITagTranslationServiceWrapped _tagTranslationServiceWrapped;

        public TagTranslationService(IMongoRepository1 mongoRepository, ITagTranslationServiceWrapped tagsServiceWrapped = null)
            : base(mongoRepository)
        {
            if (tagsServiceWrapped == null)
                tagsServiceWrapped = new TagTranslationServiceWrapped(mongoRepository);
            _tagTranslationServiceWrapped = tagsServiceWrapped;
        }

        public IList<TagTranslation> All()
        {
            return _tagTranslationServiceWrapped.All();
        }

    


        public IList<TagTranslation> FindByIds(params string[] ids)
        {
            return _tagTranslationServiceWrapped.FindByIds(ids);
        }

        public TagTranslation FindById(string id)
        {
            return _tagTranslationServiceWrapped.FindById(id);
        }

        public TagTranslation FindByUrlSlug(string UrlSlug, Brand brand)
        {
            return _tagTranslationServiceWrapped.FindByUrlSlug(UrlSlug, brand);
        }

        public List<Data.Model.TagTranslation> FindByUrlSlug(string[] urlSlugs, Brand brand)
        {
            return _tagTranslationServiceWrapped.FindByUrlSlug(urlSlugs, brand);
        }

        public TagTranslation FindByOriginalTag(string displayText, Brand brand)
        {
            return _tagTranslationServiceWrapped.FindByOriginalTag(displayText, brand);
        }

        public Data.DTOs.TagTranslation deHydrate(Data.Model.TagTranslation viewModel)
        {
            var article = Mapper.Map<Data.Model.TagTranslation, Data.DTOs.TagTranslation>(viewModel);
            return article;
        }

        public Data.Model.TagTranslation hydrate(Data.DTOs.TagTranslation tag)
        {
            if (tag == null) return null;
            // If hydration settings arent supplied then use defaults

            var viewModel = Mapper.Map<Data.DTOs.TagTranslation, TagTranslation>(tag);

            //include viewcount

            return viewModel;
        }

        public void Save(TagTranslation tag)
        {
            var _tag = deHydrate(tag);

           

            //#region Bulk Update Tags in Articles
            //if (previousTag != null)
            //{ 
            ////UPDATES RELATED TAGS
            //var filter = Builders<Article>.Filter.ElemMatch(x => x.RelatedTags, x => x.DisplayText == previousTag.DisplayText);
            //var update = Builders<Article>.Update.Set(x => x.RelatedTags[-1].DisplayText, tag.DisplayText)
            //    .Set(x => x.RelatedTags[-1].UrlSlug, tag.UrlSlug);
           
            //    MongoRepository.GetCollection<Article>().UpdateMany(filter, update);



            ////UPDATES TAGS ARRAY
            //var filterForUrlSlug = Builders<Article>.Filter.AnyIn(x => x.Tags, new[] { previousTag.UrlSlug });
            //var updateForUrlSlugPush = Builders<Article>.Update.Push(x => x.Tags, tag.UrlSlug);
            //var updateForUrlSlugPull = Builders<Article>.Update.Pull(x => x.Tags, previousTag.UrlSlug);


            //    MongoRepository.GetCollection<Article>().UpdateMany(filterForUrlSlug, updateForUrlSlugPush);
            //MongoRepository.GetCollection<Article>().UpdateMany(filterForUrlSlug, updateForUrlSlugPull);

            //}
            //#endregion

            if (tag.OriginalSlug == null) MongoRepository.Insert(_tag);
            else MongoRepository.Update(_tag);
        }

        public void Insert(TagTranslation tag)
        {
            var _tag = deHydrate(tag);
            MongoRepository.Insert(_tag);
        }

        public void Delete(string id)
        {
            var filter = Builders<TagTranslation>.Filter.Eq("_id", id);
            MongoRepository.GetCollection<TagTranslation>().DeleteOne(filter);
        }
    }
}