﻿using System.Collections.Generic;
using System.Linq;
using ITOK.Core.Data;
using ITOK.Core.Data.DTOs;
using ITOK.Core.Services.Core.Wrapped;

namespace ITOK.Core.Services.Core
    {
  public   class CertificateRegistrationService: BaseService
    {
        private ICertificateRegistrationServiceWrapped _CertificateRegistrationServiceWrapped;

        public CertificateRegistrationService(IMongoRepository1 mongoRepository, ICertificateRegistrationServiceWrapped CertificateRegistrationServiceWrapped = null)
            : base(mongoRepository)
        {
        if (CertificateRegistrationServiceWrapped == null)
            CertificateRegistrationServiceWrapped = new CertificateRegistrationServiceWrapped(mongoRepository);
            _CertificateRegistrationServiceWrapped = CertificateRegistrationServiceWrapped;
        }



        public void Save(CertificateRegistration certificateRegistration)
            {

            if (certificateRegistration.Id == null) MongoRepository.Insert(certificateRegistration);
            else MongoRepository.Update(certificateRegistration);
            }




      
        
        }
    }
