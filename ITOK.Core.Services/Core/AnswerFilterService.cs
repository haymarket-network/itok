﻿
using System;
using AutoMapper;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using ITOK.Core.Services;
using ITOK.Core.Services.Core;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using ITOK.Core.Data.DTOs;

namespace ITOK.Core.Services.Core
{
    public class AnswerFilterService : BaseService, IAnswerFilterService
    {
        private IMongoRepository1 _mongoRepository;
        public readonly ITagsService _tagsService;
        //private readonly IFavoriteCountServiceWrapped _favoriteCountService;
        //private readonly ReadingListService _readingListService;

        public AnswerFilterService(IMongoRepository1 mongoRepository)
        {
            _mongoRepository = mongoRepository;
            _tagsService = ITOK.Core.Common.AppKernel.GetInstance<ITagsService>();
            //  _favoriteCountService = favoriteCountService;
        }

        public IList<AnswerFilterData> FindByIds(params string[] ids)
        {
            if (ids == null || !ids.Any()) return new List<AnswerFilterData>();

            var filter = Builders<ITOK.Core.Data.DTOs.Answer>.Filter.In("_id", ids.ToList());

            var list = _mongoRepository.GetCollection<ITOK.Core.Data.DTOs.Answer>().Find(filter).ToList();

            return list
               .Select(x => hydrate(x))
               .ToList();

        }

        public IFindFluent<ITOK.Core.Data.DTOs.Answer, ITOK.Core.Data.DTOs.Answer> GenerateBasicQueryToGetFilterData(string[] tags, string searchText, bool allTags = true, int? skip = null, int? take = null)
        {
          var filter = Builders<ITOK.Core.Data.DTOs.Answer>.Filter.Empty;

            if (tags.Any())
            {
                if (allTags) {
                    filter = tags.Aggregate(filter,
                      (current, tag) => current & Builders<ITOK.Core.Data.DTOs.Answer>.Filter.Where(x => x.AnswerTags.Any() & x.AnswerTags.Any(y=>y.UrlSlug==tag)));

                    //filter = tags.Aggregate(filter,
                    //   (current, tag) => current & Builders<ITOK.Core.Data.DTOs.Answer>.Filter.ElemMatch(x=>x.AnswerTags,x=>x.UrlSlug==tag));
                }
                else{
                    filter = tags.Aggregate(filter,
                        (current, tag) => current | Builders<ITOK.Core.Data.DTOs.Answer>.Filter.ElemMatch(x=>x.AnswerTags,x=>x.Text==tag));
                }
            }

            if (!string.IsNullOrWhiteSpace(searchText) && searchText != "undefined")
            {
                var searchTerms = searchText.ToLower().Split(' ');
                var sb = new StringBuilder("^");
                foreach (var term in searchTerms.Where(term => !string.IsNullOrEmpty(term)))
                {
                    sb.Append(string.Format("(?=.*?({0}))", term));
                }
                sb.Append(".*$");
                var re = new Regex(sb.ToString(), RegexOptions.IgnoreCase | RegexOptions.Multiline);

                filter = filter & (Builders<ITOK.Core.Data.DTOs.Answer>.Filter.Regex(x => x.QuestionText, re) | Builders<ITOK.Core.Data.DTOs.Answer>.Filter.Regex(x => x.AnswerValue, re));
                //filter = filter | Builders<ITOK.Core.Data.DTOs.Answer>.Filter.Regex(x => x.AnswerValue, re);
            }

            var query = _mongoRepository.GetCollection<ITOK.Core.Data.DTOs.Answer>().Find(filter, new FindOptions() { });

            query = query.Skip(skip).Limit(take);

            return query;
        }

        public IList<AnswerFilterData> GetPagedOrderedBy(SortOrder sortOrder, int skip, int take, string[] tags, bool allTags, string searchText, bool ascending = false, string userId = null)
        {
            var query = GenerateBasicQueryToGetFilterData(tags, searchText, allTags, skip, take);
          
            return query.ToList()
                .Select(x => hydrate(x, userId))
                .ToList();
        }

        public IList<AnswerFilterData> GetFavoriteAnswers(string userId, int skip, int take)
        {
            var answerIds = _mongoRepository.AsQueryable<ITOK.Core.Data.DTOs.FavoriteStat>()
               .Where(x => x.UserId == userId && x.StatisticType == FavoriteStatType.Yes).OrderBy(x => x.Timestamp).Skip(skip).Take(take).Select(x => x.ArticleId).ToArray();
            return FindByIds(answerIds);
        }

        public IList<string> GetFavoriteAnswerIds(string userId, int skip, int? take)
        {
            var FavoriteStat = _mongoRepository.AsQueryable<ITOK.Core.Data.DTOs.FavoriteStat>()
                .Where(x => x.UserId == userId && x.StatisticType == FavoriteStatType.Yes);

            return take != null ? FavoriteStat.Skip(skip).Take(take.Value).Select(x => x.ArticleId).ToArray() : FavoriteStat.Skip(skip).Select(x => x.ArticleId).ToArray();
        }

        public IList<AnswerFilterData> GetReadingList(string readingListId, int skip, int take)
        {
            var answerIds = _mongoRepository.AsQueryable<ITOK.Core.Data.DTOs.ReadingStat>()
                .Where(x => x.ReadingListId == readingListId).OrderBy(x => x.Timestamp).Skip(skip).Take(take).Select(x => x.ArticleId).ToArray();
            return FindByIds(answerIds);
        }

        public bool RemoveAnswerFromReadingList(string readingListId, string answerId)
        {
            ITOK.Core.Data.DTOs.ReadingStat readingStats = _mongoRepository
                .AsQueryable<ReadingStat>().FirstOrDefault(x => x.ReadingListId == readingListId && x.ArticleId == answerId);

            if (readingStats != null)
            {
                _mongoRepository.Remove(readingStats, readingStats.Id);
                return true;
            }

            return false;
        }

        public AnswerFilterData hydrate(ITOK.Core.Data.DTOs.Answer answer, string userId = null)
        {
            if (answer == null) return null;
            var viewModel = Mapper.Map<ITOK.Core.Data.DTOs.Answer, AnswerFilterData>(answer);
            // code for attachment name
            if (answer.AnswerType == AnswerType.Attachment)
            {
                var _ids = answer.AttachmentId.Split(',');

                var mediaTitles = _mongoRepository.AsQueryable<ITOK.Core.Data.DTOs.Media>()
                 .Where(x => _ids.Contains(x.Id)).ToList();

                foreach (var media in mediaTitles)
                {
                    viewModel.AttachmentMedias.Add(new AttachmentMedia { Id = media.Id, Title = media.Title });
                }
            }
                foreach (var item in viewModel.AnswerTags)
            {
                var _tags = _tagsService.FindByUrlSlug(new string[] { item.UrlSlug });
                if(_tags.Count>0)
                item.TagType = _tags.FirstOrDefault().TagType;
            }
            viewModel.FavCount = FavouriteCount(viewModel.Id);
            if (!string.IsNullOrEmpty(userId))
            {
                viewModel.IsFavoriteForCurrentUser = isUserFavourited(viewModel.Id, userId);
                viewModel.IsReadingListForCurrentUser = isUserReadingList(viewModel.Id, userId);
            }
            var readingListCount = ReadingListCount(viewModel.Id);
            viewModel.ReadingListCount = readingListCount ?? 0;
            return viewModel;
        }

        private int TotalViewCount(string answerId)
        {
            return _mongoRepository.AsQueryable<ITOK.Core.Data.DTOs.ArticleStat>()
                    .Where(x => x.ArticleId == answerId)
                    .Select(x => x.UserId)
                    .Distinct()
                    .Count();
        }

        public int FavouriteCount(string answerId)
        {
            return
                _mongoRepository.AsQueryable<ITOK.Core.Data.DTOs.FavoriteStat>()
                    .Count(x => x.ArticleId == answerId && x.StatisticType == FavoriteStatType.Yes);
        }
        public bool isUserFavourited(string answerId, string userId)
        {
            return Convert.ToBoolean(_mongoRepository.AsQueryable<ITOK.Core.Data.DTOs.FavoriteStat>().Count(x => x.ArticleId == answerId && x.StatisticType == FavoriteStatType.Yes && x.UserId == userId));
        }
        private int? ReadingListCount(string answerId)
        {
            return _mongoRepository.AsQueryable<ITOK.Core.Data.DTOs.ReadingStat>().Count(x => x.ArticleId == answerId);
        }

        private bool isUserReadingList(string answerId, string userId)
        {
            var readingLists = _mongoRepository.AsQueryable<ITOK.Core.Data.DTOs.ReadingList>().Where(x => x.UserId == userId).ToList();

            foreach (var item in readingLists)
            {
                var readingStat = _mongoRepository.AsQueryable<ITOK.Core.Data.DTOs.ReadingStat>().FirstOrDefault(x => x.ReadingListId == item.Id && x.ArticleId == answerId);

                if (readingStat != null)
                {
                    return true;
                }
            }

            return false;
        }



    }

    public interface IAnswerFilterService
    {
        IList<AnswerFilterData> GetFavoriteAnswers(string userId, int skip, int take);

        IList<string> GetFavoriteAnswerIds(string userId, int skip, int? take);

        IList<AnswerFilterData> GetReadingList(string readingListId, int skip, int take);

        AnswerFilterData hydrate(ITOK.Core.Data.DTOs.Answer answer, string userId = null);

        IFindFluent<ITOK.Core.Data.DTOs.Answer, ITOK.Core.Data.DTOs.Answer> GenerateBasicQueryToGetFilterData(string[] tags, string searchText, bool allTags = true,
            int? skip = null, int? take = null);

        IList<AnswerFilterData> GetPagedOrderedBy(SortOrder sortOrder, int skip, int take, string[] tags, bool allTags, string searchText, bool ascending = false, string userId = null);
        bool RemoveAnswerFromReadingList(string readingListId, string answerId);
    }
}
