﻿using System;
using AutoMapper;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Text.RegularExpressions;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using ITOK.Core.Data.Model.EqualityComparers;
using ITOK.Core.Services.Core.Wrapped;
using ITOK.Core.Common.Constants;

namespace ITOK.Core.Services.Core
{
    public class AlertService : BaseService
    {
        private readonly IAlertServiceWrapped _alertServiceWrapped;

        public AlertService(IMongoRepository1 mongoRepository, IAlertServiceWrapped AlertServiceWrapped = null)
            : base(mongoRepository)
        {
            if (AlertServiceWrapped == null)
                AlertServiceWrapped = new AlertServiceWrapped(mongoRepository);
            _alertServiceWrapped = AlertServiceWrapped;
        }

        public IList<Alert> All()
        {
            return _alertServiceWrapped.All();
        }
           

     
        public IList<Alert> FindByIds(params string[] ids)
        {
            return _alertServiceWrapped.FindByIds(ids);
        }

        public Alert FindById(string id)
        {
            return _alertServiceWrapped.FindById(id);
        }


        public IList<Alert> FindByBrand(Brand brand, bool onlyActive)
        {
            return _alertServiceWrapped.FindByBrand(brand, onlyActive);
        }

        public Data.DTOs.Alert deHydrate(Data.Model.Alert viewModel)
        {
            
            var article = Mapper.Map<Data.Model.Alert, Data.DTOs.Alert>(viewModel);
            return article;
        }

        public Data.Model.Alert hydrate(Data.DTOs.Alert alert)
        {
            if (alert == null) return null;
            // If hydration settings arent supplied then use defaults

            var viewModel = Mapper.Map<Data.DTOs.Alert, Alert>(alert);

            //include viewcount

            return viewModel;
        }

        public void Save(Alert alert)
        {
            var _alert = deHydrate(alert);


            if (alert.Id == null)
            {
                _alert.CreatedOn = alert.UpdatedOn = DateTime.Now;

                MongoRepository.Insert(_alert);
            }
            else
            {
            var previousalert = FindById(alert.Id);

            _alert.CreatedOn = previousalert.CreatedOn;
            _alert.UpdatedOn = DateTime.Now;

                MongoRepository.Update(_alert);
            }
        }

        public void Insert(Alert alert)
        {
            var _alert = deHydrate(alert);
            MongoRepository.Insert(_alert);
        }

        public void Delete(string id)
        {
            var deletealert = FindById(id);

            var filter = Builders<Alert>.Filter.Eq("_id", id);
            MongoRepository.GetCollection<Alert>().DeleteOne(filter);
        }
    }
}