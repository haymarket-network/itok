﻿using System;
using System.Collections.Generic;
using System.Linq;
using ITOK.Core.Common;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;

using ITOK.Core.Services;

namespace ITOK.Core.Services.Core
{
    public class SiteSettingsService : BaseService, ISiteSettingsService
    {
        public SiteSettingsService(IMongoRepository1 mongoRepository)
            : base(mongoRepository)
        {
        }

        public IList<SiteSettings> GetAll()
        {
            var query = MongoRepository.AsQueryable<SiteSettings>();
            return query.ToList();
        }

        public SiteSettings ById(string id)
        {
            if (string.IsNullOrEmpty(id)) return null;
            return MongoRepository.AsQueryable<SiteSettings>().SingleOrDefault(x => x.Id == id.ToLower());
        }

        public SiteSettings ByKey(string Key)
        {
            return string.IsNullOrEmpty(Key) ? null : MongoRepository.AsQueryable<SiteSettings>().SingleOrDefault(x => x.Key == Key);
        }

        public string GetCacheBuster()
        {
            var firstOrDefault = MongoRepository.AsQueryable<SiteSettings>().FirstOrDefault(x => x.Key == "CacheBuster");
            if (firstOrDefault == null) return DateTime.Today.ToBinary().ToString();
            var siteSetting = firstOrDefault.Value;
            return siteSetting;
        }

        public static string GetKey(string key)
        {
            var siteSettingsService = new SiteSettingsService(AppKernel.GetInstance<IMongoRepository1>());

            return siteSettingsService.ByKey(key).Value;
        }
    }

    public interface ISiteSettingsService
    {
       ITOK.Core.Data.Model.SiteSettings ById(string id);
        ITOK.Core.Data.Model.SiteSettings ByKey(string Key);
        IList<SiteSettings> GetAll();
        string GetCacheBuster();
        
    }
}