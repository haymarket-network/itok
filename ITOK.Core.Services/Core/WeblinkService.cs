﻿using AutoMapper;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Text.RegularExpressions;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using ITOK.Core.Data.Model.EqualityComparers;
using ITOK.Core.Services.Core.Wrapped;
using ITOK.Core.Common.Constants;

namespace ITOK.Core.Services.Core
{
    public class WeblinkService : BaseService
    {
        private readonly IWeblinkServiceWrapped _WeblinkServiceWrapped;

        public WeblinkService(IMongoRepository1 mongoRepository, IWeblinkServiceWrapped WeblinkServiceWrapped = null)
            : base(mongoRepository)
        {
            if (WeblinkServiceWrapped == null)
                WeblinkServiceWrapped = new WeblinkServiceWrapped(mongoRepository);
            _WeblinkServiceWrapped = WeblinkServiceWrapped;
        }

        public IList<Weblink> All()
        {
            return _WeblinkServiceWrapped.All();
        }
           

     
        public IList<Weblink> FindByIds(params string[] ids)
        {
            return _WeblinkServiceWrapped.FindByIds(ids);
        }

        public Weblink FindById(string id)
        {
            return _WeblinkServiceWrapped.FindById(id);
        }
            

        public IList<Weblink> FindByBrand(Brand brand)
        {
            return _WeblinkServiceWrapped.FindByBrand(brand);
        }

        public Data.DTOs.Weblink deHydrate(Data.Model.Weblink viewModel)
        {
            var article = Mapper.Map<Data.Model.Weblink, Data.DTOs.Weblink>(viewModel);
            return article;
        }

        public Data.Model.Weblink hydrate(Data.DTOs.Weblink tag)
        {
            if (tag == null) return null;
            // If hydration settings arent supplied then use defaults

            var viewModel = Mapper.Map<Data.DTOs.Weblink, Weblink>(tag);

            //include viewcount

            return viewModel;
        }

        public void Save(Weblink tag)
        {
            var _tag = deHydrate(tag);

            var previousTag = FindById(tag.Id);

            if (tag.Id == null) MongoRepository.Insert(_tag);
            else MongoRepository.Update(_tag);
        }

        public void Insert(Weblink tag)
        {
            var _tag = deHydrate(tag);
            MongoRepository.Insert(_tag);
        }

        public void Delete(string id)
        {
            var deleteTag = FindById(id);

            var filter = Builders<Weblink>.Filter.Eq("_id", id);
            MongoRepository.GetCollection<Weblink>().DeleteOne(filter);
        }
    }
}