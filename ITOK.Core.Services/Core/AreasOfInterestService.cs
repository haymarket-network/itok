﻿using AutoMapper;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using ITOK.Core.Data.Model.EqualityComparers;
using ITOK.Core.Services.Core.Wrapped;

namespace ITOK.Core.Services.Core
{
    public class AreasOfInterestService : BaseService
    {
        private readonly IAreasOfInterestServiceWrapped _areasOfInterestServiceWrapped;

        public AreasOfInterestService(IMongoRepository1 mongoRepository, IAreasOfInterestServiceWrapped areasOfInterestServiceWrapped = null)
            : base(mongoRepository)
        {
            if (areasOfInterestServiceWrapped == null)
                areasOfInterestServiceWrapped = new AreasOfInterestServiceWrapped(mongoRepository);
            _areasOfInterestServiceWrapped = areasOfInterestServiceWrapped;
        }

        public IList<AreasOfInterest> All()
        {
            return _areasOfInterestServiceWrapped.All();
        }

    


        public IList<AreasOfInterest> FindByIds(params string[] ids)
        {
            return _areasOfInterestServiceWrapped.FindByIds(ids);
        }

        public AreasOfInterest FindById(string id)
        {
            return _areasOfInterestServiceWrapped.FindById(id);
        }

        public AreasOfInterest FindByUrlSlug(string UrlSlug)
        {
            return _areasOfInterestServiceWrapped.FindByUrlSlug(UrlSlug);
        }

        public List<Data.Model.AreasOfInterest> FindByParent(string urlSlug)
        {
            return _areasOfInterestServiceWrapped.FindByParent(urlSlug);
        }

     

        public Data.DTOs.AreasOfInterest deHydrate(Data.Model.AreasOfInterest viewModel)
        {
            var article = Mapper.Map<Data.Model.AreasOfInterest, Data.DTOs.AreasOfInterest>(viewModel);
            return article;
        }

        public Data.Model.AreasOfInterest hydrate(Data.DTOs.AreasOfInterest tag)
        {
            if (tag == null) return null;
            // If hydration settings arent supplied then use defaults

            var viewModel = Mapper.Map<Data.DTOs.AreasOfInterest, AreasOfInterest>(tag);

            //include viewcount

            return viewModel;
        }

        public void Save(AreasOfInterest area)
        {
            var _tag = deHydrate(area);

           

            //#region Bulk Update Tags in Articles
            //if (previousTag != null)
            //{ 
            ////UPDATES RELATED TAGS
            //var filter = Builders<Article>.Filter.ElemMatch(x => x.RelatedTags, x => x.DisplayText == previousTag.DisplayText);
            //var update = Builders<Article>.Update.Set(x => x.RelatedTags[-1].DisplayText, tag.DisplayText)
            //    .Set(x => x.RelatedTags[-1].UrlSlug, tag.UrlSlug);
           
            //    MongoRepository.GetCollection<Article>().UpdateMany(filter, update);



            ////UPDATES TAGS ARRAY
            //var filterForUrlSlug = Builders<Article>.Filter.AnyIn(x => x.Tags, new[] { previousTag.UrlSlug });
            //var updateForUrlSlugPush = Builders<Article>.Update.Push(x => x.Tags, tag.UrlSlug);
            //var updateForUrlSlugPull = Builders<Article>.Update.Pull(x => x.Tags, previousTag.UrlSlug);


            //    MongoRepository.GetCollection<Article>().UpdateMany(filterForUrlSlug, updateForUrlSlugPush);
            //MongoRepository.GetCollection<Article>().UpdateMany(filterForUrlSlug, updateForUrlSlugPull);

            //}
            //#endregion

            if (area.Id == null) MongoRepository.Insert(_tag);
            else MongoRepository.Update(_tag);
        }

        public void Insert(AreasOfInterest tag)
        {
            var _tag = deHydrate(tag);
            MongoRepository.Insert(_tag);
        }

        public void Delete(string id)
        {
            var filter = Builders<AreasOfInterest>.Filter.Eq("_id", id);
            MongoRepository.GetCollection<AreasOfInterest>().DeleteOne(filter);
        }
    }
}