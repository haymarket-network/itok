﻿using System.Collections.Generic;
using System.Linq;
using ITOK.Core.Data;
using ITOK.Core.Data.DTOs;
using ITOK.Core.Services.Core.Wrapped;

namespace ITOK.Core.Services.Core
{
    public class SearchService : BaseService
    {
        private ISearchServiceWrapped _SearchServiceWrapped;

        public SearchService(IMongoRepository1 mongoRepository, ISearchServiceWrapped SearchServiceWrapped = null)
            : base(mongoRepository)
        {
            if (SearchServiceWrapped == null)
                SearchServiceWrapped = new SearchServiceWrapped(mongoRepository);
            _SearchServiceWrapped = SearchServiceWrapped;
        }



        public void Save(Search search)
        {

            if (search.Id == null) MongoRepository.Insert(search);
            else MongoRepository.Update(search);
        }




        public IList<Search> GetFavoriteArticleIds(string userId, int? skip = 0, int? take = 20)
        {
            return MongoRepository.AsQueryable<Search>()
                .Where(x => x.UserId == userId)
                //.Skip( skip)
                //.Take(take)
                .ToList();
        }

        public Search FindById(string searchId)
        {
            return MongoRepository.AsQueryable<Search>( )
                .FirstOrDefault(x => x.Id == searchId);
        }
    }
}
