﻿using AutoMapper;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using ITOK.Core.Common.Constants;
using ITOK.Core.Common.Utils;
using ITOK.Core.Data;
using ITOK.Core.Data.Model.HS;
using ITOK.Core.Services.Core.Wrapped;
using Tags = ITOK.Core.Data.DTOs.Tags;
using ITOK.Core.Common;

namespace ITOK.Core.Services.Core
{
    public class SubmittedArticleService : BaseService
    {
        private readonly StatsService _statService;

        private readonly ITagsService _tagsSerice;

        private readonly FavoriteCountService _favoriteCountService;
        private readonly ReadingListService _readingListService;

        private readonly ISubmittedArticleServiceWrapped _articleServiceWrapped;

        public SubmittedArticleService(IMongoRepository1 mongoRepository, ISubmittedArticleServiceWrapped articleServiceWrapped = null)
            : base(mongoRepository)
        {
            _statService = new StatsService(mongoRepository);
            _tagsSerice = AppKernel.GetInstance<ITagsService>();
            _favoriteCountService =new FavoriteCountService(mongoRepository);
            _readingListService = new ReadingListService(mongoRepository);
            if (articleServiceWrapped == null)
                articleServiceWrapped = new SubmittedArticleServiceWrapped(mongoRepository);
            _articleServiceWrapped = articleServiceWrapped;
        }

        /// <summary>
        /// The maximnum years back to search for ThisDayIn articles.
        /// </summary>

        private const int CleanStatsOlderThanDays = 10;

        private const int MaxUniqueUrlSlugChecks = 100; // Prevent infinate loops

        public ITOK.Core.Data.Model.SubmittedArticle FindBySlug(Brand brand,string urlSlug, SubmittedArticleHydrationSettings hydrationSettings, SubmissionStatus[] showWithStatus = null, bool showOnlyLive = false)
        {
            return _articleServiceWrapped.FindBySlug(brand,urlSlug, hydrationSettings, showWithStatus, showOnlyLive);
        }

        public List<ITOK.Core.Data.Model.SubmittedArticle> FindAllBySlug(Brand brand, string urlSlug, SubmittedArticleHydrationSettings hydrationSettings, SubmissionStatus[] showWithStatus = null, bool showOnlyLive = false)
        {
            return _articleServiceWrapped.FindAllBySlug(brand,urlSlug, hydrationSettings, showWithStatus, showOnlyLive);
        }

        public List<ITOK.Core.Data.Model.SubmittedArticle> FindByBrandArticleNumber(Brand brand, string brandArticleNumber, SubmittedArticleHydrationSettings hydrationSettings, SubmissionStatus[] showWithStatus = null, bool showOnlyLive = false)
        {
            return _articleServiceWrapped.FindByBrandArticleNumber(brand,brandArticleNumber, hydrationSettings, showWithStatus, showOnlyLive).ToList();
        }

        public List<ITOK.Core.Data.Model.SubmittedArticle> FindBySEOTitle(Brand brand,string seoTitle , SubmittedArticleHydrationSettings hydrationSettings, SubmissionStatus[] showWithStatus = null, bool showOnlyLive = false)
        {
            return _articleServiceWrapped.FindBySEOTitle(brand,seoTitle,  hydrationSettings, showWithStatus, showOnlyLive).ToList();
        }

        public List<ITOK.Core.Data.Model.SubmittedArticle> FindByBrandArticleNumberStartWith(Brand brand, string brandArticleNumber, SubmittedArticleHydrationSettings hydrationSettings, int? limit = null, SubmissionStatus[] showWithStatus = null, bool showOnlyLive = false)
        {
            //return _articleServiceWrapped.FindByBrandArticleNumberStartWith(brandArticleNumber, hydrationSettings, showWithStatus, showOnlyLive).ToList();
            var articles = MongoRepository.AsQueryable<Data.DTOs.SubmittedArticle>()
               .Where(x => x.BrandArticleNumber.ToLower().StartsWith(brandArticleNumber.ToLower()));
            var result = articles;
            if(limit!=null)
                result = articles.Take((int)limit);
             return result.ToList()
               .Select(x => _articleServiceWrapped.hydrate(x, hydrationSettings)).ToList();
              
        }

        public ITOK.Core.Data.Model.SubmittedArticle FindById(Brand brand,string id, SubmittedArticleHydrationSettings hydrationSettings)
        {
            return MongoRepository.AsQueryable<Data.DTOs.SubmittedArticle>()
                .Where(x => x.Id == id && x.brand == brand).ToList()
                .Select(x => _articleServiceWrapped.hydrate(x, hydrationSettings))
                .SingleOrDefault();
        }

        public IList<ITOK.Core.Data.Model.SubmittedArticle> FindByIds(Brand brand, SubmittedArticleHydrationSettings hydrationSettings = null, params string[] ids)
        {
            return _articleServiceWrapped.FindByIds(brand, hydrationSettings, ids);
        }

        public IEnumerable<ITOK.Core.Data.Model.SubmittedArticle> FindByUrlSlugs(Brand brand, SubmittedArticleHydrationSettings hydrationSettings = null, params string[] articleIds)
        {
        return _articleServiceWrapped.FindByIds(brand, hydrationSettings, articleIds);
        }
    


        public int GetCount(Brand brand,  string[] tags,SubmissionStatus[] showWithStatus = null, bool showOnlyLive = false, SubmissionStatus? submissionStatus = null, bool allTags = true, string userID = "")
            {
            return _articleServiceWrapped.GetCount(brand, tags, showWithStatus, showOnlyLive, submissionStatus, allTags, userID);
            }


        public IList<Data.Model.SubmittedArticle> Search(Brand brand, string searchText, int skip, int take, bool searchTitle = true,
         bool searchMeta = false, bool searchBody = false, SubmissionStatus[] showWithStatus = null,
         bool showOnlyLive = false, SubmissionStatus? submissionStatus = null,
         SubmittedArticleHydrationSettings hydrationSettings = null, string userID = "")
        {
            return _articleServiceWrapped.Search(brand, searchText, skip, take, searchTitle,
             searchMeta, searchBody, showWithStatus,
             showOnlyLive, submissionStatus,
             hydrationSettings, userID);
        }

        public IList<Data.Model.SubmittedArticle> Search(Brand brand, string searchText, bool searchTitle = true, bool searchMeta = false,
            bool searchBody = false, int resultLimit = 10, SubmissionStatus[] showWithStatus = null,
            bool showOnlyLive = false, SubmissionStatus? submissionStatus = null,
            SubmittedArticleHydrationSettings hydrationSettings = null)
        {
            return _articleServiceWrapped.Search(brand, searchText, searchTitle, searchMeta,
            searchBody, resultLimit, showWithStatus,
            showOnlyLive, submissionStatus,
            hydrationSettings);
        }



        public IList<ITOK.Core.Data.Model.SubmittedArticle> GetPagedOrderedBy(Brand brand, SortOrder sortOrder, int skip, int take, string[] tags,
            SubmissionStatus[] showWithStatus = null, bool showOnlyLive = false, bool ascending = false,
            SubmissionStatus? submissionStatus = null, SubmittedArticleHydrationSettings hydrationSettings = null, bool allTags = true, string userID = "" )
        {
            return _articleServiceWrapped.GetPagedOrderedBy(brand, sortOrder, skip, take, tags,
            showWithStatus, showOnlyLive, ascending,
            submissionStatus, hydrationSettings, allTags, userID );
        }

        public IList<ITOK.Core.Data.Model.SubmittedArticle> GetPagedInDateRangeOrderedBy(Brand brand, SortOrder sortOrder, int daysAgo, int skip, int? take, string[] tags, List<string> ids,
           SubmissionStatus[] showWithStatus = null, bool showOnlyLive = false, bool ascending = false,
           SubmissionStatus? submissionStatus = null, SubmittedArticleHydrationSettings hydrationSettings = null, bool allTags = true)
        {
            return _articleServiceWrapped.GetPagedInDateRangeOrderedBy(brand, sortOrder, daysAgo, skip, take,  tags, ids,
            showWithStatus, showOnlyLive, ascending,
            submissionStatus, hydrationSettings, allTags);
        }



     


        public int GetInDateRangeCount(Brand brand,  int daysAgo,string[] tags,   List<string> ids,SubmissionStatus[] showWithStatus = null, bool showOnlyLive = false, SubmissionStatus? submissionStatus = null, bool allTags = true )
            {
            return _articleServiceWrapped.GetCountInDateRange(brand,  daysAgo,  tags, ids,showWithStatus, showOnlyLive, submissionStatus, allTags);
            }


    


        //public int GetCount(string[] tags, SubmissionStatus[] showWithStatus = null, bool showOnlyLive = false,
        //    ArticleType? articleType = null)
        //{
        //    return _articleServiceWrapped.GetCount(tags, showWithStatus, showOnlyLive, articleType);
        //}

        #region Search



        #endregion Search

        #region Popular





        public IList<ITOK.Core.Data.Model.SubmittedArticle> GetPopularArticlesInJobRole(Brand brand, string jobRoleId, int take = 10, int hours = -2, SubmittedArticleHydrationSettings hydrationSettings = null)
        {
        var articleIds = _statService.FindArticleList(jobRoleId, DateTime.Now.AddHours(hours), take).ToArray().Select(x => x["_id"].AsBsonDocument.GetElement("_id").Value.ToString()).ToArray();

        return FindByIds(brand, hydrationSettings, articleIds).ToList();
        }

        public List<Data.Model.Tags> GetPopularHubsInJobRole(Brand brand,string jobRoleId, int take = 10, int hours = -2)
        {
        var urlslugs = _statService.FindHubsList(brand, jobRoleId, DateTime.Now.AddHours(hours), take).ToArray().Select(x => x["_id"].AsBsonDocument.GetElement("UrlSlug").Value.ToString()).ToArray();

            return _tagsSerice.FindByUrlSlug(urlslugs).ToList();
        }

        public string[] GetPopularHubsInJobRoleArray(Brand brand, string jobRoleId, int i = 10, int hours = -2)
        {
            
            return _statService.FindHubsList(brand,jobRoleId, DateTime.Now.AddHours(hours), i).ToArray().Select(x => x["_id"].AsBsonDocument.GetElement("UrlSlug").Value.ToString()).ToArray();
        }

        #endregion Popular

        //public void RegisterStatisticView(ArticleType articleType, string urlslug)
        //{
        //    var articleStat = new Data.DTOs.ArticleStat
        //    {
        //        Timestamp = DateTime.UtcNow,
        //        StatisticType = ArticleStatType.View,
        //        UrlSlug = urlslug
        //    };

        //    MongoRepository.Insert(articleStat);
        //}

        /// <summary>
        /// TODO: Multiple requests to the db. MapReduce could make this much faster.
        /// </summary>
        /// <param name="statisticType"></param>
        /// <param name="limit"></param>
        /// <param name="articleType"></param>
        /// <param name="hydrationSettings"></param>
        /// <returns></returns>
        ///
             

        /// <summary>
        /// TODO: Multiple requests to the db. MapReduce could make this much faster.
        /// Returns popular tags.
        /// </summary>
        /// <returns>A list of popular tags for the URL Slug</returns>
        public IList<Tags> GetPopularTagsList(string urlSlug)
        {
            if (urlSlug == null || !urlSlug.Any()) return new List<Tags>();
            return MongoRepository.AsQueryable<Data.DTOs.Tags>()
                .Where(x => x.UrlSlug == urlSlug)
                .ToList();
        }
        public List<Data.Model.SubmittedArticle> FindRelatedTagsForDelete(Brand brand, string tag, SubmissionStatus[] showWithStatus, bool showOnlyLive, bool b1)
        {
            return _articleServiceWrapped.FindRelatedTagsForDelete(brand, tag, showWithStatus, showOnlyLive, b1);
        }

        public List<BsonDocument> FindRelatedTags(Brand brand, string[] tags, SubmissionStatus[] showWithStatus, bool showOnlyLive, bool b1, List<string> ids = null )
        {
            return _articleServiceWrapped.FindRelatedTags(brand, tags, showWithStatus, showOnlyLive, b1, ids);
        }

        #region Private Methods

        protected IQueryable<Data.DTOs.Article> GenerateMostViewedQuery(SubmissionStatus[] showWithStatus = null,
            bool showOnlyLive = true, ArticleType? articleType = null)
        {
            var query = MongoRepository.AsQueryable<Data.DTOs.Article>().Where(i => i.LiveFrom > DateTime.Now.AddDays(-10));
            if (showWithStatus != null)
            {
                IEnumerable<int> showWithStatusInts = showWithStatus.Select(x => (int)x).ToList();
                query = query.Where(x => showWithStatusInts.Contains((int)x.Status));
            }
            if (showOnlyLive)
                query = query.Where(x => x.LiveFrom < DateTime.Now);

            if (articleType != null)
                query = articleType != null ? query.Where(x => x.ArticleType == (ArticleType)articleType) : query.Where(x => x.ArticleType != ArticleType.article);

            return query;
        }

        /// <summary>
        /// TO DO: Include related Sugar companies, training courses & publications
        /// </summary>
        /// <param name="article"></param>
        /// <param name="hydrationSettings"></param>
        /// <returns></returns>

        //protected Data.Model.Article hydrate(Data.DTOs.Article article, SubmittedArticleHydrationSettings hydrationSettings)
        //{
        //    if (article == null) return null;
        //    // If hydration settings arent supplied then use defaults
        //    if (hydrationSettings == null) hydrationSettings = new SubmittedArticleHydrationSettings();

        //    var viewModel = Mapper.Map<Data.DTOs.Article, ITOK.Core.Data.Model.SubmittedArticle>(article);

        //    if (hydrationSettings.PrimaryMedia != null || hydrationSettings.RelatedMedia != null)
        //    {
        //        var mediaService = new MediaService(base.MongoRepository);
        //        if (hydrationSettings.PrimaryMedia != null)
        //            viewModel.PrimaryMedia =
        //                mediaService.FindByIds(hydrationSettings.PrimaryMedia, article.PrimaryMediaId).FirstOrDefault();
        //        if (hydrationSettings.RelatedMedia != null && article.RelatedMediaIds != null)
        //            viewModel.RelatedMedia = mediaService.FindByIds(hydrationSettings.RelatedMedia,
        //                article.RelatedMediaIds.ToArray());
        //    }

        //    if (hydrationSettings.RelatedArticles != null && article.RelatedArticleIds != null)
        //        viewModel.RelatedArticles = FindByIds(hydrationSettings.RelatedArticles,
        //            article.RelatedArticleIds.ToArray());

        //    if (hydrationSettings.RelatedGallery != null && article.RelatedGalleryIds != null)
        //    {
        //        var mediaServiceG = new MediaService(base.MongoRepository);
        //        viewModel.RelatedGallery = mediaServiceG.FindByIds(hydrationSettings.RelatedGallery,
        //                article.RelatedGalleryIds.ToArray());
        //    }

        //    if (hydrationSettings.RelatedDownload != null && article.RelatedDownloadIds != null)
        //    {
        //        var mediaServiceG = new MediaService(base.MongoRepository);
        //        viewModel.RelatedDownload = mediaServiceG.FindByIds(hydrationSettings.RelatedDownload,
        //                article.RelatedDownloadIds.ToArray());
        //    }

        //    //if (hydrationSettings.RelatedTags != null && article.RelatedTagIds != null)
        //    //{
        //    //    var tagService = new TagsService();
        //    //    viewModel.RelatedTags = tagService.FindByIds(article.RelatedTagIds.ToArray());
        //    //}
        //    if (hydrationSettings.RelatedJobs != null && article.RelatedJobRoleIds != null)
        //    {
        //        var jobService = new JobRoleService(base.MongoRepository);
        //        viewModel.RelatedJobRoles = jobService.FindByIds(hydrationSettings.RelatedJobs, article.RelatedJobRoleIds.ToArray());
        //    }

        //    if (hydrationSettings.CreatedBy != null && !string.IsNullOrEmpty(article.CreatedById))
        //    {
        //        var userService = new UserService(base.MongoRepository);
        //        viewModel.CreatedBy = userService.FindById(article.CreatedById);
        //    }
        //    if (hydrationSettings.UpdatedBy == null || string.IsNullOrEmpty(article.UpdatedById)) return viewModel;
        //    {
        //        var userService = new UserService(base.MongoRepository);
        //        viewModel.UpdatedBy = userService.FindById(article.UpdatedById);
        //    }

        //    viewModel.LiveFrom = (article.LiveFrom == DateTime.MinValue) ? null : article.LiveFrom as DateTime?;

        //    if (article.LiveFrom == DateTime.MinValue) viewModel.LiveFrom = null;
        //    else viewModel.LiveFrom = article.LiveFrom;
            
        //    //include viewcount

        //    var favCount = _favoriteCountService.FindByArticleId(article.Id);
        //    viewModel.FavCount = favCount == null ? 0 : favCount.YesCount;

        //    var readingListCount = _readingListService.ReadingListCount(article.UrlSlug);
        //    viewModel.ReadingListCount = readingListCount ?? 0;


        //    return viewModel;
        //}

        //protected Data.DTOs.Article deHydrate(Data.Model.Article viewModel)
        //{
        //    var article = Mapper.Map<Data.Model.Article, Data.DTOs.Article>(viewModel);


        //    article.brands = new[] {Brand.vw,}; //Todo:FIX ASAP
        //    article.LiveFrom = viewModel.LiveFrom ?? DateTime.MinValue;

        //    DateTimeUtilities.ToUtcPreserved(article.LiveFrom);
        //    DateTimeUtilities.ToUtcPreserved(article.LiveTo);

        //    return article;
        //}

        #endregion Private Methods

    

        public IList<Data.Model.SubmittedArticle> SearchArticles(Brand brand, string filter, int pageSize,  SubmittedArticleHydrationSettings _hydrationSettings)
            {

            return _articleServiceWrapped.SearchArticles(brand, filter, pageSize, _hydrationSettings);

            }


      
    }
}