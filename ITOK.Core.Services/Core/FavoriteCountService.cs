﻿using AutoMapper;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using ITOK.Core.Services.Core.Wrapped;

namespace ITOK.Core.Services.Core
{
    public class FavoriteCountService : BaseService
    {
        private IFavoriteCountServiceWrapped _FavoriteCountServiceWrapped;

        public FavoriteCountService(IMongoRepository1 mongoRepository, IFavoriteCountServiceWrapped FavoriteCountServiceWrapped = null)
            : base(mongoRepository)
        {
            if (FavoriteCountServiceWrapped == null)
                FavoriteCountServiceWrapped = new FavoriteCountServiceWrapped(mongoRepository);
            _FavoriteCountServiceWrapped = FavoriteCountServiceWrapped;
        }

        public IList<FavoriteCount> All()
        {
            return _FavoriteCountServiceWrapped.All();
        }

        public IList<FavoriteCount> FindByIds(params string[] ids)
        {
            if (ids == null || ids.Length == 0) return new List<FavoriteCount>();
            return MongoRepository.AsQueryable<Data.DTOs.FavoriteCount>()
                .Where(x => ids.Contains(x.Id))
                .Select(x => hydrate(x))
                .ToList();
        }

        public FavoriteCount FindById(string id)
        {
            return _FavoriteCountServiceWrapped.FindById(id);
        }

        public FavoriteCount FindByArticleId(string id)
        {
            var favitem = _FavoriteCountServiceWrapped.FindByArticleId(id);

            return favitem ?? new FavoriteCount{Article = new Article(), Id =  id,YesCount = 0,NoCount = 0};
        }
        public int FindByArticleIdCount(string articleId)
            {
            return _FavoriteCountServiceWrapped.FindByArticleIdCount(articleId);
           
            }



        public Data.DTOs.FavoriteCount deHydrate(Data.Model.FavoriteCount count)
        {
            var article = Mapper.Map<Data.Model.FavoriteCount, Data.DTOs.FavoriteCount>(count);
            return article;
        }

        public Data.Model.FavoriteCount hydrate(Data.DTOs.FavoriteCount count)
        {
            if (count == null) return null;
            // If hydration settings arent supplied then use defaults

            var viewModel = Mapper.Map<Data.DTOs.FavoriteCount, FavoriteCount>(count);

            //include viewcount

            return viewModel;
        }

        public void Save(FavoriteCount count)
        {
            var _count = deHydrate(count);

            if (_count.Id == null) MongoRepository.Insert(_count);
            else MongoRepository.Update(_count);
        }

        public void Insert(FavoriteCount count)
        {
            var _count = deHydrate(count);
            MongoRepository.Insert(_count);
        }

        public void Delete(string id)
        {
            var filter = Builders<FavoriteCount>.Filter.Eq("Id", id);
            MongoRepository.GetCollection<FavoriteCount>().DeleteOne(filter);
        }
    }
}