﻿using AutoMapper;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Text.RegularExpressions;
using MongoDB.Bson;
using ITOK.Core.Data;
using ITOK.Core.Data.Model.EqualityComparers;
using ITOK.Core.Services.Core.Wrapped;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data.DTOs;
using Article = ITOK.Core.Data.Model.Article;
using User = ITOK.Core.Data.Model.User;
using Tags = ITOK.Core.Data.Model.Tags;

namespace ITOK.Core.Services.Core
{
    public class TagsService : BaseService, ITagsService
    {
        protected ITagsServiceWrapped _tagsServiceWrapped;

        public TagsService(IMongoRepository1 mongoRepository, ITagsServiceWrapped tagsServiceWrapped)
        {
            _mongoRepository = mongoRepository;
            _tagsServiceWrapped = tagsServiceWrapped;
        }
        
        public IList<Tags> All()
        {
            return _tagsServiceWrapped.All();
        }

        public IList<Tags> AllTagsThatStartWith(string text)
        {
            return MongoRepository.AsQueryable<ITOK.Core.Data.DTOs.Tags>().Where(x => x.DisplayText.ToLower().StartsWith(text.ToLower())).OrderBy(x => x.DisplayText).ToList().Select(x => hydrate(x)).ToList();
        }

        public IList<Tags> AllTagsThatContains(string text)
        {
            return MongoRepository.AsQueryable<ITOK.Core.Data.DTOs.Tags>().Where(x => x.DisplayText.ToLower().Contains(text.ToLower())).OrderBy(x => x.DisplayText).ToList().Select(x => hydrate(x)).ToList();
        }

        public IList<Tags> AllTagsThatContainsWithoutStartWith(string text)
        {
            return MongoRepository.AsQueryable<ITOK.Core.Data.DTOs.Tags>().Where(x => x.DisplayText.ToLower().Contains(text.ToLower()) && !x.DisplayText.ToLower().StartsWith(text.ToLower())).OrderBy(x=>x.DisplayText).ToList().Select(x => hydrate(x)).ToList();
        }


        public IList<Tags> Search(string searchText, bool searchName = true, int resultLimit = 10)
        {
            if (string.IsNullOrWhiteSpace(searchText) || (!searchName)) return new List<Tags>();
            var foundTags = new List<Tags>();
            var searchTerms = searchText.ToLower().Split(' ');
            var sb = new StringBuilder("^");

            foreach (var term in searchTerms.Where(term => !string.IsNullOrWhiteSpace(term)))
                sb.Append(string.Format("(?=.*?({0}))", term));

            sb.Append(".*$");

            var re = new Regex(sb.ToString(), RegexOptions.IgnoreCase | RegexOptions.Multiline);
            var eventQuery = MongoRepository.AsQueryable<Tags>();

            if (searchName)
            {
                var nameQuery = eventQuery
                    .Where(x =>
                        re.IsMatch(x.DisplayText));
                foundTags.AddRange(nameQuery.ToList()
                    );
            }

            foundTags = foundTags.OrderByDescending(x => x.DisplayText).ToList();

            // Remove duplicates and crop to result limit
            foundTags = foundTags
                .Distinct(new TagEqualityComparer())
                .Take(resultLimit)
                .ToList();

            return foundTags;
        }

        public IList<Tags> FindByIds(params string[] ids)
        {
            return _tagsServiceWrapped.FindByIds(ids);
        }

        public IList<Tags> FindByTypeAndParent(TagType tagType,params string[] ids)
        {
            return _tagsServiceWrapped.FindByTypeAndParent(tagType,ids);
        }

        public Tags FindById(string id)
        {
            return _tagsServiceWrapped.FindById(id);
        }

        public Tags FindByUrlSlug(string UrlSlug, Brand brand=Brand.itok)
        {
            return _tagsServiceWrapped.FindByUrlSlug(UrlSlug);
        }
        public IList<Tags> FindByParentId(TagType tagtype,string id)
        {
            return _tagsServiceWrapped.FindByParentId(tagtype,id);
        }
        public List<Data.Model.Tags> FindByUrlSlug(string[] urlSlugs)
        {
            return _tagsServiceWrapped.FindByUrlSlug(urlSlugs);
        }

        public List<Data.Model.Tags> FindMenuItems(Brand brand)
        {
            return _tagsServiceWrapped.FindMenuItems(brand);
        }

        public Tags FindByDisplayText(string displayText, Brand brand)
        {
            return _tagsServiceWrapped.FindByDisplayText(displayText, brand);
        }

        public Data.DTOs.Tags deHydrate(Data.Model.Tags viewModel)
        {
            var article = Mapper.Map<Data.Model.Tags, Data.DTOs.Tags>(viewModel);
            return article;
        }

        public Data.Model.Tags hydrate(Data.DTOs.Tags tag)
        {
            if (tag == null) return null;
            // If hydration settings arent supplied then use defaults

            var viewModel = Mapper.Map<Data.DTOs.Tags, Tags>(tag);

            //include viewcount

            return viewModel;
        }

        public void Save(Tags tag)
        {
            var _tag = deHydrate(tag);

            var previousTag = FindById(tag.Id);

            #region Bulk Update Tags in Articles
            if (previousTag != null)
            { 
            //UPDATES RELATED TAGS
            var filter = Builders<Article>.Filter.ElemMatch(x => x.RelatedTags, x => x.DisplayText == previousTag.DisplayText);
            //use the brand of the tag
            filter = filter & filter & Builders<Article>.Filter.Eq("brand", previousTag.Brand);

                var update = Builders<Article>.Update.Set(x => x.RelatedTags[-1].DisplayText, tag.DisplayText)
                .Set(x => x.RelatedTags[-1].UrlSlug, tag.UrlSlug);
           
                MongoRepository.GetCollection<Article>().UpdateMany(filter, update);



                //UPDATES TAGS ARRAY
                if (previousTag.UrlSlug != tag.UrlSlug)
                {
                    var filterForUrlSlug = Builders<Article>.Filter.AnyIn(x => x.Tags, new[] { previousTag.UrlSlug });
                    //filter further by the brand of the tag
                    filter = filter & filter & Builders<Article>.Filter.Eq("brand", previousTag.Brand);
                    var updateForUrlSlugPush = Builders<Article>.Update.Push(x => x.Tags, tag.UrlSlug);                
                    var updateForUrlSlugPull = Builders<Article>.Update.Pull(x => x.Tags, previousTag.UrlSlug);


                    MongoRepository.GetCollection<Article>().UpdateMany(filterForUrlSlug, updateForUrlSlugPush);
                    MongoRepository.GetCollection<Article>().UpdateMany(filterForUrlSlug, updateForUrlSlugPull);
                }

            }
            #endregion

            if (tag.Id == null) MongoRepository.Insert(_tag);
            else MongoRepository.Update(_tag);
        }

        public void Insert(Tags tag)
        {
            var _tag = deHydrate(tag);
            MongoRepository.Insert(_tag);
        }

        public void Delete(string id)
        {
            var deleteTag = FindById(id);

            #region Bulk Update Tags in Articles
            if (deleteTag != null)
            {             

                //remove anywhere using the tag
                var filterForUrlSlug = Builders<Article>.Filter.AnyIn(x => x.Tags, new[] { deleteTag.UrlSlug });
                filterForUrlSlug = filterForUrlSlug & Builders<Article>.Filter.Eq("brand", deleteTag.Brand);
                //this bit works
                var updateForUrlSlugPull = Builders<Article>.Update.Pull(x => x.Tags, deleteTag.UrlSlug);
                MongoRepository.GetCollection<Article>().UpdateMany(filterForUrlSlug, updateForUrlSlugPull);


                var filter3 = Builders<Article>.Filter.ElemMatch(x => x.RelatedTags, x => x.UrlSlug == deleteTag.UrlSlug);
                filter3 = filter3 & Builders<Article>.Filter.Eq("brand", deleteTag.Brand);
                var update = Builders<Article>.Update.Pull("Article.RelatedTags", filter3);


                var query = MongoRepository.GetCollection<Article>().Find(filter3, new FindOptions() { });

                MongoRepository.GetCollection<Article>().UpdateMany(filter3, update);


            ///Todo:Test NH-128
               // CleanTagsInArticles(deleteTag.Brand, deleteTag.UrlSlug);


                //nope
                //var update = Builders<Article>.Update.PullFilter("Article.RelatedTags", filter3);
                // var update = Builders<Article>.Update.PullFilter("RelatedTags", filter3);
                // var update = Builders<Article>.Update.PullFilter("Article.RelatedTags.DisplayText", filter3);
                //var update = Builders<Article>.Update.Pull("Article.RelatedTags", filter3);
                //errors
                //var update = Builders<Article>.Update.Pull("RelatedTags", filter3);

                //MongoRepository.GetCollection<Article>().UpdateMany(filter3, update);


                //doesn't error, but doesn't work
                //
                //var filterForUrlSlug2 = Builders<Article>.Filter.AnyEq(x => x.RelatedTags, deleteTag);
                //var update2 = Builders<Article>.Update.Pull(x => x.RelatedTags, deleteTag);
                //MongoRepository.GetCollection<Article>().UpdateMany(filterForUrlSlug2, update2);

                //filter works fine






            }
            #endregion
            var filter = Builders<Tags>.Filter.Eq("_id", id);
            MongoRepository.GetCollection<Tags>().DeleteOne(filter);
        }



        /// <summary>
        /// Cleans Auth Users and Hubstats of a tag when removed. NH-128
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="tag"></param>
        public void CleanHubStatsAndUser(Brand brand, Tags tag)
        {

            //Clean User
            var listofUser = MongoRepository.AsQueryable<User>()
                .Where(x => x.favouriteTags.Any(y => y.tag.UrlSlug == tag.UrlSlug && y.brand == brand));


            foreach (var User in listofUser)
            {
                var favourtag =
                    User.favouriteTags.FirstOrDefault(y => y.tag.UrlSlug == tag.UrlSlug && y.brand == brand);

                User.favouriteTags.Remove(favourtag);

                MongoRepository.UpdateWithComment(User);

            }
            
            //Clean Hubstats
            var collection = MongoRepository.GetCollection<BsonDocument>("HubStat");
            var filter = Builders<BsonDocument>.Filter.Eq("UrlSlug", tag.UrlSlug);
            filter = filter & Builders<BsonDocument>.Filter.Eq("Brand", brand);
            var result = collection.DeleteMany(filter);


        }

        /// <summary>
        /// For every user check and clean favourite Tags NH-128
        /// </summary>
        public void CleanUser()
        {

            //Clean User
            var listofUser = MongoRepository.AsQueryable<User>();

            foreach (var User in listofUser)
            {
                bool UserChange = false;

                foreach (var favouriteTags in User.favouriteTags)
                {
                    var relatedTagExists = FindByUrlSlug(favouriteTags.tag.UrlSlug, favouriteTags.brand) != null;

                    if (relatedTagExists) continue;

                    User.favouriteTags.Remove(favouriteTags);
                    UserChange = true;
                }

                if (UserChange)
                    MongoRepository.UpdateWithComment(User);

            }
        }



        /// <summary>
        /// Cleans article removing invalid Tags and Related Tags NH-128
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="ArticleId"></param>
        public void CleanTagsRelatedTagsInArticles(Brand brand, string ArticleId)
        {

            var listofarticles = MongoRepository.AsQueryable<Data.DTOs.Article>()
                .Where(x => x.brand == brand);

            if (!string.IsNullOrEmpty(ArticleId))
                listofarticles = listofarticles.Where(x => x.Id == ArticleId);
            


            foreach (var article in listofarticles.ToList())
            {
                bool articleChange = false;

                var articleBrand = article.brand;

                foreach (var relatedTag in article.RelatedTags)
                {
                    var relatedTagExists = FindByUrlSlug(relatedTag.UrlSlug, articleBrand) != null;

                    if (relatedTagExists) continue;

                    article.RelatedTags.Remove(relatedTag);
                    articleChange = true;
                }


                foreach (var tag in article.Tags)
                {
                    var tagExists = FindByUrlSlug(tag, articleBrand) != null;
                    if (tagExists) continue;
                    article.Tags.Remove(tag);
                    articleChange = true;

                }

                if (articleChange)
                    MongoRepository.UpdateWithComment(article);
            }


        }




        /// <summary>
        /// Cleans article removing invalid Tags and Related Tags NH-128
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="ArticleId"></param>
        private void CleanTagsInArticles(Brand brand, string TagUrlSlug)
        {

            var listofarticles = MongoRepository.AsQueryable<Data.DTOs.Article>()
                .Where(x => x.brand == brand &&  (x.RelatedTags.Any(y => y.UrlSlug == TagUrlSlug) || x.Tags.Contains(TagUrlSlug)));

            



            foreach (var article in listofarticles.ToList())
                {
                bool articleChange = false;

                var articleBrand = article.brand;

                foreach (var relatedTag in article.RelatedTags)
                    {
                    var relatedTagExists = FindByUrlSlug(relatedTag.UrlSlug, articleBrand) != null;

                    if (relatedTagExists) continue;

                    article.RelatedTags.Remove(relatedTag);
                    articleChange = true;
                    }


                foreach (var tag in article.Tags)
                    {
                    var tagExists = FindByUrlSlug(tag, articleBrand) != null;
                    if (tagExists) continue;
                    article.Tags.Remove(tag);
                    articleChange = true;

                    }

                if (articleChange)
                    MongoRepository.UpdateWithComment(article);
                }


            }




    }

    public interface ITagsService
    {
        IList<Tags> All();
        IList<Tags> AllTagsThatStartWith(string text);
        IList<Tags> AllTagsThatContains(string text);
        IList<Tags> AllTagsThatContainsWithoutStartWith(string text);
        IList<Tags> Search(string searchText, bool searchName = true, int resultLimit = 10);
        IList<Tags> FindByIds(params string[] ids);
        Tags FindById(string id);
        Tags FindByUrlSlug(string UrlSlug, Brand brand);
        List<Data.Model.Tags> FindByUrlSlug(string[] urlSlugs);
        List<Data.Model.Tags> FindMenuItems(Brand brand);
        Tags FindByDisplayText(string displayText, Brand brand);
        Data.DTOs.Tags deHydrate(Data.Model.Tags viewModel);
        Data.Model.Tags hydrate(Data.DTOs.Tags tag);
        void Save(Tags tag);
        void Insert(Tags tag);
        void Delete(string id);

        /// <summary>
        /// Cleans Auth Users and Hubstats of a tag when removed. NH-128
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="tag"></param>
        void CleanHubStatsAndUser(Brand brand, Tags tag);

        /// <summary>
        /// For every user check and clean favourite Tags NH-128
        /// </summary>
        void CleanUser();

        /// <summary>
        /// Cleans article removing invalid Tags and Related Tags NH-128
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="ArticleId"></param>
        void CleanTagsRelatedTagsInArticles(Brand brand, string ArticleId);
        IList<Tags> FindByTypeAndParent(TagType tagType,params string[] ids);
        IList<Tags> FindByParentId(TagType tagtype,string id);
    }
}