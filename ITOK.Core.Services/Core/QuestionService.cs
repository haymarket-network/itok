﻿using AutoMapper;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Text.RegularExpressions;
using MongoDB.Bson;
using ITOK.Core.Data;
using ITOK.Core.Data.Model.EqualityComparers;
using ITOK.Core.Services.Core.Wrapped;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data.DTOs;
using Question = ITOK.Core.Data.Model.Question;
using ITOK.Core.Common;

namespace ITOK.Core.Services.Core
{
    public class QuestionService : BaseService, IQuestionService
    {
        private readonly IQuestionServiceWrapped _questionServiceWrapped;

        //TODO: after merge on master remove this constructor
        public QuestionService()
        {
            _mongoRepository = AppKernel.GetInstance<IMongoRepository1>();
            _questionServiceWrapped = AppKernel.GetInstance<IQuestionServiceWrapped>();
        }

        public QuestionService(IMongoRepository1 mongoRepository, IQuestionServiceWrapped questionServiceWrapped)
        {
            _mongoRepository = mongoRepository;
            _questionServiceWrapped = questionServiceWrapped;
        }
        
        public IList<Question> All()
        {
            return _questionServiceWrapped.All();
        }
          

        public IList<Question> AllQuestionsThatContain(string text)
        {
            return MongoRepository.AsQueryable<ITOK.Core.Data.DTOs.Question>().Where(x => x.QuestionText.ToLower().Contains(text.ToLower())).OrderBy(x => x.Order).ToList().Select(x => hydrate(x)).ToList();
        }


        public IList<Question> Search(string searchText, bool searchName = true, int resultLimit = 10)
        {
            if (string.IsNullOrWhiteSpace(searchText) || (!searchName)) return new List<Question>();
            var foundTags = new List<Question>();
            var searchTerms = searchText.ToLower().Split(' ');
            var sb = new StringBuilder("^");

            foreach (var term in searchTerms.Where(term => !string.IsNullOrWhiteSpace(term)))
                sb.Append(string.Format("(?=.*?({0}))", term));

            sb.Append(".*$");

            var re = new Regex(sb.ToString(), RegexOptions.IgnoreCase | RegexOptions.Multiline);
            var eventQuery = MongoRepository.AsQueryable<Question>();

            if (searchName)
            {
                var nameQuery = eventQuery
                    .Where(x =>
                        re.IsMatch(x.QuestionText));
                foundTags.AddRange(nameQuery.ToList()
                    );
            }

            foundTags = foundTags.OrderByDescending(x => x.QuestionText).ToList();

            // Remove duplicates and crop to result limit
            foundTags = foundTags
                .Distinct(new QuestionEqualityComparer())
                .Take(resultLimit)
                .ToList();

            return foundTags;
        }

        public IList<Question> FindByIds(params string[] ids)
        {
            return _questionServiceWrapped.FindByIds(ids);
        }
        public IList<Question> FindByParentId(string ids)
        {
            return _questionServiceWrapped.FindByParentId(ids);
        }
        public Question FindById(string id)
        {
            return _questionServiceWrapped.FindById(id);
        }



        public Data.DTOs.Question deHydrate(Data.Model.Question viewModel)
        {
            var question = Mapper.Map<Data.Model.Question, Data.DTOs.Question>(viewModel);
            return question;
        }

        public Data.Model.Question hydrate(Data.DTOs.Question question)
        {
            if (question == null) return null;
            // If hydration settings arent supplied then use defaults

            var viewModel = Mapper.Map<Data.DTOs.Question, Question>(question);

            //include viewcount

            return viewModel;
        }

        public void Save(Question question)
        {
            var _tag = deHydrate(question);

            var previousTag = FindById(question.Id);

       

            if (question.Id == null) MongoRepository.Insert(_tag);
            else MongoRepository.Update(_tag);
        }

        public void Insert(Question question)
        {
            var _question = deHydrate(question);
            MongoRepository.Insert(_question);
        }

        public void Delete(string id)
        {
            var deleteTag = FindById(id);

            //#region Bulk Update Tags in Articles
            //if (deleteTag != null)
            //{             

            //    //remove anywhere using the tag
            //    var filterForUrlSlug = Builders<Article>.Filter.AnyIn(x => x.Tags, new[] { deleteTag.UrlSlug });
            //    filterForUrlSlug = filterForUrlSlug & Builders<Article>.Filter.Eq("brand", deleteTag.Brand);
            //    //this bit works
            //    var updateForUrlSlugPull = Builders<Article>.Update.Pull(x => x.Tags, deleteTag.UrlSlug);
            //    MongoRepository.GetCollection<Article>().UpdateMany(filterForUrlSlug, updateForUrlSlugPull);


            //    var filter3 = Builders<Article>.Filter.ElemMatch(x => x.RelatedTags, x => x.UrlSlug == deleteTag.UrlSlug);
            //    filter3 = filter3 & Builders<Article>.Filter.Eq("brand", deleteTag.Brand);
            //    var update = Builders<Article>.Update.Pull("Article.RelatedTags", filter3);


            //    var query = MongoRepository.GetCollection<Article>().Find(filter3, new FindOptions() { });

            //    MongoRepository.GetCollection<Article>().UpdateMany(filter3, update);


         
            //}
            //#endregion
            var filter = Builders<Question>.Filter.Eq("_id", id);
            MongoRepository.GetCollection<Question>().DeleteOne(filter);
        }

    }

    public interface IQuestionService
    {
        IList<Question> All();
        IList<Question> AllQuestionsThatContain(string text);
        IList<Question> Search(string searchText, bool searchName = true, int resultLimit = 10);
        IList<Question> FindByIds(params string[] ids);
        Question FindById(string id);
        Data.DTOs.Question deHydrate(Data.Model.Question viewModel);
        Data.Model.Question hydrate(Data.DTOs.Question question);
        void Save(Question question);
        void Insert(Question question);
        void Delete(string id);
        IList<Question> FindByParentId(string ids);
    }
}