﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITOK.Core.Data.Model
{
    public class QuestionTemplate
    {
        public string Id { get; set; }
        public bool isReadOnly { get; set; }
        public List<QuestionRow> Questions { get; set; }
        public string EventId { get; set; }
        public string YearId { get; set; }
        public string AreaId { get; set; }
        public string QuestionId { get; set; }
        public QuestionTemplateType TemplateType { get; set; }

        public QuestionTemplate()
        {
            Questions = new List<QuestionRow>();
        }
    }

    public class QuestionRow
    {
        public string Question { get; set; }
        public AnswerDataType AnswerDataType { get; set; }
        public List<AnswerRow> AnswersRows { get; set; }
        public string MvcItemName { get; set; }

        public string GetMvcItemName(string propertyName)
        {
            return MvcItemName + "." + propertyName;
        }

        public QuestionRow()
        {
            AnswersRows = new List<AnswerRow>();
        }

        public virtual QuestionRow ShallowCopy()
        {
            return (QuestionRow)this.MemberwiseClone();
        }
    }

    public class AnswerRow
    {
        public string Title { get; set; }
        public AnswerDataType AnswerDataType { get; set; }
        public String AnswerString { get; set; }
        public bool AnswerBool { get; set; }
        public int? AnswerNumber { get; set; }
        public DateTime AnswerDate { get; set; }
        public Decimal AnwerMoney { get; set; }
        public List<AnswerIntList> AnswerIntList { get; set; }
        public string MvcItemName { get; set; }
        public int QuestionVerticallyIndex { get; set; }
        public int QuestionHorizontallyIndex { get; set; }

        public string GetMvcItemName(string propertyName)
        {
            return MvcItemName + "." + propertyName;
        }

    }

    public class AnswerIntList
    {
        public string Title { get; set; }
        public int Value { get; set; }
    }

    public enum AnswerDataType
    {
        FreeText = 1,
        YesNo = 2,
        Numbers = 3,
        Date = 4,
        Money = 5,
        IntArray = 6,
        CalculatedVertically = 7,
        CalculatedHorizontally = 8
    }
}
