﻿using System;
using System.Collections.Generic;
using ITOK.Core.Common.Constants;
using System.ComponentModel.DataAnnotations;

namespace ITOK.Core.Data.Model
{
    [Serializable]

    public class Article : ContentBase
    {



        public IList<Author> Authors { get; set; }
        
        //[JsonProperty(PropertyName = "Rg")]
        //public IList<MediaGallery> RelatedGalleries { get; set; }
        public string OriginalUrlSlug { get; set; }

        public IList<Article> RelatedArticles { get; set; }

        public IList<Link> Links { get; set; }

        public IList<Document> RelatedDocuments { get; set; }

        public IList<Tags> RelatedTags { get; set; }

        public IList<JobRole> RelatedJobRoles { get; set; }
        
        public bool Featured { get; set; }


        public int FeaturedOrder { get; set; }

       
     
       [Required(ErrorMessage = "An event date is required")]
        public DateTime? LiveFrom { get; set; }

        public DateTime? LiveTo { get; set; }

        public DateTime? ActionDate { get; set; }
        public DateTime? VersionDate { get; set; }

        /// <summary>
        /// TODO: This and the other LiveFrom... properties should be removed asap.
        /// </summary>
        public int LiveFromDay
        {
            get { return LiveFrom.HasValue ? LiveFrom.Value.Day : int.MinValue; }
        }

        public int LiveFromMonth
        {
            get { return LiveFrom.HasValue ? LiveFrom.Value.Month : int.MinValue; }
        }

        public int LiveFromYear
        {
            get { return LiveFrom.HasValue ? LiveFrom.Value.Year : int.MinValue; }
        }



        public User CreatedBy { get; set; }

        public User UpdatedBy { get; set; }



        public IList<String> Tags { get; set; }

        public bool IsFavoriteForCurrentUser { get; set; }

        public DateTime SeenTimeStamp { get; set; }

        public virtual string AuthorUserId { get; set; }


        public Boolean EnableGotItBlock { get; set; }

        public Boolean EnableMessageAuthorBlock { get; set; }

        public Article()
        {
            Authors = new List<Author>();
            RelatedMedia = new List<Media>();
            //RelatedGalleries = new List<MediaGallery>();
            brands = new List<Common.Constants.Brand>();
            RelatedArticles = new List<Article>();
            Links = new List<Link>();
            RelatedDocuments = new List<Document>();
            Status = PublicationStatus.UnPublished;
            Tags = new List<string>();
            RelatedGallery = new List<Media>();
            RelatedDownload = new List<Media>();
            RelatedJobRoles = new List<JobRole>();
            RelatedTags = new List<Tags>();
            EnableGotItBlock = true;
            
            EnableMessageAuthorBlock = true;
        }

        public int FavCount { get; set; }

        public int ReadingListCount { get; set; }
    }
}
