﻿using System;

namespace ITOK.Core.Data.Model
    {
    [Serializable]
    public class JobRole : Entity
        {
     
        public string ParentRoleId { get; set; }
        public string JobRoleName { get; set; }

        public DateTime CreatedOn { get; set; }

        public User CreatedBy { get; set; }

        public DateTime UpdatedOn { get; set; }

        public User UpdatedBy { get; set; }
        }
    }
