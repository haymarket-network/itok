﻿using System;

namespace ITOK.Core.Data.Model
    {
        [Serializable]
        public class SiteSettings
        {
        public string Id { get; set; }

        public string Key { get; set; }

        public string Value { get; set; }

        public object Object { get; set; }
        }
    }