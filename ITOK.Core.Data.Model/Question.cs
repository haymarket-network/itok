﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ITOK.Core.Common.Constants;

namespace ITOK.Core.Data.Model
{
    [Serializable]
    public class Question : Entity
    {
        public AnswerType AnswerType { get; set; }
        public bool IsAttachment { get; set; }
        public string Options { get; set; }
        public QuestionOptionType OptionType { get; set; }

        public int Order { get; set; }

        public string ParentTagSlug { get; set; }
        public string ParentTagId { get; set; }
        [Required]
        public QuestionTemplateType QuestionTemplates { get; set; }
        public Brand Brand { get; set; }
        public bool Show { get; set; }
        public string QuestionText { get; set; }
    }

    public enum QuestionTemplateType
    {
        [Display(Name = "Transportation Template")]
        TransportationTemplate = 1,
        [Display(Name = "Volunteers Per Area")]
        VolunteersPerArea = 2,
        [Display(Name = "Staff Hired In LOC")]
        StaffHiredInLOC = 3,
        [Display(Name = "Site Visits Summary")]
        SiteVisitsSummary = 4,
        [Display(Name = "Carpool Usage per day")]
        CarpoolUsagePerDay = 5,
        [Display(Name = "Accommodation Form Updated by End of Event")]
        AccommodationFormUpdatedbyEndofEvent = 6
    }

}


