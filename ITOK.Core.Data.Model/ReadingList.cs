﻿using System;

namespace ITOK.Core.Data.Model
    {
    [Serializable]
    public class ReadingList : Entity
        {

        
            public string Name { get; set; }

            public Boolean IsPublic { get; set; }

            public DateTime Timestamp { get ; set; }

            //THIS IS AUTHENTICATED USER
            public string UserId { get; set; }

            public User User { get; set; }

            public string JobRoleId { get; set; }

            public string RetailerId { get; set; }
        }
    }
