﻿using System;
using ITOK.Core.Common.Constants;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ITOK.Core.Data.Model
    {
    [Serializable]
    public class CuratedContent
    {
        public string Id { get; set; }
        [Required(ErrorMessage = "A specific article reference is required")]
        public string ArticleId { get; set; }
        public Article Article { get; set; }
        public string PrimaryJobRole { get; set; }

        public Tags roleTag { get; set; }
        public int Order { get; set; }
        public IList<Tags> RelatedTags { get; set; }


        public Brand Brand { get; set; }

    }
}
