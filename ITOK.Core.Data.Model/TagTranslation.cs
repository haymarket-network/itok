﻿using System;

namespace ITOK.Core.Data.Model
    {
    [Serializable]
    public class TagTranslation
        {

        public virtual string Id { get; set; }
        public string OriginalTag { get; set; }

        public string OriginalSlug { get; set; }

        public bool Ignore { get; set; }

        public string NewTagSlugs { get; set; }

        }
    }
