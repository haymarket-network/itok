﻿using System.Collections.Generic;

namespace ITOK.Core.Data.Model.EqualityComparers
    {
    public class AuthorEqualityComparer : IEqualityComparer<Author>
        {
        public bool Equals(Author x, Author y)
            {
            if (x == null || y == null) return false;
            return x.Id == y.Id;
            }

        public int GetHashCode(Author obj)
            {
            return base.GetHashCode();
            }
        }
    }
