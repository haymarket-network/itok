﻿using System.Collections.Generic;

namespace ITOK.Core.Data.Model.EqualityComparers
    {
    public class ArticleEqualityComparer : IEqualityComparer<Article>
        {
        public bool Equals(Article x, Article y)
            {
            if (x == null || y == null) return false;
            return x.Id == y.Id;
            }

        public int GetHashCode(Article obj)
            {
            return obj.GetHashCode();
            }
        }

    public class TagEqualityComparer : IEqualityComparer<Tags>
        {
        public bool Equals(Tags x, Tags y)
            {
            if (x == null || y == null) return false;
            return x.UrlSlug == y.UrlSlug;
            }

        public int GetHashCode(Tags obj)
            {
            return obj.GetHashCode();
            }
        }

    public class QuestionEqualityComparer : IEqualityComparer<Question>
    {
        public bool Equals(Question x, Question y)
        {
            if (x == null || y == null) return false;
            return x.QuestionText == y.QuestionText;
        }

        public int GetHashCode(Question obj)
        {
            return obj.GetHashCode();
        }
    }



}