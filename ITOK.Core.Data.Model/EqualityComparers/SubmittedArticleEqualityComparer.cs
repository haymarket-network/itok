﻿using System.Collections.Generic;

namespace ITOK.Core.Data.Model.EqualityComparers
    {
    public class SubmittedArticleEqualityComparer : IEqualityComparer<SubmittedArticle>
        {
        public bool Equals(SubmittedArticle x, SubmittedArticle y)
            {
            if (x == null || y == null) return false;
            return x.Id == y.Id;
            }

        public int GetHashCode(SubmittedArticle obj)
            {
            return obj.GetHashCode();
            }
        }

 

  

}