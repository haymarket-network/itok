﻿using System.Collections.Generic;

namespace ITOK.Core.Data.Model.EqualityComparers
    {
    public class MediaEqualityComparer : IEqualityComparer<Model.Media>
        {
        public bool Equals(Model.Media x, Model.Media y)
            {
            if (x == null || y == null) return false;
            return x.Id == y.Id;
            }

        public int GetHashCode(Model.Media obj)
            {
            return base.GetHashCode();
            }
        }
    }
