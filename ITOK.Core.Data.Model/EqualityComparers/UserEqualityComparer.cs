﻿using System.Collections.Generic;

namespace ITOK.Core.Data.Model.EqualityComparers
{
    public class UserEqualityComparer : IEqualityComparer<User>
    {
        public bool Equals(User x, User y)
        {
            if (x == null || y == null) return false;
            return x.Id == y.Id;
        }

        public int GetHashCode(User obj)
        {
            return base.GetHashCode();
        }
    }
}