﻿using System;

namespace ITOK.Core.Data.Model
{
    [Serializable]
    public class UserGroupMember : Entity
        {
            public string UserGroupID { get; set; }
            public string UserID { get; set; }

            public DateTime CreatedOn { get; set; }

            public User CreatedBy { get; set; }

            public DateTime UpdatedOn { get; set; }

            public User UpdatedBy { get; set; }
        }
}
