﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ITOK.Core.Data.Model
    {
    [Serializable]
    public class Author
        {
        public string Id { get; set; }

        [Required(ErrorMessage = "Forename is required")]
        public string Forename { get; set; }

        [Required(ErrorMessage = "Surname is required")]
        public string Surname { get; set; }

        [Required(ErrorMessage = "Display name is required")]
        public string DisplayName { get; set; }


        [Required(ErrorMessage = "Email is required")]
        public string Email { get; set; }

        public DateTime CreatedOn { get; set; }

        public User CreatedBy { get; set; }

        public DateTime UpdatedOn { get; set; }

        public User UpdatedBy { get; set; }
        }
    }
