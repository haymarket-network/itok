﻿using System;

namespace ITOK.Core.Data.Model
    {
    [Serializable]
    public class UserGroup : Entity
        {
     
        public string Name { get; set; }

        public DateTime CreatedOn { get; set; }

        public User CreatedBy { get; set; }

        public DateTime UpdatedOn { get; set; }

        public User UpdatedBy { get; set; }
        }
    }
