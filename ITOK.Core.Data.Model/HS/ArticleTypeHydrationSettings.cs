﻿namespace ITOK.Core.Data.Model.HS
{
    public class ArticleTypeHydrationSettings
    {

        public UserHydrationSettings CreatedBy = null;

        public UserHydrationSettings UpdatedBy = null;
    }
}