﻿namespace ITOK.Core.Data.Model.HS
{
    public class MediaHydrationSettings
    {
        public ArticleHydrationSettings RelatedArticles = null;
        public UserHydrationSettings CreatedBy = null;
        public UserHydrationSettings UpdatedBy = null;
    }
}