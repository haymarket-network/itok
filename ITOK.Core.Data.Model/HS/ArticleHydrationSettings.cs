﻿namespace ITOK.Core.Data.Model.HS
{
    public class ArticleHydrationSettings
    {
        public MediaHydrationSettings PrimaryMedia = null;
        public MediaHydrationSettings RelatedMedia = null;
        public ArticleHydrationSettings RelatedArticles = null;
        public UserHydrationSettings CreatedBy = null;
        public UserHydrationSettings UpdatedBy = null;
        public TagHydrationSettings RelatedTags = null;
        public JobRoleHydrationSettings RelatedJobs = null;
        public AuthorHydrationSettings author = null;
        public MediaHydrationSettings RelatedGallery = null;
        public MediaHydrationSettings RelatedDownload = null;
        public bool ViewStats = false;
    }
}