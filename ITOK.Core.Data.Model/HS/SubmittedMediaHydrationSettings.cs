﻿namespace ITOK.Core.Data.Model.HS
{
    public class SubmittedMediaHydrationSettings
    {
        public UserHydrationSettings CreatedBy = null;
        public UserHydrationSettings UpdatedBy = null;
    }
}