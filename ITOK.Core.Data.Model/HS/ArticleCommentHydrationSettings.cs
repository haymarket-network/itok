﻿namespace ITOK.Core.Data.Model.HS
{
    public class ArticleCommentHydrationSettings
    {

        public UserHydrationSettings CreatedBy = null;

        public UserHydrationSettings UpdatedBy = null;
    }
}