﻿namespace ITOK.Core.Data.Model.HS
{
    public class UserGroupMemberHydrationSettings
    {
        public UserHydrationSettings CreatedBy = null;
        public UserHydrationSettings UpdatedBy = null;
    }
}
