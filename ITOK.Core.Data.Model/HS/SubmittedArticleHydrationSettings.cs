﻿namespace ITOK.Core.Data.Model.HS
{
    public class SubmittedArticleHydrationSettings
    {
        public SubmittedMediaHydrationSettings PrimaryMedia = null;
        public SubmittedMediaHydrationSettings RelatedMedia = null;
        public SubmittedArticleHydrationSettings RelatedArticles = null;
        public UserHydrationSettings CreatedBy = null;
        public UserHydrationSettings UpdatedBy = null;
        public TagHydrationSettings RelatedTags = null;
        public JobRoleHydrationSettings RelatedJobs = null;
        public AuthorHydrationSettings author = null;
        public SubmittedMediaHydrationSettings RelatedGallery = null;
        public SubmittedMediaHydrationSettings RelatedDownload = null;
        public bool ViewStats = false;
    }
}