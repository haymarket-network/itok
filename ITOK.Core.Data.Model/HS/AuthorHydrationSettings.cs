﻿namespace ITOK.Core.Data.Model.HS
{
    public class AuthorHydrationSettings
    {
      
        public UserHydrationSettings CreatedBy = null;

        public UserHydrationSettings UpdatedBy = null;
    }
}