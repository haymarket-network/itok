﻿using ITOK.Core.Common.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ITOK.Core.Data.Model
{
    public abstract class ContentBase : Entity
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "A title is required")]
        public string Title { get; set; }

        public string SEOTitle { get; set; }
          
        public string UrlSlug { get; set; }
        public string MetaDescription { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Stand first is required")]
        public string StandFirst { get; set; }

        public Brand brand { get; set; }

        public List<Brand> brands { get; set; }
        public string  BrandArticleNumber { get; set; }

        public string Body { get; set; }

        public string PlainTextBody { get; set; }

        public ArticleType ArticleType { get; set; }

        public Media PrimaryMedia { get; set; }

        public ResultCard ResultCard { get; set; }

        public PublicationStatus Status { get; set; }

        public int ViewCount { get; set; }
       
       
        public IList<Media> RelatedMedia { get; set; }

        public IList<Media> RelatedGallery { get; set; } // TODO: Return read-only list

        public IList<Media> RelatedDownload { get; set; } // TODO: Return read-only list

        

        public DateTime CreatedOn { get; set; }
        //public User CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        //public User UpdatedBy { get; set; }
    }
}