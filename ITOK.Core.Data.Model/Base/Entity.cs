﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ITOK.Core.Data.Model
{ // Summary:
    //     Abstract Entity for all the BusinessEntities.
    [Serializable]
    [BsonIgnoreExtraElements(Inherited = true)]
    public abstract class Entity : IEntity<string>
    {  // Summary:
        //     Gets or sets the id for this object (the primary record for an entity).
        [BsonRepresentation(BsonType.ObjectId)]
        [BsonIgnoreIfDefault]
        public virtual string Id { get; set; }
    }

    public interface IEntity<TKey>
    {
        // Summary:
        //     Gets or sets the Id of the Entity.
        [BsonId]
        TKey Id { get; set; }
    }
}
