﻿using System;
using ITOK.Core.Common.Constants;

namespace ITOK.Core.Data.Model
    {
    [Serializable]
    public class Retailer
        {
        public string Id { get; set; }

        public string RetailerId { get; set; }
        public string RetailerName { get; set; }
        public Brand Brand { get; set; }
        public DateTime CreatedOn { get; set; }

        public User CreatedBy { get; set; }

        public DateTime UpdatedOn { get; set; }

        public User UpdatedBy { get; set; }
        }
    }
