﻿using System;
using System.Collections.Generic;

namespace ITOK.Core.Data.Model
{
    public class QuestionComment : Entity
    {
        public string AnswerId { get; set; }
        public string QuestionText { get; set; }
        public string CommentText { get; set; }
        public string UpdateCommentDate { get; set; }
        public string AreaId { get; set; }
        public string EventId { get; set; }
        public string YearId { get; set; }

        public IList<UsersComment> UsersComment;

        public QuestionComment()
        {
            UsersComment = new List<UsersComment>();
        }
    }

    public class UsersComment
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Comment { get; set; }
        public DateTime CommentDate { get; set; }
    }
}
