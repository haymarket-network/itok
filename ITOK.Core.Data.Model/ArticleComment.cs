﻿using System;

namespace ITOK.Core.Data.Model
    {
    [Serializable]
    public class ArticleComment : Entity
        {

        public string ArticleId { get; set; }
        public string UserIdentification { get; set; }
        public string Comment { get; set; }

        public DateTime CreatedOn { get; set; }

        public User CreatedBy { get; set; }

        public DateTime UpdatedOn { get; set; }

        public User UpdatedBy { get; set; }
        }
    }
