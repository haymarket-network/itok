﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using ITOK.Core.Common.Constants;


namespace ITOK.Core.Data.Model
    {
    [Serializable]
    public class Tags : Entity
        {
        public string DisplayText { get; set; }

        public Brand Brand { get; set; }

        public string UrlSlug { get; set; }

        public string ParentSlug { get; set; }

        public string ParentId { get; set; }

        public Dictionary<string, string> TopicHeaders { get; set; }
        public List<string> ArticleUrls { get; set; }
        [AllowHtml]
        public string Description { get; set; }

        public TagType TagType { get; set; }

        public bool Show { get; set; }

        public int Order { get; set; }

        public string ResponsibleName { get; set; }
        public string ResponsibleTitle { get; set; }
        public string ResponsiblePhone { get; set; }
        public string ResponsibleEmail { get; set; }
        public string ContactEmail { get; set; }



    }
}
