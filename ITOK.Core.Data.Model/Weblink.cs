﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using ITOK.Core.Common.Constants;

namespace ITOK.Core.Data.Model
    {
    [Serializable]
    public class Weblink : Entity
        {
        public string DisplayText { get; set; }

        public string Url { get; set; }
        public IList<Tags> RoleTags { get; set; }
        public Brand Brand { get; set; }
      

    }
    }
