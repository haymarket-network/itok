﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ITOK.Core.Common.Constants;

namespace ITOK.Core.Data.Model
{
    public class Answer 
    {
        public string Id { get; set; }
        public string QuestionId { get; set; }
        public string QuestionText { get; set; }
        public List<string> AnswerValues { get; set; }
        public string AnswerValue { get; set; } 
        public AnswerType AnswerType { get; set; }
        public QuestionOptionType OptionType { get; set; }
        public QuestionTemplateType TemplateType { get; set; }
        public QuestionTemplate QuestionTemplate { get; set; }
        public string UserId { get; set; }
        public List<AnswerTag> AnswerTags { get; set; }
        public List<string> OptionList { get; set; }
        public string EventId { get; set; }
        public string YearId { get; set; }
        public string AreaId { get; set; }
        public Media AttachmentMedia { get; set; }
        public string AttachmentId { get; set; }
        public IList<Media> Attachments { get; set; }
        public string AnsweredBy { get; set; }
        public DateTime AnsweredDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }

        public Answer()
        {
            AnswerTags = new List<AnswerTag>();
            OptionList = new List<string>();
            AnswerValues = new List<string>();
            AnswerValue = string.Empty;
        }
    }

    public class AnswerTag
    {
        public string Text { get; set; }
        public string UrlSlug { get; set; }
        public TagType TagType { get; set; }
    }
}
