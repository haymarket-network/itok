﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data.Model.EqualityComparers;

namespace ITOK.Core.Data.Model
    {
    [Serializable]
    public class Alert : Entity
        {
        [Required(ErrorMessage = "Headline is required")]
        public string Headline { get; set; }

        public string AlertText { get; set; }

        public IList<Tags> RoleTags { get; set; }
        public Brand Brand { get; set; }
        public DateTime ExpiryDate { get; set; }
        [Required(ErrorMessage = "Status is required")]
        public AlertStatus status { get; set; }

        public string BrandArticleNumber { get; set; }
        public DateTime CreatedOn { get; set; }
        //public User CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        //public User UpdatedBy { get; set; }
    }
    }
