﻿using System;

namespace ITOK.Core.Data.Model
    {
    [Serializable]
    public class Link
        {
        public string Id { get; set; }

        public string DisplayText { get; set; }

        public string Url { get; set; }
        
        public string ImgUrl { get; set; }

        public bool NewWindow { get; set; }
        
        }
    }
