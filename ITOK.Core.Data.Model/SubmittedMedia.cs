﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using ITOK.Core.Common.Constants;

namespace ITOK.Core.Data.Model
    {
    [Serializable]
    public class SubmittedMedia
        {
        public string Id { get; set; }

        public string ImportId { get; set; }

        [Required(ErrorMessage = "A title is required")]
        [JsonProperty(PropertyName = "T")]
        public string Title { get; set; }
        public Brand Brand { get; set; }
        public string SEOTitle { get; set; }

        public string UrlSlug { get; set; }

        public string MetaDescription { get; set; }


        [JsonProperty(PropertyName = "C")]
        public string Credit { get; set; }

        public bool ShowInMedia { get; set; }

        public bool Complete { get; set; }

        [JsonProperty(PropertyName = "Fn")]
        public string FileName { get; set; }
        public string OriginalFileName { get; set; }


       
        public FileFormat Format { get; set; }

        public MediaHosting Hosting { get; set; }       


        public string RemoteItemCode { get; set; }

        public string PlayerId { get; set; }

        
        public int SourceWidth { get; set; }

        public int SourceHeight { get; set; }

        public IList<MediaRatio> AvailableRatios { get; set; }

        public SubmissionStatus Status { get; set; }

        public DateTime LiveFrom { get; set; }

        public DateTime? LiveTo { get; set; }

        // IAuditable
        public DateTime CreatedOn { get; set; }

        public User CreatedBy { get; set; }

        public DateTime UpdatedOn { get; set; }

        public User UpdatedBy { get; set; }

        public SubmittedMedia()
            {
            ShowInMedia = false;
            AvailableRatios = new List<MediaRatio>();
           
            Status = SubmissionStatus.InProgress;
            LiveFrom = DateTime.Today;

           
            }
        }
    }
