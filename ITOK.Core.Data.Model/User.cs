﻿using ITOK.Core.Common.Constants;
using ITOK.Core.Common.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ITOK.Core.Data.Model
{
    [Serializable]
    public class User
    {
        public string Id { get; set; }

        [Required(ErrorMessage = "Forename is required")]
        public virtual string Forename { get; set; }

        [Required(ErrorMessage = "Surname is required")]
        public virtual string Surname { get; set; }

        [Required(ErrorMessage = "An email is required")]
        public virtual string Email { get; set; }

        public virtual bool AcceptedRegulations { get; set; }

        /// <summary>
        /// Salt and hash!
        /// </summary>
        public virtual string Password { get; set; }

        public virtual int LoginAttempts { get; set; }

        public string Organisation { get; set; }
        public virtual IList<Permission> Permissions { get; set; }
        public virtual string PermissionsStr { get; set; }
        public List<FavouriteTag> favouriteTags { get; set; }
        public DateTime LastLoggedOn { get; set; }
        public DateTime LastLoginDate { get; set; }
        // IAuditable
        public string CreatedById { get; set; }

        private DateTime _createdOn;

        public DateTime CreatedOn { get { return _createdOn; } set { _createdOn = DateTimeUtilities.ToUtcPreserved(value); } }

        public string UpdatedById { get; set; }

        private DateTime _updatedOn;

        public DateTime UpdatedOn { get { return _updatedOn; } set { _updatedOn = DateTimeUtilities.ToUtcPreserved(value); } }
        
        public List<Tag> Questions { get; set; }

        public IList<Tag> tags { get; set; }
        public List<Tag> Eventtags { get; set; }
        public List<Tag> Yeartags { get; set; }
        public IList<Tag> Areatags { get; set; }
        public string AreaTagId { get; set; }
        public bool IsItokLeader { get; set; }

        public User()
        {
            Yeartags = new List<Tag>();
            Questions = new List<Tag>();
        }
        public List<UserEvent> UserEvents { get; set; }
    }

    public class FavouriteTag
    {
        public Brand brand;
        public Tags tag;
    }
    public class Tag
    {
        public string Id { get; set; }
        public string DisplayText { get; set; }
        public TagType TagType { get; set; }
        public bool IsChecked { get; set; }
        public string ParentId { get; set; }
    }
    public class EventTag
    {
        public string Id { get; set; }
        public string DisplayText { get; set; }
        public bool IsChecked { get; set; }
    }
    public class YearTag
    {
        public string Id { get; set; }
        public string DisplayText { get; set; }
        public string ParentId { get; set; }
    }
    public class AreaTag
    {
        public string Id { get; set; }
        public string DisplayText { get; set; }
    }
}