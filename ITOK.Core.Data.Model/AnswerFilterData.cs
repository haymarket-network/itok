﻿using ITOK.Core.Common.Constants;
using System;
using System.Collections.Generic;

namespace ITOK.Core.Data.Model
{
    public class AnswerFilterData
    {
        public string Id { get; set; }
        public string AnswerValue { get; set; }
        public List<string> AnswerValues { get; set; }
        public DateTime AnsweredDate { get; set; }
        public IList<AnswerTag> AnswerTags { get; set; }
        public AnswerType AnswerType { get; set; }
        public string AttachmentId { get; set; }
        public QuestionTemplate QuestionTemplate { get; set; }
        public QuestionTemplateType TemplateType { get; set; }

        public int FavCount { get; set; }
        public string Headline { get; set; }
        public bool IsFavoriteForCurrentUser { get; set; }
        public string QuestionId { get; set; }
        public List<string> OptionList { get; set; }
        public QuestionOptionType OptionType { get; set; }
        public string QuestionText { get; set; }
        public int ReadingListCount { get; set; }
        public bool IsReadingListForCurrentUser { get; set; }
        //public IList<Tags> RelatedTags { get; set; }
        public string TagTypeText { get; set; }
        public string UserId { get; set; }
        public int ViewCount { get; set; }

        public bool isCommented { get; set; }
        public bool isFlagSet { get; set; }
        public Common.Constants.FlagColor FlagColor { get; set; }

        public List<AttachmentMedia> AttachmentMedias { get; set; }
        public AnswerFilterData()
        {
            AnswerValues = new List<string>();
            AnswerTags = new List<AnswerTag>();
            OptionList = new List<string>();
            AttachmentMedias = new List<AttachmentMedia>();
            //RelatedTags = new List<Tags>();
        }
    }

    public class AttachmentMedia
    {
        public string Title { get; set; }
        public string Id { get; set; }
    }
}
