﻿using System;
using ITOK.Core.Common.Constants;
using ITOK.Core.Common.Utils;

namespace ITOK.Core.Data.Model
{
    public class Search
    {
    public string Id { get; set; }

    public string tags { get; set; }
    public Brand brand { get; set; }

    public string searchText { get; set; }
    public SearchFilter searchFilter { get; set; }
    public SortOrder searchOrder { get; set; }

    public DateTime start { get; set; }
    public DateTime? end { get; set; }

    private DateTime _timestamp;

    public DateTime Timestamp { get { return _timestamp; } set { _timestamp = DateTimeUtilities.ToUtcPreserved(value); } }
    public string UserId { get; set; }
    public string JobRoleId { get; set; }
    }



     
}
