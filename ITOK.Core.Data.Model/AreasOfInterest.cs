﻿using System;
using ITOK.Core.Common.Constants;

namespace ITOK.Core.Data.Model
    {
    [Serializable]
    public class AreasOfInterest
    {
        public string Id { get; set; }

        public string ParentSlug { get; set; }

        public string ParentId { get; set; }

        public string InterestTag { get; set; }
        public string InterestTagName { get; set; }
        public string InterestTagId { get; set; }

        public Brand brand { get; set; }

    }
}
