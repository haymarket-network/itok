﻿using System;
using System.Collections.Generic;
using ITOK.Core.Common.Constants;
using System.ComponentModel.DataAnnotations;

namespace ITOK.Core.Data.Model
{
    [Serializable]

    public class SubmittedArticle : Entity
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "A title is required")]
        public string Title { get; set; }

        public string SEOTitle { get; set; }

        public string UrlSlug { get; set; }
        public string MetaDescription { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Stand first is required")]
        public string StandFirst { get; set; }

        public Brand brand { get; set; }

        public List<Brand> brands { get; set; }
        public string BrandArticleNumber { get; set; }

        public string Body { get; set; }

        public string PlainTextBody { get; set; }

        public ArticleType ArticleType { get; set; }

        public SubmittedMedia PrimaryMedia { get; set; }

        public ResultCard ResultCard { get; set; }

        public SubmissionStatus Status { get; set; }

        public int ViewCount { get; set; }


        public IList<SubmittedMedia> RelatedMedia { get; set; }

        public IList<SubmittedMedia> RelatedGallery { get; set; } // TODO: Return read-only list

        public IList<SubmittedMedia> RelatedDownload { get; set; } // TODO: Return read-only list



        public DateTime CreatedOn { get; set; }
        //public User CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        //public User UpdatedBy { get; set; }


        public IList<Author> Authors { get; set; }
        
        //[JsonProperty(PropertyName = "Rg")]
        //public IList<MediaGallery> RelatedGalleries { get; set; }
        public string OriginalUrlSlug { get; set; }

        public IList<SubmittedArticle> RelatedArticles { get; set; }

        public IList<Link> Links { get; set; }

        public IList<Document> RelatedDocuments { get; set; }

        public IList<Tags> RelatedTags { get; set; }
                
        public bool Featured { get; set; }


        public int FeaturedOrder { get; set; }

       
     
       [Required(ErrorMessage = "A live from date is required")]
        public DateTime? LiveFrom { get; set; }

        public DateTime? LiveTo { get; set; }

        public DateTime? ActionDate { get; set; }
        public DateTime? VersionDate { get; set; }

        /// <summary>
        /// TODO: This and the other LiveFrom... properties should be removed asap.
        /// </summary>
        public int LiveFromDay
        {
            get { return LiveFrom.HasValue ? LiveFrom.Value.Day : int.MinValue; }
        }

        public int LiveFromMonth
        {
            get { return LiveFrom.HasValue ? LiveFrom.Value.Month : int.MinValue; }
        }

        public int LiveFromYear
        {
            get { return LiveFrom.HasValue ? LiveFrom.Value.Year : int.MinValue; }
        }



        public User CreatedBy { get; set; }

        public User UpdatedBy { get; set; }



        public IList<String> Tags { get; set; }

        public bool IsFavoriteForCurrentUser { get; set; }

        public DateTime SeenTimeStamp { get; set; }

        public virtual string AuthorUserId { get; set; }


        public Boolean EnableGotItBlock { get; set; }

        public Boolean EnableMessageAuthorBlock { get; set; }

        public SubmittedArticle()
        {
            Authors = new List<Author>();
            RelatedMedia = new List<SubmittedMedia>();
            //RelatedGalleries = new List<MediaGallery>();
            brands = new List<Common.Constants.Brand>();
            RelatedArticles = new List<SubmittedArticle>();
            Links = new List<Link>();
            RelatedDocuments = new List<Document>();
            Status = SubmissionStatus.InProgress;
            LiveFrom = DateTime.UtcNow;
            Tags = new List<string>();
            RelatedGallery = new List<SubmittedMedia>();
            RelatedDownload = new List<SubmittedMedia>();
            RelatedTags = new List<Tags>();
            EnableGotItBlock = true;
            
            EnableMessageAuthorBlock = true;
        }

        public int FavCount { get; set; }

        public int ReadingListCount { get; set; }
    }
}
