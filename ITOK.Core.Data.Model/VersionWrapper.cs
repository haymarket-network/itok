﻿using ITOK.Core.Common.Constants;
using System;

namespace ITOK.Core.Data.Model
{
    public class VersionWrapper
    {
        public string Id { get; set; }

        public VersionRecordType RecordType { get; set; }

        public string Comment { get; set; }

        public User ModifiedBy { get; set; }

        public DateTime ModifiedOn { get; set; }
    }
}
