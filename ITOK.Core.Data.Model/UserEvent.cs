﻿using System;
using System.Collections.Generic;

namespace ITOK.Core.Data.Model
{
    public class UserEvent
    {
        public string EventTitle { get; set; }
        public string EventId { get; set; }
        public string YearTitle { get; set; }
        public string YearId { get; set; }
        public string AreaTitle { get; set; }
        public string AreaId { get; set; }
        public EventAnswerStatus Status { get; set; }
        public List<UserQuestion> QuestionList { get; set; }
        public IList<Tags> Events { get; set; }
        public IList<Tags> Years { get; set; }
        public IList<Tags> Categories { get; set; }
        public IList<Tags> Areas { get; set; }
        public string QuestionIds { get; set; }
        public string QuestionTexts { get; set; }
    }

    public class UserQuestion
    {
        public string QuestionId { get; set; }
        public string QuestionText { get; set; }
        public Question Question { get; set; }
        public bool isAnswered { get; set; }
        public bool isCommented { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }

        //below properties are use in set flag color
        public bool isFlagSet { get; set; }
        public Common.Constants.FlagColor FlagColor { get; set; }
        public string AnswerId { get; set; }
        public string AnswerFlagId { get; set; }

        [MongoDB.Bson.Serialization.Attributes.BsonIgnore]
        public int AttachmentCount { get; set; }

        // below 3 properties are just to reuse normal answer functionality to itok leader index page
        public string EventId { get; set; }
        public string YearId { get; set; }
        public string AreaId { get; set; }


        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            if (!(obj is UserQuestion))
                return false;

            var t = obj as UserQuestion;
            return t.QuestionId == this.QuestionId
                && t.QuestionText == this.QuestionText;
        }
        public override int GetHashCode()
        {
            return QuestionId.GetHashCode();
        }
        public bool Equals(UserQuestion questiontoCompareTo)
        {
            // Check if person is being compared to a non person. In that case always return false.
            if (questiontoCompareTo == null) return false;

            // If the person to compare to does not have a Name assigned yet, we can't define if it's the same. Return false.
            if (string.IsNullOrEmpty(questiontoCompareTo.QuestionId)) return false;

            // Check if both person objects contain the same Name. In that case they're assumed equal.
            return QuestionId.Equals(questiontoCompareTo.QuestionId);
        }
    }

    public enum EventAnswerStatus
    {
        Unstarted = 0,
        InProgress = 1,
        Complete = 2
    }
}