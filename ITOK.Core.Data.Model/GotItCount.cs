﻿using System;

namespace ITOK.Core.Data.Model
    {
    [Serializable]
    public class GotItCount : Entity
        {
        
        public Article Article { get; set; }

        public int YesCount { get; set; }

        public int NoCount { get; set; }
        }
    }
