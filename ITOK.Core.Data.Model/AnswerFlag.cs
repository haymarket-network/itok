﻿using ITOK.Core.Common.Constants;
using System;

namespace ITOK.Core.Data.Model
{
    public class AnswerFlag
    {
        public string Id { get; set; }
        public string AnswerId { get; set; }
        public FlagColor FlagColor { get; set; }
        public string SetColorBy { get; set; }
        public DateTime SetColorDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
