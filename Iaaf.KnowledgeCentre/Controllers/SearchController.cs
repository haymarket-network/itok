﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ITOKFrontend.Helpers.Authorise;
using ITOKFrontend.Models;
using ITOK.Core.Common;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data;
using ITOK.Core.Data.DTOs;
using ITOK.Core.Data.Model.HS;
using ITOK.Core.Services.Core;
using ITOK.Core.Services.Security;
using Article = ITOK.Core.Data.Model.Article;
using ArticleService = ITOK.Core.Services.CMS.ArticleService;
using Tags = ITOK.Core.Data.Model.Tags;
using ITOK.Core.Services.Core;

namespace ITOKFrontend.Controllers
{
    public class SearchController : BaseController
    {
        private IMongoRepository1 mongoRepository;
        public readonly IAnswerFilterService _answerFilterService;

        public SearchController()
        {
            mongoRepository = AppKernel.GetInstance<IMongoRepository1>();
            _answerFilterService = AppKernel.GetInstance<IAnswerFilterService>();
        }

        //public SearchController(IMongoRepository1 _mongoRepository)
        //{
        //    this.mongoRepository = _mongoRepository;
        //    _articleService = new ArticleService(mongoRepository);
        //    _tagsService = AppKernel.GetInstance<ITagsService>();
        //    _searchService = new SearchService(mongoRepository);
        //}

        // GET: Hub
        //public readonly ArticleService _articleService;

        //public readonly ITagsService _tagsService;

        //public readonly SearchService _searchService;
       
        public int DefaultPageSize = 20;

        //[CustomAuthorise]
        //public ActionResult Index()
        //{
        //    if (!LiveUser.Get().AcceptedRegulations)
        //        return RedirectToAction("AcceptRegulations", "Home");
        //    // var brand = (Brand)Enum.Parse(typeof(Brand), RouteData.Values["brand"].ToString(), true);
        //    var brand = Brand.itok;

        //    if (Request.QueryString["q"] == null && Request.QueryString["t"] == null)
        //        return View(new SearchModels.SearchFilterModel
        //        {
        //            Articles = new List<Article>(),
        //            TotalItemCount = 0,
        //            SearchFilter = SearchFilter.Or,
        //            FilterTags = new List<Tag>(),
        //            SelectedTags = new List<Tags>(),
        //            SearchText = "",
        //            Tags = "",
        //            SortOrder = SortOrder.Relevance,
        //            Start = null,
        //            End = null,
        //            BaseDataUrl = "",
        //            BaseUrl = string.Format("/{0}/search", brand)
        //        });

        //    var tags = Request.QueryString["t"];
        //    //ar brand = GlobalVariables.CurrentBrand;
        //    var searchText = Request.QueryString["q"];
        //    var searchFilter = SearchFilter.Or;

        //    if (Request.QueryString["sf"] != null)
        //        switch (Request.QueryString["sf"])
        //        {
        //            case "phrase":

        //                searchFilter = SearchFilter.Phrase;
        //                break;

        //            case "all":
        //            case "and":

        //                searchFilter = SearchFilter.And;
        //                break;

        //            case "any":
        //            default:
        //                searchFilter = SearchFilter.Or;
        //                break;
        //        }

        //    var start = Request.QueryString["s"] != null ? DateTime.Parse(Request.QueryString["s"].ToString()) : (DateTime?)null;
        //    var end = Request.QueryString["e"] != null ? DateTime.Parse(Request.QueryString["e"].ToString()) : (DateTime?)null;
        //    var searchtags = string.IsNullOrEmpty(tags) ? new string[] { } : tags.Split(' ').Where(x => !string.IsNullOrEmpty(x)).ToArray();
        //    var sortOrder = Request.QueryString["so"] != null ? (SortOrder)Enum.Parse(typeof(SortOrder), Request.QueryString["so"]) : SortOrder.LiveFrom;
        //    var take = 20;
        //    int? skip = null;

        //    var results = _articleService.SearchArticles(brand, searchText, searchFilter, start, end, searchtags, sortOrder,
        //        take, skip, _hydrationSettings);

        //    var count = _articleService.SearchArticlesCount(brand, searchText, searchFilter, start, end, searchtags);

        //    var relatedTags = _articleService.FindRelatedTagsForSearch(brand, searchText, searchFilter, start, end, searchtags);

        //    var commonTags = (from tag in relatedTags.ToList()
        //                      let details = tag["_id"].AsBsonDocument
        //                      select new Tag
        //                      {
        //                          tagCount = tag.GetElement("count").Value.ToString(),
        //                          tagName = details.GetElement("DisplayText").Value.ToString(),
        //                          tagSlug = details.GetElement("UrlSlug").Value.ToString()
        //                      }).ToList().Take(25);

        //    var selectedTags = new List<ITOK.Core.Data.Model.Tags>();

        //    if (searchtags.Any())
        //    {
        //        selectedTags = _tagsService.FindByUrlSlug(searchtags);
        //    }
        //    var search = new Search()
        //    {
        //        brand = brand,
        //        EndFilter = end,
        //        StartFilter = start,
        //        JobRoleId = "",
        //        searchFilter = searchFilter,
        //        searchText = searchText,
        //        searchOrder = sortOrder,
        //        tags = tags,
        //        UserId = LoginManager.CurrentUser.Id,
        //        TotalItemCount = count,
        //        Timestamp = DateTime.Now
        //    };
        //    _searchService.Save(search);

        //    var BaseDataUrl = string.Format("/searchdata/{1}",
        //        brand, search.Id);

        //    var model = new SearchModels.SearchFilterModel
        //    {
        //        Articles = results,
        //        TotalItemCount = count,
        //        SearchFilter = searchFilter,
        //        FilterTags = commonTags,
        //        SelectedTags = selectedTags,
        //        SearchText = searchText,
        //        Tags = tags,
        //        SortOrder = sortOrder,
        //        Start = start,
        //        End = end,
        //        BaseDataUrl = BaseDataUrl,
        //        BaseUrl = string.Format("/search", brand)
        //    };

        //    return View(model);
        //}
    }
}