﻿using System.Web.Mvc;
using ITOK.Core.Common;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data;
using ITOK.Core.Services.Core;

namespace ITOKFrontend.Controllers
    {
    public class StatsController : BaseController
        {
        private readonly StatsService _statService = new StatsService(AppKernel.GetInstance<IMongoRepository1>());

        [Route("article-viewed/{userId}/{urlSlug}")]
        [HttpGet]
        public void ArticleViewed(string userId, string urlSlug)
            {
            if (!Request.Browser.Crawler)
                _statService.RegisterArticleViewed(userId, "", urlSlug, Brand.itok);
            }

        [Route("hub-viewed/{userId}/{urlSlug}")]
        [HttpGet]
        public void HubViewed(string userId, string urlSlug)
            {
            if (!Request.Browser.Crawler)
                _statService.RegisterHubViewed(Brand.itok, userId, "", urlSlug);
            }

       
        }
    }