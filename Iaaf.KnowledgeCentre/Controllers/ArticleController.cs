﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using ITOKFrontend.Helpers;
using ITOKFrontend.Helpers.Authorise;
using ITOKFrontend.Models;
using ITOKFrontend.Models.Constants;
using HtmlAgilityPack;
using ITOK.Core.Common;
using ITOK.Core.Common.Constants;
using ITOK.Core.Common.Utilities;
using ITOK.Core.Common.Utils;
using ITOK.Core.Data;
using ITOK.Core.Data.Model.HS;
using ITOK.Core.Services.Core;
using ITOK.Core.Services.Security;
using ITOK.Core.Data.Model;

namespace ITOKFrontend.Controllers
{
    public class ArticleController : BaseController
    {
        private IMongoRepository1 mongoRepository;

        public int DefaultPageSize = 20;

        public ArticleController()
        {
            mongoRepository = AppKernel.GetInstance<IMongoRepository1>();
            Init();
        }

        public ArticleController(IMongoRepository1 _mongoRepository)
        {
            this.mongoRepository = _mongoRepository;

            Init();
        }

        public void Init()
        {
            _articleService = new ArticleService(mongoRepository);
            _tagsService = AppKernel.GetInstance<ITagsService>();
            _MediaService = new MediaService(mongoRepository);
            _gotitService = new GotItCountService(mongoRepository);
            _statService = new StatsService(mongoRepository);
            _readingListService = new ReadingListService(mongoRepository);
            _articleCommentService = new ArticleCommentService(mongoRepository);
            _UserService = new UserService(mongoRepository);
            _curatedContentService = new CuratedContentService(mongoRepository);
        }

        public ArticleService _articleService;
        public ITagsService _tagsService;
        public StatsService _statService;
        public MediaService _MediaService;
        private CuratedContentService _curatedContentService;

        private readonly ArticleHydrationSettings _hydrationSettings = new ArticleHydrationSettings
        {
            PrimaryMedia = new MediaHydrationSettings()
        };

        private ArticleFilterModel _model;

        [CustomAuthorise]
        public ActionResult TestVideo()
        {
            return View();
        }

        [CMSAuthorize("UseFrontEnd")]
        [CustomAuthorise]
        public ActionResult Detail(string urlSlug)
        {

            if (!LiveUser.Get().AcceptedRegulations)
                return RedirectToAction("AcceptRegulations", "Home");

            var hub = Server.UrlEncode(Request.QueryString["hub"]);
            var brand = Brand.itok;
            ViewBag.hub = hub;
            if (string.IsNullOrWhiteSpace(urlSlug)) throw new HttpException(404, "");
            var article = _articleService.FindBySlug(brand, urlSlug, new ArticleHydrationSettings
            {
                PrimaryMedia = new MediaHydrationSettings(),
                RelatedMedia = new MediaHydrationSettings(),
                RelatedArticles = new ArticleHydrationSettings { PrimaryMedia = new MediaHydrationSettings() },
                RelatedTags = new TagHydrationSettings()
            }, new[] { PublicationStatus.Published, });
            if (article == null)
                throw new HttpException(404, "");

            if (article.Status != PublicationStatus.Published)
                throw new HttpException(404, "");

            if (GlobalVariables.CurrentBrand != brand)
            {
                return Redirect(string.Format("/home/changebrand/{0}?redirect={1}", brand, Request.Url));
            }


            if (article.LiveTo.HasValue && article.LiveTo < DateTime.Now)
            {
                throw new HttpException(404, "");
            }

            //var articleBrands = article.brands.Select(s => s.ToString());

            //bool hasMatch = LoginManager.CurrentUser.brands.Intersect(articleBrands).Any();

            //if (!hasMatch) //if user is not permitted to see this brand
            //{
            //    throw new HttpException(404, "");
            //}
            //switch global brand if necessary
            //if (!articleBrands.Contains(RouteData.Values["brand"].ToString()))
            //    Session["ChangeBrand"] = articleBrands.ToList()[0];

            var artTime = article.VersionDate;
            //if (!this.IsModified(artTime))
            //    return this.NotModified();
            if (artTime != null)
                Response.AddHeader("Last-Modified", artTime.Value.ToUniversalTime().ToString("R"));

            //var nextArticleUrl = _articleService.FindNext((DateTime)article.LiveFrom, NextPrevious.Next);
            //var previousArticleUrl = _articleService.FindNext((DateTime)article.LiveFrom, NextPrevious.Previous);

            article.Body = FindEndededImages(article.Body, brand);

            //if (article.RelatedArticles.Count < 3)
            //    article.RelatedArticles = _articleService.GetPagedOrderedBy(brand, SortOrder.VersionDate, 0, 6,
            //            article.Tags.ToArray(),
            //            new[] { PublicationStatus.Published }, true, false, null, new ArticleHydrationSettings
            //            {
            //                PrimaryMedia = new MediaHydrationSettings(),
            //            }, true);

            var model = new ArticleDetailViewModel
            {
                Article = article,
                //RelatedArticles = article.RelatedArticles,
                ArticleUrl = ArticleUtilities.GetArticleUri(article),
            };


            if (model.Article.RelatedTags.Any(i => i.TagType == TagType.Area))
            {
                model.AreaTag = _tagsService.FindById(model.Article.RelatedTags.Where(i => i.TagType == TagType.Area).First().Id);
            }
            if (string.IsNullOrEmpty(article.AuthorUserId)) return View("Article", model);
            var author = _UserService.FindById(article.AuthorUserId);
            if (author != null)
                model.AuthorName = author.Forename + " " + author.Surname;

            return View("Article", model);
        }

        [CustomAuthorise]
        public ActionResult preview(string id)
        {

            if (string.IsNullOrWhiteSpace(id)) throw new HttpException(404, "");
            var article = _articleService.FindById(Brand.itok, id, new ArticleHydrationSettings
            {
                PrimaryMedia = new MediaHydrationSettings(),
                RelatedMedia = new MediaHydrationSettings(),
                RelatedArticles = new ArticleHydrationSettings { PrimaryMedia = new MediaHydrationSettings() }
            });
            if (article == null)
                throw new HttpException(404, "");

            var articleBrands = article.brands.Select(s => s.ToString());

            var artTime = article.VersionDate;

            if (artTime != null)
                Response.AddHeader("Last-Modified", artTime.Value.ToUniversalTime().ToString("R"));

            //var nextArticleUrl = _articleService.FindNext((DateTime)article.LiveFrom, NextPrevious.Next);
            //var previousArticleUrl = _articleService.FindNext((DateTime)article.LiveFrom, NextPrevious.Previous);

            article.Body = FindEndededImages(article.Body, Brand.itok);

            var model = new ArticleDetailViewModel
            {
                Article = article,
                RelatedArticles = article.RelatedArticles,
                ArticleUrl = ArticleUtilities.GetArticleUri(article),
            };

            if (string.IsNullOrEmpty(article.AuthorUserId)) return View("Article", model);
            var author = _UserService.FindById(article.AuthorUserId);
            if (author != null)
                model.AuthorName = author.Forename + " " + author.Surname;

            return View("Article", model);
        }

        private string FindEndededImages(string rawHTML, Brand brand)
        {
            try
            {
                var doc = new HtmlDocument();
                doc.Load(new StringReader(rawHTML));
                //rawHTML will contain the above HTML.
                //var SpanNodes = doc.DocumentNode.SelectNodes("//div[@class = \"media\"]");
                //for details of XPath syntax see the above table, // operater returns
                //all nodes matching the name after //. In above example it will return all
                //span nodes.

                var query = doc.DocumentNode.SelectNodes("//div[@class = \"dropMedia\"]");
                foreach (var htmlNode in query.ToList())
                {
                    //var imgId = item.Attributes.Hashitems[1].Value.Value;

                    var img = new MvcHtmlString(MediaUtilities.GetMediaMarkup(
                        MediaProfile.size_600x300, _MediaService.FindByIds(null, htmlNode.Id).FirstOrDefault(), brand, null, null, false, false, false)).ToHtmlString();

                    htmlNode.InnerHtml = img;
                }

                string result = null;
                using (var writer = new StringWriter())
                {
                    doc.Save(writer);
                    result = writer.ToString();
                }
                return result;
            }
            catch (Exception e)
            {
                //Write your exception handling code here
            }



            return rawHTML;
        }

        //  [DonutOutputCache(Location = OutputCacheLocation.Any, Duration = 60, VaryByParam = "type;filter;cols;pagesize")]
        /// <summary>
        /// Returns the list of articles used on homepage and hub/hashtag pages
        /// </summary>
        /// <param name="type"></param>
        /// <param name="filter"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        public ActionResult ListContainer(ArticleFilterType type, string filter = "", int? pagesize = null, bool ShowTags = true)
        {
            var pageSize = pagesize ?? DefaultPageSize;

            string[] tags = { };

            if (!string.IsNullOrEmpty(filter))
            {
                tags = filter.Split('+').ToArray().OrderBy(x => x).ToArray();
            }

            _model = new ArticleFilterModel { Filter = filter, FilterType = type, ShowTags = ShowTags, };

            var brand = GlobalVariables.CurrentBrand;
            switch (type)
            {
                case ArticleFilterType.None:

                    _model.Articles = _articleService.GetPagedOrderedBy(brand, SortOrder.LiveFrom, 0, pageSize,
                        tags,
                        new[] { PublicationStatus.Published }, true, false, null, new ArticleHydrationSettings
                        {
                            PrimaryMedia = new MediaHydrationSettings(),
                            RelatedTags = new TagHydrationSettings()
                        }, true);

                    _model.TotalItemCount = _articleService.GetCount(brand, tags, new[] { PublicationStatus.Published },
                        true, null, true);

                    _model.BaseUrl = "/" + brand + "/data/posts/None/" + filter;

                    if (!Request.Browser.Crawler && _model.TotalItemCount > 0)
                        _statService.RegisterHubViewed(brand, LoginManager.CurrentUser.Id,
                           "Front end", filter);

                    break;

                case ArticleFilterType.Latest:

                    var statList =
                        _statService.GetViewedArticlesByUserId(LoginManager.CurrentUser.Id, null, -14)
                            .ToList();

                    _model.Articles = _articleService.GetPagedInDateRangeOrderedBy(brand,
                        SortOrder.LiveFrom,
                        -System.Math.Abs(14), 0,
                        20,
                        tags, new List<string>(),
                        new[] { PublicationStatus.Published }, true, false, null, _hydrationSettings, false);

                    _model.TotalItemCount = _articleService.GetInDateRangeCount(brand, -System.Math.Abs(14),
                        tags, new List<string>(),
                        new[] { PublicationStatus.Published }, true, null, false);

                    _model.UrlTraker = "?utm_source=home&utm_medium=latest&utm_content=filter&utm_campaign=" + filter +
                                       "&utm_term=14";

                    _model.ShowTags = false;
                    _model.BaseUrl = "/" + brand + "/data/posts/Latest/" + filter;
                    break;

                case ArticleFilterType.Unread:

                    tags =
                        LoginManager.CurrentUser.favouriteTags.Where(i => i.brand == brand)
                            .Select(x => x.tag.UrlSlug).ToArray();

                    var statListUnread =
                        _statService.GetViewedArticlesByUserId(LoginManager.CurrentUser.Id, null, null)
                            .ToList();

                    _model.Articles = _articleService.GetPagedUnreadOrderedBy(brand,
                        SortOrder.LiveFrom,
                         0,
                        20,
                        tags, statListUnread,
                        new[] { PublicationStatus.Published }, true, false, null, _hydrationSettings, false);

                    _model.TotalItemCount = _articleService.GetUnreadDateRangeCount(brand, tags, statListUnread,
                        new[] { PublicationStatus.Published }, true, null, false);

                    _model.UrlTraker = "?utm_source=home&utm_medium=unread&utm_content=filter&utm_campaign=" + filter +
                                       "&utm_term=14";

                    //_model.ShowTags = false;
                    _model.BaseUrl = "/" + brand + "/data/posts/Unread/" + filter;
                    break;


                case ArticleFilterType.UserFav:

                    _model.Articles = _articleService.GetFavoriteArticles(brand,
                        LiveUser.Get().Id, new ArticleHydrationSettings
                        {
                            RelatedTags = new TagHydrationSettings()
                        }, 0, pageSize);

                    _model.TotalItemCount = _model.Articles.Count();

                    _model.BaseUrl = "/" + brand + "/data/posts/UserFav/" + filter;

                    break;

                case ArticleFilterType.UserReading:

                    if (filter != null)
                    {
                        var readingListId = filter;
                        var readingList = _readingListService.FindReadingListById(filter);

                        _model.Articles = _articleService.GetReadingArticlesByReadingList(brand, readingListId,
                            new ArticleHydrationSettings
                            {
                                PrimaryMedia = new MediaHydrationSettings(),
                                RelatedTags = new TagHydrationSettings()
                            }, 0, pageSize);

                        _model.TotalItemCount = _model.Articles.Count();

                        _model.BaseUrl = "/" + brand + "/data/posts/UserReading/" + filter;

                        ViewBag.ReadingList = readingList;
                    }

                    break;

                case ArticleFilterType.History:
                    _model.Articles =
                        _statService.GetViewedArticlesByUserId(brand, LoginManager.CurrentUser.Id, 0,
                            pageSize, new ArticleHydrationSettings
                            {
                                PrimaryMedia = new MediaHydrationSettings(),
                                RelatedTags = new TagHydrationSettings()
                            }).ToArray();
                    _model.TotalItemCount = _model.Articles.Count();

                    _model.BaseUrl = "/" + brand + "/data/posts/History/" + filter;
                    break;

                case ArticleFilterType.Popular:
                    _model.Articles = _articleService.GetPopularArticlesInJobRole(brand, filter, pageSize, -48,
                        _hydrationSettings);
                    _model.TotalItemCount = _model.Articles.Count();

                    _model.BaseUrl = "";
                    break;

                case ArticleFilterType.Search:
                    _model.Articles = _articleService.SearchArticles(brand, filter, pageSize, _hydrationSettings);
                    _model.TotalItemCount = _model.Articles.Count();

                    _model.BaseUrl = "";
                    break;

                default:
                    _model.Articles = _articleService.GetPagedOrderedBy(brand, SortOrder.LiveFrom, 0, pageSize,
                        tags,
                        new[] { PublicationStatus.Published }, true, false, ArticleType.article,
                        new ArticleHydrationSettings
                        {
                            PrimaryMedia = new MediaHydrationSettings(),
                            RelatedTags = new TagHydrationSettings()
                        });

                    _model.TotalItemCount = _articleService.GetCount(brand, tags, new[] { PublicationStatus.Published },
                        true,
                        null);
                    _model.BaseUrl = "/" + brand + "/data/posts/None/" + filter;
                    break;
            }

            _model.FavoriteArticles =
                _articleService.GetFavoriteArticleIds(LoginManager.CurrentUser.Id,
                    new ITOK.Core.Data.Model.HS.ArticleHydrationSettings(), 0, pageSize);
            foreach (var art in _model.Articles)
            {
                art.IsFavoriteForCurrentUser = _model.FavoriteArticles.Contains(art.Id);
            }

            return PartialView("_ListContainer", _model);
        }

        //  [DonutOutputCache(Location = OutputCacheLocation.Any, Duration = 60, VaryByParam = "type;filter;cols;pagesize")]
        /// <summary>
        /// Returns the list of articles used on homepage and hub/hashtag pages
        /// </summary>
        /// <param name="type"></param>
        /// <param name="filter"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        public ActionResult ListContainerLite(ArticleFilterType type, string filter = "", int? pagesize = null,
            bool ShowTags = true, string misc = "")
        {
            var pageSize = pagesize ?? DefaultPageSize;

            string[] tags = { };

            if (!string.IsNullOrEmpty(filter))
            {
                tags = filter.Split('+').ToArray().OrderBy(x => x).ToArray();
            }

            _model = new ArticleFilterModel { Filter = filter, FilterType = type, ShowTags = ShowTags, };

            var brand = GlobalVariables.CurrentBrand;

            switch (type)
            {
                case ArticleFilterType.None:

                    _model.Articles = _articleService.GetPagedOrderedBy(brand, SortOrder.LiveFrom, 0, pageSize,
                        tags,
                        new[] { PublicationStatus.Published }, true, false, null, new ArticleHydrationSettings
                        {
                            PrimaryMedia = new MediaHydrationSettings(),
                            RelatedTags = new TagHydrationSettings()
                        });

                    _model.TotalItemCount = _articleService.GetCount(brand, tags, new[] { PublicationStatus.Published },
                        true,
                        null);

                    _model.BaseUrl = "/" + brand + "/data/posts/None/" + filter;

                    if (!Request.Browser.Crawler && _model.TotalItemCount > 0)
                        _statService.RegisterHubViewed(brand, LoginManager.CurrentUser.Id,
                           "Frontend", filter);

                    break;

                case ArticleFilterType.Latest:

                    var statList =
                        _statService.GetViewedArticlesByUserId(LoginManager.CurrentUser.Id, null, -14)
                            .ToList();

                    _model.Articles = _articleService.GetPagedInDateRangeOrderedBy(brand,
                        SortOrder.LiveFrom,
                        -System.Math.Abs(7), 0,
                        10,
                        tags, new List<string>(),
                        new[] { PublicationStatus.Published }, true, false, null, _hydrationSettings, false);

                    _model.TotalItemCount = _articleService.GetInDateRangeCount(brand, -System.Math.Abs(7),
                        tags, new List<string>(),
                        new[] { PublicationStatus.Published }, true, null, false);

                    _model.UrlTraker = "?utm_source=home&utm_medium=latest&utm_content=filter&utm_campaign=" + filter +
                                       "&utm_term=7";

                    _model.ShowTags = false;

                    break;


                case ArticleFilterType.UserFav:

                    _model.Articles = _articleService.GetFavoriteArticles(brand,
                        LoginManager.CurrentUser.Id, new ArticleHydrationSettings
                        {
                            RelatedTags = new TagHydrationSettings()
                        }, 0, pageSize);

                    _model.TotalItemCount = _model.Articles.Count();

                    _model.BaseUrl = "/" + brand + "/data/posts/UserFav/" + filter;

                    break;

                case ArticleFilterType.UserReading:

                    if (filter != null)
                    {
                        var readingListId = filter;
                        var readingList = _readingListService.FindReadingListById(filter);

                        _model.Articles = _articleService.GetReadingArticlesByReadingList(brand, readingListId,
                            new ArticleHydrationSettings
                            {
                                PrimaryMedia = new MediaHydrationSettings(),
                                RelatedTags = new TagHydrationSettings()
                            }, 0, pageSize);

                        _model.TotalItemCount = _model.Articles.Count();

                        _model.BaseUrl = "/" + brand + "/data/posts/UserReading/" + filter;

                        ViewBag.ReadingList = readingList;
                    }

                    break;

                case ArticleFilterType.History:
                    _model.Articles =
                        _statService.GetViewedArticlesByUserId(brand, LoginManager.CurrentUser.Id, 0,
                            pageSize, new ArticleHydrationSettings
                            {
                                PrimaryMedia = new MediaHydrationSettings(),
                                RelatedTags = new TagHydrationSettings()
                            }).ToArray();
                    _model.TotalItemCount = _model.Articles.Count();

                    _model.BaseUrl = "/" + brand + "/data/posts/History/" + filter;
                    break;

                case ArticleFilterType.Popular:
                    _model.Articles = _articleService.GetPopularArticlesInJobRole(brand, filter, pageSize, -10,
                        _hydrationSettings);
                    _model.TotalItemCount = _model.Articles.Count();

                    _model.BaseUrl = "";
                    break;

                case ArticleFilterType.Article:
                    _model.Articles = _articleService.GetPagedOrderedBy(brand, SortOrder.LiveFrom, 0, 6, tags,
                        new[] { PublicationStatus.Published }, true, false, null, new ArticleHydrationSettings
                        {
                            PrimaryMedia = new MediaHydrationSettings(),
                        }, true).Where(x => x.UrlSlug != misc).ToList();
                    _model.TotalItemCount = 0;

                    _model.BaseUrl = "";

                    break;

                case ArticleFilterType.Search:
                    _model.Articles = _articleService.SearchArticles(brand, filter, pageSize, _hydrationSettings);
                    _model.TotalItemCount = _model.Articles.Count();

                    _model.BaseUrl = "";
                    break;

                default:
                    _model.Articles = _articleService.GetPagedOrderedBy(brand, SortOrder.LiveFrom, 0, pageSize,
                        tags,
                        new[] { PublicationStatus.Published }, true, false, ArticleType.article,
                        new ArticleHydrationSettings
                        {
                            PrimaryMedia = new MediaHydrationSettings(),
                            RelatedTags = new TagHydrationSettings()
                        });

                    _model.TotalItemCount = _articleService.GetCount(brand, tags, new[] { PublicationStatus.Published },
                        true,
                        null);
                    _model.BaseUrl = "/" + brand + "/data/posts/None/" + filter;
                    break;
            }

            _model.FavoriteArticles =
                _articleService.GetFavoriteArticleIds(LoginManager.CurrentUser.Id,
                    new ITOK.Core.Data.Model.HS.ArticleHydrationSettings(), 0, pageSize);
            foreach (var art in _model.Articles)
            {
                if (art != null && art.Id != null)
                    art.IsFavoriteForCurrentUser = _model.FavoriteArticles.Contains(art.Id);
            }

            return PartialView("_ListContainerLite", _model);
        }

        /// <summary>
        /// Returns the next and previous article compared to the current one, given current filters
        /// </summary>
        /// <param name="type"></param>
        /// <param name="value"></param>
        /// <param name="filter"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        public ActionResult PreviousNext(ArticleFilterType type, string value, string filter = "", int skip = 0, int? pagesize = null, bool iconOnly = false)
        {
            var pageSize = pagesize ?? DefaultPageSize;

            string[] tags = { };

            if (!string.IsNullOrEmpty(filter))
            {
                tags = filter.Split('+').ToArray().OrderBy(x => x).ToArray();
            }

            ///TODO: Work out paging - when the article is item number 22
            /// will need to pass in page from listcontainer (and load more) and across to article detail
            var model = new ArticlePreviousNextModel
            {
                Filter = filter,
                iconOnly = iconOnly
            };
            List<Article> list;
            var brand = GlobalVariables.CurrentBrand;

            switch (type)
            {
                case ArticleFilterType.None:

                    list = _articleService.GetPagedOrderedBy(brand, SortOrder.LiveFrom, 0, pageSize + skip,
                        tags,
                        new[] { PublicationStatus.Published }, true, false, null, new ArticleHydrationSettings
                        {
                            PrimaryMedia = new MediaHydrationSettings(),
                        }).ToList();

                    break;

                default:
                    list = _articleService.GetPagedOrderedBy(brand, SortOrder.LiveFrom, 0, pageSize + skip,
                        tags,
                        new[] { PublicationStatus.Published }, true, false, ArticleType.article, new ArticleHydrationSettings
                        {
                            PrimaryMedia = new MediaHydrationSettings(),
                        }).ToList();

                    break;
            }
            var article = list.FirstOrDefault(i => i.Id == value);
            model.nextArticle = list.SkipWhile(x => x != article).Skip(1).FirstOrDefault();
            list.Reverse();
            model.previousArticle = list.SkipWhile(x => x != article).Skip(1).FirstOrDefault();
            ViewBag.skip = skip;
            return PartialView("PreviousNext", model);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult AddComment(string comment, string title, string urlSlug)
        {
            if (string.IsNullOrEmpty(comment) || string.IsNullOrEmpty(urlSlug)) return Json(new { Success = true });
            var article = _articleService.FindBySlug(Brand.itok, urlSlug, null);

            if (article == null)
                return Json(new { Success = false });

            _articleCommentService.Save(new ArticleComment() { Comment = comment, ArticleId = article.Id, UserIdentification = LiveUser.Get().Id, CreatedOn = DateTime.UtcNow });

            #region Send Email

            try
            {
                var sb = new StringBuilder();
                sb.AppendFormat("Article: {0}<br/>", article.Title);
                sb.AppendFormat("From: {0} {1}<br/>", LiveUser.Get().Forename, LiveUser.Get().Surname);
                sb.AppendFormat("Email: {0}<br/>", LiveUser.Get().Email);
                sb.AppendFormat("Comment: {0}", comment);
                var defaultNotificationEmail = ConfigurationManager.AppSettings["DefaultEmail"];

                var areaTag = article.RelatedTags.Where(i => i.TagType == TagType.Area).First();
                if (areaTag != null)
                {
                    var tagDetail = _tagsService.FindByUrlSlug(areaTag.UrlSlug, Brand.itok);

                    //if the tag has a contact email set, then change the email address to it
                    if (!string.IsNullOrEmpty(tagDetail.ContactEmail))
                        defaultNotificationEmail = tagDetail.ContactEmail;
                }


                SESUtilities.SendEmail(defaultNotificationEmail, "", "Comment Received", sb.ToString(), sb.ToString());

            }
            catch (Exception ex)
            {
            }

            #endregion Send Email

            return Json(new { Success = true });
        }

        public ActionResult PopularArticlesPartial(string jobRoleId = "")
        {
            var popularArticles = _articleService.GetPopularArticlesInJobRole(GlobalVariables.CurrentBrand, jobRoleId, 5);
            return PartialView("_PopularArticlesList", popularArticles);
        }

        public ActionResult PopularTagsPartialViewResult(string urlSlug)
        {
            var model = new TagsViewModel
            {
                PopularTags = _articleService.GetPopularTagsList(urlSlug)
            };
            return PartialView("PopularTabs", model);
        }



        [CustomAuthorise]
        public ActionResult UpdateFavorite(string articleId, bool yesno)
        {
            var brand = GlobalVariables.CurrentBrand;

            _statService.RegisterFavorite(LiveUser.Get().Id, "Front end", articleId, yesno ? 0 : 1, LiveUser.Get().Id, brand);

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [CustomAuthorise]
        public ActionResult GetFavorite(string articleId)
        {
            var favorite = _statService.FindFavoriteByArticleAndUser(articleId, LiveUser.Get().Id);

            return Json(favorite != null ? new { Favorited = true, YesNo = favorite.StatisticType } : new { Favorited = false, YesNo = FavoriteStatType.Yes }, JsonRequestBehavior.AllowGet);
        }

        [CustomAuthorise]
        public ActionResult UpdateReading(string articleId, bool yesno, string readingListId)
        {
            var brand = GlobalVariables.CurrentBrand;

            _statService.RegisterReading(LoginManager.CurrentUser.Id, "Front end", articleId, yesno ? 0 : 1, LoginManager.CurrentUser.Id, readingListId, brand);

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [CustomAuthorise]
        [Route("{brand}/article/getreading")]
        public ActionResult GetReading(Brand brand, string articleId, string readingListId)
        {
            if (string.IsNullOrEmpty(articleId) || string.IsNullOrEmpty(readingListId))
                return Json(null, JsonRequestBehavior.AllowGet);
            var article = _articleService.FindById(brand, articleId, new ArticleHydrationSettings());
            var reading = _statService.FindReadingByArticleAndReadingList(article.UrlSlug, readingListId);

            return Json(reading != null ? new { Reading = true, YesNo = reading.StatisticType } : new { Reading = false, YesNo = ReadingStatType.Yes }, JsonRequestBehavior.AllowGet);
        }
    }
}