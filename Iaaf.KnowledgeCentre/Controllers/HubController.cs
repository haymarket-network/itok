﻿using ITOK.Core.Common;
using ITOK.Core.Common.Constants;
using ITOK.Core.Common.Utilities;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using ITOK.Core.Data.Model.HS;
using ITOK.Core.Services.Core;
using ITOK.Core.Services.Security;
using ITOKFrontend.Helpers;
using ITOKFrontend.Helpers.Authorise;
using ITOKFrontend.Models;
using ITOKFrontend.Models.Constants;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Tag = ITOKFrontend.Models.Tag;
using UsersComment = ITOKFrontend.Models.UsersComment;

namespace ITOKFrontend.Controllers
{
    public class HubController : BaseController
    {
        // GET: Hub
        public readonly ArticleService _articleService;

        public readonly ITagsService _tagsService;
        public readonly StatsService _statService;

        private IMongoRepository1 mongoRepository;
        public readonly IAnswerFilterService _answerFilterService;
        private readonly IQuestionCommentService _questionCommentService;
        private readonly IAnswerFlagService _answerFlagService;
        private readonly ITOK.Core.Services.CMS.UserService _authUserService;

        private readonly IQuestionService _questionService;
        private readonly IAnswerService _answerService;
        private readonly IQuestionTemplateService _questionTemplateService;

        public HubController()
        {
            mongoRepository = AppKernel.GetInstance<IMongoRepository1>();
            _answerFilterService = AppKernel.GetInstance<IAnswerFilterService>();

            _tagsService = AppKernel.GetInstance<ITagsService>();
            _questionCommentService = AppKernel.GetInstance<QuestionCommentService>();
            _answerFlagService = AppKernel.GetInstance<AnswerFlagService>();
            _authUserService = AppKernel.GetInstance<ITOK.Core.Services.CMS.UserService>();

            _questionService = AppKernel.GetInstance<IQuestionService>();
            _answerService = AppKernel.GetInstance<IAnswerService>();
            _questionTemplateService = AppKernel.GetInstance<IQuestionTemplateService>();
            _articleService = new ArticleService(mongoRepository);
        }
        public int DefaultPageSize = 20;
        [CustomAuthorise]
        public ActionResult Index()
        {
            if (!LiveUser.Get().AcceptedRegulations)
            {
                return RedirectToAction("AcceptRegulations", "Home");
            }
            var area = Request.QueryString["a"];
            var events = Request.QueryString["e"];
            var category = Request.QueryString["c"];
            var year = Request.QueryString["y"];
            var searchText = Request.QueryString["s"];
            if (string.IsNullOrEmpty(searchText) && string.IsNullOrEmpty(area) && string.IsNullOrEmpty(events) && string.IsNullOrEmpty(category) && string.IsNullOrEmpty(year))
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                var tagNames = new List<string>();
                if (!string.IsNullOrEmpty(area))
                { tagNames.Add(area); }
                if (!string.IsNullOrEmpty(events))
                { tagNames.Add(events); }
                if (!string.IsNullOrEmpty(category))
                { tagNames.Add(category); }
                if (!string.IsNullOrEmpty(year))
                { tagNames.Add(year); }

                var _tags = _tagsService.FindByUrlSlug(tagNames.ToArray());
                var groupBy = new List<string>();
                var groups = new List<string>();
                bool isGroupHeader = false;
                var filteredData = _answerFilterService.GetPagedOrderedBy(SortOrder.CreatedOn, 0, int.MaxValue, tagNames.ToArray(), true, searchText, false, LiveUser.Get().Id);

                // check if there are any answer which is having blank data, Will reduce that row

                var finalData = RemoveEmptyAnswers(filteredData);
                filteredData = finalData;

                if (!string.IsNullOrEmpty(area) && !string.IsNullOrEmpty(year) && !string.IsNullOrEmpty(events))
                { }
                else if (string.IsNullOrEmpty(area) && string.IsNullOrEmpty(category) && !string.IsNullOrEmpty(year) && !string.IsNullOrEmpty(events))
                {
                    groups = filteredData.Select(x => x.AnswerTags.Where(y => y.TagType == TagType.Area).FirstOrDefault()).ToList().Select(z => z.Text).ToList().Distinct().ToList();
                    isGroupHeader = true;
                }

                var _favoriteArticles =
              _answerFilterService.GetFavoriteAnswerIds(LoginManager.CurrentUser.Id, 0, DefaultPageSize);

                var answerFilterData = new List<AnswerFilter>();
                foreach (var item in groups)
                {
                    var _filteredResult = filteredData.Where(x => x.AnswerTags.Any(y => y.Text.Equals(item))).ToList();

                    var answerFilters = new AnswerFilter
                    {
                        GroupByHeader = item,
                        Answers = _filteredResult
                    };
                    foreach (var ans in answerFilters.Answers)
                    {
                        ans.IsFavoriteForCurrentUser = _favoriteArticles.Contains(ans.Id);
                    }
                    answerFilterData.Add(answerFilters);
                };
                if (!isGroupHeader)
                {
                    var answerFilters = new AnswerFilter
                    {
                        GroupByHeader = "",
                        Answers = filteredData.ToList()
                    };
                    foreach (var ans in answerFilters.Answers)
                    {
                        //ans.IsFavoriteForCurrentUser = _favoriteArticles.Contains(ans.Id);
                    }
                    answerFilterData.Add(answerFilters);
                }

                var allTaglist = _tagsService.All();

                var areaTags = !string.IsNullOrEmpty(category) ? allTaglist.Where(i => i.ParentSlug == category).OrderBy(i => i.DisplayText).ToList() : allTaglist.Where(i => i.TagType == TagType.Area).OrderBy(i => i.DisplayText).ToList();
                var yearTags = !string.IsNullOrEmpty(events) ? allTaglist.Where(i => i.ParentSlug == events).OrderBy(i => i.DisplayText).ToList() : allTaglist.Where(i => i.TagType == TagType.Year).OrderBy(i => i.DisplayText).ToList();
                var eventTags = allTaglist.Where(i => i.TagType == TagType.Events).OrderBy(i => i.DisplayText).ToList();
                var categoryTags = allTaglist.Where(i => i.TagType == TagType.Category).OrderBy(i => i.DisplayText).ToList();

                var _selectedEventId = ""; var _selectedYearId = ""; var _selectedCategoryId = ""; var _selectedAreaId = "";

                if (_tags.Any())
                {
                    _selectedEventId = _tags.Where(x => x.TagType == TagType.Events).Any() ? _tags.Where(x => x.TagType == TagType.Events).FirstOrDefault().UrlSlug : "";
                    _selectedYearId = _tags.Where(x => x.TagType == TagType.Year).Any() ? _tags.Where(x => x.TagType == TagType.Year).FirstOrDefault().UrlSlug : "";
                    _selectedCategoryId = _tags.Where(x => x.TagType == TagType.Category).Any() ? _tags.Where(x => x.TagType == TagType.Category).FirstOrDefault().UrlSlug : "";
                    _selectedAreaId = _tags.Where(x => x.TagType == TagType.Area).Any() ? _tags.Where(x => x.TagType == TagType.Area).FirstOrDefault().UrlSlug : "";
                }
                if (answerFilterData.Count > 0)
                {
                    FillAnswerFlagAndCheckComment(answerFilterData);
                }

                var model = new AnswerFilterViewModel
                {
                    AllFilteredData = answerFilterData,
                    FilterByTag = groupBy,
                    OverviewTitle = "Event Overview",
                    SearchHeader = "Result for medical",
                    EventTags = eventTags,
                    YearTags = yearTags,
                    AreaTags = areaTags,
                    CategoryTags = categoryTags,
                    SelectedAreaId = _selectedAreaId,
                    SelectedCategoryId = _selectedCategoryId,
                    SelectedEventId = _selectedEventId,
                    SelectedYearId = _selectedYearId
                };

                return View(model);
            }
        }

        private void calculateStaffHiredInLOC(Answer answer)
        {
            var questionRow = new QuestionRow();


            var lastQuestionRow = answer.QuestionTemplate.Questions.Last();


            lastQuestionRow = answer.QuestionTemplate.Questions.Last();
            lastQuestionRow.MvcItemName = string.Format("Model.Questions[{0}]", answer.QuestionTemplate.Questions.Count);
            lastQuestionRow.Question = "";
            for (int i = 0; i < answer.QuestionTemplate.Questions.Count; i++)
            {
                var question = answer.QuestionTemplate.Questions[i];

                if (i != answer.QuestionTemplate.Questions.Count - 1)
                {
                    for (int j = 0; j < question.AnswersRows.Count; j++)
                    {
                        var currentAnswer = question.AnswersRows[j];

                        var lastQuestionAnswer = lastQuestionRow.AnswersRows[j];
                        if (lastQuestionAnswer.AnswerNumber == null)
                        {
                            lastQuestionAnswer.AnswerNumber = 0;
                        }

                        lastQuestionAnswer.AnswerNumber += currentAnswer.AnswerNumber == null ? 0 : currentAnswer.AnswerNumber;

                    }
                }
            }


            var rowCount = 0;
            foreach (var item in lastQuestionRow.AnswersRows)
            {
                questionRow.AnswersRows.Add(new AnswerRow
                {
                    AnswerDataType = item.AnswerDataType
                });

                rowCount++;
            }
            answer.QuestionTemplate.Questions.Add(questionRow);
            lastQuestionRow = answer.QuestionTemplate.Questions.Last();
            lastQuestionRow.MvcItemName = string.Format("Model.Questions[{0}]", answer.QuestionTemplate.Questions.Count);
            lastQuestionRow.Question = "";
            for (int i = 0; i < answer.QuestionTemplate.Questions.Count; i++)
            {
                var question = answer.QuestionTemplate.Questions[i];

                if (i != answer.QuestionTemplate.Questions.Count - 1)
                {
                    for (int j = 0; j < question.AnswersRows.Count; j++)
                    {
                        var currentAnswer = question.AnswersRows[j];

                        var lastQuestionAnswer = lastQuestionRow.AnswersRows[j];
                        if (lastQuestionAnswer.AnswerNumber == null)
                        {
                            lastQuestionAnswer.AnswerNumber = 0;
                        }

                        lastQuestionAnswer.AnswerNumber += currentAnswer.AnswerNumber == null ? 0 : currentAnswer.AnswerNumber;

                    }
                }
            }


            var numbers = new List<int>();
            for (int i = 0; i < lastQuestionRow.AnswersRows.Count; i++)
            {
                AnswerRow currentCol = null;

                if (lastQuestionRow.AnswersRows.Count != i) // skip last col it is useless
                {
                    currentCol = lastQuestionRow.AnswersRows[i];
                    numbers.Add(currentCol.AnswerNumber == null ? 0 : currentCol.AnswerNumber.Value);
                }
            }

            for (int i = 0; i < lastQuestionRow.AnswersRows.Count; i++)
            {
                AnswerRow currentCol = null;
                AnswerRow prevCol = null;
                if (lastQuestionRow.AnswersRows.Count != i + 1) // skip last col it is useless
                {
                    currentCol = lastQuestionRow.AnswersRows[i];

                    if (i > 0)
                    {
                        prevCol = lastQuestionRow.AnswersRows[i - 1];
                    }

                    if (currentCol != null && prevCol != null)
                    {
                        currentCol.AnswerNumber = prevCol.AnswerNumber + numbers[i];
                    }
                }
            }
        }

        public ActionResult GetQuestionDetail(string answerId)
        {
            var answer = _answerService.FindById(answerId);

            if (answer.TemplateType == QuestionTemplateType.TransportationTemplate)
            {
                return PartialView("_TransportationTemplate", answer);
            }
            else if (answer.TemplateType == QuestionTemplateType.VolunteersPerArea)
            {
                return PartialView("_VolunteersPerArea", answer);
            }
            else if (answer.TemplateType == QuestionTemplateType.StaffHiredInLOC)
            {
                calculateStaffHiredInLOC(answer);

                return PartialView("_StaffHiredInLOC", answer);
            }
            else if (answer.TemplateType == QuestionTemplateType.SiteVisitsSummary)
            {
                return PartialView("_SiteVisitsSummary", answer);
            }
            else if (answer.TemplateType == QuestionTemplateType.CarpoolUsagePerDay)
            {
                return PartialView("_CarpoolUsagePerDay", answer);
            }
            else if (answer.TemplateType == QuestionTemplateType.AccommodationFormUpdatedbyEndofEvent)
            {
                return PartialView("_AccommodationFormUpdated", answer);
            }
            else
            {
                return PartialView("", null);
            }

            return null;
        }

        public ActionResult GetQuestionChart(string answerId)
        {
            var answer = _answerService.FindById(answerId);

            if (answer.TemplateType == QuestionTemplateType.VolunteersPerArea)
            {
                return PartialView("_VolunteersPerAreaCharts", answer);
            }
            else if (answer.TemplateType == QuestionTemplateType.StaffHiredInLOC)
            {
                calculateStaffHiredInLOC(answer);
                return PartialView("_StaffHiredInLOCCharts", answer);
            }
            else if (answer.TemplateType == QuestionTemplateType.CarpoolUsagePerDay)
            {
                return PartialView("_CarpoolUsagePerDayCharts", answer);
            }
            else
            {
                return PartialView("", null);
            }

            return null;
        }

        public ActionResult GetQuestionCommentsByAnswerId(string answerId, string questionText)
        {
            AnswersComments model = new AnswersComments();
            if (answerId != null)
            {
                var comments = _questionCommentService.FindByAnswerId(answerId);
                if (comments != null)
                {
                    foreach (var ucomment in comments.UsersComment)
                    {
                        var user = _authUserService.FindById(ucomment.UserId);
                        ucomment.UserName = user.Forename + " " + user.Surname;

                        model.UsersComment.Add(new UsersComment
                        {
                            Comment = ucomment.Comment,
                            CommentDate = ucomment.CommentDate,
                            UserName = ucomment.UserName
                        });
                    }
                }
                model.QuestionText = questionText;
            }
            return PartialView("_AnswerComment", model);
        }

        [CustomAuthorise]
        public ActionResult Find(Brand brand, string brandArticleNumber)
        {

            if (!LiveUser.Get().AcceptedRegulations)
            {
                return RedirectToAction("AcceptRegulations", "Home");
            }

            System.Text.RegularExpressions.Regex compare = new System.Text.RegularExpressions.Regex(@"/.{2}\d+/");
            var matchPattern = @".{2}\d+";
            if (System.Text.RegularExpressions.Regex.IsMatch(brandArticleNumber, matchPattern, System.Text.RegularExpressions.RegexOptions.IgnoreCase))
            {
                var articles = _articleService.FindByBrandArticleNumber(brand,
                    brandArticleNumber,
                    new ArticleHydrationSettings() { }, new[] { PublicationStatus.Published },
                    true);
                if (articles.Count == 1)
                {
                    return Redirect(ArticleUtilities.GetArticleUri(articles.First()));
                }
                // return RedirectToAction("Detail", "Article", new { urlSlug = articles.First().UrlSlug });
                else
                {
                    return RedirectToAction("Index", "Hub", new { urlSlug = brandArticleNumber });
                }
            }
            else
            {
                return RedirectToAction("Index", "Hub", new { urlSlug = brandArticleNumber });
            }
        }

        [CustomAuthorise]
        public ActionResult GetUserFavourite(string userId)
        {
            var _favouriteAnswerData = _answerFilterService.GetFavoriteAnswers(userId, 0, DefaultPageSize);
            var _favouriteAnswerList = new List<AnswerFilter>();

            _favouriteAnswerList.Add(new AnswerFilter
            {
                GroupByHeader = "",
                Answers = _favouriteAnswerData.ToList()
            });

            return PartialView("_ListContent", _favouriteAnswerList);
        }

        public ActionResult ListContainerReadingList(ArticleFilterType type, string filter = "", int? pagesize = null, bool ShowTags = true)
        {

            var readingListId = filter;
            var readingList = _readingListService.FindReadingListById(filter);

            IList<AnswerFilterData> readingLists = _answerFilterService.GetReadingList(filter, 0, int.MaxValue);

            var _favouriteAnswerList = new List<AnswerFilter>();
            _favouriteAnswerList.Add(new AnswerFilter
            {
                GroupByHeader = "",
                Answers = readingLists.ToList()
            });

            ViewBag.readingListId = readingListId;

            return PartialView("_ListContentReadingList", _favouriteAnswerList);
        }

        public bool RemoveFromReadingList(string readingListId, string answerId)
        {
            return _answerFilterService.RemoveAnswerFromReadingList(readingListId, answerId);
        }

        [CustomAuthorise]
        public ActionResult Latest(ArticleType? type)
        {
            return View(type);
        }

        [CustomAuthorise]
        public ActionResult Unread()
        {
            return View();
        }

        [CustomAuthorise]
        public ActionResult Popular()
        {
            return View();
        }

        /// <summary>
        /// Returns JSON of related tags, according to the current filter
        /// </summary>
        /// <param name="tagFilter"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public JsonResult GetRelatedTags(string tagFilter)
        {
            var tags = new string[] { };

            var articleFilterType = ArticleFilterType.None;

            if (tagFilter == "Unread")
            {
                articleFilterType = ArticleFilterType.Unread;
                tagFilter = string.Empty;
            }
            else if (tagFilter == "Latest")
            {
                articleFilterType = ArticleFilterType.Latest;
                tagFilter = string.Empty;
            }

            if (!string.IsNullOrEmpty(tagFilter))
            {
                tags = tagFilter.Split('+').ToArray();
            }

            switch (articleFilterType)
            {
                case ArticleFilterType.Unread:

                    var statListUnread =
                        _statService.GetViewedArticlesByUserId(LoginManager.CurrentUser.Id, null, null)
                            .ToList();

                    tags =
                        LoginManager.CurrentUser.favouriteTags.Where(i => i.brand == GlobalVariables.CurrentBrand)
                            .Select(x => x.tag.UrlSlug).ToArray();

                    var relatedTagsUnread = _articleService.FindRelatedTags(GlobalVariables.CurrentBrand, tags, new[] { PublicationStatus.Published }, true, false, statListUnread);

                    return Json((from tag in relatedTagsUnread.ToList()
                                 let details = tag["_id"].AsBsonDocument
                                 select new Tag
                                 {
                                     tagCount = tag.GetElement("count").Value.ToString(),
                                     tagName = details.GetElement("DisplayText").Value.ToString(),
                                     tagSlug = details.GetElement("UrlSlug").Value.ToString()
                                 }).ToList(), JsonRequestBehavior.AllowGet);

                case ArticleFilterType.Latest:

                    return Json((from tag in _articleService.FindRelatedTags(GlobalVariables.CurrentBrand, tags, new[] { PublicationStatus.Published }, true, false).ToList()
                                 let details = tag["_id"].AsBsonDocument
                                 select new Tag
                                 {
                                     tagCount = tag.GetElement("count").Value.ToString(),
                                     tagName = details.GetElement("DisplayText").Value.ToString(),
                                     tagSlug = details.GetElement("UrlSlug").Value.ToString()
                                 }).ToList(), JsonRequestBehavior.AllowGet);

                default:
                    break;
            }

            var relatedTags = _articleService.FindRelatedTags(GlobalVariables.CurrentBrand, tags, new[] { PublicationStatus.Published }, true, false);

            var results = (from tag in relatedTags.ToList()
                           let details = tag["_id"].AsBsonDocument
                           select new Tag
                           {
                               tagCount = tag.GetElement("count").Value.ToString(),
                               tagName = details.GetElement("DisplayText").Value.ToString(),
                               tagSlug = details.GetElement("UrlSlug").Value.ToString()
                           }).ToList();

            return Json(results, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Partial View for related tags - inside the view it calls GetRelatedTags
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public ActionResult RelatedTags(string filter)
        {
            return PartialView("RelatedTags", new RelatedTagsModel { filter = filter });
        }

        public ActionResult PopularHubPartial(string jobRoleId = "", int count = 20, int hours = -48)
        {
            var popularHubs = _articleService.GetPopularHubsInJobRole(GlobalVariables.CurrentBrand, jobRoleId, count, hours);
            return PartialView("_PopularHubList", popularHubs);
        }

        public ActionResult PopularTags()
        {
            return PartialView("PopularTags");
        }

        public ActionResult PopularArticles()
        {
            return PartialView("PopularArticles");
        }

        public ActionResult Pages(string filter)
        {
            var articles = _articleService.GetPagedOrderedBy(GlobalVariables.CurrentBrand, SortOrder.LiveFrom, 0, 100,
                          filter.Split('+').ToArray(),
                          new[] { PublicationStatus.Published }, true, false, ArticleType.page, new ArticleHydrationSettings
                          {
                              PrimaryMedia = new MediaHydrationSettings(),
                              RelatedTags = new TagHydrationSettings()
                          });

            return PartialView("Pages", articles);
        }

        #region private functions
        private void FillAnswerFlagAndCheckComment(List<AnswerFilter> answersFilterData)
        {

            foreach (var item in answersFilterData.FirstOrDefault().Answers)
            {
                string answerId = item.Id;

                #region Check Iscomment
                var comments = _questionCommentService.FindByAnswerId(answerId);
                if (comments != null)
                {
                    item.isCommented = true;
                }

                #endregion

                #region Get flag data
                var flagData = _answerFlagService.FindByAnswerId(answerId);
                if (flagData != null)
                {
                    item.isFlagSet = true;
                    item.FlagColor = flagData.FlagColor;
                }
                #endregion
            }
        }

        private IList<AnswerFilterData> RemoveEmptyAnswers(IList<AnswerFilterData> data)
        {
            var filterdData = new List<AnswerFilterData>();
            foreach (var item in data)
            {
                switch (item.AnswerType)
                {
                    case AnswerType.Attachment:
                        if (!string.IsNullOrEmpty(item.AttachmentId))
                        {
                            filterdData.Add(item);
                        }
                        break;
                    case AnswerType.Template:
                        if (isTemplateAnswerAvailable(item.QuestionTemplate.Questions))
                        {
                            filterdData.Add(item);
                        }
                        break;
                    default:
                        if (!string.IsNullOrEmpty(item.AnswerValue))
                        {
                            filterdData.Add(item);
                        }
                        break;
                }

            }
            return filterdData;
        }
        private bool isTemplateAnswerAvailable(List<QuestionRow> questions)
        {
            foreach (var question in questions)
            {
                foreach (var answers in question.AnswersRows)
                {
                    if (answers.AnswerNumber != null || answers.AnswerString != null)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        #endregion
    }
}