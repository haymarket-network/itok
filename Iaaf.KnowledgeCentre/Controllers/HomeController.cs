﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ITOKFrontend.Helpers;
using ITOKFrontend.Models;
using ITOKFrontend.Models.Constants;
using ITOK.Core.Common;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data;
using ITOK.Core.Data.Model.HS;
using ITOK.Core.Services.Core;
using ITOK.Core.Services.Security;

namespace ITOKFrontend.Controllers
    {
    public class HomeController : BaseController
        {
        private readonly IMongoRepository1 _mongoRepository;

        public HomeController()
            {
            _mongoRepository = AppKernel.GetInstance<IMongoRepository1>();

            _articleService = new ArticleService(_mongoRepository);
            _jobRoleService = new JobRoleService(_mongoRepository);
            _areasService = new AreasOfInterestService(_mongoRepository);
            _UserService = new ITOK.Core.Services.CMS.UserService(_mongoRepository);
            _tagsService = AppKernel.GetInstance<ITagsService>();
            _weblinkService = new WeblinkService(_mongoRepository);
            _alertService = new AlertService(_mongoRepository);

            _curatedContentService = new CuratedContentService(_mongoRepository);
            _statService = new StatsService(_mongoRepository);
            }

        public HomeController(IMongoRepository1 mongoRepository)
            {
            this._mongoRepository = mongoRepository;

            _articleService = new ArticleService(_mongoRepository);
            _areasService = new AreasOfInterestService(_mongoRepository);
            _jobRoleService = new JobRoleService(_mongoRepository);
            _UserService = new ITOK.Core.Services.CMS.UserService(_mongoRepository);
            _tagsService = AppKernel.GetInstance<ITagsService>();
            _weblinkService = new WeblinkService(_mongoRepository);
            _alertService = new AlertService(_mongoRepository);
            _curatedContentService = new CuratedContentService(_mongoRepository);
            _statService = new StatsService(_mongoRepository);
            }

        private readonly ArticleService _articleService;
        private readonly AreasOfInterestService _areasService;
        private readonly JobRoleService _jobRoleService;
        private readonly ITagsService _tagsService;
        private readonly ITOK.Core.Services.CMS.UserService _UserService;
        private readonly WeblinkService _weblinkService;
        private readonly AlertService _alertService;
        private readonly CuratedContentService _curatedContentService;
        private readonly StatsService _statService;

        private readonly ArticleHydrationSettings _hydrationSettings = new ArticleHydrationSettings
        {
            PrimaryMedia = new MediaHydrationSettings()
        };

        public int DefaultPageSize = 20;

        [CMSAuthorize("UseFrontEnd")]
        public ActionResult Index(string urlSlug)
        {
          
            return RedirectToAction("History");
        }
        [CMSAuthorize("UseFrontEnd")]
        public ActionResult AcceptRegulations()
        {        

            return View();
        }

        [HttpPost]
        [CMSAuthorize("UseFrontEnd")]
        public ActionResult AcceptRegulations(bool? accept)
        {

            if (accept == true)
            {
                LiveUser.Get().AcceptedRegulations = true;

                var user = _UserService.FindById(LiveUser.Get().Id);
                user.AcceptedRegulations = true;
                _UserService.Save(user);

                return RedirectToAction("History", "Home");
            }
            else
            {
                ViewBag.ErrorMessage = "You must accept before you can proceed";
                return View();
            }
        }

        [CMSAuthorize("UseFrontEnd")]
        public ActionResult History(string urlSlug)
        {

            if (!LiveUser.Get().AcceptedRegulations)
                return RedirectToAction("AcceptRegulations");

            var tags = new List<string> { };
            var tagNames = new List<string> { };
            var _brand = GlobalVariables.CurrentBrand;

          

            var filter = string.Join("+", tags.ToArray());

            //VW Briefing

        

            //latest x on primary job role.

            var model = new HomeViewModel
            {
                Tags = _tagsService.FindByUrlSlug(tags.ToArray()),
                Missed14Count = TypeFiltersCount(FeedType.Missing, filter, 14, false),
                MyRoleContent = null,
                EventTags = _tagsService.All().Where(i => i.TagType == TagType.Events).ToList(),
                AreaTags = _tagsService.All().Where(i => i.TagType == TagType.Area).ToList()

            };

            return View(model);
            }

        [CMSAuthorize("UseFrontEnd")]
        public ActionResult FindBy(string type)
        {
            List<ITOK.Core.Data.Model.Tags> options = new List<ITOK.Core.Data.Model.Tags>();
            switch (type)
            {
                case "event":
                    options = _tagsService.All().Where(i => i.TagType == TagType.Events).ToList();
                    break;
                case "year":
                    options = _tagsService.All().Where(i => i.TagType == TagType.Year).ToList();
                    break;
                case "area":
                default:
                    options = _tagsService.All().Where(i => i.TagType == TagType.Area).ToList();
                    break;
            }

            return View(options);
        }

        [CMSAuthorize("UseFrontEnd")]
        public ActionResult YourFollowedTopics()
            {
            var tags = new List<YourTopics> { };

            var _brand = Brand.itok;

            int noofdays = -14;           

            if (LoginManager.CurrentUser.favouriteTags != null &&
                LoginManager.CurrentUser.favouriteTags.Any(i => i.brand == _brand))
                {
                tags.AddRange(LoginManager.CurrentUser.favouriteTags.Where(i => i.brand == _brand).Select(favouritetags => new YourTopics()
                {
                    UrlSlug = favouritetags.tag.UrlSlug,
                    DisplayText = favouritetags.tag.DisplayText,
                    UnreadCount = TypeFiltersCount(FeedType.Missing, favouritetags.tag.UrlSlug, noofdays, false),
                    Forced = false
                    
                }));
                }



            var model = new YourTopicViewModel() { Topics = tags, Brand = _brand };

            return PartialView("_YourFollowedTopics", model);
            }


        public ActionResult MenuTopics()
        {
            var menuItems = _tagsService.FindMenuItems(Brand.itok);

            var uniqueIds = menuItems.Select(i => i.Id).ToList(); //list of all ids

            //parents will not have a parent id within the list
            var parents = menuItems.Where(i => !uniqueIds.Contains(i.ParentId)).ToList();

            var model = new MenuTopicsModel
            {
                EventTags = _tagsService.All().Where(i => i.TagType == TagType.Events).ToList(),
                AreaTags = _tagsService.All().Where(i => i.TagType == TagType.Area).ToList()
            };

            return PartialView("_MenuItems", model);
        }


        public ActionResult ChangeWasEvent()
        {
            //event 2, Area = 0
            var eventTags = _tagsService.All().Where(i => i.TagType == TagType.Events).OrderBy(i => i.DisplayText).ToList();         

            return PartialView("_ChangeWasEvent", eventTags);
        }

        public ActionResult AreaYearTags(string filter = "")
        {
            //event 2, Area = 0
            var eventTags = new List<ITOK.Core.Data.Model.Tags>();
            var filterTags = filter.Split('+');
            List<ITOK.Core.Data.Model.Tags> filters = new List<ITOK.Core.Data.Model.Tags>();
            //grab all the filters
            foreach(var tag in filterTags)
            {
                filters.Add(_tagsService.FindByUrlSlug(tag, Brand.itok));
            }
            //do we have any event tags in use
            if (filters.Any(i => i.TagType == TagType.Events)) //if we have any event
            {
                //if we do, get the relevant years for the selected events
                foreach (var eventTag in filters.Where(i => i.TagType == TagType.Events))
                {
                    eventTags.AddRange(_tagsService.All().Where(i => i.TagType == TagType.Year && i.ParentId == eventTag.Id).ToList());
                }        
            }
            else
            {
                eventTags = _tagsService.All().Where(i => i.TagType != TagType.Events).OrderBy(i => i.DisplayText).ToList();
            }
            eventTags.AddRange(_tagsService.All().Where(i => i.TagType == TagType.Area).ToList());
            return PartialView("_AreaYearTags", eventTags);
        }

        #region users


        [CMSAuthorize("UseFrontEnd")]
        public ActionResult FavouriteTags(string id)
            {
            //var model = new UserSetupModel { jobRoles = _tagsService.All().Where(i => i.ParentId == id && i.TagType!=1 && i.TagType!=0).ToList() };
            var model = new FavouriteTagsModel { jobRoles = _areasService.All().Where(i => i.ParentId == id).ToList() };// && i.brand == GlobalVariables.CurrentBrand
            return PartialView("FavouriteTagsList", model);
            }

        #endregion users

        #region Logout
        [CMSAuthorize("UseFrontEnd")]
        public ActionResult SignOut()
            {
            Request.Headers.Remove("Cache-Control");
            System.Web.Security.FormsAuthentication.SignOut();
            HttpContext.Session.Abandon();

            return Redirect(string.Format(ConfigHelper.IMLogOutUrl,
                "Logout?realm=ITOK&goto=https%3A%2F%2F",
                Request.Url.DnsSafeHost));
            }

        #endregion Logout

        #region Fixed Pages

        [CMSAuthorize("UseFrontEnd")]
        public ActionResult ObserverProgramme()
        {
          
            return View();
        }

        [CMSAuthorize("UseFrontEnd")]
        public ActionResult BiddingCandidature()
        {

            return View();
        }
        [CMSAuthorize("UseFrontEnd")]
        public ActionResult EventPlanning()
        {

            return View();
        }
        #endregion
    }
    }