﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ITOKFrontend.Helpers;
using ITOKFrontend.Models;
using ITOKFrontend.Models.Constants;
using ITOK.Core.Common;
using ITOK.Core.Common.Constants;
using ITOK.Core.Common.Utilities;
using ITOK.Core.Common.Utils;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using ITOK.Core.Data.Model.HS;
using ITOK.Core.Services.Core;
using ITOK.Core.Services.Security;
using FavouriteTag = ITOK.Core.Data.Model.FavouriteTag;
using System;

namespace ITOKFrontend.Controllers
    {
    public class DataController : BaseController
        {
        private IMongoRepository1 mongoRepository;

        public DataController()
            {
            mongoRepository = AppKernel.GetInstance<IMongoRepository1>();
            Init();
            }

        public DataController(IMongoRepository1 _mongoRepository)
            {
            this.mongoRepository = _mongoRepository;

            Init();
            }

        public ITagsService _tagsService;
        public ITOK.Core.Services.CMS.UserService _authUserService;

        public void Init()
            {
            _searchService = new SearchService(mongoRepository);
            _articleService = new ArticleService(mongoRepository);
            _gotitService = new GotItCountService(mongoRepository);
            _statService = new StatsService(mongoRepository);
            _readingListService = new ReadingListService(mongoRepository);
            _articleCommentService = new ArticleCommentService(mongoRepository);
            _UserService = new UserService(mongoRepository);
            _tagsService = AppKernel.GetInstance<ITagsService>();
            _authUserService = new ITOK.Core.Services.CMS.UserService(mongoRepository);
            }

        public ArticleService _articleService;
        private StatsService _statService;
        public SearchService _searchService;

        private readonly ArticleHydrationSettings _hydrationSettings = new ArticleHydrationSettings
        {
            PrimaryMedia = new MediaHydrationSettings(),
            RelatedTags = new TagHydrationSettings()
        };

        private ArticleFilterModel _model;

        [HttpGet]
        [AcceptVerbs(HttpVerbs.Get)]
        [Route("{brand}/data/{type}/{filter?}/{daysAgo?}")]
        public JsonResult TypeFilters(Brand brand, FeedType type, string filter = "", int daysAgo = 14)
            {
            //type = Important (Group Articles not past their Action Date)
            //Missing - I have not read yet
            //Latest - date order

            //filter - the home page tags
            string[] tags = { };
            if (!string.IsNullOrEmpty(filter))
                {
                tags = filter.Split('+').ToArray();
                }

            var skip = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["skip"]))
                skip = int.Parse(Request.QueryString["skip"]);

            var take = 20; //CR: amended to match initial load
            if (!string.IsNullOrEmpty(Request.QueryString["take"]))
                take = int.Parse(Request.QueryString["take"]);

            var urlTraker = "";

            IList<Article> articleList = new List<Article>();

            switch (type)
                {
                case FeedType.Important:
                    var articleType = ArticleUtilities.ArticleTypeFromString("group");

                    //gets only articles of type group, with an action date in the future which are live and published
                    articleList = _articleService.GetActionableOrderedBy(brand,
                        SortOrder.LiveFrom, skip,
                        take,
                        tags, new List<string>(),
                        new[] { PublicationStatus.Published },
                        true,
                        false,
                        articleType,
                        _hydrationSettings, false);

                    urlTraker =
                        "?utm_source=home&utm_medium=important&utm_content=filter&utm_campaign=" + filter;

                    break;

                case FeedType.Missing:
                case FeedType.Role:

                    var statList =
                        _statService.GetViewedArticlesByUserId(LoginManager.CurrentUser.Id, null, -14).ToList();

                    articleList = _articleService.GetPagedInDateRangeOrderedBy(brand,
                        SortOrder.LiveFrom, -System.Math.Abs(daysAgo),
                        skip,
                        take,
                        tags, statList,
                        new[] { PublicationStatus.Published }, true, false, null, _hydrationSettings, false);

                    //combo!

                    urlTraker = "?utm_source=home&utm_medium=missed&utm_content=filter&utm_campaign=" + filter +
                                "&utm_term=" + daysAgo;

                    break;

                case FeedType.Latest:
                default:
                    //standard latest - date ranges
                    articleList = _articleService.GetPagedInDateRangeOrderedBy(brand,
                        SortOrder.LiveFrom,
                        -System.Math.Abs(daysAgo), skip,
                        take,
                        tags, new List<string>(),
                        new[] { PublicationStatus.Published }, true, false, null, _hydrationSettings, false);

                    urlTraker = "?utm_source=home&utm_medium=latest&utm_content=filter&utm_campaign=" + filter +
                                "&utm_term=" + daysAgo;

                    break;
                }

            var favoriteArticles =
                _articleService.GetFavoriteArticleIds(LoginManager.CurrentUser.Id,
                    new ITOK.Core.Data.Model.HS.ArticleHydrationSettings());

            foreach (var art in articleList)
                {
                art.IsFavoriteForCurrentUser = favoriteArticles.Contains(art.Id);
                }

            var itemMediaProfile = ITOK.Core.Common.Constants.MediaProfile.size_600x300;
            var profileConfig = ITOK.Core.Common.Config.ITOKConfig.Instance.MediaProfiles.GetProfileConfig(itemMediaProfile);

            var ReadArticle = _statService.GetViewedArticlesByUserId(LoginManager.CurrentUser.Id, null,
                null);

            var list = articleList.Select(x => new ArticleListJsonResult(x.Id,
                    DateTimeUtilities.DateFormat(x.LiveFrom),
                    DateTimeUtilities.TimeAgo(x.VersionDate.Value),
                    x.LiveFrom != x.VersionDate,
                    x.Title,
                   x.ArticleType == ArticleType.bulletin ? x.Body : x.StandFirst,
                    x.RelatedTags.ToList(),
                    ArticleUtilities.GetArticleUri(x) + urlTraker,
                    FormatMedia(x.PrimaryMedia),
                    DateTimeUtilities.DateFormat(x.VersionDate),
                    x.BrandArticleNumber, null,
                    x.ArticleType.ToString(),
                    x.ResultCard,

                    x.VersionDate > LoginManager.CurrentUser.LastLoggedOn ? (x.LiveFrom != x.VersionDate ? "updated-item" : "new-item") : !ReadArticle.Contains(x.Id) ? "unread-item" : "",
                    x.FavCount,
                    x.ReadingListCount,
                    x.IsFavoriteForCurrentUser,

                    MediaUtilities.GetMediaMarkup(itemMediaProfile, x.PrimaryMedia, brand, profileConfig.Height, profileConfig.Width, true,
                    false, true))).AsEnumerable(); //need skip for the prev/next on the article page

            var count = list.Count();

            var feedData = new { count = count, data = list };

            return Json(feedData, @"application/json", JsonRequestBehavior.AllowGet);
            }

        [HttpGet]
        [AcceptVerbs(HttpVerbs.Get)]
        [Route("{brand}/data/posts/{type}/{filter?}")]
        public JsonResult MoreData(Brand brand, ArticleFilterType type, string filter = "")
            {
            string[] filtertags = { };
            if (!string.IsNullOrEmpty(filter))
                {
                filtertags = filter.Split('+').ToArray();
                }

            var skip = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["skip"]))
                skip = int.Parse(Request.QueryString["skip"]);

            var take = 20; //CR: amended to match initial load
            if (!string.IsNullOrEmpty(Request.QueryString["take"]))
                take = int.Parse(Request.QueryString["take"]);

            IList<Article> articleList = new List<Article>();

            var FavoriteArticles =
               _articleService.GetFavoriteArticleIds(LoginManager.CurrentUser.Id,
                   new ITOK.Core.Data.Model.HS.ArticleHydrationSettings());

            var ReadArticle = _statService.GetViewedArticlesByUserId(LoginManager.CurrentUser.Id, null,
                null, brand);

            var itemMediaProfile = ITOK.Core.Common.Constants.MediaProfile.size_600x300;
            var profileConfig = ITOK.Core.Common.Config.ITOKConfig.Instance.MediaProfiles.GetProfileConfig(itemMediaProfile);

            switch (type)
                {
                case ArticleFilterType.None:
                    //        //var articleType = ArticleUtilities.ArticleTypeFromString(filter);

                    articleList = _articleService.GetPagedOrderedBy(brand,
                            SortOrder.LiveFrom, skip,
                            take,
                            filtertags,
                            new[] { PublicationStatus.Published }, true, false, null, _hydrationSettings);

                    break;

                case ArticleFilterType.Latest:
                    //        //var articleType = ArticleUtilities.ArticleTypeFromString(filter);

                    articleList = _articleService.GetPagedInDateRangeOrderedBy(brand,
                        SortOrder.LiveFrom,
                        -System.Math.Abs(14), skip,
                            take,
                        filtertags, new List<string>(),
                        new[] { PublicationStatus.Published }, true, false, null, _hydrationSettings, false);
                    break;

                case ArticleFilterType.Unread:
                    //        //var articleType = ArticleUtilities.ArticleTypeFromString(filter);

                    filtertags =
                        LoginManager.CurrentUser.favouriteTags.Where(i => i.brand == GlobalVariables.CurrentBrand)
                            .Select(x => x.tag.UrlSlug).ToArray();

                    articleList = _articleService.GetPagedUnreadOrderedBy(brand,
                        SortOrder.LiveFrom,
                         0,
                        20,
                        filtertags, ReadArticle.ToList(),
                        new[] { PublicationStatus.Published }, true, false, null, _hydrationSettings, false);

                    foreach (var art in articleList)
                        {
                        art.IsFavoriteForCurrentUser = FavoriteArticles.Contains(art.Id);
                        }

                    var listunread = articleList.Select(x => new ArticleListJsonResult(x.Id,
                        DateTimeUtilities.DateFormat(x.LiveFrom),
                        DateTimeUtilities.TimeAgo(x.VersionDate),
                        x.LiveFrom != x.VersionDate,
                        x.Title,
                        x.ArticleType == ArticleType.bulletin ? x.Body : x.StandFirst,
                        //tags = x.Tags.ToArray(),
                        x.RelatedTags.Where(z => z.TagType == TagType.Events).OrderBy(z => z.DisplayText).ToList(),
                        ArticleUtilities.GetArticleUri(x) + "?skip=" + skip + "&hub=" + filter,
                        FormatMedia(x.PrimaryMedia), DateTimeUtilities.DateFormat(x.VersionDate),
                        x.BrandArticleNumber, null,
                        x.ArticleType.ToString(),
                        x.ResultCard,
                       x.VersionDate > LoginManager.CurrentUser.LastLoggedOn ? (x.LiveFrom != x.VersionDate ? "updated-item" : "new-item") : "unread-item",
                        0, 0, false,
                        MediaUtilities.GetMediaMarkup(itemMediaProfile, x.PrimaryMedia, brand, profileConfig.Height, profileConfig.Width, true,
                        false, true),
                        (x.PrimaryMedia != null ? x.PrimaryMedia.RemoteItemCode : ""),
                        x.ActionDate > DateTime.Now ? true: false)).AsEnumerable();
                    //need skip for the prev/next on the article page

                    return Json(listunread, @"application/json", JsonRequestBehavior.AllowGet);

                    break;

                case ArticleFilterType.UserFav:

                    articleList = _articleService.GetFavoriteArticles(brand,
                        LoginManager.CurrentUser.Id,
                        _hydrationSettings, skip, take);

                    break;

                case ArticleFilterType.UserReading:

                    if (Request.QueryString["ReadingListId"] != null)
                        {
                        var readingList = Server.UrlEncode(Request.QueryString["ReadingListId"]);

                        articleList = _articleService.GetReadingArticlesByReadingList(brand,
                            readingList, _hydrationSettings, skip, take);
                        }

                    break;

                case ArticleFilterType.Popular:

                    articleList = new List<Article>();

                    break;

                default:

                    articleList = _articleService.GetPagedOrderedBy(brand,
                        SortOrder.LiveFrom, skip,
                        take,
                        filtertags,
                        new[] { PublicationStatus.Published }, true, false, null, _hydrationSettings);
                    break;
                }

            foreach (var art in articleList)
                {
                art.IsFavoriteForCurrentUser = FavoriteArticles.Contains(art.Id);
                }

            var list = articleList.Select(x => new ArticleListJsonResult(x.Id,
                DateTimeUtilities.DateFormat(x.LiveFrom),
                DateTimeUtilities.TimeAgo(x.VersionDate),
                x.LiveFrom != x.VersionDate,
                x.Title,
                x.ArticleType == ArticleType.bulletin ? x.Body : x.StandFirst,
                //tags = x.Tags.ToArray(),
                x.RelatedTags.Where(z => z.TagType == TagType.Events).OrderBy(z => z.DisplayText).ToList(),
                ArticleUtilities.GetArticleUri(x) + "?skip=" + skip + "&hub=" + filter,
                FormatMedia(x.PrimaryMedia), DateTimeUtilities.DateFormat(x.VersionDate),
                x.BrandArticleNumber, null,
                x.ArticleType.ToString(),
                x.ResultCard,
                x.VersionDate > LoginManager.CurrentUser.LastLoggedOn ? (x.LiveFrom != x.VersionDate ? "updated-item" : "new-item") : !ReadArticle.Contains(x.Id) ? "unread-item" : "",
                0, 0, false,
                MediaUtilities.GetMediaMarkup(itemMediaProfile, x.PrimaryMedia, brand, profileConfig.Height, profileConfig.Width, true,
                false, true),
                (x.PrimaryMedia != null ? x.PrimaryMedia.RemoteItemCode : ""),
                x.ActionDate > DateTime.Now ? true : false)).AsEnumerable();
            //need skip for the prev/next on the article page

            return Json(list, @"application/json", JsonRequestBehavior.AllowGet);
            }

        [HttpGet]
        [AcceptVerbs(HttpVerbs.Get)]
        [Route("{brand}/searchdata/{searchId}")]
        public JsonResult SearchArticles(Brand brand, string searchId)
            {
            //Brand brand = GlobalVariables.CurrentBrand;
            //string searchText = Request.QueryString["q"];
            //SearchFilter searchFilter = SearchFilter.Phrase;
            //DateTime? start = DateTime.Today.AddYears(-1);
            //DateTime? end = null;

            var search = _searchService.FindById(searchId);

            string[] tagsArray = { };

            if (!string.IsNullOrEmpty(search.tags))
                search.tags.Split(' ').ToArray();

            var skip = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["skip"]))
                skip = int.Parse(Request.QueryString["skip"]);

            var take = 20; //CR: amended to match initial load
            if (!string.IsNullOrEmpty(Request.QueryString["take"]))
                take = int.Parse(Request.QueryString["take"]);

            var results = _articleService.SearchArticles(brand, search.searchText, search.searchFilter, search.StartFilter, search.EndFilter, tagsArray, search.searchOrder,
                take, skip, _hydrationSettings);

            //var count = _articleService.SearchArticlesCount(brand, searchText, searchFilter, start, end, tags);

            //var model = new ArticleFilterModel
            //{
            //    Articles = results,
            //    TotalItemCount = count
            //};

            var urlTraker = "";
            urlTraker =
                 "?utm_source=home&utm_medium=important&utm_content=filter&utm_campaign=" + search.Id + "_" + skip + "_" + take;

            var itemMediaProfile = ITOK.Core.Common.Constants.MediaProfile.size_600x300;
            var profileConfig = ITOK.Core.Common.Config.ITOKConfig.Instance.MediaProfiles.GetProfileConfig(itemMediaProfile);

            var ReadArticle = _statService.GetViewedArticlesByUserId(LoginManager.CurrentUser.Id, null,
                null, brand);

            var list = results.Select(x => new ArticleListJsonResult(x.Id,
                DateTimeUtilities.DateFormat(x.LiveFrom),
                DateTimeUtilities.TimeAgo(x.VersionDate.Value),
                x.LiveFrom != x.VersionDate,
                x.Title,
               x.ArticleType == ArticleType.bulletin ? x.Body : x.StandFirst,
                x.RelatedTags.ToList(),
                ArticleUtilities.GetArticleUri(x) + urlTraker,
                FormatMedia(x.PrimaryMedia),
                DateTimeUtilities.DateFormat(x.VersionDate),
                x.BrandArticleNumber, null,
                x.ArticleType.ToString(),
                x.ResultCard,
                 x.VersionDate > LoginManager.CurrentUser.LastLoggedOn ? (x.LiveFrom != x.VersionDate ? "updated-item" : "new-item") : !ReadArticle.Contains(x.Id) ? "unread-item" : "",
                x.FavCount,
                x.ReadingListCount,
                x.IsFavoriteForCurrentUser,
                MediaUtilities.GetMediaMarkup(itemMediaProfile, x.PrimaryMedia, brand, profileConfig.Height, profileConfig.Width, true,
                false, true))).AsEnumerable();

            return Json(list, @"application/json", JsonRequestBehavior.AllowGet);
            }

        #region Tags

        /// <summary>
        /// Returns JSON of related tags, according to the current filter
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{brand}/data/gettags/{urlSlug}")]
        public ActionResult GetTags(string urlSlug)
            {
            var results = _tagsService.AllTagsThatStartWith(urlSlug).Where(i => i.TagType > TagType.Year && i.Brand == GlobalVariables.CurrentBrand);
            var tagsContainsWithoutStartWith = _tagsService.AllTagsThatContainsWithoutStartWith(urlSlug).Where(i => i.TagType > TagType.Year && i.Brand == GlobalVariables.CurrentBrand);
            //var brandArticleNumbers = _articleService.FindByBrandArticleNumberStartWith(urlSlug, new ArticleHydrationSettings(),20);
            //var brandArticleNumbersResult = brandArticleNumbers.ToList().Select(x => x.BrandArticleNumber).ToList();
            return Json(results.Union(tagsContainsWithoutStartWith).Select(x => new
            {
                label = x.DisplayText,
                x.UrlSlug
            }).ToList() /*.Union(brandArticleNumbersResult).ToList()*/, JsonRequestBehavior.AllowGet);
            }

        [HttpGet]
        [AcceptVerbs(HttpVerbs.Get)]
        [Route("data/profile/addtag/{tag}")]
        public ActionResult AddTag(string tag)
            {
            var user = _authUserService.FindById(LiveUser.Get().Id);
            var _authUser = LiveUser.Get();

            var selectedTag = _tagsService.FindByUrlSlug(tag, GlobalVariables.CurrentBrand);

            if (user.favouriteTags.Select(x => x.tag.UrlSlug).ToArray().Contains(tag))
                return Json("alreadyAdded", @"application/json", JsonRequestBehavior.AllowGet);

            _authUser.favouriteTags.Add(new FavouriteTag() { brand = Brand.itok, tag = selectedTag });
            user.favouriteTags.Add(new ITOK.Core.Data.Model.FavouriteTag()
            {
                tag = selectedTag,
                brand = Brand.itok
            });
            _authUserService.Save(user);

        // need to reset liveuser?

            return Json("success", @"application/json", JsonRequestBehavior.AllowGet);
            }

        [HttpGet]
        [AcceptVerbs(HttpVerbs.Get)]
        [Route("data/profile/removetag/{tag}")]
        public ActionResult Removetag(string tag)
            {
            var user = _authUserService.FindById(LiveUser.Get().Id);
            var brand = Brand.itok;
            var _authUser = LiveUser.Get();

            //var selectedTag = _tagsService.FindByUrlSlug(tag);

            if (!user.favouriteTags.Select(x => x.tag.UrlSlug).ToArray().Contains(tag))
                return Json("alreadyRemoved", @"application/json", JsonRequestBehavior.AllowGet);

            var selectedTag = _authUser.favouriteTags.FirstOrDefault(x => x.tag.UrlSlug == tag && x.brand == brand);//.Remove(new FavouriteTag() { Brand = RouteData.Values["brand"].ToString(), tag = selectedTag });

            _authUser.favouriteTags.Remove(selectedTag);

            var userSelectedTag = user.favouriteTags.FirstOrDefault(x => x.tag.UrlSlug == tag && x.brand == brand);

            user.favouriteTags.Remove(userSelectedTag);
            _authUserService.Save(user);

            LoginManager.CurrentUser = _authUser;

            return Json("success", @"application/json", JsonRequestBehavior.AllowGet);
            }

        [HttpGet]
        [AcceptVerbs(HttpVerbs.Get)]
        [Route("{brand}/data/yourtopics")]
        public ActionResult YourTopics()
            {
            var tags = new List<YourTopics> { };

            var _brand = GlobalVariables.CurrentBrand;

            int noofdays = -14;

            var statList =
                       _statService.GetViewedArticlesByUserId(LoginManager.CurrentUser.Id, null, noofdays).ToList();

            if (LoginManager.CurrentUser.favouriteTags != null &&
                LoginManager.CurrentUser.favouriteTags.Any(i => i.brand == _brand))
                {
                tags.AddRange(LoginManager.CurrentUser.favouriteTags.Where(i => i.brand == _brand).Select(favouritetags => new YourTopics()
                {
                    UrlSlug = favouritetags.tag.UrlSlug,
                    DisplayText = favouritetags.tag.DisplayText,
                    UnreadCount = TypeFiltersCount(FeedType.Missing, favouritetags.tag.UrlSlug, noofdays, false)
                }));
                }

            return Json(tags /*.Union(brandArticleNumbersResult).ToList()*/
                , JsonRequestBehavior.AllowGet);
            }

        #endregion Tags

        #region Reading Lists

        [HttpGet]
        [Route("data/readinglists")]
        public JsonResult ReadingLists()
            {
            var list =
               _readingListService.FindAccessibleAllowAddingArticleToLists(
                    LiveUser.Get().Id, GlobalVariables.CurrentBrand);

            return Json(list.Select(x => new { Text = x.Name, Value = x.Id }), @"application/json", JsonRequestBehavior.AllowGet);
            }

        #endregion Reading Lists

        public ActionResult VideoPlayer(string id)
            {
            if (string.IsNullOrEmpty(id))
                return new EmptyResult();
            else
                return PartialView("TestVideo", (object)id);
            }

        public ActionResult GalleryVideo(string id)
            {
            return PartialView("GalleryVideo", (object)id);
            }
        }
    }