﻿using ITOK.Core.Common;
using ITOK.Core.Data;
using ITOK.Core.Services.CMS;
using ITOKFrontend.Helpers;
using System.Web.Mvc;

namespace ITOKFrontend.Controllers
{
    public class UserController : Controller
    {
        private UserService _userService;// AppKernel.GetInstance<UserService>();
        private IMongoRepository1 mongoRepository;


        public UserController()
        {
            mongoRepository = AppKernel.GetInstance<IMongoRepository1>();
            _userService = new UserService(mongoRepository);
         
        }
        // GET: User
      
        //public ActionResult Login()
        //{
        //    return View();
        //}
        [AllowAnonymous]
        public ActionResult Login(string key)
        {
            
            if (!string.IsNullOrWhiteSpace(key) && _userService.PasswordResetTicketIsValid(key))
            {
                ViewBag.PasswordResetTicketKey = key.ToLower();
            }
            if (!string.IsNullOrWhiteSpace(key) && !_userService.PasswordResetTicketIsActive(key))
            {
                ViewBag.PasswordResetTicketHasTimedOut = true;
            }
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(string username, string password, bool? rememberMe)
        {

            if (MembershipHelper.LoginFrontEnd(username, password, rememberMe == true))
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ViewBag.ErrorMessage = "Invalid username and/or password. Please check the spelling and try again";
                return View();
            }
        }

        public ActionResult Logout()
        {
            MembershipHelper.Logout();
            return RedirectToAction("Login", "User");
        }

    }
}