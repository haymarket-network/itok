﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ITOKFrontend.Helpers;
using ITOKFrontend.Models;
using ITOKFrontend.Models.Constants;
using ITOK.Core.Common;
using ITOK.Core.Common.Constants;
using ITOK.Core.Common.Utilities;
using ITOK.Core.Common.Utils;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using ITOK.Core.Data.Model.HS;
using ITOK.Core.Services.Core;
using ITOK.Core.Services.Security;

namespace ITOKFrontend.Controllers
    {
    public class HistoryController : BaseController
        {
        private IMongoRepository1 mongoRepository;

        public HistoryController()
            {
            mongoRepository = AppKernel.GetInstance<IMongoRepository1>();
            Init();
            }

        public HistoryController(IMongoRepository1 _mongoRepository)
            {
            this.mongoRepository = _mongoRepository;
            Init();
            }

        public void Init()
            {
            _articleService = new ArticleService(mongoRepository);
            _gotitService = new GotItCountService(mongoRepository);
            _statService = new StatsService(mongoRepository);
            _readingListService = new ReadingListService(mongoRepository);
            }

        public ArticleService _articleService;
        public GotItCountService _gotitService;
        private StatsService _statService;
        public ReadingListService _readingListService;

        private readonly ArticleHydrationSettings _hydrationSettings = new ArticleHydrationSettings
        {
            PrimaryMedia = new MediaHydrationSettings()
        };

        private ArticleFilterModel _model;

        public int DefaultPageSize = 20;

        //  [DonutOutputCache(Location = OutputCacheLocation.Any, Duration = 60, VaryByParam = "type;filter;cols;pagesize")]
        public ActionResult ListContainer(int? pagesize = null)
            {
            var pageSize = pagesize ?? DefaultPageSize;

            _model = new ArticleFilterModel { };

            _model.Articles = _statService.GetViewedArticlesByUserId(GlobalVariables.CurrentBrand, LiveUser.Get().Id, 0, pageSize, new ArticleHydrationSettings()).ToArray();
            _model.TotalItemCount = _model.Articles.Count();

            _model.BaseUrl = "/" + GlobalVariables.CurrentBrand + "/history/data/posts/";

            return PartialView("_HistoryListContainer", _model);
            }

        public ActionResult ListContainerLite(int? pagesize = null)
        {
            var pageSize = pagesize ?? DefaultPageSize;

            _model = new ArticleFilterModel { };

            _model.Articles = _statService.GetUniqueViewedArticlesByUserId(GlobalVariables.CurrentBrand, LiveUser.Get().Id, 0, 20, new ArticleHydrationSettings()).Take(5).ToArray();
            _model.TotalItemCount = _model.Articles.Count();

            _model.BaseUrl = "/" + GlobalVariables.CurrentBrand + "/history/data/posts/";

            return PartialView("_HistoryListContainerLite", _model);
        }

        /// <summary>
        /// Returns the next and previous article compared to the current one, given current filters
        /// </summary>
        /// <param name="type"></param>
        /// <param name="value"></param>
        /// <param name="filter"></param>
        /// <param name="skip"></param>
        /// <param name="pagesize"></param>
        /// <param name="iconOnly"></param>
        /// <returns></returns>
        public ActionResult PreviousNext(ArticleFilterType type, string value, string filter = "", int skip = 0, int? pagesize = null, bool iconOnly = false)
            {
            var pageSize = pagesize ?? DefaultPageSize;

            string[] tags = { };

            if (!string.IsNullOrEmpty(filter))
                {
                tags = filter.Split('+').ToArray().OrderBy(x => x).ToArray();
                }

            ///TODO: Work out paging - when the article is item number 22
            /// will need to pass in page from listcontainer (and load more) and across to article detail

            var model = new ArticlePreviousNextModel
            {
                Filter = filter,
                iconOnly = iconOnly
            };
            List<Article> list;

            switch (type)
                {
                case ArticleFilterType.None:

                    list = _articleService.GetPagedOrderedBy(GlobalVariables.CurrentBrand, SortOrder.LiveFrom, 0, pageSize + skip,
                        tags,
                        new[] { PublicationStatus.Published }, true, false, null, new ArticleHydrationSettings
                        {
                            PrimaryMedia = new MediaHydrationSettings(),
                        }).ToList();

                    break;

                default:
                    list = _articleService.GetPagedOrderedBy(GlobalVariables.CurrentBrand, SortOrder.LiveFrom, 0, pageSize + skip,
                         tags,
                         new[] { PublicationStatus.Published }, true, false, ArticleType.article, new ArticleHydrationSettings
                         {
                             PrimaryMedia = new MediaHydrationSettings(),
                         }).ToList();

                    break;
                }
            var article = list.FirstOrDefault(i => i.Id == value);
            model.nextArticle = list.SkipWhile(x => x != article).Skip(1).FirstOrDefault();
            list.Reverse();
            model.previousArticle = list.SkipWhile(x => x != article).Skip(1).FirstOrDefault();
            ViewBag.skip = skip;
            return PartialView("PreviousNext", model);
            }

        [HttpGet]
        [AcceptVerbs(HttpVerbs.Get)]
        [Route("{brand}/history/data/posts/")]
        public JsonResult MoreData(string filter = "")
            {
            var skip = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["skip"]))
                skip = int.Parse(Request.QueryString["skip"]);

            var take = 20; //CR: amended to match initial load
            if (!string.IsNullOrEmpty(Request.QueryString["take"]))
                take = int.Parse(Request.QueryString["take"]);

            IList<Article> articleList = _statService.GetViewedArticlesByUserId(Brand.itok,
                LoginManager.CurrentUser.Id, skip, take, new ArticleHydrationSettings()).ToArray();

            var list = articleList.Select(x => new ArticleListJsonResult(x.Id,
                DateTimeUtilities.DateFormat(x.LiveFrom),
                DateTimeUtilities.TimeAgo(x.VersionDate),
                x.LiveFrom != x.VersionDate,
                x.Title,
                x.StandFirst,
                null,
                ArticleUtilities.GetArticleUri(x) + x.UrlSlug,
                null,
                DateTimeUtilities.DateFormat(x.VersionDate), x.BrandArticleNumber,
                Convert.ToInt32(GetHistoryGroup(x)).ToString(),
                x.ArticleType.ToString(), x.ResultCard)).AsEnumerable();
            //need skip for the prev/next on the article page

            return Json(list, @"application/json", JsonRequestBehavior.AllowGet);
            }

        private HistoryGroup GetHistoryGroup(Article article)
            {
            if (article.SeenTimeStamp.Date == DateTime.UtcNow.Date)
                {
                return HistoryGroup.Today;
                }
            if (article.SeenTimeStamp.Date == DateTime.UtcNow.Date.AddDays(-1))
                {
                return HistoryGroup.Yesterday;
                }
            if (article.SeenTimeStamp.Date >= DateTime.UtcNow.Date.AddDays(-7))
                {
                return HistoryGroup.LastWeek;
                }
            if (article.SeenTimeStamp.Date >= DateTime.UtcNow.Date.AddDays(-30))
                {
                return HistoryGroup.LastMonth;
                }
            if (article.SeenTimeStamp.Date < DateTime.UtcNow.Date.AddDays(-30))
                {
                return HistoryGroup.Older;
                }
            return HistoryGroup.Today;
            }
        }
    }