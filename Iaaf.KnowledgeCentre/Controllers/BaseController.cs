﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ITOKFrontend.Helpers;
using ITOKFrontend.Models;
using ITOKFrontend.Models.Constants;
using ITOK.Core.Common;
using ITOK.Core.Common.Config;
using ITOK.Core.Common.Constants;
using ITOK.Core.Common.Utilities;
using ITOK.Core.Data;
using ITOK.Core.Data.Model;
using ITOK.Core.Data.Model.HS;
using ITOK.Core.Services.Core;
using ITOK.Core.Services.Security;

namespace ITOKFrontend.Controllers
    {
    public class BaseController : Controller
        {
        private IMongoRepository1 mongoRepository;

        public BaseController()
            {
            mongoRepository = AppKernel.GetInstance<IMongoRepository1>();
            Init();
            }

        public BaseController(IMongoRepository1 _mongoRepository)
            {
            this.mongoRepository = _mongoRepository;

            Init();
            }

        public void Init()
            {
            _articleService = new ArticleService(mongoRepository);
            _gotitService = new GotItCountService(mongoRepository);
            _statService = new StatsService(mongoRepository);
            _readingListService = new ReadingListService(mongoRepository);
            _articleCommentService = new ArticleCommentService(mongoRepository);
            _UserService = new UserService(mongoRepository);
            }

        private ArticleService _articleService;
        public GotItCountService _gotitService;
        private StatsService _statService;
        public ReadingListService _readingListService;
        public ArticleCommentService _articleCommentService;
        public UserService _UserService;

        private readonly ArticleHydrationSettings _hydrationSettings = new ArticleHydrationSettings
        {
            PrimaryMedia = new MediaHydrationSettings()
        };

        private ArticleFilterModel _model;

        public bool IsNew(DateTime CreatedOn, string articleId)
            {
            DateTime lastLogin = new DateTime();
            if (lastLogin == DateTime.MinValue)
                {
                return !_statService.IsArticleViewed(articleId, LoginManager.CurrentUser.Id);
                }
            else
                return CreatedOn > LoginManager.CurrentUser.LastLoggedOn.AddDays(-3) && !_statService.IsArticleViewed(articleId, LoginManager.CurrentUser.Id);
            }

        protected static bool IsUpdated(DateTime VersionDate)
            {
            return VersionDate > LoginManager.CurrentUser.LastLoggedOn;
            }

        protected static MediaJson FormatMedia(Media media)
            {
            return media != null
                ? new MediaJson(MediaUtilities.GetMediaUri(MediaProfile.size_300x200, media, GlobalVariables.CurrentBrand),
                    MediaUtilities.GetMediaUri(MediaProfile.size_100x67, media, GlobalVariables.CurrentBrand),
                    media.SEOTitle)
                : new MediaJson(
                    MediaUtilities.GetDefaultMediaUri(
                        ITOKConfig.Instance.MediaProfiles.GetProfileConfig(MediaProfile.size_300x200)),
                    MediaUtilities.GetDefaultMediaUri(
                        ITOKConfig.Instance.MediaProfiles.GetProfileConfig(MediaProfile.size_100x67)), "");
            }

        public int TypeFiltersCount(FeedType type, string filter = "", int daysAgo = 14, bool allTags = true)
            {
            //type = Important (Group Articles not past their Action Date)
            //Missing - I have not read yet
            //Latest - date order

            //filter - the home page tags
            string[] tags = { };
            if (!string.IsNullOrEmpty(filter))
                {
                tags = filter.Split('+').ToArray();
                }

            switch (type)
                {
                case FeedType.Important:
                    var articleType = ArticleUtilities.ArticleTypeFromString("group");
                    var statListImportant =
                        _statService.GetViewedArticlesByUserId(LoginManager.CurrentUser.Id)
                            .ToList();
                    //gets only articles of type group, with an action date in the future which are live and published
                    return _articleService.GetImportantCount(GlobalVariables.CurrentBrand, tags, statListImportant,
                        new[] { PublicationStatus.Published }, true, articleType, allTags);

                case FeedType.Missing:
                case FeedType.Role:

                    var statList =
                        _statService.GetViewedArticlesByUserId(LoginManager.CurrentUser.Id, null,
                            -14).ToList();

                    return _articleService.GetInDateRangeCount(GlobalVariables.CurrentBrand,
                        -System.Math.Abs(daysAgo), tags, statList, new[] { PublicationStatus.Published }, true, null,
                        allTags);
                //get last 100 - not sure the

                default:
                    //standard latest - date ranges
                    var list = new List<string>();

                    return _articleService.GetInDateRangeCount(GlobalVariables.CurrentBrand,
                        -System.Math.Abs(daysAgo), tags, list, new[] { PublicationStatus.Published }, true, null,
                        allTags);
                    break;
                }
            }
        }
    }