﻿using System;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ITOK.Core.Common.Utilities;
using ITOK.Core.Common.Utils;

namespace ITOKFrontend.Controllers
    {
    public class DownloadController : BaseController
        {
        private MemoryStream _memoryStream;

        // GET: Download
        [HttpGet]
        public ActionResult Download(string filename, string urlslug)
            {
            try
                {
                var filePath = MediaUtilities.GetDocumentUri(filename);
                var extension = Path.GetExtension(filename);

                var downloadfilename = string.IsNullOrWhiteSpace(filename)
                    ? string.Format("ITOK_File.{0}", extension)
                    : string.Format("{0}{1}", urlslug, extension);

                return new FileStreamResult(GetStreamFromUrl(filePath),
                    FileUtilities.ContentTypeFromFormat(FileUtilities.FormatFromFilename(filename)))
                {
                    FileDownloadName = downloadfilename
                };
                }
            catch (Exception)
                {
                throw new HttpException(404, "Not found");
                }
            }

        #region Private Methods

        private MemoryStream GetStreamFromUrl(string url)
            {
            // Check to see if the MemoryStream has already been created,
            // if not, then create memory stream
            if (_memoryStream != null) return _memoryStream;
            var client = new WebClient();
            try
                {
                _memoryStream = new MemoryStream(client.DownloadData(url));
                }
            catch (Exception ex)
                {
                throw (ex);
                }
            finally
                {
                client.Dispose();
                }
            return _memoryStream;
            }

        #endregion Private Methods
        }
    }