﻿using System.Linq;
using System.Web.Mvc;
using ITOKFrontend.Helpers;
using ITOKFrontend.Helpers.Authorise;
using ITOK.Core.Common;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data;
using ITOK.Core.Data.Model.HS;
using ITOK.Core.Services.Core;
using ITOK.Core.Services.Security;

namespace ITOKFrontend.Controllers
{
    public class ReadingListController : BaseController
    {
        private IMongoRepository1 mongoRepository;

        public ReadingListController()
        {
            mongoRepository = AppKernel.GetInstance<IMongoRepository1>();

            _jobRoleService = AppKernel.GetInstance<ITagsService>();
            _authUserService = new UserService(mongoRepository);
            _articleService = new ArticleService(mongoRepository);
            _readingListService = new ReadingListService(mongoRepository);
            _statService = new StatsService(mongoRepository);
            _tagsService = AppKernel.GetInstance<ITagsService>();
        }

        public ReadingListController(IMongoRepository1 _mongoRepository)
        {
            this.mongoRepository = _mongoRepository;
            _jobRoleService = AppKernel.GetInstance<ITagsService>();
            _authUserService = new UserService(mongoRepository);
            _statService = new StatsService(mongoRepository);
        }

        public readonly ITagsService _tagsService;
        public readonly ITagsService _jobRoleService;
        public readonly UserService _authUserService;
        public readonly ArticleService _articleService;
        public readonly StatsService _statService;

        // GET: Profile
        [CustomAuthorise]
        public ActionResult Index()
        {
            if (!LiveUser.Get().AcceptedRegulations)
                return RedirectToAction("AcceptRegulations", "Home");

            var brand = Brand.itok;// (Brand)Enum.Parse(typeof(Brand), RouteData.Values["brand"].ToString(), true);

            if (GlobalVariables.CurrentBrand != brand)
            {
                return Redirect(string.Format("/home/changebrand/{0}?redirect={1}", brand, Request.Url));
            }

            var readingLists = _readingListService.FindAccessibleReadingLists(null,
                LiveUser.Get().Id,
                GlobalVariables.CurrentBrand,
                new ReadingListHydrationSettings() { User = new UserHydrationSettings() });

            return View(readingLists);
        }

        [HttpGet]
        [CustomAuthorise]
        public ActionResult List(string id)
        {
            if (!LiveUser.Get().AcceptedRegulations)
                return RedirectToAction("AcceptRegulations", "Home");
            var readingLists = _readingListService.FindAccessibleReadingLists(null,
                LiveUser.Get().Id,
                GlobalVariables.CurrentBrand,
                new ReadingListHydrationSettings() { User = new UserHydrationSettings() });

            var selectedReadingList = readingLists.FirstOrDefault(x => x.Id == id);

            if (selectedReadingList == null)
            {
                return RedirectToAction("Index");
            }

            ViewBag.SelectedReadingList = selectedReadingList.Id;
            ViewBag.ReadingLists = readingLists;

            return View(selectedReadingList);
        }

        [HttpGet]
        [CustomAuthorise]
        public ActionResult New(string id = null)
        {
            if (!LiveUser.Get().AcceptedRegulations)
                return RedirectToAction("AcceptRegulations", "Home");

            ViewBag.Name = string.Empty;
            ViewBag.IsPublic = false;
            ViewBag.ReadingListId = string.Empty;
            if (string.IsNullOrEmpty(id)) return View();

            var readingList = _readingListService.FindReadingListById(id);
            if (readingList == null) return View();

            if (readingList.UserId != LiveUser.Get().Id)
                return View("Unauthorised");

            ViewBag.Name = readingList.Name;
            ViewBag.IsPublic = readingList.IsPublic;
            ViewBag.ReadingListId = id;
            return View();
        }

        [HttpPost]
        [CustomAuthorise]
        public ActionResult New(string Name, bool IsPublic, string ReadingListId = null)
        {
            ViewBag.ErrorMessage = string.Empty;

            if (!IsFormValid(Name))
                return View();
            if (string.IsNullOrEmpty(ReadingListId))
            {
                var inserted = _readingListService.Insert(Name, IsPublic, LiveUser.Get().Id,
                    null, null, GlobalVariables.CurrentBrand);
                if (!inserted)
                    ViewBag.ErrorMessage = "You already have a Reading List with that name";
                else
                    return RedirectToAction("Index");
            }
            else
            {
                var inserted = _readingListService.Update(ReadingListId, Name, IsPublic,
                    LiveUser.Get().Id, null,
                    null, GlobalVariables.CurrentBrand);
                if (!inserted)
                    ViewBag.ErrorMessage = "You already have a Reading List with that name";
                else
                    return RedirectToAction("Index");
            }
            return View();
        }

        [CustomAuthorise]
        public ActionResult Delete(string id)
        {
            var readinglist = _readingListService.FindReadingListById(id);

            if (readinglist.UserId != LiveUser.Get().Id)
                return Json(false, JsonRequestBehavior.AllowGet);
            _readingListService.Delete(id);

            return RedirectToAction("Index");
        }

        [CustomAuthorise]
        public ActionResult RemoveArticleFromReadingList(Brand brand, string articleId, string readingListId)
        {
            //_readingListService.(id);
            var article = _articleService.FindById(brand, articleId, new ArticleHydrationSettings());
            var readingList = _readingListService.FindReadingListById(readingListId);
            if (readingList.UserId == LiveUser.Get().Id)
                _statService.RemoveReading(article.Id, LiveUser.Get().Id, readingListId);
            else
                return Json(false, JsonRequestBehavior.AllowGet);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public bool IsFormValid(string Name)
        {
            if (string.IsNullOrEmpty(Name))
            {
                ViewBag.ErrorMessage = "Enter Name";
                return false;
            }
            if (Name.Length >= 100)
            {
                ViewBag.ErrorMessage = "Name must have less than 100 characters";
                return false;
            }
            if (Name.Length > 2) return true;
            ViewBag.ErrorMessage = "Name must have more than 2 characters";
            return false;
        }
    }
}