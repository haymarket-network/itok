﻿using System;
using System.Linq;
using System.Web.Mvc;
using ITOKFrontend.Helpers;
using ITOKFrontend.Helpers.Authorise;
using ITOKFrontend.Models;
using ITOK.Core.Common;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data;
using ITOK.Core.Services.Core;
using ITOK.Core.Services.Security;

namespace ITOKFrontend.Controllers
    {
    public class ProfileController : BaseController
        {
        private IMongoRepository1 mongoRepository;

        public ProfileController()
            {
            mongoRepository = AppKernel.GetInstance<IMongoRepository1>();

            _jobRoleService = AppKernel.GetInstance<ITagsService>();
            _authUserService = new ITOK.Core.Services.CMS.UserService(mongoRepository);
            _articleService = new ArticleService(mongoRepository);
            _readingListService = new ReadingListService(mongoRepository);
            _statService = new StatsService(mongoRepository);
            _tagsService = AppKernel.GetInstance<ITagsService>();
        }

        public ProfileController(IMongoRepository1 _mongoRepository)
            {
            this.mongoRepository = _mongoRepository;
            _jobRoleService = AppKernel.GetInstance<ITagsService>();
            _authUserService = new ITOK.Core.Services.CMS.UserService(mongoRepository);
            _statService = new StatsService(mongoRepository);
            }

        private readonly ITagsService _tagsService;
        private readonly ITagsService _jobRoleService;
        private readonly ITOK.Core.Services.CMS.UserService _authUserService;
        private readonly ArticleService _articleService;
        private readonly ReadingListService _readingListService;
        private readonly StatsService _statService;

        // GET: Profile
        [CustomAuthorise]
        public ActionResult Index()
            {
            if (!LiveUser.Get().AcceptedRegulations)
                return RedirectToAction("AcceptRegulations", "Home");

            var brand = Brand.itok;

            if (GlobalVariables.CurrentBrand != brand)
                {
                return Redirect(string.Format("/home/changebrand/{0}?redirect={1}", brand, Request.Url));
                }

            var model = new ProfileEditModel
            {
               
            };

          
            return View(model);
            }

        [HttpPost]
        [CustomAuthorise]
        public ActionResult Index(string PrimaryJobRole, string[] secondaryJobRoles, string SelectedSecondaryJobRoleId, string[] favouriteTags)
            {
            var user = _authUserService.FindById(LoginManager.CurrentUser.Id);
            var _authUser = LoginManager.CurrentUser;



            var selectedfavouriteTags = _tagsService.FindByIds(favouriteTags);


            var secondaryRole = _jobRoleService
                        .All().FirstOrDefault(i => i.Id == SelectedSecondaryJobRoleId);

         

            //new array of roles must be updated. Change Brand will select from here
      
            _authUserService.Save(user);

            //reset the current user's roles
       
            LoginManager.CurrentUser = _authUser;

            var model = new ProfileEditModel
            {
                      favouriteTags = _tagsService.All().ToList(),
            };
        
            ViewBag.Msg = "Your Profile was updated";
            return View(model);
            }

      

        [HttpGet]
        [CustomAuthorise]
        public ActionResult FavoriteArticles()
            {
            return View();
            }

        [HttpPost]
        public ActionResult DeleteTopic(string UrlSlug)
            {
            //var user = _authUserService.FindById(LoginManager.CurrentUser.Id);
            //var _authUser = LoginManager.CurrentUser;

            //user.favouriteTags.Remove(user.favouriteTags.First(i => i.tag.UrlSlug == UrlSlug));
            //_authUserService.Save(user);
            //_authUser.favouriteTags.Remove(_authUser.favouriteTags.First(i => i.tag.UrlSlug == UrlSlug));
            //LoginManager.CurrentUser = _authUser;

            // Return HTML to update the view
            return PartialView("FavouriteTopics");
            }

        #region History

        [CustomAuthorise]
        public ActionResult ViewedArticleHistory(string id)
            {
            var brand = Brand.itok;

            if (GlobalVariables.CurrentBrand != brand)
                {
                return Redirect(string.Format("/home/changebrand/{0}?redirect={1}", brand, Request.Url));
                }

            return View();
            }

        #endregion History
        }
    }