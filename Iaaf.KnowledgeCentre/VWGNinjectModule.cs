﻿using Ninject.Modules;
using ITOK.Core.Data;
using ITOK.Core.Common;
using ITOK.Core.Services.CMS;
using ITOK.Core.Services.Core.Wrapped;
using IQuestionService = ITOK.Core.Services.Core.IQuestionService;
using QuestionService = ITOK.Core.Services.Core.QuestionService;
using UserService = ITOK.Core.Services.CMS.UserService;
using ITOK.Core.Services.Core;

namespace ITOKFrontend
{
    public class ITOKNinjectModule : NinjectModule
    {
        public override void Load()
        {
            AppKernel.kernel.Settings.AllowNullInjection = true;
            AppKernel.kernel.Bind<IMongoRepository1>().To<MongoRepository>();
            AppKernel.kernel.Bind<IMongoOutputCacheRepository>().To<MongoOutputCacheRepository>();

            AppKernel.kernel.Bind<ITagsService>().To<TagsService>();
            AppKernel.kernel.Bind<ITagsServiceWrapped>().To<TagsServiceWrapped>();


            //AppKernel.kernel.Bind<IMongoCollection<ITOK.Core.Data.DTOs.Article>>()
            //.To < MongoCollectionBase<ITOK.Core.Data.DTOs.Article>>();
            
            AppKernel.kernel.Bind<IQuestionServiceWrapped>().To<QuestionServiceWrapped>();
            //AppKernel.kernel.Bind<IQuestionService>().To<QuestionService>();
            AppKernel.kernel.Bind<IQuestionService>().To<QuestionService>();
            AppKernel.kernel.Bind<IAnswerFilterService>().To<AnswerFilterService>();
            AppKernel.kernel.Bind<IUserService>().To<UserService>();
            AppKernel.kernel.Bind<IAnswerService>().To<AnswerService>();
 
            
            AppKernel.kernel.Bind<IQuestionTemplateService>().To<QuestionTemplateService>();
            
        }
    }
}
