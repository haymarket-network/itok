﻿using ITOK.Core.Data;
using System;
using System.Linq;
using ITOK.Core.Common;
using ITOK.Core.Common.Constants;
using ITOK.Core.Common.Utilities;
using ITOK.Core.Services.CMS;

namespace ITOKFrontend
{
    public partial class _404 : System.Web.UI.Page
    {
        private IMongoRepository1 mongoRepository;
        protected void Page_Load(object sender, EventArgs e)
        {
            var brand = Brand.itok; ///ToDo: Change to be picked up by URL

            string url = Request.RawUrl;

            string[] splitUrl = url.Split('/');
            string urlSlug = splitUrl[splitUrl.Length - 1];

            if (urlSlug.Length > 45)
            {
                urlSlug = urlSlug.Substring(0, 45);

            }

            mongoRepository = AppKernel.GetInstance<IMongoRepository1>();
            ArticleService _articleService = new ArticleService(mongoRepository);
            var newArticle = _articleService.FindAllBySlug(brand,urlSlug, new ITOK.Core.Data.Model.HS.ArticleHydrationSettings());
            if (newArticle.Count() == 1)
            {
                //woo, single match - let's go
                
                Response.RedirectPermanent(ArticleUtilities.GetArticleUri(newArticle.FirstOrDefault()));
            }
            else
            {
                //darn, what else have we got to check against!
                //Brand brand = splitUrl[0];
                var articleToRedirect = newArticle.Where(i => i.brand == brand).ToList();
                if (articleToRedirect.Count() == 1)
                {
                    //woo, that wasn't so bad
                Response.RedirectPermanent(ArticleUtilities.GetArticleUri(articleToRedirect.FirstOrDefault()));

                }
            }






        }
    }
}