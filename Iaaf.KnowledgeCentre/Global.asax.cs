﻿using System;
using System.Globalization;
using System.Threading;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using ITOK.Core.Services.Helpers;
using ITOKFrontend.Helpers;
using ITOK.Core.Services.Security;

namespace ITOKFrontend
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            ITOKNinjectModule module = new ITOKNinjectModule();
            module.Load();


            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            EntityMapper.InitialiseMappings();

            var membershipProvider = new MembershipProvider();
            LiveUser.SetMembershipProvider(membershipProvider);
            CMSAuthorizeAttribute.SetMembershipProvider(membershipProvider);
 
        }

        protected void Application_BeginRequest(Object sender, EventArgs e)
        {
            var newCulture = (CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            newCulture.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
            newCulture.DateTimeFormat.DateSeparator = "/";
            Thread.CurrentThread.CurrentCulture = newCulture;
        }


    }
}
