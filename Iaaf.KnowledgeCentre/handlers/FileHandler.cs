﻿using System;
using System.Web;
using ITOK.Core.Common;
using ITOK.Core.Data;
using ITOK.Core.Services.CMS;
using ITOK.Core.Common.Constants;
using ITOK.Core.Common.Config;
using System.Net;
using System.IO;

namespace ITOKFrontend.handlers
{
    public class FileHandler : IHttpHandler
    {
        private IMongoRepository1 mongoRepository;

        /// <summary>
        /// You will need to configure this handler in the Web.config file of your 
        /// web and register it with IIS before being able to use it. For more information
        /// see the following link: http://go.microsoft.com/?linkid=8101007
        /// </summary>
        #region IHttpHandler Members

        public bool IsReusable
        {
            // Return false in case your Managed Handler cannot be reused for another request.
            // Usually this would be false in case you have some state information preserved per request.
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            mongoRepository = AppKernel.GetInstance<IMongoRepository1>();

            MediaService _MediaService = new MediaService(mongoRepository);
            var documentLocation = ITOKConfig.Instance.Document.Directory;
            var imageLocation = ITOKConfig.Instance.Media.Directory+ "/Original";
            var root = ITOKConfig.Instance.Media.baseUri;
            var contentType = "application/pdf";
            var media = _MediaService.FindByIds(new ITOK.Core.Data.Model.HS.MediaHydrationSettings(), new string[] { context.Request.QueryString["fid"] });
            if (media != null && media.Count > 0)
            {
                string path = String.Format("{0}/{2}/{1}", root, media[0].FileName, documentLocation);
             //   string filePath = Path.Combine(@"d:\wwwroot\portal_daten", context.Request.QueryString["fid"]);

                switch(media[0].Format)
                {
                    case FileFormat.JPEG:
                        contentType = "image/jpeg";
                        path= String.Format("{0}/{2}/{1}", root, media[0].FileName, imageLocation);
                        break;
                    case FileFormat.PNG:
                        contentType = "image/png";
                        path = String.Format("{0}/{2}/{1}", root, media[0].FileName, imageLocation);
                        break;
                    case FileFormat.PDF:
                        contentType = "application/pdf";
                        break;
                    case FileFormat.Excel:
                        contentType = "application/vnd.ms-excel";
                        break;
                    case FileFormat.Word:
                        contentType = "application/msword";
                        break;
                    case FileFormat.Text:
                        contentType = "text/html";
                        break;
                    case FileFormat.PPT:
                        contentType = "application/vnd.ms-powerpoint";
                        break;
                    case FileFormat.PPTX:
                        contentType = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
                        break;
                }
                if (!string.IsNullOrEmpty(context.Request.QueryString["dl"]))
                {

                    if (URLExists(path))
                    {
                        try {
                            Stream stream = null;
                            int bytesToRead = 10000;
                            byte[] buffer = new Byte[bytesToRead];
                            HttpWebRequest fileReq = (HttpWebRequest)HttpWebRequest.Create(path);
                            HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();
                            if (fileReq.ContentLength > 0)
                                fileResp.ContentLength = fileReq.ContentLength;
                            stream = fileResp.GetResponseStream();

                            // prepare the response to the client. resp is the client Response
                            var resp = HttpContext.Current.Response;

                            //Indicate the type of data being sent
                            resp.ContentType = contentType;

                            var fileExtension = System.IO.Path.GetExtension(media[0].FileName);
                            //Name the file 
                            resp.AddHeader("Content-Disposition", "attachment; filename=\"" + media[0].Title + fileExtension + "\"");
                            resp.AddHeader("Content-Length", fileResp.ContentLength.ToString());

                            int length;
                            do
                            {
                                // Verify that the client is connected.
                                if (resp.IsClientConnected)
                                {
                                    // Read data into the buffer.
                                    length = stream.Read(buffer, 0, bytesToRead);
                                    // and write it out to the response's output stream
                                    resp.OutputStream.Write(buffer, 0, length);
                                    // Flush the data
                                    resp.Flush();
                                    //Clear the buffer
                                    buffer = new Byte[bytesToRead];
                                }
                                else
                                {
                                    // cancel the download if client has disconnected
                                    length = -1;
                                }
                            } while (length > 0);
                        }
                        catch (Exception ex)
                        {
                            context.Response.ContentType = "text/plain";
                            context.Response.Write(ex.Message);
                        }
                    }
                    else
                    {
                        throw new Exception("File not found");

                    }
                }
                else //return the document
                {
                    context.Response.Redirect(path);
                }
                
                

                //xlsx: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
            }
            //write your handler implementation here.
        }
        public bool URLExists(string url)
        {
            bool result = false;

            WebRequest webRequest = WebRequest.Create(url);
            webRequest.Timeout = 1200; // miliseconds
            webRequest.Method = "HEAD";

            HttpWebResponse response = null;

            try
            {
                response = (HttpWebResponse)webRequest.GetResponse();
                result = true;
            }
            catch (WebException webException)
            {
               
            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                }
            }

            return result;
        }
        #endregion
    }
}
