﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="404.aspx.cs" Inherits="ITOKFrontend._404" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        404 Error Page
    </title>

    <link href="~/Assets/Styles/bootstrap/bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="~/Assets/Styles/bootstrap/bootstrap-3.3.6-dist/css/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="~/Assets/Styles/font-awesome/font-awesome-4.5.0/css/font-awesome.min.css?v=1234" rel="stylesheet" />

       <%
           var CssLink = "<link href=\"~/Assets/Fonts/webfonts/fonts.css\" rel=\"stylesheet\" />";%>
    <%= CssLink %>

    <link href="~/Assets/Styles/custom/custom.css" rel="stylesheet" />
    <link href="~/Assets/Styles/custom/temp.css" rel="stylesheet" />

</head>
<body class="itok">

    <div class="col-md-12">
        <div class="col-md-6 col-md-offset-3">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="/" class="logo-box">
                        &nbsp;
                    </a>
                    <h1 class="panel-title offset-top-20">404 Page Not Found</h1>
                </div>
                <div class="panel-body offset-top-20">
                    <p>
                        The page you were looking for appears to have been moved, deleted or does not exist. You could go back to where you were or head straight to our home page.
                    </p>

                    <a href="/" class="btn btn-primary offset-top-20">Back to Homepage</a>
                </div>



            </div>

        </div>
    </div>

    


</body>
</html>
