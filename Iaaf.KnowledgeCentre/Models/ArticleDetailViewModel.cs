﻿using System.Collections.Generic;

namespace ITOKFrontend.Models
{
  public  class ArticleDetailViewModel
    {
        public ITOK.Core.Data.Model.Article Article { get; set; }

        public IList<ITOK.Core.Data.Model.Article> RelatedArticles { get; set; }

        public string ArticleUrl { get; set; }

        public string AuthorName { get; set; }

        public ITOK.Core.Data.Model.Tags AreaTag { get; set; }
    }
}
