﻿using System.Collections.Generic;
using MongoDB.Bson;
using ITOK.Core.Data.Model;

namespace ITOKFrontend.Models
{
   public class TagHubViewModel
    {
       public List<BsonDocument> RelatedTags { get; set; }

        public ITOK.Core.ViewModels.RelatedTagsModel RelatedTagsWithNames { get; set; }
        public List<Tags> Tags { get; set; }

        public Tags AreaTag { get; set; }
        public string[] TagNames { get; set; }
       public string UrlSlug { get; set; }
       public string TagDescription { get; set; }
       public int ImportantCount
           { get; set; }

       public int Missed1Count { get; set; }
       public int Missed7Count { get; set; }
       public int Missed14Count { get; set; }

       public int Missed2Count { get; set; }

       public int LatestCount { get; set; }

       public int Count { get; set; }
       public int Latest1Count { get; set; }
       public int Latest2Count { get; set; }
       public int Latest7Count { get; set; }
       public int Latest14Count { get; set; }

       public int Role1Count { get; set; }
       public int Role2Count { get; set; }
       public int Role7Count { get; set; }
       public int Role14Count { get; set; }

        public List<Tags> EventTags;
        public List<Tags> YearTags;
        public List<Tags> AreaTags;
        public List<Tags> CategoryTags;

        public TagHubViewModel()
        {
            EventTags = new List<Tags>();
            YearTags = new List<Tags>();
            AreaTags = new List<Tags>();
            CategoryTags = new List<Tags>();
        }
    }
}
