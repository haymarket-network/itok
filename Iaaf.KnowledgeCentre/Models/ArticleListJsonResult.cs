﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data.Model;

namespace ITOKFrontend.Models
    {
    [DataContract]
    public class ArticleListJsonResult
        {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string LiveFrom { get; set; }

        [DataMember]
        public string DateTimeAgo { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string StandFirst { get; set; }

        [DataMember]
        public string ArticleType { get; set; }

        [DataMember]
        public string ArticleTypeUrlslug { get; set; }

        [DataMember]
        public ResultCard ResultCard { get; set; }

        [DataMember]
        public MediaJson PrimaryMedia { get; set; }

        [DataMember]
        public string Articleurl { get; set; }

        //[DataMember]
        //public bool IsNew { get; set; }

        //[DataMember]
        //public bool IsUpdated { get; set; }

        //[DataMember]
        //public bool IsUnread { get; set; }

        [DataMember]
        public string VersionDate { get; set; }

        [DataMember]
        public string BrandArticleNumber { get; set; }

        [DataMember]
        public string HistoryGroup { get; set; }

        public List<Tags> Tags { get; set; }

        [DataMember]
        public int ReadingListCount { get; set; }

        [DataMember]
        public int FavCount { get; set; }

        [DataMember]
        public bool IsFavoriteForCurrentUser { get; set; }
          [DataMember]
        public string MediaMarkup { get; set; }
          [DataMember]
        public string BrightcoveID { get; set; }
          [DataMember]
        public bool ShowUpdatedText { get; set; }
        
          [DataMember]
        public string ViewType { get; set; }

        public bool ActionRequired { get; set; }



        public ArticleListJsonResult(string id, string livefrom, string title, string standfirst, string articleType,
            string articleTypeUrlslug, MediaJson primarymedia,
            string articleurl, int favCount = 0, int readingListCount = 0)
            {
            Id = id;
            LiveFrom = livefrom;
            Title = title;
            StandFirst = standfirst;
            ArticleType = articleType;
            ArticleTypeUrlslug = articleTypeUrlslug;

            PrimaryMedia = primarymedia;
            Articleurl = articleurl;
            FavCount = favCount;
            ReadingListCount = readingListCount;
            }

        public ArticleListJsonResult(string id, string livefrom, string title, string standfirst, List<Tags> tags,
            string articleurl, int favCount = 0, int readingListCount = 0)
            {
            Id = id;
            LiveFrom = livefrom;
            Title = title;
            StandFirst = standfirst;
            Tags = tags;
            Articleurl = articleurl;
            FavCount = favCount;
            ReadingListCount = readingListCount;
            }

        public ArticleListJsonResult(string id, string livefrom, string datetimeago, bool showupdatedtext,
            string title, string standfirst, List<Tags> tags, string articleurl,
            MediaJson primarymedia, string VersionDate, string BrandArticleNumber, string HistoryGroup,
            string articleType, ResultCard resultCard, string viewType = "", int favCount = 0,
            int readingListCount = 0,
            bool isFavoriteForCurrentUser = false, string mediaMarkup = "", string brightcoveId = "", bool actionrequired = false)
            {
            Id = id;
            LiveFrom = livefrom;
            DateTimeAgo = datetimeago;
            ShowUpdatedText = showupdatedtext;
            Title = title;
            StandFirst = standfirst;
            Tags = tags;
            Articleurl = articleurl;
            ResultCard = resultCard;
            PrimaryMedia = primarymedia;

            this.ViewType = viewType;
            this.BrandArticleNumber = BrandArticleNumber;
            this.VersionDate = VersionDate;
            this.HistoryGroup = HistoryGroup;
            ArticleType = articleType;
            FavCount = favCount;
            ReadingListCount = readingListCount;
            IsFavoriteForCurrentUser = isFavoriteForCurrentUser;
            MediaMarkup = mediaMarkup;
            BrightcoveID = brightcoveId;
            ActionRequired = actionrequired;
            }
        }
    }