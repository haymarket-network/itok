﻿using System.Collections.Generic;
using ITOKFrontend.Models.Constants;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data.Model;

namespace ITOKFrontend.Models
{

    public class ArticleDetailModel
    {
        public Article Article;
        public string CompetitionUrlSlug;

        //public IList<DisciplineAtEvent> DisciplineAtEvents = new List<DisciplineAtEvent>();
        //public MedalTableViewModel medalTable = new MedalTableViewModel();

        public string ArticleUrl;

        public string DisciplineTitle { get; set; }

        public string EventUrlSlug { get; set; }

        // public List<EventPhase> EventPhases { get; set; }

        public List<string> AvailablePhases { get; set; }

        public int PhaseId { get; set; }
        public Article NextArticle { get; set; }

        public Article PrevArticle { get; set; }
    }

    /// <summary>
    /// Used for plain index and filtered which are the same view
    /// </summary>
    public class ArticleFilterModel
    {
        public string ListHeading;
        public IList<Article> Articles = new List<Article>();
        public IList<string> FavoriteArticles = new List<string>();
        
        // public PaginationModel Pagination = new PaginationModel();
        public ArticleFilterType FilterType;

        // Filter type specific entities. TODO: Review the decision to combine all article listing into one controller/view.
        //public CompetitorFullModel Competitor;

        // public EventFullModel Event;
        //public DisciplineFullModel Discipline;
        // public DisciplineType DisciplineType;
        // public Competition Competition;
        public ArticleType? ArticleType = null;
        //public ArticleTypeGroup? ArticleTypeGroup = null;
        //public IList<EventFullModel> CompetitionEvents = new List<EventFullModel>();
        //public MedalTableViewModel medalTable = new MedalTableViewModel();

        public string Title { get; set; }

        public int TotalItemCount { get; set; }

        public string Filter { get; set; }

        public int? Cols { get; set; }
        public string BaseUrl { get; set; }

        public bool ShowTags { get; set; }
        public string UrlTraker { get; set; }
    }

    public class FeatureArticleViewModel
    {
        public Article primary { get; set; }

        public IList<Article> featuredNews = new List<Article>();
    }

    public class ArticlePreviousNextModel
    {
        public Article nextArticle { get; set; }
        public Article previousArticle { get; set; }
        public bool iconOnly { get; set; }
        public string Filter { get; set; }
    }

    public class RelatedTagsModel
    {
        public string filter { get; set; }
        public string UrlSlug { get; set; }
    }
}