﻿using System.Collections.Generic;

namespace ITOKFrontend.Models
    {
    public class NoResultModel
        {
        public string urlSlug;
        
        
        public IEnumerable<Tag> commonresults;
        public List<ITOK.Core.Data.Model.Tags> popularHubs;

        public NoResultModel(string urlSlug, IEnumerable<Tag> tags)
            {
            // TODO: Complete member initialization
            this.urlSlug = urlSlug;
            this.commonresults = tags;
            }

        public NoResultModel(string urlSlug1, IEnumerable<Tag> commonresults, List<ITOK.Core.Data.Model.Tags> popularHubs)
            {
            // TODO: Complete member initialization
            this.urlSlug = urlSlug1;
            this.commonresults = commonresults;
            this.popularHubs = popularHubs;
            }
        }
    }