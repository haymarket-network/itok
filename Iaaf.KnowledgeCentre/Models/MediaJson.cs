﻿namespace ITOKFrontend.Models
{
    public class MediaJson
    {

        public string fileFormatGroup { get; set; }
        public string Url { get; set; }
        public string MobileUrl { get; set; }
        public string SEOTitle { get; set; }
        public string otherLink { get; set; }
        public string Date { get; set; }
        public bool Gallery { get; set; }

        public MediaJson(string url, string mobileUrl, string seotitle)
        {
            this.Url = url;
            this.MobileUrl = mobileUrl;
            this.SEOTitle = seotitle;
        }



        public MediaJson(string url, string mobileUrl, string seotitle, string fileformatgroup, string otherlink, string date, bool gallery = false)
        {
            // TODO: Complete member initialization
            Url = url;
            MobileUrl = mobileUrl;
            SEOTitle = seotitle;
            fileFormatGroup = fileformatgroup;
            otherLink = otherlink;
            Date = date;
            Gallery = gallery;

        }




        public MediaJson(string url, string mobileUrl, string seotitle, string fileformatgroup, string date)
        {
            // TODO: Complete member initialization
            Url = url;
            MobileUrl = mobileUrl;
            SEOTitle = seotitle;
            fileFormatGroup = fileformatgroup;
            Date = date;
        }




    }
}
