﻿using System;
using System.Collections.Generic;
using ITOK.Core.Common.Constants;
using ITOK.Core.Data.Model;

namespace ITOKFrontend.Models
    {
    public class SearchModels
        {

        public class SearchFilterModel
            {
            public string ListHeading;
            public IList<Article> Articles = new List<Article>();

            public string Title { get; set; }

            public int TotalItemCount { get; set; }

            public string Filter { get; set; }


            public string BaseUrl { get; set; }




            public string SearchText { get; set; }
            public SearchFilter SearchFilter { get; set; }
            public DateTime? Start { get; set; }
            public DateTime? End { get; set; }

            public SortOrder SortOrder { get; set; }


            public string  Tags { get; set; }
            public string BaseDataUrl { get; set; }

            public IEnumerable<Tag> FilterTags { get; set; }

            public List<ITOK.Core.Data.Model.Tags> SelectedTags { get; set; }
            }
        }
    }