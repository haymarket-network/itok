﻿using ITOK.Core.Data.Model;
using System.Collections.Generic;

namespace ITOKFrontend.Models
{
    public class AnswerFilterViewModel
    {
        public string OverviewTitle { get; set; }
        public string SearchHeader { get; set; }
        public string SelectedEventId { get; set; }
        public string SelectedYearId { get; set; }
        public string SelectedCategoryId { get; set; }
        public string SelectedAreaId { get; set; }

        public List<AnswerFilter> AllFilteredData { get; set; }
        public List<Tags> EventTags;
        public List<Tags> YearTags;
        public List<Tags> AreaTags;
        public List<Tags> CategoryTags;
        public List<string> FilterByTag;

        public AnswerFilterViewModel()
        {
            EventTags = new List<Tags>();
            YearTags = new List<Tags>();
            AreaTags = new List<Tags>();
            CategoryTags = new List<Tags>();
            FilterByTag = new List<string>();
        }

    }

    public class AnswerFilter
    {
        public List<AnswerFilterData> Answers;
        public string GroupByHeader { get; set; }
        public bool IsFavoriteForCurrentUser { get; set; }

        public AnswerFilter()
        {
            Answers = new List<AnswerFilterData>();
        }
    }
}