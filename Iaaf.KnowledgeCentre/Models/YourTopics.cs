﻿namespace ITOKFrontend.Models
    {
        public class YourTopics
        {
        public string UrlSlug { get; set; }

        public string DisplayText { get; set; }

        public int UnreadCount { get; set; }

        public bool Forced { get; set; }
        }
    }
