﻿using System.Collections.Generic;

namespace ITOKFrontend.Models
{
    public class SuggestionFindArticleViewModel
    {
        public IList<SuggestionFindArticle> PopularTags { get; set; }
    }

    public class SuggestionFindArticle
    {
        public string Name { get; set; }
    }
}