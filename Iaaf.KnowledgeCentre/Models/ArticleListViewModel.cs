﻿using System.Collections.Generic;
using ITOK.Core.Data.Model;

namespace ITOKFrontend.Models
{
    public class ArticleListViewModel
    {
        public IList<Article> ArticleList { get; set; }
    }
}