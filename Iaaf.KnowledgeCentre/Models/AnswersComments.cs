﻿using System;
using System.Collections.Generic;

namespace ITOKFrontend.Models
{
    public class AnswersComments 
    {
        public string QuestionText { get; set; }
        public IList<UsersComment> UsersComment;

        public AnswersComments()
        {
            UsersComment = new List<UsersComment>();
        }
    }

    public class UsersComment
    {
        public string UserName { get; set; }
        public string Comment { get; set; }
        public DateTime CommentDate { get; set; }
    }
}