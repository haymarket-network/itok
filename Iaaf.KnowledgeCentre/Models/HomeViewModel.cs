﻿using System.Collections.Generic;

namespace ITOKFrontend.Models
    {
    public class HomeViewModel
        {
        public List<ITOK.Core.Data.Model.Tags> Tags { get; set; }

        

        public List<ITOK.Core.Data.Model.CuratedContent> Breifing { get; set; }

        public IEnumerable<HomeTopModel> MyRoleContent { get; set; }
        public int Missed14Count { get; set; }
        public List<ITOK.Core.Data.Model.Tags> AreaTags { get; set; }
        public List<ITOK.Core.Data.Model.Tags> EventTags { get; set; }

    }

    public class HomeTopModel{
        public string UrlSlug { get; set; }
        public ITOK.Core.Data.Model.Media PrimaryImage { get; set; }
        public string Title { get; set; }
    }
    }