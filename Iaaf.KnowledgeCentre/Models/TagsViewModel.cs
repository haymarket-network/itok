﻿using System.Collections.Generic;
using ITOK.Core.Data.DTOs;

namespace ITOKFrontend.Models
{
    public class TagsViewModel
    {
        public IList<Tags> PopularTags { get; set; }
    }

    public class Tag
    {
        public string tagName { get; set; }
        public string tagSlug { get; set; }
        public string tagCount { get; set; }
    }

    public class MenuTopicsModel
    {
        public List<ITOK.Core.Data.Model.Tags> AreaTags { get; set; }
        public List<ITOK.Core.Data.Model.Tags> EventTags { get; set; }
    }
}