﻿namespace ITOKFrontend.Models.Constants
{
    public enum ArticleFilterType
        {
        None,
        UserFav,
        UserReading,
        History,
        Popular,
        Search,
        Latest,
        Breifing,
        Article,
        Unread
        }
}