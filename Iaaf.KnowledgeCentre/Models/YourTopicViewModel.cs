﻿using System.Collections.Generic;

namespace ITOKFrontend.Models
    {
    public class YourTopicViewModel
        {
        public List<YourTopics> Topics { get; set; }

        public ITOK.Core.Common.Constants.Brand Brand { get; set; }

        public List<ITOK.Core.Data.Model.Tags> PopularHubs { get; set; }
        }
    }