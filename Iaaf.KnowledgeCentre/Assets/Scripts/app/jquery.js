﻿define(["jquery", "intro", "jqueryui", "bootstrap"], function ($, introJs) {
    


    // jquery has been loaded
    console.log("jquery has been loaded");



    /**
    * User Login
    */

    $("#PrimaryJobRole").on("change", function () {
        if ($(this).val() === "") {
            $("#confirm").addClass("disabled");
        } else {
            $("#confirm").removeClass("disabled");
        }
    });

    $(document).ready(function () {
        if ($("#PrimaryJobRole").val() === "") {
            $("#confirm").addClass("disabled");
        } else {
            $("#confirm").removeClass("disabled");
        }
    });

    $("#confirm").click(function () {
        if (!$(this).hasClass("disabled")) {
            // tidy
            $("#primaryPlaceHolder").hide();
            $(this).hide();
            // function
            $("#confirmsecjobrole").show();
            var selectedID = $("#PrimaryJobRole").val();
            $.get("/home/secondaryjobroles/" + selectedID, function (data) {
                $("#secondaryPlaceHolder").html(data);
                $("#secondaryPlaceHolder").slideUp().fadeIn("fast");
            });
        }
    });

    $(document).on("change", "#secondaryPlaceHolder .checklist #secondaryJobRoles", function () {
        console.log("Value " + $("select[name='secondaryJobRoles']").val());
        if ($("select[name='secondaryJobRoles']").val() == "") {
            console.log("none");
            // function
            $("#confirmsecjobrole").addClass("disabled");
            $("#confirmsecjobrole").attr('disabled', 'disabled');
        } else {
            console.log("one or more than one");
            // function
            $("#confirmsecjobrole").removeClass("disabled");
            $("#confirmsecjobrole").removeAttr("disabled");
        }
    });

    $("#confirmsecjobrole").click(function () {
        if (!$(this).hasClass("disabled")) {
            // tidy
            $("#secondaryPlaceHolder").hide();
            $(this).hide();
            // function
            $("#enter").show();
            var selectedID = $("#PrimaryJobRole").val();
            $.get("/home/FavouriteTags/" + selectedID, function (data) {
                $("#tertiaryPlaceHolder").html(data);
                $("#tertiaryPlaceHolder").slideUp().fadeIn("fast");
            });
        }
    });

    $(document).on("change", "#tertiaryPlaceHolder .checklist input[type='checkbox']", function () {
        if ($("input[name='favouriteTags']:checked") == null || $("input[name='favouriteTags']:checked").length == 0) {
            console.log("none");
            // function
            $("#enter").addClass("disabled");
            $("#enter").attr('disabled', 'disabled');
        } else {
            console.log("one or more than one #2");
            // function
            $("#enter").removeClass("disabled");
            $("#enter").removeAttr("disabled");
        }
    });



    $("#searchexpand").click(function () {
        if ($("#searchbar").hasClass("in")) {
            $(this).removeClass("active");
            $("#searchbar").removeClass("in");
        } else {
            $(this).addClass("active");
            $("#searchbar").addClass("in");
        }
    });






    $("#showrelatedtags").click(function () {
        if ($("#relatedtagscollapse").hasClass("in")) {
            $(this).removeClass("btn-topics-active");
            $("#relatedtagscollapse").removeClass("in");
        } else {
            $(this).addClass("btn-topics-active");
            $("#relatedtagscollapse").addClass("in");
        }
    });




    $(".showrelatedalerts").click(function () {
        if ($(this).siblings(".relatedalertscollapse").hasClass("in")) {
            $(this).removeClass("btn-alerts-active");
            $(this).siblings(".relatedalertscollapse").removeClass("in");
        } else {
            $(this).addClass("btn-alerts-active");
            $(this).siblings(".relatedalertscollapse").addClass("in");
        }
    });
    



    /**
    * Back to Top
    */
    $("#back-to-top > a.btn").click(function () {
        $("html, body").animate({ scrollTop: 0 }, "fast");
        return false;
    });
    $(window).scroll(function () {
        if ($(window).scrollTop() > 100) {
            $("#back-to-top").show();
        } else {
            $("#back-to-top").hide();
        }
    });




    /**
    * Bootstrap Tooltip
    */
    $(document).ready(function () {
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    });




    /**
    * Bootstrap Popover
    */
    $(function () {
        $('[data-toggle="popover"]').popover()
    });




    /**
    * Bootstrap Popover
    */
    $("#addtag").click(function () {
        $.get($(this).attr("data-urlcall"), function (data) {
            console.log(data);
        });
        $(this).addClass("hide");
        $(this).siblings("#removetag").removeClass("hide");
    });


    $("#removetag").click(function () {
        $.get($(this).attr("data-urlcall"), function (data) {
            console.log(data);
        });
        $(this).addClass("hide");
        $(this).siblings("#addtag").removeClass("hide");
    });
    $(".profileaddtag").click(function () {
        $.get($(this).attr("data-urlcall"), function (data) {
            console.log(data);
        });
        $(this).addClass("hide");
        $(this).siblings(".profileremovetag").removeClass("hide");
    });


    $(".profileremovetag").click(function () {
        $.get($(this).attr("data-urlcall"), function (data) {
            console.log(data);
        });
        $(this).addClass("hide");
        $(this).siblings(".profileaddtag").removeClass("hide");
    });





    $(".add-this-tag").click(function () {
        $.get($(this).attr("data-urlcall"), function (data) {
            console.log(data);
        });
        $(this).addClass("hide");
        $(this).siblings(".remove-this-tag").removeClass("hide");
    });


    $(".remove-this-tag").click(function () {
        $.get($(this).attr("data-urlcall"), function (data) {
            console.log(data);
        });
        $(this).addClass("hide");
        $(this).siblings(".add-this-tag").removeClass("hide");
    });








    //startIntro
    $("#startIntro").click(function () {
        
        startIntro();

    });
    $("#profileStartIntro").click(function () {

        startIntro();

    });



    var intro = introJs();

    var helpSteps = [];

    // check if id exists and then add to helpSteps
  
    if ($("#step2").length > 0) {
        helpSteps.push({
            element: '#step2',
            intro: "<h4>Popular topics - what others are reading</h4><p>Click on this box to see what's trending across the Hub. This will help you find the most read subjects across the networks from all areas. Simply click + next to the topic to add it to your key topics list.</p>"
        });
    }
 
    if ($("#step4").length > 0) {
        helpSteps.push({
            element: '#step4',
            intro: "<h4>Filter search results</h4><p>Use the search filters to find the information you need. You can filter by Competition, Editions or Areas.</p>"
        });
    }
 
    if ($("#searchformnav").length > 0) {
        helpSteps.push({
            element: '#searchformnav',
            intro: "<h4>Search - plain, simple and effective</h4><p>Find what you are looking for, quickly and easily. </p>"
        });
    }
    
    if ($("#profileFavourite").length > 0) {
        helpSteps.push({
            element: '#profileFavourite',
            intro: "<h4>Save answers</h4><p>Save time looking for the information you use every day by storing them in your favourites. This can be added to and deleted easily by clicking * and makes using the Knowledge Centre even easier.</p>"
        });
    }
    if ($("#profileReadingListMenu").length > 0) {
        helpSteps.push({
            element: '#profileFavouriteMenu',
            intro: "<h4>My Favourite Articles</h4><p>You can access your favourited answers from here too</p>"
        });
    }
    if ($("#profileReadingList").length > 0) {
        helpSteps.push({
            element: '#profileReadingList',
            intro: "<h4>My Folders</h4><p>Another way to organise information you want to keep track of. Arrange answers you want to keep track of into your own set of folders</p>"
        });
    }
    if ($("#profileReadingListMenu").length > 0) {
        helpSteps.push({
            element: '#profileReadingList',
            intro: "<h4>My Folders</h4><p>You can access your folders of saved answers from here too</p>"
        });
    }
  
   


    intro.setOptions({
        steps: helpSteps
    });

    function startIntro() {
        intro.start();
    }

    if (localStorage.getItem("startIntro") === null) {

        localStorage.setItem("startIntro", true);

        if (localStorage.getItem("startIntro")) {
            setTimeout(function () {
                startIntro();
            }, 1000);
            localStorage.setItem("startIntro", false);
        }

    }



    // don't close tabs
    $(document).on('click', '.no-close-on-tabs', function (e) {
        e.stopPropagation();
    });



    










});