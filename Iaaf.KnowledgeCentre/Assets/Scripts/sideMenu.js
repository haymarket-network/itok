﻿
$(function () {

    var selectedEvent = getParameterByName('e');
    var selectedArea = getParameterByName('a');

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    if (selectedEvent || selectedArea) {


        $('.sidebar-menu-sec .submenu li a').each(function () {
            var $this = $(this);
            // if the current path is ends with matched url key, make it active
            if ($this.attr('href').lastIndexOf(selectedEvent) > 0 || $this.attr('href').lastIndexOf(selectedArea) > 0) {
                $this.parent("li").addClass('active');
                $this.parent().parent("ul").show();
                $this.parent().parent("ul").parent("li").addClass("active");
            }
        });
    }
    $(".sidebar-menu-sec > li > span:not(.profile-span)").click(function () {
        $(this).parent("li").toggleClass("active");
        $(this).parent("li").children(".submenu").slideToggle();
    });
});