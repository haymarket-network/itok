﻿$(function () {

    $('#eventId').change(function () {
        $("#yearId").val('');
    });
    $('#categoryId').change(function () {
        $("#areaId").val('');
    });

    $('.drop-filter').change(function () {
        debugger;
        $(".overlay-loader").show();
        var filter = "";
        var eventId = $('#eventId option:selected').val();
        var yearId = $('#yearId option:selected').val();
        var categoryId = $('#categoryId option:selected').val();
        var areaId = $('#areaId option:selected').val();

        if (eventId) {
            filter = "?e=" + eventId;
        }
        if (yearId) {
            filter = filter == "" ? "?y=" + yearId : filter + "&y=" + yearId;
        }
        if (categoryId) {
            filter = filter == "" ? "?c=" + categoryId : filter + "&c=" + categoryId;
        }
        if (areaId) {
            filter = filter == "" ? "?a=" + areaId : filter + "&a=" + areaId;
        }
        if (filter) {
            document.location.href = window.location.origin + window.location.pathname + filter;
        }
        $(".overlay-loader").hide();

    }
    );



});
function viewComments(ansId, questionText) {
    debugger;
    $('#modalWrapper').html("");
    $.ajax({
        url: "/Hub/GetQuestionCommentsByAnswerId", //
        data: { answerId: ansId, questionText: questionText },
        success: function (data) {
            $('#modalWrapper').html(data);
        }
    });
}