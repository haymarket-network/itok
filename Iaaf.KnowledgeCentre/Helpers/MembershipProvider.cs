﻿using ITOK.Core.Data.Model;
using ITOK.Core.Common.Interfaces;

namespace ITOKFrontend.Helpers
{

    internal class MembershipProvider : IMembershipProvider
    {
        public User ActiveUser
        {
            get { return MembershipHelper.GetActiveUser(); }
        }

        public string ActiveUserId
        {
            get
            {
                return MembershipHelper.GetActiveUserId;
            }
        }
    }
}
