﻿using System.Web;
using System.Web.Mvc;
using ITOK.Core.Services.Security;

namespace ITOKFrontend.Helpers.Authorise
{
    public class CustomAuthorise : AuthorizeAttribute
    {

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (!HasValidUser())
            {
                    base.HandleUnauthorizedRequest(filterContext);
               

            }
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return HasValidUser();
        }

        private bool HasValidUser()
        {


            return LiveUser.Get() != null;
 // default user
            // LoginManager.CurrentUser = user;
        }

    }
}