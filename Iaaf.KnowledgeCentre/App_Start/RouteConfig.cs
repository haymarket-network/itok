﻿using System.Web.Mvc;
using System.Web.Routing;
using ITOK.Core.Common.Utilities.Routing;


namespace ITOKFrontend
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapMvcAttributeRoutes();
      
            
            routes.MapRoute(
                name: "History",
                url: "profile/history",
                defaults: new { controller = "Profile", action = "ViewedArticleHistory" }                );

            routes.MapRoute(
               name: "favorite",
               url: "profile/favorites",
               defaults: new { controller = "Profile", action = "FavoriteArticles" }
               );
            routes.MapRoute(
              name: "deletefavouritetopic",
              url: "profile/deletetopic",
              defaults: new { controller = "Profile", action = "DeleteTopic" }
              
              );

            routes.MapRoute(
               name: "readinglists",
               url: "profile/readinglists",
               defaults: new { controller = "Profile", action = "ReadingArticles" }
               
               );

            routes.MapRoute(
               name: "readinglistnew",
               url: "readinglist/new",
               defaults: new { controller = "ReadingList", action = "new" }
               
               );

            routes.MapRoute(
               name: "readinglistindex",
               url: "readinglist",
               defaults: new { controller = "ReadingList", action = "Index" }
               
               );

            routes.MapRoute(
               name: "myprofile",
               url: "profile",
               defaults: new { controller = "Profile", action = "Index" }
               
               );

            routes.MapRoute(
                name: "AddComment",
                url: "article/addcomment",
                defaults: new { controller = "Article", action = "AddComment" }
               
                );
          

            routes.MapRoute(
                name: "FindByBrandArticleNumber",
                url: "hub/find/{brandarticlenumber}",
                defaults: new { controller = "Hub", action = "Find" }
               
                );
            routes.MapRoute(
                name: "FavoritePost",
                url: "article/favorite/{articleId}/{yesno}",
                defaults: new { controller = "Article", action = "UpdateFavorite" }
               
                );

            routes.MapRoute(
                name: "FavoriteCount",
                url: "article/getfavorite/{articleId}",
                defaults: new { controller = "Article", action = "GetFavorite" }
               
                );

            routes.MapRoute(
                name: "ReadingPost",
                url: "article/reading/{articleId}/{yesno}/{readingListId}",
                defaults: new { controller = "Article", action = "UpdateReading" }
               
                );

            routes.MapRoute(
                name: "ReadingCount",
                url: "article/getreading/{articleId}/{readingListId}",
                defaults: new { controller = "Article", action = "GetReading" }
               
                );
            
            routes.MapRoute(
                name: "GetRelatedTags",
                url: "hub/getrelatedtags/{tagFilter}",
                defaults: new { controller = "Hub", action = "GetRelatedTags", tagFilter = UrlParameter.Optional }
               
                
                );

            routes.MapRoute(
                name: "GetTags",
                url: "hub/GetTags/{urlSlug}",
                defaults: new { controller = "Hub", action = "GetTags" }
               
                );

            routes.MapRoute(
                name: "DataPosts",
                url: "data/posts/{type}/{filter}",
                defaults: new { controller = "Article", action = "MoreData", filter = UrlParameter.Optional }
               
                );

            routes.MapRoute(
                name: "HistoryDataPosts",
                url: "history/data/posts/",
                defaults: new { controller = "History", action = "MoreData", filter = UrlParameter.Optional }
               
                );

            routes.MapRoute(
               name: "unread",
               url: "unread",
               defaults: new { controller = "Hub", action = "Unread" }
              
               );

            routes.MapRoute(
                name: "latest-type",
                url: "latest/{type}",
                defaults: new {controller = "Hub", action = "Latest", type = UrlParameter.Optional},
                constraints :new {type = new PostTypeConstraint()}
                );

            routes.MapRoute(
                name: "latest",
                url: "latest",
                defaults: new { controller = "Hub", action = "Latest" }
               
                );
            routes.MapRoute(
               name: "FilterArticles",
               url: "search/ListContainer",
               defaults: new { controller = "Search", action = "ListContainer" }
              
               );


            routes.MapRoute(
                name: "popular",
                url: "popular",
                defaults: new { controller = "Hub", action = "Popular" }
               
                );


            routes.MapRoute(
                name: "Preview",
                url: "preview/{id}",
                defaults: new { controller = "Article", action = "Preview", }
               
                );

            //routes.MapRoute(
            //    name: "Hub",
            //    url: "hub/{urlSlug}",
            //    defaults: new { controller = "Hub", action = "Index", }
               
            //    );

            routes.MapRoute(
                name: "Article",
                url: "{articletype}/{urlSlug}",
                defaults: new {controller = "Article", action = "Detail",},
                constraints: new {articletype = new PostTypeConstraint()}
                );


        
            routes.MapRoute(
              name: "UserSignout",
              url: "home/signout",
              defaults: new { controller = "Home", action = "signout" }             ,
               namespaces: new[] { "ITOKFrontend.Controllers" }
              );
            routes.MapRoute(
             name: "MenuTopics",
             url: "home/menutopics",
             defaults: new { controller = "Home", action = "MenuTopics" }            ,
              namespaces: new[] { "ITOKFrontend.Controllers" }
             );
            routes.MapRoute(
           name: "FindBy",
           url: "findby/{type}",
           defaults: new { controller = "Home", action = "FindBy" },
            namespaces: new[] { "ITOKFrontend.Controllers" }
           );



            routes.MapRoute(
              name: "observerprogramme",
              url: "observer-programme",
              defaults: new { controller = "Home", action = "ObserverProgramme", }

              );

            routes.MapRoute(
             name: "biddingcandidature",
             url: "bidding-candidature",
             defaults: new { controller = "Home", action = "BiddingCandidature", }

             );

            routes.MapRoute(
           name: "acceptregulations",
           url: "acceptregulations",
           defaults: new { controller = "Home", action = "AcceptRegulations", }

           );
            routes.MapRoute(
            name: "eventplanning",
            url: "event-planning",
            defaults: new { controller = "Home", action = "EventPlanning", }

            );
            routes.MapRoute(
          name: "CapturingHistory",
          url: "history",
          defaults: new { controller = "Home", action = "History", }

          );



            routes.MapRoute(
              name: "DefaultUrlslug",
              url: "{urlSlug}",
              defaults: new { controller = "Home", action = "Index", urlSlug  = UrlParameter.Optional}
             ,
               namespaces: new[] { "ITOKFrontend.Controllers" }
              );


            routes.MapRoute(
              name: "NewDefault",
              url: "{controller}/{action}/{id}",
              defaults: new { controller = "Home", action = "Index" },
               namespaces: new[] { "ITOKFrontend.Controllers" }
              );

            routes.MapRoute(
               name: "Home",
               url: "",
               defaults: new { controller = "Home", action = "Index", },
               namespaces: new[] { "ITOKFrontend.Controllers" }
               );       



            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new {controller = "Home", action = "Index", id = UrlParameter.Optional},
                namespaces: new [] { "ITOKFrontend.Controllers" });

        }
    }
}
