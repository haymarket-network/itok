﻿using System;
using System.Linq;
using System.Web;
using System.Web.Routing;
using ITOK.Core.Common.Constants;

namespace ITOKFrontend.Routing
    {
    public class BrandConstraint : IRouteConstraint
        {

        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
            {
            // You can also try Enum.IsDefined, but docs say nothing as to
            // is it case sensitive or not.
            var response = Enum.GetNames(typeof(Brand)).Any(s => s.ToLowerInvariant() == values[parameterName].ToString().ToLowerInvariant());
            return response;
            }
        
        }
    }