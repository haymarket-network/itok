﻿using ITOK.Core.Data.Model;

namespace ITOK.Core.Common.Interfaces
    {
    public interface IMembershipProvider
        {
        User ActiveUser { get; }

        string ActiveUserId { get; }
        }
    }