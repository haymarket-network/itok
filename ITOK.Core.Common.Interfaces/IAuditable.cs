﻿using System;

namespace ITOK.Core.Common.Interfaces
    {
    public interface IAuditable
        {
        string CreatedById { get; set; }
        DateTime CreatedOn { get; set; }
        string UpdatedById { get; set; }
        DateTime UpdatedOn { get; set; }
        }
    }