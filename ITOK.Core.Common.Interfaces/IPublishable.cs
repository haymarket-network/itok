﻿using System;
using ITOK.Core.Common.Constants;

namespace ITOK.Core.Common.Interfaces
    {
    public interface IPublishable
        {
        PublicationStatus Status { get; set; }

        /// <summary>
        /// Null value can be used when the entity is in unpublished state (ArticleStatus) and the live date is not known
        /// </summary>
        DateTime LiveFrom { get; set; }

        /// <summary>
        /// Null value indicates indefinite publication period
        /// </summary>
        DateTime? LiveTo { get; set; }
        }
    }