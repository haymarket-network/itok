﻿namespace ITOK.Core.Common.Interfaces
    {
    public interface IInterceptorFor<T>
        {
        void Intercept(T entity);
        }
    }