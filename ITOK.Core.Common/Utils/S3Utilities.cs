﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading;
using Amazon.S3;
using Amazon.S3.Model;
using ITOK.Core.Common.Config;
using ITOK.Core.Common.Constants;
using ImageResizer;

namespace ITOK.Core.Common.Utils
{
    public static partial class S3Utilities
    {
        private static AmazonS3Client _s3Client;

        static S3Utilities()
        {
            _s3Client = new AmazonS3Client(ITOKConfig.Instance.S3.PublicAccessKey, ITOKConfig.Instance.S3.PrivateAccessKey, Amazon.RegionEndpoint.EUWest1);
        }

        public static List<S3Object> ListError()
        {
            return ListBucketFileName("weberror");
        }

        public static void SaveImage(Bitmap croppedImage, MediaProfileElement profileConfig, String filename)
        {
            saveImageFile(croppedImage, profileConfig, filename, FileFormatGroup.Image);
        }

        public static void SaveImageNoResize(Bitmap croppedImage, MediaProfileElement profileConfig, String filename)
        {
            saveImageFile(croppedImage, profileConfig, filename, FileFormatGroup.Image, true);
        }

        public static void SaveImage(Stream croppedImageStream, MediaProfileElement profileConfig, string filename)
        {
            var bm = new Bitmap(croppedImageStream);
            saveImageFile(bm, profileConfig, filename, FileFormatGroup.Image);
        }

        public static bool MediaExists(MediaProfileElement profileConfig, string filename)
        {
            try
            {
                var response = _s3Client.GetObjectMetadata(new GetObjectMetadataRequest
                {
                    BucketName = ITOKConfig.Instance.S3.BucketName,
                    Key = string.Format("{0}/{1}/{2}",
                        ITOKConfig.Instance.Media.Directory,
                        profileConfig.DirectoryName,
                        filename)
                });

                return true;
            }
            catch (Amazon.S3.AmazonS3Exception ex)
            {
                if (ex.StatusCode == System.Net.HttpStatusCode.NotFound ||
                    ex.StatusCode == System.Net.HttpStatusCode.Forbidden)
                    return false;

                //status wasn't not found, so throw the exception
                throw;
            }
        }

        public static void DeleteMedia(MediaProfileElement profileConfig, string filename)
        {
            var response = _s3Client.DeleteObject(new DeleteObjectRequest
            {
                BucketName = ITOKConfig.Instance.S3.BucketName,
                Key = filename
            });
        }

        public static Image GetImage(MediaProfileElement profileConfig, string filename)
        {
            var img = GetImg(profileConfig, filename);

            var elapsed = new TimeSpan();
            while ((img == null) && (elapsed < TimeSpan.FromSeconds(10)))
            {
                Thread.Sleep(1000);
                elapsed.Add(TimeSpan.FromSeconds(1));
                img = GetImg(profileConfig, filename);
            }

            return img;
        }

        private static Image GetImg(MediaProfileElement profileConfig, string filename)
        {
            var response = _s3Client.GetObject(new GetObjectRequest
            {
                BucketName = ITOKConfig.Instance.S3.BucketName,
                Key = string.Format("{0}/{1}/{2}",
                    ITOKConfig.Instance.Media.Directory,
                    profileConfig.DirectoryName,
                    filename)
            });
            if (response.ResponseStream == null) return null;
            return new Bitmap(response.ResponseStream);
        }

        public static void SaveAudio(Stream stream, string filename)
        {
            putObject(stream,
                string.Format("{0}/{1}",
                    ITOKConfig.Instance.Audio.Directory,
                    filename));
        }

        public static void DeleteAudio(string filename)
        {
            var response = _s3Client.DeleteObject(new DeleteObjectRequest
            {
                BucketName = ITOKConfig.Instance.S3.BucketName,
                Key = string.Format("{0}/{1}",
                    ITOKConfig.Instance.Audio.Directory,
                    filename)
            });
        }

        public static void SaveDocument(System.Web.HttpPostedFileBase file, string fileName)
        {
            putObject(file.InputStream,
                string.Format("{0}/{1}",
                    ITOKConfig.Instance.Document.Directory,
                    fileName));
        }

        public static void SaveOriginal(System.Web.HttpPostedFileBase file, string fileName)
        {
            putObject(file.InputStream,
                string.Format("{0}/{1}/{2}",
                    ITOKConfig.Instance.Media.Directory,
                    "Original",
                    fileName));
        }

        public static void SaveDocument(Stream stream, string fileName)
        {
            putObject(stream, string.Format("{0}/{1}",
                    ITOKConfig.Instance.Document.Directory,
                    fileName));
        }

        public static bool DocumentExists(string fileName)
        {
            try
            {
                var response = _s3Client.GetObjectMetadata(new GetObjectMetadataRequest()
                {
                    BucketName = ITOKConfig.Instance.S3.BucketName,
                    Key = string.Format("{0}/{1}",
                         ITOKConfig.Instance.Document.Directory,
                         fileName)
                });

                return true;
            }
            catch (Amazon.S3.AmazonS3Exception ex)
            {
                if (ex.StatusCode == System.Net.HttpStatusCode.NotFound ||
                    ex.StatusCode == System.Net.HttpStatusCode.Forbidden)
                    return false;

                //status wasn't not found, so throw the exception
                throw;
            }
        }

        public static void DeleteDocument(string fileName)
        {
            var response = _s3Client.DeleteObject(new DeleteObjectRequest
            {
                BucketName = ITOKConfig.Instance.S3.BucketName,
                Key = string.Format("{0}/{1}",
                    ITOKConfig.Instance.Document.Directory,
                    fileName)
            });
        }

        public static void SaveToLocation(System.Web.HttpPostedFileBase file, string fileName)
        {
            putObject(file.InputStream,
                string.Format("{0}",
                    fileName));
        }

        public static void DeleteFromLocation(string fileName)
        {
            var response = _s3Client.DeleteObject(new DeleteObjectRequest
            {
                BucketName = ITOKConfig.Instance.S3.BucketName,
                Key = string.Format("{0}",
                    fileName)
            });
        }

        public static Stream DownloadDocument(string fileName)
        {
            Stream imageStream = new MemoryStream();
            var request = new GetObjectRequest
            {
                BucketName = ITOKConfig.Instance.S3.BucketName,
                Key = string.Format("{0}/{1}",
                    ITOKConfig.Instance.Document.Directory,
                    fileName)
            };
            using (var response = _s3Client.GetObject(request))
            {
                response.ResponseStream.CopyTo(imageStream);
            }
            imageStream.Position = 0;
            // Clean up temporary file.
            // System.IO.File.Delete(dest);
            return imageStream;
        }

        #region Private Methods

        private static void saveImageFile(Bitmap croppedImage, MediaProfileElement profileConfig, string filename, FileFormatGroup fileFormatGroup)
        {
            saveImageFile(croppedImage, profileConfig, filename, fileFormatGroup, true);
        }

        private static void saveImageFile(Bitmap croppedImage, MediaProfileElement profileConfig, string filename, FileFormatGroup fileFormatGroup, bool DoNotStretch)
        {
            using (var fileStream = new MemoryStream())
            {
                var bm = new Bitmap(croppedImage);
                var fileExtension = Path.GetExtension(filename);
                var resizeSettings = new ResizeSettings();
                if (!DoNotStretch)
                    resizeSettings.Mode = FitMode.Stretch;
                else
                    resizeSettings.Mode = FitMode.Pad;

                resizeSettings.Width = profileConfig.Width;
                resizeSettings.Scale = ScaleMode.Both;
                resizeSettings.PaddingColor = Color.White;
                resizeSettings.BackgroundColor = Color.White;
                var imageFormat = ImageFormat.Jpeg;
                switch (fileExtension)
                {
                    case ".png":
                        imageFormat = ImageFormat.Png;
                        resizeSettings.Format = "png";
                        resizeSettings.Width = profileConfig.Width;
                        resizeSettings.Height = profileConfig.Height;
                        break;

                    case ".jpeg":
                    case ".jpg":
                        imageFormat = ImageFormat.Jpeg;
                        resizeSettings.Format = "jpg";
                        resizeSettings.Width = profileConfig.Width;
                        resizeSettings.Height = profileConfig.Height;
                        break;

                    default:
                        break;
                }

                if (profileConfig.ProfileName != MediaProfile.Original
                    && fileFormatGroup == FileFormatGroup.Image)
                    bm = ImageBuilder.Current.Build(croppedImage, resizeSettings, false);

                bm.Save(fileStream, imageFormat);

                if (fileStream.Length < 5242880)

                    putObject(fileStream,
                    string.Format("{0}/{1}/{2}",
                        ITOKConfig.Instance.Media.Directory,
                        profileConfig.DirectoryName,
                        filename));
                else
                    putMultiPartObject(fileStream,
                        string.Format("{0}/{1}/{2}",
                            ITOKConfig.Instance.Media.Directory,
                            profileConfig.DirectoryName,
                            filename));
            }
        }

        private static bool putObject(Stream fileSteam, string keyName)
        {
            var request = new PutObjectRequest()
            {
                BucketName = ITOKConfig.Instance.S3.BucketName,
                Key = keyName,
                InputStream = fileSteam,
                CannedACL = S3CannedACL.PublicRead,
            };

            request.Headers.CacheControl = "public,max-age=15200";

            _s3Client.PutObject(request);
            return true;
        }

        /// <summary>
        /// ToDo:Not fully tested.
        /// </summary>
        /// <param name="fileSteam"></param>
        /// <param name="keyName"></param>
        /// <returns></returns>
        private static bool putMultiPartObject(Stream fileSteam, string keyName)
        {
            // List to store upload part responses.
            var uploadResponses = new List<UploadPartResponse>();

            var initRequest =
                new InitiateMultipartUploadRequest()
                {
                    BucketName = ITOKConfig.Instance.S3.BucketName,
                    Key = keyName
                };

            var initResponse =
                _s3Client.InitiateMultipartUpload(initRequest);

            var contentLength = fileSteam.Length;
            long partSize = 5242880; // 5MB

            long filePosition = 0;
            for (var i = 1; filePosition < contentLength; i++)
            {
                //Create request to upload a part.
                var uploadRequest = new UploadPartRequest()
                {
                    BucketName = ITOKConfig.Instance.S3.BucketName,
                    Key = keyName,
                    UploadId = initResponse.UploadId,
                    PartNumber = i,
                    PartSize = partSize,
                    FilePosition = filePosition,
                    InputStream = fileSteam
                };
                //Upload part and add response to our list.
                uploadResponses.Add(_s3Client.UploadPart(uploadRequest));

                filePosition += partSize;
            }

            // complete
            var compRequest = new CompleteMultipartUploadRequest()
            {
                BucketName = ITOKConfig.Instance.S3.BucketName,
                Key = keyName,
                UploadId = initResponse.UploadId
            };

            var completeUploadResponse =
            _s3Client.CompleteMultipartUpload(compRequest);

            return true;
        }

        private static List<S3Object> ListBucketFileName(string bucketName)
        {
            var keys = new List<S3Object>();

            var request = new ListObjectsRequest();
            request = new ListObjectsRequest();
            request.BucketName = bucketName;
            do
            {
                var response = _s3Client.ListObjects(request);

                foreach (var entry in response.S3Objects)
                {
                    keys.Add(entry);
                }

                // Process response.
                // ...

                // If response is truncated, set the marker to get the next
                // set of keys.
                if (response.IsTruncated)
                {
                    request.Marker = response.NextMarker;
                }
                else
                {
                    request = null;
                }
            } while (request != null);

            return keys;
        }

        #endregion Private Methods

        public static void MoveFile(MediaProfileElement current, MediaProfileElement move, string fileName)
        {
            //get image
            var imgToMove = GetImage(current, fileName);

            var Key = string.Format("{0}/{1}/{2}",
                    ITOKConfig.Instance.Media.Directory,
                    move.DirectoryName,
                    fileName);

            using (var stream = new MemoryStream())
            {
                // Save image to stream.
                imgToMove.Save(stream, ImageFormat.Bmp);
                //save img
                putObject(stream, Key);
            }
        }
    }
}