﻿using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using RazorEngine;
using System;
using System.IO;
using System.Linq;
using System.Web;
using ITOK.Core.Common.Config;

namespace ITOK.Core.Common.Utils
{
    public static partial class SESUtilities
    {
        public static Boolean SendEmail(string to, string bcc, string subject, string text = null, string html = null, string emailReplyTo = null, string returnPath = null)
        {
            if (text != null && html != null)
            {
                var from = ITOKConfig.Instance.AWS.VerifiedEmail;

                var toEmails
                    = to
                    .Replace(", ", ",")
                    .Split(',')
                    .ToList();

                var bccEmails
                   = bcc
                   .Replace(", ", ",")
                   .Split(',')
                   .ToList();

                var destination = new Destination
                {
                    ToAddresses = toEmails
                };
                if (bccEmails.Count > 0 && bccEmails[0].Length > 0)
                    destination.BccAddresses = bccEmails;

                var subjectContent = new Content
                {
                    Charset = "UTF-8",
                    Data = subject
                };

                var htmlContent = new Content
                {
                    Charset = "UTF-8",
                    Data = html
                };

                var textContent = new Content
                {
                    Charset = "UTF-8",
                    Data = text
                };

                var bodyContent = new Body
                {
                    Html = htmlContent,
                    Text = textContent
                };

                var messageContent = new Message { Body = bodyContent, Subject = subjectContent };

                var ses = new AmazonSimpleEmailServiceClient(ITOKConfig.Instance.AWS.PublicAccessKey, ITOKConfig.Instance.AWS.PrivateAccessKey, Amazon.RegionEndpoint.USEast1);

                var request = new SendEmailRequest { Destination = destination, Message = messageContent, Source = from, };

                //if (emailReplyTo != null)
                //    {
                //    List<String> replyToEmails
                //        = emailReplyTo
                //        .Replace(", ", ",")
                //        .Split(',')
                //        .ToList();

                //    request.WithReplyToAddresses(replyToEmails);
                //    }

                //if (returnPath != null)
                //    {
                //    request.WithReturnPath(returnPath);
                //    }

                try
                {
                    var response = ses.SendEmail(request);

                    return true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            throw new ArgumentException("Html and Plain-text message bodies must both be provided", "HTML, Text");
        }

        public static Boolean SendTemplateEmail<M>(string to, string subject, string textTemplatePath, string htmlTemplatePath, M templateModel, string emailReplyTo = null, string returnPath = null)
        {
            if (string.IsNullOrEmpty(textTemplatePath) && string.IsNullOrEmpty(htmlTemplatePath)) throw new Exception("Either text or html templates or both must be provided");

            string text = null;
            string html = null;

            StreamReader sr = null;
            if (!string.IsNullOrEmpty(textTemplatePath))
            {
                var absoluteTextTemplatePath = HttpContext.Current.Server.MapPath(textTemplatePath);
                if (!File.Exists(absoluteTextTemplatePath)) throw new Exception(string.Format("Could not find the text template at {0}. Check the supplied virtual path", absoluteTextTemplatePath));
                sr = new StreamReader(absoluteTextTemplatePath);
                var textTemplate = sr.ReadToEnd();
                sr.Close();
                text = Razor.Parse<M>(textTemplate, templateModel);
            }
            if (!string.IsNullOrEmpty(htmlTemplatePath))
            {
                var absoluteHtmlTemplatePath = HttpContext.Current.Server.MapPath(htmlTemplatePath);
                if (!File.Exists(absoluteHtmlTemplatePath)) throw new Exception(String.Format("Could not find the html template at {0}. Check the supplied virtual path", absoluteHtmlTemplatePath));
                sr = new StreamReader(absoluteHtmlTemplatePath);
                var htmlTemplate = sr.ReadToEnd();
                sr.Close();
                html = Razor.Parse<M>(htmlTemplate, templateModel);
            }

            return SendEmail(to, "", subject, text, html, emailReplyTo, returnPath);
        }
    }
}