﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using ITOK.Core.Common.Constants;

namespace ITOK.Core.Common.Utils
{
    public static partial class FileUtilities
    {
        private static readonly IDictionary<FileFormat, string> ContentTypeMap;
        private static readonly IDictionary<string, FileFormat> ExtensionFormatMap;
        private static readonly IDictionary<FileFormat, FileFormatGroup[]> FileFormatToGroupMap;
        private static readonly IDictionary<FileFormatGroup, FileFormat[]> GroupToFileFormatMap;

        static FileUtilities()
        {
            ExtensionFormatMap = new Dictionary<string, FileFormat>
            {
                { ".avi", FileFormat.AVI },
                { ".bmp", FileFormat.Bitmap },
                { ".bitmap", FileFormat.Bitmap },
                { ".xls", FileFormat.Excel },
                { ".xlsx", FileFormat.Excel },
                { ".flv", FileFormat.FLV },
                { ".gif", FileFormat.GIF },
                { ".jpg", FileFormat.JPEG },
                { ".jpeg", FileFormat.JPEG },
                { ".mov", FileFormat.MOV },
                { ".mp3", FileFormat.MP3 },
                { ".mp4", FileFormat.MP4 },
                { ".mpg", FileFormat.MPG },
                { ".mpeg", FileFormat.MPG },
                { ".pdf", FileFormat.PDF },
                { ".png", FileFormat.PNG },
                { ".ram", FileFormat.RAM },
                { ".rm", FileFormat.RAM },
                { ".swf", FileFormat.SWF },
                { ".wma", FileFormat.WMA },
                { ".wmv", FileFormat.WMV },
                { ".doc", FileFormat.Word },
                { ".docx", FileFormat.Word },
                { ".txt", FileFormat.Text },
                { ".ppt", FileFormat.PPT },
                { ".pptx", FileFormat.PPTX }
            };
            ContentTypeMap = new Dictionary<FileFormat, string>
            {
                { FileFormat.JPEG, "image/jpeg" },
                { FileFormat.PNG, "image/png" },
                { FileFormat.Bitmap, "image/bmp" },
                { FileFormat.GIF, "image/gif" },
                { FileFormat.PDF, "application/pdf" },
                { FileFormat.AVI, "video/avi" },
                { FileFormat.WMV, "video/x-ms-wmv" },
                { FileFormat.FLV, "video/x-flv" },
                { FileFormat.MP3, "audio/mp3" },
                { FileFormat.MPG, "video/mpeg" },
                { FileFormat.SWF, "application/x-shockwave-flash" },
                { FileFormat.MOV, "video/quicktime" },
                { FileFormat.WMA, "audio/x-ms-wma" },
                { FileFormat.MP4, "audio/mp4" },
                { FileFormat.RAM, "text/plain" }, //? : http://filext.com/file-extension/ram
                { FileFormat.Excel, "application/msexcel" },
                { FileFormat.Word, "application/doc" },
                { FileFormat.Text, "text/plain" },
                 { FileFormat.PPT, "application/vnd.ms-powerpoint" },
                  { FileFormat.PPTX, "application/vnd.ms-powerpoint" }
            };
            FileFormatToGroupMap = new Dictionary<FileFormat, FileFormatGroup[]>
            {
                { FileFormat.Bitmap, new[] { FileFormatGroup.Image, FileFormatGroup.Resource } },
                { FileFormat.GIF, new[] { FileFormatGroup.Image, FileFormatGroup.Resource } },
                { FileFormat.JPEG, new[] { FileFormatGroup.Image, FileFormatGroup.Resource } },
                { FileFormat.PNG, new[] { FileFormatGroup.Image, FileFormatGroup.Resource } },

                { FileFormat.AVI, new[] { FileFormatGroup.Video } },
                { FileFormat.FLV, new[] { FileFormatGroup.Video } },
                { FileFormat.MOV, new[] { FileFormatGroup.Video } },
                { FileFormat.MP4, new[] { FileFormatGroup.Video } },//, FileFormatGroup.Audio
                { FileFormat.MPG, new[] { FileFormatGroup.Video } },
                { FileFormat.RAM, new[] { FileFormatGroup.Video, FileFormatGroup.Audio } },
                { FileFormat.WMV, new[] { FileFormatGroup.Video } },

                { FileFormat.MP3, new[] { FileFormatGroup.Audio } },
                { FileFormat.WMA, new[] { FileFormatGroup.Audio } },
                { FileFormat.Excel, new[] { FileFormatGroup.Document, FileFormatGroup.Resource } },
                { FileFormat.PDF, new[] { FileFormatGroup.Document, FileFormatGroup.Resource } },
                { FileFormat.Word, new[] { FileFormatGroup.Document, FileFormatGroup.Resource } },
                { FileFormat.PPTX, new[] { FileFormatGroup.Document, FileFormatGroup.Resource } },
                { FileFormat.PPT, new[] { FileFormatGroup.Document, FileFormatGroup.Resource } },
                { FileFormat.Text, new[] { FileFormatGroup.Document, FileFormatGroup.Resource } }
            };

            GroupToFileFormatMap = new Dictionary<FileFormatGroup, FileFormat[]>
            {
                { FileFormatGroup.Image, new[] { FileFormat.Bitmap, FileFormat.GIF, FileFormat.JPEG, FileFormat.PNG } },
                { FileFormatGroup.Video, new[] { FileFormat.AVI, FileFormat.FLV, FileFormat.MOV, FileFormat.MP4, FileFormat.MPG, FileFormat.RAM, FileFormat.SWF, FileFormat.WMV } },
                { FileFormatGroup.Audio, new[] { FileFormat.MP3, FileFormat.RAM, FileFormat.WMA } },//FileFormat.MP4, 
                { FileFormatGroup.Document, new[] { FileFormat.Excel, FileFormat.PDF, FileFormat.Word, FileFormat.Text, FileFormat.PPT, FileFormat.PPTX } },
                { FileFormatGroup.Resource, new[] { FileFormat.Excel, FileFormat.PDF, FileFormat.Word, FileFormat.Text, FileFormat.PPTX, FileFormat.Bitmap, FileFormat.GIF, FileFormat.JPEG, FileFormat.PNG } },
            };
        }

        public static string ContentTypeFromFormat(FileFormat format)
        {
            if (!ContentTypeMap.ContainsKey(format)) return string.Empty;
            return ContentTypeMap[format];
        }

        public static string ExtensionFromFormat(FileFormat format)
        {
            switch (format)
            {
                case FileFormat.AVI: return "avi";
                case FileFormat.Bitmap: return "bmp";
                case FileFormat.Excel: return "xls";
                case FileFormat.FLV: return "flv";
                case FileFormat.GIF: return "gif";
                case FileFormat.JPEG: return "jpg";
                case FileFormat.MOV: return "mov";
                case FileFormat.MP3: return "mp3";
                case FileFormat.MP4: return "mp4";
                case FileFormat.MPG: return "mpg";
                case FileFormat.PDF: return "pdf";
                case FileFormat.PNG: return "png";
                case FileFormat.RAM: return "ram";
                case FileFormat.SWF: return "swf";
                case FileFormat.WMA: return "wma";
                case FileFormat.WMV: return "wmv";
                case FileFormat.Word: return "doc";
                case FileFormat.PPTX: return "pptx";
                case FileFormat.PPT:
                    return "ppt";

                default: return string.Empty;
            }
        }

        public static bool ExtensionIsRecognised(string extension)
        {
            return ExtensionFormatMap.ContainsKey(extension.ToLower());
        }

        public static FileFormat FormatFromFilename(string filename)
        {
            var extension = Path.GetExtension(filename).ToLower();
            if (ExtensionFormatMap.ContainsKey(extension))
                return ExtensionFormatMap[extension];
            else
                throw new Exception("Unrecognised extension. Call FileUtilities.ExtensionIsRecognised(string extension) to check before calling this method.");
        }

        public static FileFormat[] FileFormatsFromGroup(FileFormatGroup group)
        {
            if (!GroupToFileFormatMap.ContainsKey(group)) return null;
            return GroupToFileFormatMap[group];
        }

        public static bool FormatIsInGroup(FileFormat format, FileFormatGroup group)
        {
            if (!GroupToFileFormatMap.ContainsKey(group)) throw new Exception("Unmapped FileFormatGroup supplied");
            return new List<FileFormat>(GroupToFileFormatMap[group]).Contains(format);
        }

        public static FileFormatGroup[] GroupsFromFileFormat(FileFormat format)
        {
            if (!FileFormatToGroupMap.ContainsKey(format)) return null;
            return FileFormatToGroupMap[format];
        }

        public static string FilenameFromText(string text)
        {
            var re = new Regex("[^A-Za-z0-9]");
            text = re.Replace(text, "");
            return text;
        }
    }
}