﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web;
using System.Web.Script.Serialization;

namespace ITOK.Core.Common.Utils
{
    public static partial class ScriptRegistrar
    {
        private const string ScriptPathCacheKey = "ScriptPathCache";
        private const string ScriptVariableCacheKey = "ScriptVariableCache";
        private const string StylePathCacheKey = "StylePathCache";

        public static void RegisterScript(string path)
        {
            var scriptPaths = ScriptPaths;
            scriptPaths.Add(UrlUtilities.CdnContent(path.Trim()));
            ScriptPaths = scriptPaths;
        }

        public static void RegisterStyle(string path)
        {
            var stylePaths = StylePaths;
            stylePaths.Add(path.Trim());
            StylePaths = stylePaths;
        }

        public static void RegisterClientVariable(string name, string value)
        {
            var clientVariables = ScriptVariables;
            if (clientVariables.ContainsKey(name)) return; // throw new Exception(string.Format("a client variable with the name {0} has already been set with the value {1}", name, clientVariables[name]));
            clientVariables.Add(name, string.Format("\"{0}\"", value));
            ScriptVariables = clientVariables;
        }

        public static void RegisterClientVariable(string name, int value)
        {
            var clientVariables = ScriptVariables;
            if (clientVariables.ContainsKey(name)) return; // throw new Exception(string.Format("a client variable with the name {0} has already been set with the value {1}", name, clientVariables[name]));
            clientVariables.Add(name, value.ToString());
            ScriptVariables = clientVariables;
        }

        public static void RegisterClientVariable(string name, bool value)
        {
            var clientVariables = ScriptVariables;
            if (clientVariables.ContainsKey(name)) return;
            clientVariables.Add(name, value.ToString().ToLower());
            ScriptVariables = clientVariables;
        }

        public static void RegisterClientVariable(string name, object value)
        {
            var clientVariables = ScriptVariables;
            if (clientVariables.ContainsKey(name)) return; // throw new Exception(string.Format("a client variable with the name {0} has already been set with the value {1}", name, clientVariables[name]));
            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(value);

            clientVariables.Add(name, json);
            ScriptVariables = clientVariables;
        }

        public static void RegisterClientConstant<TEnum>() where TEnum : struct, IConvertible
        {
            if (!typeof(TEnum).IsEnum) throw new ArgumentException("TEnum must be an enumerated type");
            var name = typeof(TEnum).Name;
            var clientVariables = ScriptVariables;
            if (clientVariables.ContainsKey(name)) return;
            var jsEnum = new List<string>();
            foreach (TEnum value in Enum.GetValues(typeof(TEnum)))
            {
                var valueName = value.ToString().ToCharArray();
                var processedValueName = new List<char>();
                for (var i = 0; i < valueName.Length; i++)
                {
                    var letter = valueName[i];
                    if (char.IsUpper(letter) && i != 0)
                    {
                        processedValueName.Add('_');
                    }
                    processedValueName.Add(char.ToUpper(letter));
                }
                jsEnum.Add(string.Format("{0}:{1}", new string(processedValueName.ToArray()), value.ToInt32(CultureInfo.InvariantCulture).ToString()));
            }
            clientVariables.Add(name, "{" + string.Join(", ", jsEnum.ToArray()) + "}");
            ScriptVariables = clientVariables;
        }

        public static ISet<string> ScriptPaths
        {
            get
            {
                var scriptPaths = HttpContext.Current.Items[ScriptPathCacheKey] as ISet<string>;
                if (scriptPaths == null)
                {
                    scriptPaths = new HashSet<string>();
                    HttpContext.Current.Items[ScriptPathCacheKey] = scriptPaths;
                }
                return scriptPaths;
            }
            private set
            {
                HttpContext.Current.Items[ScriptPathCacheKey] = value;
            }
        }

        public static IDictionary<string, string> ScriptVariables
        {
            get
            {
                var scriptVariables = HttpContext.Current.Items[ScriptVariableCacheKey] as IDictionary<string, string>;
                if (scriptVariables == null)
                {
                    scriptVariables = new Dictionary<string, string>();
                    HttpContext.Current.Items[ScriptVariableCacheKey] = scriptVariables;
                }
                return scriptVariables;
            }
            private set
            {
                HttpContext.Current.Items[ScriptVariableCacheKey] = value;
            }
        }

        public static ISet<string> StylePaths
        {
            get
            {
                var stylePaths = HttpContext.Current.Items[StylePathCacheKey] as ISet<string>;
                if (stylePaths == null)
                {
                    stylePaths = new HashSet<string>();
                    HttpContext.Current.Items[StylePathCacheKey] = stylePaths;
                }
                return stylePaths;
            }
            private set
            {
                HttpContext.Current.Items[StylePathCacheKey] = value;
            }
        }
    }
}