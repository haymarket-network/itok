﻿using System.Text;
using System.Web.Mvc;
using ITOK.Core.Common.Utils;

namespace ITOK.Core.Common.HtmlExtensions
{
    public static class ScriptRegistrarExtensions
    {
        private const string ExternalScriptTemplate = "<script src='{0}' type='text/javascript'></script>";
        private const string InlineScriptTemplate = "<script type='text/javascript'>{0}</script>";
        private const string ClientVariableNamespace = "hncms";
        private const string StyleTemplate = "<link href='{0}' rel='Stylesheet' type='text/css' />";

        public static MvcHtmlString RenderScripts(this HtmlHelper helper)
        {
            var sb = new StringBuilder();
            foreach (var path in ScriptRegistrar.ScriptPaths)
            {
                sb.Append(string.Format(ExternalScriptTemplate, UrlUtilities.CdnContent(path)));
            }
            sb.Append(BuildClientVariableScript());
            return new MvcHtmlString(sb.ToString());
        }

        public static MvcHtmlString RenderStyles(this HtmlHelper helper)
        {
            var sb = new StringBuilder();
            foreach (var path in ScriptRegistrar.StylePaths)
            {
                sb.Append(string.Format(StyleTemplate, UrlUtilities.CdnContent(path)));
            }
            return new MvcHtmlString(sb.ToString());
        }

        #region Private Methods

        private static string BuildClientVariableScript()
        {
            if (ScriptRegistrar.ScriptVariables != null && ScriptRegistrar.ScriptVariables.Count != 0)
            {
                var sb = new StringBuilder("var " + ClientVariableNamespace + " = {};");
                foreach (var variableEntry in ScriptRegistrar.ScriptVariables)
                {
                    sb.Append(string.Format("{0}.{1} = {2};", ClientVariableNamespace, variableEntry.Key, variableEntry.Value));
                }
                return string.Format(InlineScriptTemplate, sb.ToString());
            }
            return string.Empty;
        }

        #endregion Private Methods
    }
}