﻿using System.Web.Mvc;

namespace Iaaf.Common.Extensions.Web.Html
{
    public static class ArticleExtensions
    {
        //public static string PrettifyArticleType(this HtmlHelper helper, ArticleType articleType)
        //{
        //    return ArticleUtilities.PrettifyArticleType(articleType);
        //}

        public static string PrettifyTopicType(this HtmlHelper helper, int topicType)
        {
            string TopicTypeName = "";
            switch (topicType)
            {
                case 0:
                    TopicTypeName = "General";
                    break;

                case 1:
                    TopicTypeName = "Country";
                    break;

                case 2:
                    TopicTypeName = "Medals";
                    break;

                case 3:
                    TopicTypeName = "Chinese";
                    break;
            }
            return TopicTypeName;
        }
    }
}