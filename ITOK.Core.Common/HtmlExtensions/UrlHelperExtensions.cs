﻿using System.Web.Mvc;
using ITOK.Core.Common.Config;
using ITOK.Core.Common.Utils;

namespace ITOK.Core.Common.HtmlExtensions
{
    public static class UrlHelperExtensions
    {
        public static string CdnContent(this UrlHelper helper, string contentPath)
        {
            return UrlUtilities.CdnContent(contentPath);
        }

        public static string GetCdnUri(this UrlHelper helper, string filename)
        {
            return string.Format("{0}/{1}",
                    ITOKConfig.Instance.Media.baseUri,
                    filename);
        }
    }
}