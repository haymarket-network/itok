﻿using System;
using System.Web.Mvc;
using ITOK.Core.Common.Utils;

namespace ITOK.Core.Common.Extensions
{
    public static class DateTimeExtensions
    {
        public static MvcHtmlString DateFormat(this HtmlHelper helper, DateTime? dateToFormat)
        {
            return MvcHtmlString.Create(DateTimeUtilities.DateFormat(dateToFormat));
        }

        public static MvcHtmlString DateTimeAgo(this HtmlHelper helper, DateTime? dateToFormat)
        {
            if (dateToFormat == null || dateToFormat.Value == DateTime.Parse("01/01/0001 00:00:00"))
                return MvcHtmlString.Create("");

            return MvcHtmlString.Create(DateTimeUtilities.TimeAgo(dateToFormat.Value));
        }

        /// <summary>
        /// dd MMMM yyyy
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="dateToFormat"></param>
        /// <returns></returns>
        public static MvcHtmlString DateFormatLong(this HtmlHelper helper, DateTime? dateToFormat)
        {
            if (dateToFormat == null) return MvcHtmlString.Create("");
            dateToFormat = dateToFormat.Value.ToLocalTime();
            return MvcHtmlString.Create(dateToFormat.Value.ToString("dd MMMM yyyy").ToUpper());
        }

        /// <summary>
        /// dddd dd MMMM yyyy
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="dateToFormat"></param>
        /// <returns></returns>
        public static MvcHtmlString DateFormatDay(this HtmlHelper helper, DateTime? dateToFormat)
        {
            if (dateToFormat == null) return MvcHtmlString.Create("");
            dateToFormat = dateToFormat.Value.ToLocalTime();
            return MvcHtmlString.Create(dateToFormat.Value.ToString("dddd dd MMMM yyyy").ToUpper());
        }

        public static MvcHtmlString DateFormatISO(this HtmlHelper helper, DateTime? dateToFormat)
        {
            return MvcHtmlString.Create(DateTimeUtilities.DateFormatISO(dateToFormat));
        }

        public static MvcHtmlString DateFormat(this HtmlHelper helper, DateTime start, DateTime? end, bool showYear = true)
        {
            if (end == null)
                return MvcHtmlString.Create(start.ToString("dd MMM yyyy").ToUpper());
            if (end.Value <= start) return MvcHtmlString.Create(start.ToString("dd MMM yyyy").ToUpper());
            end = end.Value.ToLocalTime();
            return MvcHtmlString.Create(start.ToString("dd MMM yyyy").ToUpper() + " - " + end.Value.ToString("dd MMM yyyy").ToUpper());
        }

        /// <summary>
        /// dd MMM
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="dateToFormat"></param>
        /// <returns></returns>
        public static MvcHtmlString DateNoYear(this HtmlHelper helper, DateTime? dateToFormat)
        {
            if (dateToFormat == null) return MvcHtmlString.Create("");
            dateToFormat = dateToFormat.Value.ToLocalTime();
            return MvcHtmlString.Create(dateToFormat.Value.ToString("dd MMM").ToUpper());
        }

        /// <summary>
        /// dd MMM yyyy HH:mm unless Time = "00:00:00"
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="dateToFormat"></param>
        /// <returns></returns>
        public static MvcHtmlString DateTimeFormat(this HtmlHelper helper, DateTime? date, bool HasTime = true)
        {
            return MvcHtmlString.Create(dtFormat(date, HasTime));
        }

        public static MvcHtmlString DateTimeFormatId(this HtmlHelper helper, DateTime? date, bool hasTime = true)
        {
            return MvcHtmlString.Create(dtFormat(date, hasTime).Replace(" ", "-"));
        }

        private static string dtFormat(DateTime? dateToFormat, bool HasTime = true)
        {
            if (dateToFormat == null || dateToFormat.Value == DateTime.Parse("01/01/0001 00:00:00"))
                return string.Empty;
            dateToFormat = dateToFormat.Value.ToLocalTime();

            if (dateToFormat.Value.TimeOfDay.ToString() != "00:00:00" && HasTime)
                return dateToFormat.Value.ToString("dd MMM yyyy HH:mm").ToUpper();
            return dateToFormat.Value.ToString("dd MMM yyyy").ToUpper();
        }

        /// <summary>
        /// HH:mm
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="dateToFormat"></param>
        /// <returns></returns>
        public static MvcHtmlString TimeFormat(this HtmlHelper helper, DateTime? dateToFormat)
        {
            if (dateToFormat == null || dateToFormat.Value == DateTime.Parse("01/01/0001 00:00:00"))
                return MvcHtmlString.Create("");
            //TimeZoneInfo russianTimeZone = TimeZoneInfo.FindSystemTimeZoneById
            //dateToFormat = dateToFormat.Value.ToLocalTime();

            if (dateToFormat.Value.TimeOfDay.ToString() != "00:00:00")
                return MvcHtmlString.Create(dateToFormat.Value.ToString("HH:mm").ToUpper());
            return MvcHtmlString.Create("00:00");
        }

        /// <summary>
        /// yyyy
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="dateToFormat"></param>
        /// <returns></returns>
        public static MvcHtmlString DateYear(this HtmlHelper helper, DateTime? dateToFormat)
        {
            if (dateToFormat == null || dateToFormat.Value == DateTime.Parse("01/01/0001 00:00:00"))
                return MvcHtmlString.Create("");
            dateToFormat = dateToFormat.Value.ToLocalTime();
            return MvcHtmlString.Create(dateToFormat.Value.ToString("yyyy").ToUpper());
        }

        

       

       

        public static string DateFormat(DateTime start, DateTime? end, bool showYear = true)
        {
            if (end == null)
                return start.ToString("dd MMM yyyy").ToUpper();
            if (end.Value <= start) return start.ToString("dd MMM yyyy").ToUpper();
            end = end.Value.ToLocalTime();
            return start.ToString("dd MMM yyyy").ToUpper() + " - " + end.Value.ToString("dd MMM yyyy").ToUpper();
        }
    }
}