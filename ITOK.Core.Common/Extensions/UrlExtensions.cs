﻿using System.Text.RegularExpressions;

namespace ITOK.Core.Common.Extensions.Strings
{
    public static class UrlExtensions
    {
        public static string GenerateSlug(this string phrase)
        {
            if (string.IsNullOrEmpty(phrase))
                return null;

            var str = phrase.RemoveAccent().ToLower();

            str = Regex.Replace(str, @"[^a-z0-9\s-]", ""); // invalid chars
            str = Regex.Replace(str, @"\s+", " ").Trim(); // convert multiple spaces into one space
            //str = str.Substring(0, str.Length <= 45 ? str.Length : 45).Trim(); // cut and trim it
            str = Regex.Replace(str, @"\s", "-"); // hyphens
            str = Regex.Replace(str, @"(-)\1{1,}", "-"); //replace duplicate hyphen with just one -

            return str;
        }

        public static string RemoveAccent(this string txt)
        {
            if (txt != null)
            {
                var bytes = System.Text.Encoding.GetEncoding("Cyrillic").GetBytes(txt);
                return System.Text.Encoding.ASCII.GetString(bytes);
            }
            else
                return "";
        }
    }
}