﻿using System.Configuration;

namespace ITOK.Core.Common.Config
{
    public class S3Element : ConfigurationElement
    {

        private const string PublicAccessKeyName = "accessKey";
        private const string PrivateAccessKeyName = "secretAccessKey";
        private const string BucketNameAttributeName = "bucketName";
        private const string BaseUriAttributeName = "baseUri";
        private const string PrivateKeyAttributeName = "privateKey";
        private const string PublicKeyAttributeName = "publicKey";
        private const string EndPointAttributeName = "endPoint";

        [ConfigurationProperty(PublicKeyAttributeName, IsRequired = true)]
        public string PublicAccessKey
        {
            get
            {
                return (string)this[PublicKeyAttributeName];
            }
        }
        [ConfigurationProperty(PrivateKeyAttributeName, IsRequired = true)]
        public string PrivateAccessKey
        {
            get
            {
                return (string)this[PrivateKeyAttributeName];
            }
        }

        [ConfigurationProperty(BucketNameAttributeName, IsRequired = true)]
        public string BucketName
        {
            get
            {
                return (string)this[BucketNameAttributeName];
            }
        }
        [ConfigurationProperty(BaseUriAttributeName, IsRequired = true)]
        public string BaseUri
        {
            get
            {
                return (string)this[BaseUriAttributeName];
            }
        }


        [ConfigurationProperty(EndPointAttributeName, IsRequired = true)]
        public string EndPoint
        {
            get
            {
                return (string)this[EndPointAttributeName];
            }
        }


    }
}
