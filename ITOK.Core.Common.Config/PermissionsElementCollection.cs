﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using ITOK.Core.Common.Constants;

namespace ITOK.Core.Common.Config
{
    public class PermissionsElementCollection : ConfigurationElementCollection
    {
        public PermissionsElement this[Permission key]
        {
            get
            {
                return BaseGet(key) as PermissionsElement;
            }
        }

        public IList<PermissionsElement> ToList()
        {
            return this.Cast<PermissionsElement>().ToList();
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new PermissionsElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((PermissionsElement)element).Name;
        }
    }
}