﻿using System;
using System.Configuration;

namespace ITOK.Core.Common.Config
{
    public class MediaElement : ConfigurationElement
    {
        private const string DirectoryAttributeName = "directory";
        private const string BaseURIAttributeName = "baseUri";
        private const string SanLocationName = "sanLocation";

        [ConfigurationProperty(DirectoryAttributeName, IsRequired = true)]
        public Uri Directory
        {
            get
            {
                return this[DirectoryAttributeName] as Uri;
            }
        }


       

        [ConfigurationProperty(BaseURIAttributeName, IsRequired = true)]
        public string baseUri
        {
            get
            {
                return (string)this[BaseURIAttributeName];
            }
        }


        [ConfigurationProperty(SanLocationName, IsRequired = true)]
        public string sanLocation
        {
            get
            {
                return (string)this[SanLocationName];
            }
        }
        public override bool IsReadOnly()
        {
            return false;
        }
    }
}