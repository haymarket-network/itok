﻿using System.Configuration;

namespace ITOK.Core.Common.Config
{
    public class AWSElement : ConfigurationElement
    {
        private const string PublicAccessKeyName = "accessKey";
        private const string PrivateAccessKeyName = "secretAccessKey";

        private const string PrivateKeyAttributeName = "privateKey";
        private const string PublicKeyAttributeName = "publicKey";

        private const string VerifiedEmailAddress = "verifiedEmailAddress";

        [ConfigurationProperty(PublicKeyAttributeName, IsRequired = true)]
        public string PublicAccessKey
        {
            get
            {
                return (string)this[PublicKeyAttributeName];
            }
        }

        [ConfigurationProperty(PrivateKeyAttributeName, IsRequired = true)]
        public string PrivateAccessKey
        {
            get
            {
                return (string)this[PrivateKeyAttributeName];
            }
        }

        [ConfigurationProperty(VerifiedEmailAddress, IsRequired = true)]
        public string VerifiedEmail
        {
            get
            {
                return (string)this[VerifiedEmailAddress];
            }
        }
    }
}