﻿using System;
using System.Collections.Generic;
using System.Configuration;
using ITOK.Core.Common.Constants;

namespace ITOK.Core.Common.Config
{
    public class MediaProfileElementCollection : ConfigurationElementCollection
    {
        public MediaProfileElement GetProfileConfig(MediaProfile profile)
        {
            var mediaProfile = BaseGet(profile) as MediaProfileElement;
            if (mediaProfile == null) throw new Exception(string.Format("Couldn't find configuration for media profile: {0}", profile.ToString()));
            return mediaProfile;
        }

        public IList<MediaProfileElement> GetProfileConfigsFromRatio(MediaRatio ratio)
        {
            IList<MediaProfileElement> profilesWithRatio = new List<MediaProfileElement>();
            foreach (MediaProfileElement mediaProfile in this)
            {
                if (mediaProfile.Ratio == ratio) profilesWithRatio.Add(mediaProfile);
            }
            return profilesWithRatio;
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new MediaProfileElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((MediaProfileElement)element).ProfileName;
        }
    }
}