﻿using System.Configuration;

namespace ITOK.Core.Common.Config
{
    public class MongoOutputCacheElement : ConfigurationElement
    {
        private const string MongoOutputCacheAttributeName = "expirytime";

        [ConfigurationProperty(MongoOutputCacheAttributeName, IsRequired = true)]
        public int ExpiryTime
        {
            get
            {
                return int.Parse(this[MongoOutputCacheAttributeName].ToString());
            }
        }

        public override bool IsReadOnly()
        {
            return false;
        }
    }
}