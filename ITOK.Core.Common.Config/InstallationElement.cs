﻿using System.Configuration;

namespace ITOK.Core.Common.Config
{
    public class InstallationElement : ConfigurationElement
    {
        private const string InstallationName = "status";

        [ConfigurationProperty(InstallationName, IsRequired = true)]
        public InstallationStatus Status
        {
            get
            {
                return (InstallationStatus)this[InstallationName];
            }
        }

        public override bool IsReadOnly()
        {
            return false;
        }
    }
}