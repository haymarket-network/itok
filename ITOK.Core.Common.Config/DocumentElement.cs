﻿using System.Configuration;

namespace ITOK.Core.Common.Config
{
    public class DocumentElement : ConfigurationElement
    {
        private const string DirectoryAttributeName = "directory";

        [ConfigurationProperty(DirectoryAttributeName, IsRequired = true)]
        public string Directory
        {
            get
            {
                return this[DirectoryAttributeName] as string;
            }
        }

        public override bool IsReadOnly()
        {
            return false;
        }
    }
}