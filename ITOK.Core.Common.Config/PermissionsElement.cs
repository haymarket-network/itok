﻿using System.Configuration;
using ITOK.Core.Common.Constants;

namespace ITOK.Core.Common.Config
{
    public class PermissionsElement : ConfigurationElement
    {
        private const string NameAttributeName = "name";
        private const string DisplayNameAttributeName = "displayName";
        private const string DescriptionAttributeName = "description";

        [ConfigurationProperty(NameAttributeName, IsRequired = true)]
        public Permission Name
        {
            get
            {
                return (Permission)this[NameAttributeName];
            }
        }

        [ConfigurationProperty(DisplayNameAttributeName, IsRequired = true)]
        public string DisplayName
        {
            get
            {
                return (string)this[DisplayNameAttributeName];
            }
        }

        [ConfigurationProperty(DescriptionAttributeName, IsRequired = true)]
        public string Description
        {
            get
            {
                return (string)this[DescriptionAttributeName];
            }
        }
    }
}