﻿using System.Configuration;
using ITOK.Core.Common.Constants;

namespace ITOK.Core.Common.Config
{
    public class MediaRatioElement : ConfigurationElement
    {
        private const string RatioNameAttributeName = "ratioName";
        private const string PrettyRatioNameAttributeName = "prettyRatioName";
        private const string MediaWidthAttributeName = "width";
        private const string MediaHeightAttributeName = "height";
        private const string RequiredAttributeName = "required";

        [ConfigurationProperty(RatioNameAttributeName, IsKey = true, IsRequired = true)]
        public MediaRatio RatioName
        {
            get { return (MediaRatio)this[RatioNameAttributeName]; }
        }

        [ConfigurationProperty(PrettyRatioNameAttributeName, IsRequired = true)]
        public string PrettyRatioName
        {
            get { return (string)this[PrettyRatioNameAttributeName]; }
        }

        [ConfigurationProperty(MediaWidthAttributeName, IsRequired = true)]
        public int Width
        {
            get { return (int)this[MediaWidthAttributeName]; }
        }

        [ConfigurationProperty(MediaHeightAttributeName, IsRequired = true)]
        public int Height
        {
            get { return (int)this[MediaHeightAttributeName]; }
        }

        [ConfigurationProperty(RequiredAttributeName, IsRequired = true)]
        public bool IsRequired
        {
            get { return (bool)this[RequiredAttributeName]; }
        }
    }
}